using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='PlaceDetail']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/PlaceDetail", DoNotGenerateAcw=true)]
	public partial class PlaceDetail : global::Com.Gimbal.Protocol.Place {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/PlaceDetail", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PlaceDetail); }
		}

		protected PlaceDetail (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='PlaceDetail']/constructor[@name='PlaceDetail' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe PlaceDetail ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (PlaceDetail)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAttributes;
#pragma warning disable 0169
		static Delegate GetGetAttributesHandler ()
		{
			if (cb_getAttributes == null)
				cb_getAttributes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAttributes);
			return cb_getAttributes;
		}

		static IntPtr n_GetAttributes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.PlaceDetail __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.PlaceDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaDictionary<string, string>.ToLocalJniHandle (__this.Attributes);
		}
#pragma warning restore 0169

		static Delegate cb_setAttributes_Ljava_util_Map_;
#pragma warning disable 0169
		static Delegate GetSetAttributes_Ljava_util_Map_Handler ()
		{
			if (cb_setAttributes_Ljava_util_Map_ == null)
				cb_setAttributes_Ljava_util_Map_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAttributes_Ljava_util_Map_);
			return cb_setAttributes_Ljava_util_Map_;
		}

		static void n_SetAttributes_Ljava_util_Map_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.PlaceDetail __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.PlaceDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaDictionary<string, string>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Attributes = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getAttributes;
		static IntPtr id_setAttributes_Ljava_util_Map_;
		public virtual unsafe global::System.Collections.Generic.IDictionary<string, string> Attributes {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='PlaceDetail']/method[@name='getAttributes' and count(parameter)=0]"
			[Register ("getAttributes", "()Ljava/util/Map;", "GetGetAttributesHandler")]
			get {
				if (id_getAttributes == IntPtr.Zero)
					id_getAttributes = JNIEnv.GetMethodID (class_ref, "getAttributes", "()Ljava/util/Map;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaDictionary<string, string>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getAttributes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaDictionary<string, string>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAttributes", "()Ljava/util/Map;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='PlaceDetail']/method[@name='setAttributes' and count(parameter)=1 and parameter[1][@type='java.util.Map&lt;java.lang.String, java.lang.String&gt;']]"
			[Register ("setAttributes", "(Ljava/util/Map;)V", "GetSetAttributes_Ljava_util_Map_Handler")]
			set {
				if (id_setAttributes_Ljava_util_Map_ == IntPtr.Zero)
					id_setAttributes_Ljava_util_Map_ = JNIEnv.GetMethodID (class_ref, "setAttributes", "(Ljava/util/Map;)V");
				IntPtr native_value = global::Android.Runtime.JavaDictionary<string, string>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setAttributes_Ljava_util_Map_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAttributes", "(Ljava/util/Map;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getBeacons;
#pragma warning disable 0169
		static Delegate GetGetBeaconsHandler ()
		{
			if (cb_getBeacons == null)
				cb_getBeacons = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBeacons);
			return cb_getBeacons;
		}

		static IntPtr n_GetBeacons (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.PlaceDetail __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.PlaceDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Beacon>.ToLocalJniHandle (__this.Beacons);
		}
#pragma warning restore 0169

		static Delegate cb_setBeacons_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetBeacons_Ljava_util_List_Handler ()
		{
			if (cb_setBeacons_Ljava_util_List_ == null)
				cb_setBeacons_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetBeacons_Ljava_util_List_);
			return cb_setBeacons_Ljava_util_List_;
		}

		static void n_SetBeacons_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.PlaceDetail __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.PlaceDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Beacon>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Beacons = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getBeacons;
		static IntPtr id_setBeacons_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Protocol.Beacon> Beacons {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='PlaceDetail']/method[@name='getBeacons' and count(parameter)=0]"
			[Register ("getBeacons", "()Ljava/util/List;", "GetGetBeaconsHandler")]
			get {
				if (id_getBeacons == IntPtr.Zero)
					id_getBeacons = JNIEnv.GetMethodID (class_ref, "getBeacons", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Beacon>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getBeacons), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Beacon>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBeacons", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='PlaceDetail']/method[@name='setBeacons' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.protocol.Beacon&gt;']]"
			[Register ("setBeacons", "(Ljava/util/List;)V", "GetSetBeacons_Ljava_util_List_Handler")]
			set {
				if (id_setBeacons_Ljava_util_List_ == IntPtr.Zero)
					id_setBeacons_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setBeacons", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Beacon>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setBeacons_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setBeacons", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getGeofence;
#pragma warning disable 0169
		static Delegate GetGetGeofenceHandler ()
		{
			if (cb_getGeofence == null)
				cb_getGeofence = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetGeofence);
			return cb_getGeofence;
		}

		static IntPtr n_GetGeofence (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.PlaceDetail __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.PlaceDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Geofence);
		}
#pragma warning restore 0169

		static Delegate cb_setGeofence_Lcom_gimbal_protocol_Geofence_;
#pragma warning disable 0169
		static Delegate GetSetGeofence_Lcom_gimbal_protocol_Geofence_Handler ()
		{
			if (cb_setGeofence_Lcom_gimbal_protocol_Geofence_ == null)
				cb_setGeofence_Lcom_gimbal_protocol_Geofence_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetGeofence_Lcom_gimbal_protocol_Geofence_);
			return cb_setGeofence_Lcom_gimbal_protocol_Geofence_;
		}

		static void n_SetGeofence_Lcom_gimbal_protocol_Geofence_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.PlaceDetail __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.PlaceDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Protocol.Geofence p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Geofence = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getGeofence;
		static IntPtr id_setGeofence_Lcom_gimbal_protocol_Geofence_;
		public virtual unsafe global::Com.Gimbal.Protocol.Geofence Geofence {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='PlaceDetail']/method[@name='getGeofence' and count(parameter)=0]"
			[Register ("getGeofence", "()Lcom/gimbal/protocol/Geofence;", "GetGetGeofenceHandler")]
			get {
				if (id_getGeofence == IntPtr.Zero)
					id_getGeofence = JNIEnv.GetMethodID (class_ref, "getGeofence", "()Lcom/gimbal/protocol/Geofence;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (JNIEnv.CallObjectMethod  (Handle, id_getGeofence), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getGeofence", "()Lcom/gimbal/protocol/Geofence;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='PlaceDetail']/method[@name='setGeofence' and count(parameter)=1 and parameter[1][@type='com.gimbal.protocol.Geofence']]"
			[Register ("setGeofence", "(Lcom/gimbal/protocol/Geofence;)V", "GetSetGeofence_Lcom_gimbal_protocol_Geofence_Handler")]
			set {
				if (id_setGeofence_Lcom_gimbal_protocol_Geofence_ == IntPtr.Zero)
					id_setGeofence_Lcom_gimbal_protocol_Geofence_ = JNIEnv.GetMethodID (class_ref, "setGeofence", "(Lcom/gimbal/protocol/Geofence;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setGeofence_Lcom_gimbal_protocol_Geofence_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setGeofence", "(Lcom/gimbal/protocol/Geofence;)V"), __args);
				} finally {
				}
			}
		}

	}
}
