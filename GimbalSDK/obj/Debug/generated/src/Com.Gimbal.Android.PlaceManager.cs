using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceManager']"
	[global::Android.Runtime.Register ("com/gimbal/android/PlaceManager", DoNotGenerateAcw=true)]
	public partial class PlaceManager : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/PlaceManager", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PlaceManager); }
		}

		protected PlaceManager (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_getInstance;
		public static unsafe global::Com.Gimbal.Android.PlaceManager Instance {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceManager']/method[@name='getInstance' and count(parameter)=0]"
			[Register ("getInstance", "()Lcom/gimbal/android/PlaceManager;", "GetGetInstanceHandler")]
			get {
				if (id_getInstance == IntPtr.Zero)
					id_getInstance = JNIEnv.GetStaticMethodID (class_ref, "getInstance", "()Lcom/gimbal/android/PlaceManager;");
				try {
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.PlaceManager> (JNIEnv.CallStaticObjectMethod  (class_ref, id_getInstance), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_isMonitoring;
#pragma warning disable 0169
		static Delegate GetIsMonitoringHandler ()
		{
			if (cb_isMonitoring == null)
				cb_isMonitoring = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsMonitoring);
			return cb_isMonitoring;
		}

		static bool n_IsMonitoring (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.PlaceManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.PlaceManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsMonitoring;
		}
#pragma warning restore 0169

		static IntPtr id_isMonitoring;
		public virtual unsafe bool IsMonitoring {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceManager']/method[@name='isMonitoring' and count(parameter)=0]"
			[Register ("isMonitoring", "()Z", "GetIsMonitoringHandler")]
			get {
				if (id_isMonitoring == IntPtr.Zero)
					id_isMonitoring = JNIEnv.GetMethodID (class_ref, "isMonitoring", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isMonitoring);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isMonitoring", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_addListener_Lcom_gimbal_android_PlaceEventListener_;
#pragma warning disable 0169
		static Delegate GetAddListener_Lcom_gimbal_android_PlaceEventListener_Handler ()
		{
			if (cb_addListener_Lcom_gimbal_android_PlaceEventListener_ == null)
				cb_addListener_Lcom_gimbal_android_PlaceEventListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddListener_Lcom_gimbal_android_PlaceEventListener_);
			return cb_addListener_Lcom_gimbal_android_PlaceEventListener_;
		}

		static void n_AddListener_Lcom_gimbal_android_PlaceEventListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Android.PlaceManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.PlaceManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.PlaceEventListener p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.PlaceEventListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addListener_Lcom_gimbal_android_PlaceEventListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceManager']/method[@name='addListener' and count(parameter)=1 and parameter[1][@type='com.gimbal.android.PlaceEventListener']]"
		[Register ("addListener", "(Lcom/gimbal/android/PlaceEventListener;)V", "GetAddListener_Lcom_gimbal_android_PlaceEventListener_Handler")]
		public virtual unsafe void AddListener (global::Com.Gimbal.Android.PlaceEventListener p0)
		{
			if (id_addListener_Lcom_gimbal_android_PlaceEventListener_ == IntPtr.Zero)
				id_addListener_Lcom_gimbal_android_PlaceEventListener_ = JNIEnv.GetMethodID (class_ref, "addListener", "(Lcom/gimbal/android/PlaceEventListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addListener_Lcom_gimbal_android_PlaceEventListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addListener", "(Lcom/gimbal/android/PlaceEventListener;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_currentVisits;
#pragma warning disable 0169
		static Delegate GetCurrentVisitsHandler ()
		{
			if (cb_currentVisits == null)
				cb_currentVisits = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_CurrentVisits);
			return cb_currentVisits;
		}

		static IntPtr n_CurrentVisits (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.PlaceManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.PlaceManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Android.Visit>.ToLocalJniHandle (__this.CurrentVisits ());
		}
#pragma warning restore 0169

		static IntPtr id_currentVisits;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceManager']/method[@name='currentVisits' and count(parameter)=0]"
		[Register ("currentVisits", "()Ljava/util/List;", "GetCurrentVisitsHandler")]
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Android.Visit> CurrentVisits ()
		{
			if (id_currentVisits == IntPtr.Zero)
				id_currentVisits = JNIEnv.GetMethodID (class_ref, "currentVisits", "()Ljava/util/List;");
			try {

				if (GetType () == ThresholdType)
					return global::Android.Runtime.JavaList<global::Com.Gimbal.Android.Visit>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_currentVisits), JniHandleOwnership.TransferLocalRef);
				else
					return global::Android.Runtime.JavaList<global::Com.Gimbal.Android.Visit>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "currentVisits", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_removeListener_Lcom_gimbal_android_PlaceEventListener_;
#pragma warning disable 0169
		static Delegate GetRemoveListener_Lcom_gimbal_android_PlaceEventListener_Handler ()
		{
			if (cb_removeListener_Lcom_gimbal_android_PlaceEventListener_ == null)
				cb_removeListener_Lcom_gimbal_android_PlaceEventListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RemoveListener_Lcom_gimbal_android_PlaceEventListener_);
			return cb_removeListener_Lcom_gimbal_android_PlaceEventListener_;
		}

		static void n_RemoveListener_Lcom_gimbal_android_PlaceEventListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Android.PlaceManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.PlaceManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.PlaceEventListener p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.PlaceEventListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_removeListener_Lcom_gimbal_android_PlaceEventListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceManager']/method[@name='removeListener' and count(parameter)=1 and parameter[1][@type='com.gimbal.android.PlaceEventListener']]"
		[Register ("removeListener", "(Lcom/gimbal/android/PlaceEventListener;)V", "GetRemoveListener_Lcom_gimbal_android_PlaceEventListener_Handler")]
		public virtual unsafe void RemoveListener (global::Com.Gimbal.Android.PlaceEventListener p0)
		{
			if (id_removeListener_Lcom_gimbal_android_PlaceEventListener_ == IntPtr.Zero)
				id_removeListener_Lcom_gimbal_android_PlaceEventListener_ = JNIEnv.GetMethodID (class_ref, "removeListener", "(Lcom/gimbal/android/PlaceEventListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_removeListener_Lcom_gimbal_android_PlaceEventListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "removeListener", "(Lcom/gimbal/android/PlaceEventListener;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_startMonitoring;
#pragma warning disable 0169
		static Delegate GetStartMonitoringHandler ()
		{
			if (cb_startMonitoring == null)
				cb_startMonitoring = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StartMonitoring);
			return cb_startMonitoring;
		}

		static void n_StartMonitoring (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.PlaceManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.PlaceManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StartMonitoring ();
		}
#pragma warning restore 0169

		static IntPtr id_startMonitoring;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceManager']/method[@name='startMonitoring' and count(parameter)=0]"
		[Register ("startMonitoring", "()V", "GetStartMonitoringHandler")]
		public virtual unsafe void StartMonitoring ()
		{
			if (id_startMonitoring == IntPtr.Zero)
				id_startMonitoring = JNIEnv.GetMethodID (class_ref, "startMonitoring", "()V");
			try {

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_startMonitoring);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "startMonitoring", "()V"));
			} finally {
			}
		}

		static Delegate cb_stopMonitoring;
#pragma warning disable 0169
		static Delegate GetStopMonitoringHandler ()
		{
			if (cb_stopMonitoring == null)
				cb_stopMonitoring = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StopMonitoring);
			return cb_stopMonitoring;
		}

		static void n_StopMonitoring (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.PlaceManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.PlaceManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StopMonitoring ();
		}
#pragma warning restore 0169

		static IntPtr id_stopMonitoring;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceManager']/method[@name='stopMonitoring' and count(parameter)=0]"
		[Register ("stopMonitoring", "()V", "GetStopMonitoringHandler")]
		public virtual unsafe void StopMonitoring ()
		{
			if (id_stopMonitoring == IntPtr.Zero)
				id_stopMonitoring = JNIEnv.GetMethodID (class_ref, "stopMonitoring", "()V");
			try {

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_stopMonitoring);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "stopMonitoring", "()V"));
			} finally {
			}
		}

	}
}
