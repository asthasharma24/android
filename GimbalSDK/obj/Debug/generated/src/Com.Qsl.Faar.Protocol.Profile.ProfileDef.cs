using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Profile {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='ProfileDef']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/profile/ProfileDef", DoNotGenerateAcw=true)]
	public partial class ProfileDef : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/profile/ProfileDef", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ProfileDef); }
		}

		protected ProfileDef (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='ProfileDef']/constructor[@name='ProfileDef' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ProfileDef ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ProfileDef)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAttributes;
#pragma warning disable 0169
		static Delegate GetGetAttributesHandler ()
		{
			if (cb_getAttributes == null)
				cb_getAttributes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAttributes);
			return cb_getAttributes;
		}

		static IntPtr n_GetAttributes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Profile.ProfileDef __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.ProfileDef> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaDictionary<string, global::System.Collections.Generic.IList<string>>.ToLocalJniHandle (__this.Attributes);
		}
#pragma warning restore 0169

		static Delegate cb_setAttributes_Ljava_util_Map_;
#pragma warning disable 0169
		static Delegate GetSetAttributes_Ljava_util_Map_Handler ()
		{
			if (cb_setAttributes_Ljava_util_Map_ == null)
				cb_setAttributes_Ljava_util_Map_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAttributes_Ljava_util_Map_);
			return cb_setAttributes_Ljava_util_Map_;
		}

		static void n_SetAttributes_Ljava_util_Map_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Profile.ProfileDef __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.ProfileDef> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaDictionary<string, global::System.Collections.Generic.IList<string>>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Attributes = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getAttributes;
		static IntPtr id_setAttributes_Ljava_util_Map_;
		public virtual unsafe global::System.Collections.Generic.IDictionary<string, global::System.Collections.Generic.IList<string>> Attributes {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='ProfileDef']/method[@name='getAttributes' and count(parameter)=0]"
			[Register ("getAttributes", "()Ljava/util/Map;", "GetGetAttributesHandler")]
			get {
				if (id_getAttributes == IntPtr.Zero)
					id_getAttributes = JNIEnv.GetMethodID (class_ref, "getAttributes", "()Ljava/util/Map;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaDictionary<string, global::System.Collections.Generic.IList<string>>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getAttributes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaDictionary<string, global::System.Collections.Generic.IList<string>>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAttributes", "()Ljava/util/Map;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='ProfileDef']/method[@name='setAttributes' and count(parameter)=1 and parameter[1][@type='java.util.Map&lt;java.lang.String, java.util.List&lt;java.lang.String&gt;&gt;']]"
			[Register ("setAttributes", "(Ljava/util/Map;)V", "GetSetAttributes_Ljava_util_Map_Handler")]
			set {
				if (id_setAttributes_Ljava_util_Map_ == IntPtr.Zero)
					id_setAttributes_Ljava_util_Map_ = JNIEnv.GetMethodID (class_ref, "setAttributes", "(Ljava/util/Map;)V");
				IntPtr native_value = global::Android.Runtime.JavaDictionary<string, global::System.Collections.Generic.IList<string>>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setAttributes_Ljava_util_Map_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAttributes", "(Ljava/util/Map;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
