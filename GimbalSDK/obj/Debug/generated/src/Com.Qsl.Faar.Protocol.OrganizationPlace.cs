using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/OrganizationPlace", DoNotGenerateAcw=true)]
	public partial class OrganizationPlace : global::Com.Qsl.Faar.Protocol.BasePlace {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/OrganizationPlace", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (OrganizationPlace); }
		}

		protected OrganizationPlace (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/constructor[@name='OrganizationPlace' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe OrganizationPlace ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (OrganizationPlace)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAddressLineOne;
#pragma warning disable 0169
		static Delegate GetGetAddressLineOneHandler ()
		{
			if (cb_getAddressLineOne == null)
				cb_getAddressLineOne = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAddressLineOne);
			return cb_getAddressLineOne;
		}

		static IntPtr n_GetAddressLineOne (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.AddressLineOne);
		}
#pragma warning restore 0169

		static Delegate cb_setAddressLineOne_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetAddressLineOne_Ljava_lang_String_Handler ()
		{
			if (cb_setAddressLineOne_Ljava_lang_String_ == null)
				cb_setAddressLineOne_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAddressLineOne_Ljava_lang_String_);
			return cb_setAddressLineOne_Ljava_lang_String_;
		}

		static void n_SetAddressLineOne_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddressLineOne = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getAddressLineOne;
		static IntPtr id_setAddressLineOne_Ljava_lang_String_;
		public virtual unsafe string AddressLineOne {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='getAddressLineOne' and count(parameter)=0]"
			[Register ("getAddressLineOne", "()Ljava/lang/String;", "GetGetAddressLineOneHandler")]
			get {
				if (id_getAddressLineOne == IntPtr.Zero)
					id_getAddressLineOne = JNIEnv.GetMethodID (class_ref, "getAddressLineOne", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getAddressLineOne), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAddressLineOne", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='setAddressLineOne' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setAddressLineOne", "(Ljava/lang/String;)V", "GetSetAddressLineOne_Ljava_lang_String_Handler")]
			set {
				if (id_setAddressLineOne_Ljava_lang_String_ == IntPtr.Zero)
					id_setAddressLineOne_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setAddressLineOne", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setAddressLineOne_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAddressLineOne", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getAddressLineTwo;
#pragma warning disable 0169
		static Delegate GetGetAddressLineTwoHandler ()
		{
			if (cb_getAddressLineTwo == null)
				cb_getAddressLineTwo = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAddressLineTwo);
			return cb_getAddressLineTwo;
		}

		static IntPtr n_GetAddressLineTwo (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.AddressLineTwo);
		}
#pragma warning restore 0169

		static Delegate cb_setAddressLineTwo_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetAddressLineTwo_Ljava_lang_String_Handler ()
		{
			if (cb_setAddressLineTwo_Ljava_lang_String_ == null)
				cb_setAddressLineTwo_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAddressLineTwo_Ljava_lang_String_);
			return cb_setAddressLineTwo_Ljava_lang_String_;
		}

		static void n_SetAddressLineTwo_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddressLineTwo = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getAddressLineTwo;
		static IntPtr id_setAddressLineTwo_Ljava_lang_String_;
		public virtual unsafe string AddressLineTwo {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='getAddressLineTwo' and count(parameter)=0]"
			[Register ("getAddressLineTwo", "()Ljava/lang/String;", "GetGetAddressLineTwoHandler")]
			get {
				if (id_getAddressLineTwo == IntPtr.Zero)
					id_getAddressLineTwo = JNIEnv.GetMethodID (class_ref, "getAddressLineTwo", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getAddressLineTwo), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAddressLineTwo", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='setAddressLineTwo' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setAddressLineTwo", "(Ljava/lang/String;)V", "GetSetAddressLineTwo_Ljava_lang_String_Handler")]
			set {
				if (id_setAddressLineTwo_Ljava_lang_String_ == IntPtr.Zero)
					id_setAddressLineTwo_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setAddressLineTwo", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setAddressLineTwo_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAddressLineTwo", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getCity;
#pragma warning disable 0169
		static Delegate GetGetCityHandler ()
		{
			if (cb_getCity == null)
				cb_getCity = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCity);
			return cb_getCity;
		}

		static IntPtr n_GetCity (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.City);
		}
#pragma warning restore 0169

		static Delegate cb_setCity_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetCity_Ljava_lang_String_Handler ()
		{
			if (cb_setCity_Ljava_lang_String_ == null)
				cb_setCity_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCity_Ljava_lang_String_);
			return cb_setCity_Ljava_lang_String_;
		}

		static void n_SetCity_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.City = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getCity;
		static IntPtr id_setCity_Ljava_lang_String_;
		public virtual unsafe string City {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='getCity' and count(parameter)=0]"
			[Register ("getCity", "()Ljava/lang/String;", "GetGetCityHandler")]
			get {
				if (id_getCity == IntPtr.Zero)
					id_getCity = JNIEnv.GetMethodID (class_ref, "getCity", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getCity), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCity", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='setCity' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setCity", "(Ljava/lang/String;)V", "GetSetCity_Ljava_lang_String_Handler")]
			set {
				if (id_setCity_Ljava_lang_String_ == IntPtr.Zero)
					id_setCity_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setCity", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCity_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCity", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getCountry;
#pragma warning disable 0169
		static Delegate GetGetCountryHandler ()
		{
			if (cb_getCountry == null)
				cb_getCountry = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCountry);
			return cb_getCountry;
		}

		static IntPtr n_GetCountry (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Country);
		}
#pragma warning restore 0169

		static Delegate cb_setCountry_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetCountry_Ljava_lang_String_Handler ()
		{
			if (cb_setCountry_Ljava_lang_String_ == null)
				cb_setCountry_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCountry_Ljava_lang_String_);
			return cb_setCountry_Ljava_lang_String_;
		}

		static void n_SetCountry_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Country = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getCountry;
		static IntPtr id_setCountry_Ljava_lang_String_;
		public virtual unsafe string Country {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='getCountry' and count(parameter)=0]"
			[Register ("getCountry", "()Ljava/lang/String;", "GetGetCountryHandler")]
			get {
				if (id_getCountry == IntPtr.Zero)
					id_getCountry = JNIEnv.GetMethodID (class_ref, "getCountry", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getCountry), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCountry", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='setCountry' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setCountry", "(Ljava/lang/String;)V", "GetSetCountry_Ljava_lang_String_Handler")]
			set {
				if (id_setCountry_Ljava_lang_String_ == IntPtr.Zero)
					id_setCountry_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setCountry", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCountry_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCountry", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_isHideGeoFence;
#pragma warning disable 0169
		static Delegate GetIsHideGeoFenceHandler ()
		{
			if (cb_isHideGeoFence == null)
				cb_isHideGeoFence = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsHideGeoFence);
			return cb_isHideGeoFence;
		}

		static bool n_IsHideGeoFence (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HideGeoFence;
		}
#pragma warning restore 0169

		static Delegate cb_setHideGeoFence_Z;
#pragma warning disable 0169
		static Delegate GetSetHideGeoFence_ZHandler ()
		{
			if (cb_setHideGeoFence_Z == null)
				cb_setHideGeoFence_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetHideGeoFence_Z);
			return cb_setHideGeoFence_Z;
		}

		static void n_SetHideGeoFence_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.HideGeoFence = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isHideGeoFence;
		static IntPtr id_setHideGeoFence_Z;
		public virtual unsafe bool HideGeoFence {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='isHideGeoFence' and count(parameter)=0]"
			[Register ("isHideGeoFence", "()Z", "GetIsHideGeoFenceHandler")]
			get {
				if (id_isHideGeoFence == IntPtr.Zero)
					id_isHideGeoFence = JNIEnv.GetMethodID (class_ref, "isHideGeoFence", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isHideGeoFence);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isHideGeoFence", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='setHideGeoFence' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setHideGeoFence", "(Z)V", "GetSetHideGeoFence_ZHandler")]
			set {
				if (id_setHideGeoFence_Z == IntPtr.Zero)
					id_setHideGeoFence_Z = JNIEnv.GetMethodID (class_ref, "setHideGeoFence", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setHideGeoFence_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setHideGeoFence", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getState;
#pragma warning disable 0169
		static Delegate GetGetStateHandler ()
		{
			if (cb_getState == null)
				cb_getState = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetState);
			return cb_getState;
		}

		static IntPtr n_GetState (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.State);
		}
#pragma warning restore 0169

		static Delegate cb_setState_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetState_Ljava_lang_String_Handler ()
		{
			if (cb_setState_Ljava_lang_String_ == null)
				cb_setState_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetState_Ljava_lang_String_);
			return cb_setState_Ljava_lang_String_;
		}

		static void n_SetState_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.State = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getState;
		static IntPtr id_setState_Ljava_lang_String_;
		public virtual unsafe string State {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='getState' and count(parameter)=0]"
			[Register ("getState", "()Ljava/lang/String;", "GetGetStateHandler")]
			get {
				if (id_getState == IntPtr.Zero)
					id_getState = JNIEnv.GetMethodID (class_ref, "getState", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getState), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getState", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='setState' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setState", "(Ljava/lang/String;)V", "GetSetState_Ljava_lang_String_Handler")]
			set {
				if (id_setState_Ljava_lang_String_ == IntPtr.Zero)
					id_setState_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setState", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setState_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setState", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getZipcode;
#pragma warning disable 0169
		static Delegate GetGetZipcodeHandler ()
		{
			if (cb_getZipcode == null)
				cb_getZipcode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetZipcode);
			return cb_getZipcode;
		}

		static IntPtr n_GetZipcode (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Zipcode);
		}
#pragma warning restore 0169

		static Delegate cb_setZipcode_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetZipcode_Ljava_lang_String_Handler ()
		{
			if (cb_setZipcode_Ljava_lang_String_ == null)
				cb_setZipcode_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetZipcode_Ljava_lang_String_);
			return cb_setZipcode_Ljava_lang_String_;
		}

		static void n_SetZipcode_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Zipcode = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getZipcode;
		static IntPtr id_setZipcode_Ljava_lang_String_;
		public virtual unsafe string Zipcode {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='getZipcode' and count(parameter)=0]"
			[Register ("getZipcode", "()Ljava/lang/String;", "GetGetZipcodeHandler")]
			get {
				if (id_getZipcode == IntPtr.Zero)
					id_getZipcode = JNIEnv.GetMethodID (class_ref, "getZipcode", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getZipcode), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getZipcode", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlace']/method[@name='setZipcode' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setZipcode", "(Ljava/lang/String;)V", "GetSetZipcode_Ljava_lang_String_Handler")]
			set {
				if (id_setZipcode_Ljava_lang_String_ == IntPtr.Zero)
					id_setZipcode_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setZipcode", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setZipcode_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setZipcode", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
