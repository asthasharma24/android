using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Rest.Context {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.rest.context']/class[@name='ContextUserAgentBuilder']"
	[global::Android.Runtime.Register ("com/gimbal/internal/rest/context/ContextUserAgentBuilder", DoNotGenerateAcw=true)]
	public partial class ContextUserAgentBuilder : global::Com.Gimbal.Android.Util.UserAgentBuilder {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.internal.rest.context']/class[@name='ContextUserAgentBuilder']/field[@name='UNKNOWN']"
		[Register ("UNKNOWN")]
		public const string Unknown = (string) "UNKNOWN";
		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/rest/context/ContextUserAgentBuilder", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ContextUserAgentBuilder); }
		}

		protected ContextUserAgentBuilder (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static Delegate cb_getUserAgent;
#pragma warning disable 0169
		static Delegate GetGetUserAgentHandler ()
		{
			if (cb_getUserAgent == null)
				cb_getUserAgent = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUserAgent);
			return cb_getUserAgent;
		}

		static IntPtr n_GetUserAgent (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Rest.Context.ContextUserAgentBuilder __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Rest.Context.ContextUserAgentBuilder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.UserAgent);
		}
#pragma warning restore 0169

		static IntPtr id_getUserAgent;
		public virtual unsafe string UserAgent {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.rest.context']/class[@name='ContextUserAgentBuilder']/method[@name='getUserAgent' and count(parameter)=0]"
			[Register ("getUserAgent", "()Ljava/lang/String;", "GetGetUserAgentHandler")]
			get {
				if (id_getUserAgent == IntPtr.Zero)
					id_getUserAgent = JNIEnv.GetMethodID (class_ref, "getUserAgent", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUserAgent), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUserAgent", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

	}
}
