using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/BasePlace", DoNotGenerateAcw=true)]
	public partial class BasePlace : global::Java.Lang.Object, global::Com.Qsl.Faar.Protocol.IPlace {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/BasePlace", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (BasePlace); }
		}

		protected BasePlace (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/constructor[@name='BasePlace' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe BasePlace ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (BasePlace)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getGeoFenceCircle;
#pragma warning disable 0169
		static Delegate GetGetGeoFenceCircleHandler ()
		{
			if (cb_getGeoFenceCircle == null)
				cb_getGeoFenceCircle = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetGeoFenceCircle);
			return cb_getGeoFenceCircle;
		}

		static IntPtr n_GetGeoFenceCircle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GeoFenceCircle);
		}
#pragma warning restore 0169

		static Delegate cb_setGeoFenceCircle_Lcom_qsl_faar_protocol_GeoFenceCircle_;
#pragma warning disable 0169
		static Delegate GetSetGeoFenceCircle_Lcom_qsl_faar_protocol_GeoFenceCircle_Handler ()
		{
			if (cb_setGeoFenceCircle_Lcom_qsl_faar_protocol_GeoFenceCircle_ == null)
				cb_setGeoFenceCircle_Lcom_qsl_faar_protocol_GeoFenceCircle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetGeoFenceCircle_Lcom_qsl_faar_protocol_GeoFenceCircle_);
			return cb_setGeoFenceCircle_Lcom_qsl_faar_protocol_GeoFenceCircle_;
		}

		static void n_SetGeoFenceCircle_Lcom_qsl_faar_protocol_GeoFenceCircle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.GeoFenceCircle p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFenceCircle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.GeoFenceCircle = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getGeoFenceCircle;
		static IntPtr id_setGeoFenceCircle_Lcom_qsl_faar_protocol_GeoFenceCircle_;
		public virtual unsafe global::Com.Qsl.Faar.Protocol.GeoFenceCircle GeoFenceCircle {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='getGeoFenceCircle' and count(parameter)=0]"
			[Register ("getGeoFenceCircle", "()Lcom/qsl/faar/protocol/GeoFenceCircle;", "GetGetGeoFenceCircleHandler")]
			get {
				if (id_getGeoFenceCircle == IntPtr.Zero)
					id_getGeoFenceCircle = JNIEnv.GetMethodID (class_ref, "getGeoFenceCircle", "()Lcom/qsl/faar/protocol/GeoFenceCircle;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFenceCircle> (JNIEnv.CallObjectMethod  (Handle, id_getGeoFenceCircle), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFenceCircle> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getGeoFenceCircle", "()Lcom/qsl/faar/protocol/GeoFenceCircle;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='setGeoFenceCircle' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.GeoFenceCircle']]"
			[Register ("setGeoFenceCircle", "(Lcom/qsl/faar/protocol/GeoFenceCircle;)V", "GetSetGeoFenceCircle_Lcom_qsl_faar_protocol_GeoFenceCircle_Handler")]
			set {
				if (id_setGeoFenceCircle_Lcom_qsl_faar_protocol_GeoFenceCircle_ == IntPtr.Zero)
					id_setGeoFenceCircle_Lcom_qsl_faar_protocol_GeoFenceCircle_ = JNIEnv.GetMethodID (class_ref, "setGeoFenceCircle", "(Lcom/qsl/faar/protocol/GeoFenceCircle;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setGeoFenceCircle_Lcom_qsl_faar_protocol_GeoFenceCircle_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setGeoFenceCircle", "(Lcom/qsl/faar/protocol/GeoFenceCircle;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getGeoFencePolygon;
#pragma warning disable 0169
		static Delegate GetGetGeoFencePolygonHandler ()
		{
			if (cb_getGeoFencePolygon == null)
				cb_getGeoFencePolygon = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetGeoFencePolygon);
			return cb_getGeoFencePolygon;
		}

		static IntPtr n_GetGeoFencePolygon (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GeoFencePolygon);
		}
#pragma warning restore 0169

		static Delegate cb_setGeoFencePolygon_Lcom_qsl_faar_protocol_GeoFencePolygon_;
#pragma warning disable 0169
		static Delegate GetSetGeoFencePolygon_Lcom_qsl_faar_protocol_GeoFencePolygon_Handler ()
		{
			if (cb_setGeoFencePolygon_Lcom_qsl_faar_protocol_GeoFencePolygon_ == null)
				cb_setGeoFencePolygon_Lcom_qsl_faar_protocol_GeoFencePolygon_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetGeoFencePolygon_Lcom_qsl_faar_protocol_GeoFencePolygon_);
			return cb_setGeoFencePolygon_Lcom_qsl_faar_protocol_GeoFencePolygon_;
		}

		static void n_SetGeoFencePolygon_Lcom_qsl_faar_protocol_GeoFencePolygon_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.GeoFencePolygon p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.GeoFencePolygon = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getGeoFencePolygon;
		static IntPtr id_setGeoFencePolygon_Lcom_qsl_faar_protocol_GeoFencePolygon_;
		public virtual unsafe global::Com.Qsl.Faar.Protocol.GeoFencePolygon GeoFencePolygon {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='getGeoFencePolygon' and count(parameter)=0]"
			[Register ("getGeoFencePolygon", "()Lcom/qsl/faar/protocol/GeoFencePolygon;", "GetGetGeoFencePolygonHandler")]
			get {
				if (id_getGeoFencePolygon == IntPtr.Zero)
					id_getGeoFencePolygon = JNIEnv.GetMethodID (class_ref, "getGeoFencePolygon", "()Lcom/qsl/faar/protocol/GeoFencePolygon;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (JNIEnv.CallObjectMethod  (Handle, id_getGeoFencePolygon), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getGeoFencePolygon", "()Lcom/qsl/faar/protocol/GeoFencePolygon;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='setGeoFencePolygon' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.GeoFencePolygon']]"
			[Register ("setGeoFencePolygon", "(Lcom/qsl/faar/protocol/GeoFencePolygon;)V", "GetSetGeoFencePolygon_Lcom_qsl_faar_protocol_GeoFencePolygon_Handler")]
			set {
				if (id_setGeoFencePolygon_Lcom_qsl_faar_protocol_GeoFencePolygon_ == IntPtr.Zero)
					id_setGeoFencePolygon_Lcom_qsl_faar_protocol_GeoFencePolygon_ = JNIEnv.GetMethodID (class_ref, "setGeoFencePolygon", "(Lcom/qsl/faar/protocol/GeoFencePolygon;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setGeoFencePolygon_Lcom_qsl_faar_protocol_GeoFencePolygon_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setGeoFencePolygon", "(Lcom/qsl/faar/protocol/GeoFencePolygon;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_Long_Handler ()
		{
			if (cb_setId_Ljava_lang_Long_ == null)
				cb_setId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_Long_);
			return cb_setId_Ljava_lang_Long_;
		}

		static void n_SetId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/Long;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setId", "(Ljava/lang/Long;)V", "GetSetId_Ljava_lang_Long_Handler")]
			set {
				if (id_setId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getName;
#pragma warning disable 0169
		static Delegate GetGetNameHandler ()
		{
			if (cb_getName == null)
				cb_getName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetName);
			return cb_getName;
		}

		static IntPtr n_GetName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Name);
		}
#pragma warning restore 0169

		static Delegate cb_setName_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetName_Ljava_lang_String_Handler ()
		{
			if (cb_setName_Ljava_lang_String_ == null)
				cb_setName_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetName_Ljava_lang_String_);
			return cb_setName_Ljava_lang_String_;
		}

		static void n_SetName_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Name = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getName;
		static IntPtr id_setName_Ljava_lang_String_;
		public virtual unsafe string Name {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='getName' and count(parameter)=0]"
			[Register ("getName", "()Ljava/lang/String;", "GetGetNameHandler")]
			get {
				if (id_getName == IntPtr.Zero)
					id_getName = JNIEnv.GetMethodID (class_ref, "getName", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getName), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getName", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='setName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setName", "(Ljava/lang/String;)V", "GetSetName_Ljava_lang_String_Handler")]
			set {
				if (id_setName_Ljava_lang_String_ == IntPtr.Zero)
					id_setName_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setName", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setName_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setName", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getOrganizationId;
#pragma warning disable 0169
		static Delegate GetGetOrganizationIdHandler ()
		{
			if (cb_getOrganizationId == null)
				cb_getOrganizationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationId);
			return cb_getOrganizationId;
		}

		static IntPtr n_GetOrganizationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OrganizationId);
		}
#pragma warning restore 0169

		static Delegate cb_setOrganizationId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationId_Ljava_lang_Long_Handler ()
		{
			if (cb_setOrganizationId_Ljava_lang_Long_ == null)
				cb_setOrganizationId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationId_Ljava_lang_Long_);
			return cb_setOrganizationId_Ljava_lang_Long_;
		}

		static void n_SetOrganizationId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OrganizationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationId;
		static IntPtr id_setOrganizationId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long OrganizationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='getOrganizationId' and count(parameter)=0]"
			[Register ("getOrganizationId", "()Ljava/lang/Long;", "GetGetOrganizationIdHandler")]
			get {
				if (id_getOrganizationId == IntPtr.Zero)
					id_getOrganizationId = JNIEnv.GetMethodID (class_ref, "getOrganizationId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='setOrganizationId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setOrganizationId", "(Ljava/lang/Long;)V", "GetSetOrganizationId_Ljava_lang_Long_Handler")]
			set {
				if (id_setOrganizationId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setOrganizationId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setOrganizationId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOrganizationId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPlaceAttributes;
#pragma warning disable 0169
		static Delegate GetGetPlaceAttributesHandler ()
		{
			if (cb_getPlaceAttributes == null)
				cb_getPlaceAttributes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlaceAttributes);
			return cb_getPlaceAttributes;
		}

		static IntPtr n_GetPlaceAttributes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.PlaceAttribute>.ToLocalJniHandle (__this.PlaceAttributes);
		}
#pragma warning restore 0169

		static Delegate cb_setPlaceAttributes_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetPlaceAttributes_Ljava_util_List_Handler ()
		{
			if (cb_setPlaceAttributes_Ljava_util_List_ == null)
				cb_setPlaceAttributes_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPlaceAttributes_Ljava_util_List_);
			return cb_setPlaceAttributes_Ljava_util_List_;
		}

		static void n_SetPlaceAttributes_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.PlaceAttribute>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.PlaceAttributes = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPlaceAttributes;
		static IntPtr id_setPlaceAttributes_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.PlaceAttribute> PlaceAttributes {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='getPlaceAttributes' and count(parameter)=0]"
			[Register ("getPlaceAttributes", "()Ljava/util/List;", "GetGetPlaceAttributesHandler")]
			get {
				if (id_getPlaceAttributes == IntPtr.Zero)
					id_getPlaceAttributes = JNIEnv.GetMethodID (class_ref, "getPlaceAttributes", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.PlaceAttribute>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getPlaceAttributes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.PlaceAttribute>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlaceAttributes", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='setPlaceAttributes' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.qsl.faar.protocol.PlaceAttribute&gt;']]"
			[Register ("setPlaceAttributes", "(Ljava/util/List;)V", "GetSetPlaceAttributes_Ljava_util_List_Handler")]
			set {
				if (id_setPlaceAttributes_Ljava_util_List_ == IntPtr.Zero)
					id_setPlaceAttributes_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setPlaceAttributes", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.PlaceAttribute>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPlaceAttributes_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPlaceAttributes", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		static Delegate cb_setUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setUuid_Ljava_lang_String_ == null)
				cb_setUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUuid_Ljava_lang_String_);
			return cb_setUuid_Ljava_lang_String_;
		}

		static void n_SetUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.BasePlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.BasePlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Uuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUuid;
		static IntPtr id_setUuid_Ljava_lang_String_;
		public virtual unsafe string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler")]
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='BasePlace']/method[@name='setUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUuid", "(Ljava/lang/String;)V", "GetSetUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
