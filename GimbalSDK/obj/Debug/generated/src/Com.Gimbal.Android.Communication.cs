using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='Communication']"
	[global::Android.Runtime.Register ("com/gimbal/android/Communication", DoNotGenerateAcw=true)]
	public partial class Communication : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/Communication", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Communication); }
		}

		protected Communication (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android']/class[@name='Communication']/constructor[@name='Communication' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Communication ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Communication)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAttributes;
#pragma warning disable 0169
		static Delegate GetGetAttributesHandler ()
		{
			if (cb_getAttributes == null)
				cb_getAttributes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAttributes);
			return cb_getAttributes;
		}

		static IntPtr n_GetAttributes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Communication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Communication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Attributes);
		}
#pragma warning restore 0169

		static IntPtr id_getAttributes;
		public virtual unsafe global::Com.Gimbal.Android.IAttributes Attributes {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Communication']/method[@name='getAttributes' and count(parameter)=0]"
			[Register ("getAttributes", "()Lcom/gimbal/android/Attributes;", "GetGetAttributesHandler")]
			get {
				if (id_getAttributes == IntPtr.Zero)
					id_getAttributes = JNIEnv.GetMethodID (class_ref, "getAttributes", "()Lcom/gimbal/android/Attributes;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.IAttributes> (JNIEnv.CallObjectMethod  (Handle, id_getAttributes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.IAttributes> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAttributes", "()Lcom/gimbal/android/Attributes;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getDeliveryDate;
#pragma warning disable 0169
		static Delegate GetGetDeliveryDateHandler ()
		{
			if (cb_getDeliveryDate == null)
				cb_getDeliveryDate = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetDeliveryDate);
			return cb_getDeliveryDate;
		}

		static long n_GetDeliveryDate (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Communication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Communication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DeliveryDate;
		}
#pragma warning restore 0169

		static IntPtr id_getDeliveryDate;
		public virtual unsafe long DeliveryDate {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Communication']/method[@name='getDeliveryDate' and count(parameter)=0]"
			[Register ("getDeliveryDate", "()J", "GetGetDeliveryDateHandler")]
			get {
				if (id_getDeliveryDate == IntPtr.Zero)
					id_getDeliveryDate = JNIEnv.GetMethodID (class_ref, "getDeliveryDate", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getDeliveryDate);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDeliveryDate", "()J"));
				} finally {
				}
			}
		}

		static Delegate cb_getDescription;
#pragma warning disable 0169
		static Delegate GetGetDescriptionHandler ()
		{
			if (cb_getDescription == null)
				cb_getDescription = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDescription);
			return cb_getDescription;
		}

		static IntPtr n_GetDescription (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Communication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Communication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Description);
		}
#pragma warning restore 0169

		static IntPtr id_getDescription;
		public virtual unsafe string Description {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Communication']/method[@name='getDescription' and count(parameter)=0]"
			[Register ("getDescription", "()Ljava/lang/String;", "GetGetDescriptionHandler")]
			get {
				if (id_getDescription == IntPtr.Zero)
					id_getDescription = JNIEnv.GetMethodID (class_ref, "getDescription", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getDescription), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDescription", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getExpiryTimeInMillis;
#pragma warning disable 0169
		static Delegate GetGetExpiryTimeInMillisHandler ()
		{
			if (cb_getExpiryTimeInMillis == null)
				cb_getExpiryTimeInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetExpiryTimeInMillis);
			return cb_getExpiryTimeInMillis;
		}

		static long n_GetExpiryTimeInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Communication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Communication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ExpiryTimeInMillis;
		}
#pragma warning restore 0169

		static IntPtr id_getExpiryTimeInMillis;
		public virtual unsafe long ExpiryTimeInMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Communication']/method[@name='getExpiryTimeInMillis' and count(parameter)=0]"
			[Register ("getExpiryTimeInMillis", "()J", "GetGetExpiryTimeInMillisHandler")]
			get {
				if (id_getExpiryTimeInMillis == IntPtr.Zero)
					id_getExpiryTimeInMillis = JNIEnv.GetMethodID (class_ref, "getExpiryTimeInMillis", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getExpiryTimeInMillis);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getExpiryTimeInMillis", "()J"));
				} finally {
				}
			}
		}

		static Delegate cb_getIdentifier;
#pragma warning disable 0169
		static Delegate GetGetIdentifierHandler ()
		{
			if (cb_getIdentifier == null)
				cb_getIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIdentifier);
			return cb_getIdentifier;
		}

		static IntPtr n_GetIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Communication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Communication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Identifier);
		}
#pragma warning restore 0169

		static IntPtr id_getIdentifier;
		public virtual unsafe string Identifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Communication']/method[@name='getIdentifier' and count(parameter)=0]"
			[Register ("getIdentifier", "()Ljava/lang/String;", "GetGetIdentifierHandler")]
			get {
				if (id_getIdentifier == IntPtr.Zero)
					id_getIdentifier = JNIEnv.GetMethodID (class_ref, "getIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getTitle;
#pragma warning disable 0169
		static Delegate GetGetTitleHandler ()
		{
			if (cb_getTitle == null)
				cb_getTitle = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTitle);
			return cb_getTitle;
		}

		static IntPtr n_GetTitle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Communication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Communication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Title);
		}
#pragma warning restore 0169

		static IntPtr id_getTitle;
		public virtual unsafe string Title {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Communication']/method[@name='getTitle' and count(parameter)=0]"
			[Register ("getTitle", "()Ljava/lang/String;", "GetGetTitleHandler")]
			get {
				if (id_getTitle == IntPtr.Zero)
					id_getTitle = JNIEnv.GetMethodID (class_ref, "getTitle", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getTitle), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTitle", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getURL;
#pragma warning disable 0169
		static Delegate GetGetURLHandler ()
		{
			if (cb_getURL == null)
				cb_getURL = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetURL);
			return cb_getURL;
		}

		static IntPtr n_GetURL (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Communication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Communication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.URL);
		}
#pragma warning restore 0169

		static IntPtr id_getURL;
		public virtual unsafe string URL {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Communication']/method[@name='getURL' and count(parameter)=0]"
			[Register ("getURL", "()Ljava/lang/String;", "GetGetURLHandler")]
			get {
				if (id_getURL == IntPtr.Zero)
					id_getURL = JNIEnv.GetMethodID (class_ref, "getURL", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getURL), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getURL", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

	}
}
