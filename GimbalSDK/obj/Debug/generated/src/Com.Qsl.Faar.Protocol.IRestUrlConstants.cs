using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	[Register ("com/qsl/faar/protocol/RestUrlConstants")]
	public abstract class RestUrlConstants {

		internal RestUrlConstants ()
		{
		}

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='ADDRESS_URL']"
		[Register ("ADDRESS_URL")]
		public const string AddressUrl = (string) "address";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='ANALYTICS']"
		[Register ("ANALYTICS")]
		public const string Analytics = (string) "analytics";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='ANDROID']"
		[Register ("ANDROID")]
		public const string Android = (string) "android";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='APPLICATION']"
		[Register ("APPLICATION")]
		public const string Application = (string) "application";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='ATTRIBUTES']"
		[Register ("ATTRIBUTES")]
		public const string Attributes = (string) "attributes";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='BUBBLE']"
		[Register ("BUBBLE")]
		public const string Bubble = (string) "bubble";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='CLIENT_LOG_FILE']"
		[Register ("CLIENT_LOG_FILE")]
		public const string ClientLogFile = (string) "clientlogfile";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='CONTENT']"
		[Register ("CONTENT")]
		public const string Content = (string) "content";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='CONTENT_ATTRIBUTES']"
		[Register ("CONTENT_ATTRIBUTES")]
		public const string ContentAttributes = (string) "contentattributes";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='CURRENT_PLACE_STATE']"
		[Register ("CURRENT_PLACE_STATE")]
		public const string CurrentPlaceState = (string) "currentplacestate";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='ESTABLISHED_LOCATIONS']"
		[Register ("ESTABLISHED_LOCATIONS")]
		public const string EstablishedLocations = (string) "est-loc";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='EVENTS']"
		[Register ("EVENTS")]
		public const string Events = (string) "events";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='FILTER']"
		[Register ("FILTER")]
		public const string Filter = (string) "filter";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='GEOCODE']"
		[Register ("GEOCODE")]
		public const string Geocode = (string) "geocode";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='HTTP_AUTHORIZATION']"
		[Register ("HTTP_AUTHORIZATION")]
		public const string HttpAuthorization = (string) "AUTHORIZATION";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='HTTP_PLATFORM_HEADER']"
		[Register ("HTTP_PLATFORM_HEADER")]
		public const string HttpPlatformHeader = (string) "X-Client-Platform";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='HTTP_TIMEZONE_HEADER']"
		[Register ("HTTP_TIMEZONE_HEADER")]
		public const string HttpTimezoneHeader = (string) "X-Client-Timezone";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='HTTP_USER_AGENT_HEADER']"
		[Register ("HTTP_USER_AGENT_HEADER")]
		public const string HttpUserAgentHeader = (string) "User-Agent";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='INSTANT_PUSH_DETAIL']"
		[Register ("INSTANT_PUSH_DETAIL")]
		public const string InstantPushDetail = (string) "instantPushDetail";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='ORGANIZATION']"
		[Register ("ORGANIZATION")]
		public const string Organization = (string) "organization";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='OVERLAY']"
		[Register ("OVERLAY")]
		public const string Overlay = (string) "overlay";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='PERMISSION']"
		[Register ("PERMISSION")]
		public const string Permission = (string) "permission";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='PLACE']"
		[Register ("PLACE")]
		public const string Place = (string) "place";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='PLACE_EVENTS']"
		[Register ("PLACE_EVENTS")]
		public const string PlaceEvents = (string) "placeEvents";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='PLACE_EVENT_URL']"
		[Register ("PLACE_EVENT_URL")]
		public const string PlaceEventUrl = (string) "placeevent";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='PLATFORM']"
		[Register ("PLATFORM")]
		public const string Platform = (string) "platform";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='PPOI']"
		[Register ("PPOI")]
		public const string Ppoi = (string) "ppoi";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='PROFILE']"
		[Register ("PROFILE")]
		public const string Profile = (string) "profile";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='PROFILE_DEF']"
		[Register ("PROFILE_DEF")]
		public const string ProfileDef = (string) "profiledef";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='PUSH_CREDENTIALS']"
		[Register ("PUSH_CREDENTIALS")]
		public const string PushCredentials = (string) "pushcredentials";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='PUSH_DETAIL']"
		[Register ("PUSH_DETAIL")]
		public const string PushDetail = (string) "pushdetail";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='PUSH_LIMITS']"
		[Register ("PUSH_LIMITS")]
		public const string PushLimits = (string) "pushlimits";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='REGISTRATION']"
		[Register ("REGISTRATION")]
		public const string Registration = (string) "registration";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='RULES']"
		[Register ("RULES")]
		public const string Rules = (string) "rules";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='SEARCH']"
		[Register ("SEARCH")]
		public const string Search = (string) "search";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='SEPARATOR']"
		[Register ("SEPARATOR")]
		public const string Separator = (string) "/";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='TARGET_ACQUIRED']"
		[Register ("TARGET_ACQUIRED")]
		public const string TargetAcquired = (string) "targetacquired";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='TIMEZONE']"
		[Register ("TIMEZONE")]
		public const string Timezone = (string) "timezone";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='TIME_EVENT']"
		[Register ("TIME_EVENT")]
		public const string TimeEvent = (string) "timeevent";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='TIME_TRIGGERS']"
		[Register ("TIME_TRIGGERS")]
		public const string TimeTriggers = (string) "timetriggers";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='RestUrlConstants']/field[@name='USER']"
		[Register ("USER")]
		public const string User = (string) "user";
	}

	[global::System.Obsolete ("Use the 'RestUrlConstants' type. This type will be removed in a future release.")]
	public abstract class RestUrlConstantsConsts : RestUrlConstants {

		private RestUrlConstantsConsts ()
		{
		}
	}

}
