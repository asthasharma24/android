using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Point']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/Point", DoNotGenerateAcw=true)]
	public partial class Point : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/Point", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Point); }
		}

		protected Point (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Point']/constructor[@name='Point' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Point ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Point)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getLatitude;
#pragma warning disable 0169
		static Delegate GetGetLatitudeHandler ()
		{
			if (cb_getLatitude == null)
				cb_getLatitude = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLatitude);
			return cb_getLatitude;
		}

		static IntPtr n_GetLatitude (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Point __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Point> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Latitude);
		}
#pragma warning restore 0169

		static Delegate cb_setLatitude_Ljava_lang_Double_;
#pragma warning disable 0169
		static Delegate GetSetLatitude_Ljava_lang_Double_Handler ()
		{
			if (cb_setLatitude_Ljava_lang_Double_ == null)
				cb_setLatitude_Ljava_lang_Double_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLatitude_Ljava_lang_Double_);
			return cb_setLatitude_Ljava_lang_Double_;
		}

		static void n_SetLatitude_Ljava_lang_Double_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Point __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Point> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Double p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Latitude = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLatitude;
		static IntPtr id_setLatitude_Ljava_lang_Double_;
		public virtual unsafe global::Java.Lang.Double Latitude {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Point']/method[@name='getLatitude' and count(parameter)=0]"
			[Register ("getLatitude", "()Ljava/lang/Double;", "GetGetLatitudeHandler")]
			get {
				if (id_getLatitude == IntPtr.Zero)
					id_getLatitude = JNIEnv.GetMethodID (class_ref, "getLatitude", "()Ljava/lang/Double;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (JNIEnv.CallObjectMethod  (Handle, id_getLatitude), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLatitude", "()Ljava/lang/Double;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Point']/method[@name='setLatitude' and count(parameter)=1 and parameter[1][@type='java.lang.Double']]"
			[Register ("setLatitude", "(Ljava/lang/Double;)V", "GetSetLatitude_Ljava_lang_Double_Handler")]
			set {
				if (id_setLatitude_Ljava_lang_Double_ == IntPtr.Zero)
					id_setLatitude_Ljava_lang_Double_ = JNIEnv.GetMethodID (class_ref, "setLatitude", "(Ljava/lang/Double;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLatitude_Ljava_lang_Double_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLatitude", "(Ljava/lang/Double;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getLongitude;
#pragma warning disable 0169
		static Delegate GetGetLongitudeHandler ()
		{
			if (cb_getLongitude == null)
				cb_getLongitude = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLongitude);
			return cb_getLongitude;
		}

		static IntPtr n_GetLongitude (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Point __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Point> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Longitude);
		}
#pragma warning restore 0169

		static Delegate cb_setLongitude_Ljava_lang_Double_;
#pragma warning disable 0169
		static Delegate GetSetLongitude_Ljava_lang_Double_Handler ()
		{
			if (cb_setLongitude_Ljava_lang_Double_ == null)
				cb_setLongitude_Ljava_lang_Double_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLongitude_Ljava_lang_Double_);
			return cb_setLongitude_Ljava_lang_Double_;
		}

		static void n_SetLongitude_Ljava_lang_Double_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Point __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Point> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Double p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Longitude = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLongitude;
		static IntPtr id_setLongitude_Ljava_lang_Double_;
		public virtual unsafe global::Java.Lang.Double Longitude {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Point']/method[@name='getLongitude' and count(parameter)=0]"
			[Register ("getLongitude", "()Ljava/lang/Double;", "GetGetLongitudeHandler")]
			get {
				if (id_getLongitude == IntPtr.Zero)
					id_getLongitude = JNIEnv.GetMethodID (class_ref, "getLongitude", "()Ljava/lang/Double;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (JNIEnv.CallObjectMethod  (Handle, id_getLongitude), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLongitude", "()Ljava/lang/Double;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Point']/method[@name='setLongitude' and count(parameter)=1 and parameter[1][@type='java.lang.Double']]"
			[Register ("setLongitude", "(Ljava/lang/Double;)V", "GetSetLongitude_Ljava_lang_Double_Handler")]
			set {
				if (id_setLongitude_Ljava_lang_Double_ == IntPtr.Zero)
					id_setLongitude_Ljava_lang_Double_ = JNIEnv.GetMethodID (class_ref, "setLongitude", "(Ljava/lang/Double;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLongitude_Ljava_lang_Double_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLongitude", "(Ljava/lang/Double;)V"), __args);
				} finally {
				}
			}
		}

	}
}
