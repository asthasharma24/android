using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='AdvertisingIdentifierInfo']"
	[global::Android.Runtime.Register ("com/gimbal/internal/protocol/AdvertisingIdentifierInfo", DoNotGenerateAcw=true)]
	public partial class AdvertisingIdentifierInfo : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/protocol/AdvertisingIdentifierInfo", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (AdvertisingIdentifierInfo); }
		}

		protected AdvertisingIdentifierInfo (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='AdvertisingIdentifierInfo']/constructor[@name='AdvertisingIdentifierInfo' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe AdvertisingIdentifierInfo ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (AdvertisingIdentifierInfo)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAdvertisingIdentifier;
#pragma warning disable 0169
		static Delegate GetGetAdvertisingIdentifierHandler ()
		{
			if (cb_getAdvertisingIdentifier == null)
				cb_getAdvertisingIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAdvertisingIdentifier);
			return cb_getAdvertisingIdentifier;
		}

		static IntPtr n_GetAdvertisingIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.AdvertisingIdentifierInfo __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.AdvertisingIdentifierInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.AdvertisingIdentifier);
		}
#pragma warning restore 0169

		static Delegate cb_setAdvertisingIdentifier_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetAdvertisingIdentifier_Ljava_lang_String_Handler ()
		{
			if (cb_setAdvertisingIdentifier_Ljava_lang_String_ == null)
				cb_setAdvertisingIdentifier_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAdvertisingIdentifier_Ljava_lang_String_);
			return cb_setAdvertisingIdentifier_Ljava_lang_String_;
		}

		static void n_SetAdvertisingIdentifier_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.AdvertisingIdentifierInfo __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.AdvertisingIdentifierInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AdvertisingIdentifier = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getAdvertisingIdentifier;
		static IntPtr id_setAdvertisingIdentifier_Ljava_lang_String_;
		public virtual unsafe string AdvertisingIdentifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='AdvertisingIdentifierInfo']/method[@name='getAdvertisingIdentifier' and count(parameter)=0]"
			[Register ("getAdvertisingIdentifier", "()Ljava/lang/String;", "GetGetAdvertisingIdentifierHandler")]
			get {
				if (id_getAdvertisingIdentifier == IntPtr.Zero)
					id_getAdvertisingIdentifier = JNIEnv.GetMethodID (class_ref, "getAdvertisingIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getAdvertisingIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAdvertisingIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='AdvertisingIdentifierInfo']/method[@name='setAdvertisingIdentifier' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setAdvertisingIdentifier", "(Ljava/lang/String;)V", "GetSetAdvertisingIdentifier_Ljava_lang_String_Handler")]
			set {
				if (id_setAdvertisingIdentifier_Ljava_lang_String_ == IntPtr.Zero)
					id_setAdvertisingIdentifier_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setAdvertisingIdentifier", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setAdvertisingIdentifier_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAdvertisingIdentifier", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_isAdvertisingTrackingEnabled;
#pragma warning disable 0169
		static Delegate GetIsAdvertisingTrackingEnabledHandler ()
		{
			if (cb_isAdvertisingTrackingEnabled == null)
				cb_isAdvertisingTrackingEnabled = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsAdvertisingTrackingEnabled);
			return cb_isAdvertisingTrackingEnabled;
		}

		static bool n_IsAdvertisingTrackingEnabled (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.AdvertisingIdentifierInfo __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.AdvertisingIdentifierInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.AdvertisingTrackingEnabled;
		}
#pragma warning restore 0169

		static Delegate cb_setAdvertisingTrackingEnabled_Z;
#pragma warning disable 0169
		static Delegate GetSetAdvertisingTrackingEnabled_ZHandler ()
		{
			if (cb_setAdvertisingTrackingEnabled_Z == null)
				cb_setAdvertisingTrackingEnabled_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetAdvertisingTrackingEnabled_Z);
			return cb_setAdvertisingTrackingEnabled_Z;
		}

		static void n_SetAdvertisingTrackingEnabled_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Gimbal.Internal.Protocol.AdvertisingIdentifierInfo __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.AdvertisingIdentifierInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.AdvertisingTrackingEnabled = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isAdvertisingTrackingEnabled;
		static IntPtr id_setAdvertisingTrackingEnabled_Z;
		public virtual unsafe bool AdvertisingTrackingEnabled {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='AdvertisingIdentifierInfo']/method[@name='isAdvertisingTrackingEnabled' and count(parameter)=0]"
			[Register ("isAdvertisingTrackingEnabled", "()Z", "GetIsAdvertisingTrackingEnabledHandler")]
			get {
				if (id_isAdvertisingTrackingEnabled == IntPtr.Zero)
					id_isAdvertisingTrackingEnabled = JNIEnv.GetMethodID (class_ref, "isAdvertisingTrackingEnabled", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isAdvertisingTrackingEnabled);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isAdvertisingTrackingEnabled", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='AdvertisingIdentifierInfo']/method[@name='setAdvertisingTrackingEnabled' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setAdvertisingTrackingEnabled", "(Z)V", "GetSetAdvertisingTrackingEnabled_ZHandler")]
			set {
				if (id_setAdvertisingTrackingEnabled_Z == IntPtr.Zero)
					id_setAdvertisingTrackingEnabled_Z = JNIEnv.GetMethodID (class_ref, "setAdvertisingTrackingEnabled", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setAdvertisingTrackingEnabled_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAdvertisingTrackingEnabled", "(Z)V"), __args);
				} finally {
				}
			}
		}

	}
}
