using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Communication.Services {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.communication.services']/class[@name='ScheduledCommunication']"
	[global::Android.Runtime.Register ("com/gimbal/internal/communication/services/ScheduledCommunication", DoNotGenerateAcw=true)]
	public partial class ScheduledCommunication : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/communication/services/ScheduledCommunication", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ScheduledCommunication); }
		}

		protected ScheduledCommunication (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Ljava_lang_String_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.communication.services']/class[@name='ScheduledCommunication']/constructor[@name='ScheduledCommunication' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register (".ctor", "(Ljava/lang/String;)V", "")]
		public unsafe ScheduledCommunication (string p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				if (GetType () != typeof (ScheduledCommunication)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Ljava/lang/String;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Ljava/lang/String;)V", __args);
					return;
				}

				if (id_ctor_Ljava_lang_String_ == IntPtr.Zero)
					id_ctor_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Ljava/lang/String;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Ljava_lang_String_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Ljava_lang_String_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.communication.services']/class[@name='ScheduledCommunication']/constructor[@name='ScheduledCommunication' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ScheduledCommunication ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ScheduledCommunication)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getCommunicationId;
#pragma warning disable 0169
		static Delegate GetGetCommunicationIdHandler ()
		{
			if (cb_getCommunicationId == null)
				cb_getCommunicationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCommunicationId);
			return cb_getCommunicationId;
		}

		static IntPtr n_GetCommunicationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.CommunicationId);
		}
#pragma warning restore 0169

		static Delegate cb_setCommunicationId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetCommunicationId_Ljava_lang_String_Handler ()
		{
			if (cb_setCommunicationId_Ljava_lang_String_ == null)
				cb_setCommunicationId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCommunicationId_Ljava_lang_String_);
			return cb_setCommunicationId_Ljava_lang_String_;
		}

		static void n_SetCommunicationId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.CommunicationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getCommunicationId;
		static IntPtr id_setCommunicationId_Ljava_lang_String_;
		public virtual unsafe string CommunicationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication.services']/class[@name='ScheduledCommunication']/method[@name='getCommunicationId' and count(parameter)=0]"
			[Register ("getCommunicationId", "()Ljava/lang/String;", "GetGetCommunicationIdHandler")]
			get {
				if (id_getCommunicationId == IntPtr.Zero)
					id_getCommunicationId = JNIEnv.GetMethodID (class_ref, "getCommunicationId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getCommunicationId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCommunicationId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication.services']/class[@name='ScheduledCommunication']/method[@name='setCommunicationId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setCommunicationId", "(Ljava/lang/String;)V", "GetSetCommunicationId_Ljava_lang_String_Handler")]
			set {
				if (id_setCommunicationId_Ljava_lang_String_ == IntPtr.Zero)
					id_setCommunicationId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setCommunicationId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCommunicationId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCommunicationId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_String_Handler ()
		{
			if (cb_setId_Ljava_lang_String_ == null)
				cb_setId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_String_);
			return cb_setId_Ljava_lang_String_;
		}

		static void n_SetId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_String_;
		public virtual unsafe string Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication.services']/class[@name='ScheduledCommunication']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/String;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication.services']/class[@name='ScheduledCommunication']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setId", "(Ljava/lang/String;)V", "GetSetId_Ljava_lang_String_Handler")]
			set {
				if (id_setId_Ljava_lang_String_ == IntPtr.Zero)
					id_setId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getPlaceEvent;
#pragma warning disable 0169
		static Delegate GetGetPlaceEventHandler ()
		{
			if (cb_getPlaceEvent == null)
				cb_getPlaceEvent = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlaceEvent);
			return cb_getPlaceEvent;
		}

		static IntPtr n_GetPlaceEvent (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.PlaceEvent);
		}
#pragma warning restore 0169

		static Delegate cb_setPlaceEvent_Lcom_gimbal_internal_location_services_InternalPlaceEvent_;
#pragma warning disable 0169
		static Delegate GetSetPlaceEvent_Lcom_gimbal_internal_location_services_InternalPlaceEvent_Handler ()
		{
			if (cb_setPlaceEvent_Lcom_gimbal_internal_location_services_InternalPlaceEvent_ == null)
				cb_setPlaceEvent_Lcom_gimbal_internal_location_services_InternalPlaceEvent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPlaceEvent_Lcom_gimbal_internal_location_services_InternalPlaceEvent_);
			return cb_setPlaceEvent_Lcom_gimbal_internal_location_services_InternalPlaceEvent_;
		}

		static void n_SetPlaceEvent_Lcom_gimbal_internal_location_services_InternalPlaceEvent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.PlaceEvent = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPlaceEvent;
		static IntPtr id_setPlaceEvent_Lcom_gimbal_internal_location_services_InternalPlaceEvent_;
		public virtual unsafe global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent PlaceEvent {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication.services']/class[@name='ScheduledCommunication']/method[@name='getPlaceEvent' and count(parameter)=0]"
			[Register ("getPlaceEvent", "()Lcom/gimbal/internal/location/services/InternalPlaceEvent;", "GetGetPlaceEventHandler")]
			get {
				if (id_getPlaceEvent == IntPtr.Zero)
					id_getPlaceEvent = JNIEnv.GetMethodID (class_ref, "getPlaceEvent", "()Lcom/gimbal/internal/location/services/InternalPlaceEvent;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (JNIEnv.CallObjectMethod  (Handle, id_getPlaceEvent), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlaceEvent", "()Lcom/gimbal/internal/location/services/InternalPlaceEvent;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication.services']/class[@name='ScheduledCommunication']/method[@name='setPlaceEvent' and count(parameter)=1 and parameter[1][@type='com.gimbal.internal.location.services.InternalPlaceEvent']]"
			[Register ("setPlaceEvent", "(Lcom/gimbal/internal/location/services/InternalPlaceEvent;)V", "GetSetPlaceEvent_Lcom_gimbal_internal_location_services_InternalPlaceEvent_Handler")]
			set {
				if (id_setPlaceEvent_Lcom_gimbal_internal_location_services_InternalPlaceEvent_ == IntPtr.Zero)
					id_setPlaceEvent_Lcom_gimbal_internal_location_services_InternalPlaceEvent_ = JNIEnv.GetMethodID (class_ref, "setPlaceEvent", "(Lcom/gimbal/internal/location/services/InternalPlaceEvent;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPlaceEvent_Lcom_gimbal_internal_location_services_InternalPlaceEvent_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPlaceEvent", "(Lcom/gimbal/internal/location/services/InternalPlaceEvent;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getScheduledTime;
#pragma warning disable 0169
		static Delegate GetGetScheduledTimeHandler ()
		{
			if (cb_getScheduledTime == null)
				cb_getScheduledTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetScheduledTime);
			return cb_getScheduledTime;
		}

		static long n_GetScheduledTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ScheduledTime;
		}
#pragma warning restore 0169

		static Delegate cb_setScheduledTime_J;
#pragma warning disable 0169
		static Delegate GetSetScheduledTime_JHandler ()
		{
			if (cb_setScheduledTime_J == null)
				cb_setScheduledTime_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetScheduledTime_J);
			return cb_setScheduledTime_J;
		}

		static void n_SetScheduledTime_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.Services.ScheduledCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ScheduledTime = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getScheduledTime;
		static IntPtr id_setScheduledTime_J;
		public virtual unsafe long ScheduledTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication.services']/class[@name='ScheduledCommunication']/method[@name='getScheduledTime' and count(parameter)=0]"
			[Register ("getScheduledTime", "()J", "GetGetScheduledTimeHandler")]
			get {
				if (id_getScheduledTime == IntPtr.Zero)
					id_getScheduledTime = JNIEnv.GetMethodID (class_ref, "getScheduledTime", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getScheduledTime);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getScheduledTime", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication.services']/class[@name='ScheduledCommunication']/method[@name='setScheduledTime' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setScheduledTime", "(J)V", "GetSetScheduledTime_JHandler")]
			set {
				if (id_setScheduledTime_J == IntPtr.Zero)
					id_setScheduledTime_J = JNIEnv.GetMethodID (class_ref, "setScheduledTime", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setScheduledTime_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setScheduledTime", "(J)V"), __args);
				} finally {
				}
			}
		}

	}
}
