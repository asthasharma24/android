using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='Push']"
	[global::Android.Runtime.Register ("com/gimbal/android/Push", DoNotGenerateAcw=true)]
	public partial class Push : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='Push.PushType']"
		[global::Android.Runtime.Register ("com/gimbal/android/Push$PushType", DoNotGenerateAcw=true)]
		public sealed partial class PushType : global::Java.Lang.Enum {


			static IntPtr INSTANT_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android']/class[@name='Push.PushType']/field[@name='INSTANT']"
			[Register ("INSTANT")]
			public static global::Com.Gimbal.Android.Push.PushType Instant {
				get {
					if (INSTANT_jfieldId == IntPtr.Zero)
						INSTANT_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "INSTANT", "Lcom/gimbal/android/Push$PushType;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, INSTANT_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Push.PushType> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr TIME_TRIGGERED_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android']/class[@name='Push.PushType']/field[@name='TIME_TRIGGERED']"
			[Register ("TIME_TRIGGERED")]
			public static global::Com.Gimbal.Android.Push.PushType TimeTriggered {
				get {
					if (TIME_TRIGGERED_jfieldId == IntPtr.Zero)
						TIME_TRIGGERED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "TIME_TRIGGERED", "Lcom/gimbal/android/Push$PushType;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, TIME_TRIGGERED_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Push.PushType> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/gimbal/android/Push$PushType", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (PushType); }
			}

			internal PushType (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Push.PushType']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/gimbal/android/Push$PushType;", "")]
			public static unsafe global::Com.Gimbal.Android.Push.PushType ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/gimbal/android/Push$PushType;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Gimbal.Android.Push.PushType __ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Push.PushType> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Push.PushType']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/gimbal/android/Push$PushType;", "")]
			public static unsafe global::Com.Gimbal.Android.Push.PushType[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/gimbal/android/Push$PushType;");
				try {
					return (global::Com.Gimbal.Android.Push.PushType[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Gimbal.Android.Push.PushType));
				} finally {
				}
			}

		}

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/Push", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Push); }
		}

		protected Push (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android']/class[@name='Push']/constructor[@name='Push' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Push ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Push)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getPushType;
#pragma warning disable 0169
		static Delegate GetGetPushTypeHandler ()
		{
			if (cb_getPushType == null)
				cb_getPushType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPushType);
			return cb_getPushType;
		}

		static IntPtr n_GetPushType (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Push __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Push> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetPushType ());
		}
#pragma warning restore 0169

		static IntPtr id_getPushType;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Push']/method[@name='getPushType' and count(parameter)=0]"
		[Register ("getPushType", "()Lcom/gimbal/android/Push$PushType;", "GetGetPushTypeHandler")]
		public virtual unsafe global::Com.Gimbal.Android.Push.PushType GetPushType ()
		{
			if (id_getPushType == IntPtr.Zero)
				id_getPushType = JNIEnv.GetMethodID (class_ref, "getPushType", "()Lcom/gimbal/android/Push$PushType;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Push.PushType> (JNIEnv.CallObjectMethod  (Handle, id_getPushType), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Push.PushType> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPushType", "()Lcom/gimbal/android/Push$PushType;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

	}
}
