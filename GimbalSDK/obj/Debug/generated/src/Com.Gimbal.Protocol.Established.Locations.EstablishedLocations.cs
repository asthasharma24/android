using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol.Established.Locations {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocations']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/established/locations/EstablishedLocations", DoNotGenerateAcw=true)]
	public partial class EstablishedLocations : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/established/locations/EstablishedLocations", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (EstablishedLocations); }
		}

		protected EstablishedLocations (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocations']/constructor[@name='EstablishedLocations' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe EstablishedLocations ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (EstablishedLocations)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getLocations;
#pragma warning disable 0169
		static Delegate GetGetLocationsHandler ()
		{
			if (cb_getLocations == null)
				cb_getLocations = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLocations);
			return cb_getLocations;
		}

		static IntPtr n_GetLocations (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocations __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocations> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation>.ToLocalJniHandle (__this.Locations);
		}
#pragma warning restore 0169

		static Delegate cb_setLocations_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetLocations_Ljava_util_List_Handler ()
		{
			if (cb_setLocations_Ljava_util_List_ == null)
				cb_setLocations_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLocations_Ljava_util_List_);
			return cb_setLocations_Ljava_util_List_;
		}

		static void n_SetLocations_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocations __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocations> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Locations = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLocations;
		static IntPtr id_setLocations_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation> Locations {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocations']/method[@name='getLocations' and count(parameter)=0]"
			[Register ("getLocations", "()Ljava/util/List;", "GetGetLocationsHandler")]
			get {
				if (id_getLocations == IntPtr.Zero)
					id_getLocations = JNIEnv.GetMethodID (class_ref, "getLocations", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getLocations), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLocations", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocations']/method[@name='setLocations' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.protocol.established.locations.EstablishedLocation&gt;']]"
			[Register ("setLocations", "(Ljava/util/List;)V", "GetSetLocations_Ljava_util_List_Handler")]
			set {
				if (id_setLocations_Ljava_util_List_ == IntPtr.Zero)
					id_setLocations_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setLocations", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLocations_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLocations", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
