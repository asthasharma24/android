using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Json {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='LowerCaseWithUnderscoresPropertyNameMapper']"
	[global::Android.Runtime.Register ("com/gimbal/internal/json/LowerCaseWithUnderscoresPropertyNameMapper", DoNotGenerateAcw=true)]
	public partial class LowerCaseWithUnderscoresPropertyNameMapper : global::Com.Gimbal.Internal.Json.PropertyNameMapper {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/json/LowerCaseWithUnderscoresPropertyNameMapper", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (LowerCaseWithUnderscoresPropertyNameMapper); }
		}

		protected LowerCaseWithUnderscoresPropertyNameMapper (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='LowerCaseWithUnderscoresPropertyNameMapper']/constructor[@name='LowerCaseWithUnderscoresPropertyNameMapper' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe LowerCaseWithUnderscoresPropertyNameMapper ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (LowerCaseWithUnderscoresPropertyNameMapper)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetConvertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_Handler ()
		{
			if (cb_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ == null)
				cb_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_ConvertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_);
			return cb_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_;
		}

		static IntPtr n_ConvertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.LowerCaseWithUnderscoresPropertyNameMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.LowerCaseWithUnderscoresPropertyNameMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.ConvertFromJsonPropertyName (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='LowerCaseWithUnderscoresPropertyNameMapper']/method[@name='convertFromJsonPropertyName' and count(parameter)=2 and parameter[1][@type='java.lang.Class&lt;?&gt;'] and parameter[2][@type='java.lang.String']]"
		[Register ("convertFromJsonPropertyName", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;", "GetConvertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_Handler")]
		public override unsafe string ConvertFromJsonPropertyName (global::Java.Lang.Class p0, string p1)
		{
			if (id_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ == IntPtr.Zero)
				id_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "convertFromJsonPropertyName", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				string __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "convertFromJsonPropertyName", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static Delegate cb_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetConvertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_Handler ()
		{
			if (cb_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ == null)
				cb_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_ConvertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_);
			return cb_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_;
		}

		static IntPtr n_ConvertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.LowerCaseWithUnderscoresPropertyNameMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.LowerCaseWithUnderscoresPropertyNameMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.ConvertToJsonPropertyName (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='LowerCaseWithUnderscoresPropertyNameMapper']/method[@name='convertToJsonPropertyName' and count(parameter)=2 and parameter[1][@type='java.lang.Class&lt;?&gt;'] and parameter[2][@type='java.lang.String']]"
		[Register ("convertToJsonPropertyName", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;", "GetConvertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_Handler")]
		public override unsafe string ConvertToJsonPropertyName (global::Java.Lang.Class p0, string p1)
		{
			if (id_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ == IntPtr.Zero)
				id_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "convertToJsonPropertyName", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				string __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "convertToJsonPropertyName", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static IntPtr id_mapJavaToRubyPropertyName_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='LowerCaseWithUnderscoresPropertyNameMapper']/method[@name='mapJavaToRubyPropertyName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("mapJavaToRubyPropertyName", "(Ljava/lang/String;)Ljava/lang/String;", "")]
		public static unsafe string MapJavaToRubyPropertyName (string p0)
		{
			if (id_mapJavaToRubyPropertyName_Ljava_lang_String_ == IntPtr.Zero)
				id_mapJavaToRubyPropertyName_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "mapJavaToRubyPropertyName", "(Ljava/lang/String;)Ljava/lang/String;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				string __ret = JNIEnv.GetString (JNIEnv.CallStaticObjectMethod  (class_ref, id_mapJavaToRubyPropertyName_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_mapRubyToJavaPropertyName_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='LowerCaseWithUnderscoresPropertyNameMapper']/method[@name='mapRubyToJavaPropertyName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("mapRubyToJavaPropertyName", "(Ljava/lang/String;)Ljava/lang/String;", "")]
		public static unsafe string MapRubyToJavaPropertyName (string p0)
		{
			if (id_mapRubyToJavaPropertyName_Ljava_lang_String_ == IntPtr.Zero)
				id_mapRubyToJavaPropertyName_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "mapRubyToJavaPropertyName", "(Ljava/lang/String;)Ljava/lang/String;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				string __ret = JNIEnv.GetString (JNIEnv.CallStaticObjectMethod  (class_ref, id_mapRubyToJavaPropertyName_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
