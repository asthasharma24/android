using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconManager']"
	[global::Android.Runtime.Register ("com/gimbal/android/BeaconManager", DoNotGenerateAcw=true)]
	public partial class BeaconManager : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/BeaconManager", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (BeaconManager); }
		}

		protected BeaconManager (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconManager']/constructor[@name='BeaconManager' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe BeaconManager ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (BeaconManager)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_addListener_Lcom_gimbal_android_BeaconEventListener_;
#pragma warning disable 0169
		static Delegate GetAddListener_Lcom_gimbal_android_BeaconEventListener_Handler ()
		{
			if (cb_addListener_Lcom_gimbal_android_BeaconEventListener_ == null)
				cb_addListener_Lcom_gimbal_android_BeaconEventListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddListener_Lcom_gimbal_android_BeaconEventListener_);
			return cb_addListener_Lcom_gimbal_android_BeaconEventListener_;
		}

		static void n_AddListener_Lcom_gimbal_android_BeaconEventListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Android.BeaconManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.BeaconManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.BeaconEventListener p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.BeaconEventListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addListener_Lcom_gimbal_android_BeaconEventListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconManager']/method[@name='addListener' and count(parameter)=1 and parameter[1][@type='com.gimbal.android.BeaconEventListener']]"
		[Register ("addListener", "(Lcom/gimbal/android/BeaconEventListener;)V", "GetAddListener_Lcom_gimbal_android_BeaconEventListener_Handler")]
		public virtual unsafe void AddListener (global::Com.Gimbal.Android.BeaconEventListener p0)
		{
			if (id_addListener_Lcom_gimbal_android_BeaconEventListener_ == IntPtr.Zero)
				id_addListener_Lcom_gimbal_android_BeaconEventListener_ = JNIEnv.GetMethodID (class_ref, "addListener", "(Lcom/gimbal/android/BeaconEventListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addListener_Lcom_gimbal_android_BeaconEventListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addListener", "(Lcom/gimbal/android/BeaconEventListener;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_removeListener_Lcom_gimbal_android_BeaconEventListener_;
#pragma warning disable 0169
		static Delegate GetRemoveListener_Lcom_gimbal_android_BeaconEventListener_Handler ()
		{
			if (cb_removeListener_Lcom_gimbal_android_BeaconEventListener_ == null)
				cb_removeListener_Lcom_gimbal_android_BeaconEventListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RemoveListener_Lcom_gimbal_android_BeaconEventListener_);
			return cb_removeListener_Lcom_gimbal_android_BeaconEventListener_;
		}

		static void n_RemoveListener_Lcom_gimbal_android_BeaconEventListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Android.BeaconManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.BeaconManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.BeaconEventListener p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.BeaconEventListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_removeListener_Lcom_gimbal_android_BeaconEventListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconManager']/method[@name='removeListener' and count(parameter)=1 and parameter[1][@type='com.gimbal.android.BeaconEventListener']]"
		[Register ("removeListener", "(Lcom/gimbal/android/BeaconEventListener;)V", "GetRemoveListener_Lcom_gimbal_android_BeaconEventListener_Handler")]
		public virtual unsafe void RemoveListener (global::Com.Gimbal.Android.BeaconEventListener p0)
		{
			if (id_removeListener_Lcom_gimbal_android_BeaconEventListener_ == IntPtr.Zero)
				id_removeListener_Lcom_gimbal_android_BeaconEventListener_ = JNIEnv.GetMethodID (class_ref, "removeListener", "(Lcom/gimbal/android/BeaconEventListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_removeListener_Lcom_gimbal_android_BeaconEventListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "removeListener", "(Lcom/gimbal/android/BeaconEventListener;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_startListening;
#pragma warning disable 0169
		static Delegate GetStartListeningHandler ()
		{
			if (cb_startListening == null)
				cb_startListening = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StartListening);
			return cb_startListening;
		}

		static void n_StartListening (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.BeaconManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.BeaconManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StartListening ();
		}
#pragma warning restore 0169

		static IntPtr id_startListening;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconManager']/method[@name='startListening' and count(parameter)=0]"
		[Register ("startListening", "()V", "GetStartListeningHandler")]
		public virtual unsafe void StartListening ()
		{
			if (id_startListening == IntPtr.Zero)
				id_startListening = JNIEnv.GetMethodID (class_ref, "startListening", "()V");
			try {

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_startListening);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "startListening", "()V"));
			} finally {
			}
		}

		static Delegate cb_stopListening;
#pragma warning disable 0169
		static Delegate GetStopListeningHandler ()
		{
			if (cb_stopListening == null)
				cb_stopListening = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StopListening);
			return cb_stopListening;
		}

		static void n_StopListening (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.BeaconManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.BeaconManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StopListening ();
		}
#pragma warning restore 0169

		static IntPtr id_stopListening;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconManager']/method[@name='stopListening' and count(parameter)=0]"
		[Register ("stopListening", "()V", "GetStopListeningHandler")]
		public virtual unsafe void StopListening ()
		{
			if (id_stopListening == IntPtr.Zero)
				id_stopListening = JNIEnv.GetMethodID (class_ref, "stopListening", "()V");
			try {

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_stopListening);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "stopListening", "()V"));
			} finally {
			}
		}

	}
}
