using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Logging {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogLevel']"
	[global::Android.Runtime.Register ("com/gimbal/logging/GimbalLogLevel", DoNotGenerateAcw=true)]
	public sealed partial class GimbalLogLevel : global::Java.Lang.Enum {


		static IntPtr DEBUG_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogLevel']/field[@name='DEBUG']"
		[Register ("DEBUG")]
		public static global::Com.Gimbal.Logging.GimbalLogLevel Debug {
			get {
				if (DEBUG_jfieldId == IntPtr.Zero)
					DEBUG_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "DEBUG", "Lcom/gimbal/logging/GimbalLogLevel;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, DEBUG_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.GimbalLogLevel> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr ERROR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogLevel']/field[@name='ERROR']"
		[Register ("ERROR")]
		public static global::Com.Gimbal.Logging.GimbalLogLevel Error {
			get {
				if (ERROR_jfieldId == IntPtr.Zero)
					ERROR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "ERROR", "Lcom/gimbal/logging/GimbalLogLevel;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, ERROR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.GimbalLogLevel> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr INFO_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogLevel']/field[@name='INFO']"
		[Register ("INFO")]
		public static global::Com.Gimbal.Logging.GimbalLogLevel Info {
			get {
				if (INFO_jfieldId == IntPtr.Zero)
					INFO_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "INFO", "Lcom/gimbal/logging/GimbalLogLevel;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, INFO_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.GimbalLogLevel> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr TRACE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogLevel']/field[@name='TRACE']"
		[Register ("TRACE")]
		public static global::Com.Gimbal.Logging.GimbalLogLevel Trace {
			get {
				if (TRACE_jfieldId == IntPtr.Zero)
					TRACE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "TRACE", "Lcom/gimbal/logging/GimbalLogLevel;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, TRACE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.GimbalLogLevel> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr WARN_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogLevel']/field[@name='WARN']"
		[Register ("WARN")]
		public static global::Com.Gimbal.Logging.GimbalLogLevel Warn {
			get {
				if (WARN_jfieldId == IntPtr.Zero)
					WARN_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "WARN", "Lcom/gimbal/logging/GimbalLogLevel;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, WARN_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.GimbalLogLevel> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/logging/GimbalLogLevel", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (GimbalLogLevel); }
		}

		internal GimbalLogLevel (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_valueOf;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogLevel']/method[@name='valueOf' and count(parameter)=0]"
		[Register ("valueOf", "()I", "")]
		public unsafe int ValueOf ()
		{
			if (id_valueOf == IntPtr.Zero)
				id_valueOf = JNIEnv.GetMethodID (class_ref, "valueOf", "()I");
			try {
				return JNIEnv.CallIntMethod  (Handle, id_valueOf);
			} finally {
			}
		}

		static IntPtr id_valueOf_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogLevel']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("valueOf", "(Ljava/lang/String;)Lcom/gimbal/logging/GimbalLogLevel;", "")]
		public static unsafe global::Com.Gimbal.Logging.GimbalLogLevel ValueOf (string p0)
		{
			if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
				id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/gimbal/logging/GimbalLogLevel;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				global::Com.Gimbal.Logging.GimbalLogLevel __ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.GimbalLogLevel> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_values;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogLevel']/method[@name='values' and count(parameter)=0]"
		[Register ("values", "()[Lcom/gimbal/logging/GimbalLogLevel;", "")]
		public static unsafe global::Com.Gimbal.Logging.GimbalLogLevel[] Values ()
		{
			if (id_values == IntPtr.Zero)
				id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/gimbal/logging/GimbalLogLevel;");
			try {
				return (global::Com.Gimbal.Logging.GimbalLogLevel[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Gimbal.Logging.GimbalLogLevel));
			} finally {
			}
		}

	}
}
