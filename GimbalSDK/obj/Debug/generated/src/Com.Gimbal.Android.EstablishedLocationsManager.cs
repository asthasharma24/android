using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='EstablishedLocationsManager']"
	[global::Android.Runtime.Register ("com/gimbal/android/EstablishedLocationsManager", DoNotGenerateAcw=true)]
	public partial class EstablishedLocationsManager : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/EstablishedLocationsManager", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (EstablishedLocationsManager); }
		}

		protected EstablishedLocationsManager (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static Delegate cb_getEstablishedLocations;
#pragma warning disable 0169
		static Delegate GetGetEstablishedLocationsHandler ()
		{
			if (cb_getEstablishedLocations == null)
				cb_getEstablishedLocations = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEstablishedLocations);
			return cb_getEstablishedLocations;
		}

		static IntPtr n_GetEstablishedLocations (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.EstablishedLocationsManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.EstablishedLocationsManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Android.EstablishedLocation>.ToLocalJniHandle (__this.EstablishedLocations);
		}
#pragma warning restore 0169

		static IntPtr id_getEstablishedLocations;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Android.EstablishedLocation> EstablishedLocations {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='EstablishedLocationsManager']/method[@name='getEstablishedLocations' and count(parameter)=0]"
			[Register ("getEstablishedLocations", "()Ljava/util/List;", "GetGetEstablishedLocationsHandler")]
			get {
				if (id_getEstablishedLocations == IntPtr.Zero)
					id_getEstablishedLocations = JNIEnv.GetMethodID (class_ref, "getEstablishedLocations", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Android.EstablishedLocation>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getEstablishedLocations), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Android.EstablishedLocation>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEstablishedLocations", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getInstance;
		public static unsafe global::Com.Gimbal.Android.EstablishedLocationsManager Instance {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='EstablishedLocationsManager']/method[@name='getInstance' and count(parameter)=0]"
			[Register ("getInstance", "()Lcom/gimbal/android/EstablishedLocationsManager;", "GetGetInstanceHandler")]
			get {
				if (id_getInstance == IntPtr.Zero)
					id_getInstance = JNIEnv.GetStaticMethodID (class_ref, "getInstance", "()Lcom/gimbal/android/EstablishedLocationsManager;");
				try {
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.EstablishedLocationsManager> (JNIEnv.CallStaticObjectMethod  (class_ref, id_getInstance), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_isMonitoring;
#pragma warning disable 0169
		static Delegate GetIsMonitoringHandler ()
		{
			if (cb_isMonitoring == null)
				cb_isMonitoring = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsMonitoring);
			return cb_isMonitoring;
		}

		static bool n_IsMonitoring (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.EstablishedLocationsManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.EstablishedLocationsManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsMonitoring;
		}
#pragma warning restore 0169

		static IntPtr id_isMonitoring;
		public virtual unsafe bool IsMonitoring {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='EstablishedLocationsManager']/method[@name='isMonitoring' and count(parameter)=0]"
			[Register ("isMonitoring", "()Z", "GetIsMonitoringHandler")]
			get {
				if (id_isMonitoring == IntPtr.Zero)
					id_isMonitoring = JNIEnv.GetMethodID (class_ref, "isMonitoring", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isMonitoring);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isMonitoring", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_startMonitoring;
#pragma warning disable 0169
		static Delegate GetStartMonitoringHandler ()
		{
			if (cb_startMonitoring == null)
				cb_startMonitoring = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StartMonitoring);
			return cb_startMonitoring;
		}

		static void n_StartMonitoring (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.EstablishedLocationsManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.EstablishedLocationsManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StartMonitoring ();
		}
#pragma warning restore 0169

		static IntPtr id_startMonitoring;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='EstablishedLocationsManager']/method[@name='startMonitoring' and count(parameter)=0]"
		[Register ("startMonitoring", "()V", "GetStartMonitoringHandler")]
		public virtual unsafe void StartMonitoring ()
		{
			if (id_startMonitoring == IntPtr.Zero)
				id_startMonitoring = JNIEnv.GetMethodID (class_ref, "startMonitoring", "()V");
			try {

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_startMonitoring);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "startMonitoring", "()V"));
			} finally {
			}
		}

		static Delegate cb_stopMonitoring;
#pragma warning disable 0169
		static Delegate GetStopMonitoringHandler ()
		{
			if (cb_stopMonitoring == null)
				cb_stopMonitoring = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StopMonitoring);
			return cb_stopMonitoring;
		}

		static void n_StopMonitoring (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.EstablishedLocationsManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.EstablishedLocationsManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StopMonitoring ();
		}
#pragma warning restore 0169

		static IntPtr id_stopMonitoring;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='EstablishedLocationsManager']/method[@name='stopMonitoring' and count(parameter)=0]"
		[Register ("stopMonitoring", "()V", "GetStopMonitoringHandler")]
		public virtual unsafe void StopMonitoring ()
		{
			if (id_stopMonitoring == IntPtr.Zero)
				id_stopMonitoring = JNIEnv.GetMethodID (class_ref, "stopMonitoring", "()V");
			try {

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_stopMonitoring);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "stopMonitoring", "()V"));
			} finally {
			}
		}

	}
}
