using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='InstanceStatusProperties']"
	[global::Android.Runtime.Register ("com/gimbal/internal/protocol/InstanceStatusProperties", DoNotGenerateAcw=true)]
	public partial class InstanceStatusProperties : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/protocol/InstanceStatusProperties", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (InstanceStatusProperties); }
		}

		protected InstanceStatusProperties (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='InstanceStatusProperties']/constructor[@name='InstanceStatusProperties' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe InstanceStatusProperties ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (InstanceStatusProperties)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getEnabledAt;
#pragma warning disable 0169
		static Delegate GetGetEnabledAtHandler ()
		{
			if (cb_getEnabledAt == null)
				cb_getEnabledAt = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetEnabledAt);
			return cb_getEnabledAt;
		}

		static long n_GetEnabledAt (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.InstanceStatusProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.InstanceStatusProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.EnabledAt;
		}
#pragma warning restore 0169

		static Delegate cb_setEnabledAt_J;
#pragma warning disable 0169
		static Delegate GetSetEnabledAt_JHandler ()
		{
			if (cb_setEnabledAt_J == null)
				cb_setEnabledAt_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetEnabledAt_J);
			return cb_setEnabledAt_J;
		}

		static void n_SetEnabledAt_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Internal.Protocol.InstanceStatusProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.InstanceStatusProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.EnabledAt = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEnabledAt;
		static IntPtr id_setEnabledAt_J;
		public virtual unsafe long EnabledAt {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='InstanceStatusProperties']/method[@name='getEnabledAt' and count(parameter)=0]"
			[Register ("getEnabledAt", "()J", "GetGetEnabledAtHandler")]
			get {
				if (id_getEnabledAt == IntPtr.Zero)
					id_getEnabledAt = JNIEnv.GetMethodID (class_ref, "getEnabledAt", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getEnabledAt);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEnabledAt", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='InstanceStatusProperties']/method[@name='setEnabledAt' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setEnabledAt", "(J)V", "GetSetEnabledAt_JHandler")]
			set {
				if (id_setEnabledAt_J == IntPtr.Zero)
					id_setEnabledAt_J = JNIEnv.GetMethodID (class_ref, "setEnabledAt", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEnabledAt_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEnabledAt", "(J)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPermittedAt;
#pragma warning disable 0169
		static Delegate GetGetPermittedAtHandler ()
		{
			if (cb_getPermittedAt == null)
				cb_getPermittedAt = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetPermittedAt);
			return cb_getPermittedAt;
		}

		static long n_GetPermittedAt (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.InstanceStatusProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.InstanceStatusProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.PermittedAt;
		}
#pragma warning restore 0169

		static Delegate cb_setPermittedAt_J;
#pragma warning disable 0169
		static Delegate GetSetPermittedAt_JHandler ()
		{
			if (cb_setPermittedAt_J == null)
				cb_setPermittedAt_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetPermittedAt_J);
			return cb_setPermittedAt_J;
		}

		static void n_SetPermittedAt_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Internal.Protocol.InstanceStatusProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.InstanceStatusProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.PermittedAt = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPermittedAt;
		static IntPtr id_setPermittedAt_J;
		public virtual unsafe long PermittedAt {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='InstanceStatusProperties']/method[@name='getPermittedAt' and count(parameter)=0]"
			[Register ("getPermittedAt", "()J", "GetGetPermittedAtHandler")]
			get {
				if (id_getPermittedAt == IntPtr.Zero)
					id_getPermittedAt = JNIEnv.GetMethodID (class_ref, "getPermittedAt", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getPermittedAt);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPermittedAt", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='InstanceStatusProperties']/method[@name='setPermittedAt' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setPermittedAt", "(J)V", "GetSetPermittedAt_JHandler")]
			set {
				if (id_setPermittedAt_J == IntPtr.Zero)
					id_setPermittedAt_J = JNIEnv.GetMethodID (class_ref, "setPermittedAt", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPermittedAt_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPermittedAt", "(J)V"), __args);
				} finally {
				}
			}
		}

	}
}
