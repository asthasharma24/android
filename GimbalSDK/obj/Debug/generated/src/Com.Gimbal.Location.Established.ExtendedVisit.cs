using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Location.Established {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ExtendedVisit']"
	[global::Android.Runtime.Register ("com/gimbal/location/established/ExtendedVisit", DoNotGenerateAcw=true)]
	public partial class ExtendedVisit : global::Com.Gimbal.Protocol.Established.Locations.Visit, global::Com.Gimbal.Proguard.IKeep {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/location/established/ExtendedVisit", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ExtendedVisit); }
		}

		protected ExtendedVisit (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Ljava_lang_String_Ljava_lang_Long_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ExtendedVisit']/constructor[@name='ExtendedVisit' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.Long']]"
		[Register (".ctor", "(Ljava/lang/String;Ljava/lang/Long;)V", "")]
		public unsafe ExtendedVisit (string p0, global::Java.Lang.Long p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (p1);
				if (GetType () != typeof (ExtendedVisit)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Ljava/lang/String;Ljava/lang/Long;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Ljava/lang/String;Ljava/lang/Long;)V", __args);
					return;
				}

				if (id_ctor_Ljava_lang_String_Ljava_lang_Long_ == IntPtr.Zero)
					id_ctor_Ljava_lang_String_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Ljava/lang/String;Ljava/lang/Long;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Ljava_lang_String_Ljava_lang_Long_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Ljava_lang_String_Ljava_lang_Long_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_createdAt;
#pragma warning disable 0169
		static Delegate GetCreatedAtHandler ()
		{
			if (cb_createdAt == null)
				cb_createdAt = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_CreatedAt);
			return cb_createdAt;
		}

		static IntPtr n_CreatedAt (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.ExtendedVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ExtendedVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.CreatedAt ());
		}
#pragma warning restore 0169

		static IntPtr id_createdAt;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ExtendedVisit']/method[@name='createdAt' and count(parameter)=0]"
		[Register ("createdAt", "()Ljava/lang/Long;", "GetCreatedAtHandler")]
		public virtual unsafe global::Java.Lang.Long CreatedAt ()
		{
			if (id_createdAt == IntPtr.Zero)
				id_createdAt = JNIEnv.GetMethodID (class_ref, "createdAt", "()Ljava/lang/Long;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_createdAt), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "createdAt", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_durationInMillis;
#pragma warning disable 0169
		static Delegate GetDurationInMillisHandler ()
		{
			if (cb_durationInMillis == null)
				cb_durationInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_DurationInMillis);
			return cb_durationInMillis;
		}

		static long n_DurationInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.ExtendedVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ExtendedVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DurationInMillis ();
		}
#pragma warning restore 0169

		static IntPtr id_durationInMillis;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ExtendedVisit']/method[@name='durationInMillis' and count(parameter)=0]"
		[Register ("durationInMillis", "()J", "GetDurationInMillisHandler")]
		public virtual unsafe long DurationInMillis ()
		{
			if (id_durationInMillis == IntPtr.Zero)
				id_durationInMillis = JNIEnv.GetMethodID (class_ref, "durationInMillis", "()J");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallLongMethod  (Handle, id_durationInMillis);
				else
					return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "durationInMillis", "()J"));
			} finally {
			}
		}

		static Delegate cb_locationId;
#pragma warning disable 0169
		static Delegate GetLocationIdHandler ()
		{
			if (cb_locationId == null)
				cb_locationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_LocationId);
			return cb_locationId;
		}

		static IntPtr n_LocationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.ExtendedVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ExtendedVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.LocationId ());
		}
#pragma warning restore 0169

		static IntPtr id_locationId;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ExtendedVisit']/method[@name='locationId' and count(parameter)=0]"
		[Register ("locationId", "()Ljava/lang/String;", "GetLocationIdHandler")]
		public virtual unsafe string LocationId ()
		{
			if (id_locationId == IntPtr.Zero)
				id_locationId = JNIEnv.GetMethodID (class_ref, "locationId", "()Ljava/lang/String;");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_locationId), JniHandleOwnership.TransferLocalRef);
				else
					return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "locationId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

	}
}
