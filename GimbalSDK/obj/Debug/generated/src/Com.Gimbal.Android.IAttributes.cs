using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.gimbal.android']/interface[@name='Attributes']"
	[Register ("com/gimbal/android/Attributes", "", "Com.Gimbal.Android.IAttributesInvoker")]
	public partial interface IAttributes : IJavaObject {

		global::System.Collections.Generic.IList<string> AllKeys {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/interface[@name='Attributes']/method[@name='getAllKeys' and count(parameter)=0]"
			[Register ("getAllKeys", "()Ljava/util/List;", "GetGetAllKeysHandler:Com.Gimbal.Android.IAttributesInvoker, GimbalSDK")] get;
		}

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/interface[@name='Attributes']/method[@name='getValue' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("getValue", "(Ljava/lang/String;)Ljava/lang/String;", "GetGetValue_Ljava_lang_String_Handler:Com.Gimbal.Android.IAttributesInvoker, GimbalSDK")]
		string GetValue (string p0);

	}

	[global::Android.Runtime.Register ("com/gimbal/android/Attributes", DoNotGenerateAcw=true)]
	internal class IAttributesInvoker : global::Java.Lang.Object, IAttributes {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/gimbal/android/Attributes");
		IntPtr class_ref;

		public static IAttributes GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IAttributes> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.gimbal.android.Attributes"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IAttributesInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IAttributesInvoker); }
		}

		static Delegate cb_getAllKeys;
#pragma warning disable 0169
		static Delegate GetGetAllKeysHandler ()
		{
			if (cb_getAllKeys == null)
				cb_getAllKeys = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAllKeys);
			return cb_getAllKeys;
		}

		static IntPtr n_GetAllKeys (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.IAttributes __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.IAttributes> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<string>.ToLocalJniHandle (__this.AllKeys);
		}
#pragma warning restore 0169

		IntPtr id_getAllKeys;
		public unsafe global::System.Collections.Generic.IList<string> AllKeys {
			get {
				if (id_getAllKeys == IntPtr.Zero)
					id_getAllKeys = JNIEnv.GetMethodID (class_ref, "getAllKeys", "()Ljava/util/List;");
				return global::Android.Runtime.JavaList<string>.FromJniHandle (JNIEnv.CallObjectMethod (Handle, id_getAllKeys), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getValue_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetGetValue_Ljava_lang_String_Handler ()
		{
			if (cb_getValue_Ljava_lang_String_ == null)
				cb_getValue_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_GetValue_Ljava_lang_String_);
			return cb_getValue_Ljava_lang_String_;
		}

		static IntPtr n_GetValue_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Android.IAttributes __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.IAttributes> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.GetValue (p0));
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_getValue_Ljava_lang_String_;
		public unsafe string GetValue (string p0)
		{
			if (id_getValue_Ljava_lang_String_ == IntPtr.Zero)
				id_getValue_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "getValue", "(Ljava/lang/String;)Ljava/lang/String;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (native_p0);
			string __ret = JNIEnv.GetString (JNIEnv.CallObjectMethod (Handle, id_getValue_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
			JNIEnv.DeleteLocalRef (native_p0);
			return __ret;
		}

	}

}
