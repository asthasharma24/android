using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolAssignedTransmitter']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/ProtocolAssignedTransmitter", DoNotGenerateAcw=true)]
	public partial class ProtocolAssignedTransmitter : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/ProtocolAssignedTransmitter", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ProtocolAssignedTransmitter); }
		}

		protected ProtocolAssignedTransmitter (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolAssignedTransmitter']/constructor[@name='ProtocolAssignedTransmitter' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ProtocolAssignedTransmitter ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ProtocolAssignedTransmitter)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getDeviceId;
#pragma warning disable 0169
		static Delegate GetGetDeviceIdHandler ()
		{
			if (cb_getDeviceId == null)
				cb_getDeviceId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDeviceId);
			return cb_getDeviceId;
		}

		static IntPtr n_GetDeviceId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.DeviceId);
		}
#pragma warning restore 0169

		static Delegate cb_setDeviceId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetDeviceId_Ljava_lang_String_Handler ()
		{
			if (cb_setDeviceId_Ljava_lang_String_ == null)
				cb_setDeviceId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDeviceId_Ljava_lang_String_);
			return cb_setDeviceId_Ljava_lang_String_;
		}

		static void n_SetDeviceId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DeviceId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDeviceId;
		static IntPtr id_setDeviceId_Ljava_lang_String_;
		public virtual unsafe string DeviceId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolAssignedTransmitter']/method[@name='getDeviceId' and count(parameter)=0]"
			[Register ("getDeviceId", "()Ljava/lang/String;", "GetGetDeviceIdHandler")]
			get {
				if (id_getDeviceId == IntPtr.Zero)
					id_getDeviceId = JNIEnv.GetMethodID (class_ref, "getDeviceId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getDeviceId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDeviceId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolAssignedTransmitter']/method[@name='setDeviceId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setDeviceId", "(Ljava/lang/String;)V", "GetSetDeviceId_Ljava_lang_String_Handler")]
			set {
				if (id_setDeviceId_Ljava_lang_String_ == IntPtr.Zero)
					id_setDeviceId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setDeviceId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDeviceId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDeviceId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getFactoryId;
#pragma warning disable 0169
		static Delegate GetGetFactoryIdHandler ()
		{
			if (cb_getFactoryId == null)
				cb_getFactoryId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetFactoryId);
			return cb_getFactoryId;
		}

		static IntPtr n_GetFactoryId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.FactoryId);
		}
#pragma warning restore 0169

		static Delegate cb_setFactoryId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetFactoryId_Ljava_lang_String_Handler ()
		{
			if (cb_setFactoryId_Ljava_lang_String_ == null)
				cb_setFactoryId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetFactoryId_Ljava_lang_String_);
			return cb_setFactoryId_Ljava_lang_String_;
		}

		static void n_SetFactoryId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.FactoryId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getFactoryId;
		static IntPtr id_setFactoryId_Ljava_lang_String_;
		public virtual unsafe string FactoryId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolAssignedTransmitter']/method[@name='getFactoryId' and count(parameter)=0]"
			[Register ("getFactoryId", "()Ljava/lang/String;", "GetGetFactoryIdHandler")]
			get {
				if (id_getFactoryId == IntPtr.Zero)
					id_getFactoryId = JNIEnv.GetMethodID (class_ref, "getFactoryId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getFactoryId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getFactoryId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolAssignedTransmitter']/method[@name='setFactoryId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setFactoryId", "(Ljava/lang/String;)V", "GetSetFactoryId_Ljava_lang_String_Handler")]
			set {
				if (id_setFactoryId_Ljava_lang_String_ == IntPtr.Zero)
					id_setFactoryId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setFactoryId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setFactoryId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setFactoryId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getKey1;
#pragma warning disable 0169
		static Delegate GetGetKey1Handler ()
		{
			if (cb_getKey1 == null)
				cb_getKey1 = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetKey1);
			return cb_getKey1;
		}

		static IntPtr n_GetKey1 (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Key1);
		}
#pragma warning restore 0169

		static Delegate cb_setKey1_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetKey1_Ljava_lang_String_Handler ()
		{
			if (cb_setKey1_Ljava_lang_String_ == null)
				cb_setKey1_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetKey1_Ljava_lang_String_);
			return cb_setKey1_Ljava_lang_String_;
		}

		static void n_SetKey1_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Key1 = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getKey1;
		static IntPtr id_setKey1_Ljava_lang_String_;
		public virtual unsafe string Key1 {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolAssignedTransmitter']/method[@name='getKey1' and count(parameter)=0]"
			[Register ("getKey1", "()Ljava/lang/String;", "GetGetKey1Handler")]
			get {
				if (id_getKey1 == IntPtr.Zero)
					id_getKey1 = JNIEnv.GetMethodID (class_ref, "getKey1", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getKey1), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getKey1", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolAssignedTransmitter']/method[@name='setKey1' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setKey1", "(Ljava/lang/String;)V", "GetSetKey1_Ljava_lang_String_Handler")]
			set {
				if (id_setKey1_Ljava_lang_String_ == IntPtr.Zero)
					id_setKey1_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setKey1", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setKey1_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setKey1", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getKey2;
#pragma warning disable 0169
		static Delegate GetGetKey2Handler ()
		{
			if (cb_getKey2 == null)
				cb_getKey2 = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetKey2);
			return cb_getKey2;
		}

		static IntPtr n_GetKey2 (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Key2);
		}
#pragma warning restore 0169

		static Delegate cb_setKey2_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetKey2_Ljava_lang_String_Handler ()
		{
			if (cb_setKey2_Ljava_lang_String_ == null)
				cb_setKey2_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetKey2_Ljava_lang_String_);
			return cb_setKey2_Ljava_lang_String_;
		}

		static void n_SetKey2_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Key2 = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getKey2;
		static IntPtr id_setKey2_Ljava_lang_String_;
		public virtual unsafe string Key2 {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolAssignedTransmitter']/method[@name='getKey2' and count(parameter)=0]"
			[Register ("getKey2", "()Ljava/lang/String;", "GetGetKey2Handler")]
			get {
				if (id_getKey2 == IntPtr.Zero)
					id_getKey2 = JNIEnv.GetMethodID (class_ref, "getKey2", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getKey2), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getKey2", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolAssignedTransmitter']/method[@name='setKey2' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setKey2", "(Ljava/lang/String;)V", "GetSetKey2_Ljava_lang_String_Handler")]
			set {
				if (id_setKey2_Ljava_lang_String_ == IntPtr.Zero)
					id_setKey2_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setKey2", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setKey2_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setKey2", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getProvisionTimestamp;
#pragma warning disable 0169
		static Delegate GetGetProvisionTimestampHandler ()
		{
			if (cb_getProvisionTimestamp == null)
				cb_getProvisionTimestamp = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetProvisionTimestamp);
			return cb_getProvisionTimestamp;
		}

		static long n_GetProvisionTimestamp (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ProvisionTimestamp;
		}
#pragma warning restore 0169

		static Delegate cb_setProvisionTimestamp_J;
#pragma warning disable 0169
		static Delegate GetSetProvisionTimestamp_JHandler ()
		{
			if (cb_setProvisionTimestamp_J == null)
				cb_setProvisionTimestamp_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetProvisionTimestamp_J);
			return cb_setProvisionTimestamp_J;
		}

		static void n_SetProvisionTimestamp_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolAssignedTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ProvisionTimestamp = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getProvisionTimestamp;
		static IntPtr id_setProvisionTimestamp_J;
		public virtual unsafe long ProvisionTimestamp {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolAssignedTransmitter']/method[@name='getProvisionTimestamp' and count(parameter)=0]"
			[Register ("getProvisionTimestamp", "()J", "GetGetProvisionTimestampHandler")]
			get {
				if (id_getProvisionTimestamp == IntPtr.Zero)
					id_getProvisionTimestamp = JNIEnv.GetMethodID (class_ref, "getProvisionTimestamp", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getProvisionTimestamp);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getProvisionTimestamp", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolAssignedTransmitter']/method[@name='setProvisionTimestamp' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setProvisionTimestamp", "(J)V", "GetSetProvisionTimestamp_JHandler")]
			set {
				if (id_setProvisionTimestamp_J == IntPtr.Zero)
					id_setProvisionTimestamp_J = JNIEnv.GetMethodID (class_ref, "setProvisionTimestamp", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setProvisionTimestamp_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setProvisionTimestamp", "(J)V"), __args);
				} finally {
				}
			}
		}

	}
}
