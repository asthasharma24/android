using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Content {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentsResponse']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/content/ContentsResponse", DoNotGenerateAcw=true)]
	public partial class ContentsResponse : global::Java.Lang.Object, global::Java.IO.ISerializable {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/content/ContentsResponse", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ContentsResponse); }
		}

		protected ContentsResponse (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentsResponse']/constructor[@name='ContentsResponse' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ContentsResponse ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ContentsResponse)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getOrganizationPlaceEventContents;
#pragma warning disable 0169
		static Delegate GetGetOrganizationPlaceEventContentsHandler ()
		{
			if (cb_getOrganizationPlaceEventContents == null)
				cb_getOrganizationPlaceEventContents = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationPlaceEventContents);
			return cb_getOrganizationPlaceEventContents;
		}

		static IntPtr n_GetOrganizationPlaceEventContents (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentsResponse __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentsResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.OrganizationPlaceEventContent>.ToLocalJniHandle (__this.OrganizationPlaceEventContents);
		}
#pragma warning restore 0169

		static Delegate cb_setOrganizationPlaceEventContents_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationPlaceEventContents_Ljava_util_List_Handler ()
		{
			if (cb_setOrganizationPlaceEventContents_Ljava_util_List_ == null)
				cb_setOrganizationPlaceEventContents_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationPlaceEventContents_Ljava_util_List_);
			return cb_setOrganizationPlaceEventContents_Ljava_util_List_;
		}

		static void n_SetOrganizationPlaceEventContents_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentsResponse __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentsResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.OrganizationPlaceEventContent>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OrganizationPlaceEventContents = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationPlaceEventContents;
		static IntPtr id_setOrganizationPlaceEventContents_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.Content.OrganizationPlaceEventContent> OrganizationPlaceEventContents {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentsResponse']/method[@name='getOrganizationPlaceEventContents' and count(parameter)=0]"
			[Register ("getOrganizationPlaceEventContents", "()Ljava/util/List;", "GetGetOrganizationPlaceEventContentsHandler")]
			get {
				if (id_getOrganizationPlaceEventContents == IntPtr.Zero)
					id_getOrganizationPlaceEventContents = JNIEnv.GetMethodID (class_ref, "getOrganizationPlaceEventContents", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.OrganizationPlaceEventContent>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationPlaceEventContents), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.OrganizationPlaceEventContent>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationPlaceEventContents", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentsResponse']/method[@name='setOrganizationPlaceEventContents' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.qsl.faar.protocol.content.OrganizationPlaceEventContent&gt;']]"
			[Register ("setOrganizationPlaceEventContents", "(Ljava/util/List;)V", "GetSetOrganizationPlaceEventContents_Ljava_util_List_Handler")]
			set {
				if (id_setOrganizationPlaceEventContents_Ljava_util_List_ == IntPtr.Zero)
					id_setOrganizationPlaceEventContents_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setOrganizationPlaceEventContents", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.OrganizationPlaceEventContent>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOrganizationPlaceEventContents_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationPlaceEventContents", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getTimeEventContents;
#pragma warning disable 0169
		static Delegate GetGetTimeEventContentsHandler ()
		{
			if (cb_getTimeEventContents == null)
				cb_getTimeEventContents = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTimeEventContents);
			return cb_getTimeEventContents;
		}

		static IntPtr n_GetTimeEventContents (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentsResponse __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentsResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent>.ToLocalJniHandle (__this.TimeEventContents);
		}
#pragma warning restore 0169

		static Delegate cb_setTimeEventContents_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetTimeEventContents_Ljava_util_List_Handler ()
		{
			if (cb_setTimeEventContents_Ljava_util_List_ == null)
				cb_setTimeEventContents_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTimeEventContents_Ljava_util_List_);
			return cb_setTimeEventContents_Ljava_util_List_;
		}

		static void n_SetTimeEventContents_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentsResponse __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentsResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.TimeEventContents = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTimeEventContents;
		static IntPtr id_setTimeEventContents_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent> TimeEventContents {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentsResponse']/method[@name='getTimeEventContents' and count(parameter)=0]"
			[Register ("getTimeEventContents", "()Ljava/util/List;", "GetGetTimeEventContentsHandler")]
			get {
				if (id_getTimeEventContents == IntPtr.Zero)
					id_getTimeEventContents = JNIEnv.GetMethodID (class_ref, "getTimeEventContents", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getTimeEventContents), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTimeEventContents", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentsResponse']/method[@name='setTimeEventContents' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.qsl.faar.protocol.content.TimeEventContent&gt;']]"
			[Register ("setTimeEventContents", "(Ljava/util/List;)V", "GetSetTimeEventContents_Ljava_util_List_Handler")]
			set {
				if (id_setTimeEventContents_Ljava_util_List_ == IntPtr.Zero)
					id_setTimeEventContents_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setTimeEventContents", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTimeEventContents_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTimeEventContents", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
