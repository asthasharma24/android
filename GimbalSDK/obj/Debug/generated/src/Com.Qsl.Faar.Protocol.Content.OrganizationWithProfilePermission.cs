using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Content {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='OrganizationWithProfilePermission']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/content/OrganizationWithProfilePermission", DoNotGenerateAcw=true)]
	public partial class OrganizationWithProfilePermission : global::Java.Lang.Object, global::Java.IO.ISerializable {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/content/OrganizationWithProfilePermission", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (OrganizationWithProfilePermission); }
		}

		protected OrganizationWithProfilePermission (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='OrganizationWithProfilePermission']/constructor[@name='OrganizationWithProfilePermission' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe OrganizationWithProfilePermission ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (OrganizationWithProfilePermission)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_Long_Handler ()
		{
			if (cb_setId_Ljava_lang_Long_ == null)
				cb_setId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_Long_);
			return cb_setId_Ljava_lang_Long_;
		}

		static void n_SetId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='OrganizationWithProfilePermission']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/Long;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='OrganizationWithProfilePermission']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setId", "(Ljava/lang/Long;)V", "GetSetId_Ljava_lang_Long_Handler")]
			set {
				if (id_setId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getProfilePermission;
#pragma warning disable 0169
		static Delegate GetGetProfilePermissionHandler ()
		{
			if (cb_getProfilePermission == null)
				cb_getProfilePermission = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetProfilePermission);
			return cb_getProfilePermission;
		}

		static IntPtr n_GetProfilePermission (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ProfilePermission);
		}
#pragma warning restore 0169

		static Delegate cb_setProfilePermission_Ljava_lang_Boolean_;
#pragma warning disable 0169
		static Delegate GetSetProfilePermission_Ljava_lang_Boolean_Handler ()
		{
			if (cb_setProfilePermission_Ljava_lang_Boolean_ == null)
				cb_setProfilePermission_Ljava_lang_Boolean_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetProfilePermission_Ljava_lang_Boolean_);
			return cb_setProfilePermission_Ljava_lang_Boolean_;
		}

		static void n_SetProfilePermission_Ljava_lang_Boolean_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Boolean p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ProfilePermission = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getProfilePermission;
		static IntPtr id_setProfilePermission_Ljava_lang_Boolean_;
		public virtual unsafe global::Java.Lang.Boolean ProfilePermission {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='OrganizationWithProfilePermission']/method[@name='getProfilePermission' and count(parameter)=0]"
			[Register ("getProfilePermission", "()Ljava/lang/Boolean;", "GetGetProfilePermissionHandler")]
			get {
				if (id_getProfilePermission == IntPtr.Zero)
					id_getProfilePermission = JNIEnv.GetMethodID (class_ref, "getProfilePermission", "()Ljava/lang/Boolean;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallObjectMethod  (Handle, id_getProfilePermission), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getProfilePermission", "()Ljava/lang/Boolean;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='OrganizationWithProfilePermission']/method[@name='setProfilePermission' and count(parameter)=1 and parameter[1][@type='java.lang.Boolean']]"
			[Register ("setProfilePermission", "(Ljava/lang/Boolean;)V", "GetSetProfilePermission_Ljava_lang_Boolean_Handler")]
			set {
				if (id_setProfilePermission_Ljava_lang_Boolean_ == IntPtr.Zero)
					id_setProfilePermission_Ljava_lang_Boolean_ = JNIEnv.GetMethodID (class_ref, "setProfilePermission", "(Ljava/lang/Boolean;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setProfilePermission_Ljava_lang_Boolean_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setProfilePermission", "(Ljava/lang/Boolean;)V"), __args);
				} finally {
				}
			}
		}

	}
}
