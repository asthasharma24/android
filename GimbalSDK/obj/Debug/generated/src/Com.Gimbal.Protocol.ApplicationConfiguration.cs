using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/ApplicationConfiguration", DoNotGenerateAcw=true)]
	public partial class ApplicationConfiguration : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/ApplicationConfiguration", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ApplicationConfiguration); }
		}

		protected ApplicationConfiguration (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/constructor[@name='ApplicationConfiguration' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ApplicationConfiguration ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ApplicationConfiguration)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getArrivalRSSI;
#pragma warning disable 0169
		static Delegate GetGetArrivalRSSIHandler ()
		{
			if (cb_getArrivalRSSI == null)
				cb_getArrivalRSSI = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetArrivalRSSI);
			return cb_getArrivalRSSI;
		}

		static IntPtr n_GetArrivalRSSI (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ArrivalRSSI);
		}
#pragma warning restore 0169

		static Delegate cb_setArrivalRSSI_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetArrivalRSSI_Ljava_lang_Integer_Handler ()
		{
			if (cb_setArrivalRSSI_Ljava_lang_Integer_ == null)
				cb_setArrivalRSSI_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetArrivalRSSI_Ljava_lang_Integer_);
			return cb_setArrivalRSSI_Ljava_lang_Integer_;
		}

		static void n_SetArrivalRSSI_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ArrivalRSSI = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getArrivalRSSI;
		static IntPtr id_setArrivalRSSI_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer ArrivalRSSI {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='getArrivalRSSI' and count(parameter)=0]"
			[Register ("getArrivalRSSI", "()Ljava/lang/Integer;", "GetGetArrivalRSSIHandler")]
			get {
				if (id_getArrivalRSSI == IntPtr.Zero)
					id_getArrivalRSSI = JNIEnv.GetMethodID (class_ref, "getArrivalRSSI", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getArrivalRSSI), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getArrivalRSSI", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setArrivalRSSI' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setArrivalRSSI", "(Ljava/lang/Integer;)V", "GetSetArrivalRSSI_Ljava_lang_Integer_Handler")]
			set {
				if (id_setArrivalRSSI_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setArrivalRSSI_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setArrivalRSSI", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setArrivalRSSI_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setArrivalRSSI", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getConfigFetchIntervalInMillis;
#pragma warning disable 0169
		static Delegate GetGetConfigFetchIntervalInMillisHandler ()
		{
			if (cb_getConfigFetchIntervalInMillis == null)
				cb_getConfigFetchIntervalInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetConfigFetchIntervalInMillis);
			return cb_getConfigFetchIntervalInMillis;
		}

		static IntPtr n_GetConfigFetchIntervalInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ConfigFetchIntervalInMillis);
		}
#pragma warning restore 0169

		static Delegate cb_setConfigFetchIntervalInMillis_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetConfigFetchIntervalInMillis_Ljava_lang_Long_Handler ()
		{
			if (cb_setConfigFetchIntervalInMillis_Ljava_lang_Long_ == null)
				cb_setConfigFetchIntervalInMillis_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetConfigFetchIntervalInMillis_Ljava_lang_Long_);
			return cb_setConfigFetchIntervalInMillis_Ljava_lang_Long_;
		}

		static void n_SetConfigFetchIntervalInMillis_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ConfigFetchIntervalInMillis = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getConfigFetchIntervalInMillis;
		static IntPtr id_setConfigFetchIntervalInMillis_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long ConfigFetchIntervalInMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='getConfigFetchIntervalInMillis' and count(parameter)=0]"
			[Register ("getConfigFetchIntervalInMillis", "()Ljava/lang/Long;", "GetGetConfigFetchIntervalInMillisHandler")]
			get {
				if (id_getConfigFetchIntervalInMillis == IntPtr.Zero)
					id_getConfigFetchIntervalInMillis = JNIEnv.GetMethodID (class_ref, "getConfigFetchIntervalInMillis", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getConfigFetchIntervalInMillis), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getConfigFetchIntervalInMillis", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setConfigFetchIntervalInMillis' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setConfigFetchIntervalInMillis", "(Ljava/lang/Long;)V", "GetSetConfigFetchIntervalInMillis_Ljava_lang_Long_Handler")]
			set {
				if (id_setConfigFetchIntervalInMillis_Ljava_lang_Long_ == IntPtr.Zero)
					id_setConfigFetchIntervalInMillis_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setConfigFetchIntervalInMillis", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setConfigFetchIntervalInMillis_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setConfigFetchIntervalInMillis", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureIntervalInBackgroundInMillis;
#pragma warning disable 0169
		static Delegate GetGetDepartureIntervalInBackgroundInMillisHandler ()
		{
			if (cb_getDepartureIntervalInBackgroundInMillis == null)
				cb_getDepartureIntervalInBackgroundInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDepartureIntervalInBackgroundInMillis);
			return cb_getDepartureIntervalInBackgroundInMillis;
		}

		static IntPtr n_GetDepartureIntervalInBackgroundInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DepartureIntervalInBackgroundInMillis);
		}
#pragma warning restore 0169

		static Delegate cb_setDepartureIntervalInBackgroundInMillis_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetDepartureIntervalInBackgroundInMillis_Ljava_lang_Long_Handler ()
		{
			if (cb_setDepartureIntervalInBackgroundInMillis_Ljava_lang_Long_ == null)
				cb_setDepartureIntervalInBackgroundInMillis_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDepartureIntervalInBackgroundInMillis_Ljava_lang_Long_);
			return cb_setDepartureIntervalInBackgroundInMillis_Ljava_lang_Long_;
		}

		static void n_SetDepartureIntervalInBackgroundInMillis_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DepartureIntervalInBackgroundInMillis = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureIntervalInBackgroundInMillis;
		static IntPtr id_setDepartureIntervalInBackgroundInMillis_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long DepartureIntervalInBackgroundInMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='getDepartureIntervalInBackgroundInMillis' and count(parameter)=0]"
			[Register ("getDepartureIntervalInBackgroundInMillis", "()Ljava/lang/Long;", "GetGetDepartureIntervalInBackgroundInMillisHandler")]
			get {
				if (id_getDepartureIntervalInBackgroundInMillis == IntPtr.Zero)
					id_getDepartureIntervalInBackgroundInMillis = JNIEnv.GetMethodID (class_ref, "getDepartureIntervalInBackgroundInMillis", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getDepartureIntervalInBackgroundInMillis), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureIntervalInBackgroundInMillis", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setDepartureIntervalInBackgroundInMillis' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setDepartureIntervalInBackgroundInMillis", "(Ljava/lang/Long;)V", "GetSetDepartureIntervalInBackgroundInMillis_Ljava_lang_Long_Handler")]
			set {
				if (id_setDepartureIntervalInBackgroundInMillis_Ljava_lang_Long_ == IntPtr.Zero)
					id_setDepartureIntervalInBackgroundInMillis_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setDepartureIntervalInBackgroundInMillis", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDepartureIntervalInBackgroundInMillis_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDepartureIntervalInBackgroundInMillis", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureIntervalInForegroundInMillis;
#pragma warning disable 0169
		static Delegate GetGetDepartureIntervalInForegroundInMillisHandler ()
		{
			if (cb_getDepartureIntervalInForegroundInMillis == null)
				cb_getDepartureIntervalInForegroundInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDepartureIntervalInForegroundInMillis);
			return cb_getDepartureIntervalInForegroundInMillis;
		}

		static IntPtr n_GetDepartureIntervalInForegroundInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DepartureIntervalInForegroundInMillis);
		}
#pragma warning restore 0169

		static Delegate cb_setDepartureIntervalInForegroundInMillis_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetDepartureIntervalInForegroundInMillis_Ljava_lang_Long_Handler ()
		{
			if (cb_setDepartureIntervalInForegroundInMillis_Ljava_lang_Long_ == null)
				cb_setDepartureIntervalInForegroundInMillis_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDepartureIntervalInForegroundInMillis_Ljava_lang_Long_);
			return cb_setDepartureIntervalInForegroundInMillis_Ljava_lang_Long_;
		}

		static void n_SetDepartureIntervalInForegroundInMillis_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DepartureIntervalInForegroundInMillis = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureIntervalInForegroundInMillis;
		static IntPtr id_setDepartureIntervalInForegroundInMillis_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long DepartureIntervalInForegroundInMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='getDepartureIntervalInForegroundInMillis' and count(parameter)=0]"
			[Register ("getDepartureIntervalInForegroundInMillis", "()Ljava/lang/Long;", "GetGetDepartureIntervalInForegroundInMillisHandler")]
			get {
				if (id_getDepartureIntervalInForegroundInMillis == IntPtr.Zero)
					id_getDepartureIntervalInForegroundInMillis = JNIEnv.GetMethodID (class_ref, "getDepartureIntervalInForegroundInMillis", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getDepartureIntervalInForegroundInMillis), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureIntervalInForegroundInMillis", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setDepartureIntervalInForegroundInMillis' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setDepartureIntervalInForegroundInMillis", "(Ljava/lang/Long;)V", "GetSetDepartureIntervalInForegroundInMillis_Ljava_lang_Long_Handler")]
			set {
				if (id_setDepartureIntervalInForegroundInMillis_Ljava_lang_Long_ == IntPtr.Zero)
					id_setDepartureIntervalInForegroundInMillis_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setDepartureIntervalInForegroundInMillis", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDepartureIntervalInForegroundInMillis_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDepartureIntervalInForegroundInMillis", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureRSSI;
#pragma warning disable 0169
		static Delegate GetGetDepartureRSSIHandler ()
		{
			if (cb_getDepartureRSSI == null)
				cb_getDepartureRSSI = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDepartureRSSI);
			return cb_getDepartureRSSI;
		}

		static IntPtr n_GetDepartureRSSI (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DepartureRSSI);
		}
#pragma warning restore 0169

		static Delegate cb_setDepartureRSSI_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetDepartureRSSI_Ljava_lang_Integer_Handler ()
		{
			if (cb_setDepartureRSSI_Ljava_lang_Integer_ == null)
				cb_setDepartureRSSI_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDepartureRSSI_Ljava_lang_Integer_);
			return cb_setDepartureRSSI_Ljava_lang_Integer_;
		}

		static void n_SetDepartureRSSI_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DepartureRSSI = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureRSSI;
		static IntPtr id_setDepartureRSSI_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer DepartureRSSI {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='getDepartureRSSI' and count(parameter)=0]"
			[Register ("getDepartureRSSI", "()Ljava/lang/Integer;", "GetGetDepartureRSSIHandler")]
			get {
				if (id_getDepartureRSSI == IntPtr.Zero)
					id_getDepartureRSSI = JNIEnv.GetMethodID (class_ref, "getDepartureRSSI", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getDepartureRSSI), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureRSSI", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setDepartureRSSI' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setDepartureRSSI", "(Ljava/lang/Integer;)V", "GetSetDepartureRSSI_Ljava_lang_Integer_Handler")]
			set {
				if (id_setDepartureRSSI_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setDepartureRSSI_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setDepartureRSSI", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDepartureRSSI_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDepartureRSSI", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getEstablishedLocationsMaxCountToSend;
#pragma warning disable 0169
		static Delegate GetGetEstablishedLocationsMaxCountToSendHandler ()
		{
			if (cb_getEstablishedLocationsMaxCountToSend == null)
				cb_getEstablishedLocationsMaxCountToSend = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEstablishedLocationsMaxCountToSend);
			return cb_getEstablishedLocationsMaxCountToSend;
		}

		static IntPtr n_GetEstablishedLocationsMaxCountToSend (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.EstablishedLocationsMaxCountToSend);
		}
#pragma warning restore 0169

		static Delegate cb_setEstablishedLocationsMaxCountToSend_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetEstablishedLocationsMaxCountToSend_Ljava_lang_Integer_Handler ()
		{
			if (cb_setEstablishedLocationsMaxCountToSend_Ljava_lang_Integer_ == null)
				cb_setEstablishedLocationsMaxCountToSend_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetEstablishedLocationsMaxCountToSend_Ljava_lang_Integer_);
			return cb_setEstablishedLocationsMaxCountToSend_Ljava_lang_Integer_;
		}

		static void n_SetEstablishedLocationsMaxCountToSend_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.EstablishedLocationsMaxCountToSend = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEstablishedLocationsMaxCountToSend;
		static IntPtr id_setEstablishedLocationsMaxCountToSend_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer EstablishedLocationsMaxCountToSend {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='getEstablishedLocationsMaxCountToSend' and count(parameter)=0]"
			[Register ("getEstablishedLocationsMaxCountToSend", "()Ljava/lang/Integer;", "GetGetEstablishedLocationsMaxCountToSendHandler")]
			get {
				if (id_getEstablishedLocationsMaxCountToSend == IntPtr.Zero)
					id_getEstablishedLocationsMaxCountToSend = JNIEnv.GetMethodID (class_ref, "getEstablishedLocationsMaxCountToSend", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getEstablishedLocationsMaxCountToSend), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEstablishedLocationsMaxCountToSend", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setEstablishedLocationsMaxCountToSend' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setEstablishedLocationsMaxCountToSend", "(Ljava/lang/Integer;)V", "GetSetEstablishedLocationsMaxCountToSend_Ljava_lang_Integer_Handler")]
			set {
				if (id_setEstablishedLocationsMaxCountToSend_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setEstablishedLocationsMaxCountToSend_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setEstablishedLocationsMaxCountToSend", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEstablishedLocationsMaxCountToSend_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEstablishedLocationsMaxCountToSend", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getEstablishedLocationsMinDurationInMillis;
#pragma warning disable 0169
		static Delegate GetGetEstablishedLocationsMinDurationInMillisHandler ()
		{
			if (cb_getEstablishedLocationsMinDurationInMillis == null)
				cb_getEstablishedLocationsMinDurationInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEstablishedLocationsMinDurationInMillis);
			return cb_getEstablishedLocationsMinDurationInMillis;
		}

		static IntPtr n_GetEstablishedLocationsMinDurationInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.EstablishedLocationsMinDurationInMillis);
		}
#pragma warning restore 0169

		static Delegate cb_setEstablishedLocationsMinDurationInMillis_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetEstablishedLocationsMinDurationInMillis_Ljava_lang_Integer_Handler ()
		{
			if (cb_setEstablishedLocationsMinDurationInMillis_Ljava_lang_Integer_ == null)
				cb_setEstablishedLocationsMinDurationInMillis_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetEstablishedLocationsMinDurationInMillis_Ljava_lang_Integer_);
			return cb_setEstablishedLocationsMinDurationInMillis_Ljava_lang_Integer_;
		}

		static void n_SetEstablishedLocationsMinDurationInMillis_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.EstablishedLocationsMinDurationInMillis = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEstablishedLocationsMinDurationInMillis;
		static IntPtr id_setEstablishedLocationsMinDurationInMillis_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer EstablishedLocationsMinDurationInMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='getEstablishedLocationsMinDurationInMillis' and count(parameter)=0]"
			[Register ("getEstablishedLocationsMinDurationInMillis", "()Ljava/lang/Integer;", "GetGetEstablishedLocationsMinDurationInMillisHandler")]
			get {
				if (id_getEstablishedLocationsMinDurationInMillis == IntPtr.Zero)
					id_getEstablishedLocationsMinDurationInMillis = JNIEnv.GetMethodID (class_ref, "getEstablishedLocationsMinDurationInMillis", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getEstablishedLocationsMinDurationInMillis), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEstablishedLocationsMinDurationInMillis", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setEstablishedLocationsMinDurationInMillis' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setEstablishedLocationsMinDurationInMillis", "(Ljava/lang/Integer;)V", "GetSetEstablishedLocationsMinDurationInMillis_Ljava_lang_Integer_Handler")]
			set {
				if (id_setEstablishedLocationsMinDurationInMillis_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setEstablishedLocationsMinDurationInMillis_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setEstablishedLocationsMinDurationInMillis", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEstablishedLocationsMinDurationInMillis_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEstablishedLocationsMinDurationInMillis", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getEstablishedLocationsUploadIntervalInMillis;
#pragma warning disable 0169
		static Delegate GetGetEstablishedLocationsUploadIntervalInMillisHandler ()
		{
			if (cb_getEstablishedLocationsUploadIntervalInMillis == null)
				cb_getEstablishedLocationsUploadIntervalInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEstablishedLocationsUploadIntervalInMillis);
			return cb_getEstablishedLocationsUploadIntervalInMillis;
		}

		static IntPtr n_GetEstablishedLocationsUploadIntervalInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.EstablishedLocationsUploadIntervalInMillis);
		}
#pragma warning restore 0169

		static Delegate cb_setEstablishedLocationsUploadIntervalInMillis_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetEstablishedLocationsUploadIntervalInMillis_Ljava_lang_Long_Handler ()
		{
			if (cb_setEstablishedLocationsUploadIntervalInMillis_Ljava_lang_Long_ == null)
				cb_setEstablishedLocationsUploadIntervalInMillis_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetEstablishedLocationsUploadIntervalInMillis_Ljava_lang_Long_);
			return cb_setEstablishedLocationsUploadIntervalInMillis_Ljava_lang_Long_;
		}

		static void n_SetEstablishedLocationsUploadIntervalInMillis_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.EstablishedLocationsUploadIntervalInMillis = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEstablishedLocationsUploadIntervalInMillis;
		static IntPtr id_setEstablishedLocationsUploadIntervalInMillis_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long EstablishedLocationsUploadIntervalInMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='getEstablishedLocationsUploadIntervalInMillis' and count(parameter)=0]"
			[Register ("getEstablishedLocationsUploadIntervalInMillis", "()Ljava/lang/Long;", "GetGetEstablishedLocationsUploadIntervalInMillisHandler")]
			get {
				if (id_getEstablishedLocationsUploadIntervalInMillis == IntPtr.Zero)
					id_getEstablishedLocationsUploadIntervalInMillis = JNIEnv.GetMethodID (class_ref, "getEstablishedLocationsUploadIntervalInMillis", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getEstablishedLocationsUploadIntervalInMillis), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEstablishedLocationsUploadIntervalInMillis", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setEstablishedLocationsUploadIntervalInMillis' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setEstablishedLocationsUploadIntervalInMillis", "(Ljava/lang/Long;)V", "GetSetEstablishedLocationsUploadIntervalInMillis_Ljava_lang_Long_Handler")]
			set {
				if (id_setEstablishedLocationsUploadIntervalInMillis_Ljava_lang_Long_ == IntPtr.Zero)
					id_setEstablishedLocationsUploadIntervalInMillis_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setEstablishedLocationsUploadIntervalInMillis", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEstablishedLocationsUploadIntervalInMillis_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEstablishedLocationsUploadIntervalInMillis", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getForegroundOnlyMode;
#pragma warning disable 0169
		static Delegate GetGetForegroundOnlyModeHandler ()
		{
			if (cb_getForegroundOnlyMode == null)
				cb_getForegroundOnlyMode = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetForegroundOnlyMode);
			return cb_getForegroundOnlyMode;
		}

		static IntPtr n_GetForegroundOnlyMode (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ForegroundOnlyMode);
		}
#pragma warning restore 0169

		static Delegate cb_setForegroundOnlyMode_Ljava_lang_Boolean_;
#pragma warning disable 0169
		static Delegate GetSetForegroundOnlyMode_Ljava_lang_Boolean_Handler ()
		{
			if (cb_setForegroundOnlyMode_Ljava_lang_Boolean_ == null)
				cb_setForegroundOnlyMode_Ljava_lang_Boolean_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetForegroundOnlyMode_Ljava_lang_Boolean_);
			return cb_setForegroundOnlyMode_Ljava_lang_Boolean_;
		}

		static void n_SetForegroundOnlyMode_Ljava_lang_Boolean_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Boolean p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ForegroundOnlyMode = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getForegroundOnlyMode;
		static IntPtr id_setForegroundOnlyMode_Ljava_lang_Boolean_;
		public virtual unsafe global::Java.Lang.Boolean ForegroundOnlyMode {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='getForegroundOnlyMode' and count(parameter)=0]"
			[Register ("getForegroundOnlyMode", "()Ljava/lang/Boolean;", "GetGetForegroundOnlyModeHandler")]
			get {
				if (id_getForegroundOnlyMode == IntPtr.Zero)
					id_getForegroundOnlyMode = JNIEnv.GetMethodID (class_ref, "getForegroundOnlyMode", "()Ljava/lang/Boolean;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallObjectMethod  (Handle, id_getForegroundOnlyMode), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getForegroundOnlyMode", "()Ljava/lang/Boolean;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setForegroundOnlyMode' and count(parameter)=1 and parameter[1][@type='java.lang.Boolean']]"
			[Register ("setForegroundOnlyMode", "(Ljava/lang/Boolean;)V", "GetSetForegroundOnlyMode_Ljava_lang_Boolean_Handler")]
			set {
				if (id_setForegroundOnlyMode_Ljava_lang_Boolean_ == IntPtr.Zero)
					id_setForegroundOnlyMode_Ljava_lang_Boolean_ = JNIEnv.GetMethodID (class_ref, "setForegroundOnlyMode", "(Ljava/lang/Boolean;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setForegroundOnlyMode_Ljava_lang_Boolean_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setForegroundOnlyMode", "(Ljava/lang/Boolean;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getSightingsUploadIntervalInMillis;
#pragma warning disable 0169
		static Delegate GetGetSightingsUploadIntervalInMillisHandler ()
		{
			if (cb_getSightingsUploadIntervalInMillis == null)
				cb_getSightingsUploadIntervalInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSightingsUploadIntervalInMillis);
			return cb_getSightingsUploadIntervalInMillis;
		}

		static IntPtr n_GetSightingsUploadIntervalInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.SightingsUploadIntervalInMillis);
		}
#pragma warning restore 0169

		static Delegate cb_setSightingsUploadIntervalInMillis_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetSightingsUploadIntervalInMillis_Ljava_lang_Long_Handler ()
		{
			if (cb_setSightingsUploadIntervalInMillis_Ljava_lang_Long_ == null)
				cb_setSightingsUploadIntervalInMillis_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSightingsUploadIntervalInMillis_Ljava_lang_Long_);
			return cb_setSightingsUploadIntervalInMillis_Ljava_lang_Long_;
		}

		static void n_SetSightingsUploadIntervalInMillis_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SightingsUploadIntervalInMillis = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSightingsUploadIntervalInMillis;
		static IntPtr id_setSightingsUploadIntervalInMillis_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long SightingsUploadIntervalInMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='getSightingsUploadIntervalInMillis' and count(parameter)=0]"
			[Register ("getSightingsUploadIntervalInMillis", "()Ljava/lang/Long;", "GetGetSightingsUploadIntervalInMillisHandler")]
			get {
				if (id_getSightingsUploadIntervalInMillis == IntPtr.Zero)
					id_getSightingsUploadIntervalInMillis = JNIEnv.GetMethodID (class_ref, "getSightingsUploadIntervalInMillis", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getSightingsUploadIntervalInMillis), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSightingsUploadIntervalInMillis", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setSightingsUploadIntervalInMillis' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setSightingsUploadIntervalInMillis", "(Ljava/lang/Long;)V", "GetSetSightingsUploadIntervalInMillis_Ljava_lang_Long_Handler")]
			set {
				if (id_setSightingsUploadIntervalInMillis_Ljava_lang_Long_ == IntPtr.Zero)
					id_setSightingsUploadIntervalInMillis_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setSightingsUploadIntervalInMillis", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSightingsUploadIntervalInMillis_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSightingsUploadIntervalInMillis", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getSmoothingWindow;
#pragma warning disable 0169
		static Delegate GetGetSmoothingWindowHandler ()
		{
			if (cb_getSmoothingWindow == null)
				cb_getSmoothingWindow = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSmoothingWindow);
			return cb_getSmoothingWindow;
		}

		static IntPtr n_GetSmoothingWindow (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.SmoothingWindow);
		}
#pragma warning restore 0169

		static Delegate cb_setSmoothingWindow_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetSmoothingWindow_Ljava_lang_Integer_Handler ()
		{
			if (cb_setSmoothingWindow_Ljava_lang_Integer_ == null)
				cb_setSmoothingWindow_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSmoothingWindow_Ljava_lang_Integer_);
			return cb_setSmoothingWindow_Ljava_lang_Integer_;
		}

		static void n_SetSmoothingWindow_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SmoothingWindow = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSmoothingWindow;
		static IntPtr id_setSmoothingWindow_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer SmoothingWindow {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='getSmoothingWindow' and count(parameter)=0]"
			[Register ("getSmoothingWindow", "()Ljava/lang/Integer;", "GetGetSmoothingWindowHandler")]
			get {
				if (id_getSmoothingWindow == IntPtr.Zero)
					id_getSmoothingWindow = JNIEnv.GetMethodID (class_ref, "getSmoothingWindow", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getSmoothingWindow), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSmoothingWindow", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setSmoothingWindow' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setSmoothingWindow", "(Ljava/lang/Integer;)V", "GetSetSmoothingWindow_Ljava_lang_Integer_Handler")]
			set {
				if (id_setSmoothingWindow_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setSmoothingWindow_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setSmoothingWindow", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSmoothingWindow_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSmoothingWindow", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_isAllowCollectIDFA;
#pragma warning disable 0169
		static Delegate GetIsAllowCollectIDFAHandler ()
		{
			if (cb_isAllowCollectIDFA == null)
				cb_isAllowCollectIDFA = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_IsAllowCollectIDFA);
			return cb_isAllowCollectIDFA;
		}

		static IntPtr n_IsAllowCollectIDFA (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.IsAllowCollectIDFA ());
		}
#pragma warning restore 0169

		static IntPtr id_isAllowCollectIDFA;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='isAllowCollectIDFA' and count(parameter)=0]"
		[Register ("isAllowCollectIDFA", "()Ljava/lang/Boolean;", "GetIsAllowCollectIDFAHandler")]
		public virtual unsafe global::Java.Lang.Boolean IsAllowCollectIDFA ()
		{
			if (id_isAllowCollectIDFA == IntPtr.Zero)
				id_isAllowCollectIDFA = JNIEnv.GetMethodID (class_ref, "isAllowCollectIDFA", "()Ljava/lang/Boolean;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallObjectMethod  (Handle, id_isAllowCollectIDFA), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isAllowCollectIDFA", "()Ljava/lang/Boolean;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_isAllowCommunicate;
#pragma warning disable 0169
		static Delegate GetIsAllowCommunicateHandler ()
		{
			if (cb_isAllowCommunicate == null)
				cb_isAllowCommunicate = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_IsAllowCommunicate);
			return cb_isAllowCommunicate;
		}

		static IntPtr n_IsAllowCommunicate (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.IsAllowCommunicate ());
		}
#pragma warning restore 0169

		static IntPtr id_isAllowCommunicate;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='isAllowCommunicate' and count(parameter)=0]"
		[Register ("isAllowCommunicate", "()Ljava/lang/Boolean;", "GetIsAllowCommunicateHandler")]
		public virtual unsafe global::Java.Lang.Boolean IsAllowCommunicate ()
		{
			if (id_isAllowCommunicate == IntPtr.Zero)
				id_isAllowCommunicate = JNIEnv.GetMethodID (class_ref, "isAllowCommunicate", "()Ljava/lang/Boolean;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallObjectMethod  (Handle, id_isAllowCommunicate), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isAllowCommunicate", "()Ljava/lang/Boolean;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_isAllowEstablishedLocations;
#pragma warning disable 0169
		static Delegate GetIsAllowEstablishedLocationsHandler ()
		{
			if (cb_isAllowEstablishedLocations == null)
				cb_isAllowEstablishedLocations = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_IsAllowEstablishedLocations);
			return cb_isAllowEstablishedLocations;
		}

		static IntPtr n_IsAllowEstablishedLocations (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.IsAllowEstablishedLocations ());
		}
#pragma warning restore 0169

		static IntPtr id_isAllowEstablishedLocations;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='isAllowEstablishedLocations' and count(parameter)=0]"
		[Register ("isAllowEstablishedLocations", "()Ljava/lang/Boolean;", "GetIsAllowEstablishedLocationsHandler")]
		public virtual unsafe global::Java.Lang.Boolean IsAllowEstablishedLocations ()
		{
			if (id_isAllowEstablishedLocations == IntPtr.Zero)
				id_isAllowEstablishedLocations = JNIEnv.GetMethodID (class_ref, "isAllowEstablishedLocations", "()Ljava/lang/Boolean;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallObjectMethod  (Handle, id_isAllowEstablishedLocations), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isAllowEstablishedLocations", "()Ljava/lang/Boolean;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_isAllowGeofence;
#pragma warning disable 0169
		static Delegate GetIsAllowGeofenceHandler ()
		{
			if (cb_isAllowGeofence == null)
				cb_isAllowGeofence = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_IsAllowGeofence);
			return cb_isAllowGeofence;
		}

		static IntPtr n_IsAllowGeofence (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.IsAllowGeofence ());
		}
#pragma warning restore 0169

		static IntPtr id_isAllowGeofence;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='isAllowGeofence' and count(parameter)=0]"
		[Register ("isAllowGeofence", "()Ljava/lang/Boolean;", "GetIsAllowGeofenceHandler")]
		public virtual unsafe global::Java.Lang.Boolean IsAllowGeofence ()
		{
			if (id_isAllowGeofence == IntPtr.Zero)
				id_isAllowGeofence = JNIEnv.GetMethodID (class_ref, "isAllowGeofence", "()Ljava/lang/Boolean;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallObjectMethod  (Handle, id_isAllowGeofence), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isAllowGeofence", "()Ljava/lang/Boolean;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_isAllowKitKat;
#pragma warning disable 0169
		static Delegate GetIsAllowKitKatHandler ()
		{
			if (cb_isAllowKitKat == null)
				cb_isAllowKitKat = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_IsAllowKitKat);
			return cb_isAllowKitKat;
		}

		static IntPtr n_IsAllowKitKat (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.IsAllowKitKat ());
		}
#pragma warning restore 0169

		static IntPtr id_isAllowKitKat;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='isAllowKitKat' and count(parameter)=0]"
		[Register ("isAllowKitKat", "()Ljava/lang/Boolean;", "GetIsAllowKitKatHandler")]
		public virtual unsafe global::Java.Lang.Boolean IsAllowKitKat ()
		{
			if (id_isAllowKitKat == IntPtr.Zero)
				id_isAllowKitKat = JNIEnv.GetMethodID (class_ref, "isAllowKitKat", "()Ljava/lang/Boolean;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallObjectMethod  (Handle, id_isAllowKitKat), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isAllowKitKat", "()Ljava/lang/Boolean;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_isAllowProximity;
#pragma warning disable 0169
		static Delegate GetIsAllowProximityHandler ()
		{
			if (cb_isAllowProximity == null)
				cb_isAllowProximity = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_IsAllowProximity);
			return cb_isAllowProximity;
		}

		static IntPtr n_IsAllowProximity (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.IsAllowProximity ());
		}
#pragma warning restore 0169

		static IntPtr id_isAllowProximity;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='isAllowProximity' and count(parameter)=0]"
		[Register ("isAllowProximity", "()Ljava/lang/Boolean;", "GetIsAllowProximityHandler")]
		public virtual unsafe global::Java.Lang.Boolean IsAllowProximity ()
		{
			if (id_isAllowProximity == IntPtr.Zero)
				id_isAllowProximity = JNIEnv.GetMethodID (class_ref, "isAllowProximity", "()Ljava/lang/Boolean;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallObjectMethod  (Handle, id_isAllowProximity), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isAllowProximity", "()Ljava/lang/Boolean;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_isCollectSightingsLocationData;
#pragma warning disable 0169
		static Delegate GetIsCollectSightingsLocationDataHandler ()
		{
			if (cb_isCollectSightingsLocationData == null)
				cb_isCollectSightingsLocationData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_IsCollectSightingsLocationData);
			return cb_isCollectSightingsLocationData;
		}

		static IntPtr n_IsCollectSightingsLocationData (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.IsCollectSightingsLocationData ());
		}
#pragma warning restore 0169

		static IntPtr id_isCollectSightingsLocationData;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='isCollectSightingsLocationData' and count(parameter)=0]"
		[Register ("isCollectSightingsLocationData", "()Ljava/lang/Boolean;", "GetIsCollectSightingsLocationDataHandler")]
		public virtual unsafe global::Java.Lang.Boolean IsCollectSightingsLocationData ()
		{
			if (id_isCollectSightingsLocationData == IntPtr.Zero)
				id_isCollectSightingsLocationData = JNIEnv.GetMethodID (class_ref, "isCollectSightingsLocationData", "()Ljava/lang/Boolean;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallObjectMethod  (Handle, id_isCollectSightingsLocationData), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isCollectSightingsLocationData", "()Ljava/lang/Boolean;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_isSendPlaceStateToServer;
#pragma warning disable 0169
		static Delegate GetIsSendPlaceStateToServerHandler ()
		{
			if (cb_isSendPlaceStateToServer == null)
				cb_isSendPlaceStateToServer = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_IsSendPlaceStateToServer);
			return cb_isSendPlaceStateToServer;
		}

		static IntPtr n_IsSendPlaceStateToServer (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.IsSendPlaceStateToServer ());
		}
#pragma warning restore 0169

		static IntPtr id_isSendPlaceStateToServer;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='isSendPlaceStateToServer' and count(parameter)=0]"
		[Register ("isSendPlaceStateToServer", "()Ljava/lang/Boolean;", "GetIsSendPlaceStateToServerHandler")]
		public virtual unsafe global::Java.Lang.Boolean IsSendPlaceStateToServer ()
		{
			if (id_isSendPlaceStateToServer == IntPtr.Zero)
				id_isSendPlaceStateToServer = JNIEnv.GetMethodID (class_ref, "isSendPlaceStateToServer", "()Ljava/lang/Boolean;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallObjectMethod  (Handle, id_isSendPlaceStateToServer), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isSendPlaceStateToServer", "()Ljava/lang/Boolean;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_setAllowCollectIDFA_Ljava_lang_Boolean_;
#pragma warning disable 0169
		static Delegate GetSetAllowCollectIDFA_Ljava_lang_Boolean_Handler ()
		{
			if (cb_setAllowCollectIDFA_Ljava_lang_Boolean_ == null)
				cb_setAllowCollectIDFA_Ljava_lang_Boolean_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAllowCollectIDFA_Ljava_lang_Boolean_);
			return cb_setAllowCollectIDFA_Ljava_lang_Boolean_;
		}

		static void n_SetAllowCollectIDFA_Ljava_lang_Boolean_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Boolean p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetAllowCollectIDFA (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setAllowCollectIDFA_Ljava_lang_Boolean_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setAllowCollectIDFA' and count(parameter)=1 and parameter[1][@type='java.lang.Boolean']]"
		[Register ("setAllowCollectIDFA", "(Ljava/lang/Boolean;)V", "GetSetAllowCollectIDFA_Ljava_lang_Boolean_Handler")]
		public virtual unsafe void SetAllowCollectIDFA (global::Java.Lang.Boolean p0)
		{
			if (id_setAllowCollectIDFA_Ljava_lang_Boolean_ == IntPtr.Zero)
				id_setAllowCollectIDFA_Ljava_lang_Boolean_ = JNIEnv.GetMethodID (class_ref, "setAllowCollectIDFA", "(Ljava/lang/Boolean;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setAllowCollectIDFA_Ljava_lang_Boolean_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAllowCollectIDFA", "(Ljava/lang/Boolean;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setAllowCommunicate_Ljava_lang_Boolean_;
#pragma warning disable 0169
		static Delegate GetSetAllowCommunicate_Ljava_lang_Boolean_Handler ()
		{
			if (cb_setAllowCommunicate_Ljava_lang_Boolean_ == null)
				cb_setAllowCommunicate_Ljava_lang_Boolean_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAllowCommunicate_Ljava_lang_Boolean_);
			return cb_setAllowCommunicate_Ljava_lang_Boolean_;
		}

		static void n_SetAllowCommunicate_Ljava_lang_Boolean_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Boolean p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetAllowCommunicate (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setAllowCommunicate_Ljava_lang_Boolean_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setAllowCommunicate' and count(parameter)=1 and parameter[1][@type='java.lang.Boolean']]"
		[Register ("setAllowCommunicate", "(Ljava/lang/Boolean;)V", "GetSetAllowCommunicate_Ljava_lang_Boolean_Handler")]
		public virtual unsafe void SetAllowCommunicate (global::Java.Lang.Boolean p0)
		{
			if (id_setAllowCommunicate_Ljava_lang_Boolean_ == IntPtr.Zero)
				id_setAllowCommunicate_Ljava_lang_Boolean_ = JNIEnv.GetMethodID (class_ref, "setAllowCommunicate", "(Ljava/lang/Boolean;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setAllowCommunicate_Ljava_lang_Boolean_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAllowCommunicate", "(Ljava/lang/Boolean;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setAllowEstablishedLocations_Ljava_lang_Boolean_;
#pragma warning disable 0169
		static Delegate GetSetAllowEstablishedLocations_Ljava_lang_Boolean_Handler ()
		{
			if (cb_setAllowEstablishedLocations_Ljava_lang_Boolean_ == null)
				cb_setAllowEstablishedLocations_Ljava_lang_Boolean_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAllowEstablishedLocations_Ljava_lang_Boolean_);
			return cb_setAllowEstablishedLocations_Ljava_lang_Boolean_;
		}

		static void n_SetAllowEstablishedLocations_Ljava_lang_Boolean_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Boolean p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetAllowEstablishedLocations (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setAllowEstablishedLocations_Ljava_lang_Boolean_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setAllowEstablishedLocations' and count(parameter)=1 and parameter[1][@type='java.lang.Boolean']]"
		[Register ("setAllowEstablishedLocations", "(Ljava/lang/Boolean;)V", "GetSetAllowEstablishedLocations_Ljava_lang_Boolean_Handler")]
		public virtual unsafe void SetAllowEstablishedLocations (global::Java.Lang.Boolean p0)
		{
			if (id_setAllowEstablishedLocations_Ljava_lang_Boolean_ == IntPtr.Zero)
				id_setAllowEstablishedLocations_Ljava_lang_Boolean_ = JNIEnv.GetMethodID (class_ref, "setAllowEstablishedLocations", "(Ljava/lang/Boolean;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setAllowEstablishedLocations_Ljava_lang_Boolean_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAllowEstablishedLocations", "(Ljava/lang/Boolean;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setAllowGeofence_Ljava_lang_Boolean_;
#pragma warning disable 0169
		static Delegate GetSetAllowGeofence_Ljava_lang_Boolean_Handler ()
		{
			if (cb_setAllowGeofence_Ljava_lang_Boolean_ == null)
				cb_setAllowGeofence_Ljava_lang_Boolean_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAllowGeofence_Ljava_lang_Boolean_);
			return cb_setAllowGeofence_Ljava_lang_Boolean_;
		}

		static void n_SetAllowGeofence_Ljava_lang_Boolean_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Boolean p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetAllowGeofence (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setAllowGeofence_Ljava_lang_Boolean_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setAllowGeofence' and count(parameter)=1 and parameter[1][@type='java.lang.Boolean']]"
		[Register ("setAllowGeofence", "(Ljava/lang/Boolean;)V", "GetSetAllowGeofence_Ljava_lang_Boolean_Handler")]
		public virtual unsafe void SetAllowGeofence (global::Java.Lang.Boolean p0)
		{
			if (id_setAllowGeofence_Ljava_lang_Boolean_ == IntPtr.Zero)
				id_setAllowGeofence_Ljava_lang_Boolean_ = JNIEnv.GetMethodID (class_ref, "setAllowGeofence", "(Ljava/lang/Boolean;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setAllowGeofence_Ljava_lang_Boolean_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAllowGeofence", "(Ljava/lang/Boolean;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setAllowKitKat_Ljava_lang_Boolean_;
#pragma warning disable 0169
		static Delegate GetSetAllowKitKat_Ljava_lang_Boolean_Handler ()
		{
			if (cb_setAllowKitKat_Ljava_lang_Boolean_ == null)
				cb_setAllowKitKat_Ljava_lang_Boolean_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAllowKitKat_Ljava_lang_Boolean_);
			return cb_setAllowKitKat_Ljava_lang_Boolean_;
		}

		static void n_SetAllowKitKat_Ljava_lang_Boolean_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Boolean p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetAllowKitKat (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setAllowKitKat_Ljava_lang_Boolean_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setAllowKitKat' and count(parameter)=1 and parameter[1][@type='java.lang.Boolean']]"
		[Register ("setAllowKitKat", "(Ljava/lang/Boolean;)V", "GetSetAllowKitKat_Ljava_lang_Boolean_Handler")]
		public virtual unsafe void SetAllowKitKat (global::Java.Lang.Boolean p0)
		{
			if (id_setAllowKitKat_Ljava_lang_Boolean_ == IntPtr.Zero)
				id_setAllowKitKat_Ljava_lang_Boolean_ = JNIEnv.GetMethodID (class_ref, "setAllowKitKat", "(Ljava/lang/Boolean;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setAllowKitKat_Ljava_lang_Boolean_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAllowKitKat", "(Ljava/lang/Boolean;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setAllowProximity_Ljava_lang_Boolean_;
#pragma warning disable 0169
		static Delegate GetSetAllowProximity_Ljava_lang_Boolean_Handler ()
		{
			if (cb_setAllowProximity_Ljava_lang_Boolean_ == null)
				cb_setAllowProximity_Ljava_lang_Boolean_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAllowProximity_Ljava_lang_Boolean_);
			return cb_setAllowProximity_Ljava_lang_Boolean_;
		}

		static void n_SetAllowProximity_Ljava_lang_Boolean_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Boolean p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetAllowProximity (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setAllowProximity_Ljava_lang_Boolean_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setAllowProximity' and count(parameter)=1 and parameter[1][@type='java.lang.Boolean']]"
		[Register ("setAllowProximity", "(Ljava/lang/Boolean;)V", "GetSetAllowProximity_Ljava_lang_Boolean_Handler")]
		public virtual unsafe void SetAllowProximity (global::Java.Lang.Boolean p0)
		{
			if (id_setAllowProximity_Ljava_lang_Boolean_ == IntPtr.Zero)
				id_setAllowProximity_Ljava_lang_Boolean_ = JNIEnv.GetMethodID (class_ref, "setAllowProximity", "(Ljava/lang/Boolean;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setAllowProximity_Ljava_lang_Boolean_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAllowProximity", "(Ljava/lang/Boolean;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setCollectSightingsLocationData_Ljava_lang_Boolean_;
#pragma warning disable 0169
		static Delegate GetSetCollectSightingsLocationData_Ljava_lang_Boolean_Handler ()
		{
			if (cb_setCollectSightingsLocationData_Ljava_lang_Boolean_ == null)
				cb_setCollectSightingsLocationData_Ljava_lang_Boolean_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCollectSightingsLocationData_Ljava_lang_Boolean_);
			return cb_setCollectSightingsLocationData_Ljava_lang_Boolean_;
		}

		static void n_SetCollectSightingsLocationData_Ljava_lang_Boolean_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Boolean p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetCollectSightingsLocationData (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setCollectSightingsLocationData_Ljava_lang_Boolean_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setCollectSightingsLocationData' and count(parameter)=1 and parameter[1][@type='java.lang.Boolean']]"
		[Register ("setCollectSightingsLocationData", "(Ljava/lang/Boolean;)V", "GetSetCollectSightingsLocationData_Ljava_lang_Boolean_Handler")]
		public virtual unsafe void SetCollectSightingsLocationData (global::Java.Lang.Boolean p0)
		{
			if (id_setCollectSightingsLocationData_Ljava_lang_Boolean_ == IntPtr.Zero)
				id_setCollectSightingsLocationData_Ljava_lang_Boolean_ = JNIEnv.GetMethodID (class_ref, "setCollectSightingsLocationData", "(Ljava/lang/Boolean;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setCollectSightingsLocationData_Ljava_lang_Boolean_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCollectSightingsLocationData", "(Ljava/lang/Boolean;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setSendPlaceStateToServer_Ljava_lang_Boolean_;
#pragma warning disable 0169
		static Delegate GetSetSendPlaceStateToServer_Ljava_lang_Boolean_Handler ()
		{
			if (cb_setSendPlaceStateToServer_Ljava_lang_Boolean_ == null)
				cb_setSendPlaceStateToServer_Ljava_lang_Boolean_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSendPlaceStateToServer_Ljava_lang_Boolean_);
			return cb_setSendPlaceStateToServer_Ljava_lang_Boolean_;
		}

		static void n_SetSendPlaceStateToServer_Ljava_lang_Boolean_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ApplicationConfiguration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Boolean p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Boolean> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetSendPlaceStateToServer (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setSendPlaceStateToServer_Ljava_lang_Boolean_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ApplicationConfiguration']/method[@name='setSendPlaceStateToServer' and count(parameter)=1 and parameter[1][@type='java.lang.Boolean']]"
		[Register ("setSendPlaceStateToServer", "(Ljava/lang/Boolean;)V", "GetSetSendPlaceStateToServer_Ljava_lang_Boolean_Handler")]
		public virtual unsafe void SetSendPlaceStateToServer (global::Java.Lang.Boolean p0)
		{
			if (id_setSendPlaceStateToServer_Ljava_lang_Boolean_ == IntPtr.Zero)
				id_setSendPlaceStateToServer_Ljava_lang_Boolean_ = JNIEnv.GetMethodID (class_ref, "setSendPlaceStateToServer", "(Ljava/lang/Boolean;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setSendPlaceStateToServer_Ljava_lang_Boolean_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSendPlaceStateToServer", "(Ljava/lang/Boolean;)V"), __args);
			} finally {
			}
		}

	}
}
