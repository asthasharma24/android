using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Communication {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']"
	[global::Android.Runtime.Register ("com/gimbal/internal/communication/InternalCommunication", DoNotGenerateAcw=true)]
	public partial class InternalCommunication : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/communication/InternalCommunication", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (InternalCommunication); }
		}

		protected InternalCommunication (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/constructor[@name='InternalCommunication' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe InternalCommunication ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (InternalCommunication)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAttributes;
#pragma warning disable 0169
		static Delegate GetGetAttributesHandler ()
		{
			if (cb_getAttributes == null)
				cb_getAttributes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAttributes);
			return cb_getAttributes;
		}

		static IntPtr n_GetAttributes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Internal.Places.InternalAttribute>.ToLocalJniHandle (__this.Attributes);
		}
#pragma warning restore 0169

		static Delegate cb_setAttributes_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetAttributes_Ljava_util_List_Handler ()
		{
			if (cb_setAttributes_Ljava_util_List_ == null)
				cb_setAttributes_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAttributes_Ljava_util_List_);
			return cb_setAttributes_Ljava_util_List_;
		}

		static void n_SetAttributes_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Internal.Places.InternalAttribute>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Attributes = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getAttributes;
		static IntPtr id_setAttributes_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Internal.Places.InternalAttribute> Attributes {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='getAttributes' and count(parameter)=0]"
			[Register ("getAttributes", "()Ljava/util/List;", "GetGetAttributesHandler")]
			get {
				if (id_getAttributes == IntPtr.Zero)
					id_getAttributes = JNIEnv.GetMethodID (class_ref, "getAttributes", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Internal.Places.InternalAttribute>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getAttributes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Internal.Places.InternalAttribute>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAttributes", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='setAttributes' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.internal.places.InternalAttribute&gt;']]"
			[Register ("setAttributes", "(Ljava/util/List;)V", "GetSetAttributes_Ljava_util_List_Handler")]
			set {
				if (id_setAttributes_Ljava_util_List_ == IntPtr.Zero)
					id_setAttributes_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setAttributes", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Gimbal.Internal.Places.InternalAttribute>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setAttributes_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAttributes", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getContentUrl;
#pragma warning disable 0169
		static Delegate GetGetContentUrlHandler ()
		{
			if (cb_getContentUrl == null)
				cb_getContentUrl = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetContentUrl);
			return cb_getContentUrl;
		}

		static IntPtr n_GetContentUrl (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ContentUrl);
		}
#pragma warning restore 0169

		static Delegate cb_setContentUrl_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetContentUrl_Ljava_lang_String_Handler ()
		{
			if (cb_setContentUrl_Ljava_lang_String_ == null)
				cb_setContentUrl_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetContentUrl_Ljava_lang_String_);
			return cb_setContentUrl_Ljava_lang_String_;
		}

		static void n_SetContentUrl_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ContentUrl = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getContentUrl;
		static IntPtr id_setContentUrl_Ljava_lang_String_;
		public virtual unsafe string ContentUrl {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='getContentUrl' and count(parameter)=0]"
			[Register ("getContentUrl", "()Ljava/lang/String;", "GetGetContentUrlHandler")]
			get {
				if (id_getContentUrl == IntPtr.Zero)
					id_getContentUrl = JNIEnv.GetMethodID (class_ref, "getContentUrl", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getContentUrl), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getContentUrl", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='setContentUrl' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setContentUrl", "(Ljava/lang/String;)V", "GetSetContentUrl_Ljava_lang_String_Handler")]
			set {
				if (id_setContentUrl_Ljava_lang_String_ == IntPtr.Zero)
					id_setContentUrl_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setContentUrl", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setContentUrl_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setContentUrl", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getDelayInSeconds;
#pragma warning disable 0169
		static Delegate GetGetDelayInSecondsHandler ()
		{
			if (cb_getDelayInSeconds == null)
				cb_getDelayInSeconds = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetDelayInSeconds);
			return cb_getDelayInSeconds;
		}

		static long n_GetDelayInSeconds (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DelayInSeconds;
		}
#pragma warning restore 0169

		static Delegate cb_setDelayInSeconds_J;
#pragma warning disable 0169
		static Delegate GetSetDelayInSeconds_JHandler ()
		{
			if (cb_setDelayInSeconds_J == null)
				cb_setDelayInSeconds_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetDelayInSeconds_J);
			return cb_setDelayInSeconds_J;
		}

		static void n_SetDelayInSeconds_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.DelayInSeconds = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDelayInSeconds;
		static IntPtr id_setDelayInSeconds_J;
		public virtual unsafe long DelayInSeconds {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='getDelayInSeconds' and count(parameter)=0]"
			[Register ("getDelayInSeconds", "()J", "GetGetDelayInSecondsHandler")]
			get {
				if (id_getDelayInSeconds == IntPtr.Zero)
					id_getDelayInSeconds = JNIEnv.GetMethodID (class_ref, "getDelayInSeconds", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getDelayInSeconds);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDelayInSeconds", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='setDelayInSeconds' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setDelayInSeconds", "(J)V", "GetSetDelayInSeconds_JHandler")]
			set {
				if (id_setDelayInSeconds_J == IntPtr.Zero)
					id_setDelayInSeconds_J = JNIEnv.GetMethodID (class_ref, "setDelayInSeconds", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDelayInSeconds_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDelayInSeconds", "(J)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDeliveryDate;
#pragma warning disable 0169
		static Delegate GetGetDeliveryDateHandler ()
		{
			if (cb_getDeliveryDate == null)
				cb_getDeliveryDate = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetDeliveryDate);
			return cb_getDeliveryDate;
		}

		static long n_GetDeliveryDate (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DeliveryDate;
		}
#pragma warning restore 0169

		static Delegate cb_setDeliveryDate_J;
#pragma warning disable 0169
		static Delegate GetSetDeliveryDate_JHandler ()
		{
			if (cb_setDeliveryDate_J == null)
				cb_setDeliveryDate_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetDeliveryDate_J);
			return cb_setDeliveryDate_J;
		}

		static void n_SetDeliveryDate_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.DeliveryDate = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDeliveryDate;
		static IntPtr id_setDeliveryDate_J;
		public virtual unsafe long DeliveryDate {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='getDeliveryDate' and count(parameter)=0]"
			[Register ("getDeliveryDate", "()J", "GetGetDeliveryDateHandler")]
			get {
				if (id_getDeliveryDate == IntPtr.Zero)
					id_getDeliveryDate = JNIEnv.GetMethodID (class_ref, "getDeliveryDate", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getDeliveryDate);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDeliveryDate", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='setDeliveryDate' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setDeliveryDate", "(J)V", "GetSetDeliveryDate_JHandler")]
			set {
				if (id_setDeliveryDate_J == IntPtr.Zero)
					id_setDeliveryDate_J = JNIEnv.GetMethodID (class_ref, "setDeliveryDate", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDeliveryDate_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDeliveryDate", "(J)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDescription;
#pragma warning disable 0169
		static Delegate GetGetDescriptionHandler ()
		{
			if (cb_getDescription == null)
				cb_getDescription = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDescription);
			return cb_getDescription;
		}

		static IntPtr n_GetDescription (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Description);
		}
#pragma warning restore 0169

		static Delegate cb_setDescription_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetDescription_Ljava_lang_String_Handler ()
		{
			if (cb_setDescription_Ljava_lang_String_ == null)
				cb_setDescription_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDescription_Ljava_lang_String_);
			return cb_setDescription_Ljava_lang_String_;
		}

		static void n_SetDescription_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Description = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDescription;
		static IntPtr id_setDescription_Ljava_lang_String_;
		public virtual unsafe string Description {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='getDescription' and count(parameter)=0]"
			[Register ("getDescription", "()Ljava/lang/String;", "GetGetDescriptionHandler")]
			get {
				if (id_getDescription == IntPtr.Zero)
					id_getDescription = JNIEnv.GetMethodID (class_ref, "getDescription", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getDescription), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDescription", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='setDescription' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setDescription", "(Ljava/lang/String;)V", "GetSetDescription_Ljava_lang_String_Handler")]
			set {
				if (id_setDescription_Ljava_lang_String_ == IntPtr.Zero)
					id_setDescription_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setDescription", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDescription_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDescription", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getExpiryTimeInMillis;
#pragma warning disable 0169
		static Delegate GetGetExpiryTimeInMillisHandler ()
		{
			if (cb_getExpiryTimeInMillis == null)
				cb_getExpiryTimeInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetExpiryTimeInMillis);
			return cb_getExpiryTimeInMillis;
		}

		static long n_GetExpiryTimeInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ExpiryTimeInMillis;
		}
#pragma warning restore 0169

		static Delegate cb_setExpiryTimeInMillis_J;
#pragma warning disable 0169
		static Delegate GetSetExpiryTimeInMillis_JHandler ()
		{
			if (cb_setExpiryTimeInMillis_J == null)
				cb_setExpiryTimeInMillis_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetExpiryTimeInMillis_J);
			return cb_setExpiryTimeInMillis_J;
		}

		static void n_SetExpiryTimeInMillis_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ExpiryTimeInMillis = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getExpiryTimeInMillis;
		static IntPtr id_setExpiryTimeInMillis_J;
		public virtual unsafe long ExpiryTimeInMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='getExpiryTimeInMillis' and count(parameter)=0]"
			[Register ("getExpiryTimeInMillis", "()J", "GetGetExpiryTimeInMillisHandler")]
			get {
				if (id_getExpiryTimeInMillis == IntPtr.Zero)
					id_getExpiryTimeInMillis = JNIEnv.GetMethodID (class_ref, "getExpiryTimeInMillis", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getExpiryTimeInMillis);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getExpiryTimeInMillis", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='setExpiryTimeInMillis' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setExpiryTimeInMillis", "(J)V", "GetSetExpiryTimeInMillis_JHandler")]
			set {
				if (id_setExpiryTimeInMillis_J == IntPtr.Zero)
					id_setExpiryTimeInMillis_J = JNIEnv.GetMethodID (class_ref, "setExpiryTimeInMillis", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setExpiryTimeInMillis_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setExpiryTimeInMillis", "(J)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getFrequencyLimitInHours;
#pragma warning disable 0169
		static Delegate GetGetFrequencyLimitInHoursHandler ()
		{
			if (cb_getFrequencyLimitInHours == null)
				cb_getFrequencyLimitInHours = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetFrequencyLimitInHours);
			return cb_getFrequencyLimitInHours;
		}

		static IntPtr n_GetFrequencyLimitInHours (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.FrequencyLimitInHours);
		}
#pragma warning restore 0169

		static IntPtr id_getFrequencyLimitInHours;
		public virtual unsafe global::Java.Lang.Long FrequencyLimitInHours {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='getFrequencyLimitInHours' and count(parameter)=0]"
			[Register ("getFrequencyLimitInHours", "()Ljava/lang/Long;", "GetGetFrequencyLimitInHoursHandler")]
			get {
				if (id_getFrequencyLimitInHours == IntPtr.Zero)
					id_getFrequencyLimitInHours = JNIEnv.GetMethodID (class_ref, "getFrequencyLimitInHours", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getFrequencyLimitInHours), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getFrequencyLimitInHours", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getIdentifier;
#pragma warning disable 0169
		static Delegate GetGetIdentifierHandler ()
		{
			if (cb_getIdentifier == null)
				cb_getIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIdentifier);
			return cb_getIdentifier;
		}

		static IntPtr n_GetIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Identifier);
		}
#pragma warning restore 0169

		static Delegate cb_setIdentifier_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetIdentifier_Ljava_lang_String_Handler ()
		{
			if (cb_setIdentifier_Ljava_lang_String_ == null)
				cb_setIdentifier_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetIdentifier_Ljava_lang_String_);
			return cb_setIdentifier_Ljava_lang_String_;
		}

		static void n_SetIdentifier_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Identifier = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getIdentifier;
		static IntPtr id_setIdentifier_Ljava_lang_String_;
		public virtual unsafe string Identifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='getIdentifier' and count(parameter)=0]"
			[Register ("getIdentifier", "()Ljava/lang/String;", "GetGetIdentifierHandler")]
			get {
				if (id_getIdentifier == IntPtr.Zero)
					id_getIdentifier = JNIEnv.GetMethodID (class_ref, "getIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='setIdentifier' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setIdentifier", "(Ljava/lang/String;)V", "GetSetIdentifier_Ljava_lang_String_Handler")]
			set {
				if (id_setIdentifier_Ljava_lang_String_ == IntPtr.Zero)
					id_setIdentifier_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setIdentifier", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setIdentifier_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setIdentifier", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getMetaData;
#pragma warning disable 0169
		static Delegate GetGetMetaDataHandler ()
		{
			if (cb_getMetaData == null)
				cb_getMetaData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetMetaData);
			return cb_getMetaData;
		}

		static IntPtr n_GetMetaData (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.MetaData);
		}
#pragma warning restore 0169

		static Delegate cb_setMetaData_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetMetaData_Ljava_lang_String_Handler ()
		{
			if (cb_setMetaData_Ljava_lang_String_ == null)
				cb_setMetaData_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetMetaData_Ljava_lang_String_);
			return cb_setMetaData_Ljava_lang_String_;
		}

		static void n_SetMetaData_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.MetaData = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getMetaData;
		static IntPtr id_setMetaData_Ljava_lang_String_;
		public virtual unsafe string MetaData {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='getMetaData' and count(parameter)=0]"
			[Register ("getMetaData", "()Ljava/lang/String;", "GetGetMetaDataHandler")]
			get {
				if (id_getMetaData == IntPtr.Zero)
					id_getMetaData = JNIEnv.GetMethodID (class_ref, "getMetaData", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getMetaData), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMetaData", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='setMetaData' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setMetaData", "(Ljava/lang/String;)V", "GetSetMetaData_Ljava_lang_String_Handler")]
			set {
				if (id_setMetaData_Ljava_lang_String_ == IntPtr.Zero)
					id_setMetaData_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setMetaData", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setMetaData_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setMetaData", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getTitle;
#pragma warning disable 0169
		static Delegate GetGetTitleHandler ()
		{
			if (cb_getTitle == null)
				cb_getTitle = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTitle);
			return cb_getTitle;
		}

		static IntPtr n_GetTitle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Title);
		}
#pragma warning restore 0169

		static Delegate cb_setTitle_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetTitle_Ljava_lang_String_Handler ()
		{
			if (cb_setTitle_Ljava_lang_String_ == null)
				cb_setTitle_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTitle_Ljava_lang_String_);
			return cb_setTitle_Ljava_lang_String_;
		}

		static void n_SetTitle_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Title = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTitle;
		static IntPtr id_setTitle_Ljava_lang_String_;
		public virtual unsafe string Title {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='getTitle' and count(parameter)=0]"
			[Register ("getTitle", "()Ljava/lang/String;", "GetGetTitleHandler")]
			get {
				if (id_getTitle == IntPtr.Zero)
					id_getTitle = JNIEnv.GetMethodID (class_ref, "getTitle", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getTitle), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTitle", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='setTitle' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setTitle", "(Ljava/lang/String;)V", "GetSetTitle_Ljava_lang_String_Handler")]
			set {
				if (id_setTitle_Ljava_lang_String_ == IntPtr.Zero)
					id_setTitle_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setTitle", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTitle_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTitle", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_setFrequencyLimitInHours_J;
#pragma warning disable 0169
		static Delegate GetSetFrequencyLimitInHours_JHandler ()
		{
			if (cb_setFrequencyLimitInHours_J == null)
				cb_setFrequencyLimitInHours_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetFrequencyLimitInHours_J);
			return cb_setFrequencyLimitInHours_J;
		}

		static void n_SetFrequencyLimitInHours_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Internal.Communication.InternalCommunication __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Communication.InternalCommunication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetFrequencyLimitInHours (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setFrequencyLimitInHours_J;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.communication']/class[@name='InternalCommunication']/method[@name='setFrequencyLimitInHours' and count(parameter)=1 and parameter[1][@type='long']]"
		[Register ("setFrequencyLimitInHours", "(J)V", "GetSetFrequencyLimitInHours_JHandler")]
		public virtual unsafe void SetFrequencyLimitInHours (long p0)
		{
			if (id_setFrequencyLimitInHours_J == IntPtr.Zero)
				id_setFrequencyLimitInHours_J = JNIEnv.GetMethodID (class_ref, "setFrequencyLimitInHours", "(J)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setFrequencyLimitInHours_J, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setFrequencyLimitInHours", "(J)V"), __args);
			} finally {
			}
		}

	}
}
