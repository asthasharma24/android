using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='RegisteredUser']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/RegisteredUser", DoNotGenerateAcw=true)]
	public partial class RegisteredUser : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/RegisteredUser", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (RegisteredUser); }
		}

		protected RegisteredUser (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='RegisteredUser']/constructor[@name='RegisteredUser' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe RegisteredUser ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (RegisteredUser)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getApplicationID;
#pragma warning disable 0169
		static Delegate GetGetApplicationIDHandler ()
		{
			if (cb_getApplicationID == null)
				cb_getApplicationID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApplicationID);
			return cb_getApplicationID;
		}

		static IntPtr n_GetApplicationID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.RegisteredUser __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.RegisteredUser> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ApplicationID);
		}
#pragma warning restore 0169

		static Delegate cb_setApplicationID_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetApplicationID_Ljava_lang_Long_Handler ()
		{
			if (cb_setApplicationID_Ljava_lang_Long_ == null)
				cb_setApplicationID_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApplicationID_Ljava_lang_Long_);
			return cb_setApplicationID_Ljava_lang_Long_;
		}

		static void n_SetApplicationID_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.RegisteredUser __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.RegisteredUser> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApplicationID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApplicationID;
		static IntPtr id_setApplicationID_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long ApplicationID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='RegisteredUser']/method[@name='getApplicationID' and count(parameter)=0]"
			[Register ("getApplicationID", "()Ljava/lang/Long;", "GetGetApplicationIDHandler")]
			get {
				if (id_getApplicationID == IntPtr.Zero)
					id_getApplicationID = JNIEnv.GetMethodID (class_ref, "getApplicationID", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getApplicationID), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApplicationID", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='RegisteredUser']/method[@name='setApplicationID' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setApplicationID", "(Ljava/lang/Long;)V", "GetSetApplicationID_Ljava_lang_Long_Handler")]
			set {
				if (id_setApplicationID_Ljava_lang_Long_ == IntPtr.Zero)
					id_setApplicationID_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setApplicationID", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApplicationID_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApplicationID", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getEncryptedPassword;
#pragma warning disable 0169
		static Delegate GetGetEncryptedPasswordHandler ()
		{
			if (cb_getEncryptedPassword == null)
				cb_getEncryptedPassword = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEncryptedPassword);
			return cb_getEncryptedPassword;
		}

		static IntPtr n_GetEncryptedPassword (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.RegisteredUser __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.RegisteredUser> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.EncryptedPassword);
		}
#pragma warning restore 0169

		static Delegate cb_setEncryptedPassword_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetEncryptedPassword_Ljava_lang_String_Handler ()
		{
			if (cb_setEncryptedPassword_Ljava_lang_String_ == null)
				cb_setEncryptedPassword_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetEncryptedPassword_Ljava_lang_String_);
			return cb_setEncryptedPassword_Ljava_lang_String_;
		}

		static void n_SetEncryptedPassword_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.RegisteredUser __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.RegisteredUser> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.EncryptedPassword = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEncryptedPassword;
		static IntPtr id_setEncryptedPassword_Ljava_lang_String_;
		public virtual unsafe string EncryptedPassword {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='RegisteredUser']/method[@name='getEncryptedPassword' and count(parameter)=0]"
			[Register ("getEncryptedPassword", "()Ljava/lang/String;", "GetGetEncryptedPasswordHandler")]
			get {
				if (id_getEncryptedPassword == IntPtr.Zero)
					id_getEncryptedPassword = JNIEnv.GetMethodID (class_ref, "getEncryptedPassword", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getEncryptedPassword), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEncryptedPassword", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='RegisteredUser']/method[@name='setEncryptedPassword' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setEncryptedPassword", "(Ljava/lang/String;)V", "GetSetEncryptedPassword_Ljava_lang_String_Handler")]
			set {
				if (id_setEncryptedPassword_Ljava_lang_String_ == IntPtr.Zero)
					id_setEncryptedPassword_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setEncryptedPassword", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEncryptedPassword_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEncryptedPassword", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUser;
#pragma warning disable 0169
		static Delegate GetGetUserHandler ()
		{
			if (cb_getUser == null)
				cb_getUser = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUser);
			return cb_getUser;
		}

		static IntPtr n_GetUser (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.RegisteredUser __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.RegisteredUser> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.User);
		}
#pragma warning restore 0169

		static Delegate cb_setUser_Lcom_qsl_faar_protocol_User_;
#pragma warning disable 0169
		static Delegate GetSetUser_Lcom_qsl_faar_protocol_User_Handler ()
		{
			if (cb_setUser_Lcom_qsl_faar_protocol_User_ == null)
				cb_setUser_Lcom_qsl_faar_protocol_User_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUser_Lcom_qsl_faar_protocol_User_);
			return cb_setUser_Lcom_qsl_faar_protocol_User_;
		}

		static void n_SetUser_Lcom_qsl_faar_protocol_User_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.RegisteredUser __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.RegisteredUser> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.User p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.User = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUser;
		static IntPtr id_setUser_Lcom_qsl_faar_protocol_User_;
		public virtual unsafe global::Com.Qsl.Faar.Protocol.User User {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='RegisteredUser']/method[@name='getUser' and count(parameter)=0]"
			[Register ("getUser", "()Lcom/qsl/faar/protocol/User;", "GetGetUserHandler")]
			get {
				if (id_getUser == IntPtr.Zero)
					id_getUser = JNIEnv.GetMethodID (class_ref, "getUser", "()Lcom/qsl/faar/protocol/User;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (JNIEnv.CallObjectMethod  (Handle, id_getUser), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUser", "()Lcom/qsl/faar/protocol/User;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='RegisteredUser']/method[@name='setUser' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.User']]"
			[Register ("setUser", "(Lcom/qsl/faar/protocol/User;)V", "GetSetUser_Lcom_qsl_faar_protocol_User_Handler")]
			set {
				if (id_setUser_Lcom_qsl_faar_protocol_User_ == IntPtr.Zero)
					id_setUser_Lcom_qsl_faar_protocol_User_ = JNIEnv.GetMethodID (class_ref, "setUser", "(Lcom/qsl/faar/protocol/User;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUser_Lcom_qsl_faar_protocol_User_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUser", "(Lcom/qsl/faar/protocol/User;)V"), __args);
				} finally {
				}
			}
		}

	}
}
