using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol.Established.Locations {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Circle']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/established/locations/Circle", DoNotGenerateAcw=true)]
	public partial class Circle : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/established/locations/Circle", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Circle); }
		}

		protected Circle (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Circle']/constructor[@name='Circle' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Circle ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Circle)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static IntPtr id_ctor_DDD;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Circle']/constructor[@name='Circle' and count(parameter)=3 and parameter[1][@type='double'] and parameter[2][@type='double'] and parameter[3][@type='double']]"
		[Register (".ctor", "(DDD)V", "")]
		public unsafe Circle (double p0, double p1, double p2)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				if (GetType () != typeof (Circle)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(DDD)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(DDD)V", __args);
					return;
				}

				if (id_ctor_DDD == IntPtr.Zero)
					id_ctor_DDD = JNIEnv.GetMethodID (class_ref, "<init>", "(DDD)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_DDD, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_DDD, __args);
			} finally {
			}
		}

		static Delegate cb_getCenter;
#pragma warning disable 0169
		static Delegate GetGetCenterHandler ()
		{
			if (cb_getCenter == null)
				cb_getCenter = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCenter);
			return cb_getCenter;
		}

		static IntPtr n_GetCenter (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Established.Locations.Circle __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Circle> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Center);
		}
#pragma warning restore 0169

		static Delegate cb_setCenter_Lcom_gimbal_protocol_established_locations_Point_;
#pragma warning disable 0169
		static Delegate GetSetCenter_Lcom_gimbal_protocol_established_locations_Point_Handler ()
		{
			if (cb_setCenter_Lcom_gimbal_protocol_established_locations_Point_ == null)
				cb_setCenter_Lcom_gimbal_protocol_established_locations_Point_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCenter_Lcom_gimbal_protocol_established_locations_Point_);
			return cb_setCenter_Lcom_gimbal_protocol_established_locations_Point_;
		}

		static void n_SetCenter_Lcom_gimbal_protocol_established_locations_Point_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Established.Locations.Circle __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Circle> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Protocol.Established.Locations.Point p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Point> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Center = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getCenter;
		static IntPtr id_setCenter_Lcom_gimbal_protocol_established_locations_Point_;
		public virtual unsafe global::Com.Gimbal.Protocol.Established.Locations.Point Center {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Circle']/method[@name='getCenter' and count(parameter)=0]"
			[Register ("getCenter", "()Lcom/gimbal/protocol/established/locations/Point;", "GetGetCenterHandler")]
			get {
				if (id_getCenter == IntPtr.Zero)
					id_getCenter = JNIEnv.GetMethodID (class_ref, "getCenter", "()Lcom/gimbal/protocol/established/locations/Point;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Point> (JNIEnv.CallObjectMethod  (Handle, id_getCenter), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Point> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCenter", "()Lcom/gimbal/protocol/established/locations/Point;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Circle']/method[@name='setCenter' and count(parameter)=1 and parameter[1][@type='com.gimbal.protocol.established.locations.Point']]"
			[Register ("setCenter", "(Lcom/gimbal/protocol/established/locations/Point;)V", "GetSetCenter_Lcom_gimbal_protocol_established_locations_Point_Handler")]
			set {
				if (id_setCenter_Lcom_gimbal_protocol_established_locations_Point_ == IntPtr.Zero)
					id_setCenter_Lcom_gimbal_protocol_established_locations_Point_ = JNIEnv.GetMethodID (class_ref, "setCenter", "(Lcom/gimbal/protocol/established/locations/Point;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCenter_Lcom_gimbal_protocol_established_locations_Point_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCenter", "(Lcom/gimbal/protocol/established/locations/Point;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getRadiusInMeters;
#pragma warning disable 0169
		static Delegate GetGetRadiusInMetersHandler ()
		{
			if (cb_getRadiusInMeters == null)
				cb_getRadiusInMeters = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_GetRadiusInMeters);
			return cb_getRadiusInMeters;
		}

		static double n_GetRadiusInMeters (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Established.Locations.Circle __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Circle> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.RadiusInMeters;
		}
#pragma warning restore 0169

		static Delegate cb_setRadiusInMeters_D;
#pragma warning disable 0169
		static Delegate GetSetRadiusInMeters_DHandler ()
		{
			if (cb_setRadiusInMeters_D == null)
				cb_setRadiusInMeters_D = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, double>) n_SetRadiusInMeters_D);
			return cb_setRadiusInMeters_D;
		}

		static void n_SetRadiusInMeters_D (IntPtr jnienv, IntPtr native__this, double p0)
		{
			global::Com.Gimbal.Protocol.Established.Locations.Circle __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Circle> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.RadiusInMeters = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getRadiusInMeters;
		static IntPtr id_setRadiusInMeters_D;
		public virtual unsafe double RadiusInMeters {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Circle']/method[@name='getRadiusInMeters' and count(parameter)=0]"
			[Register ("getRadiusInMeters", "()D", "GetGetRadiusInMetersHandler")]
			get {
				if (id_getRadiusInMeters == IntPtr.Zero)
					id_getRadiusInMeters = JNIEnv.GetMethodID (class_ref, "getRadiusInMeters", "()D");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallDoubleMethod  (Handle, id_getRadiusInMeters);
					else
						return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRadiusInMeters", "()D"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Circle']/method[@name='setRadiusInMeters' and count(parameter)=1 and parameter[1][@type='double']]"
			[Register ("setRadiusInMeters", "(D)V", "GetSetRadiusInMeters_DHandler")]
			set {
				if (id_setRadiusInMeters_D == IntPtr.Zero)
					id_setRadiusInMeters_D = JNIEnv.GetMethodID (class_ref, "setRadiusInMeters", "(D)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setRadiusInMeters_D, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setRadiusInMeters", "(D)V"), __args);
				} finally {
				}
			}
		}

	}
}
