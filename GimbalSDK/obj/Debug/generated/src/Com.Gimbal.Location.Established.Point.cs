using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Location.Established {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Point']"
	[global::Android.Runtime.Register ("com/gimbal/location/established/Point", DoNotGenerateAcw=true)]
	public partial class Point : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/location/established/Point", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Point); }
		}

		protected Point (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_gimbal_location_established_Point_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Point']/constructor[@name='Point' and count(parameter)=1 and parameter[1][@type='com.gimbal.location.established.Point']]"
		[Register (".ctor", "(Lcom/gimbal/location/established/Point;)V", "")]
		public unsafe Point (global::Com.Gimbal.Location.Established.Point p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (Point)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Lcom/gimbal/location/established/Point;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Lcom/gimbal/location/established/Point;)V", __args);
					return;
				}

				if (id_ctor_Lcom_gimbal_location_established_Point_ == IntPtr.Zero)
					id_ctor_Lcom_gimbal_location_established_Point_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/gimbal/location/established/Point;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_gimbal_location_established_Point_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Lcom_gimbal_location_established_Point_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Point']/constructor[@name='Point' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		protected unsafe Point ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Point)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static IntPtr id_ctor_DD;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Point']/constructor[@name='Point' and count(parameter)=2 and parameter[1][@type='double'] and parameter[2][@type='double']]"
		[Register (".ctor", "(DD)V", "")]
		public unsafe Point (double p0, double p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (GetType () != typeof (Point)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(DD)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(DD)V", __args);
					return;
				}

				if (id_ctor_DD == IntPtr.Zero)
					id_ctor_DD = JNIEnv.GetMethodID (class_ref, "<init>", "(DD)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_DD, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_DD, __args);
			} finally {
			}
		}

		static Delegate cb_getLatitude;
#pragma warning disable 0169
		static Delegate GetGetLatitudeHandler ()
		{
			if (cb_getLatitude == null)
				cb_getLatitude = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_GetLatitude);
			return cb_getLatitude;
		}

		static double n_GetLatitude (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Point __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Point> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Latitude;
		}
#pragma warning restore 0169

		static IntPtr id_getLatitude;
		public virtual unsafe double Latitude {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Point']/method[@name='getLatitude' and count(parameter)=0]"
			[Register ("getLatitude", "()D", "GetGetLatitudeHandler")]
			get {
				if (id_getLatitude == IntPtr.Zero)
					id_getLatitude = JNIEnv.GetMethodID (class_ref, "getLatitude", "()D");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallDoubleMethod  (Handle, id_getLatitude);
					else
						return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLatitude", "()D"));
				} finally {
				}
			}
		}

		static Delegate cb_getLongitude;
#pragma warning disable 0169
		static Delegate GetGetLongitudeHandler ()
		{
			if (cb_getLongitude == null)
				cb_getLongitude = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_GetLongitude);
			return cb_getLongitude;
		}

		static double n_GetLongitude (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Point __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Point> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Longitude;
		}
#pragma warning restore 0169

		static IntPtr id_getLongitude;
		public virtual unsafe double Longitude {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Point']/method[@name='getLongitude' and count(parameter)=0]"
			[Register ("getLongitude", "()D", "GetGetLongitudeHandler")]
			get {
				if (id_getLongitude == IntPtr.Zero)
					id_getLongitude = JNIEnv.GetMethodID (class_ref, "getLongitude", "()D");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallDoubleMethod  (Handle, id_getLongitude);
					else
						return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLongitude", "()D"));
				} finally {
				}
			}
		}

		static Delegate cb_metersTo_Lcom_gimbal_location_established_Point_;
#pragma warning disable 0169
		static Delegate GetMetersTo_Lcom_gimbal_location_established_Point_Handler ()
		{
			if (cb_metersTo_Lcom_gimbal_location_established_Point_ == null)
				cb_metersTo_Lcom_gimbal_location_established_Point_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, double>) n_MetersTo_Lcom_gimbal_location_established_Point_);
			return cb_metersTo_Lcom_gimbal_location_established_Point_;
		}

		static double n_MetersTo_Lcom_gimbal_location_established_Point_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Location.Established.Point __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Point> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Location.Established.Point p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Point> (native_p0, JniHandleOwnership.DoNotTransfer);
			double __ret = __this.MetersTo (p0);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_metersTo_Lcom_gimbal_location_established_Point_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Point']/method[@name='metersTo' and count(parameter)=1 and parameter[1][@type='com.gimbal.location.established.Point']]"
		[Register ("metersTo", "(Lcom/gimbal/location/established/Point;)D", "GetMetersTo_Lcom_gimbal_location_established_Point_Handler")]
		public virtual unsafe double MetersTo (global::Com.Gimbal.Location.Established.Point p0)
		{
			if (id_metersTo_Lcom_gimbal_location_established_Point_ == IntPtr.Zero)
				id_metersTo_Lcom_gimbal_location_established_Point_ = JNIEnv.GetMethodID (class_ref, "metersTo", "(Lcom/gimbal/location/established/Point;)D");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				double __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.CallDoubleMethod  (Handle, id_metersTo_Lcom_gimbal_location_established_Point_, __args);
				else
					__ret = JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "metersTo", "(Lcom/gimbal/location/established/Point;)D"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_setLatitude_D;
#pragma warning disable 0169
		static Delegate GetSetLatitude_DHandler ()
		{
			if (cb_setLatitude_D == null)
				cb_setLatitude_D = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, double>) n_SetLatitude_D);
			return cb_setLatitude_D;
		}

		static void n_SetLatitude_D (IntPtr jnienv, IntPtr native__this, double p0)
		{
			global::Com.Gimbal.Location.Established.Point __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Point> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetLatitude (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setLatitude_D;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Point']/method[@name='setLatitude' and count(parameter)=1 and parameter[1][@type='double']]"
		[Register ("setLatitude", "(D)V", "GetSetLatitude_DHandler")]
		protected virtual unsafe void SetLatitude (double p0)
		{
			if (id_setLatitude_D == IntPtr.Zero)
				id_setLatitude_D = JNIEnv.GetMethodID (class_ref, "setLatitude", "(D)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setLatitude_D, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLatitude", "(D)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setLongitude_D;
#pragma warning disable 0169
		static Delegate GetSetLongitude_DHandler ()
		{
			if (cb_setLongitude_D == null)
				cb_setLongitude_D = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, double>) n_SetLongitude_D);
			return cb_setLongitude_D;
		}

		static void n_SetLongitude_D (IntPtr jnienv, IntPtr native__this, double p0)
		{
			global::Com.Gimbal.Location.Established.Point __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Point> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SetLongitude (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setLongitude_D;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Point']/method[@name='setLongitude' and count(parameter)=1 and parameter[1][@type='double']]"
		[Register ("setLongitude", "(D)V", "GetSetLongitude_DHandler")]
		protected virtual unsafe void SetLongitude (double p0)
		{
			if (id_setLongitude_D == IntPtr.Zero)
				id_setLongitude_D = JNIEnv.GetMethodID (class_ref, "setLongitude", "(D)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setLongitude_D, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLongitude", "(D)V"), __args);
			} finally {
			}
		}

	}
}
