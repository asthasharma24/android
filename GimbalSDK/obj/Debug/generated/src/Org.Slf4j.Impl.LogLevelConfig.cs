using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Org.Slf4j.Impl {

	// Metadata.xml XPath class reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevelConfig']"
	[global::Android.Runtime.Register ("org/slf4j/impl/LogLevelConfig", DoNotGenerateAcw=true)]
	public partial class LogLevelConfig : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("org/slf4j/impl/LogLevelConfig", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (LogLevelConfig); }
		}

		protected LogLevelConfig (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevelConfig']/constructor[@name='LogLevelConfig' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe LogLevelConfig ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (LogLevelConfig)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static IntPtr id_getInstance;
		public static unsafe global::Org.Slf4j.Impl.LogLevelConfig Instance {
			// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevelConfig']/method[@name='getInstance' and count(parameter)=0]"
			[Register ("getInstance", "()Lorg/slf4j/impl/LogLevelConfig;", "GetGetInstanceHandler")]
			get {
				if (id_getInstance == IntPtr.Zero)
					id_getInstance = JNIEnv.GetStaticMethodID (class_ref, "getInstance", "()Lorg/slf4j/impl/LogLevelConfig;");
				try {
					return global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevelConfig> (JNIEnv.CallStaticObjectMethod  (class_ref, id_getInstance), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_isDebugEnabled;
#pragma warning disable 0169
		static Delegate GetIsDebugEnabledHandler ()
		{
			if (cb_isDebugEnabled == null)
				cb_isDebugEnabled = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsDebugEnabled);
			return cb_isDebugEnabled;
		}

		static bool n_IsDebugEnabled (IntPtr jnienv, IntPtr native__this)
		{
			global::Org.Slf4j.Impl.LogLevelConfig __this = global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevelConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsDebugEnabled;
		}
#pragma warning restore 0169

		static IntPtr id_isDebugEnabled;
		public virtual unsafe bool IsDebugEnabled {
			// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevelConfig']/method[@name='isDebugEnabled' and count(parameter)=0]"
			[Register ("isDebugEnabled", "()Z", "GetIsDebugEnabledHandler")]
			get {
				if (id_isDebugEnabled == IntPtr.Zero)
					id_isDebugEnabled = JNIEnv.GetMethodID (class_ref, "isDebugEnabled", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isDebugEnabled);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isDebugEnabled", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isErrorEnabled;
#pragma warning disable 0169
		static Delegate GetIsErrorEnabledHandler ()
		{
			if (cb_isErrorEnabled == null)
				cb_isErrorEnabled = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsErrorEnabled);
			return cb_isErrorEnabled;
		}

		static bool n_IsErrorEnabled (IntPtr jnienv, IntPtr native__this)
		{
			global::Org.Slf4j.Impl.LogLevelConfig __this = global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevelConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsErrorEnabled;
		}
#pragma warning restore 0169

		static IntPtr id_isErrorEnabled;
		public virtual unsafe bool IsErrorEnabled {
			// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevelConfig']/method[@name='isErrorEnabled' and count(parameter)=0]"
			[Register ("isErrorEnabled", "()Z", "GetIsErrorEnabledHandler")]
			get {
				if (id_isErrorEnabled == IntPtr.Zero)
					id_isErrorEnabled = JNIEnv.GetMethodID (class_ref, "isErrorEnabled", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isErrorEnabled);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isErrorEnabled", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isInfoEnabled;
#pragma warning disable 0169
		static Delegate GetIsInfoEnabledHandler ()
		{
			if (cb_isInfoEnabled == null)
				cb_isInfoEnabled = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsInfoEnabled);
			return cb_isInfoEnabled;
		}

		static bool n_IsInfoEnabled (IntPtr jnienv, IntPtr native__this)
		{
			global::Org.Slf4j.Impl.LogLevelConfig __this = global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevelConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsInfoEnabled;
		}
#pragma warning restore 0169

		static IntPtr id_isInfoEnabled;
		public virtual unsafe bool IsInfoEnabled {
			// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevelConfig']/method[@name='isInfoEnabled' and count(parameter)=0]"
			[Register ("isInfoEnabled", "()Z", "GetIsInfoEnabledHandler")]
			get {
				if (id_isInfoEnabled == IntPtr.Zero)
					id_isInfoEnabled = JNIEnv.GetMethodID (class_ref, "isInfoEnabled", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isInfoEnabled);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isInfoEnabled", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isTraceEnabled;
#pragma warning disable 0169
		static Delegate GetIsTraceEnabledHandler ()
		{
			if (cb_isTraceEnabled == null)
				cb_isTraceEnabled = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsTraceEnabled);
			return cb_isTraceEnabled;
		}

		static bool n_IsTraceEnabled (IntPtr jnienv, IntPtr native__this)
		{
			global::Org.Slf4j.Impl.LogLevelConfig __this = global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevelConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsTraceEnabled;
		}
#pragma warning restore 0169

		static IntPtr id_isTraceEnabled;
		public virtual unsafe bool IsTraceEnabled {
			// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevelConfig']/method[@name='isTraceEnabled' and count(parameter)=0]"
			[Register ("isTraceEnabled", "()Z", "GetIsTraceEnabledHandler")]
			get {
				if (id_isTraceEnabled == IntPtr.Zero)
					id_isTraceEnabled = JNIEnv.GetMethodID (class_ref, "isTraceEnabled", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isTraceEnabled);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isTraceEnabled", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isWarnEnabled;
#pragma warning disable 0169
		static Delegate GetIsWarnEnabledHandler ()
		{
			if (cb_isWarnEnabled == null)
				cb_isWarnEnabled = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsWarnEnabled);
			return cb_isWarnEnabled;
		}

		static bool n_IsWarnEnabled (IntPtr jnienv, IntPtr native__this)
		{
			global::Org.Slf4j.Impl.LogLevelConfig __this = global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevelConfig> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsWarnEnabled;
		}
#pragma warning restore 0169

		static IntPtr id_isWarnEnabled;
		public virtual unsafe bool IsWarnEnabled {
			// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevelConfig']/method[@name='isWarnEnabled' and count(parameter)=0]"
			[Register ("isWarnEnabled", "()Z", "GetIsWarnEnabledHandler")]
			get {
				if (id_isWarnEnabled == IntPtr.Zero)
					id_isWarnEnabled = JNIEnv.GetMethodID (class_ref, "isWarnEnabled", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isWarnEnabled);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isWarnEnabled", "()Z"));
				} finally {
				}
			}
		}

		static IntPtr id_getLogLevel;
		static IntPtr id_setLogLevel_Lorg_slf4j_impl_LogLevel_;
		public static unsafe global::Org.Slf4j.Impl.LogLevel LogLevel {
			// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevelConfig']/method[@name='getLogLevel' and count(parameter)=0]"
			[Register ("getLogLevel", "()Lorg/slf4j/impl/LogLevel;", "GetGetLogLevelHandler")]
			get {
				if (id_getLogLevel == IntPtr.Zero)
					id_getLogLevel = JNIEnv.GetStaticMethodID (class_ref, "getLogLevel", "()Lorg/slf4j/impl/LogLevel;");
				try {
					return global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevel> (JNIEnv.CallStaticObjectMethod  (class_ref, id_getLogLevel), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevelConfig']/method[@name='setLogLevel' and count(parameter)=1 and parameter[1][@type='org.slf4j.impl.LogLevel']]"
			[Register ("setLogLevel", "(Lorg/slf4j/impl/LogLevel;)V", "GetSetLogLevel_Lorg_slf4j_impl_LogLevel_Handler")]
			set {
				if (id_setLogLevel_Lorg_slf4j_impl_LogLevel_ == IntPtr.Zero)
					id_setLogLevel_Lorg_slf4j_impl_LogLevel_ = JNIEnv.GetStaticMethodID (class_ref, "setLogLevel", "(Lorg/slf4j/impl/LogLevel;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);
					JNIEnv.CallStaticVoidMethod  (class_ref, id_setLogLevel_Lorg_slf4j_impl_LogLevel_, __args);
				} finally {
				}
			}
		}

	}
}
