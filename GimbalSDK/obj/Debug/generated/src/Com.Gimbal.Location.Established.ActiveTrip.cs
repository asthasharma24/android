using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Location.Established {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveTrip']"
	[global::Android.Runtime.Register ("com/gimbal/location/established/ActiveTrip", DoNotGenerateAcw=true)]
	public partial class ActiveTrip : global::Com.Gimbal.Location.Established.Trip {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/location/established/ActiveTrip", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ActiveTrip); }
		}

		protected ActiveTrip (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveTrip']/constructor[@name='ActiveTrip' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ActiveTrip ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ActiveTrip)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getEndFix;
#pragma warning disable 0169
		static Delegate GetGetEndFixHandler ()
		{
			if (cb_getEndFix == null)
				cb_getEndFix = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEndFix);
			return cb_getEndFix;
		}

		static IntPtr n_GetEndFix (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.ActiveTrip __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ActiveTrip> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.EndFix);
		}
#pragma warning restore 0169

		static IntPtr id_getEndFix;
		public virtual unsafe global::Com.Gimbal.Location.Established.Fix EndFix {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveTrip']/method[@name='getEndFix' and count(parameter)=0]"
			[Register ("getEndFix", "()Lcom/gimbal/location/established/Fix;", "GetGetEndFixHandler")]
			get {
				if (id_getEndFix == IntPtr.Zero)
					id_getEndFix = JNIEnv.GetMethodID (class_ref, "getEndFix", "()Lcom/gimbal/location/established/Fix;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (JNIEnv.CallObjectMethod  (Handle, id_getEndFix), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEndFix", "()Lcom/gimbal/location/established/Fix;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getStartFix;
#pragma warning disable 0169
		static Delegate GetGetStartFixHandler ()
		{
			if (cb_getStartFix == null)
				cb_getStartFix = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetStartFix);
			return cb_getStartFix;
		}

		static IntPtr n_GetStartFix (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.ActiveTrip __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ActiveTrip> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.StartFix);
		}
#pragma warning restore 0169

		static IntPtr id_getStartFix;
		public virtual unsafe global::Com.Gimbal.Location.Established.Fix StartFix {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveTrip']/method[@name='getStartFix' and count(parameter)=0]"
			[Register ("getStartFix", "()Lcom/gimbal/location/established/Fix;", "GetGetStartFixHandler")]
			get {
				if (id_getStartFix == IntPtr.Zero)
					id_getStartFix = JNIEnv.GetMethodID (class_ref, "getStartFix", "()Lcom/gimbal/location/established/Fix;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (JNIEnv.CallObjectMethod  (Handle, id_getStartFix), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getStartFix", "()Lcom/gimbal/location/established/Fix;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_add_Lcom_gimbal_location_established_Fix_;
#pragma warning disable 0169
		static Delegate GetAdd_Lcom_gimbal_location_established_Fix_Handler ()
		{
			if (cb_add_Lcom_gimbal_location_established_Fix_ == null)
				cb_add_Lcom_gimbal_location_established_Fix_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_Add_Lcom_gimbal_location_established_Fix_);
			return cb_add_Lcom_gimbal_location_established_Fix_;
		}

		static void n_Add_Lcom_gimbal_location_established_Fix_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Location.Established.ActiveTrip __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ActiveTrip> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Location.Established.Fix p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Add (p0);
		}
#pragma warning restore 0169

		static IntPtr id_add_Lcom_gimbal_location_established_Fix_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveTrip']/method[@name='add' and count(parameter)=1 and parameter[1][@type='com.gimbal.location.established.Fix']]"
		[Register ("add", "(Lcom/gimbal/location/established/Fix;)V", "GetAdd_Lcom_gimbal_location_established_Fix_Handler")]
		public virtual unsafe void Add (global::Com.Gimbal.Location.Established.Fix p0)
		{
			if (id_add_Lcom_gimbal_location_established_Fix_ == IntPtr.Zero)
				id_add_Lcom_gimbal_location_established_Fix_ = JNIEnv.GetMethodID (class_ref, "add", "(Lcom/gimbal/location/established/Fix;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_add_Lcom_gimbal_location_established_Fix_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "add", "(Lcom/gimbal/location/established/Fix;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_fixes;
#pragma warning disable 0169
		static Delegate GetFixesHandler ()
		{
			if (cb_fixes == null)
				cb_fixes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_Fixes);
			return cb_fixes;
		}

		static IntPtr n_Fixes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.ActiveTrip __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ActiveTrip> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.Fix>.ToLocalJniHandle (__this.Fixes ());
		}
#pragma warning restore 0169

		static IntPtr id_fixes;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveTrip']/method[@name='fixes' and count(parameter)=0]"
		[Register ("fixes", "()Ljava/util/List;", "GetFixesHandler")]
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Location.Established.Fix> Fixes ()
		{
			if (id_fixes == IntPtr.Zero)
				id_fixes = JNIEnv.GetMethodID (class_ref, "fixes", "()Ljava/util/List;");
			try {

				if (GetType () == ThresholdType)
					return global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.Fix>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_fixes), JniHandleOwnership.TransferLocalRef);
				else
					return global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.Fix>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "fixes", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_fixesString;
#pragma warning disable 0169
		static Delegate GetFixesStringHandler ()
		{
			if (cb_fixesString == null)
				cb_fixesString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_FixesString);
			return cb_fixesString;
		}

		static IntPtr n_FixesString (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.ActiveTrip __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ActiveTrip> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.FixesString ());
		}
#pragma warning restore 0169

		static IntPtr id_fixesString;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveTrip']/method[@name='fixesString' and count(parameter)=0]"
		[Register ("fixesString", "()Ljava/lang/String;", "GetFixesStringHandler")]
		public virtual unsafe string FixesString ()
		{
			if (id_fixesString == IntPtr.Zero)
				id_fixesString = JNIEnv.GetMethodID (class_ref, "fixesString", "()Ljava/lang/String;");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_fixesString), JniHandleOwnership.TransferLocalRef);
				else
					return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "fixesString", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_size;
#pragma warning disable 0169
		static Delegate GetSizeHandler ()
		{
			if (cb_size == null)
				cb_size = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_Size);
			return cb_size;
		}

		static int n_Size (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.ActiveTrip __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ActiveTrip> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Size ();
		}
#pragma warning restore 0169

		static IntPtr id_size;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveTrip']/method[@name='size' and count(parameter)=0]"
		[Register ("size", "()I", "GetSizeHandler")]
		public virtual unsafe int Size ()
		{
			if (id_size == IntPtr.Zero)
				id_size = JNIEnv.GetMethodID (class_ref, "size", "()I");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallIntMethod  (Handle, id_size);
				else
					return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "size", "()I"));
			} finally {
			}
		}

	}
}
