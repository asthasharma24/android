using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Service.Protocol.Internal {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.internal']/class[@name='BeaconUUID']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/service/protocol/internal/BeaconUUID", DoNotGenerateAcw=true)]
	public partial class BeaconUUID : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/service/protocol/internal/BeaconUUID", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (BeaconUUID); }
		}

		protected BeaconUUID (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.internal']/class[@name='BeaconUUID']/constructor[@name='BeaconUUID' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe BeaconUUID ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (BeaconUUID)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getMajor;
#pragma warning disable 0169
		static Delegate GetGetMajorHandler ()
		{
			if (cb_getMajor == null)
				cb_getMajor = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetMajor);
			return cb_getMajor;
		}

		static int n_GetMajor (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Major;
		}
#pragma warning restore 0169

		static Delegate cb_setMajor_I;
#pragma warning disable 0169
		static Delegate GetSetMajor_IHandler ()
		{
			if (cb_setMajor_I == null)
				cb_setMajor_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetMajor_I);
			return cb_setMajor_I;
		}

		static void n_SetMajor_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Major = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getMajor;
		static IntPtr id_setMajor_I;
		public virtual unsafe int Major {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.internal']/class[@name='BeaconUUID']/method[@name='getMajor' and count(parameter)=0]"
			[Register ("getMajor", "()I", "GetGetMajorHandler")]
			get {
				if (id_getMajor == IntPtr.Zero)
					id_getMajor = JNIEnv.GetMethodID (class_ref, "getMajor", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getMajor);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMajor", "()I"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.internal']/class[@name='BeaconUUID']/method[@name='setMajor' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setMajor", "(I)V", "GetSetMajor_IHandler")]
			set {
				if (id_setMajor_I == IntPtr.Zero)
					id_setMajor_I = JNIEnv.GetMethodID (class_ref, "setMajor", "(I)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setMajor_I, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setMajor", "(I)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getMinor;
#pragma warning disable 0169
		static Delegate GetGetMinorHandler ()
		{
			if (cb_getMinor == null)
				cb_getMinor = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetMinor);
			return cb_getMinor;
		}

		static int n_GetMinor (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Minor;
		}
#pragma warning restore 0169

		static Delegate cb_setMinor_I;
#pragma warning disable 0169
		static Delegate GetSetMinor_IHandler ()
		{
			if (cb_setMinor_I == null)
				cb_setMinor_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetMinor_I);
			return cb_setMinor_I;
		}

		static void n_SetMinor_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Minor = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getMinor;
		static IntPtr id_setMinor_I;
		public virtual unsafe int Minor {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.internal']/class[@name='BeaconUUID']/method[@name='getMinor' and count(parameter)=0]"
			[Register ("getMinor", "()I", "GetGetMinorHandler")]
			get {
				if (id_getMinor == IntPtr.Zero)
					id_getMinor = JNIEnv.GetMethodID (class_ref, "getMinor", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getMinor);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMinor", "()I"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.internal']/class[@name='BeaconUUID']/method[@name='setMinor' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setMinor", "(I)V", "GetSetMinor_IHandler")]
			set {
				if (id_setMinor_I == IntPtr.Zero)
					id_setMinor_I = JNIEnv.GetMethodID (class_ref, "setMinor", "(I)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setMinor_I, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setMinor", "(I)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		static Delegate cb_setUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setUuid_Ljava_lang_String_ == null)
				cb_setUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUuid_Ljava_lang_String_);
			return cb_setUuid_Ljava_lang_String_;
		}

		static void n_SetUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Uuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUuid;
		static IntPtr id_setUuid_Ljava_lang_String_;
		public virtual unsafe string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.internal']/class[@name='BeaconUUID']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler")]
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.internal']/class[@name='BeaconUUID']/method[@name='setUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUuid", "(Ljava/lang/String;)V", "GetSetUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
