using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Json {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapper']"
	[global::Android.Runtime.Register ("com/gimbal/internal/json/JsonMapper", DoNotGenerateAcw=true)]
	public partial class JsonMapper : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/json/JsonMapper", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (JsonMapper); }
		}

		protected JsonMapper (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapper']/constructor[@name='JsonMapper' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe JsonMapper ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (JsonMapper)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_;
#pragma warning disable 0169
		static Delegate GetAddClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_Handler ()
		{
			if (cb_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_ == null)
				cb_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_AddClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_);
			return cb_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_;
		}

		static void n_AddClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.JsonMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Internal.Json.ClassHandler p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.ClassHandler> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class[] p1 = (global::Java.Lang.Class[]) JNIEnv.GetArray (native_p1, JniHandleOwnership.DoNotTransfer, typeof (global::Java.Lang.Class));
			__this.AddClassHandler (p0, p1);
			if (p1 != null)
				JNIEnv.CopyArray (p1, native_p1);
		}
#pragma warning restore 0169

		static IntPtr id_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapper']/method[@name='addClassHandler' and count(parameter)=2 and parameter[1][@type='com.gimbal.internal.json.ClassHandler'] and parameter[2][@type='java.lang.Class&lt;?&gt;...']]"
		[Register ("addClassHandler", "(Lcom/gimbal/internal/json/ClassHandler;[Ljava/lang/Class;)V", "GetAddClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_Handler")]
		public virtual unsafe void AddClassHandler (global::Com.Gimbal.Internal.Json.ClassHandler p0, params global:: Java.Lang.Class[] p1)
		{
			if (id_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_ == IntPtr.Zero)
				id_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "addClassHandler", "(Lcom/gimbal/internal/json/ClassHandler;[Ljava/lang/Class;)V");
			IntPtr native_p1 = JNIEnv.NewArray (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addClassHandler", "(Lcom/gimbal/internal/json/ClassHandler;[Ljava/lang/Class;)V"), __args);
			} finally {
				if (p1 != null) {
					JNIEnv.CopyArray (native_p1, p1);
					JNIEnv.DeleteLocalRef (native_p1);
				}
			}
		}

		static Delegate cb_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_;
#pragma warning disable 0169
		static Delegate GetAddPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_Handler ()
		{
			if (cb_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_ == null)
				cb_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_AddPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_);
			return cb_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_;
		}

		static void n_AddPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.JsonMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Internal.Json.PropertyNameMapper p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.PropertyNameMapper> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class[] p1 = (global::Java.Lang.Class[]) JNIEnv.GetArray (native_p1, JniHandleOwnership.DoNotTransfer, typeof (global::Java.Lang.Class));
			__this.AddPropertyNameMapper (p0, p1);
			if (p1 != null)
				JNIEnv.CopyArray (p1, native_p1);
		}
#pragma warning restore 0169

		static IntPtr id_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapper']/method[@name='addPropertyNameMapper' and count(parameter)=2 and parameter[1][@type='com.gimbal.internal.json.PropertyNameMapper'] and parameter[2][@type='java.lang.Class&lt;?&gt;...']]"
		[Register ("addPropertyNameMapper", "(Lcom/gimbal/internal/json/PropertyNameMapper;[Ljava/lang/Class;)V", "GetAddPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_Handler")]
		public virtual unsafe void AddPropertyNameMapper (global::Com.Gimbal.Internal.Json.PropertyNameMapper p0, params global:: Java.Lang.Class[] p1)
		{
			if (id_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_ == IntPtr.Zero)
				id_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "addPropertyNameMapper", "(Lcom/gimbal/internal/json/PropertyNameMapper;[Ljava/lang/Class;)V");
			IntPtr native_p1 = JNIEnv.NewArray (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addPropertyNameMapper", "(Lcom/gimbal/internal/json/PropertyNameMapper;[Ljava/lang/Class;)V"), __args);
			} finally {
				if (p1 != null) {
					JNIEnv.CopyArray (native_p1, p1);
					JNIEnv.DeleteLocalRef (native_p1);
				}
			}
		}

		static Delegate cb_readValue_Ljava_lang_Class_arrayB;
#pragma warning disable 0169
		static Delegate GetReadValue_Ljava_lang_Class_arrayBHandler ()
		{
			if (cb_readValue_Ljava_lang_Class_arrayB == null)
				cb_readValue_Ljava_lang_Class_arrayB = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_ReadValue_Ljava_lang_Class_arrayB);
			return cb_readValue_Ljava_lang_Class_arrayB;
		}

		static IntPtr n_ReadValue_Ljava_lang_Class_arrayB (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.JsonMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			byte[] p1 = (byte[]) JNIEnv.GetArray (native_p1, JniHandleOwnership.DoNotTransfer, typeof (byte));
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.ReadValue (p0, p1));
			if (p1 != null)
				JNIEnv.CopyArray (p1, native_p1);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_readValue_Ljava_lang_Class_arrayB;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapper']/method[@name='readValue' and count(parameter)=2 and parameter[1][@type='java.lang.Class&lt;X&gt;'] and parameter[2][@type='byte[]']]"
		[Register ("readValue", "(Ljava/lang/Class;[B)Ljava/lang/Object;", "GetReadValue_Ljava_lang_Class_arrayBHandler")]
		[global::Java.Interop.JavaTypeParameters (new string [] {"X"})]
		public virtual unsafe global::Java.Lang.Object ReadValue (global::Java.Lang.Class p0, byte[] p1)
		{
			if (id_readValue_Ljava_lang_Class_arrayB == IntPtr.Zero)
				id_readValue_Ljava_lang_Class_arrayB = JNIEnv.GetMethodID (class_ref, "readValue", "(Ljava/lang/Class;[B)Ljava/lang/Object;");
			IntPtr native_p1 = JNIEnv.NewArray (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				global::Java.Lang.Object __ret;
				if (GetType () == ThresholdType)
					__ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod  (Handle, id_readValue_Ljava_lang_Class_arrayB, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "readValue", "(Ljava/lang/Class;[B)Ljava/lang/Object;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				if (p1 != null) {
					JNIEnv.CopyArray (native_p1, p1);
					JNIEnv.DeleteLocalRef (native_p1);
				}
			}
		}

		static Delegate cb_readValue_Ljava_lang_Class_Ljava_io_InputStream_;
#pragma warning disable 0169
		static Delegate GetReadValue_Ljava_lang_Class_Ljava_io_InputStream_Handler ()
		{
			if (cb_readValue_Ljava_lang_Class_Ljava_io_InputStream_ == null)
				cb_readValue_Ljava_lang_Class_Ljava_io_InputStream_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_ReadValue_Ljava_lang_Class_Ljava_io_InputStream_);
			return cb_readValue_Ljava_lang_Class_Ljava_io_InputStream_;
		}

		static IntPtr n_ReadValue_Ljava_lang_Class_Ljava_io_InputStream_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.JsonMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			System.IO.Stream p1 = global::Android.Runtime.InputStreamInvoker.FromJniHandle (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.ReadValue (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_readValue_Ljava_lang_Class_Ljava_io_InputStream_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapper']/method[@name='readValue' and count(parameter)=2 and parameter[1][@type='java.lang.Class&lt;X&gt;'] and parameter[2][@type='java.io.InputStream']]"
		[Register ("readValue", "(Ljava/lang/Class;Ljava/io/InputStream;)Ljava/lang/Object;", "GetReadValue_Ljava_lang_Class_Ljava_io_InputStream_Handler")]
		[global::Java.Interop.JavaTypeParameters (new string [] {"X"})]
		public virtual unsafe global::Java.Lang.Object ReadValue (global::Java.Lang.Class p0, global::System.IO.Stream p1)
		{
			if (id_readValue_Ljava_lang_Class_Ljava_io_InputStream_ == IntPtr.Zero)
				id_readValue_Ljava_lang_Class_Ljava_io_InputStream_ = JNIEnv.GetMethodID (class_ref, "readValue", "(Ljava/lang/Class;Ljava/io/InputStream;)Ljava/lang/Object;");
			IntPtr native_p1 = global::Android.Runtime.InputStreamAdapter.ToLocalJniHandle (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				global::Java.Lang.Object __ret;
				if (GetType () == ThresholdType)
					__ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod  (Handle, id_readValue_Ljava_lang_Class_Ljava_io_InputStream_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "readValue", "(Ljava/lang/Class;Ljava/io/InputStream;)Ljava/lang/Object;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static Delegate cb_readValue_Ljava_lang_Class_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetReadValue_Ljava_lang_Class_Ljava_lang_String_Handler ()
		{
			if (cb_readValue_Ljava_lang_Class_Ljava_lang_String_ == null)
				cb_readValue_Ljava_lang_Class_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_ReadValue_Ljava_lang_Class_Ljava_lang_String_);
			return cb_readValue_Ljava_lang_Class_Ljava_lang_String_;
		}

		static IntPtr n_ReadValue_Ljava_lang_Class_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.JsonMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.ReadValue (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_readValue_Ljava_lang_Class_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapper']/method[@name='readValue' and count(parameter)=2 and parameter[1][@type='java.lang.Class&lt;X&gt;'] and parameter[2][@type='java.lang.String']]"
		[Register ("readValue", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;", "GetReadValue_Ljava_lang_Class_Ljava_lang_String_Handler")]
		[global::Java.Interop.JavaTypeParameters (new string [] {"X"})]
		public virtual unsafe global::Java.Lang.Object ReadValue (global::Java.Lang.Class p0, string p1)
		{
			if (id_readValue_Ljava_lang_Class_Ljava_lang_String_ == IntPtr.Zero)
				id_readValue_Ljava_lang_Class_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "readValue", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				global::Java.Lang.Object __ret;
				if (GetType () == ThresholdType)
					__ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod  (Handle, id_readValue_Ljava_lang_Class_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "readValue", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static IntPtr id_toString_Ljava_lang_Object_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapper']/method[@name='toString' and count(parameter)=2 and parameter[1][@type='java.lang.Object'] and parameter[2][@type='int']]"
		[Register ("toString", "(Ljava/lang/Object;I)Ljava/lang/String;", "")]
		public static unsafe string ToString (global::Java.Lang.Object p0, int p1)
		{
			if (id_toString_Ljava_lang_Object_I == IntPtr.Zero)
				id_toString_Ljava_lang_Object_I = JNIEnv.GetStaticMethodID (class_ref, "toString", "(Ljava/lang/Object;I)Ljava/lang/String;");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				string __ret = JNIEnv.GetString (JNIEnv.CallStaticObjectMethod  (class_ref, id_toString_Ljava_lang_Object_I, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_writeValueAsArray_Ljava_lang_Object_;
#pragma warning disable 0169
		static Delegate GetWriteValueAsArray_Ljava_lang_Object_Handler ()
		{
			if (cb_writeValueAsArray_Ljava_lang_Object_ == null)
				cb_writeValueAsArray_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_WriteValueAsArray_Ljava_lang_Object_);
			return cb_writeValueAsArray_Ljava_lang_Object_;
		}

		static IntPtr n_WriteValueAsArray_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Json.JsonMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Object p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.WriteValueAsArray (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_writeValueAsArray_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapper']/method[@name='writeValueAsArray' and count(parameter)=1 and parameter[1][@type='java.lang.Object']]"
		[Register ("writeValueAsArray", "(Ljava/lang/Object;)Lorg/json/JSONArray;", "GetWriteValueAsArray_Ljava_lang_Object_Handler")]
		public virtual unsafe global::Org.Json.JSONArray WriteValueAsArray (global::Java.Lang.Object p0)
		{
			if (id_writeValueAsArray_Ljava_lang_Object_ == IntPtr.Zero)
				id_writeValueAsArray_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "writeValueAsArray", "(Ljava/lang/Object;)Lorg/json/JSONArray;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				global::Org.Json.JSONArray __ret;
				if (GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Org.Json.JSONArray> (JNIEnv.CallObjectMethod  (Handle, id_writeValueAsArray_Ljava_lang_Object_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Org.Json.JSONArray> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeValueAsArray", "(Ljava/lang/Object;)Lorg/json/JSONArray;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_writeValueAsObject_Ljava_lang_Object_;
#pragma warning disable 0169
		static Delegate GetWriteValueAsObject_Ljava_lang_Object_Handler ()
		{
			if (cb_writeValueAsObject_Ljava_lang_Object_ == null)
				cb_writeValueAsObject_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_WriteValueAsObject_Ljava_lang_Object_);
			return cb_writeValueAsObject_Ljava_lang_Object_;
		}

		static IntPtr n_WriteValueAsObject_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Json.JsonMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Object p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.WriteValueAsObject (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_writeValueAsObject_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapper']/method[@name='writeValueAsObject' and count(parameter)=1 and parameter[1][@type='java.lang.Object']]"
		[Register ("writeValueAsObject", "(Ljava/lang/Object;)Lorg/json/JSONObject;", "GetWriteValueAsObject_Ljava_lang_Object_Handler")]
		public virtual unsafe global::Org.Json.JSONObject WriteValueAsObject (global::Java.Lang.Object p0)
		{
			if (id_writeValueAsObject_Ljava_lang_Object_ == IntPtr.Zero)
				id_writeValueAsObject_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "writeValueAsObject", "(Ljava/lang/Object;)Lorg/json/JSONObject;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				global::Org.Json.JSONObject __ret;
				if (GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Org.Json.JSONObject> (JNIEnv.CallObjectMethod  (Handle, id_writeValueAsObject_Ljava_lang_Object_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Org.Json.JSONObject> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeValueAsObject", "(Ljava/lang/Object;)Lorg/json/JSONObject;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_writeValueAsString_Ljava_lang_Object_;
#pragma warning disable 0169
		static Delegate GetWriteValueAsString_Ljava_lang_Object_Handler ()
		{
			if (cb_writeValueAsString_Ljava_lang_Object_ == null)
				cb_writeValueAsString_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_WriteValueAsString_Ljava_lang_Object_);
			return cb_writeValueAsString_Ljava_lang_Object_;
		}

		static IntPtr n_WriteValueAsString_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Json.JsonMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Object p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.WriteValueAsString (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_writeValueAsString_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapper']/method[@name='writeValueAsString' and count(parameter)=1 and parameter[1][@type='java.lang.Object']]"
		[Register ("writeValueAsString", "(Ljava/lang/Object;)Ljava/lang/String;", "GetWriteValueAsString_Ljava_lang_Object_Handler")]
		public virtual unsafe string WriteValueAsString (global::Java.Lang.Object p0)
		{
			if (id_writeValueAsString_Ljava_lang_Object_ == IntPtr.Zero)
				id_writeValueAsString_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "writeValueAsString", "(Ljava/lang/Object;)Ljava/lang/String;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				string __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_writeValueAsString_Ljava_lang_Object_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeValueAsString", "(Ljava/lang/Object;)Ljava/lang/String;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

	}
}
