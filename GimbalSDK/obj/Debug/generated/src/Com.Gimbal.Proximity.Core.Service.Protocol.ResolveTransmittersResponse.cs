using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Service.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersResponse']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/service/protocol/ResolveTransmittersResponse", DoNotGenerateAcw=true)]
	public partial class ResolveTransmittersResponse : global::Com.Gimbal.Protocol.ProtocolTransmitter {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/service/protocol/ResolveTransmittersResponse", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ResolveTransmittersResponse); }
		}

		protected ResolveTransmittersResponse (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersResponse']/constructor[@name='ResolveTransmittersResponse' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ResolveTransmittersResponse ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ResolveTransmittersResponse)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getSighting;
#pragma warning disable 0169
		static Delegate GetGetSightingHandler ()
		{
			if (cb_getSighting == null)
				cb_getSighting = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSighting);
			return cb_getSighting;
		}

		static IntPtr n_GetSighting (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Sighting);
		}
#pragma warning restore 0169

		static Delegate cb_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_;
#pragma warning disable 0169
		static Delegate GetSetSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Handler ()
		{
			if (cb_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_ == null)
				cb_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSighting_Lcom_gimbal_proximity_core_sighting_Sighting_);
			return cb_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_;
		}

		static void n_SetSighting_Lcom_gimbal_proximity_core_sighting_Sighting_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Proximity.Core.Sighting.Sighting p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.Sighting> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Sighting = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSighting;
		static IntPtr id_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_;
		public virtual unsafe global::Com.Gimbal.Proximity.Core.Sighting.Sighting Sighting {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersResponse']/method[@name='getSighting' and count(parameter)=0]"
			[Register ("getSighting", "()Lcom/gimbal/proximity/core/sighting/Sighting;", "GetGetSightingHandler")]
			get {
				if (id_getSighting == IntPtr.Zero)
					id_getSighting = JNIEnv.GetMethodID (class_ref, "getSighting", "()Lcom/gimbal/proximity/core/sighting/Sighting;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.Sighting> (JNIEnv.CallObjectMethod  (Handle, id_getSighting), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.Sighting> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSighting", "()Lcom/gimbal/proximity/core/sighting/Sighting;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersResponse']/method[@name='setSighting' and count(parameter)=1 and parameter[1][@type='com.gimbal.proximity.core.sighting.Sighting']]"
			[Register ("setSighting", "(Lcom/gimbal/proximity/core/sighting/Sighting;)V", "GetSetSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Handler")]
			set {
				if (id_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_ == IntPtr.Zero)
					id_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_ = JNIEnv.GetMethodID (class_ref, "setSighting", "(Lcom/gimbal/proximity/core/sighting/Sighting;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSighting", "(Lcom/gimbal/proximity/core/sighting/Sighting;)V"), __args);
				} finally {
				}
			}
		}

	}
}
