using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/User", DoNotGenerateAcw=true)]
	public partial class User : global::Java.Lang.Object {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/field[@name='ANONYMOUS']"
		[Register ("ANONYMOUS")]
		public const string Anonymous = (string) "ANONYMOUS";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/field[@name='REGISTERED']"
		[Register ("REGISTERED")]
		public const string Registered = (string) "REGISTERED";
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/User", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (User); }
		}

		protected User (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/constructor[@name='User' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe User ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (User)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getEmail;
#pragma warning disable 0169
		static Delegate GetGetEmailHandler ()
		{
			if (cb_getEmail == null)
				cb_getEmail = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEmail);
			return cb_getEmail;
		}

		static IntPtr n_GetEmail (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.User __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Email);
		}
#pragma warning restore 0169

		static Delegate cb_setEmail_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetEmail_Ljava_lang_String_Handler ()
		{
			if (cb_setEmail_Ljava_lang_String_ == null)
				cb_setEmail_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetEmail_Ljava_lang_String_);
			return cb_setEmail_Ljava_lang_String_;
		}

		static void n_SetEmail_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.User __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Email = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEmail;
		static IntPtr id_setEmail_Ljava_lang_String_;
		public virtual unsafe string Email {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/method[@name='getEmail' and count(parameter)=0]"
			[Register ("getEmail", "()Ljava/lang/String;", "GetGetEmailHandler")]
			get {
				if (id_getEmail == IntPtr.Zero)
					id_getEmail = JNIEnv.GetMethodID (class_ref, "getEmail", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getEmail), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEmail", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/method[@name='setEmail' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setEmail", "(Ljava/lang/String;)V", "GetSetEmail_Ljava_lang_String_Handler")]
			set {
				if (id_setEmail_Ljava_lang_String_ == IntPtr.Zero)
					id_setEmail_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setEmail", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEmail_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEmail", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.User __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_Long_Handler ()
		{
			if (cb_setId_Ljava_lang_Long_ == null)
				cb_setId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_Long_);
			return cb_setId_Ljava_lang_Long_;
		}

		static void n_SetId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.User __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/Long;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setId", "(Ljava/lang/Long;)V", "GetSetId_Ljava_lang_Long_Handler")]
			set {
				if (id_setId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPassword;
#pragma warning disable 0169
		static Delegate GetGetPasswordHandler ()
		{
			if (cb_getPassword == null)
				cb_getPassword = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPassword);
			return cb_getPassword;
		}

		static IntPtr n_GetPassword (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.User __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Password);
		}
#pragma warning restore 0169

		static Delegate cb_setPassword_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetPassword_Ljava_lang_String_Handler ()
		{
			if (cb_setPassword_Ljava_lang_String_ == null)
				cb_setPassword_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPassword_Ljava_lang_String_);
			return cb_setPassword_Ljava_lang_String_;
		}

		static void n_SetPassword_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.User __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Password = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPassword;
		static IntPtr id_setPassword_Ljava_lang_String_;
		public virtual unsafe string Password {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/method[@name='getPassword' and count(parameter)=0]"
			[Register ("getPassword", "()Ljava/lang/String;", "GetGetPasswordHandler")]
			get {
				if (id_getPassword == IntPtr.Zero)
					id_getPassword = JNIEnv.GetMethodID (class_ref, "getPassword", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getPassword), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPassword", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/method[@name='setPassword' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setPassword", "(Ljava/lang/String;)V", "GetSetPassword_Ljava_lang_String_Handler")]
			set {
				if (id_setPassword_Ljava_lang_String_ == IntPtr.Zero)
					id_setPassword_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setPassword", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPassword_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPassword", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getRole;
#pragma warning disable 0169
		static Delegate GetGetRoleHandler ()
		{
			if (cb_getRole == null)
				cb_getRole = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetRole);
			return cb_getRole;
		}

		static IntPtr n_GetRole (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.User __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Role);
		}
#pragma warning restore 0169

		static Delegate cb_setRole_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetRole_Ljava_lang_String_Handler ()
		{
			if (cb_setRole_Ljava_lang_String_ == null)
				cb_setRole_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetRole_Ljava_lang_String_);
			return cb_setRole_Ljava_lang_String_;
		}

		static void n_SetRole_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.User __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Role = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getRole;
		static IntPtr id_setRole_Ljava_lang_String_;
		public virtual unsafe string Role {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/method[@name='getRole' and count(parameter)=0]"
			[Register ("getRole", "()Ljava/lang/String;", "GetGetRoleHandler")]
			get {
				if (id_getRole == IntPtr.Zero)
					id_getRole = JNIEnv.GetMethodID (class_ref, "getRole", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getRole), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRole", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/method[@name='setRole' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setRole", "(Ljava/lang/String;)V", "GetSetRole_Ljava_lang_String_Handler")]
			set {
				if (id_setRole_Ljava_lang_String_ == IntPtr.Zero)
					id_setRole_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setRole", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setRole_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setRole", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_amIAnonymous;
#pragma warning disable 0169
		static Delegate GetAmIAnonymousHandler ()
		{
			if (cb_amIAnonymous == null)
				cb_amIAnonymous = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_AmIAnonymous);
			return cb_amIAnonymous;
		}

		static bool n_AmIAnonymous (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.User __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.AmIAnonymous ();
		}
#pragma warning restore 0169

		static IntPtr id_amIAnonymous;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/method[@name='amIAnonymous' and count(parameter)=0]"
		[Register ("amIAnonymous", "()Z", "GetAmIAnonymousHandler")]
		public virtual unsafe bool AmIAnonymous ()
		{
			if (id_amIAnonymous == IntPtr.Zero)
				id_amIAnonymous = JNIEnv.GetMethodID (class_ref, "amIAnonymous", "()Z");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallBooleanMethod  (Handle, id_amIAnonymous);
				else
					return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "amIAnonymous", "()Z"));
			} finally {
			}
		}

		static Delegate cb_amIRegistered;
#pragma warning disable 0169
		static Delegate GetAmIRegisteredHandler ()
		{
			if (cb_amIRegistered == null)
				cb_amIRegistered = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_AmIRegistered);
			return cb_amIRegistered;
		}

		static bool n_AmIRegistered (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.User __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.AmIRegistered ();
		}
#pragma warning restore 0169

		static IntPtr id_amIRegistered;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/method[@name='amIRegistered' and count(parameter)=0]"
		[Register ("amIRegistered", "()Z", "GetAmIRegisteredHandler")]
		public virtual unsafe bool AmIRegistered ()
		{
			if (id_amIRegistered == IntPtr.Zero)
				id_amIRegistered = JNIEnv.GetMethodID (class_ref, "amIRegistered", "()Z");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallBooleanMethod  (Handle, id_amIRegistered);
				else
					return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "amIRegistered", "()Z"));
			} finally {
			}
		}

		static Delegate cb_cloneUser;
#pragma warning disable 0169
		static Delegate GetCloneUserHandler ()
		{
			if (cb_cloneUser == null)
				cb_cloneUser = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_CloneUser);
			return cb_cloneUser;
		}

		static IntPtr n_CloneUser (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.User __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.CloneUser ());
		}
#pragma warning restore 0169

		static IntPtr id_cloneUser;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/method[@name='cloneUser' and count(parameter)=0]"
		[Register ("cloneUser", "()Lcom/qsl/faar/protocol/User;", "GetCloneUserHandler")]
		public virtual unsafe global::Com.Qsl.Faar.Protocol.User CloneUser ()
		{
			if (id_cloneUser == IntPtr.Zero)
				id_cloneUser = JNIEnv.GetMethodID (class_ref, "cloneUser", "()Lcom/qsl/faar/protocol/User;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (JNIEnv.CallObjectMethod  (Handle, id_cloneUser), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "cloneUser", "()Lcom/qsl/faar/protocol/User;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_setOrganizations_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetOrganizations_Ljava_util_List_Handler ()
		{
			if (cb_setOrganizations_Ljava_util_List_ == null)
				cb_setOrganizations_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizations_Ljava_util_List_);
			return cb_setOrganizations_Ljava_util_List_;
		}

		static void n_SetOrganizations_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.User __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.User> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Java.Lang.Long>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetOrganizations (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setOrganizations_Ljava_util_List_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='User']/method[@name='setOrganizations' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;java.lang.Long&gt;']]"
		[Obsolete (@"deprecated")]
		[Register ("setOrganizations", "(Ljava/util/List;)V", "GetSetOrganizations_Ljava_util_List_Handler")]
		public virtual unsafe void SetOrganizations (global::System.Collections.Generic.IList<global::Java.Lang.Long> p0)
		{
			if (id_setOrganizations_Ljava_util_List_ == IntPtr.Zero)
				id_setOrganizations_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setOrganizations", "(Ljava/util/List;)V");
			IntPtr native_p0 = global::Android.Runtime.JavaList<global::Java.Lang.Long>.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setOrganizations_Ljava_util_List_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizations", "(Ljava/util/List;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
