using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Location.Established {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Visit']"
	[global::Android.Runtime.Register ("com/gimbal/location/established/Visit", DoNotGenerateAcw=true)]
	public partial class Visit : global::Com.Gimbal.Location.Established.Locatable {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/location/established/Visit", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Visit); }
		}

		protected Visit (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_gimbal_location_established_Visit_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Visit']/constructor[@name='Visit' and count(parameter)=1 and parameter[1][@type='com.gimbal.location.established.Visit']]"
		[Register (".ctor", "(Lcom/gimbal/location/established/Visit;)V", "")]
		public unsafe Visit (global::Com.Gimbal.Location.Established.Visit p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (Visit)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Lcom/gimbal/location/established/Visit;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Lcom/gimbal/location/established/Visit;)V", __args);
					return;
				}

				if (id_ctor_Lcom_gimbal_location_established_Visit_ == IntPtr.Zero)
					id_ctor_Lcom_gimbal_location_established_Visit_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/gimbal/location/established/Visit;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_gimbal_location_established_Visit_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Lcom_gimbal_location_established_Visit_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Visit']/constructor[@name='Visit' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Visit ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Visit)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getLocation;
#pragma warning disable 0169
		static Delegate GetGetLocationHandler ()
		{
			if (cb_getLocation == null)
				cb_getLocation = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLocation);
			return cb_getLocation;
		}

		static IntPtr n_GetLocation (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Visit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Visit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Location);
		}
#pragma warning restore 0169

		static IntPtr id_getLocation;
		public virtual unsafe global::Com.Gimbal.Location.Established.Centroid Location {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Visit']/method[@name='getLocation' and count(parameter)=0]"
			[Register ("getLocation", "()Lcom/gimbal/location/established/Centroid;", "GetGetLocationHandler")]
			get {
				if (id_getLocation == IntPtr.Zero)
					id_getLocation = JNIEnv.GetMethodID (class_ref, "getLocation", "()Lcom/gimbal/location/established/Centroid;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Centroid> (JNIEnv.CallObjectMethod  (Handle, id_getLocation), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Centroid> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLocation", "()Lcom/gimbal/location/established/Centroid;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_createId_Ljava_lang_Class_;
#pragma warning disable 0169
		static Delegate GetCreateId_Ljava_lang_Class_Handler ()
		{
			if (cb_createId_Ljava_lang_Class_ == null)
				cb_createId_Ljava_lang_Class_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_CreateId_Ljava_lang_Class_);
			return cb_createId_Ljava_lang_Class_;
		}

		static IntPtr n_CreateId_Ljava_lang_Class_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Location.Established.Visit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Visit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.CreateId (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_createId_Ljava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Visit']/method[@name='createId' and count(parameter)=1 and parameter[1][@type='java.lang.Class&lt;?&gt;']]"
		[Register ("createId", "(Ljava/lang/Class;)Ljava/lang/String;", "GetCreateId_Ljava_lang_Class_Handler")]
		protected override unsafe string CreateId (global::Java.Lang.Class p0)
		{
			if (id_createId_Ljava_lang_Class_ == IntPtr.Zero)
				id_createId_Ljava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "createId", "(Ljava/lang/Class;)Ljava/lang/String;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				string __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_createId_Ljava_lang_Class_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "createId", "(Ljava/lang/Class;)Ljava/lang/String;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

	}
}
