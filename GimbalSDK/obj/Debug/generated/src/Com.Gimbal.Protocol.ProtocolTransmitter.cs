using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/ProtocolTransmitter", DoNotGenerateAcw=true)]
	public partial class ProtocolTransmitter : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/ProtocolTransmitter", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ProtocolTransmitter); }
		}

		protected ProtocolTransmitter (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/constructor[@name='ProtocolTransmitter' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ProtocolTransmitter ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ProtocolTransmitter)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getDataMasks;
#pragma warning disable 0169
		static Delegate GetGetDataMasksHandler ()
		{
			if (cb_getDataMasks == null)
				cb_getDataMasks = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDataMasks);
			return cb_getDataMasks;
		}

		static IntPtr n_GetDataMasks (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<string>.ToLocalJniHandle (__this.DataMasks);
		}
#pragma warning restore 0169

		static Delegate cb_setDataMasks_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetDataMasks_Ljava_util_List_Handler ()
		{
			if (cb_setDataMasks_Ljava_util_List_ == null)
				cb_setDataMasks_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDataMasks_Ljava_util_List_);
			return cb_setDataMasks_Ljava_util_List_;
		}

		static void n_SetDataMasks_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<string>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DataMasks = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDataMasks;
		static IntPtr id_setDataMasks_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<string> DataMasks {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='getDataMasks' and count(parameter)=0]"
			[Register ("getDataMasks", "()Ljava/util/List;", "GetGetDataMasksHandler")]
			get {
				if (id_getDataMasks == IntPtr.Zero)
					id_getDataMasks = JNIEnv.GetMethodID (class_ref, "getDataMasks", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<string>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getDataMasks), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<string>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDataMasks", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='setDataMasks' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;java.lang.String&gt;']]"
			[Register ("setDataMasks", "(Ljava/util/List;)V", "GetSetDataMasks_Ljava_util_List_Handler")]
			set {
				if (id_setDataMasks_Ljava_util_List_ == IntPtr.Zero)
					id_setDataMasks_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setDataMasks", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<string>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDataMasks_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDataMasks", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getExternalUuid;
#pragma warning disable 0169
		static Delegate GetGetExternalUuidHandler ()
		{
			if (cb_getExternalUuid == null)
				cb_getExternalUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetExternalUuid);
			return cb_getExternalUuid;
		}

		static IntPtr n_GetExternalUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ExternalUuid);
		}
#pragma warning restore 0169

		static Delegate cb_setExternalUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetExternalUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setExternalUuid_Ljava_lang_String_ == null)
				cb_setExternalUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetExternalUuid_Ljava_lang_String_);
			return cb_setExternalUuid_Ljava_lang_String_;
		}

		static void n_SetExternalUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ExternalUuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getExternalUuid;
		static IntPtr id_setExternalUuid_Ljava_lang_String_;
		public virtual unsafe string ExternalUuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='getExternalUuid' and count(parameter)=0]"
			[Register ("getExternalUuid", "()Ljava/lang/String;", "GetGetExternalUuidHandler")]
			get {
				if (id_getExternalUuid == IntPtr.Zero)
					id_getExternalUuid = JNIEnv.GetMethodID (class_ref, "getExternalUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getExternalUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getExternalUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='setExternalUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setExternalUuid", "(Ljava/lang/String;)V", "GetSetExternalUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setExternalUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setExternalUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setExternalUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setExternalUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setExternalUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getLookupKeys;
#pragma warning disable 0169
		static Delegate GetGetLookupKeysHandler ()
		{
			if (cb_getLookupKeys == null)
				cb_getLookupKeys = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLookupKeys);
			return cb_getLookupKeys;
		}

		static IntPtr n_GetLookupKeys (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<string>.ToLocalJniHandle (__this.LookupKeys);
		}
#pragma warning restore 0169

		static Delegate cb_setLookupKeys_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetLookupKeys_Ljava_util_List_Handler ()
		{
			if (cb_setLookupKeys_Ljava_util_List_ == null)
				cb_setLookupKeys_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLookupKeys_Ljava_util_List_);
			return cb_setLookupKeys_Ljava_util_List_;
		}

		static void n_SetLookupKeys_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<string>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.LookupKeys = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLookupKeys;
		static IntPtr id_setLookupKeys_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<string> LookupKeys {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='getLookupKeys' and count(parameter)=0]"
			[Register ("getLookupKeys", "()Ljava/util/List;", "GetGetLookupKeysHandler")]
			get {
				if (id_getLookupKeys == IntPtr.Zero)
					id_getLookupKeys = JNIEnv.GetMethodID (class_ref, "getLookupKeys", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<string>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getLookupKeys), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<string>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLookupKeys", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='setLookupKeys' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;java.lang.String&gt;']]"
			[Register ("setLookupKeys", "(Ljava/util/List;)V", "GetSetLookupKeys_Ljava_util_List_Handler")]
			set {
				if (id_setLookupKeys_Ljava_util_List_ == IntPtr.Zero)
					id_setLookupKeys_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setLookupKeys", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<string>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLookupKeys_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLookupKeys", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getName;
#pragma warning disable 0169
		static Delegate GetGetNameHandler ()
		{
			if (cb_getName == null)
				cb_getName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetName);
			return cb_getName;
		}

		static IntPtr n_GetName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Name);
		}
#pragma warning restore 0169

		static Delegate cb_setName_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetName_Ljava_lang_String_Handler ()
		{
			if (cb_setName_Ljava_lang_String_ == null)
				cb_setName_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetName_Ljava_lang_String_);
			return cb_setName_Ljava_lang_String_;
		}

		static void n_SetName_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Name = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getName;
		static IntPtr id_setName_Ljava_lang_String_;
		public virtual unsafe string Name {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='getName' and count(parameter)=0]"
			[Register ("getName", "()Ljava/lang/String;", "GetGetNameHandler")]
			get {
				if (id_getName == IntPtr.Zero)
					id_getName = JNIEnv.GetMethodID (class_ref, "getName", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getName), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getName", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='setName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setName", "(Ljava/lang/String;)V", "GetSetName_Ljava_lang_String_Handler")]
			set {
				if (id_setName_Ljava_lang_String_ == IntPtr.Zero)
					id_setName_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setName", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setName_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setName", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getOwnerId;
#pragma warning disable 0169
		static Delegate GetGetOwnerIdHandler ()
		{
			if (cb_getOwnerId == null)
				cb_getOwnerId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOwnerId);
			return cb_getOwnerId;
		}

		static IntPtr n_GetOwnerId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.OwnerId);
		}
#pragma warning restore 0169

		static Delegate cb_setOwnerId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetOwnerId_Ljava_lang_String_Handler ()
		{
			if (cb_setOwnerId_Ljava_lang_String_ == null)
				cb_setOwnerId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOwnerId_Ljava_lang_String_);
			return cb_setOwnerId_Ljava_lang_String_;
		}

		static void n_SetOwnerId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OwnerId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOwnerId;
		static IntPtr id_setOwnerId_Ljava_lang_String_;
		public virtual unsafe string OwnerId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='getOwnerId' and count(parameter)=0]"
			[Register ("getOwnerId", "()Ljava/lang/String;", "GetGetOwnerIdHandler")]
			get {
				if (id_getOwnerId == IntPtr.Zero)
					id_getOwnerId = JNIEnv.GetMethodID (class_ref, "getOwnerId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getOwnerId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOwnerId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='setOwnerId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setOwnerId", "(Ljava/lang/String;)V", "GetSetOwnerId_Ljava_lang_String_Handler")]
			set {
				if (id_setOwnerId_Ljava_lang_String_ == IntPtr.Zero)
					id_setOwnerId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setOwnerId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOwnerId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOwnerId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getPlaces;
#pragma warning disable 0169
		static Delegate GetGetPlacesHandler ()
		{
			if (cb_getPlaces == null)
				cb_getPlaces = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlaces);
			return cb_getPlaces;
		}

		static IntPtr n_GetPlaces (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.ProtocolPlace>.ToLocalJniHandle (__this.Places);
		}
#pragma warning restore 0169

		static Delegate cb_setPlaces_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetPlaces_Ljava_util_List_Handler ()
		{
			if (cb_setPlaces_Ljava_util_List_ == null)
				cb_setPlaces_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPlaces_Ljava_util_List_);
			return cb_setPlaces_Ljava_util_List_;
		}

		static void n_SetPlaces_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.ProtocolPlace>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Places = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPlaces;
		static IntPtr id_setPlaces_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Protocol.ProtocolPlace> Places {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='getPlaces' and count(parameter)=0]"
			[Register ("getPlaces", "()Ljava/util/List;", "GetGetPlacesHandler")]
			get {
				if (id_getPlaces == IntPtr.Zero)
					id_getPlaces = JNIEnv.GetMethodID (class_ref, "getPlaces", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.ProtocolPlace>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getPlaces), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.ProtocolPlace>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlaces", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='setPlaces' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.protocol.ProtocolPlace&gt;']]"
			[Register ("setPlaces", "(Ljava/util/List;)V", "GetSetPlaces_Ljava_util_List_Handler")]
			set {
				if (id_setPlaces_Ljava_util_List_ == IntPtr.Zero)
					id_setPlaces_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setPlaces", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.ProtocolPlace>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPlaces_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPlaces", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		static Delegate cb_setUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setUuid_Ljava_lang_String_ == null)
				cb_setUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUuid_Ljava_lang_String_);
			return cb_setUuid_Ljava_lang_String_;
		}

		static void n_SetUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Uuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUuid;
		static IntPtr id_setUuid_Ljava_lang_String_;
		public virtual unsafe string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler")]
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='setUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUuid", "(Ljava/lang/String;)V", "GetSetUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getVisibility;
#pragma warning disable 0169
		static Delegate GetGetVisibilityHandler ()
		{
			if (cb_getVisibility == null)
				cb_getVisibility = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetVisibility);
			return cb_getVisibility;
		}

		static int n_GetVisibility (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Visibility;
		}
#pragma warning restore 0169

		static Delegate cb_setVisibility_I;
#pragma warning disable 0169
		static Delegate GetSetVisibility_IHandler ()
		{
			if (cb_setVisibility_I == null)
				cb_setVisibility_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetVisibility_I);
			return cb_setVisibility_I;
		}

		static void n_SetVisibility_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Gimbal.Protocol.ProtocolTransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolTransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Visibility = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getVisibility;
		static IntPtr id_setVisibility_I;
		public virtual unsafe int Visibility {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='getVisibility' and count(parameter)=0]"
			[Register ("getVisibility", "()I", "GetGetVisibilityHandler")]
			get {
				if (id_getVisibility == IntPtr.Zero)
					id_getVisibility = JNIEnv.GetMethodID (class_ref, "getVisibility", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getVisibility);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getVisibility", "()I"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolTransmitter']/method[@name='setVisibility' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setVisibility", "(I)V", "GetSetVisibility_IHandler")]
			set {
				if (id_setVisibility_I == IntPtr.Zero)
					id_setVisibility_I = JNIEnv.GetMethodID (class_ref, "setVisibility", "(I)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setVisibility_I, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setVisibility", "(I)V"), __args);
				} finally {
				}
			}
		}

	}
}
