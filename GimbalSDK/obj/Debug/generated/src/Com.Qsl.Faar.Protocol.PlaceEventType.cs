using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceEventType']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/PlaceEventType", DoNotGenerateAcw=true)]
	public sealed partial class PlaceEventType : global::Java.Lang.Enum {


		static IntPtr AT_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceEventType']/field[@name='AT']"
		[Register ("AT")]
		public static global::Com.Qsl.Faar.Protocol.PlaceEventType At {
			get {
				if (AT_jfieldId == IntPtr.Zero)
					AT_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "AT", "Lcom/qsl/faar/protocol/PlaceEventType;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, AT_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PlaceEventType> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr LEFT_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceEventType']/field[@name='LEFT']"
		[Register ("LEFT")]
		public static global::Com.Qsl.Faar.Protocol.PlaceEventType Left {
			get {
				if (LEFT_jfieldId == IntPtr.Zero)
					LEFT_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "LEFT", "Lcom/qsl/faar/protocol/PlaceEventType;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, LEFT_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PlaceEventType> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr NEAR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceEventType']/field[@name='NEAR']"
		[Register ("NEAR")]
		public static global::Com.Qsl.Faar.Protocol.PlaceEventType Near {
			get {
				if (NEAR_jfieldId == IntPtr.Zero)
					NEAR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "NEAR", "Lcom/qsl/faar/protocol/PlaceEventType;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, NEAR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PlaceEventType> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr UNKNOWN_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceEventType']/field[@name='UNKNOWN']"
		[Register ("UNKNOWN")]
		public static global::Com.Qsl.Faar.Protocol.PlaceEventType Unknown {
			get {
				if (UNKNOWN_jfieldId == IntPtr.Zero)
					UNKNOWN_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "UNKNOWN", "Lcom/qsl/faar/protocol/PlaceEventType;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, UNKNOWN_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PlaceEventType> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/PlaceEventType", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PlaceEventType); }
		}

		internal PlaceEventType (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_valueByGrammerType_C;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceEventType']/method[@name='valueByGrammerType' and count(parameter)=1 and parameter[1][@type='char']]"
		[Register ("valueByGrammerType", "(C)Lcom/qsl/faar/protocol/PlaceEventType;", "")]
		public static unsafe global::Com.Qsl.Faar.Protocol.PlaceEventType ValueByGrammerType (char p0)
		{
			if (id_valueByGrammerType_C == IntPtr.Zero)
				id_valueByGrammerType_C = JNIEnv.GetStaticMethodID (class_ref, "valueByGrammerType", "(C)Lcom/qsl/faar/protocol/PlaceEventType;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PlaceEventType> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueByGrammerType_C, __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_valueOf_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceEventType']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/PlaceEventType;", "")]
		public static unsafe global::Com.Qsl.Faar.Protocol.PlaceEventType ValueOf (string p0)
		{
			if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
				id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/PlaceEventType;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				global::Com.Qsl.Faar.Protocol.PlaceEventType __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PlaceEventType> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_values;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceEventType']/method[@name='values' and count(parameter)=0]"
		[Register ("values", "()[Lcom/qsl/faar/protocol/PlaceEventType;", "")]
		public static unsafe global::Com.Qsl.Faar.Protocol.PlaceEventType[] Values ()
		{
			if (id_values == IntPtr.Zero)
				id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/PlaceEventType;");
			try {
				return (global::Com.Qsl.Faar.Protocol.PlaceEventType[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.PlaceEventType));
			} finally {
			}
		}

	}
}
