using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Receiver {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.receiver']/class[@name='GimbalBroadcastReceiver']"
	[global::Android.Runtime.Register ("com/gimbal/internal/receiver/GimbalBroadcastReceiver", DoNotGenerateAcw=true)]
	public abstract partial class GimbalBroadcastReceiver : global::Android.Content.BroadcastReceiver {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/receiver/GimbalBroadcastReceiver", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (GimbalBroadcastReceiver); }
		}

		protected GimbalBroadcastReceiver (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_a_Ljava_lang_String_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.receiver']/class[@name='GimbalBroadcastReceiver']/method[@name='a' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.Object']]"
		[Register ("a", "(Ljava/lang/String;Ljava/lang/Object;)V", "")]
		public unsafe void A (string p0, global::Java.Lang.Object p1)
		{
			if (id_a_Ljava_lang_String_Ljava_lang_Object_ == IntPtr.Zero)
				id_a_Ljava_lang_String_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "a", "(Ljava/lang/String;Ljava/lang/Object;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallVoidMethod  (Handle, id_a_Ljava_lang_String_Ljava_lang_Object_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_b_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.receiver']/class[@name='GimbalBroadcastReceiver']/method[@name='b_' and count(parameter)=0]"
		[Register ("b_", "()V", "")]
		public unsafe void B_ ()
		{
			if (id_b_ == IntPtr.Zero)
				id_b_ = JNIEnv.GetMethodID (class_ref, "b_", "()V");
			try {
				JNIEnv.CallVoidMethod  (Handle, id_b_);
			} finally {
			}
		}

		static IntPtr id_c;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.receiver']/class[@name='GimbalBroadcastReceiver']/method[@name='c' and count(parameter)=0]"
		[Register ("c", "()V", "")]
		public unsafe void C ()
		{
			if (id_c == IntPtr.Zero)
				id_c = JNIEnv.GetMethodID (class_ref, "c", "()V");
			try {
				JNIEnv.CallVoidMethod  (Handle, id_c);
			} finally {
			}
		}

	}

	[global::Android.Runtime.Register ("com/gimbal/internal/receiver/GimbalBroadcastReceiver", DoNotGenerateAcw=true)]
	internal partial class GimbalBroadcastReceiverInvoker : GimbalBroadcastReceiver {

		public GimbalBroadcastReceiverInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (GimbalBroadcastReceiverInvoker); }
		}

		static IntPtr id_onReceive_Landroid_content_Context_Landroid_content_Intent_;
		// Metadata.xml XPath method reference: path="/api/package[@name='android.content']/class[@name='BroadcastReceiver']/method[@name='onReceive' and count(parameter)=2 and parameter[1][@type='Android.Content.Context'] and parameter[2][@type='Android.Content.Intent']]"
		[Register ("onReceive", "(Landroid/content/Context;Landroid/content/Intent;)V", "GetOnReceive_Landroid_content_Context_Landroid_content_Intent_Handler")]
		public override unsafe void OnReceive (global::Android.Content.Context context, global::Android.Content.Intent intent)
		{
			if (id_onReceive_Landroid_content_Context_Landroid_content_Intent_ == IntPtr.Zero)
				id_onReceive_Landroid_content_Context_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "onReceive", "(Landroid/content/Context;Landroid/content/Intent;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (context);
				__args [1] = new JValue (intent);
				JNIEnv.CallVoidMethod  (Handle, id_onReceive_Landroid_content_Context_Landroid_content_Intent_, __args);
			} finally {
			}
		}

	}

}
