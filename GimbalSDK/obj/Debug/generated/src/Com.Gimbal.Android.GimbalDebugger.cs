using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='GimbalDebugger']"
	[global::Android.Runtime.Register ("com/gimbal/android/GimbalDebugger", DoNotGenerateAcw=true)]
	public partial class GimbalDebugger : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/GimbalDebugger", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (GimbalDebugger); }
		}

		protected GimbalDebugger (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android']/class[@name='GimbalDebugger']/constructor[@name='GimbalDebugger' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe GimbalDebugger ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (GimbalDebugger)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static IntPtr id_isBeaconSightingsLoggingEnabled;
		public static unsafe bool IsBeaconSightingsLoggingEnabled {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='GimbalDebugger']/method[@name='isBeaconSightingsLoggingEnabled' and count(parameter)=0]"
			[Register ("isBeaconSightingsLoggingEnabled", "()Z", "GetIsBeaconSightingsLoggingEnabledHandler")]
			get {
				if (id_isBeaconSightingsLoggingEnabled == IntPtr.Zero)
					id_isBeaconSightingsLoggingEnabled = JNIEnv.GetStaticMethodID (class_ref, "isBeaconSightingsLoggingEnabled", "()Z");
				try {
					return JNIEnv.CallStaticBooleanMethod  (class_ref, id_isBeaconSightingsLoggingEnabled);
				} finally {
				}
			}
		}

		static IntPtr id_disableBeaconSightingsLogging;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='GimbalDebugger']/method[@name='disableBeaconSightingsLogging' and count(parameter)=0]"
		[Register ("disableBeaconSightingsLogging", "()V", "")]
		public static unsafe void DisableBeaconSightingsLogging ()
		{
			if (id_disableBeaconSightingsLogging == IntPtr.Zero)
				id_disableBeaconSightingsLogging = JNIEnv.GetStaticMethodID (class_ref, "disableBeaconSightingsLogging", "()V");
			try {
				JNIEnv.CallStaticVoidMethod  (class_ref, id_disableBeaconSightingsLogging);
			} finally {
			}
		}

		static IntPtr id_enableBeaconSightingsLogging;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='GimbalDebugger']/method[@name='enableBeaconSightingsLogging' and count(parameter)=0]"
		[Register ("enableBeaconSightingsLogging", "()V", "")]
		public static unsafe void EnableBeaconSightingsLogging ()
		{
			if (id_enableBeaconSightingsLogging == IntPtr.Zero)
				id_enableBeaconSightingsLogging = JNIEnv.GetStaticMethodID (class_ref, "enableBeaconSightingsLogging", "()V");
			try {
				JNIEnv.CallStaticVoidMethod  (class_ref, id_enableBeaconSightingsLogging);
			} finally {
			}
		}

	}
}
