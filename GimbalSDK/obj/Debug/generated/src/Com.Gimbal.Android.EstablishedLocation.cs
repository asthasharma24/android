using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='EstablishedLocation']"
	[global::Android.Runtime.Register ("com/gimbal/android/EstablishedLocation", DoNotGenerateAcw=true)]
	public partial class EstablishedLocation : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/EstablishedLocation", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (EstablishedLocation); }
		}

		protected EstablishedLocation (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android']/class[@name='EstablishedLocation']/constructor[@name='EstablishedLocation' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe EstablishedLocation ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (EstablishedLocation)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getBoundary;
#pragma warning disable 0169
		static Delegate GetGetBoundaryHandler ()
		{
			if (cb_getBoundary == null)
				cb_getBoundary = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBoundary);
			return cb_getBoundary;
		}

		static IntPtr n_GetBoundary (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.EstablishedLocation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.EstablishedLocation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Boundary);
		}
#pragma warning restore 0169

		static IntPtr id_getBoundary;
		public virtual unsafe global::Com.Gimbal.Android.Circle Boundary {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='EstablishedLocation']/method[@name='getBoundary' and count(parameter)=0]"
			[Register ("getBoundary", "()Lcom/gimbal/android/Circle;", "GetGetBoundaryHandler")]
			get {
				if (id_getBoundary == IntPtr.Zero)
					id_getBoundary = JNIEnv.GetMethodID (class_ref, "getBoundary", "()Lcom/gimbal/android/Circle;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Circle> (JNIEnv.CallObjectMethod  (Handle, id_getBoundary), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Circle> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBoundary", "()Lcom/gimbal/android/Circle;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getScore;
#pragma warning disable 0169
		static Delegate GetGetScoreHandler ()
		{
			if (cb_getScore == null)
				cb_getScore = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_GetScore);
			return cb_getScore;
		}

		static double n_GetScore (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.EstablishedLocation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.EstablishedLocation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Score;
		}
#pragma warning restore 0169

		static IntPtr id_getScore;
		public virtual unsafe double Score {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='EstablishedLocation']/method[@name='getScore' and count(parameter)=0]"
			[Register ("getScore", "()D", "GetGetScoreHandler")]
			get {
				if (id_getScore == IntPtr.Zero)
					id_getScore = JNIEnv.GetMethodID (class_ref, "getScore", "()D");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallDoubleMethod  (Handle, id_getScore);
					else
						return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getScore", "()D"));
				} finally {
				}
			}
		}

	}
}
