using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='TimeEvent']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/TimeEvent", DoNotGenerateAcw=true)]
	public partial class TimeEvent : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/TimeEvent", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (TimeEvent); }
		}

		protected TimeEvent (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='TimeEvent']/constructor[@name='TimeEvent' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe TimeEvent ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (TimeEvent)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getCampaignId;
#pragma warning disable 0169
		static Delegate GetGetCampaignIdHandler ()
		{
			if (cb_getCampaignId == null)
				cb_getCampaignId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCampaignId);
			return cb_getCampaignId;
		}

		static IntPtr n_GetCampaignId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.TimeEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.TimeEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.CampaignId);
		}
#pragma warning restore 0169

		static Delegate cb_setCampaignId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetCampaignId_Ljava_lang_String_Handler ()
		{
			if (cb_setCampaignId_Ljava_lang_String_ == null)
				cb_setCampaignId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCampaignId_Ljava_lang_String_);
			return cb_setCampaignId_Ljava_lang_String_;
		}

		static void n_SetCampaignId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.TimeEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.TimeEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.CampaignId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getCampaignId;
		static IntPtr id_setCampaignId_Ljava_lang_String_;
		public virtual unsafe string CampaignId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='TimeEvent']/method[@name='getCampaignId' and count(parameter)=0]"
			[Register ("getCampaignId", "()Ljava/lang/String;", "GetGetCampaignIdHandler")]
			get {
				if (id_getCampaignId == IntPtr.Zero)
					id_getCampaignId = JNIEnv.GetMethodID (class_ref, "getCampaignId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getCampaignId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCampaignId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='TimeEvent']/method[@name='setCampaignId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setCampaignId", "(Ljava/lang/String;)V", "GetSetCampaignId_Ljava_lang_String_Handler")]
			set {
				if (id_setCampaignId_Ljava_lang_String_ == IntPtr.Zero)
					id_setCampaignId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setCampaignId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCampaignId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCampaignId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getEventTime;
#pragma warning disable 0169
		static Delegate GetGetEventTimeHandler ()
		{
			if (cb_getEventTime == null)
				cb_getEventTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEventTime);
			return cb_getEventTime;
		}

		static IntPtr n_GetEventTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.TimeEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.TimeEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.EventTime);
		}
#pragma warning restore 0169

		static Delegate cb_setEventTime_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetEventTime_Ljava_lang_Long_Handler ()
		{
			if (cb_setEventTime_Ljava_lang_Long_ == null)
				cb_setEventTime_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetEventTime_Ljava_lang_Long_);
			return cb_setEventTime_Ljava_lang_Long_;
		}

		static void n_SetEventTime_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.TimeEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.TimeEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.EventTime = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEventTime;
		static IntPtr id_setEventTime_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long EventTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='TimeEvent']/method[@name='getEventTime' and count(parameter)=0]"
			[Register ("getEventTime", "()Ljava/lang/Long;", "GetGetEventTimeHandler")]
			get {
				if (id_getEventTime == IntPtr.Zero)
					id_getEventTime = JNIEnv.GetMethodID (class_ref, "getEventTime", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getEventTime), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEventTime", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='TimeEvent']/method[@name='setEventTime' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setEventTime", "(Ljava/lang/Long;)V", "GetSetEventTime_Ljava_lang_Long_Handler")]
			set {
				if (id_setEventTime_Ljava_lang_Long_ == IntPtr.Zero)
					id_setEventTime_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setEventTime", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEventTime_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEventTime", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getOrganizationId;
#pragma warning disable 0169
		static Delegate GetGetOrganizationIdHandler ()
		{
			if (cb_getOrganizationId == null)
				cb_getOrganizationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationId);
			return cb_getOrganizationId;
		}

		static IntPtr n_GetOrganizationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.TimeEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.TimeEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OrganizationId);
		}
#pragma warning restore 0169

		static Delegate cb_setOrganizationId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationId_Ljava_lang_Long_Handler ()
		{
			if (cb_setOrganizationId_Ljava_lang_Long_ == null)
				cb_setOrganizationId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationId_Ljava_lang_Long_);
			return cb_setOrganizationId_Ljava_lang_Long_;
		}

		static void n_SetOrganizationId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.TimeEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.TimeEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OrganizationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationId;
		static IntPtr id_setOrganizationId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long OrganizationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='TimeEvent']/method[@name='getOrganizationId' and count(parameter)=0]"
			[Register ("getOrganizationId", "()Ljava/lang/Long;", "GetGetOrganizationIdHandler")]
			get {
				if (id_getOrganizationId == IntPtr.Zero)
					id_getOrganizationId = JNIEnv.GetMethodID (class_ref, "getOrganizationId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='TimeEvent']/method[@name='setOrganizationId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setOrganizationId", "(Ljava/lang/Long;)V", "GetSetOrganizationId_Ljava_lang_Long_Handler")]
			set {
				if (id_setOrganizationId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setOrganizationId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setOrganizationId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOrganizationId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getTweenContentRandomInterval;
#pragma warning disable 0169
		static Delegate GetGetTweenContentRandomIntervalHandler ()
		{
			if (cb_getTweenContentRandomInterval == null)
				cb_getTweenContentRandomInterval = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTweenContentRandomInterval);
			return cb_getTweenContentRandomInterval;
		}

		static IntPtr n_GetTweenContentRandomInterval (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.TimeEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.TimeEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.TweenContentRandomInterval);
		}
#pragma warning restore 0169

		static Delegate cb_setTweenContentRandomInterval_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetTweenContentRandomInterval_Ljava_lang_Integer_Handler ()
		{
			if (cb_setTweenContentRandomInterval_Ljava_lang_Integer_ == null)
				cb_setTweenContentRandomInterval_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTweenContentRandomInterval_Ljava_lang_Integer_);
			return cb_setTweenContentRandomInterval_Ljava_lang_Integer_;
		}

		static void n_SetTweenContentRandomInterval_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.TimeEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.TimeEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.TweenContentRandomInterval = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTweenContentRandomInterval;
		static IntPtr id_setTweenContentRandomInterval_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer TweenContentRandomInterval {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='TimeEvent']/method[@name='getTweenContentRandomInterval' and count(parameter)=0]"
			[Register ("getTweenContentRandomInterval", "()Ljava/lang/Integer;", "GetGetTweenContentRandomIntervalHandler")]
			get {
				if (id_getTweenContentRandomInterval == IntPtr.Zero)
					id_getTweenContentRandomInterval = JNIEnv.GetMethodID (class_ref, "getTweenContentRandomInterval", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getTweenContentRandomInterval), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTweenContentRandomInterval", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='TimeEvent']/method[@name='setTweenContentRandomInterval' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setTweenContentRandomInterval", "(Ljava/lang/Integer;)V", "GetSetTweenContentRandomInterval_Ljava_lang_Integer_Handler")]
			set {
				if (id_setTweenContentRandomInterval_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setTweenContentRandomInterval_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setTweenContentRandomInterval", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTweenContentRandomInterval_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTweenContentRandomInterval", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

	}
}
