using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/Application", DoNotGenerateAcw=true)]
	public partial class Application : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/Application", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Application); }
		}

		protected Application (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/constructor[@name='Application' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Application ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Application)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static IntPtr id_getAndroidPlatform;
		public static unsafe string AndroidPlatform {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='getAndroidPlatform' and count(parameter)=0]"
			[Register ("getAndroidPlatform", "()Ljava/lang/String;", "GetGetAndroidPlatformHandler")]
			get {
				if (id_getAndroidPlatform == IntPtr.Zero)
					id_getAndroidPlatform = JNIEnv.GetStaticMethodID (class_ref, "getAndroidPlatform", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallStaticObjectMethod  (class_ref, id_getAndroidPlatform), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getApiKey;
#pragma warning disable 0169
		static Delegate GetGetApiKeyHandler ()
		{
			if (cb_getApiKey == null)
				cb_getApiKey = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApiKey);
			return cb_getApiKey;
		}

		static IntPtr n_GetApiKey (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ApiKey);
		}
#pragma warning restore 0169

		static Delegate cb_setApiKey_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetApiKey_Ljava_lang_String_Handler ()
		{
			if (cb_setApiKey_Ljava_lang_String_ == null)
				cb_setApiKey_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApiKey_Ljava_lang_String_);
			return cb_setApiKey_Ljava_lang_String_;
		}

		static void n_SetApiKey_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApiKey = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApiKey;
		static IntPtr id_setApiKey_Ljava_lang_String_;
		public virtual unsafe string ApiKey {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='getApiKey' and count(parameter)=0]"
			[Register ("getApiKey", "()Ljava/lang/String;", "GetGetApiKeyHandler")]
			get {
				if (id_getApiKey == IntPtr.Zero)
					id_getApiKey = JNIEnv.GetMethodID (class_ref, "getApiKey", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getApiKey), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApiKey", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='setApiKey' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setApiKey", "(Ljava/lang/String;)V", "GetSetApiKey_Ljava_lang_String_Handler")]
			set {
				if (id_setApiKey_Ljava_lang_String_ == IntPtr.Zero)
					id_setApiKey_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setApiKey", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApiKey_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApiKey", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_isCustomOptIn;
#pragma warning disable 0169
		static Delegate GetIsCustomOptInHandler ()
		{
			if (cb_isCustomOptIn == null)
				cb_isCustomOptIn = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsCustomOptIn);
			return cb_isCustomOptIn;
		}

		static bool n_IsCustomOptIn (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.CustomOptIn;
		}
#pragma warning restore 0169

		static Delegate cb_setCustomOptIn_Z;
#pragma warning disable 0169
		static Delegate GetSetCustomOptIn_ZHandler ()
		{
			if (cb_setCustomOptIn_Z == null)
				cb_setCustomOptIn_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetCustomOptIn_Z);
			return cb_setCustomOptIn_Z;
		}

		static void n_SetCustomOptIn_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.CustomOptIn = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isCustomOptIn;
		static IntPtr id_setCustomOptIn_Z;
		public virtual unsafe bool CustomOptIn {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='isCustomOptIn' and count(parameter)=0]"
			[Register ("isCustomOptIn", "()Z", "GetIsCustomOptInHandler")]
			get {
				if (id_isCustomOptIn == IntPtr.Zero)
					id_isCustomOptIn = JNIEnv.GetMethodID (class_ref, "isCustomOptIn", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isCustomOptIn);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isCustomOptIn", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='setCustomOptIn' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setCustomOptIn", "(Z)V", "GetSetCustomOptIn_ZHandler")]
			set {
				if (id_setCustomOptIn_Z == IntPtr.Zero)
					id_setCustomOptIn_Z = JNIEnv.GetMethodID (class_ref, "setCustomOptIn", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCustomOptIn_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCustomOptIn", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDailyPushLimit;
#pragma warning disable 0169
		static Delegate GetGetDailyPushLimitHandler ()
		{
			if (cb_getDailyPushLimit == null)
				cb_getDailyPushLimit = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDailyPushLimit);
			return cb_getDailyPushLimit;
		}

		static IntPtr n_GetDailyPushLimit (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DailyPushLimit);
		}
#pragma warning restore 0169

		static Delegate cb_setDailyPushLimit_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetDailyPushLimit_Ljava_lang_Integer_Handler ()
		{
			if (cb_setDailyPushLimit_Ljava_lang_Integer_ == null)
				cb_setDailyPushLimit_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDailyPushLimit_Ljava_lang_Integer_);
			return cb_setDailyPushLimit_Ljava_lang_Integer_;
		}

		static void n_SetDailyPushLimit_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DailyPushLimit = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDailyPushLimit;
		static IntPtr id_setDailyPushLimit_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer DailyPushLimit {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='getDailyPushLimit' and count(parameter)=0]"
			[Register ("getDailyPushLimit", "()Ljava/lang/Integer;", "GetGetDailyPushLimitHandler")]
			get {
				if (id_getDailyPushLimit == IntPtr.Zero)
					id_getDailyPushLimit = JNIEnv.GetMethodID (class_ref, "getDailyPushLimit", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getDailyPushLimit), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDailyPushLimit", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='setDailyPushLimit' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setDailyPushLimit", "(Ljava/lang/Integer;)V", "GetSetDailyPushLimit_Ljava_lang_Integer_Handler")]
			set {
				if (id_setDailyPushLimit_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setDailyPushLimit_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setDailyPushLimit", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDailyPushLimit_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDailyPushLimit", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getFingerprint;
#pragma warning disable 0169
		static Delegate GetGetFingerprintHandler ()
		{
			if (cb_getFingerprint == null)
				cb_getFingerprint = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetFingerprint);
			return cb_getFingerprint;
		}

		static IntPtr n_GetFingerprint (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Fingerprint);
		}
#pragma warning restore 0169

		static Delegate cb_setFingerprint_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetFingerprint_Ljava_lang_String_Handler ()
		{
			if (cb_setFingerprint_Ljava_lang_String_ == null)
				cb_setFingerprint_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetFingerprint_Ljava_lang_String_);
			return cb_setFingerprint_Ljava_lang_String_;
		}

		static void n_SetFingerprint_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Fingerprint = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getFingerprint;
		static IntPtr id_setFingerprint_Ljava_lang_String_;
		public virtual unsafe string Fingerprint {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='getFingerprint' and count(parameter)=0]"
			[Register ("getFingerprint", "()Ljava/lang/String;", "GetGetFingerprintHandler")]
			get {
				if (id_getFingerprint == IntPtr.Zero)
					id_getFingerprint = JNIEnv.GetMethodID (class_ref, "getFingerprint", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getFingerprint), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getFingerprint", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='setFingerprint' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setFingerprint", "(Ljava/lang/String;)V", "GetSetFingerprint_Ljava_lang_String_Handler")]
			set {
				if (id_setFingerprint_Ljava_lang_String_ == IntPtr.Zero)
					id_setFingerprint_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setFingerprint", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setFingerprint_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setFingerprint", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_Long_Handler ()
		{
			if (cb_setId_Ljava_lang_Long_ == null)
				cb_setId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_Long_);
			return cb_setId_Ljava_lang_Long_;
		}

		static void n_SetId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/Long;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setId", "(Ljava/lang/Long;)V", "GetSetId_Ljava_lang_Long_Handler")]
			set {
				if (id_setId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getIdentifier;
#pragma warning disable 0169
		static Delegate GetGetIdentifierHandler ()
		{
			if (cb_getIdentifier == null)
				cb_getIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIdentifier);
			return cb_getIdentifier;
		}

		static IntPtr n_GetIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Identifier);
		}
#pragma warning restore 0169

		static Delegate cb_setIdentifier_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetIdentifier_Ljava_lang_String_Handler ()
		{
			if (cb_setIdentifier_Ljava_lang_String_ == null)
				cb_setIdentifier_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetIdentifier_Ljava_lang_String_);
			return cb_setIdentifier_Ljava_lang_String_;
		}

		static void n_SetIdentifier_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Identifier = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getIdentifier;
		static IntPtr id_setIdentifier_Ljava_lang_String_;
		public virtual unsafe string Identifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='getIdentifier' and count(parameter)=0]"
			[Register ("getIdentifier", "()Ljava/lang/String;", "GetGetIdentifierHandler")]
			get {
				if (id_getIdentifier == IntPtr.Zero)
					id_getIdentifier = JNIEnv.GetMethodID (class_ref, "getIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='setIdentifier' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setIdentifier", "(Ljava/lang/String;)V", "GetSetIdentifier_Ljava_lang_String_Handler")]
			set {
				if (id_setIdentifier_Ljava_lang_String_ == IntPtr.Zero)
					id_setIdentifier_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setIdentifier", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setIdentifier_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setIdentifier", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr id_getIosPlatform;
		public static unsafe string IosPlatform {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='getIosPlatform' and count(parameter)=0]"
			[Register ("getIosPlatform", "()Ljava/lang/String;", "GetGetIosPlatformHandler")]
			get {
				if (id_getIosPlatform == IntPtr.Zero)
					id_getIosPlatform = JNIEnv.GetStaticMethodID (class_ref, "getIosPlatform", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallStaticObjectMethod  (class_ref, id_getIosPlatform), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getName;
#pragma warning disable 0169
		static Delegate GetGetNameHandler ()
		{
			if (cb_getName == null)
				cb_getName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetName);
			return cb_getName;
		}

		static IntPtr n_GetName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Name);
		}
#pragma warning restore 0169

		static Delegate cb_setName_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetName_Ljava_lang_String_Handler ()
		{
			if (cb_setName_Ljava_lang_String_ == null)
				cb_setName_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetName_Ljava_lang_String_);
			return cb_setName_Ljava_lang_String_;
		}

		static void n_SetName_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Name = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getName;
		static IntPtr id_setName_Ljava_lang_String_;
		public virtual unsafe string Name {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='getName' and count(parameter)=0]"
			[Register ("getName", "()Ljava/lang/String;", "GetGetNameHandler")]
			get {
				if (id_getName == IntPtr.Zero)
					id_getName = JNIEnv.GetMethodID (class_ref, "getName", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getName), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getName", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='setName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setName", "(Ljava/lang/String;)V", "GetSetName_Ljava_lang_String_Handler")]
			set {
				if (id_setName_Ljava_lang_String_ == IntPtr.Zero)
					id_setName_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setName", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setName_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setName", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getPlatform;
#pragma warning disable 0169
		static Delegate GetGetPlatformHandler ()
		{
			if (cb_getPlatform == null)
				cb_getPlatform = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlatform);
			return cb_getPlatform;
		}

		static IntPtr n_GetPlatform (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Platform);
		}
#pragma warning restore 0169

		static Delegate cb_setPlatform_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetPlatform_Ljava_lang_String_Handler ()
		{
			if (cb_setPlatform_Ljava_lang_String_ == null)
				cb_setPlatform_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPlatform_Ljava_lang_String_);
			return cb_setPlatform_Ljava_lang_String_;
		}

		static void n_SetPlatform_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Platform = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPlatform;
		static IntPtr id_setPlatform_Ljava_lang_String_;
		public virtual unsafe string Platform {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='getPlatform' and count(parameter)=0]"
			[Register ("getPlatform", "()Ljava/lang/String;", "GetGetPlatformHandler")]
			get {
				if (id_getPlatform == IntPtr.Zero)
					id_getPlatform = JNIEnv.GetMethodID (class_ref, "getPlatform", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getPlatform), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlatform", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='setPlatform' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setPlatform", "(Ljava/lang/String;)V", "GetSetPlatform_Ljava_lang_String_Handler")]
			set {
				if (id_setPlatform_Ljava_lang_String_ == IntPtr.Zero)
					id_setPlatform_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setPlatform", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPlatform_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPlatform", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getProximityApplicationUID;
#pragma warning disable 0169
		static Delegate GetGetProximityApplicationUIDHandler ()
		{
			if (cb_getProximityApplicationUID == null)
				cb_getProximityApplicationUID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetProximityApplicationUID);
			return cb_getProximityApplicationUID;
		}

		static IntPtr n_GetProximityApplicationUID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ProximityApplicationUID);
		}
#pragma warning restore 0169

		static Delegate cb_setProximityApplicationUID_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetProximityApplicationUID_Ljava_lang_String_Handler ()
		{
			if (cb_setProximityApplicationUID_Ljava_lang_String_ == null)
				cb_setProximityApplicationUID_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetProximityApplicationUID_Ljava_lang_String_);
			return cb_setProximityApplicationUID_Ljava_lang_String_;
		}

		static void n_SetProximityApplicationUID_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ProximityApplicationUID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getProximityApplicationUID;
		static IntPtr id_setProximityApplicationUID_Ljava_lang_String_;
		public virtual unsafe string ProximityApplicationUID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='getProximityApplicationUID' and count(parameter)=0]"
			[Register ("getProximityApplicationUID", "()Ljava/lang/String;", "GetGetProximityApplicationUIDHandler")]
			get {
				if (id_getProximityApplicationUID == IntPtr.Zero)
					id_getProximityApplicationUID = JNIEnv.GetMethodID (class_ref, "getProximityApplicationUID", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getProximityApplicationUID), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getProximityApplicationUID", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='setProximityApplicationUID' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setProximityApplicationUID", "(Ljava/lang/String;)V", "GetSetProximityApplicationUID_Ljava_lang_String_Handler")]
			set {
				if (id_setProximityApplicationUID_Ljava_lang_String_ == IntPtr.Zero)
					id_setProximityApplicationUID_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setProximityApplicationUID", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setProximityApplicationUID_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setProximityApplicationUID", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		static Delegate cb_setUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setUuid_Ljava_lang_String_ == null)
				cb_setUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUuid_Ljava_lang_String_);
			return cb_setUuid_Ljava_lang_String_;
		}

		static void n_SetUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Application __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Uuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUuid;
		static IntPtr id_setUuid_Ljava_lang_String_;
		public virtual unsafe string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler")]
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Application']/method[@name='setUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUuid", "(Ljava/lang/String;)V", "GetSetUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
