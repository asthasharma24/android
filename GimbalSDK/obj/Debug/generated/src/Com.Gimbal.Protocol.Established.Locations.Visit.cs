using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol.Established.Locations {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Visit']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/established/locations/Visit", DoNotGenerateAcw=true)]
	public partial class Visit : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/established/locations/Visit", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Visit); }
		}

		protected Visit (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Visit']/constructor[@name='Visit' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Visit ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Visit)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static IntPtr id_ctor_IILjava_lang_Integer_Ljava_lang_String_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Visit']/constructor[@name='Visit' and count(parameter)=4 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='java.lang.Integer'] and parameter[4][@type='java.lang.String']]"
		[Register (".ctor", "(IILjava/lang/Integer;Ljava/lang/String;)V", "")]
		public unsafe Visit (int p0, int p1, global::Java.Lang.Integer p2, string p3)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			IntPtr native_p3 = JNIEnv.NewString (p3);
			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (native_p3);
				if (GetType () != typeof (Visit)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(IILjava/lang/Integer;Ljava/lang/String;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(IILjava/lang/Integer;Ljava/lang/String;)V", __args);
					return;
				}

				if (id_ctor_IILjava_lang_Integer_Ljava_lang_String_ == IntPtr.Zero)
					id_ctor_IILjava_lang_Integer_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "<init>", "(IILjava/lang/Integer;Ljava/lang/String;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_IILjava_lang_Integer_Ljava_lang_String_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_IILjava_lang_Integer_Ljava_lang_String_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p3);
			}
		}

		static Delegate cb_getDuration;
#pragma warning disable 0169
		static Delegate GetGetDurationHandler ()
		{
			if (cb_getDuration == null)
				cb_getDuration = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDuration);
			return cb_getDuration;
		}

		static IntPtr n_GetDuration (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Established.Locations.Visit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Visit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Duration);
		}
#pragma warning restore 0169

		static Delegate cb_setDuration_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetDuration_Ljava_lang_Integer_Handler ()
		{
			if (cb_setDuration_Ljava_lang_Integer_ == null)
				cb_setDuration_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDuration_Ljava_lang_Integer_);
			return cb_setDuration_Ljava_lang_Integer_;
		}

		static void n_SetDuration_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Established.Locations.Visit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Visit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Duration = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDuration;
		static IntPtr id_setDuration_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer Duration {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Visit']/method[@name='getDuration' and count(parameter)=0]"
			[Register ("getDuration", "()Ljava/lang/Integer;", "GetGetDurationHandler")]
			get {
				if (id_getDuration == IntPtr.Zero)
					id_getDuration = JNIEnv.GetMethodID (class_ref, "getDuration", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getDuration), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDuration", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Visit']/method[@name='setDuration' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setDuration", "(Ljava/lang/Integer;)V", "GetSetDuration_Ljava_lang_Integer_Handler")]
			set {
				if (id_setDuration_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setDuration_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setDuration", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDuration_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDuration", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPreviousLocationId;
#pragma warning disable 0169
		static Delegate GetGetPreviousLocationIdHandler ()
		{
			if (cb_getPreviousLocationId == null)
				cb_getPreviousLocationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPreviousLocationId);
			return cb_getPreviousLocationId;
		}

		static IntPtr n_GetPreviousLocationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Established.Locations.Visit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Visit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.PreviousLocationId);
		}
#pragma warning restore 0169

		static Delegate cb_setPreviousLocationId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetPreviousLocationId_Ljava_lang_String_Handler ()
		{
			if (cb_setPreviousLocationId_Ljava_lang_String_ == null)
				cb_setPreviousLocationId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPreviousLocationId_Ljava_lang_String_);
			return cb_setPreviousLocationId_Ljava_lang_String_;
		}

		static void n_SetPreviousLocationId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Established.Locations.Visit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Visit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.PreviousLocationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPreviousLocationId;
		static IntPtr id_setPreviousLocationId_Ljava_lang_String_;
		public virtual unsafe string PreviousLocationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Visit']/method[@name='getPreviousLocationId' and count(parameter)=0]"
			[Register ("getPreviousLocationId", "()Ljava/lang/String;", "GetGetPreviousLocationIdHandler")]
			get {
				if (id_getPreviousLocationId == IntPtr.Zero)
					id_getPreviousLocationId = JNIEnv.GetMethodID (class_ref, "getPreviousLocationId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getPreviousLocationId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPreviousLocationId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Visit']/method[@name='setPreviousLocationId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setPreviousLocationId", "(Ljava/lang/String;)V", "GetSetPreviousLocationId_Ljava_lang_String_Handler")]
			set {
				if (id_setPreviousLocationId_Ljava_lang_String_ == IntPtr.Zero)
					id_setPreviousLocationId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setPreviousLocationId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPreviousLocationId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPreviousLocationId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getTimeWindow;
#pragma warning disable 0169
		static Delegate GetGetTimeWindowHandler ()
		{
			if (cb_getTimeWindow == null)
				cb_getTimeWindow = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTimeWindow);
			return cb_getTimeWindow;
		}

		static IntPtr n_GetTimeWindow (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Established.Locations.Visit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Visit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.TimeWindow);
		}
#pragma warning restore 0169

		static Delegate cb_setTimeWindow_Lcom_gimbal_protocol_established_locations_WeekTimeWindow_;
#pragma warning disable 0169
		static Delegate GetSetTimeWindow_Lcom_gimbal_protocol_established_locations_WeekTimeWindow_Handler ()
		{
			if (cb_setTimeWindow_Lcom_gimbal_protocol_established_locations_WeekTimeWindow_ == null)
				cb_setTimeWindow_Lcom_gimbal_protocol_established_locations_WeekTimeWindow_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTimeWindow_Lcom_gimbal_protocol_established_locations_WeekTimeWindow_);
			return cb_setTimeWindow_Lcom_gimbal_protocol_established_locations_WeekTimeWindow_;
		}

		static void n_SetTimeWindow_Lcom_gimbal_protocol_established_locations_WeekTimeWindow_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Established.Locations.Visit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Visit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.TimeWindow = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTimeWindow;
		static IntPtr id_setTimeWindow_Lcom_gimbal_protocol_established_locations_WeekTimeWindow_;
		public virtual unsafe global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow TimeWindow {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Visit']/method[@name='getTimeWindow' and count(parameter)=0]"
			[Register ("getTimeWindow", "()Lcom/gimbal/protocol/established/locations/WeekTimeWindow;", "GetGetTimeWindowHandler")]
			get {
				if (id_getTimeWindow == IntPtr.Zero)
					id_getTimeWindow = JNIEnv.GetMethodID (class_ref, "getTimeWindow", "()Lcom/gimbal/protocol/established/locations/WeekTimeWindow;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow> (JNIEnv.CallObjectMethod  (Handle, id_getTimeWindow), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTimeWindow", "()Lcom/gimbal/protocol/established/locations/WeekTimeWindow;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='Visit']/method[@name='setTimeWindow' and count(parameter)=1 and parameter[1][@type='com.gimbal.protocol.established.locations.WeekTimeWindow']]"
			[Register ("setTimeWindow", "(Lcom/gimbal/protocol/established/locations/WeekTimeWindow;)V", "GetSetTimeWindow_Lcom_gimbal_protocol_established_locations_WeekTimeWindow_Handler")]
			set {
				if (id_setTimeWindow_Lcom_gimbal_protocol_established_locations_WeekTimeWindow_ == IntPtr.Zero)
					id_setTimeWindow_Lcom_gimbal_protocol_established_locations_WeekTimeWindow_ = JNIEnv.GetMethodID (class_ref, "setTimeWindow", "(Lcom/gimbal/protocol/established/locations/WeekTimeWindow;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTimeWindow_Lcom_gimbal_protocol_established_locations_WeekTimeWindow_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTimeWindow", "(Lcom/gimbal/protocol/established/locations/WeekTimeWindow;)V"), __args);
				} finally {
				}
			}
		}

	}
}
