using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/InstantPushDetail", DoNotGenerateAcw=true)]
	public partial class InstantPushDetail : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/InstantPushDetail", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (InstantPushDetail); }
		}

		protected InstantPushDetail (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/constructor[@name='InstantPushDetail' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe InstantPushDetail ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (InstantPushDetail)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_isCombineTitleDescriptionForiOS;
#pragma warning disable 0169
		static Delegate GetIsCombineTitleDescriptionForiOSHandler ()
		{
			if (cb_isCombineTitleDescriptionForiOS == null)
				cb_isCombineTitleDescriptionForiOS = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsCombineTitleDescriptionForiOS);
			return cb_isCombineTitleDescriptionForiOS;
		}

		static bool n_IsCombineTitleDescriptionForiOS (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.CombineTitleDescriptionForiOS;
		}
#pragma warning restore 0169

		static Delegate cb_setCombineTitleDescriptionForiOS_Z;
#pragma warning disable 0169
		static Delegate GetSetCombineTitleDescriptionForiOS_ZHandler ()
		{
			if (cb_setCombineTitleDescriptionForiOS_Z == null)
				cb_setCombineTitleDescriptionForiOS_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetCombineTitleDescriptionForiOS_Z);
			return cb_setCombineTitleDescriptionForiOS_Z;
		}

		static void n_SetCombineTitleDescriptionForiOS_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.CombineTitleDescriptionForiOS = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isCombineTitleDescriptionForiOS;
		static IntPtr id_setCombineTitleDescriptionForiOS_Z;
		public virtual unsafe bool CombineTitleDescriptionForiOS {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='isCombineTitleDescriptionForiOS' and count(parameter)=0]"
			[Register ("isCombineTitleDescriptionForiOS", "()Z", "GetIsCombineTitleDescriptionForiOSHandler")]
			get {
				if (id_isCombineTitleDescriptionForiOS == IntPtr.Zero)
					id_isCombineTitleDescriptionForiOS = JNIEnv.GetMethodID (class_ref, "isCombineTitleDescriptionForiOS", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isCombineTitleDescriptionForiOS);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isCombineTitleDescriptionForiOS", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='setCombineTitleDescriptionForiOS' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setCombineTitleDescriptionForiOS", "(Z)V", "GetSetCombineTitleDescriptionForiOS_ZHandler")]
			set {
				if (id_setCombineTitleDescriptionForiOS_Z == IntPtr.Zero)
					id_setCombineTitleDescriptionForiOS_Z = JNIEnv.GetMethodID (class_ref, "setCombineTitleDescriptionForiOS", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCombineTitleDescriptionForiOS_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCombineTitleDescriptionForiOS", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getContentId;
#pragma warning disable 0169
		static Delegate GetGetContentIdHandler ()
		{
			if (cb_getContentId == null)
				cb_getContentId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetContentId);
			return cb_getContentId;
		}

		static IntPtr n_GetContentId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ContentId);
		}
#pragma warning restore 0169

		static Delegate cb_setContentId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetContentId_Ljava_lang_String_Handler ()
		{
			if (cb_setContentId_Ljava_lang_String_ == null)
				cb_setContentId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetContentId_Ljava_lang_String_);
			return cb_setContentId_Ljava_lang_String_;
		}

		static void n_SetContentId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ContentId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getContentId;
		static IntPtr id_setContentId_Ljava_lang_String_;
		public virtual unsafe string ContentId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='getContentId' and count(parameter)=0]"
			[Register ("getContentId", "()Ljava/lang/String;", "GetGetContentIdHandler")]
			get {
				if (id_getContentId == IntPtr.Zero)
					id_getContentId = JNIEnv.GetMethodID (class_ref, "getContentId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getContentId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getContentId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='setContentId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setContentId", "(Ljava/lang/String;)V", "GetSetContentId_Ljava_lang_String_Handler")]
			set {
				if (id_setContentId_Ljava_lang_String_ == IntPtr.Zero)
					id_setContentId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setContentId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setContentId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setContentId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getData;
#pragma warning disable 0169
		static Delegate GetGetDataHandler ()
		{
			if (cb_getData == null)
				cb_getData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetData);
			return cb_getData;
		}

		static IntPtr n_GetData (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Data);
		}
#pragma warning restore 0169

		static Delegate cb_setData_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetData_Ljava_lang_String_Handler ()
		{
			if (cb_setData_Ljava_lang_String_ == null)
				cb_setData_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetData_Ljava_lang_String_);
			return cb_setData_Ljava_lang_String_;
		}

		static void n_SetData_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Data = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getData;
		static IntPtr id_setData_Ljava_lang_String_;
		public virtual unsafe string Data {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='getData' and count(parameter)=0]"
			[Register ("getData", "()Ljava/lang/String;", "GetGetDataHandler")]
			get {
				if (id_getData == IntPtr.Zero)
					id_getData = JNIEnv.GetMethodID (class_ref, "getData", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getData), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getData", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='setData' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setData", "(Ljava/lang/String;)V", "GetSetData_Ljava_lang_String_Handler")]
			set {
				if (id_setData_Ljava_lang_String_ == IntPtr.Zero)
					id_setData_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setData", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setData_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setData", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getDescription;
#pragma warning disable 0169
		static Delegate GetGetDescriptionHandler ()
		{
			if (cb_getDescription == null)
				cb_getDescription = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDescription);
			return cb_getDescription;
		}

		static IntPtr n_GetDescription (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Description);
		}
#pragma warning restore 0169

		static Delegate cb_setDescription_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetDescription_Ljava_lang_String_Handler ()
		{
			if (cb_setDescription_Ljava_lang_String_ == null)
				cb_setDescription_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDescription_Ljava_lang_String_);
			return cb_setDescription_Ljava_lang_String_;
		}

		static void n_SetDescription_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Description = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDescription;
		static IntPtr id_setDescription_Ljava_lang_String_;
		public virtual unsafe string Description {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='getDescription' and count(parameter)=0]"
			[Register ("getDescription", "()Ljava/lang/String;", "GetGetDescriptionHandler")]
			get {
				if (id_getDescription == IntPtr.Zero)
					id_getDescription = JNIEnv.GetMethodID (class_ref, "getDescription", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getDescription), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDescription", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='setDescription' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setDescription", "(Ljava/lang/String;)V", "GetSetDescription_Ljava_lang_String_Handler")]
			set {
				if (id_setDescription_Ljava_lang_String_ == IntPtr.Zero)
					id_setDescription_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setDescription", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDescription_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDescription", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_isGeofencePlaceFilterAll;
#pragma warning disable 0169
		static Delegate GetIsGeofencePlaceFilterAllHandler ()
		{
			if (cb_isGeofencePlaceFilterAll == null)
				cb_isGeofencePlaceFilterAll = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsGeofencePlaceFilterAll);
			return cb_isGeofencePlaceFilterAll;
		}

		static bool n_IsGeofencePlaceFilterAll (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.GeofencePlaceFilterAll;
		}
#pragma warning restore 0169

		static Delegate cb_setGeofencePlaceFilterAll_Z;
#pragma warning disable 0169
		static Delegate GetSetGeofencePlaceFilterAll_ZHandler ()
		{
			if (cb_setGeofencePlaceFilterAll_Z == null)
				cb_setGeofencePlaceFilterAll_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetGeofencePlaceFilterAll_Z);
			return cb_setGeofencePlaceFilterAll_Z;
		}

		static void n_SetGeofencePlaceFilterAll_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.GeofencePlaceFilterAll = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isGeofencePlaceFilterAll;
		static IntPtr id_setGeofencePlaceFilterAll_Z;
		public virtual unsafe bool GeofencePlaceFilterAll {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='isGeofencePlaceFilterAll' and count(parameter)=0]"
			[Register ("isGeofencePlaceFilterAll", "()Z", "GetIsGeofencePlaceFilterAllHandler")]
			get {
				if (id_isGeofencePlaceFilterAll == IntPtr.Zero)
					id_isGeofencePlaceFilterAll = JNIEnv.GetMethodID (class_ref, "isGeofencePlaceFilterAll", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isGeofencePlaceFilterAll);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isGeofencePlaceFilterAll", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='setGeofencePlaceFilterAll' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setGeofencePlaceFilterAll", "(Z)V", "GetSetGeofencePlaceFilterAll_ZHandler")]
			set {
				if (id_setGeofencePlaceFilterAll_Z == IntPtr.Zero)
					id_setGeofencePlaceFilterAll_Z = JNIEnv.GetMethodID (class_ref, "setGeofencePlaceFilterAll", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setGeofencePlaceFilterAll_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setGeofencePlaceFilterAll", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPlaceFilters;
#pragma warning disable 0169
		static Delegate GetGetPlaceFiltersHandler ()
		{
			if (cb_getPlaceFilters == null)
				cb_getPlaceFilters = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlaceFilters);
			return cb_getPlaceFilters;
		}

		static IntPtr n_GetPlaceFilters (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Java.Lang.Long>.ToLocalJniHandle (__this.PlaceFilters);
		}
#pragma warning restore 0169

		static Delegate cb_setPlaceFilters_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetPlaceFilters_Ljava_util_List_Handler ()
		{
			if (cb_setPlaceFilters_Ljava_util_List_ == null)
				cb_setPlaceFilters_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPlaceFilters_Ljava_util_List_);
			return cb_setPlaceFilters_Ljava_util_List_;
		}

		static void n_SetPlaceFilters_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Java.Lang.Long>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.PlaceFilters = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPlaceFilters;
		static IntPtr id_setPlaceFilters_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Java.Lang.Long> PlaceFilters {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='getPlaceFilters' and count(parameter)=0]"
			[Register ("getPlaceFilters", "()Ljava/util/List;", "GetGetPlaceFiltersHandler")]
			get {
				if (id_getPlaceFilters == IntPtr.Zero)
					id_getPlaceFilters = JNIEnv.GetMethodID (class_ref, "getPlaceFilters", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Java.Lang.Long>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getPlaceFilters), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Java.Lang.Long>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlaceFilters", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='setPlaceFilters' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;java.lang.Long&gt;']]"
			[Register ("setPlaceFilters", "(Ljava/util/List;)V", "GetSetPlaceFilters_Ljava_util_List_Handler")]
			set {
				if (id_setPlaceFilters_Ljava_util_List_ == IntPtr.Zero)
					id_setPlaceFilters_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setPlaceFilters", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Java.Lang.Long>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPlaceFilters_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPlaceFilters", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getTitle;
#pragma warning disable 0169
		static Delegate GetGetTitleHandler ()
		{
			if (cb_getTitle == null)
				cb_getTitle = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTitle);
			return cb_getTitle;
		}

		static IntPtr n_GetTitle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Title);
		}
#pragma warning restore 0169

		static Delegate cb_setTitle_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetTitle_Ljava_lang_String_Handler ()
		{
			if (cb_setTitle_Ljava_lang_String_ == null)
				cb_setTitle_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTitle_Ljava_lang_String_);
			return cb_setTitle_Ljava_lang_String_;
		}

		static void n_SetTitle_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.InstantPushDetail __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.InstantPushDetail> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Title = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTitle;
		static IntPtr id_setTitle_Ljava_lang_String_;
		public virtual unsafe string Title {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='getTitle' and count(parameter)=0]"
			[Register ("getTitle", "()Ljava/lang/String;", "GetGetTitleHandler")]
			get {
				if (id_getTitle == IntPtr.Zero)
					id_getTitle = JNIEnv.GetMethodID (class_ref, "getTitle", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getTitle), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTitle", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='InstantPushDetail']/method[@name='setTitle' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setTitle", "(Ljava/lang/String;)V", "GetSetTitle_Ljava_lang_String_Handler")]
			set {
				if (id_setTitle_Ljava_lang_String_ == IntPtr.Zero)
					id_setTitle_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setTitle", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTitle_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTitle", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
