using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Location.Established {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='WeekRange']"
	[global::Android.Runtime.Register ("com/gimbal/location/established/WeekRange", DoNotGenerateAcw=true)]
	public partial class WeekRange : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/location/established/WeekRange", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (WeekRange); }
		}

		protected WeekRange (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_II;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='WeekRange']/constructor[@name='WeekRange' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='int']]"
		[Register (".ctor", "(II)V", "")]
		public unsafe WeekRange (int p0, int p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (GetType () != typeof (WeekRange)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(II)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(II)V", __args);
					return;
				}

				if (id_ctor_II == IntPtr.Zero)
					id_ctor_II = JNIEnv.GetMethodID (class_ref, "<init>", "(II)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_II, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_II, __args);
			} finally {
			}
		}

		static Delegate cb_getEnd;
#pragma warning disable 0169
		static Delegate GetGetEndHandler ()
		{
			if (cb_getEnd == null)
				cb_getEnd = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetEnd);
			return cb_getEnd;
		}

		static int n_GetEnd (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.WeekRange __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.WeekRange> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.End;
		}
#pragma warning restore 0169

		static IntPtr id_getEnd;
		public virtual unsafe int End {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='WeekRange']/method[@name='getEnd' and count(parameter)=0]"
			[Register ("getEnd", "()I", "GetGetEndHandler")]
			get {
				if (id_getEnd == IntPtr.Zero)
					id_getEnd = JNIEnv.GetMethodID (class_ref, "getEnd", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getEnd);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEnd", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getStart;
#pragma warning disable 0169
		static Delegate GetGetStartHandler ()
		{
			if (cb_getStart == null)
				cb_getStart = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetStart);
			return cb_getStart;
		}

		static int n_GetStart (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.WeekRange __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.WeekRange> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Start;
		}
#pragma warning restore 0169

		static IntPtr id_getStart;
		public virtual unsafe int Start {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='WeekRange']/method[@name='getStart' and count(parameter)=0]"
			[Register ("getStart", "()I", "GetGetStartHandler")]
			get {
				if (id_getStart == IntPtr.Zero)
					id_getStart = JNIEnv.GetMethodID (class_ref, "getStart", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getStart);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getStart", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_inRange_Lcom_gimbal_location_established_WeekRange_I;
#pragma warning disable 0169
		static Delegate GetInRange_Lcom_gimbal_location_established_WeekRange_IHandler ()
		{
			if (cb_inRange_Lcom_gimbal_location_established_WeekRange_I == null)
				cb_inRange_Lcom_gimbal_location_established_WeekRange_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, int, bool>) n_InRange_Lcom_gimbal_location_established_WeekRange_I);
			return cb_inRange_Lcom_gimbal_location_established_WeekRange_I;
		}

		static bool n_InRange_Lcom_gimbal_location_established_WeekRange_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Com.Gimbal.Location.Established.WeekRange __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.WeekRange> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Location.Established.WeekRange p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.WeekRange> (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.InRange (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_inRange_Lcom_gimbal_location_established_WeekRange_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='WeekRange']/method[@name='inRange' and count(parameter)=2 and parameter[1][@type='com.gimbal.location.established.WeekRange'] and parameter[2][@type='int']]"
		[Register ("inRange", "(Lcom/gimbal/location/established/WeekRange;I)Z", "GetInRange_Lcom_gimbal_location_established_WeekRange_IHandler")]
		public virtual unsafe bool InRange (global::Com.Gimbal.Location.Established.WeekRange p0, int p1)
		{
			if (id_inRange_Lcom_gimbal_location_established_WeekRange_I == IntPtr.Zero)
				id_inRange_Lcom_gimbal_location_established_WeekRange_I = JNIEnv.GetMethodID (class_ref, "inRange", "(Lcom/gimbal/location/established/WeekRange;I)Z");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				bool __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.CallBooleanMethod  (Handle, id_inRange_Lcom_gimbal_location_established_WeekRange_I, __args);
				else
					__ret = JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "inRange", "(Lcom/gimbal/location/established/WeekRange;I)Z"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_inRange_III;
#pragma warning disable 0169
		static Delegate GetInRange_IIIHandler ()
		{
			if (cb_inRange_III == null)
				cb_inRange_III = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, int, int, bool>) n_InRange_III);
			return cb_inRange_III;
		}

		static bool n_InRange_III (IntPtr jnienv, IntPtr native__this, int p0, int p1, int p2)
		{
			global::Com.Gimbal.Location.Established.WeekRange __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.WeekRange> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.InRange (p0, p1, p2);
		}
#pragma warning restore 0169

		static IntPtr id_inRange_III;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='WeekRange']/method[@name='inRange' and count(parameter)=3 and parameter[1][@type='int'] and parameter[2][@type='int'] and parameter[3][@type='int']]"
		[Register ("inRange", "(III)Z", "GetInRange_IIIHandler")]
		public virtual unsafe bool InRange (int p0, int p1, int p2)
		{
			if (id_inRange_III == IntPtr.Zero)
				id_inRange_III = JNIEnv.GetMethodID (class_ref, "inRange", "(III)Z");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);

				if (GetType () == ThresholdType)
					return JNIEnv.CallBooleanMethod  (Handle, id_inRange_III, __args);
				else
					return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "inRange", "(III)Z"), __args);
			} finally {
			}
		}

		static Delegate cb_secondsTo_I;
#pragma warning disable 0169
		static Delegate GetSecondsTo_IHandler ()
		{
			if (cb_secondsTo_I == null)
				cb_secondsTo_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, int>) n_SecondsTo_I);
			return cb_secondsTo_I;
		}

		static int n_SecondsTo_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Gimbal.Location.Established.WeekRange __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.WeekRange> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SecondsTo (p0);
		}
#pragma warning restore 0169

		static IntPtr id_secondsTo_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='WeekRange']/method[@name='secondsTo' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("secondsTo", "(I)I", "GetSecondsTo_IHandler")]
		public virtual unsafe int SecondsTo (int p0)
		{
			if (id_secondsTo_I == IntPtr.Zero)
				id_secondsTo_I = JNIEnv.GetMethodID (class_ref, "secondsTo", "(I)I");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					return JNIEnv.CallIntMethod  (Handle, id_secondsTo_I, __args);
				else
					return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "secondsTo", "(I)I"), __args);
			} finally {
			}
		}

	}
}
