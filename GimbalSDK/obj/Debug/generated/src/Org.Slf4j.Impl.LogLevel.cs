using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Org.Slf4j.Impl {

	// Metadata.xml XPath class reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevel']"
	[global::Android.Runtime.Register ("org/slf4j/impl/LogLevel", DoNotGenerateAcw=true)]
	public sealed partial class LogLevel : global::Java.Lang.Enum {


		static IntPtr DEBUG_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevel']/field[@name='DEBUG']"
		[Register ("DEBUG")]
		public static global::Org.Slf4j.Impl.LogLevel Debug {
			get {
				if (DEBUG_jfieldId == IntPtr.Zero)
					DEBUG_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "DEBUG", "Lorg/slf4j/impl/LogLevel;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, DEBUG_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevel> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr ERROR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevel']/field[@name='ERROR']"
		[Register ("ERROR")]
		public static global::Org.Slf4j.Impl.LogLevel Error {
			get {
				if (ERROR_jfieldId == IntPtr.Zero)
					ERROR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "ERROR", "Lorg/slf4j/impl/LogLevel;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, ERROR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevel> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr INFO_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevel']/field[@name='INFO']"
		[Register ("INFO")]
		public static global::Org.Slf4j.Impl.LogLevel Info {
			get {
				if (INFO_jfieldId == IntPtr.Zero)
					INFO_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "INFO", "Lorg/slf4j/impl/LogLevel;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, INFO_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevel> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr TRACE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevel']/field[@name='TRACE']"
		[Register ("TRACE")]
		public static global::Org.Slf4j.Impl.LogLevel Trace {
			get {
				if (TRACE_jfieldId == IntPtr.Zero)
					TRACE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "TRACE", "Lorg/slf4j/impl/LogLevel;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, TRACE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevel> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr WARN_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevel']/field[@name='WARN']"
		[Register ("WARN")]
		public static global::Org.Slf4j.Impl.LogLevel Warn {
			get {
				if (WARN_jfieldId == IntPtr.Zero)
					WARN_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "WARN", "Lorg/slf4j/impl/LogLevel;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, WARN_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevel> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("org/slf4j/impl/LogLevel", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (LogLevel); }
		}

		internal LogLevel (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_valueOf;
		// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevel']/method[@name='valueOf' and count(parameter)=0]"
		[Register ("valueOf", "()I", "")]
		public unsafe int ValueOf ()
		{
			if (id_valueOf == IntPtr.Zero)
				id_valueOf = JNIEnv.GetMethodID (class_ref, "valueOf", "()I");
			try {
				return JNIEnv.CallIntMethod  (Handle, id_valueOf);
			} finally {
			}
		}

		static IntPtr id_valueOf_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevel']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("valueOf", "(Ljava/lang/String;)Lorg/slf4j/impl/LogLevel;", "")]
		public static unsafe global::Org.Slf4j.Impl.LogLevel ValueOf (string p0)
		{
			if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
				id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lorg/slf4j/impl/LogLevel;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				global::Org.Slf4j.Impl.LogLevel __ret = global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.LogLevel> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_values;
		// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='LogLevel']/method[@name='values' and count(parameter)=0]"
		[Register ("values", "()[Lorg/slf4j/impl/LogLevel;", "")]
		public static unsafe global::Org.Slf4j.Impl.LogLevel[] Values ()
		{
			if (id_values == IntPtr.Zero)
				id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lorg/slf4j/impl/LogLevel;");
			try {
				return (global::Org.Slf4j.Impl.LogLevel[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Org.Slf4j.Impl.LogLevel));
			} finally {
			}
		}

	}
}
