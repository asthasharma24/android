using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceEventListener']"
	[global::Android.Runtime.Register ("com/gimbal/android/PlaceEventListener", DoNotGenerateAcw=true)]
	public abstract partial class PlaceEventListener : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/PlaceEventListener", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PlaceEventListener); }
		}

		protected PlaceEventListener (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceEventListener']/constructor[@name='PlaceEventListener' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe PlaceEventListener ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (PlaceEventListener)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetOnBeaconSighting_Lcom_gimbal_android_BeaconSighting_Ljava_util_List_Handler ()
		{
			if (cb_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_Ljava_util_List_ == null)
				cb_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_OnBeaconSighting_Lcom_gimbal_android_BeaconSighting_Ljava_util_List_);
			return cb_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_Ljava_util_List_;
		}

		static void n_OnBeaconSighting_Lcom_gimbal_android_BeaconSighting_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Android.PlaceEventListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.PlaceEventListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.BeaconSighting p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.BeaconSighting> (native_p0, JniHandleOwnership.DoNotTransfer);
			var p1 = global::Android.Runtime.JavaList<global::Com.Gimbal.Android.Visit>.FromJniHandle (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.OnBeaconSighting (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_Ljava_util_List_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceEventListener']/method[@name='onBeaconSighting' and count(parameter)=2 and parameter[1][@type='com.gimbal.android.BeaconSighting'] and parameter[2][@type='java.util.List&lt;com.gimbal.android.Visit&gt;']]"
		[Register ("onBeaconSighting", "(Lcom/gimbal/android/BeaconSighting;Ljava/util/List;)V", "GetOnBeaconSighting_Lcom_gimbal_android_BeaconSighting_Ljava_util_List_Handler")]
		public virtual unsafe void OnBeaconSighting (global::Com.Gimbal.Android.BeaconSighting p0, global::System.Collections.Generic.IList<global::Com.Gimbal.Android.Visit> p1)
		{
			if (id_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_Ljava_util_List_ == IntPtr.Zero)
				id_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "onBeaconSighting", "(Lcom/gimbal/android/BeaconSighting;Ljava/util/List;)V");
			IntPtr native_p1 = global::Android.Runtime.JavaList<global::Com.Gimbal.Android.Visit>.ToLocalJniHandle (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_Ljava_util_List_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onBeaconSighting", "(Lcom/gimbal/android/BeaconSighting;Ljava/util/List;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static Delegate cb_onVisitEnd_Lcom_gimbal_android_Visit_;
#pragma warning disable 0169
		static Delegate GetOnVisitEnd_Lcom_gimbal_android_Visit_Handler ()
		{
			if (cb_onVisitEnd_Lcom_gimbal_android_Visit_ == null)
				cb_onVisitEnd_Lcom_gimbal_android_Visit_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnVisitEnd_Lcom_gimbal_android_Visit_);
			return cb_onVisitEnd_Lcom_gimbal_android_Visit_;
		}

		static void n_OnVisitEnd_Lcom_gimbal_android_Visit_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Android.PlaceEventListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.PlaceEventListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.Visit p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Visit> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnVisitEnd (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onVisitEnd_Lcom_gimbal_android_Visit_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceEventListener']/method[@name='onVisitEnd' and count(parameter)=1 and parameter[1][@type='com.gimbal.android.Visit']]"
		[Register ("onVisitEnd", "(Lcom/gimbal/android/Visit;)V", "GetOnVisitEnd_Lcom_gimbal_android_Visit_Handler")]
		public virtual unsafe void OnVisitEnd (global::Com.Gimbal.Android.Visit p0)
		{
			if (id_onVisitEnd_Lcom_gimbal_android_Visit_ == IntPtr.Zero)
				id_onVisitEnd_Lcom_gimbal_android_Visit_ = JNIEnv.GetMethodID (class_ref, "onVisitEnd", "(Lcom/gimbal/android/Visit;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_onVisitEnd_Lcom_gimbal_android_Visit_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onVisitEnd", "(Lcom/gimbal/android/Visit;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_onVisitStart_Lcom_gimbal_android_Visit_;
#pragma warning disable 0169
		static Delegate GetOnVisitStart_Lcom_gimbal_android_Visit_Handler ()
		{
			if (cb_onVisitStart_Lcom_gimbal_android_Visit_ == null)
				cb_onVisitStart_Lcom_gimbal_android_Visit_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnVisitStart_Lcom_gimbal_android_Visit_);
			return cb_onVisitStart_Lcom_gimbal_android_Visit_;
		}

		static void n_OnVisitStart_Lcom_gimbal_android_Visit_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Android.PlaceEventListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.PlaceEventListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.Visit p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Visit> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnVisitStart (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onVisitStart_Lcom_gimbal_android_Visit_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='PlaceEventListener']/method[@name='onVisitStart' and count(parameter)=1 and parameter[1][@type='com.gimbal.android.Visit']]"
		[Register ("onVisitStart", "(Lcom/gimbal/android/Visit;)V", "GetOnVisitStart_Lcom_gimbal_android_Visit_Handler")]
		public virtual unsafe void OnVisitStart (global::Com.Gimbal.Android.Visit p0)
		{
			if (id_onVisitStart_Lcom_gimbal_android_Visit_ == IntPtr.Zero)
				id_onVisitStart_Lcom_gimbal_android_Visit_ = JNIEnv.GetMethodID (class_ref, "onVisitStart", "(Lcom/gimbal/android/Visit;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_onVisitStart_Lcom_gimbal_android_Visit_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onVisitStart", "(Lcom/gimbal/android/Visit;)V"), __args);
			} finally {
			}
		}

	}

	[global::Android.Runtime.Register ("com/gimbal/android/PlaceEventListener", DoNotGenerateAcw=true)]
	internal partial class PlaceEventListenerInvoker : PlaceEventListener {

		public PlaceEventListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (PlaceEventListenerInvoker); }
		}

	}

}
