using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/PushKey", DoNotGenerateAcw=true)]
	public partial class PushKey : global::Java.Lang.Object {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']/field[@name='ATTRIBUTES']"
		[Register ("ATTRIBUTES")]
		public const string Attributes = (string) "ATTRS";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']/field[@name='COMMUNICATE']"
		[Register ("COMMUNICATE")]
		public const string Communicate = (string) "COMM";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']/field[@name='CONTENT_ID_KEY']"
		[Register ("CONTENT_ID_KEY")]
		public const string ContentIdKey = (string) "CID";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']/field[@name='DESCRIPTION']"
		[Register ("DESCRIPTION")]
		public const string Description = (string) "DESC";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']/field[@name='EXPIRY_DATE']"
		[Register ("EXPIRY_DATE")]
		public const string ExpiryDate = (string) "ED";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']/field[@name='INSTANT_CONTENT_META_DATA']"
		[Register ("INSTANT_CONTENT_META_DATA")]
		public const string InstantContentMetaData = (string) "XD";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']/field[@name='INSTANT_CONTENT_NOTIFICATON']"
		[Register ("INSTANT_CONTENT_NOTIFICATON")]
		public const string InstantContentNotificaton = (string) "IC";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']/field[@name='NOTIFICATION_TYPE_KEY']"
		[Register ("NOTIFICATION_TYPE_KEY")]
		public const string NotificationTypeKey = (string) "NT";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']/field[@name='TIME_CONTENT_NOTIFICATON']"
		[Register ("TIME_CONTENT_NOTIFICATON")]
		public const string TimeContentNotificaton = (string) "TC";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']/field[@name='TITLE']"
		[Register ("TITLE")]
		public const string Title = (string) "TL";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']/field[@name='URL']"
		[Register ("URL")]
		public const string Url = (string) "URL";
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/PushKey", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PushKey); }
		}

		protected PushKey (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushKey']/constructor[@name='PushKey' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe PushKey ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (PushKey)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

	}
}
