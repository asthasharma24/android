using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Logging {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogConfig']"
	[global::Android.Runtime.Register ("com/gimbal/logging/GimbalLogConfig", DoNotGenerateAcw=true)]
	public partial class GimbalLogConfig : global::Java.Lang.Object {


		static IntPtr shutdownHook_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogConfig']/field[@name='shutdownHook']"
		[Register ("shutdownHook")]
		public static global::Java.Lang.Thread ShutdownHook {
			get {
				if (shutdownHook_jfieldId == IntPtr.Zero)
					shutdownHook_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "shutdownHook", "Ljava/lang/Thread;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, shutdownHook_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Thread> (__ret, JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (shutdownHook_jfieldId == IntPtr.Zero)
					shutdownHook_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "shutdownHook", "Ljava/lang/Thread;");
				IntPtr native_value = JNIEnv.ToLocalJniHandle (value);
				try {
					JNIEnv.SetStaticField (class_ref, shutdownHook_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr uncaughtExceptionHandler_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogConfig']/field[@name='uncaughtExceptionHandler']"
		[Register ("uncaughtExceptionHandler")]
		public static global::Java.Lang.Thread.IUncaughtExceptionHandler UncaughtExceptionHandler {
			get {
				if (uncaughtExceptionHandler_jfieldId == IntPtr.Zero)
					uncaughtExceptionHandler_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "uncaughtExceptionHandler", "Ljava/lang/Thread$UncaughtExceptionHandler;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, uncaughtExceptionHandler_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Thread.IUncaughtExceptionHandler> (__ret, JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (uncaughtExceptionHandler_jfieldId == IntPtr.Zero)
					uncaughtExceptionHandler_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "uncaughtExceptionHandler", "Ljava/lang/Thread$UncaughtExceptionHandler;");
				IntPtr native_value = JNIEnv.ToLocalJniHandle (value);
				try {
					JNIEnv.SetStaticField (class_ref, uncaughtExceptionHandler_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/logging/GimbalLogConfig", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (GimbalLogConfig); }
		}

		protected GimbalLogConfig (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogConfig']/constructor[@name='GimbalLogConfig' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe GimbalLogConfig ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (GimbalLogConfig)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static IntPtr id_disableFileLogging;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogConfig']/method[@name='disableFileLogging' and count(parameter)=0]"
		[Register ("disableFileLogging", "()V", "")]
		public static unsafe void DisableFileLogging ()
		{
			if (id_disableFileLogging == IntPtr.Zero)
				id_disableFileLogging = JNIEnv.GetStaticMethodID (class_ref, "disableFileLogging", "()V");
			try {
				JNIEnv.CallStaticVoidMethod  (class_ref, id_disableFileLogging);
			} finally {
			}
		}

		static IntPtr id_disableUncaughtExceptionLogging;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogConfig']/method[@name='disableUncaughtExceptionLogging' and count(parameter)=0]"
		[Register ("disableUncaughtExceptionLogging", "()V", "")]
		public static unsafe void DisableUncaughtExceptionLogging ()
		{
			if (id_disableUncaughtExceptionLogging == IntPtr.Zero)
				id_disableUncaughtExceptionLogging = JNIEnv.GetStaticMethodID (class_ref, "disableUncaughtExceptionLogging", "()V");
			try {
				JNIEnv.CallStaticVoidMethod  (class_ref, id_disableUncaughtExceptionLogging);
			} finally {
			}
		}

		static IntPtr id_enableFileLogging_Landroid_content_Context_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogConfig']/method[@name='enableFileLogging' and count(parameter)=1 and parameter[1][@type='android.content.Context']]"
		[Register ("enableFileLogging", "(Landroid/content/Context;)V", "")]
		public static unsafe void EnableFileLogging (global::Android.Content.Context p0)
		{
			if (id_enableFileLogging_Landroid_content_Context_ == IntPtr.Zero)
				id_enableFileLogging_Landroid_content_Context_ = JNIEnv.GetStaticMethodID (class_ref, "enableFileLogging", "(Landroid/content/Context;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_enableFileLogging_Landroid_content_Context_, __args);
			} finally {
			}
		}

		static IntPtr id_enableUncaughtExceptionLogging;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogConfig']/method[@name='enableUncaughtExceptionLogging' and count(parameter)=0]"
		[Register ("enableUncaughtExceptionLogging", "()V", "")]
		public static unsafe void EnableUncaughtExceptionLogging ()
		{
			if (id_enableUncaughtExceptionLogging == IntPtr.Zero)
				id_enableUncaughtExceptionLogging = JNIEnv.GetStaticMethodID (class_ref, "enableUncaughtExceptionLogging", "()V");
			try {
				JNIEnv.CallStaticVoidMethod  (class_ref, id_enableUncaughtExceptionLogging);
			} finally {
			}
		}

		static IntPtr id_setLogLevel_Lcom_gimbal_logging_GimbalLogLevel_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging']/class[@name='GimbalLogConfig']/method[@name='setLogLevel' and count(parameter)=1 and parameter[1][@type='com.gimbal.logging.GimbalLogLevel']]"
		[Register ("setLogLevel", "(Lcom/gimbal/logging/GimbalLogLevel;)V", "")]
		public static unsafe void SetLogLevel (global::Com.Gimbal.Logging.GimbalLogLevel p0)
		{
			if (id_setLogLevel_Lcom_gimbal_logging_GimbalLogLevel_ == IntPtr.Zero)
				id_setLogLevel_Lcom_gimbal_logging_GimbalLogLevel_ = JNIEnv.GetStaticMethodID (class_ref, "setLogLevel", "(Lcom/gimbal/logging/GimbalLogLevel;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_setLogLevel_Lcom_gimbal_logging_GimbalLogLevel_, __args);
			} finally {
			}
		}

	}
}
