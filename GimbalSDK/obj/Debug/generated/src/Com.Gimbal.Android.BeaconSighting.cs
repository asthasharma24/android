using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconSighting']"
	[global::Android.Runtime.Register ("com/gimbal/android/BeaconSighting", DoNotGenerateAcw=true)]
	public partial class BeaconSighting : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/BeaconSighting", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (BeaconSighting); }
		}

		protected BeaconSighting (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconSighting']/constructor[@name='BeaconSighting' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe BeaconSighting ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (BeaconSighting)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getBeacon;
#pragma warning disable 0169
		static Delegate GetGetBeaconHandler ()
		{
			if (cb_getBeacon == null)
				cb_getBeacon = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBeacon);
			return cb_getBeacon;
		}

		static IntPtr n_GetBeacon (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.BeaconSighting __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.BeaconSighting> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Beacon);
		}
#pragma warning restore 0169

		static IntPtr id_getBeacon;
		public virtual unsafe global::Com.Gimbal.Android.Beacon Beacon {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconSighting']/method[@name='getBeacon' and count(parameter)=0]"
			[Register ("getBeacon", "()Lcom/gimbal/android/Beacon;", "GetGetBeaconHandler")]
			get {
				if (id_getBeacon == IntPtr.Zero)
					id_getBeacon = JNIEnv.GetMethodID (class_ref, "getBeacon", "()Lcom/gimbal/android/Beacon;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon> (JNIEnv.CallObjectMethod  (Handle, id_getBeacon), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBeacon", "()Lcom/gimbal/android/Beacon;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getRSSI;
#pragma warning disable 0169
		static Delegate GetGetRSSIHandler ()
		{
			if (cb_getRSSI == null)
				cb_getRSSI = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetRSSI);
			return cb_getRSSI;
		}

		static IntPtr n_GetRSSI (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.BeaconSighting __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.BeaconSighting> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.RSSI);
		}
#pragma warning restore 0169

		static IntPtr id_getRSSI;
		public virtual unsafe global::Java.Lang.Integer RSSI {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconSighting']/method[@name='getRSSI' and count(parameter)=0]"
			[Register ("getRSSI", "()Ljava/lang/Integer;", "GetGetRSSIHandler")]
			get {
				if (id_getRSSI == IntPtr.Zero)
					id_getRSSI = JNIEnv.GetMethodID (class_ref, "getRSSI", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getRSSI), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRSSI", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getTimeInMillis;
#pragma warning disable 0169
		static Delegate GetGetTimeInMillisHandler ()
		{
			if (cb_getTimeInMillis == null)
				cb_getTimeInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetTimeInMillis);
			return cb_getTimeInMillis;
		}

		static long n_GetTimeInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.BeaconSighting __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.BeaconSighting> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.TimeInMillis;
		}
#pragma warning restore 0169

		static IntPtr id_getTimeInMillis;
		public virtual unsafe long TimeInMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconSighting']/method[@name='getTimeInMillis' and count(parameter)=0]"
			[Register ("getTimeInMillis", "()J", "GetGetTimeInMillisHandler")]
			get {
				if (id_getTimeInMillis == IntPtr.Zero)
					id_getTimeInMillis = JNIEnv.GetMethodID (class_ref, "getTimeInMillis", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getTimeInMillis);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTimeInMillis", "()J"));
				} finally {
				}
			}
		}

	}
}
