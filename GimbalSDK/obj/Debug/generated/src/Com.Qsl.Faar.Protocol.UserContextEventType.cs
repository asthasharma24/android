using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserContextEventType']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/UserContextEventType", DoNotGenerateAcw=true)]
	public sealed partial class UserContextEventType : global::Java.Lang.Enum {


		static IntPtr PLACE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserContextEventType']/field[@name='PLACE']"
		[Register ("PLACE")]
		public static global::Com.Qsl.Faar.Protocol.UserContextEventType Place {
			get {
				if (PLACE_jfieldId == IntPtr.Zero)
					PLACE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PLACE", "Lcom/qsl/faar/protocol/UserContextEventType;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PLACE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserContextEventType> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PUSH_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserContextEventType']/field[@name='PUSH']"
		[Register ("PUSH")]
		public static global::Com.Qsl.Faar.Protocol.UserContextEventType Push {
			get {
				if (PUSH_jfieldId == IntPtr.Zero)
					PUSH_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PUSH", "Lcom/qsl/faar/protocol/UserContextEventType;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PUSH_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserContextEventType> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr TIME_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserContextEventType']/field[@name='TIME']"
		[Register ("TIME")]
		public static global::Com.Qsl.Faar.Protocol.UserContextEventType Time {
			get {
				if (TIME_jfieldId == IntPtr.Zero)
					TIME_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "TIME", "Lcom/qsl/faar/protocol/UserContextEventType;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, TIME_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserContextEventType> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/UserContextEventType", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (UserContextEventType); }
		}

		internal UserContextEventType (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_valueOf_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserContextEventType']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/UserContextEventType;", "")]
		public static unsafe global::Com.Qsl.Faar.Protocol.UserContextEventType ValueOf (string p0)
		{
			if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
				id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/UserContextEventType;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				global::Com.Qsl.Faar.Protocol.UserContextEventType __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserContextEventType> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_values;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserContextEventType']/method[@name='values' and count(parameter)=0]"
		[Register ("values", "()[Lcom/qsl/faar/protocol/UserContextEventType;", "")]
		public static unsafe global::Com.Qsl.Faar.Protocol.UserContextEventType[] Values ()
		{
			if (id_values == IntPtr.Zero)
				id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/UserContextEventType;");
			try {
				return (global::Com.Qsl.Faar.Protocol.UserContextEventType[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.UserContextEventType));
			} finally {
			}
		}

	}
}
