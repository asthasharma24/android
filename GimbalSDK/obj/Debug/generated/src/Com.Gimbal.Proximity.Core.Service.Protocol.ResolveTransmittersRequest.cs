using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Service.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersRequest']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/service/protocol/ResolveTransmittersRequest", DoNotGenerateAcw=true)]
	public partial class ResolveTransmittersRequest : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/service/protocol/ResolveTransmittersRequest", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ResolveTransmittersRequest); }
		}

		protected ResolveTransmittersRequest (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersRequest']/constructor[@name='ResolveTransmittersRequest' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ResolveTransmittersRequest ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ResolveTransmittersRequest)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getOauthToken;
#pragma warning disable 0169
		static Delegate GetGetOauthTokenHandler ()
		{
			if (cb_getOauthToken == null)
				cb_getOauthToken = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOauthToken);
			return cb_getOauthToken;
		}

		static IntPtr n_GetOauthToken (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.OauthToken);
		}
#pragma warning restore 0169

		static Delegate cb_setOauthToken_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetOauthToken_Ljava_lang_String_Handler ()
		{
			if (cb_setOauthToken_Ljava_lang_String_ == null)
				cb_setOauthToken_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOauthToken_Ljava_lang_String_);
			return cb_setOauthToken_Ljava_lang_String_;
		}

		static void n_SetOauthToken_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OauthToken = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOauthToken;
		static IntPtr id_setOauthToken_Ljava_lang_String_;
		public virtual unsafe string OauthToken {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersRequest']/method[@name='getOauthToken' and count(parameter)=0]"
			[Register ("getOauthToken", "()Ljava/lang/String;", "GetGetOauthTokenHandler")]
			get {
				if (id_getOauthToken == IntPtr.Zero)
					id_getOauthToken = JNIEnv.GetMethodID (class_ref, "getOauthToken", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getOauthToken), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOauthToken", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersRequest']/method[@name='setOauthToken' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setOauthToken", "(Ljava/lang/String;)V", "GetSetOauthToken_Ljava_lang_String_Handler")]
			set {
				if (id_setOauthToken_Ljava_lang_String_ == IntPtr.Zero)
					id_setOauthToken_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setOauthToken", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOauthToken_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOauthToken", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getPayload;
#pragma warning disable 0169
		static Delegate GetGetPayloadHandler ()
		{
			if (cb_getPayload == null)
				cb_getPayload = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPayload);
			return cb_getPayload;
		}

		static IntPtr n_GetPayload (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Payload);
		}
#pragma warning restore 0169

		static Delegate cb_setPayload_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetPayload_Ljava_lang_String_Handler ()
		{
			if (cb_setPayload_Ljava_lang_String_ == null)
				cb_setPayload_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPayload_Ljava_lang_String_);
			return cb_setPayload_Ljava_lang_String_;
		}

		static void n_SetPayload_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Payload = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPayload;
		static IntPtr id_setPayload_Ljava_lang_String_;
		public virtual unsafe string Payload {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersRequest']/method[@name='getPayload' and count(parameter)=0]"
			[Register ("getPayload", "()Ljava/lang/String;", "GetGetPayloadHandler")]
			get {
				if (id_getPayload == IntPtr.Zero)
					id_getPayload = JNIEnv.GetMethodID (class_ref, "getPayload", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getPayload), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPayload", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersRequest']/method[@name='setPayload' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setPayload", "(Ljava/lang/String;)V", "GetSetPayload_Ljava_lang_String_Handler")]
			set {
				if (id_setPayload_Ljava_lang_String_ == IntPtr.Zero)
					id_setPayload_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setPayload", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPayload_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPayload", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getReceiverUuid;
#pragma warning disable 0169
		static Delegate GetGetReceiverUuidHandler ()
		{
			if (cb_getReceiverUuid == null)
				cb_getReceiverUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetReceiverUuid);
			return cb_getReceiverUuid;
		}

		static IntPtr n_GetReceiverUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ReceiverUuid);
		}
#pragma warning restore 0169

		static Delegate cb_setReceiverUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetReceiverUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setReceiverUuid_Ljava_lang_String_ == null)
				cb_setReceiverUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetReceiverUuid_Ljava_lang_String_);
			return cb_setReceiverUuid_Ljava_lang_String_;
		}

		static void n_SetReceiverUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReceiverUuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getReceiverUuid;
		static IntPtr id_setReceiverUuid_Ljava_lang_String_;
		public virtual unsafe string ReceiverUuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersRequest']/method[@name='getReceiverUuid' and count(parameter)=0]"
			[Register ("getReceiverUuid", "()Ljava/lang/String;", "GetGetReceiverUuidHandler")]
			get {
				if (id_getReceiverUuid == IntPtr.Zero)
					id_getReceiverUuid = JNIEnv.GetMethodID (class_ref, "getReceiverUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getReceiverUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getReceiverUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersRequest']/method[@name='setReceiverUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setReceiverUuid", "(Ljava/lang/String;)V", "GetSetReceiverUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setReceiverUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setReceiverUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setReceiverUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setReceiverUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setReceiverUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getSighting;
#pragma warning disable 0169
		static Delegate GetGetSightingHandler ()
		{
			if (cb_getSighting == null)
				cb_getSighting = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSighting);
			return cb_getSighting;
		}

		static IntPtr n_GetSighting (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Sighting);
		}
#pragma warning restore 0169

		static Delegate cb_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_;
#pragma warning disable 0169
		static Delegate GetSetSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Handler ()
		{
			if (cb_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_ == null)
				cb_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSighting_Lcom_gimbal_proximity_core_sighting_Sighting_);
			return cb_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_;
		}

		static void n_SetSighting_Lcom_gimbal_proximity_core_sighting_Sighting_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ResolveTransmittersRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Proximity.Core.Sighting.Sighting p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.Sighting> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Sighting = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSighting;
		static IntPtr id_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_;
		public virtual unsafe global::Com.Gimbal.Proximity.Core.Sighting.Sighting Sighting {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersRequest']/method[@name='getSighting' and count(parameter)=0]"
			[Register ("getSighting", "()Lcom/gimbal/proximity/core/sighting/Sighting;", "GetGetSightingHandler")]
			get {
				if (id_getSighting == IntPtr.Zero)
					id_getSighting = JNIEnv.GetMethodID (class_ref, "getSighting", "()Lcom/gimbal/proximity/core/sighting/Sighting;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.Sighting> (JNIEnv.CallObjectMethod  (Handle, id_getSighting), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.Sighting> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSighting", "()Lcom/gimbal/proximity/core/sighting/Sighting;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ResolveTransmittersRequest']/method[@name='setSighting' and count(parameter)=1 and parameter[1][@type='com.gimbal.proximity.core.sighting.Sighting']]"
			[Register ("setSighting", "(Lcom/gimbal/proximity/core/sighting/Sighting;)V", "GetSetSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Handler")]
			set {
				if (id_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_ == IntPtr.Zero)
					id_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_ = JNIEnv.GetMethodID (class_ref, "setSighting", "(Lcom/gimbal/proximity/core/sighting/Sighting;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSighting_Lcom_gimbal_proximity_core_sighting_Sighting_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSighting", "(Lcom/gimbal/proximity/core/sighting/Sighting;)V"), __args);
				} finally {
				}
			}
		}

	}
}
