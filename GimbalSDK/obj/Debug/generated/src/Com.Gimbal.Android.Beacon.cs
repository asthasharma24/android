using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon']"
	[global::Android.Runtime.Register ("com/gimbal/android/Beacon", DoNotGenerateAcw=true)]
	public partial class Beacon : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon.BatteryLevel']"
		[global::Android.Runtime.Register ("com/gimbal/android/Beacon$BatteryLevel", DoNotGenerateAcw=true)]
		public sealed partial class BatteryLevel : global::Java.Lang.Enum {


			static IntPtr HIGH_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon.BatteryLevel']/field[@name='HIGH']"
			[Register ("HIGH")]
			public static global::Com.Gimbal.Android.Beacon.BatteryLevel High {
				get {
					if (HIGH_jfieldId == IntPtr.Zero)
						HIGH_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "HIGH", "Lcom/gimbal/android/Beacon$BatteryLevel;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, HIGH_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon.BatteryLevel> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr LOW_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon.BatteryLevel']/field[@name='LOW']"
			[Register ("LOW")]
			public static global::Com.Gimbal.Android.Beacon.BatteryLevel Low {
				get {
					if (LOW_jfieldId == IntPtr.Zero)
						LOW_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "LOW", "Lcom/gimbal/android/Beacon$BatteryLevel;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, LOW_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon.BatteryLevel> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr MEDIUM_HIGH_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon.BatteryLevel']/field[@name='MEDIUM_HIGH']"
			[Register ("MEDIUM_HIGH")]
			public static global::Com.Gimbal.Android.Beacon.BatteryLevel MediumHigh {
				get {
					if (MEDIUM_HIGH_jfieldId == IntPtr.Zero)
						MEDIUM_HIGH_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "MEDIUM_HIGH", "Lcom/gimbal/android/Beacon$BatteryLevel;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, MEDIUM_HIGH_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon.BatteryLevel> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr MEDIUM_LOW_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon.BatteryLevel']/field[@name='MEDIUM_LOW']"
			[Register ("MEDIUM_LOW")]
			public static global::Com.Gimbal.Android.Beacon.BatteryLevel MediumLow {
				get {
					if (MEDIUM_LOW_jfieldId == IntPtr.Zero)
						MEDIUM_LOW_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "MEDIUM_LOW", "Lcom/gimbal/android/Beacon$BatteryLevel;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, MEDIUM_LOW_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon.BatteryLevel> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/gimbal/android/Beacon$BatteryLevel", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (BatteryLevel); }
			}

			internal BatteryLevel (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon.BatteryLevel']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/gimbal/android/Beacon$BatteryLevel;", "")]
			public static unsafe global::Com.Gimbal.Android.Beacon.BatteryLevel ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/gimbal/android/Beacon$BatteryLevel;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Gimbal.Android.Beacon.BatteryLevel __ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon.BatteryLevel> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon.BatteryLevel']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/gimbal/android/Beacon$BatteryLevel;", "")]
			public static unsafe global::Com.Gimbal.Android.Beacon.BatteryLevel[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/gimbal/android/Beacon$BatteryLevel;");
				try {
					return (global::Com.Gimbal.Android.Beacon.BatteryLevel[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Gimbal.Android.Beacon.BatteryLevel));
				} finally {
				}
			}

		}

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/Beacon", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Beacon); }
		}

		protected Beacon (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon']/constructor[@name='Beacon' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Beacon ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Beacon)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getIconURL;
#pragma warning disable 0169
		static Delegate GetGetIconURLHandler ()
		{
			if (cb_getIconURL == null)
				cb_getIconURL = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIconURL);
			return cb_getIconURL;
		}

		static IntPtr n_GetIconURL (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Beacon __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.IconURL);
		}
#pragma warning restore 0169

		static IntPtr id_getIconURL;
		public virtual unsafe string IconURL {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon']/method[@name='getIconURL' and count(parameter)=0]"
			[Register ("getIconURL", "()Ljava/lang/String;", "GetGetIconURLHandler")]
			get {
				if (id_getIconURL == IntPtr.Zero)
					id_getIconURL = JNIEnv.GetMethodID (class_ref, "getIconURL", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getIconURL), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getIconURL", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getIdentifier;
#pragma warning disable 0169
		static Delegate GetGetIdentifierHandler ()
		{
			if (cb_getIdentifier == null)
				cb_getIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIdentifier);
			return cb_getIdentifier;
		}

		static IntPtr n_GetIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Beacon __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Identifier);
		}
#pragma warning restore 0169

		static IntPtr id_getIdentifier;
		public virtual unsafe string Identifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon']/method[@name='getIdentifier' and count(parameter)=0]"
			[Register ("getIdentifier", "()Ljava/lang/String;", "GetGetIdentifierHandler")]
			get {
				if (id_getIdentifier == IntPtr.Zero)
					id_getIdentifier = JNIEnv.GetMethodID (class_ref, "getIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getName;
#pragma warning disable 0169
		static Delegate GetGetNameHandler ()
		{
			if (cb_getName == null)
				cb_getName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetName);
			return cb_getName;
		}

		static IntPtr n_GetName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Beacon __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Name);
		}
#pragma warning restore 0169

		static IntPtr id_getName;
		public virtual unsafe string Name {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon']/method[@name='getName' and count(parameter)=0]"
			[Register ("getName", "()Ljava/lang/String;", "GetGetNameHandler")]
			get {
				if (id_getName == IntPtr.Zero)
					id_getName = JNIEnv.GetMethodID (class_ref, "getName", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getName), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getName", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getTemperature;
#pragma warning disable 0169
		static Delegate GetGetTemperatureHandler ()
		{
			if (cb_getTemperature == null)
				cb_getTemperature = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetTemperature);
			return cb_getTemperature;
		}

		static int n_GetTemperature (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Beacon __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Temperature;
		}
#pragma warning restore 0169

		static IntPtr id_getTemperature;
		public virtual unsafe int Temperature {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon']/method[@name='getTemperature' and count(parameter)=0]"
			[Register ("getTemperature", "()I", "GetGetTemperatureHandler")]
			get {
				if (id_getTemperature == IntPtr.Zero)
					id_getTemperature = JNIEnv.GetMethodID (class_ref, "getTemperature", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getTemperature);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTemperature", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Beacon __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		static IntPtr id_getUuid;
		public virtual unsafe string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler")]
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getBatteryLevel;
#pragma warning disable 0169
		static Delegate GetGetBatteryLevelHandler ()
		{
			if (cb_getBatteryLevel == null)
				cb_getBatteryLevel = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBatteryLevel);
			return cb_getBatteryLevel;
		}

		static IntPtr n_GetBatteryLevel (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Beacon __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetBatteryLevel ());
		}
#pragma warning restore 0169

		static IntPtr id_getBatteryLevel;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Beacon']/method[@name='getBatteryLevel' and count(parameter)=0]"
		[Register ("getBatteryLevel", "()Lcom/gimbal/android/Beacon$BatteryLevel;", "GetGetBatteryLevelHandler")]
		public virtual unsafe global::Com.Gimbal.Android.Beacon.BatteryLevel GetBatteryLevel ()
		{
			if (id_getBatteryLevel == IntPtr.Zero)
				id_getBatteryLevel = JNIEnv.GetMethodID (class_ref, "getBatteryLevel", "()Lcom/gimbal/android/Beacon$BatteryLevel;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon.BatteryLevel> (JNIEnv.CallObjectMethod  (Handle, id_getBatteryLevel), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Beacon.BatteryLevel> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBatteryLevel", "()Lcom/gimbal/android/Beacon$BatteryLevel;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

	}
}
