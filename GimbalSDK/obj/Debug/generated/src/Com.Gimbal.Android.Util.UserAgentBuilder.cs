using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android.Util {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android.util']/class[@name='UserAgentBuilder']"
	[global::Android.Runtime.Register ("com/gimbal/android/util/UserAgentBuilder", DoNotGenerateAcw=true)]
	public partial class UserAgentBuilder : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android.util']/class[@name='UserAgentBuilder']/field[@name='CLOSE_BRACKETS']"
		[Register ("CLOSE_BRACKETS")]
		public const string CloseBrackets = (string) ")";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android.util']/class[@name='UserAgentBuilder']/field[@name='COMMA']"
		[Register ("COMMA")]
		public const string Comma = (string) ",";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android.util']/class[@name='UserAgentBuilder']/field[@name='GIMBAL_SDK_VERSION_KEY']"
		[Register ("GIMBAL_SDK_VERSION_KEY")]
		public const string GimbalSdkVersionKey = (string) "com.gimbal";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android.util']/class[@name='UserAgentBuilder']/field[@name='OPEN_BRACKETS']"
		[Register ("OPEN_BRACKETS")]
		public const string OpenBrackets = (string) "(";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android.util']/class[@name='UserAgentBuilder']/field[@name='PLATFORM']"
		[Register ("PLATFORM")]
		public const string Platform = (string) "Android";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android.util']/class[@name='UserAgentBuilder']/field[@name='SEPARATOR']"
		[Register ("SEPARATOR")]
		public const string Separator = (string) "/";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android.util']/class[@name='UserAgentBuilder']/field[@name='SPACE']"
		[Register ("SPACE")]
		public const string Space = (string) " ";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.android.util']/class[@name='UserAgentBuilder']/field[@name='UNKNOWN']"
		[Register ("UNKNOWN")]
		public const string Unknown = (string) "UNKNOWN";
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/util/UserAgentBuilder", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (UserAgentBuilder); }
		}

		protected UserAgentBuilder (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_content_Context_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android.util']/class[@name='UserAgentBuilder']/constructor[@name='UserAgentBuilder' and count(parameter)=1 and parameter[1][@type='android.content.Context']]"
		[Register (".ctor", "(Landroid/content/Context;)V", "")]
		public unsafe UserAgentBuilder (global::Android.Content.Context p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (UserAgentBuilder)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Landroid/content/Context;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Landroid/content/Context;)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_ == IntPtr.Zero)
					id_ctor_Landroid_content_Context_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Landroid_content_Context_, __args);
			} finally {
			}
		}

		static Delegate cb_getUserAgent_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetGetUserAgent_Ljava_lang_String_Handler ()
		{
			if (cb_getUserAgent_Ljava_lang_String_ == null)
				cb_getUserAgent_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_GetUserAgent_Ljava_lang_String_);
			return cb_getUserAgent_Ljava_lang_String_;
		}

		static IntPtr n_GetUserAgent_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Android.Util.UserAgentBuilder __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Util.UserAgentBuilder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.GetUserAgent (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_getUserAgent_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android.util']/class[@name='UserAgentBuilder']/method[@name='getUserAgent' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("getUserAgent", "(Ljava/lang/String;)Ljava/lang/String;", "GetGetUserAgent_Ljava_lang_String_Handler")]
		public virtual unsafe string GetUserAgent (string p0)
		{
			if (id_getUserAgent_Ljava_lang_String_ == IntPtr.Zero)
				id_getUserAgent_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "getUserAgent", "(Ljava/lang/String;)Ljava/lang/String;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				string __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUserAgent_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUserAgent", "(Ljava/lang/String;)Ljava/lang/String;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
