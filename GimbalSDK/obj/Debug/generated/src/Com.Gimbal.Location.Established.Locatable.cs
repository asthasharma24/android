using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Location.Established {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']"
	[global::Android.Runtime.Register ("com/gimbal/location/established/Locatable", DoNotGenerateAcw=true)]
	public abstract partial class Locatable : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/location/established/Locatable", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Locatable); }
		}

		protected Locatable (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_gimbal_location_established_Locatable_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']/constructor[@name='Locatable' and count(parameter)=1 and parameter[1][@type='com.gimbal.location.established.Locatable']]"
		[Register (".ctor", "(Lcom/gimbal/location/established/Locatable;)V", "")]
		protected unsafe Locatable (global::Com.Gimbal.Location.Established.Locatable p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (Locatable)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Lcom/gimbal/location/established/Locatable;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Lcom/gimbal/location/established/Locatable;)V", __args);
					return;
				}

				if (id_ctor_Lcom_gimbal_location_established_Locatable_ == IntPtr.Zero)
					id_ctor_Lcom_gimbal_location_established_Locatable_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/gimbal/location/established/Locatable;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_gimbal_location_established_Locatable_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Lcom_gimbal_location_established_Locatable_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_J;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']/constructor[@name='Locatable' and count(parameter)=1 and parameter[1][@type='long']]"
		[Register (".ctor", "(J)V", "")]
		protected unsafe Locatable (long p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (Locatable)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(J)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(J)V", __args);
					return;
				}

				if (id_ctor_J == IntPtr.Zero)
					id_ctor_J = JNIEnv.GetMethodID (class_ref, "<init>", "(J)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_J, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_J, __args);
			} finally {
			}
		}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']/constructor[@name='Locatable' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Locatable ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Locatable)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getCreatedAt;
#pragma warning disable 0169
		static Delegate GetGetCreatedAtHandler ()
		{
			if (cb_getCreatedAt == null)
				cb_getCreatedAt = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCreatedAt);
			return cb_getCreatedAt;
		}

		static IntPtr n_GetCreatedAt (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Locatable __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Locatable> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.CreatedAt);
		}
#pragma warning restore 0169

		static IntPtr id_getCreatedAt;
		public virtual unsafe global::Java.Lang.Long CreatedAt {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']/method[@name='getCreatedAt' and count(parameter)=0]"
			[Register ("getCreatedAt", "()Ljava/lang/Long;", "GetGetCreatedAtHandler")]
			get {
				if (id_getCreatedAt == IntPtr.Zero)
					id_getCreatedAt = JNIEnv.GetMethodID (class_ref, "getCreatedAt", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getCreatedAt), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCreatedAt", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getDuration;
#pragma warning disable 0169
		static Delegate GetGetDurationHandler ()
		{
			if (cb_getDuration == null)
				cb_getDuration = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetDuration);
			return cb_getDuration;
		}

		static long n_GetDuration (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Locatable __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Locatable> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Duration;
		}
#pragma warning restore 0169

		static IntPtr id_getDuration;
		public virtual unsafe long Duration {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']/method[@name='getDuration' and count(parameter)=0]"
			[Register ("getDuration", "()J", "GetGetDurationHandler")]
			get {
				if (id_getDuration == IntPtr.Zero)
					id_getDuration = JNIEnv.GetMethodID (class_ref, "getDuration", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getDuration);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDuration", "()J"));
				} finally {
				}
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Locatable __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Locatable> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Id);
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		public virtual unsafe string Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/String;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getTimeRange;
#pragma warning disable 0169
		static Delegate GetGetTimeRangeHandler ()
		{
			if (cb_getTimeRange == null)
				cb_getTimeRange = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTimeRange);
			return cb_getTimeRange;
		}

		static IntPtr n_GetTimeRange (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Locatable __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Locatable> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.TimeRange);
		}
#pragma warning restore 0169

		static IntPtr id_getTimeRange;
		public virtual unsafe global::Com.Gimbal.Location.Established.WeekRange TimeRange {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']/method[@name='getTimeRange' and count(parameter)=0]"
			[Register ("getTimeRange", "()Lcom/gimbal/location/established/WeekRange;", "GetGetTimeRangeHandler")]
			get {
				if (id_getTimeRange == IntPtr.Zero)
					id_getTimeRange = JNIEnv.GetMethodID (class_ref, "getTimeRange", "()Lcom/gimbal/location/established/WeekRange;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.WeekRange> (JNIEnv.CallObjectMethod  (Handle, id_getTimeRange), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.WeekRange> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTimeRange", "()Lcom/gimbal/location/established/WeekRange;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_age;
#pragma warning disable 0169
		static Delegate GetAgeHandler ()
		{
			if (cb_age == null)
				cb_age = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_Age);
			return cb_age;
		}

		static long n_Age (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Locatable __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Locatable> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Age ();
		}
#pragma warning restore 0169

		static IntPtr id_age;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']/method[@name='age' and count(parameter)=0]"
		[Register ("age", "()J", "GetAgeHandler")]
		public virtual unsafe long Age ()
		{
			if (id_age == IntPtr.Zero)
				id_age = JNIEnv.GetMethodID (class_ref, "age", "()J");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallLongMethod  (Handle, id_age);
				else
					return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "age", "()J"));
			} finally {
			}
		}

		static Delegate cb_createId_Ljava_lang_Class_;
#pragma warning disable 0169
		static Delegate GetCreateId_Ljava_lang_Class_Handler ()
		{
			if (cb_createId_Ljava_lang_Class_ == null)
				cb_createId_Ljava_lang_Class_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_CreateId_Ljava_lang_Class_);
			return cb_createId_Ljava_lang_Class_;
		}

		static IntPtr n_CreateId_Ljava_lang_Class_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Location.Established.Locatable __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Locatable> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.CreateId (p0));
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']/method[@name='createId' and count(parameter)=1 and parameter[1][@type='java.lang.Class&lt;?&gt;']]"
		[Register ("createId", "(Ljava/lang/Class;)Ljava/lang/String;", "GetCreateId_Ljava_lang_Class_Handler")]
		protected abstract string CreateId (global::Java.Lang.Class p0);

		static Delegate cb_inRange_Lcom_gimbal_location_established_Locatable_I;
#pragma warning disable 0169
		static Delegate GetInRange_Lcom_gimbal_location_established_Locatable_IHandler ()
		{
			if (cb_inRange_Lcom_gimbal_location_established_Locatable_I == null)
				cb_inRange_Lcom_gimbal_location_established_Locatable_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, int, bool>) n_InRange_Lcom_gimbal_location_established_Locatable_I);
			return cb_inRange_Lcom_gimbal_location_established_Locatable_I;
		}

		static bool n_InRange_Lcom_gimbal_location_established_Locatable_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Com.Gimbal.Location.Established.Locatable __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Locatable> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Location.Established.Locatable p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Locatable> (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.InRange (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_inRange_Lcom_gimbal_location_established_Locatable_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']/method[@name='inRange' and count(parameter)=2 and parameter[1][@type='com.gimbal.location.established.Locatable'] and parameter[2][@type='int']]"
		[Register ("inRange", "(Lcom/gimbal/location/established/Locatable;I)Z", "GetInRange_Lcom_gimbal_location_established_Locatable_IHandler")]
		public virtual unsafe bool InRange (global::Com.Gimbal.Location.Established.Locatable p0, int p1)
		{
			if (id_inRange_Lcom_gimbal_location_established_Locatable_I == IntPtr.Zero)
				id_inRange_Lcom_gimbal_location_established_Locatable_I = JNIEnv.GetMethodID (class_ref, "inRange", "(Lcom/gimbal/location/established/Locatable;I)Z");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				bool __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.CallBooleanMethod  (Handle, id_inRange_Lcom_gimbal_location_established_Locatable_I, __args);
				else
					__ret = JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "inRange", "(Lcom/gimbal/location/established/Locatable;I)Z"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_timeTo_I;
#pragma warning disable 0169
		static Delegate GetTimeTo_IHandler ()
		{
			if (cb_timeTo_I == null)
				cb_timeTo_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, int>) n_TimeTo_I);
			return cb_timeTo_I;
		}

		static int n_TimeTo_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Gimbal.Location.Established.Locatable __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Locatable> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.TimeTo (p0);
		}
#pragma warning restore 0169

		static IntPtr id_timeTo_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']/method[@name='timeTo' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("timeTo", "(I)I", "GetTimeTo_IHandler")]
		public virtual unsafe int TimeTo (int p0)
		{
			if (id_timeTo_I == IntPtr.Zero)
				id_timeTo_I = JNIEnv.GetMethodID (class_ref, "timeTo", "(I)I");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					return JNIEnv.CallIntMethod  (Handle, id_timeTo_I, __args);
				else
					return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "timeTo", "(I)I"), __args);
			} finally {
			}
		}

	}

	[global::Android.Runtime.Register ("com/gimbal/location/established/Locatable", DoNotGenerateAcw=true)]
	internal partial class LocatableInvoker : Locatable {

		public LocatableInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (LocatableInvoker); }
		}

		static IntPtr id_createId_Ljava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Locatable']/method[@name='createId' and count(parameter)=1 and parameter[1][@type='java.lang.Class&lt;?&gt;']]"
		[Register ("createId", "(Ljava/lang/Class;)Ljava/lang/String;", "GetCreateId_Ljava_lang_Class_Handler")]
		protected override unsafe string CreateId (global::Java.Lang.Class p0)
		{
			if (id_createId_Ljava_lang_Class_ == IntPtr.Zero)
				id_createId_Ljava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "createId", "(Ljava/lang/Class;)Ljava/lang/String;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				string __ret = JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_createId_Ljava_lang_Class_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

	}

}
