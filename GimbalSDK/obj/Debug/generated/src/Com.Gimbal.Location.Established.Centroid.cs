using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Location.Established {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Centroid']"
	[global::Android.Runtime.Register ("com/gimbal/location/established/Centroid", DoNotGenerateAcw=true)]
	public partial class Centroid : global::Com.Gimbal.Location.Established.Point {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/location/established/Centroid", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Centroid); }
		}

		protected Centroid (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_gimbal_location_established_Centroid_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Centroid']/constructor[@name='Centroid' and count(parameter)=1 and parameter[1][@type='com.gimbal.location.established.Centroid']]"
		[Register (".ctor", "(Lcom/gimbal/location/established/Centroid;)V", "")]
		public unsafe Centroid (global::Com.Gimbal.Location.Established.Centroid p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (Centroid)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Lcom/gimbal/location/established/Centroid;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Lcom/gimbal/location/established/Centroid;)V", __args);
					return;
				}

				if (id_ctor_Lcom_gimbal_location_established_Centroid_ == IntPtr.Zero)
					id_ctor_Lcom_gimbal_location_established_Centroid_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/gimbal/location/established/Centroid;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_gimbal_location_established_Centroid_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Lcom_gimbal_location_established_Centroid_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Ljava_util_List_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Centroid']/constructor[@name='Centroid' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;? extends com.gimbal.location.established.Point&gt;']]"
		[Register (".ctor", "(Ljava/util/List;)V", "")]
		public unsafe Centroid (global::System.Collections.Generic.IList<global::Com.Gimbal.Location.Established.Point> p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			IntPtr native_p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.Point>.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				if (GetType () != typeof (Centroid)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Ljava/util/List;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Ljava/util/List;)V", __args);
					return;
				}

				if (id_ctor_Ljava_util_List_ == IntPtr.Zero)
					id_ctor_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Ljava/util/List;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Ljava_util_List_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Ljava_util_List_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_getRadius;
#pragma warning disable 0169
		static Delegate GetGetRadiusHandler ()
		{
			if (cb_getRadius == null)
				cb_getRadius = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_GetRadius);
			return cb_getRadius;
		}

		static double n_GetRadius (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Centroid __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Centroid> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Radius;
		}
#pragma warning restore 0169

		static IntPtr id_getRadius;
		public virtual unsafe double Radius {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Centroid']/method[@name='getRadius' and count(parameter)=0]"
			[Register ("getRadius", "()D", "GetGetRadiusHandler")]
			get {
				if (id_getRadius == IntPtr.Zero)
					id_getRadius = JNIEnv.GetMethodID (class_ref, "getRadius", "()D");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallDoubleMethod  (Handle, id_getRadius);
					else
						return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRadius", "()D"));
				} finally {
				}
			}
		}

		static Delegate cb_isClose_Lcom_gimbal_location_established_Centroid_D;
#pragma warning disable 0169
		static Delegate GetIsClose_Lcom_gimbal_location_established_Centroid_DHandler ()
		{
			if (cb_isClose_Lcom_gimbal_location_established_Centroid_D == null)
				cb_isClose_Lcom_gimbal_location_established_Centroid_D = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, double, bool>) n_IsClose_Lcom_gimbal_location_established_Centroid_D);
			return cb_isClose_Lcom_gimbal_location_established_Centroid_D;
		}

		static bool n_IsClose_Lcom_gimbal_location_established_Centroid_D (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, double p1)
		{
			global::Com.Gimbal.Location.Established.Centroid __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Centroid> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Location.Established.Centroid p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Centroid> (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.IsClose (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_isClose_Lcom_gimbal_location_established_Centroid_D;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Centroid']/method[@name='isClose' and count(parameter)=2 and parameter[1][@type='com.gimbal.location.established.Centroid'] and parameter[2][@type='double']]"
		[Register ("isClose", "(Lcom/gimbal/location/established/Centroid;D)Z", "GetIsClose_Lcom_gimbal_location_established_Centroid_DHandler")]
		public virtual unsafe bool IsClose (global::Com.Gimbal.Location.Established.Centroid p0, double p1)
		{
			if (id_isClose_Lcom_gimbal_location_established_Centroid_D == IntPtr.Zero)
				id_isClose_Lcom_gimbal_location_established_Centroid_D = JNIEnv.GetMethodID (class_ref, "isClose", "(Lcom/gimbal/location/established/Centroid;D)Z");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				bool __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.CallBooleanMethod  (Handle, id_isClose_Lcom_gimbal_location_established_Centroid_D, __args);
				else
					__ret = JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isClose", "(Lcom/gimbal/location/established/Centroid;D)Z"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_isClose_Lcom_gimbal_location_established_Point_D;
#pragma warning disable 0169
		static Delegate GetIsClose_Lcom_gimbal_location_established_Point_DHandler ()
		{
			if (cb_isClose_Lcom_gimbal_location_established_Point_D == null)
				cb_isClose_Lcom_gimbal_location_established_Point_D = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, double, bool>) n_IsClose_Lcom_gimbal_location_established_Point_D);
			return cb_isClose_Lcom_gimbal_location_established_Point_D;
		}

		static bool n_IsClose_Lcom_gimbal_location_established_Point_D (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, double p1)
		{
			global::Com.Gimbal.Location.Established.Centroid __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Centroid> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Location.Established.Point p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Point> (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.IsClose (p0, p1);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_isClose_Lcom_gimbal_location_established_Point_D;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Centroid']/method[@name='isClose' and count(parameter)=2 and parameter[1][@type='com.gimbal.location.established.Point'] and parameter[2][@type='double']]"
		[Register ("isClose", "(Lcom/gimbal/location/established/Point;D)Z", "GetIsClose_Lcom_gimbal_location_established_Point_DHandler")]
		public virtual unsafe bool IsClose (global::Com.Gimbal.Location.Established.Point p0, double p1)
		{
			if (id_isClose_Lcom_gimbal_location_established_Point_D == IntPtr.Zero)
				id_isClose_Lcom_gimbal_location_established_Point_D = JNIEnv.GetMethodID (class_ref, "isClose", "(Lcom/gimbal/location/established/Point;D)Z");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				bool __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.CallBooleanMethod  (Handle, id_isClose_Lcom_gimbal_location_established_Point_D, __args);
				else
					__ret = JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isClose", "(Lcom/gimbal/location/established/Point;D)Z"), __args);
				return __ret;
			} finally {
			}
		}

	}
}
