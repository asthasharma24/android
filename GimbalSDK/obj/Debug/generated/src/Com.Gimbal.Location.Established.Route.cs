using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Location.Established {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Route']"
	[global::Android.Runtime.Register ("com/gimbal/location/established/Route", DoNotGenerateAcw=true)]
	public partial class Route : global::Com.Gimbal.Location.Established.Aggregation {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/location/established/Route", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Route); }
		}

		protected Route (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Route']/constructor[@name='Route' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Route ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Route)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getEnd;
#pragma warning disable 0169
		static Delegate GetGetEndHandler ()
		{
			if (cb_getEnd == null)
				cb_getEnd = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEnd);
			return cb_getEnd;
		}

		static IntPtr n_GetEnd (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Route __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Route> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.End);
		}
#pragma warning restore 0169

		static IntPtr id_getEnd;
		public virtual unsafe global::Com.Gimbal.Location.Established.Centroid End {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Route']/method[@name='getEnd' and count(parameter)=0]"
			[Register ("getEnd", "()Lcom/gimbal/location/established/Centroid;", "GetGetEndHandler")]
			get {
				if (id_getEnd == IntPtr.Zero)
					id_getEnd = JNIEnv.GetMethodID (class_ref, "getEnd", "()Lcom/gimbal/location/established/Centroid;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Centroid> (JNIEnv.CallObjectMethod  (Handle, id_getEnd), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Centroid> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEnd", "()Lcom/gimbal/location/established/Centroid;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getStart;
#pragma warning disable 0169
		static Delegate GetGetStartHandler ()
		{
			if (cb_getStart == null)
				cb_getStart = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetStart);
			return cb_getStart;
		}

		static IntPtr n_GetStart (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Route __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Route> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Start);
		}
#pragma warning restore 0169

		static IntPtr id_getStart;
		public virtual unsafe global::Com.Gimbal.Location.Established.Centroid Start {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Route']/method[@name='getStart' and count(parameter)=0]"
			[Register ("getStart", "()Lcom/gimbal/location/established/Centroid;", "GetGetStartHandler")]
			get {
				if (id_getStart == IntPtr.Zero)
					id_getStart = JNIEnv.GetMethodID (class_ref, "getStart", "()Lcom/gimbal/location/established/Centroid;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Centroid> (JNIEnv.CallObjectMethod  (Handle, id_getStart), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Centroid> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getStart", "()Lcom/gimbal/location/established/Centroid;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getTrips;
#pragma warning disable 0169
		static Delegate GetGetTripsHandler ()
		{
			if (cb_getTrips == null)
				cb_getTrips = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTrips);
			return cb_getTrips;
		}

		static IntPtr n_GetTrips (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Route __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Route> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.ActiveTrip>.ToLocalJniHandle (__this.Trips);
		}
#pragma warning restore 0169

		static IntPtr id_getTrips;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Location.Established.ActiveTrip> Trips {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Route']/method[@name='getTrips' and count(parameter)=0]"
			[Register ("getTrips", "()Ljava/util/List;", "GetGetTripsHandler")]
			get {
				if (id_getTrips == IntPtr.Zero)
					id_getTrips = JNIEnv.GetMethodID (class_ref, "getTrips", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.ActiveTrip>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getTrips), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.ActiveTrip>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTrips", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_createId_Ljava_lang_Class_;
#pragma warning disable 0169
		static Delegate GetCreateId_Ljava_lang_Class_Handler ()
		{
			if (cb_createId_Ljava_lang_Class_ == null)
				cb_createId_Ljava_lang_Class_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_CreateId_Ljava_lang_Class_);
			return cb_createId_Ljava_lang_Class_;
		}

		static IntPtr n_CreateId_Ljava_lang_Class_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Location.Established.Route __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Route> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.CreateId (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_createId_Ljava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Route']/method[@name='createId' and count(parameter)=1 and parameter[1][@type='java.lang.Class&lt;?&gt;']]"
		[Register ("createId", "(Ljava/lang/Class;)Ljava/lang/String;", "GetCreateId_Ljava_lang_Class_Handler")]
		protected override unsafe string CreateId (global::Java.Lang.Class p0)
		{
			if (id_createId_Ljava_lang_Class_ == IntPtr.Zero)
				id_createId_Ljava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "createId", "(Ljava/lang/Class;)Ljava/lang/String;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				string __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_createId_Ljava_lang_Class_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "createId", "(Ljava/lang/Class;)Ljava/lang/String;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_locatables;
#pragma warning disable 0169
		static Delegate GetLocatablesHandler ()
		{
			if (cb_locatables == null)
				cb_locatables = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_Locatables);
			return cb_locatables;
		}

		static IntPtr n_Locatables (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Route __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Route> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.ActiveTrip>.ToLocalJniHandle (__this.Locatables ());
		}
#pragma warning restore 0169

		static IntPtr id_locatables;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Route']/method[@name='locatables' and count(parameter)=0]"
		[Register ("locatables", "()Ljava/util/List;", "GetLocatablesHandler")]
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Location.Established.ActiveTrip> Locatables ()
		{
			if (id_locatables == IntPtr.Zero)
				id_locatables = JNIEnv.GetMethodID (class_ref, "locatables", "()Ljava/util/List;");
			try {

				if (GetType () == ThresholdType)
					return global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.ActiveTrip>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_locatables), JniHandleOwnership.TransferLocalRef);
				else
					return global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.ActiveTrip>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "locatables", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_put_Lcom_gimbal_location_established_ActiveTrip_;
#pragma warning disable 0169
		static Delegate GetPut_Lcom_gimbal_location_established_ActiveTrip_Handler ()
		{
			if (cb_put_Lcom_gimbal_location_established_ActiveTrip_ == null)
				cb_put_Lcom_gimbal_location_established_ActiveTrip_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_Put_Lcom_gimbal_location_established_ActiveTrip_);
			return cb_put_Lcom_gimbal_location_established_ActiveTrip_;
		}

		static void n_Put_Lcom_gimbal_location_established_ActiveTrip_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Location.Established.Route __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Route> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Location.Established.ActiveTrip p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ActiveTrip> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Put (p0);
		}
#pragma warning restore 0169

		static IntPtr id_put_Lcom_gimbal_location_established_ActiveTrip_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Route']/method[@name='put' and count(parameter)=1 and parameter[1][@type='com.gimbal.location.established.ActiveTrip']]"
		[Register ("put", "(Lcom/gimbal/location/established/ActiveTrip;)V", "GetPut_Lcom_gimbal_location_established_ActiveTrip_Handler")]
		public virtual unsafe void Put (global::Com.Gimbal.Location.Established.ActiveTrip p0)
		{
			if (id_put_Lcom_gimbal_location_established_ActiveTrip_ == IntPtr.Zero)
				id_put_Lcom_gimbal_location_established_ActiveTrip_ = JNIEnv.GetMethodID (class_ref, "put", "(Lcom/gimbal/location/established/ActiveTrip;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_put_Lcom_gimbal_location_established_ActiveTrip_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "put", "(Lcom/gimbal/location/established/ActiveTrip;)V"), __args);
			} finally {
			}
		}

	}
}
