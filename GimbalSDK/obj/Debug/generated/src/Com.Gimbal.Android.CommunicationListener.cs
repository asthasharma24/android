using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationListener']"
	[global::Android.Runtime.Register ("com/gimbal/android/CommunicationListener", DoNotGenerateAcw=true)]
	public abstract partial class CommunicationListener : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/CommunicationListener", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CommunicationListener); }
		}

		protected CommunicationListener (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationListener']/constructor[@name='CommunicationListener' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CommunicationListener ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (CommunicationListener)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_onNotificationClicked_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetOnNotificationClicked_Ljava_util_List_Handler ()
		{
			if (cb_onNotificationClicked_Ljava_util_List_ == null)
				cb_onNotificationClicked_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnNotificationClicked_Ljava_util_List_);
			return cb_onNotificationClicked_Ljava_util_List_;
		}

		static void n_OnNotificationClicked_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Android.CommunicationListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Android.Communication>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnNotificationClicked (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onNotificationClicked_Ljava_util_List_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationListener']/method[@name='onNotificationClicked' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.android.Communication&gt;']]"
		[Register ("onNotificationClicked", "(Ljava/util/List;)V", "GetOnNotificationClicked_Ljava_util_List_Handler")]
		public virtual unsafe void OnNotificationClicked (global::System.Collections.Generic.IList<global::Com.Gimbal.Android.Communication> p0)
		{
			if (id_onNotificationClicked_Ljava_util_List_ == IntPtr.Zero)
				id_onNotificationClicked_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "onNotificationClicked", "(Ljava/util/List;)V");
			IntPtr native_p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Android.Communication>.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_onNotificationClicked_Ljava_util_List_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onNotificationClicked", "(Ljava/util/List;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Push_I;
#pragma warning disable 0169
		static Delegate GetPrepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Push_IHandler ()
		{
			if (cb_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Push_I == null)
				cb_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Push_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, int, IntPtr>) n_PrepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Push_I);
			return cb_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Push_I;
		}

		static IntPtr n_PrepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Push_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1, int p2)
		{
			global::Com.Gimbal.Android.CommunicationListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.Communication p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Communication> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.Push p1 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Push> (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.PrepareCommunicationForDisplay (p0, p1, p2));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Push_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationListener']/method[@name='prepareCommunicationForDisplay' and count(parameter)=3 and parameter[1][@type='com.gimbal.android.Communication'] and parameter[2][@type='com.gimbal.android.Push'] and parameter[3][@type='int']]"
		[Register ("prepareCommunicationForDisplay", "(Lcom/gimbal/android/Communication;Lcom/gimbal/android/Push;I)Landroid/app/Notification$Builder;", "GetPrepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Push_IHandler")]
		public virtual unsafe global::Android.App.Notification.Builder PrepareCommunicationForDisplay (global::Com.Gimbal.Android.Communication p0, global::Com.Gimbal.Android.Push p1, int p2)
		{
			if (id_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Push_I == IntPtr.Zero)
				id_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Push_I = JNIEnv.GetMethodID (class_ref, "prepareCommunicationForDisplay", "(Lcom/gimbal/android/Communication;Lcom/gimbal/android/Push;I)Landroid/app/Notification$Builder;");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);

				global::Android.App.Notification.Builder __ret;
				if (GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Android.App.Notification.Builder> (JNIEnv.CallObjectMethod  (Handle, id_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Push_I, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Android.App.Notification.Builder> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "prepareCommunicationForDisplay", "(Lcom/gimbal/android/Communication;Lcom/gimbal/android/Push;I)Landroid/app/Notification$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_I;
#pragma warning disable 0169
		static Delegate GetPrepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_IHandler ()
		{
			if (cb_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_I == null)
				cb_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, int, IntPtr>) n_PrepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_I);
			return cb_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_I;
		}

		static IntPtr n_PrepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1, int p2)
		{
			global::Com.Gimbal.Android.CommunicationListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.Communication p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Communication> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.Visit p1 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Visit> (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.PrepareCommunicationForDisplay (p0, p1, p2));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationListener']/method[@name='prepareCommunicationForDisplay' and count(parameter)=3 and parameter[1][@type='com.gimbal.android.Communication'] and parameter[2][@type='com.gimbal.android.Visit'] and parameter[3][@type='int']]"
		[Register ("prepareCommunicationForDisplay", "(Lcom/gimbal/android/Communication;Lcom/gimbal/android/Visit;I)Landroid/app/Notification$Builder;", "GetPrepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_IHandler")]
		public virtual unsafe global::Android.App.Notification.Builder PrepareCommunicationForDisplay (global::Com.Gimbal.Android.Communication p0, global::Com.Gimbal.Android.Visit p1, int p2)
		{
			if (id_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_I == IntPtr.Zero)
				id_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_I = JNIEnv.GetMethodID (class_ref, "prepareCommunicationForDisplay", "(Lcom/gimbal/android/Communication;Lcom/gimbal/android/Visit;I)Landroid/app/Notification$Builder;");
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);

				global::Android.App.Notification.Builder __ret;
				if (GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Android.App.Notification.Builder> (JNIEnv.CallObjectMethod  (Handle, id_prepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_I, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Android.App.Notification.Builder> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "prepareCommunicationForDisplay", "(Lcom/gimbal/android/Communication;Lcom/gimbal/android/Visit;I)Landroid/app/Notification$Builder;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_;
#pragma warning disable 0169
		static Delegate GetPresentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_Handler ()
		{
			if (cb_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_ == null)
				cb_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_PresentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_);
			return cb_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_;
		}

		static IntPtr n_PresentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Android.CommunicationListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaCollection<global::Com.Gimbal.Android.Communication>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.Push p1 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Push> (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = global::Android.Runtime.JavaCollection<global::Com.Gimbal.Android.Communication>.ToLocalJniHandle (__this.PresentNotificationForCommunications (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationListener']/method[@name='presentNotificationForCommunications' and count(parameter)=2 and parameter[1][@type='java.util.Collection&lt;com.gimbal.android.Communication&gt;'] and parameter[2][@type='com.gimbal.android.Push']]"
		[Register ("presentNotificationForCommunications", "(Ljava/util/Collection;Lcom/gimbal/android/Push;)Ljava/util/Collection;", "GetPresentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_Handler")]
		public virtual unsafe global::System.Collections.Generic.ICollection<global::Com.Gimbal.Android.Communication> PresentNotificationForCommunications (global::System.Collections.Generic.ICollection<global::Com.Gimbal.Android.Communication> p0, global::Com.Gimbal.Android.Push p1)
		{
			if (id_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_ == IntPtr.Zero)
				id_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_ = JNIEnv.GetMethodID (class_ref, "presentNotificationForCommunications", "(Ljava/util/Collection;Lcom/gimbal/android/Push;)Ljava/util/Collection;");
			IntPtr native_p0 = global::Android.Runtime.JavaCollection<global::Com.Gimbal.Android.Communication>.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (p1);

				global::System.Collections.Generic.ICollection<global::Com.Gimbal.Android.Communication> __ret;
				if (GetType () == ThresholdType)
					__ret = global::Android.Runtime.JavaCollection<global::Com.Gimbal.Android.Communication>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Android.Runtime.JavaCollection<global::Com.Gimbal.Android.Communication>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "presentNotificationForCommunications", "(Ljava/util/Collection;Lcom/gimbal/android/Push;)Ljava/util/Collection;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_;
#pragma warning disable 0169
		static Delegate GetPresentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_Handler ()
		{
			if (cb_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_ == null)
				cb_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_PresentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_);
			return cb_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_;
		}

		static IntPtr n_PresentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Android.CommunicationListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaCollection<global::Com.Gimbal.Android.Communication>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.Visit p1 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Visit> (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = global::Android.Runtime.JavaCollection<global::Com.Gimbal.Android.Communication>.ToLocalJniHandle (__this.PresentNotificationForCommunications (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationListener']/method[@name='presentNotificationForCommunications' and count(parameter)=2 and parameter[1][@type='java.util.Collection&lt;com.gimbal.android.Communication&gt;'] and parameter[2][@type='com.gimbal.android.Visit']]"
		[Register ("presentNotificationForCommunications", "(Ljava/util/Collection;Lcom/gimbal/android/Visit;)Ljava/util/Collection;", "GetPresentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_Handler")]
		public virtual unsafe global::System.Collections.Generic.ICollection<global::Com.Gimbal.Android.Communication> PresentNotificationForCommunications (global::System.Collections.Generic.ICollection<global::Com.Gimbal.Android.Communication> p0, global::Com.Gimbal.Android.Visit p1)
		{
			if (id_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_ == IntPtr.Zero)
				id_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_ = JNIEnv.GetMethodID (class_ref, "presentNotificationForCommunications", "(Ljava/util/Collection;Lcom/gimbal/android/Visit;)Ljava/util/Collection;");
			IntPtr native_p0 = global::Android.Runtime.JavaCollection<global::Com.Gimbal.Android.Communication>.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (p1);

				global::System.Collections.Generic.ICollection<global::Com.Gimbal.Android.Communication> __ret;
				if (GetType () == ThresholdType)
					__ret = global::Android.Runtime.JavaCollection<global::Com.Gimbal.Android.Communication>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_presentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Android.Runtime.JavaCollection<global::Com.Gimbal.Android.Communication>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "presentNotificationForCommunications", "(Ljava/util/Collection;Lcom/gimbal/android/Visit;)Ljava/util/Collection;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}

	[global::Android.Runtime.Register ("com/gimbal/android/CommunicationListener", DoNotGenerateAcw=true)]
	internal partial class CommunicationListenerInvoker : CommunicationListener {

		public CommunicationListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (CommunicationListenerInvoker); }
		}

	}

}
