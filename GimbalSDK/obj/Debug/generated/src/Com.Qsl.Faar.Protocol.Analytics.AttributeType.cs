using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Analytics {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/AttributeType", DoNotGenerateAcw=true)]
	public partial class AttributeType : global::Java.Lang.Object {

		// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.AR']"
		[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/AttributeType$AR", DoNotGenerateAcw=true)]
		public sealed partial class AR : global::Java.Lang.Enum {


			static IntPtr CAMPAIGN_ID_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.AR']/field[@name='CAMPAIGN_ID']"
			[Register ("CAMPAIGN_ID")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.AR CampaignId {
				get {
					if (CAMPAIGN_ID_jfieldId == IntPtr.Zero)
						CAMPAIGN_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CAMPAIGN_ID", "Lcom/qsl/faar/protocol/analytics/AttributeType$AR;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CAMPAIGN_ID_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.AR> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr TARGET_NAME_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.AR']/field[@name='TARGET_NAME']"
			[Register ("TARGET_NAME")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.AR TargetName {
				get {
					if (TARGET_NAME_jfieldId == IntPtr.Zero)
						TARGET_NAME_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "TARGET_NAME", "Lcom/qsl/faar/protocol/analytics/AttributeType$AR;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, TARGET_NAME_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.AR> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/AttributeType$AR", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (AR); }
			}

			internal AR (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.AR']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/AttributeType$AR;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.AR ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/AttributeType$AR;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.AR __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.AR> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.AR']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/qsl/faar/protocol/analytics/AttributeType$AR;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.AR[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/analytics/AttributeType$AR;");
				try {
					return (global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.AR[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.AR));
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.CONTENT']"
		[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/AttributeType$CONTENT", DoNotGenerateAcw=true)]
		public sealed partial class CONTENT : global::Java.Lang.Enum {


			static IntPtr CONTENT_ID_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.CONTENT']/field[@name='CONTENT_ID']"
			[Register ("CONTENT_ID")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT ContentId {
				get {
					if (CONTENT_ID_jfieldId == IntPtr.Zero)
						CONTENT_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CONTENT_ID", "Lcom/qsl/faar/protocol/analytics/AttributeType$CONTENT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CONTENT_ID_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr PLACE_EVENT_TYPE_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.CONTENT']/field[@name='PLACE_EVENT_TYPE']"
			[Register ("PLACE_EVENT_TYPE")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT PlaceEventType {
				get {
					if (PLACE_EVENT_TYPE_jfieldId == IntPtr.Zero)
						PLACE_EVENT_TYPE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PLACE_EVENT_TYPE", "Lcom/qsl/faar/protocol/analytics/AttributeType$CONTENT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PLACE_EVENT_TYPE_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr TRIGGER_ID_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.CONTENT']/field[@name='TRIGGER_ID']"
			[Register ("TRIGGER_ID")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT TriggerId {
				get {
					if (TRIGGER_ID_jfieldId == IntPtr.Zero)
						TRIGGER_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "TRIGGER_ID", "Lcom/qsl/faar/protocol/analytics/AttributeType$CONTENT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, TRIGGER_ID_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr TRIGGER_TYPE_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.CONTENT']/field[@name='TRIGGER_TYPE']"
			[Register ("TRIGGER_TYPE")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT TriggerType {
				get {
					if (TRIGGER_TYPE_jfieldId == IntPtr.Zero)
						TRIGGER_TYPE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "TRIGGER_TYPE", "Lcom/qsl/faar/protocol/analytics/AttributeType$CONTENT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, TRIGGER_TYPE_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/AttributeType$CONTENT", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (CONTENT); }
			}

			internal CONTENT (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.CONTENT']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/AttributeType$CONTENT;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/AttributeType$CONTENT;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.CONTENT']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/qsl/faar/protocol/analytics/AttributeType$CONTENT;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/analytics/AttributeType$CONTENT;");
				try {
					return (global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.CONTENT));
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.DEVICE']"
		[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/AttributeType$DEVICE", DoNotGenerateAcw=true)]
		public sealed partial class DEVICE : global::Java.Lang.Enum {


			static IntPtr APP_VERISON_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.DEVICE']/field[@name='APP_VERISON']"
			[Register ("APP_VERISON")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.DEVICE AppVerison {
				get {
					if (APP_VERISON_jfieldId == IntPtr.Zero)
						APP_VERISON_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "APP_VERISON", "Lcom/qsl/faar/protocol/analytics/AttributeType$DEVICE;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, APP_VERISON_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.DEVICE> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr OS_VERSION_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.DEVICE']/field[@name='OS_VERSION']"
			[Register ("OS_VERSION")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.DEVICE OsVersion {
				get {
					if (OS_VERSION_jfieldId == IntPtr.Zero)
						OS_VERSION_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "OS_VERSION", "Lcom/qsl/faar/protocol/analytics/AttributeType$DEVICE;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, OS_VERSION_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.DEVICE> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/AttributeType$DEVICE", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (DEVICE); }
			}

			internal DEVICE (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.DEVICE']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/AttributeType$DEVICE;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.DEVICE ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/AttributeType$DEVICE;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.DEVICE __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.DEVICE> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.DEVICE']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/qsl/faar/protocol/analytics/AttributeType$DEVICE;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.DEVICE[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/analytics/AttributeType$DEVICE;");
				try {
					return (global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.DEVICE[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.DEVICE));
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.IR']"
		[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/AttributeType$IR", DoNotGenerateAcw=true)]
		public sealed partial class IR : global::Java.Lang.Enum {


			static IntPtr IR_DURATION_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.IR']/field[@name='IR_DURATION']"
			[Register ("IR_DURATION")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR IrDuration {
				get {
					if (IR_DURATION_jfieldId == IntPtr.Zero)
						IR_DURATION_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "IR_DURATION", "Lcom/qsl/faar/protocol/analytics/AttributeType$IR;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, IR_DURATION_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr IR_LAUNCHED_TIME_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.IR']/field[@name='IR_LAUNCHED_TIME']"
			[Register ("IR_LAUNCHED_TIME")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR IrLaunchedTime {
				get {
					if (IR_LAUNCHED_TIME_jfieldId == IntPtr.Zero)
						IR_LAUNCHED_TIME_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "IR_LAUNCHED_TIME", "Lcom/qsl/faar/protocol/analytics/AttributeType$IR;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, IR_LAUNCHED_TIME_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr IR_TARGET_ACQUIRED_TIME_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.IR']/field[@name='IR_TARGET_ACQUIRED_TIME']"
			[Register ("IR_TARGET_ACQUIRED_TIME")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR IrTargetAcquiredTime {
				get {
					if (IR_TARGET_ACQUIRED_TIME_jfieldId == IntPtr.Zero)
						IR_TARGET_ACQUIRED_TIME_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "IR_TARGET_ACQUIRED_TIME", "Lcom/qsl/faar/protocol/analytics/AttributeType$IR;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, IR_TARGET_ACQUIRED_TIME_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr IR_TARGET_NAME_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.IR']/field[@name='IR_TARGET_NAME']"
			[Register ("IR_TARGET_NAME")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR IrTargetName {
				get {
					if (IR_TARGET_NAME_jfieldId == IntPtr.Zero)
						IR_TARGET_NAME_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "IR_TARGET_NAME", "Lcom/qsl/faar/protocol/analytics/AttributeType$IR;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, IR_TARGET_NAME_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/AttributeType$IR", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (IR); }
			}

			internal IR (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.IR']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/AttributeType$IR;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/AttributeType$IR;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.IR']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/qsl/faar/protocol/analytics/AttributeType$IR;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/analytics/AttributeType$IR;");
				try {
					return (global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.IR));
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']"
		[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT", DoNotGenerateAcw=true)]
		public sealed partial class USER_CONTEXT : global::Java.Lang.Enum {


			static IntPtr CAMPAIGN_ID_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/field[@name='CAMPAIGN_ID']"
			[Register ("CAMPAIGN_ID")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT CampaignId {
				get {
					if (CAMPAIGN_ID_jfieldId == IntPtr.Zero)
						CAMPAIGN_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CAMPAIGN_ID", "Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CAMPAIGN_ID_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr CLICK_SOURCE_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/field[@name='CLICK_SOURCE']"
			[Register ("CLICK_SOURCE")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT ClickSource {
				get {
					if (CLICK_SOURCE_jfieldId == IntPtr.Zero)
						CLICK_SOURCE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CLICK_SOURCE", "Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CLICK_SOURCE_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr CONTENT_URL_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/field[@name='CONTENT_URL']"
			[Register ("CONTENT_URL")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT ContentUrl {
				get {
					if (CONTENT_URL_jfieldId == IntPtr.Zero)
						CONTENT_URL_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CONTENT_URL", "Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CONTENT_URL_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr CURRENT_PLACE_ID_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/field[@name='CURRENT_PLACE_ID']"
			[Register ("CURRENT_PLACE_ID")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT CurrentPlaceId {
				get {
					if (CURRENT_PLACE_ID_jfieldId == IntPtr.Zero)
						CURRENT_PLACE_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CURRENT_PLACE_ID", "Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CURRENT_PLACE_ID_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr DWELL_TIME_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/field[@name='DWELL_TIME']"
			[Register ("DWELL_TIME")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT DwellTime {
				get {
					if (DWELL_TIME_jfieldId == IntPtr.Zero)
						DWELL_TIME_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "DWELL_TIME", "Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, DWELL_TIME_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr IDENTIFIER_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/field[@name='IDENTIFIER']"
			[Register ("IDENTIFIER")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT Identifier {
				get {
					if (IDENTIFIER_jfieldId == IntPtr.Zero)
						IDENTIFIER_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "IDENTIFIER", "Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, IDENTIFIER_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr LAST_PLACE_ID_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/field[@name='LAST_PLACE_ID']"
			[Register ("LAST_PLACE_ID")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT LastPlaceId {
				get {
					if (LAST_PLACE_ID_jfieldId == IntPtr.Zero)
						LAST_PLACE_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "LAST_PLACE_ID", "Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, LAST_PLACE_ID_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr ORGANIZATION_ID_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/field[@name='ORGANIZATION_ID']"
			[Register ("ORGANIZATION_ID")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT OrganizationId {
				get {
					if (ORGANIZATION_ID_jfieldId == IntPtr.Zero)
						ORGANIZATION_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "ORGANIZATION_ID", "Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, ORGANIZATION_ID_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr PLACE_EVENT_TYPE_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/field[@name='PLACE_EVENT_TYPE']"
			[Register ("PLACE_EVENT_TYPE")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT PlaceEventType {
				get {
					if (PLACE_EVENT_TYPE_jfieldId == IntPtr.Zero)
						PLACE_EVENT_TYPE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PLACE_EVENT_TYPE", "Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PLACE_EVENT_TYPE_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr PLACE_ID_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/field[@name='PLACE_ID']"
			[Register ("PLACE_ID")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT PlaceId {
				get {
					if (PLACE_ID_jfieldId == IntPtr.Zero)
						PLACE_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PLACE_ID", "Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PLACE_ID_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr SCHEME_URL_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/field[@name='SCHEME_URL']"
			[Register ("SCHEME_URL")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT SchemeUrl {
				get {
					if (SCHEME_URL_jfieldId == IntPtr.Zero)
						SCHEME_URL_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "SCHEME_URL", "Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, SCHEME_URL_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr SOURCE_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/field[@name='SOURCE']"
			[Register ("SOURCE")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT Source {
				get {
					if (SOURCE_jfieldId == IntPtr.Zero)
						SOURCE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "SOURCE", "Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, SOURCE_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (USER_CONTEXT); }
			}

			internal USER_CONTEXT (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.USER_CONTEXT']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/analytics/AttributeType$USER_CONTEXT;");
				try {
					return (global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.USER_CONTEXT));
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']"
		[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS", DoNotGenerateAcw=true)]
		public sealed partial class V2_EVENTS : global::Java.Lang.Enum {


			static IntPtr AT_TIME_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']/field[@name='AT_TIME']"
			[Register ("AT_TIME")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS AtTime {
				get {
					if (AT_TIME_jfieldId == IntPtr.Zero)
						AT_TIME_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "AT_TIME", "Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, AT_TIME_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr BEACON_ACTUAL_RSSI_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']/field[@name='BEACON_ACTUAL_RSSI']"
			[Register ("BEACON_ACTUAL_RSSI")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS BeaconActualRssi {
				get {
					if (BEACON_ACTUAL_RSSI_jfieldId == IntPtr.Zero)
						BEACON_ACTUAL_RSSI_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "BEACON_ACTUAL_RSSI", "Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, BEACON_ACTUAL_RSSI_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr BEACON_TYPE_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']/field[@name='BEACON_TYPE']"
			[Register ("BEACON_TYPE")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS BeaconType {
				get {
					if (BEACON_TYPE_jfieldId == IntPtr.Zero)
						BEACON_TYPE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "BEACON_TYPE", "Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, BEACON_TYPE_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr DWELL_TIME_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']/field[@name='DWELL_TIME']"
			[Register ("DWELL_TIME")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS DwellTime {
				get {
					if (DWELL_TIME_jfieldId == IntPtr.Zero)
						DWELL_TIME_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "DWELL_TIME", "Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, DWELL_TIME_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr EVENT_TYPE_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']/field[@name='EVENT_TYPE']"
			[Register ("EVENT_TYPE")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS EventType {
				get {
					if (EVENT_TYPE_jfieldId == IntPtr.Zero)
						EVENT_TYPE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "EVENT_TYPE", "Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, EVENT_TYPE_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr FENCE_ID_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']/field[@name='FENCE_ID']"
			[Register ("FENCE_ID")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS FenceId {
				get {
					if (FENCE_ID_jfieldId == IntPtr.Zero)
						FENCE_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "FENCE_ID", "Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, FENCE_ID_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr FENCE_TYPE_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']/field[@name='FENCE_TYPE']"
			[Register ("FENCE_TYPE")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS FenceType {
				get {
					if (FENCE_TYPE_jfieldId == IntPtr.Zero)
						FENCE_TYPE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "FENCE_TYPE", "Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, FENCE_TYPE_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr LEFT_TIME_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']/field[@name='LEFT_TIME']"
			[Register ("LEFT_TIME")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS LeftTime {
				get {
					if (LEFT_TIME_jfieldId == IntPtr.Zero)
						LEFT_TIME_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "LEFT_TIME", "Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, LEFT_TIME_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr PLACE_ID_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']/field[@name='PLACE_ID']"
			[Register ("PLACE_ID")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS PlaceId {
				get {
					if (PLACE_ID_jfieldId == IntPtr.Zero)
						PLACE_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PLACE_ID", "Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PLACE_ID_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr VISIT_ID_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']/field[@name='VISIT_ID']"
			[Register ("VISIT_ID")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS VisitId {
				get {
					if (VISIT_ID_jfieldId == IntPtr.Zero)
						VISIT_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "VISIT_ID", "Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, VISIT_ID_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (V2_EVENTS); }
			}

			internal V2_EVENTS (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType.V2_EVENTS']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/analytics/AttributeType$V2_EVENTS;");
				try {
					return (global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.Analytics.AttributeType.V2_EVENTS));
				} finally {
				}
			}

		}

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/AttributeType", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (AttributeType); }
		}

		protected AttributeType (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='AttributeType']/constructor[@name='AttributeType' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe AttributeType ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (AttributeType)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

	}
}
