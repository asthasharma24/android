using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Rest.Context {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.rest.context']/class[@name='AuthenticationProperties']"
	[global::Android.Runtime.Register ("com/gimbal/internal/rest/context/AuthenticationProperties", DoNotGenerateAcw=true)]
	public partial class AuthenticationProperties : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/rest/context/AuthenticationProperties", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (AuthenticationProperties); }
		}

		protected AuthenticationProperties (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.rest.context']/class[@name='AuthenticationProperties']/constructor[@name='AuthenticationProperties' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe AuthenticationProperties ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (AuthenticationProperties)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getUserName;
#pragma warning disable 0169
		static Delegate GetGetUserNameHandler ()
		{
			if (cb_getUserName == null)
				cb_getUserName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUserName);
			return cb_getUserName;
		}

		static IntPtr n_GetUserName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Rest.Context.AuthenticationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Rest.Context.AuthenticationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.UserName);
		}
#pragma warning restore 0169

		static Delegate cb_setUserName_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUserName_Ljava_lang_String_Handler ()
		{
			if (cb_setUserName_Ljava_lang_String_ == null)
				cb_setUserName_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUserName_Ljava_lang_String_);
			return cb_setUserName_Ljava_lang_String_;
		}

		static void n_SetUserName_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Rest.Context.AuthenticationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Rest.Context.AuthenticationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.UserName = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUserName;
		static IntPtr id_setUserName_Ljava_lang_String_;
		public virtual unsafe string UserName {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.rest.context']/class[@name='AuthenticationProperties']/method[@name='getUserName' and count(parameter)=0]"
			[Register ("getUserName", "()Ljava/lang/String;", "GetGetUserNameHandler")]
			get {
				if (id_getUserName == IntPtr.Zero)
					id_getUserName = JNIEnv.GetMethodID (class_ref, "getUserName", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUserName), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUserName", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.rest.context']/class[@name='AuthenticationProperties']/method[@name='setUserName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUserName", "(Ljava/lang/String;)V", "GetSetUserName_Ljava_lang_String_Handler")]
			set {
				if (id_setUserName_Ljava_lang_String_ == IntPtr.Zero)
					id_setUserName_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUserName", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUserName_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUserName", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUserPassword;
#pragma warning disable 0169
		static Delegate GetGetUserPasswordHandler ()
		{
			if (cb_getUserPassword == null)
				cb_getUserPassword = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUserPassword);
			return cb_getUserPassword;
		}

		static IntPtr n_GetUserPassword (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Rest.Context.AuthenticationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Rest.Context.AuthenticationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.UserPassword);
		}
#pragma warning restore 0169

		static Delegate cb_setUserPassword_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUserPassword_Ljava_lang_String_Handler ()
		{
			if (cb_setUserPassword_Ljava_lang_String_ == null)
				cb_setUserPassword_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUserPassword_Ljava_lang_String_);
			return cb_setUserPassword_Ljava_lang_String_;
		}

		static void n_SetUserPassword_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Rest.Context.AuthenticationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Rest.Context.AuthenticationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.UserPassword = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUserPassword;
		static IntPtr id_setUserPassword_Ljava_lang_String_;
		public virtual unsafe string UserPassword {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.rest.context']/class[@name='AuthenticationProperties']/method[@name='getUserPassword' and count(parameter)=0]"
			[Register ("getUserPassword", "()Ljava/lang/String;", "GetGetUserPasswordHandler")]
			get {
				if (id_getUserPassword == IntPtr.Zero)
					id_getUserPassword = JNIEnv.GetMethodID (class_ref, "getUserPassword", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUserPassword), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUserPassword", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.rest.context']/class[@name='AuthenticationProperties']/method[@name='setUserPassword' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUserPassword", "(Ljava/lang/String;)V", "GetSetUserPassword_Ljava_lang_String_Handler")]
			set {
				if (id_setUserPassword_Ljava_lang_String_ == IntPtr.Zero)
					id_setUserPassword_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUserPassword", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUserPassword_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUserPassword", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
