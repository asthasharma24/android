using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Logging.Util {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']"
	[global::Android.Runtime.Register ("com/gimbal/logging/util/LogCapture", DoNotGenerateAcw=true)]
	public partial class LogCapture : global::Java.Lang.Object {

		// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.Flusher']"
		[global::Android.Runtime.Register ("com/gimbal/logging/util/LogCapture$Flusher", DoNotGenerateAcw=true)]
		protected internal partial class Flusher : global::Java.Lang.Object, global::Java.Lang.IRunnable {

			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/gimbal/logging/util/LogCapture$Flusher", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (Flusher); }
			}

			protected Flusher (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.Flusher']/constructor[@name='LogCapture.Flusher' and count(parameter)=0]"
			[Register (".ctor", "()V", "")]
			protected unsafe Flusher ()
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (Handle != IntPtr.Zero)
					return;

				try {
					if (GetType () != typeof (Flusher)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
						return;
					}

					if (id_ctor == IntPtr.Zero)
						id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
				} finally {
				}
			}

			static Delegate cb_run;
#pragma warning disable 0169
			static Delegate GetRunHandler ()
			{
				if (cb_run == null)
					cb_run = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Run);
				return cb_run;
			}

			static void n_Run (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.Flusher __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.Flusher> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.Run ();
			}
#pragma warning restore 0169

			static IntPtr id_run;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.Flusher']/method[@name='run' and count(parameter)=0]"
			[Register ("run", "()V", "GetRunHandler")]
			public virtual unsafe void Run ()
			{
				if (id_run == IntPtr.Zero)
					id_run = JNIEnv.GetMethodID (class_ref, "run", "()V");
				try {

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_run);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "run", "()V"));
				} finally {
				}
			}

			static Delegate cb_start;
#pragma warning disable 0169
			static Delegate GetStartHandler ()
			{
				if (cb_start == null)
					cb_start = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Start);
				return cb_start;
			}

			static void n_Start (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.Flusher __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.Flusher> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.Start ();
			}
#pragma warning restore 0169

			static IntPtr id_start;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.Flusher']/method[@name='start' and count(parameter)=0]"
			[Register ("start", "()V", "GetStartHandler")]
			public virtual unsafe void Start ()
			{
				if (id_start == IntPtr.Zero)
					id_start = JNIEnv.GetMethodID (class_ref, "start", "()V");
				try {

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_start);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "start", "()V"));
				} finally {
				}
			}

			static Delegate cb_stop;
#pragma warning disable 0169
			static Delegate GetStopHandler ()
			{
				if (cb_stop == null)
					cb_stop = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Stop);
				return cb_stop;
			}

			static void n_Stop (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.Flusher __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.Flusher> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.Stop ();
			}
#pragma warning restore 0169

			static IntPtr id_stop;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.Flusher']/method[@name='stop' and count(parameter)=0]"
			[Register ("stop", "()V", "GetStopHandler")]
			public virtual unsafe void Stop ()
			{
				if (id_stop == IntPtr.Zero)
					id_stop = JNIEnv.GetMethodID (class_ref, "stop", "()V");
				try {

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_stop);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "stop", "()V"));
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']"
		[global::Android.Runtime.Register ("com/gimbal/logging/util/LogCapture$LogEntry", DoNotGenerateAcw=true)]
		public partial class LogEntry : global::Java.Lang.Object {


			static IntPtr id_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/field[@name='id']"
			[Register ("id")]
			public long Id {
				get {
					if (id_jfieldId == IntPtr.Zero)
						id_jfieldId = JNIEnv.GetFieldID (class_ref, "id", "J");
					return JNIEnv.GetLongField (Handle, id_jfieldId);
				}
				set {
					if (id_jfieldId == IntPtr.Zero)
						id_jfieldId = JNIEnv.GetFieldID (class_ref, "id", "J");
					try {
						JNIEnv.SetField (Handle, id_jfieldId, value);
					} finally {
					}
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/gimbal/logging/util/LogCapture$LogEntry", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (LogEntry); }
			}

			protected LogEntry (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_ctor;
			// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/constructor[@name='LogCapture.LogEntry' and count(parameter)=0]"
			[Register (".ctor", "()V", "")]
			public unsafe LogEntry ()
				: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
			{
				if (Handle != IntPtr.Zero)
					return;

				try {
					if (GetType () != typeof (LogEntry)) {
						SetHandle (
								global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
								JniHandleOwnership.TransferLocalRef);
						global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
						return;
					}

					if (id_ctor == IntPtr.Zero)
						id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
							JniHandleOwnership.TransferLocalRef);
					JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
				} finally {
				}
			}

			static Delegate cb_getError;
#pragma warning disable 0169
			static Delegate GetGetErrorHandler ()
			{
				if (cb_getError == null)
					cb_getError = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetError);
				return cb_getError;
			}

			static IntPtr n_GetError (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.Error);
			}
#pragma warning restore 0169

			static Delegate cb_setError_Ljava_lang_String_;
#pragma warning disable 0169
			static Delegate GetSetError_Ljava_lang_String_Handler ()
			{
				if (cb_setError_Ljava_lang_String_ == null)
					cb_setError_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetError_Ljava_lang_String_);
				return cb_setError_Ljava_lang_String_;
			}

			static void n_SetError_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.Error = p0;
			}
#pragma warning restore 0169

			static IntPtr id_getError;
			static IntPtr id_setError_Ljava_lang_String_;
			public virtual unsafe string Error {
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='getError' and count(parameter)=0]"
				[Register ("getError", "()Ljava/lang/String;", "GetGetErrorHandler")]
				get {
					if (id_getError == IntPtr.Zero)
						id_getError = JNIEnv.GetMethodID (class_ref, "getError", "()Ljava/lang/String;");
					try {

						if (GetType () == ThresholdType)
							return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getError), JniHandleOwnership.TransferLocalRef);
						else
							return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getError", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
					} finally {
					}
				}
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='setError' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
				[Register ("setError", "(Ljava/lang/String;)V", "GetSetError_Ljava_lang_String_Handler")]
				set {
					if (id_setError_Ljava_lang_String_ == IntPtr.Zero)
						id_setError_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setError", "(Ljava/lang/String;)V");
					IntPtr native_value = JNIEnv.NewString (value);
					try {
						JValue* __args = stackalloc JValue [1];
						__args [0] = new JValue (native_value);

						if (GetType () == ThresholdType)
							JNIEnv.CallVoidMethod  (Handle, id_setError_Ljava_lang_String_, __args);
						else
							JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setError", "(Ljava/lang/String;)V"), __args);
					} finally {
						JNIEnv.DeleteLocalRef (native_value);
					}
				}
			}

			static Delegate cb_getException;
#pragma warning disable 0169
			static Delegate GetGetExceptionHandler ()
			{
				if (cb_getException == null)
					cb_getException = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetException);
				return cb_getException;
			}

			static IntPtr n_GetException (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.Exception);
			}
#pragma warning restore 0169

			static Delegate cb_setException_Ljava_lang_String_;
#pragma warning disable 0169
			static Delegate GetSetException_Ljava_lang_String_Handler ()
			{
				if (cb_setException_Ljava_lang_String_ == null)
					cb_setException_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetException_Ljava_lang_String_);
				return cb_setException_Ljava_lang_String_;
			}

			static void n_SetException_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.Exception = p0;
			}
#pragma warning restore 0169

			static IntPtr id_getException;
			static IntPtr id_setException_Ljava_lang_String_;
			public virtual unsafe string Exception {
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='getException' and count(parameter)=0]"
				[Register ("getException", "()Ljava/lang/String;", "GetGetExceptionHandler")]
				get {
					if (id_getException == IntPtr.Zero)
						id_getException = JNIEnv.GetMethodID (class_ref, "getException", "()Ljava/lang/String;");
					try {

						if (GetType () == ThresholdType)
							return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getException), JniHandleOwnership.TransferLocalRef);
						else
							return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getException", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
					} finally {
					}
				}
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='setException' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
				[Register ("setException", "(Ljava/lang/String;)V", "GetSetException_Ljava_lang_String_Handler")]
				set {
					if (id_setException_Ljava_lang_String_ == IntPtr.Zero)
						id_setException_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setException", "(Ljava/lang/String;)V");
					IntPtr native_value = JNIEnv.NewString (value);
					try {
						JValue* __args = stackalloc JValue [1];
						__args [0] = new JValue (native_value);

						if (GetType () == ThresholdType)
							JNIEnv.CallVoidMethod  (Handle, id_setException_Ljava_lang_String_, __args);
						else
							JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setException", "(Ljava/lang/String;)V"), __args);
					} finally {
						JNIEnv.DeleteLocalRef (native_value);
					}
				}
			}

			static Delegate cb_getLevel;
#pragma warning disable 0169
			static Delegate GetGetLevelHandler ()
			{
				if (cb_getLevel == null)
					cb_getLevel = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLevel);
				return cb_getLevel;
			}

			static IntPtr n_GetLevel (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.Level);
			}
#pragma warning restore 0169

			static Delegate cb_setLevel_Ljava_lang_String_;
#pragma warning disable 0169
			static Delegate GetSetLevel_Ljava_lang_String_Handler ()
			{
				if (cb_setLevel_Ljava_lang_String_ == null)
					cb_setLevel_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLevel_Ljava_lang_String_);
				return cb_setLevel_Ljava_lang_String_;
			}

			static void n_SetLevel_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.Level = p0;
			}
#pragma warning restore 0169

			static IntPtr id_getLevel;
			static IntPtr id_setLevel_Ljava_lang_String_;
			public virtual unsafe string Level {
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='getLevel' and count(parameter)=0]"
				[Register ("getLevel", "()Ljava/lang/String;", "GetGetLevelHandler")]
				get {
					if (id_getLevel == IntPtr.Zero)
						id_getLevel = JNIEnv.GetMethodID (class_ref, "getLevel", "()Ljava/lang/String;");
					try {

						if (GetType () == ThresholdType)
							return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getLevel), JniHandleOwnership.TransferLocalRef);
						else
							return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLevel", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
					} finally {
					}
				}
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='setLevel' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
				[Register ("setLevel", "(Ljava/lang/String;)V", "GetSetLevel_Ljava_lang_String_Handler")]
				set {
					if (id_setLevel_Ljava_lang_String_ == IntPtr.Zero)
						id_setLevel_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setLevel", "(Ljava/lang/String;)V");
					IntPtr native_value = JNIEnv.NewString (value);
					try {
						JValue* __args = stackalloc JValue [1];
						__args [0] = new JValue (native_value);

						if (GetType () == ThresholdType)
							JNIEnv.CallVoidMethod  (Handle, id_setLevel_Ljava_lang_String_, __args);
						else
							JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLevel", "(Ljava/lang/String;)V"), __args);
					} finally {
						JNIEnv.DeleteLocalRef (native_value);
					}
				}
			}

			static Delegate cb_getLogger;
#pragma warning disable 0169
			static Delegate GetGetLoggerHandler ()
			{
				if (cb_getLogger == null)
					cb_getLogger = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLogger);
				return cb_getLogger;
			}

			static IntPtr n_GetLogger (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.Logger);
			}
#pragma warning restore 0169

			static Delegate cb_setLogger_Ljava_lang_String_;
#pragma warning disable 0169
			static Delegate GetSetLogger_Ljava_lang_String_Handler ()
			{
				if (cb_setLogger_Ljava_lang_String_ == null)
					cb_setLogger_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLogger_Ljava_lang_String_);
				return cb_setLogger_Ljava_lang_String_;
			}

			static void n_SetLogger_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.Logger = p0;
			}
#pragma warning restore 0169

			static IntPtr id_getLogger;
			static IntPtr id_setLogger_Ljava_lang_String_;
			public virtual unsafe string Logger {
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='getLogger' and count(parameter)=0]"
				[Register ("getLogger", "()Ljava/lang/String;", "GetGetLoggerHandler")]
				get {
					if (id_getLogger == IntPtr.Zero)
						id_getLogger = JNIEnv.GetMethodID (class_ref, "getLogger", "()Ljava/lang/String;");
					try {

						if (GetType () == ThresholdType)
							return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getLogger), JniHandleOwnership.TransferLocalRef);
						else
							return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLogger", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
					} finally {
					}
				}
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='setLogger' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
				[Register ("setLogger", "(Ljava/lang/String;)V", "GetSetLogger_Ljava_lang_String_Handler")]
				set {
					if (id_setLogger_Ljava_lang_String_ == IntPtr.Zero)
						id_setLogger_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setLogger", "(Ljava/lang/String;)V");
					IntPtr native_value = JNIEnv.NewString (value);
					try {
						JValue* __args = stackalloc JValue [1];
						__args [0] = new JValue (native_value);

						if (GetType () == ThresholdType)
							JNIEnv.CallVoidMethod  (Handle, id_setLogger_Ljava_lang_String_, __args);
						else
							JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLogger", "(Ljava/lang/String;)V"), __args);
					} finally {
						JNIEnv.DeleteLocalRef (native_value);
					}
				}
			}

			static Delegate cb_getMsg;
#pragma warning disable 0169
			static Delegate GetGetMsgHandler ()
			{
				if (cb_getMsg == null)
					cb_getMsg = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetMsg);
				return cb_getMsg;
			}

			static IntPtr n_GetMsg (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.Msg);
			}
#pragma warning restore 0169

			static Delegate cb_setMsg_Ljava_lang_String_;
#pragma warning disable 0169
			static Delegate GetSetMsg_Ljava_lang_String_Handler ()
			{
				if (cb_setMsg_Ljava_lang_String_ == null)
					cb_setMsg_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetMsg_Ljava_lang_String_);
				return cb_setMsg_Ljava_lang_String_;
			}

			static void n_SetMsg_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.Msg = p0;
			}
#pragma warning restore 0169

			static IntPtr id_getMsg;
			static IntPtr id_setMsg_Ljava_lang_String_;
			public virtual unsafe string Msg {
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='getMsg' and count(parameter)=0]"
				[Register ("getMsg", "()Ljava/lang/String;", "GetGetMsgHandler")]
				get {
					if (id_getMsg == IntPtr.Zero)
						id_getMsg = JNIEnv.GetMethodID (class_ref, "getMsg", "()Ljava/lang/String;");
					try {

						if (GetType () == ThresholdType)
							return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getMsg), JniHandleOwnership.TransferLocalRef);
						else
							return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMsg", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
					} finally {
					}
				}
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='setMsg' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
				[Register ("setMsg", "(Ljava/lang/String;)V", "GetSetMsg_Ljava_lang_String_Handler")]
				set {
					if (id_setMsg_Ljava_lang_String_ == IntPtr.Zero)
						id_setMsg_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setMsg", "(Ljava/lang/String;)V");
					IntPtr native_value = JNIEnv.NewString (value);
					try {
						JValue* __args = stackalloc JValue [1];
						__args [0] = new JValue (native_value);

						if (GetType () == ThresholdType)
							JNIEnv.CallVoidMethod  (Handle, id_setMsg_Ljava_lang_String_, __args);
						else
							JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setMsg", "(Ljava/lang/String;)V"), __args);
					} finally {
						JNIEnv.DeleteLocalRef (native_value);
					}
				}
			}

			static Delegate cb_getThread;
#pragma warning disable 0169
			static Delegate GetGetThreadHandler ()
			{
				if (cb_getThread == null)
					cb_getThread = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetThread);
				return cb_getThread;
			}

			static IntPtr n_GetThread (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewString (__this.Thread);
			}
#pragma warning restore 0169

			static Delegate cb_setThread_Ljava_lang_String_;
#pragma warning disable 0169
			static Delegate GetSetThread_Ljava_lang_String_Handler ()
			{
				if (cb_setThread_Ljava_lang_String_ == null)
					cb_setThread_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetThread_Ljava_lang_String_);
				return cb_setThread_Ljava_lang_String_;
			}

			static void n_SetThread_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
				__this.Thread = p0;
			}
#pragma warning restore 0169

			static IntPtr id_getThread;
			static IntPtr id_setThread_Ljava_lang_String_;
			public virtual unsafe string Thread {
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='getThread' and count(parameter)=0]"
				[Register ("getThread", "()Ljava/lang/String;", "GetGetThreadHandler")]
				get {
					if (id_getThread == IntPtr.Zero)
						id_getThread = JNIEnv.GetMethodID (class_ref, "getThread", "()Ljava/lang/String;");
					try {

						if (GetType () == ThresholdType)
							return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getThread), JniHandleOwnership.TransferLocalRef);
						else
							return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getThread", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
					} finally {
					}
				}
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='setThread' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
				[Register ("setThread", "(Ljava/lang/String;)V", "GetSetThread_Ljava_lang_String_Handler")]
				set {
					if (id_setThread_Ljava_lang_String_ == IntPtr.Zero)
						id_setThread_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setThread", "(Ljava/lang/String;)V");
					IntPtr native_value = JNIEnv.NewString (value);
					try {
						JValue* __args = stackalloc JValue [1];
						__args [0] = new JValue (native_value);

						if (GetType () == ThresholdType)
							JNIEnv.CallVoidMethod  (Handle, id_setThread_Ljava_lang_String_, __args);
						else
							JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setThread", "(Ljava/lang/String;)V"), __args);
					} finally {
						JNIEnv.DeleteLocalRef (native_value);
					}
				}
			}

			static Delegate cb_getTime;
#pragma warning disable 0169
			static Delegate GetGetTimeHandler ()
			{
				if (cb_getTime == null)
					cb_getTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetTime);
				return cb_getTime;
			}

			static long n_GetTime (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return __this.Time;
			}
#pragma warning restore 0169

			static Delegate cb_setTime_J;
#pragma warning disable 0169
			static Delegate GetSetTime_JHandler ()
			{
				if (cb_setTime_J == null)
					cb_setTime_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetTime_J);
				return cb_setTime_J;
			}

			static void n_SetTime_J (IntPtr jnienv, IntPtr native__this, long p0)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				__this.Time = p0;
			}
#pragma warning restore 0169

			static IntPtr id_getTime;
			static IntPtr id_setTime_J;
			public virtual unsafe long Time {
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='getTime' and count(parameter)=0]"
				[Register ("getTime", "()J", "GetGetTimeHandler")]
				get {
					if (id_getTime == IntPtr.Zero)
						id_getTime = JNIEnv.GetMethodID (class_ref, "getTime", "()J");
					try {

						if (GetType () == ThresholdType)
							return JNIEnv.CallLongMethod  (Handle, id_getTime);
						else
							return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTime", "()J"));
					} finally {
					}
				}
				// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='setTime' and count(parameter)=1 and parameter[1][@type='long']]"
				[Register ("setTime", "(J)V", "GetSetTime_JHandler")]
				set {
					if (id_setTime_J == IntPtr.Zero)
						id_setTime_J = JNIEnv.GetMethodID (class_ref, "setTime", "(J)V");
					try {
						JValue* __args = stackalloc JValue [1];
						__args [0] = new JValue (value);

						if (GetType () == ThresholdType)
							JNIEnv.CallVoidMethod  (Handle, id_setTime_J, __args);
						else
							JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTime", "(J)V"), __args);
					} finally {
					}
				}
			}

			static Delegate cb_getStack;
#pragma warning disable 0169
			static Delegate GetGetStackHandler ()
			{
				if (cb_getStack == null)
					cb_getStack = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetStack);
				return cb_getStack;
			}

			static IntPtr n_GetStack (IntPtr jnienv, IntPtr native__this)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				return JNIEnv.NewArray (__this.GetStack ());
			}
#pragma warning restore 0169

			static IntPtr id_getStack;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='getStack' and count(parameter)=0]"
			[Register ("getStack", "()[Ljava/lang/String;", "GetGetStackHandler")]
			public virtual unsafe string[] GetStack ()
			{
				if (id_getStack == IntPtr.Zero)
					id_getStack = JNIEnv.GetMethodID (class_ref, "getStack", "()[Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return (string[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod  (Handle, id_getStack), JniHandleOwnership.TransferLocalRef, typeof (string));
					else
						return (string[]) JNIEnv.GetArray (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getStack", "()[Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef, typeof (string));
				} finally {
				}
			}

			static Delegate cb_setStack_arrayLjava_lang_String_;
#pragma warning disable 0169
			static Delegate GetSetStack_arrayLjava_lang_String_Handler ()
			{
				if (cb_setStack_arrayLjava_lang_String_ == null)
					cb_setStack_arrayLjava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetStack_arrayLjava_lang_String_);
				return cb_setStack_arrayLjava_lang_String_;
			}

			static void n_SetStack_arrayLjava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				string[] p0 = (string[]) JNIEnv.GetArray (native_p0, JniHandleOwnership.DoNotTransfer, typeof (string));
				__this.SetStack (p0);
				if (p0 != null)
					JNIEnv.CopyArray (p0, native_p0);
			}
#pragma warning restore 0169

			static IntPtr id_setStack_arrayLjava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture.LogEntry']/method[@name='setStack' and count(parameter)=1 and parameter[1][@type='java.lang.String[]']]"
			[Register ("setStack", "([Ljava/lang/String;)V", "GetSetStack_arrayLjava_lang_String_Handler")]
			public virtual unsafe void SetStack (string[] p0)
			{
				if (id_setStack_arrayLjava_lang_String_ == IntPtr.Zero)
					id_setStack_arrayLjava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setStack", "([Ljava/lang/String;)V");
				IntPtr native_p0 = JNIEnv.NewArray (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setStack_arrayLjava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setStack", "([Ljava/lang/String;)V"), __args);
				} finally {
					if (p0 != null) {
						JNIEnv.CopyArray (native_p0, p0);
						JNIEnv.DeleteLocalRef (native_p0);
					}
				}
			}

		}

		// Metadata.xml XPath interface reference: path="/api/package[@name='com.gimbal.logging.util']/interface[@name='LogCapture.LogListener']"
		[Register ("com/gimbal/logging/util/LogCapture$LogListener", "", "Com.Gimbal.Logging.Util.LogCapture/ILogListenerInvoker")]
		public partial interface ILogListener : IJavaObject {

			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/interface[@name='LogCapture.LogListener']/method[@name='log' and count(parameter)=2 and parameter[1][@type='com.gimbal.logging.util.LogCapture.LogEntry'] and parameter[2][@type='java.util.List&lt;com.gimbal.logging.util.LogCapture.LogEntry&gt;']]"
			[Register ("log", "(Lcom/gimbal/logging/util/LogCapture$LogEntry;Ljava/util/List;)V", "GetLog_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_Handler:Com.Gimbal.Logging.Util.LogCapture/ILogListenerInvoker, GimbalSDK")]
			void Log (global::Com.Gimbal.Logging.Util.LogCapture.LogEntry p0, global::System.Collections.Generic.IList<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> p1);

		}

		[global::Android.Runtime.Register ("com/gimbal/logging/util/LogCapture$LogListener", DoNotGenerateAcw=true)]
		internal class ILogListenerInvoker : global::Java.Lang.Object, ILogListener {

			static IntPtr java_class_ref = JNIEnv.FindClass ("com/gimbal/logging/util/LogCapture$LogListener");
			IntPtr class_ref;

			public static ILogListener GetObject (IntPtr handle, JniHandleOwnership transfer)
			{
				return global::Java.Lang.Object.GetObject<ILogListener> (handle, transfer);
			}

			static IntPtr Validate (IntPtr handle)
			{
				if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
					throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
								JNIEnv.GetClassNameFromInstance (handle), "com.gimbal.logging.util.LogCapture.LogListener"));
				return handle;
			}

			protected override void Dispose (bool disposing)
			{
				if (this.class_ref != IntPtr.Zero)
					JNIEnv.DeleteGlobalRef (this.class_ref);
				this.class_ref = IntPtr.Zero;
				base.Dispose (disposing);
			}

			public ILogListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
			{
				IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
				this.class_ref = JNIEnv.NewGlobalRef (local_ref);
				JNIEnv.DeleteLocalRef (local_ref);
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (ILogListenerInvoker); }
			}

			static Delegate cb_log_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_;
#pragma warning disable 0169
			static Delegate GetLog_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_Handler ()
			{
				if (cb_log_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_ == null)
					cb_log_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_Log_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_);
				return cb_log_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_;
			}

			static void n_Log_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
			{
				global::Com.Gimbal.Logging.Util.LogCapture.ILogListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.ILogListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
				global::Com.Gimbal.Logging.Util.LogCapture.LogEntry p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> (native_p0, JniHandleOwnership.DoNotTransfer);
				var p1 = global::Android.Runtime.JavaList<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry>.FromJniHandle (native_p1, JniHandleOwnership.DoNotTransfer);
				__this.Log (p0, p1);
			}
#pragma warning restore 0169

			IntPtr id_log_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_;
			public unsafe void Log (global::Com.Gimbal.Logging.Util.LogCapture.LogEntry p0, global::System.Collections.Generic.IList<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> p1)
			{
				if (id_log_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_ == IntPtr.Zero)
					id_log_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "log", "(Lcom/gimbal/logging/util/LogCapture$LogEntry;Ljava/util/List;)V");
				IntPtr native_p1 = global::Android.Runtime.JavaList<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry>.ToLocalJniHandle (p1);
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				JNIEnv.CallVoidMethod (Handle, id_log_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_, __args);
				JNIEnv.DeleteLocalRef (native_p1);
			}

		}

		public partial class LogEventArgs : global::System.EventArgs {

			public LogEventArgs (global::Com.Gimbal.Logging.Util.LogCapture.LogEntry p0, global::System.Collections.Generic.IList<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> p1)
			{
				this.p0 = p0;
				this.p1 = p1;
			}

			global::Com.Gimbal.Logging.Util.LogCapture.LogEntry p0;
			public global::Com.Gimbal.Logging.Util.LogCapture.LogEntry P0 {
				get { return p0; }
			}

			global::System.Collections.Generic.IList<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> p1;
			public global::System.Collections.Generic.IList<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> P1 {
				get { return p1; }
			}
		}

		[global::Android.Runtime.Register ("mono/com/gimbal/logging/util/LogCapture_LogListenerImplementor")]
		internal sealed partial class ILogListenerImplementor : global::Java.Lang.Object, ILogListener {

			object sender;

			public ILogListenerImplementor (object sender)
				: base (
					global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/com/gimbal/logging/util/LogCapture_LogListenerImplementor", "()V"),
					JniHandleOwnership.TransferLocalRef)
			{
				global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
				this.sender = sender;
			}

#pragma warning disable 0649
			public EventHandler<LogEventArgs> Handler;
#pragma warning restore 0649

			public void Log (global::Com.Gimbal.Logging.Util.LogCapture.LogEntry p0, global::System.Collections.Generic.IList<global::Com.Gimbal.Logging.Util.LogCapture.LogEntry> p1)
			{
				var __h = Handler;
				if (__h != null)
					__h (sender, new LogEventArgs (p0, p1));
			}

			internal static bool __IsEmpty (ILogListenerImplementor value)
			{
				return value.Handler == null;
			}
		}


		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/logging/util/LogCapture", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (LogCapture); }
		}

		protected LogCapture (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/constructor[@name='LogCapture' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe LogCapture ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (LogCapture)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static IntPtr id_getLogDir;
		protected static unsafe global::Java.IO.File LogDir {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='getLogDir' and count(parameter)=0]"
			[Register ("getLogDir", "()Ljava/io/File;", "GetGetLogDirHandler")]
			get {
				if (id_getLogDir == IntPtr.Zero)
					id_getLogDir = JNIEnv.GetStaticMethodID (class_ref, "getLogDir", "()Ljava/io/File;");
				try {
					return global::Java.Lang.Object.GetObject<global::Java.IO.File> (JNIEnv.CallStaticObjectMethod  (class_ref, id_getLogDir), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_addListener_Lcom_gimbal_logging_util_LogCapture_LogListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='addListener' and count(parameter)=1 and parameter[1][@type='com.gimbal.logging.util.LogCapture.LogListener']]"
		[Register ("addListener", "(Lcom/gimbal/logging/util/LogCapture$LogListener;)V", "")]
		public static unsafe void AddListener (global::Com.Gimbal.Logging.Util.LogCapture.ILogListener p0)
		{
			if (id_addListener_Lcom_gimbal_logging_util_LogCapture_LogListener_ == IntPtr.Zero)
				id_addListener_Lcom_gimbal_logging_util_LogCapture_LogListener_ = JNIEnv.GetStaticMethodID (class_ref, "addListener", "(Lcom/gimbal/logging/util/LogCapture$LogListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_addListener_Lcom_gimbal_logging_util_LogCapture_LogListener_, __args);
			} finally {
			}
		}

		static IntPtr id_baseLogFileName;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='baseLogFileName' and count(parameter)=0]"
		[Register ("baseLogFileName", "()Ljava/lang/String;", "")]
		protected static unsafe string BaseLogFileName ()
		{
			if (id_baseLogFileName == IntPtr.Zero)
				id_baseLogFileName = JNIEnv.GetStaticMethodID (class_ref, "baseLogFileName", "()Ljava/lang/String;");
			try {
				return JNIEnv.GetString (JNIEnv.CallStaticObjectMethod  (class_ref, id_baseLogFileName), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_capture_Ljava_lang_String_Ljava_lang_String_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='capture' and count(parameter)=3 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.String'] and parameter[3][@type='java.lang.String']]"
		[Register ("capture", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "")]
		public static unsafe void Capture (string p0, string p1, string p2)
		{
			if (id_capture_Ljava_lang_String_Ljava_lang_String_Ljava_lang_String_ == IntPtr.Zero)
				id_capture_Ljava_lang_String_Ljava_lang_String_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "capture", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			IntPtr native_p1 = JNIEnv.NewString (p1);
			IntPtr native_p2 = JNIEnv.NewString (p2);
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (native_p1);
				__args [2] = new JValue (native_p2);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_capture_Ljava_lang_String_Ljava_lang_String_Ljava_lang_String_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
				JNIEnv.DeleteLocalRef (native_p1);
				JNIEnv.DeleteLocalRef (native_p2);
			}
		}

		static IntPtr id_capture_Ljava_lang_String_Ljava_lang_String_Ljava_lang_String_Ljava_lang_Throwable_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='capture' and count(parameter)=4 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.String'] and parameter[3][@type='java.lang.String'] and parameter[4][@type='java.lang.Throwable']]"
		[Register ("capture", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V", "")]
		public static unsafe void Capture (string p0, string p1, string p2, global::Java.Lang.Throwable p3)
		{
			if (id_capture_Ljava_lang_String_Ljava_lang_String_Ljava_lang_String_Ljava_lang_Throwable_ == IntPtr.Zero)
				id_capture_Ljava_lang_String_Ljava_lang_String_Ljava_lang_String_Ljava_lang_Throwable_ = JNIEnv.GetStaticMethodID (class_ref, "capture", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			IntPtr native_p1 = JNIEnv.NewString (p1);
			IntPtr native_p2 = JNIEnv.NewString (p2);
			try {
				JValue* __args = stackalloc JValue [4];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (native_p1);
				__args [2] = new JValue (native_p2);
				__args [3] = new JValue (p3);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_capture_Ljava_lang_String_Ljava_lang_String_Ljava_lang_String_Ljava_lang_Throwable_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
				JNIEnv.DeleteLocalRef (native_p1);
				JNIEnv.DeleteLocalRef (native_p2);
			}
		}

		static IntPtr id_cleanupLogDir_Ljava_io_File_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='cleanupLogDir' and count(parameter)=1 and parameter[1][@type='java.io.File']]"
		[Register ("cleanupLogDir", "(Ljava/io/File;)V", "")]
		protected static unsafe void CleanupLogDir (global::Java.IO.File p0)
		{
			if (id_cleanupLogDir_Ljava_io_File_ == IntPtr.Zero)
				id_cleanupLogDir_Ljava_io_File_ = JNIEnv.GetStaticMethodID (class_ref, "cleanupLogDir", "(Ljava/io/File;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_cleanupLogDir_Ljava_io_File_, __args);
			} finally {
			}
		}

		static IntPtr id_flush;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='flush' and count(parameter)=0]"
		[Register ("flush", "()V", "")]
		protected static unsafe void Flush ()
		{
			if (id_flush == IntPtr.Zero)
				id_flush = JNIEnv.GetStaticMethodID (class_ref, "flush", "()V");
			try {
				JNIEnv.CallStaticVoidMethod  (class_ref, id_flush);
			} finally {
			}
		}

		static IntPtr id_getLogFile_Ljava_io_File_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='getLogFile' and count(parameter)=1 and parameter[1][@type='java.io.File']]"
		[Register ("getLogFile", "(Ljava/io/File;)Ljava/io/File;", "")]
		protected static unsafe global::Java.IO.File GetLogFile (global::Java.IO.File p0)
		{
			if (id_getLogFile_Ljava_io_File_ == IntPtr.Zero)
				id_getLogFile_Ljava_io_File_ = JNIEnv.GetStaticMethodID (class_ref, "getLogFile", "(Ljava/io/File;)Ljava/io/File;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				global::Java.IO.File __ret = global::Java.Lang.Object.GetObject<global::Java.IO.File> (JNIEnv.CallStaticObjectMethod  (class_ref, id_getLogFile_Ljava_io_File_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_limit_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='limit' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("limit", "(Ljava/lang/String;)Ljava/lang/String;", "")]
		protected static unsafe string Limit (string p0)
		{
			if (id_limit_Ljava_lang_String_ == IntPtr.Zero)
				id_limit_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "limit", "(Ljava/lang/String;)Ljava/lang/String;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				string __ret = JNIEnv.GetString (JNIEnv.CallStaticObjectMethod  (class_ref, id_limit_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_logDirName;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='logDirName' and count(parameter)=0]"
		[Register ("logDirName", "()Ljava/lang/String;", "")]
		protected static unsafe string LogDirName ()
		{
			if (id_logDirName == IntPtr.Zero)
				id_logDirName = JNIEnv.GetStaticMethodID (class_ref, "logDirName", "()Ljava/lang/String;");
			try {
				return JNIEnv.GetString (JNIEnv.CallStaticObjectMethod  (class_ref, id_logDirName), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_notifyListeners_Lcom_gimbal_logging_util_LogCapture_LogEntry_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='notifyListeners' and count(parameter)=1 and parameter[1][@type='com.gimbal.logging.util.LogCapture.LogEntry']]"
		[Register ("notifyListeners", "(Lcom/gimbal/logging/util/LogCapture$LogEntry;)V", "")]
		public static unsafe void NotifyListeners (global::Com.Gimbal.Logging.Util.LogCapture.LogEntry p0)
		{
			if (id_notifyListeners_Lcom_gimbal_logging_util_LogCapture_LogEntry_ == IntPtr.Zero)
				id_notifyListeners_Lcom_gimbal_logging_util_LogCapture_LogEntry_ = JNIEnv.GetStaticMethodID (class_ref, "notifyListeners", "(Lcom/gimbal/logging/util/LogCapture$LogEntry;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_notifyListeners_Lcom_gimbal_logging_util_LogCapture_LogEntry_, __args);
			} finally {
			}
		}

		static IntPtr id_removeListener_Lcom_gimbal_logging_util_LogCapture_LogListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='removeListener' and count(parameter)=1 and parameter[1][@type='com.gimbal.logging.util.LogCapture.LogListener']]"
		[Register ("removeListener", "(Lcom/gimbal/logging/util/LogCapture$LogListener;)V", "")]
		public static unsafe void RemoveListener (global::Com.Gimbal.Logging.Util.LogCapture.ILogListener p0)
		{
			if (id_removeListener_Lcom_gimbal_logging_util_LogCapture_LogListener_ == IntPtr.Zero)
				id_removeListener_Lcom_gimbal_logging_util_LogCapture_LogListener_ = JNIEnv.GetStaticMethodID (class_ref, "removeListener", "(Lcom/gimbal/logging/util/LogCapture$LogListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_removeListener_Lcom_gimbal_logging_util_LogCapture_LogListener_, __args);
			} finally {
			}
		}

		static IntPtr id_rollFile;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='rollFile' and count(parameter)=0]"
		[Register ("rollFile", "()V", "")]
		public static unsafe void RollFile ()
		{
			if (id_rollFile == IntPtr.Zero)
				id_rollFile = JNIEnv.GetStaticMethodID (class_ref, "rollFile", "()V");
			try {
				JNIEnv.CallStaticVoidMethod  (class_ref, id_rollFile);
			} finally {
			}
		}

		static IntPtr id_rotateLogFile;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='rotateLogFile' and count(parameter)=0]"
		[Register ("rotateLogFile", "()V", "")]
		protected static unsafe void RotateLogFile ()
		{
			if (id_rotateLogFile == IntPtr.Zero)
				id_rotateLogFile = JNIEnv.GetStaticMethodID (class_ref, "rotateLogFile", "()V");
			try {
				JNIEnv.CallStaticVoidMethod  (class_ref, id_rotateLogFile);
			} finally {
			}
		}

		static IntPtr id_saveForUI_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='saveForUI' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("saveForUI", "(Z)V", "")]
		public static unsafe void SaveForUI (bool p0)
		{
			if (id_saveForUI_Z == IntPtr.Zero)
				id_saveForUI_Z = JNIEnv.GetStaticMethodID (class_ref, "saveForUI", "(Z)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_saveForUI_Z, __args);
			} finally {
			}
		}

		static IntPtr id_saveLine_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='saveLine' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("saveLine", "(Ljava/lang/String;)V", "")]
		protected static unsafe void SaveLine (string p0)
		{
			if (id_saveLine_Ljava_lang_String_ == IntPtr.Zero)
				id_saveLine_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "saveLine", "(Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_saveLine_Ljava_lang_String_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_saveToFile_Z;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='saveToFile' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register ("saveToFile", "(Z)V", "")]
		public static unsafe void SaveToFile (bool p0)
		{
			if (id_saveToFile_Z == IntPtr.Zero)
				id_saveToFile_Z = JNIEnv.GetStaticMethodID (class_ref, "saveToFile", "(Z)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_saveToFile_Z, __args);
			} finally {
			}
		}

		static IntPtr id_setAppContext_Landroid_content_Context_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='setAppContext' and count(parameter)=1 and parameter[1][@type='android.content.Context']]"
		[Register ("setAppContext", "(Landroid/content/Context;)V", "")]
		public static unsafe void SetAppContext (global::Android.Content.Context p0)
		{
			if (id_setAppContext_Landroid_content_Context_ == IntPtr.Zero)
				id_setAppContext_Landroid_content_Context_ = JNIEnv.GetStaticMethodID (class_ref, "setAppContext", "(Landroid/content/Context;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_setAppContext_Landroid_content_Context_, __args);
			} finally {
			}
		}

		static IntPtr id_shortName_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='shortName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("shortName", "(Ljava/lang/String;)Ljava/lang/String;", "")]
		protected static unsafe string ShortName (string p0)
		{
			if (id_shortName_Ljava_lang_String_ == IntPtr.Zero)
				id_shortName_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "shortName", "(Ljava/lang/String;)Ljava/lang/String;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				string __ret = JNIEnv.GetString (JNIEnv.CallStaticObjectMethod  (class_ref, id_shortName_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_stack_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_lang_Throwable_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.logging.util']/class[@name='LogCapture']/method[@name='stack' and count(parameter)=2 and parameter[1][@type='com.gimbal.logging.util.LogCapture.LogEntry'] and parameter[2][@type='java.lang.Throwable']]"
		[Register ("stack", "(Lcom/gimbal/logging/util/LogCapture$LogEntry;Ljava/lang/Throwable;)V", "")]
		protected static unsafe void Stack (global::Com.Gimbal.Logging.Util.LogCapture.LogEntry p0, global::Java.Lang.Throwable p1)
		{
			if (id_stack_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_lang_Throwable_ == IntPtr.Zero)
				id_stack_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_lang_Throwable_ = JNIEnv.GetStaticMethodID (class_ref, "stack", "(Lcom/gimbal/logging/util/LogCapture$LogEntry;Ljava/lang/Throwable;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_stack_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_lang_Throwable_, __args);
			} finally {
			}
		}

#region "Event implementation for Com.Gimbal.Logging.Util.LogCapture.ILogListener"

		global::Com.Gimbal.Logging.Util.LogCapture.ILogListenerImplementor __CreateILogListenerImplementor ()
		{
			return new global::Com.Gimbal.Logging.Util.LogCapture.ILogListenerImplementor (this);
		}
#endregion
	}
}
