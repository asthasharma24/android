using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Analytics {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/EventsGram", DoNotGenerateAcw=true)]
	public partial class EventsGram : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/EventsGram", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (EventsGram); }
		}

		protected EventsGram (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/constructor[@name='EventsGram' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe EventsGram ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (EventsGram)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getApplicationId;
#pragma warning disable 0169
		static Delegate GetGetApplicationIdHandler ()
		{
			if (cb_getApplicationId == null)
				cb_getApplicationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApplicationId);
			return cb_getApplicationId;
		}

		static IntPtr n_GetApplicationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Analytics.EventsGram __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventsGram> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ApplicationId);
		}
#pragma warning restore 0169

		static Delegate cb_setApplicationId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetApplicationId_Ljava_lang_Long_Handler ()
		{
			if (cb_setApplicationId_Ljava_lang_Long_ == null)
				cb_setApplicationId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApplicationId_Ljava_lang_Long_);
			return cb_setApplicationId_Ljava_lang_Long_;
		}

		static void n_SetApplicationId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Analytics.EventsGram __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventsGram> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApplicationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApplicationId;
		static IntPtr id_setApplicationId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long ApplicationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/method[@name='getApplicationId' and count(parameter)=0]"
			[Register ("getApplicationId", "()Ljava/lang/Long;", "GetGetApplicationIdHandler")]
			get {
				if (id_getApplicationId == IntPtr.Zero)
					id_getApplicationId = JNIEnv.GetMethodID (class_ref, "getApplicationId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getApplicationId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApplicationId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/method[@name='setApplicationId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setApplicationId", "(Ljava/lang/Long;)V", "GetSetApplicationId_Ljava_lang_Long_Handler")]
			set {
				if (id_setApplicationId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setApplicationId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setApplicationId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApplicationId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApplicationId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getEvents;
#pragma warning disable 0169
		static Delegate GetGetEventsHandler ()
		{
			if (cb_getEvents == null)
				cb_getEvents = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEvents);
			return cb_getEvents;
		}

		static IntPtr n_GetEvents (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Analytics.EventsGram __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventsGram> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Analytics.ClientEvent>.ToLocalJniHandle (__this.Events);
		}
#pragma warning restore 0169

		static Delegate cb_setEvents_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetEvents_Ljava_util_List_Handler ()
		{
			if (cb_setEvents_Ljava_util_List_ == null)
				cb_setEvents_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetEvents_Ljava_util_List_);
			return cb_setEvents_Ljava_util_List_;
		}

		static void n_SetEvents_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Analytics.EventsGram __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventsGram> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Analytics.ClientEvent>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Events = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEvents;
		static IntPtr id_setEvents_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.Analytics.ClientEvent> Events {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/method[@name='getEvents' and count(parameter)=0]"
			[Register ("getEvents", "()Ljava/util/List;", "GetGetEventsHandler")]
			get {
				if (id_getEvents == IntPtr.Zero)
					id_getEvents = JNIEnv.GetMethodID (class_ref, "getEvents", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Analytics.ClientEvent>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getEvents), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Analytics.ClientEvent>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEvents", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/method[@name='setEvents' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.qsl.faar.protocol.analytics.ClientEvent&gt;']]"
			[Register ("setEvents", "(Ljava/util/List;)V", "GetSetEvents_Ljava_util_List_Handler")]
			set {
				if (id_setEvents_Ljava_util_List_ == IntPtr.Zero)
					id_setEvents_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setEvents", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Analytics.ClientEvent>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEvents_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEvents", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getOrganizationId;
#pragma warning disable 0169
		static Delegate GetGetOrganizationIdHandler ()
		{
			if (cb_getOrganizationId == null)
				cb_getOrganizationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationId);
			return cb_getOrganizationId;
		}

		static IntPtr n_GetOrganizationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Analytics.EventsGram __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventsGram> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OrganizationId);
		}
#pragma warning restore 0169

		static Delegate cb_setOrganizationId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationId_Ljava_lang_Long_Handler ()
		{
			if (cb_setOrganizationId_Ljava_lang_Long_ == null)
				cb_setOrganizationId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationId_Ljava_lang_Long_);
			return cb_setOrganizationId_Ljava_lang_Long_;
		}

		static void n_SetOrganizationId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Analytics.EventsGram __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventsGram> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OrganizationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationId;
		static IntPtr id_setOrganizationId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long OrganizationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/method[@name='getOrganizationId' and count(parameter)=0]"
			[Register ("getOrganizationId", "()Ljava/lang/Long;", "GetGetOrganizationIdHandler")]
			get {
				if (id_getOrganizationId == IntPtr.Zero)
					id_getOrganizationId = JNIEnv.GetMethodID (class_ref, "getOrganizationId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/method[@name='setOrganizationId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setOrganizationId", "(Ljava/lang/Long;)V", "GetSetOrganizationId_Ljava_lang_Long_Handler")]
			set {
				if (id_setOrganizationId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setOrganizationId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setOrganizationId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOrganizationId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getReceiverUUID;
#pragma warning disable 0169
		static Delegate GetGetReceiverUUIDHandler ()
		{
			if (cb_getReceiverUUID == null)
				cb_getReceiverUUID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetReceiverUUID);
			return cb_getReceiverUUID;
		}

		static IntPtr n_GetReceiverUUID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Analytics.EventsGram __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventsGram> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ReceiverUUID);
		}
#pragma warning restore 0169

		static Delegate cb_setReceiverUUID_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetReceiverUUID_Ljava_lang_String_Handler ()
		{
			if (cb_setReceiverUUID_Ljava_lang_String_ == null)
				cb_setReceiverUUID_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetReceiverUUID_Ljava_lang_String_);
			return cb_setReceiverUUID_Ljava_lang_String_;
		}

		static void n_SetReceiverUUID_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Analytics.EventsGram __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventsGram> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReceiverUUID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getReceiverUUID;
		static IntPtr id_setReceiverUUID_Ljava_lang_String_;
		public virtual unsafe string ReceiverUUID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/method[@name='getReceiverUUID' and count(parameter)=0]"
			[Register ("getReceiverUUID", "()Ljava/lang/String;", "GetGetReceiverUUIDHandler")]
			get {
				if (id_getReceiverUUID == IntPtr.Zero)
					id_getReceiverUUID = JNIEnv.GetMethodID (class_ref, "getReceiverUUID", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getReceiverUUID), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getReceiverUUID", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/method[@name='setReceiverUUID' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setReceiverUUID", "(Ljava/lang/String;)V", "GetSetReceiverUUID_Ljava_lang_String_Handler")]
			set {
				if (id_setReceiverUUID_Ljava_lang_String_ == IntPtr.Zero)
					id_setReceiverUUID_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setReceiverUUID", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setReceiverUUID_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setReceiverUUID", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUserId;
#pragma warning disable 0169
		static Delegate GetGetUserIdHandler ()
		{
			if (cb_getUserId == null)
				cb_getUserId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUserId);
			return cb_getUserId;
		}

		static IntPtr n_GetUserId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Analytics.EventsGram __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventsGram> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.UserId);
		}
#pragma warning restore 0169

		static Delegate cb_setUserId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetUserId_Ljava_lang_Long_Handler ()
		{
			if (cb_setUserId_Ljava_lang_Long_ == null)
				cb_setUserId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUserId_Ljava_lang_Long_);
			return cb_setUserId_Ljava_lang_Long_;
		}

		static void n_SetUserId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Analytics.EventsGram __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventsGram> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.UserId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUserId;
		static IntPtr id_setUserId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long UserId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/method[@name='getUserId' and count(parameter)=0]"
			[Register ("getUserId", "()Ljava/lang/Long;", "GetGetUserIdHandler")]
			get {
				if (id_getUserId == IntPtr.Zero)
					id_getUserId = JNIEnv.GetMethodID (class_ref, "getUserId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getUserId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUserId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/method[@name='setUserId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setUserId", "(Ljava/lang/Long;)V", "GetSetUserId_Ljava_lang_Long_Handler")]
			set {
				if (id_setUserId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setUserId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setUserId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUserId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUserId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getUsername;
#pragma warning disable 0169
		static Delegate GetGetUsernameHandler ()
		{
			if (cb_getUsername == null)
				cb_getUsername = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUsername);
			return cb_getUsername;
		}

		static IntPtr n_GetUsername (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Analytics.EventsGram __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventsGram> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Username);
		}
#pragma warning restore 0169

		static Delegate cb_setUsername_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUsername_Ljava_lang_String_Handler ()
		{
			if (cb_setUsername_Ljava_lang_String_ == null)
				cb_setUsername_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUsername_Ljava_lang_String_);
			return cb_setUsername_Ljava_lang_String_;
		}

		static void n_SetUsername_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Analytics.EventsGram __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventsGram> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Username = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUsername;
		static IntPtr id_setUsername_Ljava_lang_String_;
		public virtual unsafe string Username {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/method[@name='getUsername' and count(parameter)=0]"
			[Register ("getUsername", "()Ljava/lang/String;", "GetGetUsernameHandler")]
			get {
				if (id_getUsername == IntPtr.Zero)
					id_getUsername = JNIEnv.GetMethodID (class_ref, "getUsername", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUsername), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUsername", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventsGram']/method[@name='setUsername' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUsername", "(Ljava/lang/String;)V", "GetSetUsername_Ljava_lang_String_Handler")]
			set {
				if (id_setUsername_Ljava_lang_String_ == IntPtr.Zero)
					id_setUsername_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUsername", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUsername_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUsername", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
