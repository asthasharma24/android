using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='Visit']"
	[global::Android.Runtime.Register ("com/gimbal/android/Visit", DoNotGenerateAcw=true)]
	public partial class Visit : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/Visit", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Visit); }
		}

		protected Visit (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android']/class[@name='Visit']/constructor[@name='Visit' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Visit ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Visit)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getArrivalTimeInMillis;
#pragma warning disable 0169
		static Delegate GetGetArrivalTimeInMillisHandler ()
		{
			if (cb_getArrivalTimeInMillis == null)
				cb_getArrivalTimeInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetArrivalTimeInMillis);
			return cb_getArrivalTimeInMillis;
		}

		static long n_GetArrivalTimeInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Visit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Visit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ArrivalTimeInMillis;
		}
#pragma warning restore 0169

		static IntPtr id_getArrivalTimeInMillis;
		public virtual unsafe long ArrivalTimeInMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Visit']/method[@name='getArrivalTimeInMillis' and count(parameter)=0]"
			[Register ("getArrivalTimeInMillis", "()J", "GetGetArrivalTimeInMillisHandler")]
			get {
				if (id_getArrivalTimeInMillis == IntPtr.Zero)
					id_getArrivalTimeInMillis = JNIEnv.GetMethodID (class_ref, "getArrivalTimeInMillis", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getArrivalTimeInMillis);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getArrivalTimeInMillis", "()J"));
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureTimeInMillis;
#pragma warning disable 0169
		static Delegate GetGetDepartureTimeInMillisHandler ()
		{
			if (cb_getDepartureTimeInMillis == null)
				cb_getDepartureTimeInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetDepartureTimeInMillis);
			return cb_getDepartureTimeInMillis;
		}

		static long n_GetDepartureTimeInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Visit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Visit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DepartureTimeInMillis;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureTimeInMillis;
		public virtual unsafe long DepartureTimeInMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Visit']/method[@name='getDepartureTimeInMillis' and count(parameter)=0]"
			[Register ("getDepartureTimeInMillis", "()J", "GetGetDepartureTimeInMillisHandler")]
			get {
				if (id_getDepartureTimeInMillis == IntPtr.Zero)
					id_getDepartureTimeInMillis = JNIEnv.GetMethodID (class_ref, "getDepartureTimeInMillis", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getDepartureTimeInMillis);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureTimeInMillis", "()J"));
				} finally {
				}
			}
		}

		static Delegate cb_getDwellTimeInMillis;
#pragma warning disable 0169
		static Delegate GetGetDwellTimeInMillisHandler ()
		{
			if (cb_getDwellTimeInMillis == null)
				cb_getDwellTimeInMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetDwellTimeInMillis);
			return cb_getDwellTimeInMillis;
		}

		static long n_GetDwellTimeInMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Visit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Visit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DwellTimeInMillis;
		}
#pragma warning restore 0169

		static IntPtr id_getDwellTimeInMillis;
		public virtual unsafe long DwellTimeInMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Visit']/method[@name='getDwellTimeInMillis' and count(parameter)=0]"
			[Register ("getDwellTimeInMillis", "()J", "GetGetDwellTimeInMillisHandler")]
			get {
				if (id_getDwellTimeInMillis == IntPtr.Zero)
					id_getDwellTimeInMillis = JNIEnv.GetMethodID (class_ref, "getDwellTimeInMillis", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getDwellTimeInMillis);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDwellTimeInMillis", "()J"));
				} finally {
				}
			}
		}

		static Delegate cb_getPlace;
#pragma warning disable 0169
		static Delegate GetGetPlaceHandler ()
		{
			if (cb_getPlace == null)
				cb_getPlace = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlace);
			return cb_getPlace;
		}

		static IntPtr n_GetPlace (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.Visit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Visit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Place);
		}
#pragma warning restore 0169

		static IntPtr id_getPlace;
		public virtual unsafe global::Com.Gimbal.Android.Place Place {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Visit']/method[@name='getPlace' and count(parameter)=0]"
			[Register ("getPlace", "()Lcom/gimbal/android/Place;", "GetGetPlaceHandler")]
			get {
				if (id_getPlace == IntPtr.Zero)
					id_getPlace = JNIEnv.GetMethodID (class_ref, "getPlace", "()Lcom/gimbal/android/Place;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Place> (JNIEnv.CallObjectMethod  (Handle, id_getPlace), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.Place> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlace", "()Lcom/gimbal/android/Place;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

	}
}
