using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proguard {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.gimbal.proguard']/interface[@name='Keep']"
	[Register ("com/gimbal/proguard/Keep", "", "Com.Gimbal.Proguard.IKeepInvoker")]
	public partial interface IKeep : IJavaObject {

	}

	[global::Android.Runtime.Register ("com/gimbal/proguard/Keep", DoNotGenerateAcw=true)]
	internal class IKeepInvoker : global::Java.Lang.Object, IKeep {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/gimbal/proguard/Keep");
		IntPtr class_ref;

		public static IKeep GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IKeep> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.gimbal.proguard.Keep"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IKeepInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IKeepInvoker); }
		}

	}

}
