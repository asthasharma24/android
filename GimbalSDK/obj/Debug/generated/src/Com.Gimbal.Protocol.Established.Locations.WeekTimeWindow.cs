using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol.Established.Locations {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='WeekTimeWindow']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/established/locations/WeekTimeWindow", DoNotGenerateAcw=true)]
	public partial class WeekTimeWindow : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/established/locations/WeekTimeWindow", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (WeekTimeWindow); }
		}

		protected WeekTimeWindow (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='WeekTimeWindow']/constructor[@name='WeekTimeWindow' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe WeekTimeWindow ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (WeekTimeWindow)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static IntPtr id_ctor_II;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='WeekTimeWindow']/constructor[@name='WeekTimeWindow' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='int']]"
		[Register (".ctor", "(II)V", "")]
		public unsafe WeekTimeWindow (int p0, int p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (GetType () != typeof (WeekTimeWindow)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(II)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(II)V", __args);
					return;
				}

				if (id_ctor_II == IntPtr.Zero)
					id_ctor_II = JNIEnv.GetMethodID (class_ref, "<init>", "(II)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_II, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_II, __args);
			} finally {
			}
		}

		static Delegate cb_getEndMinute;
#pragma warning disable 0169
		static Delegate GetGetEndMinuteHandler ()
		{
			if (cb_getEndMinute == null)
				cb_getEndMinute = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetEndMinute);
			return cb_getEndMinute;
		}

		static int n_GetEndMinute (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.EndMinute;
		}
#pragma warning restore 0169

		static Delegate cb_setEndMinute_I;
#pragma warning disable 0169
		static Delegate GetSetEndMinute_IHandler ()
		{
			if (cb_setEndMinute_I == null)
				cb_setEndMinute_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetEndMinute_I);
			return cb_setEndMinute_I;
		}

		static void n_SetEndMinute_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.EndMinute = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEndMinute;
		static IntPtr id_setEndMinute_I;
		public virtual unsafe int EndMinute {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='WeekTimeWindow']/method[@name='getEndMinute' and count(parameter)=0]"
			[Register ("getEndMinute", "()I", "GetGetEndMinuteHandler")]
			get {
				if (id_getEndMinute == IntPtr.Zero)
					id_getEndMinute = JNIEnv.GetMethodID (class_ref, "getEndMinute", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getEndMinute);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEndMinute", "()I"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='WeekTimeWindow']/method[@name='setEndMinute' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setEndMinute", "(I)V", "GetSetEndMinute_IHandler")]
			set {
				if (id_setEndMinute_I == IntPtr.Zero)
					id_setEndMinute_I = JNIEnv.GetMethodID (class_ref, "setEndMinute", "(I)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEndMinute_I, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEndMinute", "(I)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getStartMinute;
#pragma warning disable 0169
		static Delegate GetGetStartMinuteHandler ()
		{
			if (cb_getStartMinute == null)
				cb_getStartMinute = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetStartMinute);
			return cb_getStartMinute;
		}

		static int n_GetStartMinute (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.StartMinute;
		}
#pragma warning restore 0169

		static Delegate cb_setStartMinute_I;
#pragma warning disable 0169
		static Delegate GetSetStartMinute_IHandler ()
		{
			if (cb_setStartMinute_I == null)
				cb_setStartMinute_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetStartMinute_I);
			return cb_setStartMinute_I;
		}

		static void n_SetStartMinute_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.WeekTimeWindow> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StartMinute = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getStartMinute;
		static IntPtr id_setStartMinute_I;
		public virtual unsafe int StartMinute {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='WeekTimeWindow']/method[@name='getStartMinute' and count(parameter)=0]"
			[Register ("getStartMinute", "()I", "GetGetStartMinuteHandler")]
			get {
				if (id_getStartMinute == IntPtr.Zero)
					id_getStartMinute = JNIEnv.GetMethodID (class_ref, "getStartMinute", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getStartMinute);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getStartMinute", "()I"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='WeekTimeWindow']/method[@name='setStartMinute' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setStartMinute", "(I)V", "GetSetStartMinute_IHandler")]
			set {
				if (id_setStartMinute_I == IntPtr.Zero)
					id_setStartMinute_I = JNIEnv.GetMethodID (class_ref, "setStartMinute", "(I)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setStartMinute_I, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setStartMinute", "(I)V"), __args);
				} finally {
				}
			}
		}

	}
}
