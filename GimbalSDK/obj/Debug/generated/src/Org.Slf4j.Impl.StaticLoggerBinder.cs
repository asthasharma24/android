using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Org.Slf4j.Impl {

	// Metadata.xml XPath class reference: path="/api/package[@name='org.slf4j.impl']/class[@name='StaticLoggerBinder']"
	[global::Android.Runtime.Register ("org/slf4j/impl/StaticLoggerBinder", DoNotGenerateAcw=true)]
	public partial class StaticLoggerBinder : global::Java.Lang.Object {


		static IntPtr REQUESTED_API_VERSION_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='org.slf4j.impl']/class[@name='StaticLoggerBinder']/field[@name='REQUESTED_API_VERSION']"
		[Register ("REQUESTED_API_VERSION")]
		public static string RequestedApiVersion {
			get {
				if (REQUESTED_API_VERSION_jfieldId == IntPtr.Zero)
					REQUESTED_API_VERSION_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "REQUESTED_API_VERSION", "Ljava/lang/String;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, REQUESTED_API_VERSION_jfieldId);
				return JNIEnv.GetString (__ret, JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (REQUESTED_API_VERSION_jfieldId == IntPtr.Zero)
					REQUESTED_API_VERSION_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "REQUESTED_API_VERSION", "Ljava/lang/String;");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JNIEnv.SetStaticField (class_ref, REQUESTED_API_VERSION_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("org/slf4j/impl/StaticLoggerBinder", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (StaticLoggerBinder); }
		}

		protected StaticLoggerBinder (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static Delegate cb_getLoggerFactoryClassStr;
#pragma warning disable 0169
		static Delegate GetGetLoggerFactoryClassStrHandler ()
		{
			if (cb_getLoggerFactoryClassStr == null)
				cb_getLoggerFactoryClassStr = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLoggerFactoryClassStr);
			return cb_getLoggerFactoryClassStr;
		}

		static IntPtr n_GetLoggerFactoryClassStr (IntPtr jnienv, IntPtr native__this)
		{
			global::Org.Slf4j.Impl.StaticLoggerBinder __this = global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.StaticLoggerBinder> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.LoggerFactoryClassStr);
		}
#pragma warning restore 0169

		static IntPtr id_getLoggerFactoryClassStr;
		public virtual unsafe string LoggerFactoryClassStr {
			// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='StaticLoggerBinder']/method[@name='getLoggerFactoryClassStr' and count(parameter)=0]"
			[Register ("getLoggerFactoryClassStr", "()Ljava/lang/String;", "GetGetLoggerFactoryClassStrHandler")]
			get {
				if (id_getLoggerFactoryClassStr == IntPtr.Zero)
					id_getLoggerFactoryClassStr = JNIEnv.GetMethodID (class_ref, "getLoggerFactoryClassStr", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getLoggerFactoryClassStr), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLoggerFactoryClassStr", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_getSingleton;
		public static unsafe global::Org.Slf4j.Impl.StaticLoggerBinder Singleton {
			// Metadata.xml XPath method reference: path="/api/package[@name='org.slf4j.impl']/class[@name='StaticLoggerBinder']/method[@name='getSingleton' and count(parameter)=0]"
			[Register ("getSingleton", "()Lorg/slf4j/impl/StaticLoggerBinder;", "GetGetSingletonHandler")]
			get {
				if (id_getSingleton == IntPtr.Zero)
					id_getSingleton = JNIEnv.GetStaticMethodID (class_ref, "getSingleton", "()Lorg/slf4j/impl/StaticLoggerBinder;");
				try {
					return global::Java.Lang.Object.GetObject<global::Org.Slf4j.Impl.StaticLoggerBinder> (JNIEnv.CallStaticObjectMethod  (class_ref, id_getSingleton), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

	}
}
