using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol.Established.Locations {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocation']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/established/locations/EstablishedLocation", DoNotGenerateAcw=true)]
	public partial class EstablishedLocation : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/established/locations/EstablishedLocation", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (EstablishedLocation); }
		}

		protected EstablishedLocation (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocation']/constructor[@name='EstablishedLocation' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe EstablishedLocation ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (EstablishedLocation)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getBoundary;
#pragma warning disable 0169
		static Delegate GetGetBoundaryHandler ()
		{
			if (cb_getBoundary == null)
				cb_getBoundary = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBoundary);
			return cb_getBoundary;
		}

		static IntPtr n_GetBoundary (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Boundary);
		}
#pragma warning restore 0169

		static Delegate cb_setBoundary_Lcom_gimbal_protocol_established_locations_Circle_;
#pragma warning disable 0169
		static Delegate GetSetBoundary_Lcom_gimbal_protocol_established_locations_Circle_Handler ()
		{
			if (cb_setBoundary_Lcom_gimbal_protocol_established_locations_Circle_ == null)
				cb_setBoundary_Lcom_gimbal_protocol_established_locations_Circle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetBoundary_Lcom_gimbal_protocol_established_locations_Circle_);
			return cb_setBoundary_Lcom_gimbal_protocol_established_locations_Circle_;
		}

		static void n_SetBoundary_Lcom_gimbal_protocol_established_locations_Circle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Protocol.Established.Locations.Circle p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Circle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Boundary = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getBoundary;
		static IntPtr id_setBoundary_Lcom_gimbal_protocol_established_locations_Circle_;
		public virtual unsafe global::Com.Gimbal.Protocol.Established.Locations.Circle Boundary {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocation']/method[@name='getBoundary' and count(parameter)=0]"
			[Register ("getBoundary", "()Lcom/gimbal/protocol/established/locations/Circle;", "GetGetBoundaryHandler")]
			get {
				if (id_getBoundary == IntPtr.Zero)
					id_getBoundary = JNIEnv.GetMethodID (class_ref, "getBoundary", "()Lcom/gimbal/protocol/established/locations/Circle;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Circle> (JNIEnv.CallObjectMethod  (Handle, id_getBoundary), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.Circle> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBoundary", "()Lcom/gimbal/protocol/established/locations/Circle;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocation']/method[@name='setBoundary' and count(parameter)=1 and parameter[1][@type='com.gimbal.protocol.established.locations.Circle']]"
			[Register ("setBoundary", "(Lcom/gimbal/protocol/established/locations/Circle;)V", "GetSetBoundary_Lcom_gimbal_protocol_established_locations_Circle_Handler")]
			set {
				if (id_setBoundary_Lcom_gimbal_protocol_established_locations_Circle_ == IntPtr.Zero)
					id_setBoundary_Lcom_gimbal_protocol_established_locations_Circle_ = JNIEnv.GetMethodID (class_ref, "setBoundary", "(Lcom/gimbal/protocol/established/locations/Circle;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setBoundary_Lcom_gimbal_protocol_established_locations_Circle_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setBoundary", "(Lcom/gimbal/protocol/established/locations/Circle;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_String_Handler ()
		{
			if (cb_setId_Ljava_lang_String_ == null)
				cb_setId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_String_);
			return cb_setId_Ljava_lang_String_;
		}

		static void n_SetId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_String_;
		public virtual unsafe string Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocation']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/String;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocation']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setId", "(Ljava/lang/String;)V", "GetSetId_Ljava_lang_String_Handler")]
			set {
				if (id_setId_Ljava_lang_String_ == IntPtr.Zero)
					id_setId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getScore;
#pragma warning disable 0169
		static Delegate GetGetScoreHandler ()
		{
			if (cb_getScore == null)
				cb_getScore = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetScore);
			return cb_getScore;
		}

		static IntPtr n_GetScore (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Score);
		}
#pragma warning restore 0169

		static Delegate cb_setScore_Ljava_lang_Double_;
#pragma warning disable 0169
		static Delegate GetSetScore_Ljava_lang_Double_Handler ()
		{
			if (cb_setScore_Ljava_lang_Double_ == null)
				cb_setScore_Ljava_lang_Double_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetScore_Ljava_lang_Double_);
			return cb_setScore_Ljava_lang_Double_;
		}

		static void n_SetScore_Ljava_lang_Double_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Double p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Score = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getScore;
		static IntPtr id_setScore_Ljava_lang_Double_;
		public virtual unsafe global::Java.Lang.Double Score {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocation']/method[@name='getScore' and count(parameter)=0]"
			[Register ("getScore", "()Ljava/lang/Double;", "GetGetScoreHandler")]
			get {
				if (id_getScore == IntPtr.Zero)
					id_getScore = JNIEnv.GetMethodID (class_ref, "getScore", "()Ljava/lang/Double;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (JNIEnv.CallObjectMethod  (Handle, id_getScore), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getScore", "()Ljava/lang/Double;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocation']/method[@name='setScore' and count(parameter)=1 and parameter[1][@type='java.lang.Double']]"
			[Register ("setScore", "(Ljava/lang/Double;)V", "GetSetScore_Ljava_lang_Double_Handler")]
			set {
				if (id_setScore_Ljava_lang_Double_ == IntPtr.Zero)
					id_setScore_Ljava_lang_Double_ = JNIEnv.GetMethodID (class_ref, "setScore", "(Ljava/lang/Double;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setScore_Ljava_lang_Double_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setScore", "(Ljava/lang/Double;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getVisits;
#pragma warning disable 0169
		static Delegate GetGetVisitsHandler ()
		{
			if (cb_getVisits == null)
				cb_getVisits = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetVisits);
			return cb_getVisits;
		}

		static IntPtr n_GetVisits (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Established.Locations.Visit>.ToLocalJniHandle (__this.Visits);
		}
#pragma warning restore 0169

		static Delegate cb_setVisits_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetVisits_Ljava_util_List_Handler ()
		{
			if (cb_setVisits_Ljava_util_List_ == null)
				cb_setVisits_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetVisits_Ljava_util_List_);
			return cb_setVisits_Ljava_util_List_;
		}

		static void n_SetVisits_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Established.Locations.EstablishedLocation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Established.Locations.Visit>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Visits = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getVisits;
		static IntPtr id_setVisits_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Protocol.Established.Locations.Visit> Visits {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocation']/method[@name='getVisits' and count(parameter)=0]"
			[Register ("getVisits", "()Ljava/util/List;", "GetGetVisitsHandler")]
			get {
				if (id_getVisits == IntPtr.Zero)
					id_getVisits = JNIEnv.GetMethodID (class_ref, "getVisits", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Established.Locations.Visit>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getVisits), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Established.Locations.Visit>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getVisits", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol.established.locations']/class[@name='EstablishedLocation']/method[@name='setVisits' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.protocol.established.locations.Visit&gt;']]"
			[Register ("setVisits", "(Ljava/util/List;)V", "GetSetVisits_Ljava_util_List_Handler")]
			set {
				if (id_setVisits_Ljava_util_List_ == IntPtr.Zero)
					id_setVisits_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setVisits", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Established.Locations.Visit>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setVisits_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setVisits", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
