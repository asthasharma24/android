using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationManager']"
	[global::Android.Runtime.Register ("com/gimbal/android/CommunicationManager", DoNotGenerateAcw=true)]
	public partial class CommunicationManager : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/CommunicationManager", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CommunicationManager); }
		}

		protected CommunicationManager (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_getInstance;
		public static unsafe global::Com.Gimbal.Android.CommunicationManager Instance {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationManager']/method[@name='getInstance' and count(parameter)=0]"
			[Register ("getInstance", "()Lcom/gimbal/android/CommunicationManager;", "GetGetInstanceHandler")]
			get {
				if (id_getInstance == IntPtr.Zero)
					id_getInstance = JNIEnv.GetStaticMethodID (class_ref, "getInstance", "()Lcom/gimbal/android/CommunicationManager;");
				try {
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationManager> (JNIEnv.CallStaticObjectMethod  (class_ref, id_getInstance), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_isReceivingCommunications;
#pragma warning disable 0169
		static Delegate GetIsReceivingCommunicationsHandler ()
		{
			if (cb_isReceivingCommunications == null)
				cb_isReceivingCommunications = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsReceivingCommunications);
			return cb_isReceivingCommunications;
		}

		static bool n_IsReceivingCommunications (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.CommunicationManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsReceivingCommunications;
		}
#pragma warning restore 0169

		static IntPtr id_isReceivingCommunications;
		public virtual unsafe bool IsReceivingCommunications {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationManager']/method[@name='isReceivingCommunications' and count(parameter)=0]"
			[Register ("isReceivingCommunications", "()Z", "GetIsReceivingCommunicationsHandler")]
			get {
				if (id_isReceivingCommunications == IntPtr.Zero)
					id_isReceivingCommunications = JNIEnv.GetMethodID (class_ref, "isReceivingCommunications", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isReceivingCommunications);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isReceivingCommunications", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_addListener_Lcom_gimbal_android_CommunicationListener_;
#pragma warning disable 0169
		static Delegate GetAddListener_Lcom_gimbal_android_CommunicationListener_Handler ()
		{
			if (cb_addListener_Lcom_gimbal_android_CommunicationListener_ == null)
				cb_addListener_Lcom_gimbal_android_CommunicationListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddListener_Lcom_gimbal_android_CommunicationListener_);
			return cb_addListener_Lcom_gimbal_android_CommunicationListener_;
		}

		static void n_AddListener_Lcom_gimbal_android_CommunicationListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Android.CommunicationManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.CommunicationListener p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addListener_Lcom_gimbal_android_CommunicationListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationManager']/method[@name='addListener' and count(parameter)=1 and parameter[1][@type='com.gimbal.android.CommunicationListener']]"
		[Register ("addListener", "(Lcom/gimbal/android/CommunicationListener;)V", "GetAddListener_Lcom_gimbal_android_CommunicationListener_Handler")]
		public virtual unsafe void AddListener (global::Com.Gimbal.Android.CommunicationListener p0)
		{
			if (id_addListener_Lcom_gimbal_android_CommunicationListener_ == IntPtr.Zero)
				id_addListener_Lcom_gimbal_android_CommunicationListener_ = JNIEnv.GetMethodID (class_ref, "addListener", "(Lcom/gimbal/android/CommunicationListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addListener_Lcom_gimbal_android_CommunicationListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addListener", "(Lcom/gimbal/android/CommunicationListener;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_removeListener_Lcom_gimbal_android_CommunicationListener_;
#pragma warning disable 0169
		static Delegate GetRemoveListener_Lcom_gimbal_android_CommunicationListener_Handler ()
		{
			if (cb_removeListener_Lcom_gimbal_android_CommunicationListener_ == null)
				cb_removeListener_Lcom_gimbal_android_CommunicationListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RemoveListener_Lcom_gimbal_android_CommunicationListener_);
			return cb_removeListener_Lcom_gimbal_android_CommunicationListener_;
		}

		static void n_RemoveListener_Lcom_gimbal_android_CommunicationListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Android.CommunicationManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.CommunicationListener p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveListener (p0);
		}
#pragma warning restore 0169

		static IntPtr id_removeListener_Lcom_gimbal_android_CommunicationListener_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationManager']/method[@name='removeListener' and count(parameter)=1 and parameter[1][@type='com.gimbal.android.CommunicationListener']]"
		[Register ("removeListener", "(Lcom/gimbal/android/CommunicationListener;)V", "GetRemoveListener_Lcom_gimbal_android_CommunicationListener_Handler")]
		public virtual unsafe void RemoveListener (global::Com.Gimbal.Android.CommunicationListener p0)
		{
			if (id_removeListener_Lcom_gimbal_android_CommunicationListener_ == IntPtr.Zero)
				id_removeListener_Lcom_gimbal_android_CommunicationListener_ = JNIEnv.GetMethodID (class_ref, "removeListener", "(Lcom/gimbal/android/CommunicationListener;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_removeListener_Lcom_gimbal_android_CommunicationListener_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "removeListener", "(Lcom/gimbal/android/CommunicationListener;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_startReceivingCommunications;
#pragma warning disable 0169
		static Delegate GetStartReceivingCommunicationsHandler ()
		{
			if (cb_startReceivingCommunications == null)
				cb_startReceivingCommunications = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StartReceivingCommunications);
			return cb_startReceivingCommunications;
		}

		static void n_StartReceivingCommunications (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.CommunicationManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StartReceivingCommunications ();
		}
#pragma warning restore 0169

		static IntPtr id_startReceivingCommunications;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationManager']/method[@name='startReceivingCommunications' and count(parameter)=0]"
		[Register ("startReceivingCommunications", "()V", "GetStartReceivingCommunicationsHandler")]
		public virtual unsafe void StartReceivingCommunications ()
		{
			if (id_startReceivingCommunications == IntPtr.Zero)
				id_startReceivingCommunications = JNIEnv.GetMethodID (class_ref, "startReceivingCommunications", "()V");
			try {

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_startReceivingCommunications);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "startReceivingCommunications", "()V"));
			} finally {
			}
		}

		static Delegate cb_stopReceivingCommunications;
#pragma warning disable 0169
		static Delegate GetStopReceivingCommunicationsHandler ()
		{
			if (cb_stopReceivingCommunications == null)
				cb_stopReceivingCommunications = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_StopReceivingCommunications);
			return cb_stopReceivingCommunications;
		}

		static void n_StopReceivingCommunications (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Android.CommunicationManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.CommunicationManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.StopReceivingCommunications ();
		}
#pragma warning restore 0169

		static IntPtr id_stopReceivingCommunications;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='CommunicationManager']/method[@name='stopReceivingCommunications' and count(parameter)=0]"
		[Register ("stopReceivingCommunications", "()V", "GetStopReceivingCommunicationsHandler")]
		public virtual unsafe void StopReceivingCommunications ()
		{
			if (id_stopReceivingCommunications == IntPtr.Zero)
				id_stopReceivingCommunications = JNIEnv.GetMethodID (class_ref, "stopReceivingCommunications", "()V");
			try {

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_stopReceivingCommunications);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "stopReceivingCommunications", "()V"));
			} finally {
			}
		}

	}
}
