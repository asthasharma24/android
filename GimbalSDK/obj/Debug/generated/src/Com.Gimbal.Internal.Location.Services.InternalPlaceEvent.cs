using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Location.Services {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']"
	[global::Android.Runtime.Register ("com/gimbal/internal/location/services/InternalPlaceEvent", DoNotGenerateAcw=true)]
	public partial class InternalPlaceEvent : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/location/services/InternalPlaceEvent", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (InternalPlaceEvent); }
		}

		protected InternalPlaceEvent (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/constructor[@name='InternalPlaceEvent' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe InternalPlaceEvent ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (InternalPlaceEvent)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getArrivalTimeMillis;
#pragma warning disable 0169
		static Delegate GetGetArrivalTimeMillisHandler ()
		{
			if (cb_getArrivalTimeMillis == null)
				cb_getArrivalTimeMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetArrivalTimeMillis);
			return cb_getArrivalTimeMillis;
		}

		static IntPtr n_GetArrivalTimeMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ArrivalTimeMillis);
		}
#pragma warning restore 0169

		static Delegate cb_setArrivalTimeMillis_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetArrivalTimeMillis_Ljava_lang_Long_Handler ()
		{
			if (cb_setArrivalTimeMillis_Ljava_lang_Long_ == null)
				cb_setArrivalTimeMillis_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetArrivalTimeMillis_Ljava_lang_Long_);
			return cb_setArrivalTimeMillis_Ljava_lang_Long_;
		}

		static void n_SetArrivalTimeMillis_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ArrivalTimeMillis = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getArrivalTimeMillis;
		static IntPtr id_setArrivalTimeMillis_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long ArrivalTimeMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='getArrivalTimeMillis' and count(parameter)=0]"
			[Register ("getArrivalTimeMillis", "()Ljava/lang/Long;", "GetGetArrivalTimeMillisHandler")]
			get {
				if (id_getArrivalTimeMillis == IntPtr.Zero)
					id_getArrivalTimeMillis = JNIEnv.GetMethodID (class_ref, "getArrivalTimeMillis", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getArrivalTimeMillis), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getArrivalTimeMillis", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='setArrivalTimeMillis' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setArrivalTimeMillis", "(Ljava/lang/Long;)V", "GetSetArrivalTimeMillis_Ljava_lang_Long_Handler")]
			set {
				if (id_setArrivalTimeMillis_Ljava_lang_Long_ == IntPtr.Zero)
					id_setArrivalTimeMillis_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setArrivalTimeMillis", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setArrivalTimeMillis_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setArrivalTimeMillis", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureTimeMillis;
#pragma warning disable 0169
		static Delegate GetGetDepartureTimeMillisHandler ()
		{
			if (cb_getDepartureTimeMillis == null)
				cb_getDepartureTimeMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDepartureTimeMillis);
			return cb_getDepartureTimeMillis;
		}

		static IntPtr n_GetDepartureTimeMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DepartureTimeMillis);
		}
#pragma warning restore 0169

		static Delegate cb_setDepartureTimeMillis_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetDepartureTimeMillis_Ljava_lang_Long_Handler ()
		{
			if (cb_setDepartureTimeMillis_Ljava_lang_Long_ == null)
				cb_setDepartureTimeMillis_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDepartureTimeMillis_Ljava_lang_Long_);
			return cb_setDepartureTimeMillis_Ljava_lang_Long_;
		}

		static void n_SetDepartureTimeMillis_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DepartureTimeMillis = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureTimeMillis;
		static IntPtr id_setDepartureTimeMillis_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long DepartureTimeMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='getDepartureTimeMillis' and count(parameter)=0]"
			[Register ("getDepartureTimeMillis", "()Ljava/lang/Long;", "GetGetDepartureTimeMillisHandler")]
			get {
				if (id_getDepartureTimeMillis == IntPtr.Zero)
					id_getDepartureTimeMillis = JNIEnv.GetMethodID (class_ref, "getDepartureTimeMillis", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getDepartureTimeMillis), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureTimeMillis", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='setDepartureTimeMillis' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setDepartureTimeMillis", "(Ljava/lang/Long;)V", "GetSetDepartureTimeMillis_Ljava_lang_Long_Handler")]
			set {
				if (id_setDepartureTimeMillis_Ljava_lang_Long_ == IntPtr.Zero)
					id_setDepartureTimeMillis_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setDepartureTimeMillis", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDepartureTimeMillis_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDepartureTimeMillis", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getEventTypeAsString;
#pragma warning disable 0169
		static Delegate GetGetEventTypeAsStringHandler ()
		{
			if (cb_getEventTypeAsString == null)
				cb_getEventTypeAsString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEventTypeAsString);
			return cb_getEventTypeAsString;
		}

		static IntPtr n_GetEventTypeAsString (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.EventTypeAsString);
		}
#pragma warning restore 0169

		static IntPtr id_getEventTypeAsString;
		public virtual unsafe string EventTypeAsString {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='getEventTypeAsString' and count(parameter)=0]"
			[Register ("getEventTypeAsString", "()Ljava/lang/String;", "GetGetEventTypeAsStringHandler")]
			get {
				if (id_getEventTypeAsString == IntPtr.Zero)
					id_getEventTypeAsString = JNIEnv.GetMethodID (class_ref, "getEventTypeAsString", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getEventTypeAsString), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEventTypeAsString", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getFenceId;
#pragma warning disable 0169
		static Delegate GetGetFenceIdHandler ()
		{
			if (cb_getFenceId == null)
				cb_getFenceId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetFenceId);
			return cb_getFenceId;
		}

		static IntPtr n_GetFenceId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.FenceId);
		}
#pragma warning restore 0169

		static Delegate cb_setFenceId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetFenceId_Ljava_lang_String_Handler ()
		{
			if (cb_setFenceId_Ljava_lang_String_ == null)
				cb_setFenceId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetFenceId_Ljava_lang_String_);
			return cb_setFenceId_Ljava_lang_String_;
		}

		static void n_SetFenceId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.FenceId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getFenceId;
		static IntPtr id_setFenceId_Ljava_lang_String_;
		public virtual unsafe string FenceId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='getFenceId' and count(parameter)=0]"
			[Register ("getFenceId", "()Ljava/lang/String;", "GetGetFenceIdHandler")]
			get {
				if (id_getFenceId == IntPtr.Zero)
					id_getFenceId = JNIEnv.GetMethodID (class_ref, "getFenceId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getFenceId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getFenceId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='setFenceId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setFenceId", "(Ljava/lang/String;)V", "GetSetFenceId_Ljava_lang_String_Handler")]
			set {
				if (id_setFenceId_Ljava_lang_String_ == IntPtr.Zero)
					id_setFenceId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setFenceId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setFenceId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setFenceId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getFenceTypeAsString;
#pragma warning disable 0169
		static Delegate GetGetFenceTypeAsStringHandler ()
		{
			if (cb_getFenceTypeAsString == null)
				cb_getFenceTypeAsString = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetFenceTypeAsString);
			return cb_getFenceTypeAsString;
		}

		static IntPtr n_GetFenceTypeAsString (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.FenceTypeAsString);
		}
#pragma warning restore 0169

		static IntPtr id_getFenceTypeAsString;
		public virtual unsafe string FenceTypeAsString {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='getFenceTypeAsString' and count(parameter)=0]"
			[Register ("getFenceTypeAsString", "()Ljava/lang/String;", "GetGetFenceTypeAsStringHandler")]
			get {
				if (id_getFenceTypeAsString == IntPtr.Zero)
					id_getFenceTypeAsString = JNIEnv.GetMethodID (class_ref, "getFenceTypeAsString", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getFenceTypeAsString), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getFenceTypeAsString", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getLongitude;
#pragma warning disable 0169
		static Delegate GetGetLongitudeHandler ()
		{
			if (cb_getLongitude == null)
				cb_getLongitude = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_GetLongitude);
			return cb_getLongitude;
		}

		static double n_GetLongitude (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Longitude;
		}
#pragma warning restore 0169

		static Delegate cb_setLongitude_D;
#pragma warning disable 0169
		static Delegate GetSetLongitude_DHandler ()
		{
			if (cb_setLongitude_D == null)
				cb_setLongitude_D = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, double>) n_SetLongitude_D);
			return cb_setLongitude_D;
		}

		static void n_SetLongitude_D (IntPtr jnienv, IntPtr native__this, double p0)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Longitude = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLongitude;
		static IntPtr id_setLongitude_D;
		public virtual unsafe double Longitude {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='getLongitude' and count(parameter)=0]"
			[Register ("getLongitude", "()D", "GetGetLongitudeHandler")]
			get {
				if (id_getLongitude == IntPtr.Zero)
					id_getLongitude = JNIEnv.GetMethodID (class_ref, "getLongitude", "()D");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallDoubleMethod  (Handle, id_getLongitude);
					else
						return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLongitude", "()D"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='setLongitude' and count(parameter)=1 and parameter[1][@type='double']]"
			[Register ("setLongitude", "(D)V", "GetSetLongitude_DHandler")]
			set {
				if (id_setLongitude_D == IntPtr.Zero)
					id_setLongitude_D = JNIEnv.GetMethodID (class_ref, "setLongitude", "(D)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLongitude_D, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLongitude", "(D)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPlaceId;
#pragma warning disable 0169
		static Delegate GetGetPlaceIdHandler ()
		{
			if (cb_getPlaceId == null)
				cb_getPlaceId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlaceId);
			return cb_getPlaceId;
		}

		static IntPtr n_GetPlaceId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.PlaceId);
		}
#pragma warning restore 0169

		static Delegate cb_setPlaceId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetPlaceId_Ljava_lang_Long_Handler ()
		{
			if (cb_setPlaceId_Ljava_lang_Long_ == null)
				cb_setPlaceId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPlaceId_Ljava_lang_Long_);
			return cb_setPlaceId_Ljava_lang_Long_;
		}

		static void n_SetPlaceId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.PlaceId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPlaceId;
		static IntPtr id_setPlaceId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long PlaceId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='getPlaceId' and count(parameter)=0]"
			[Register ("getPlaceId", "()Ljava/lang/Long;", "GetGetPlaceIdHandler")]
			get {
				if (id_getPlaceId == IntPtr.Zero)
					id_getPlaceId = JNIEnv.GetMethodID (class_ref, "getPlaceId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getPlaceId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlaceId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='setPlaceId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setPlaceId", "(Ljava/lang/Long;)V", "GetSetPlaceId_Ljava_lang_Long_Handler")]
			set {
				if (id_setPlaceId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setPlaceId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setPlaceId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPlaceId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPlaceId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPlaceUuid;
#pragma warning disable 0169
		static Delegate GetGetPlaceUuidHandler ()
		{
			if (cb_getPlaceUuid == null)
				cb_getPlaceUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlaceUuid);
			return cb_getPlaceUuid;
		}

		static IntPtr n_GetPlaceUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.PlaceUuid);
		}
#pragma warning restore 0169

		static Delegate cb_setPlaceUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetPlaceUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setPlaceUuid_Ljava_lang_String_ == null)
				cb_setPlaceUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPlaceUuid_Ljava_lang_String_);
			return cb_setPlaceUuid_Ljava_lang_String_;
		}

		static void n_SetPlaceUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.PlaceUuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPlaceUuid;
		static IntPtr id_setPlaceUuid_Ljava_lang_String_;
		public virtual unsafe string PlaceUuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='getPlaceUuid' and count(parameter)=0]"
			[Register ("getPlaceUuid", "()Ljava/lang/String;", "GetGetPlaceUuidHandler")]
			get {
				if (id_getPlaceUuid == IntPtr.Zero)
					id_getPlaceUuid = JNIEnv.GetMethodID (class_ref, "getPlaceUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getPlaceUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlaceUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='setPlaceUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setPlaceUuid", "(Ljava/lang/String;)V", "GetSetPlaceUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setPlaceUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setPlaceUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setPlaceUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPlaceUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPlaceUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getlatitude;
#pragma warning disable 0169
		static Delegate GetGetlatitudeHandler ()
		{
			if (cb_getlatitude == null)
				cb_getlatitude = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_Getlatitude);
			return cb_getlatitude;
		}

		static double n_Getlatitude (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Getlatitude ();
		}
#pragma warning restore 0169

		static IntPtr id_getlatitude;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='getlatitude' and count(parameter)=0]"
		[Register ("getlatitude", "()D", "GetGetlatitudeHandler")]
		public virtual unsafe double Getlatitude ()
		{
			if (id_getlatitude == IntPtr.Zero)
				id_getlatitude = JNIEnv.GetMethodID (class_ref, "getlatitude", "()D");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallDoubleMethod  (Handle, id_getlatitude);
				else
					return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getlatitude", "()D"));
			} finally {
			}
		}

		static Delegate cb_setlatitude_D;
#pragma warning disable 0169
		static Delegate GetSetlatitude_DHandler ()
		{
			if (cb_setlatitude_D == null)
				cb_setlatitude_D = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, double>) n_Setlatitude_D);
			return cb_setlatitude_D;
		}

		static void n_Setlatitude_D (IntPtr jnienv, IntPtr native__this, double p0)
		{
			global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Location.Services.InternalPlaceEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Setlatitude (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setlatitude_D;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.location.services']/class[@name='InternalPlaceEvent']/method[@name='setlatitude' and count(parameter)=1 and parameter[1][@type='double']]"
		[Register ("setlatitude", "(D)V", "GetSetlatitude_DHandler")]
		public virtual unsafe void Setlatitude (double p0)
		{
			if (id_setlatitude_D == IntPtr.Zero)
				id_setlatitude_D = JNIEnv.GetMethodID (class_ref, "setlatitude", "(D)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setlatitude_D, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setlatitude", "(D)V"), __args);
			} finally {
			}
		}

	}
}
