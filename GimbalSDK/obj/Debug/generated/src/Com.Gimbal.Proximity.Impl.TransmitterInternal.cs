using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Impl {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/impl/TransmitterInternal", DoNotGenerateAcw=true)]
	public partial class TransmitterInternal : global::Java.Lang.Object, global::Android.OS.IParcelable, global::Com.Gimbal.Proguard.IKeep {


		static IntPtr CREATOR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/field[@name='CREATOR']"
		[Register ("CREATOR")]
		public static global::Android.OS.IParcelableCreator Creator {
			get {
				if (CREATOR_jfieldId == IntPtr.Zero)
					CREATOR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CREATOR", "Landroid/os/Parcelable$Creator;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CREATOR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelableCreator> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/impl/TransmitterInternal", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (TransmitterInternal); }
		}

		protected TransmitterInternal (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/constructor[@name='TransmitterInternal' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe TransmitterInternal ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (TransmitterInternal)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getBattery;
#pragma warning disable 0169
		static Delegate GetGetBatteryHandler ()
		{
			if (cb_getBattery == null)
				cb_getBattery = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetBattery);
			return cb_getBattery;
		}

		static int n_GetBattery (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Battery;
		}
#pragma warning restore 0169

		static IntPtr id_getBattery;
		public virtual unsafe int Battery {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='getBattery' and count(parameter)=0]"
			[Register ("getBattery", "()I", "GetGetBatteryHandler")]
			get {
				if (id_getBattery == IntPtr.Zero)
					id_getBattery = JNIEnv.GetMethodID (class_ref, "getBattery", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getBattery);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBattery", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getIconUrl;
#pragma warning disable 0169
		static Delegate GetGetIconUrlHandler ()
		{
			if (cb_getIconUrl == null)
				cb_getIconUrl = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIconUrl);
			return cb_getIconUrl;
		}

		static IntPtr n_GetIconUrl (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.IconUrl);
		}
#pragma warning restore 0169

		static Delegate cb_setIconUrl_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetIconUrl_Ljava_lang_String_Handler ()
		{
			if (cb_setIconUrl_Ljava_lang_String_ == null)
				cb_setIconUrl_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetIconUrl_Ljava_lang_String_);
			return cb_setIconUrl_Ljava_lang_String_;
		}

		static void n_SetIconUrl_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.IconUrl = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getIconUrl;
		static IntPtr id_setIconUrl_Ljava_lang_String_;
		public virtual unsafe string IconUrl {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='getIconUrl' and count(parameter)=0]"
			[Register ("getIconUrl", "()Ljava/lang/String;", "GetGetIconUrlHandler")]
			get {
				if (id_getIconUrl == IntPtr.Zero)
					id_getIconUrl = JNIEnv.GetMethodID (class_ref, "getIconUrl", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getIconUrl), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getIconUrl", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='setIconUrl' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setIconUrl", "(Ljava/lang/String;)V", "GetSetIconUrl_Ljava_lang_String_Handler")]
			set {
				if (id_setIconUrl_Ljava_lang_String_ == IntPtr.Zero)
					id_setIconUrl_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setIconUrl", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setIconUrl_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setIconUrl", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getIdentifier;
#pragma warning disable 0169
		static Delegate GetGetIdentifierHandler ()
		{
			if (cb_getIdentifier == null)
				cb_getIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIdentifier);
			return cb_getIdentifier;
		}

		static IntPtr n_GetIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Identifier);
		}
#pragma warning restore 0169

		static Delegate cb_setIdentifier_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetIdentifier_Ljava_lang_String_Handler ()
		{
			if (cb_setIdentifier_Ljava_lang_String_ == null)
				cb_setIdentifier_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetIdentifier_Ljava_lang_String_);
			return cb_setIdentifier_Ljava_lang_String_;
		}

		static void n_SetIdentifier_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Identifier = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getIdentifier;
		static IntPtr id_setIdentifier_Ljava_lang_String_;
		public virtual unsafe string Identifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='getIdentifier' and count(parameter)=0]"
			[Register ("getIdentifier", "()Ljava/lang/String;", "GetGetIdentifierHandler")]
			get {
				if (id_getIdentifier == IntPtr.Zero)
					id_getIdentifier = JNIEnv.GetMethodID (class_ref, "getIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='setIdentifier' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setIdentifier", "(Ljava/lang/String;)V", "GetSetIdentifier_Ljava_lang_String_Handler")]
			set {
				if (id_setIdentifier_Ljava_lang_String_ == IntPtr.Zero)
					id_setIdentifier_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setIdentifier", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setIdentifier_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setIdentifier", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getName;
#pragma warning disable 0169
		static Delegate GetGetNameHandler ()
		{
			if (cb_getName == null)
				cb_getName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetName);
			return cb_getName;
		}

		static IntPtr n_GetName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Name);
		}
#pragma warning restore 0169

		static Delegate cb_setName_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetName_Ljava_lang_String_Handler ()
		{
			if (cb_setName_Ljava_lang_String_ == null)
				cb_setName_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetName_Ljava_lang_String_);
			return cb_setName_Ljava_lang_String_;
		}

		static void n_SetName_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Name = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getName;
		static IntPtr id_setName_Ljava_lang_String_;
		public virtual unsafe string Name {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='getName' and count(parameter)=0]"
			[Register ("getName", "()Ljava/lang/String;", "GetGetNameHandler")]
			get {
				if (id_getName == IntPtr.Zero)
					id_getName = JNIEnv.GetMethodID (class_ref, "getName", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getName), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getName", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='setName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setName", "(Ljava/lang/String;)V", "GetSetName_Ljava_lang_String_Handler")]
			set {
				if (id_setName_Ljava_lang_String_ == IntPtr.Zero)
					id_setName_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setName", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setName_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setName", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getOwnerId;
#pragma warning disable 0169
		static Delegate GetGetOwnerIdHandler ()
		{
			if (cb_getOwnerId == null)
				cb_getOwnerId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOwnerId);
			return cb_getOwnerId;
		}

		static IntPtr n_GetOwnerId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.OwnerId);
		}
#pragma warning restore 0169

		static Delegate cb_setOwnerId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetOwnerId_Ljava_lang_String_Handler ()
		{
			if (cb_setOwnerId_Ljava_lang_String_ == null)
				cb_setOwnerId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOwnerId_Ljava_lang_String_);
			return cb_setOwnerId_Ljava_lang_String_;
		}

		static void n_SetOwnerId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OwnerId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOwnerId;
		static IntPtr id_setOwnerId_Ljava_lang_String_;
		public virtual unsafe string OwnerId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='getOwnerId' and count(parameter)=0]"
			[Register ("getOwnerId", "()Ljava/lang/String;", "GetGetOwnerIdHandler")]
			get {
				if (id_getOwnerId == IntPtr.Zero)
					id_getOwnerId = JNIEnv.GetMethodID (class_ref, "getOwnerId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getOwnerId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOwnerId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='setOwnerId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setOwnerId", "(Ljava/lang/String;)V", "GetSetOwnerId_Ljava_lang_String_Handler")]
			set {
				if (id_setOwnerId_Ljava_lang_String_ == IntPtr.Zero)
					id_setOwnerId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setOwnerId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOwnerId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOwnerId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getPlaces;
#pragma warning disable 0169
		static Delegate GetGetPlacesHandler ()
		{
			if (cb_getPlaces == null)
				cb_getPlaces = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlaces);
			return cb_getPlaces;
		}

		static IntPtr n_GetPlaces (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Impl.PlaceInternal>.ToLocalJniHandle (__this.Places);
		}
#pragma warning restore 0169

		static Delegate cb_setPlaces_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetPlaces_Ljava_util_List_Handler ()
		{
			if (cb_setPlaces_Ljava_util_List_ == null)
				cb_setPlaces_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPlaces_Ljava_util_List_);
			return cb_setPlaces_Ljava_util_List_;
		}

		static void n_SetPlaces_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Impl.PlaceInternal>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Places = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPlaces;
		static IntPtr id_setPlaces_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Proximity.Impl.PlaceInternal> Places {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='getPlaces' and count(parameter)=0]"
			[Register ("getPlaces", "()Ljava/util/List;", "GetGetPlacesHandler")]
			get {
				if (id_getPlaces == IntPtr.Zero)
					id_getPlaces = JNIEnv.GetMethodID (class_ref, "getPlaces", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Impl.PlaceInternal>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getPlaces), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Impl.PlaceInternal>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlaces", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='setPlaces' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.proximity.impl.PlaceInternal&gt;']]"
			[Register ("setPlaces", "(Ljava/util/List;)V", "GetSetPlaces_Ljava_util_List_Handler")]
			set {
				if (id_setPlaces_Ljava_util_List_ == IntPtr.Zero)
					id_setPlaces_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setPlaces", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Impl.PlaceInternal>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPlaces_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPlaces", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getTemperature;
#pragma warning disable 0169
		static Delegate GetGetTemperatureHandler ()
		{
			if (cb_getTemperature == null)
				cb_getTemperature = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetTemperature);
			return cb_getTemperature;
		}

		static int n_GetTemperature (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Temperature;
		}
#pragma warning restore 0169

		static IntPtr id_getTemperature;
		public virtual unsafe int Temperature {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='getTemperature' and count(parameter)=0]"
			[Register ("getTemperature", "()I", "GetGetTemperatureHandler")]
			get {
				if (id_getTemperature == IntPtr.Zero)
					id_getTemperature = JNIEnv.GetMethodID (class_ref, "getTemperature", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getTemperature);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTemperature", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		static Delegate cb_setUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setUuid_Ljava_lang_String_ == null)
				cb_setUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUuid_Ljava_lang_String_);
			return cb_setUuid_Ljava_lang_String_;
		}

		static void n_SetUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Uuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUuid;
		static IntPtr id_setUuid_Ljava_lang_String_;
		public virtual unsafe string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler")]
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='setUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUuid", "(Ljava/lang/String;)V", "GetSetUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_describeContents;
#pragma warning disable 0169
		static Delegate GetDescribeContentsHandler ()
		{
			if (cb_describeContents == null)
				cb_describeContents = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_DescribeContents);
			return cb_describeContents;
		}

		static int n_DescribeContents (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DescribeContents ();
		}
#pragma warning restore 0169

		static IntPtr id_describeContents;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='describeContents' and count(parameter)=0]"
		[Register ("describeContents", "()I", "GetDescribeContentsHandler")]
		public virtual unsafe int DescribeContents ()
		{
			if (id_describeContents == IntPtr.Zero)
				id_describeContents = JNIEnv.GetMethodID (class_ref, "describeContents", "()I");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallIntMethod  (Handle, id_describeContents);
				else
					return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "describeContents", "()I"));
			} finally {
			}
		}

		static Delegate cb_setBattery_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetBattery_Ljava_lang_Integer_Handler ()
		{
			if (cb_setBattery_Ljava_lang_Integer_ == null)
				cb_setBattery_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetBattery_Ljava_lang_Integer_);
			return cb_setBattery_Ljava_lang_Integer_;
		}

		static void n_SetBattery_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetBattery (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setBattery_Ljava_lang_Integer_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='setBattery' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
		[Register ("setBattery", "(Ljava/lang/Integer;)V", "GetSetBattery_Ljava_lang_Integer_Handler")]
		public virtual unsafe void SetBattery (global::Java.Lang.Integer p0)
		{
			if (id_setBattery_Ljava_lang_Integer_ == IntPtr.Zero)
				id_setBattery_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setBattery", "(Ljava/lang/Integer;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setBattery_Ljava_lang_Integer_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setBattery", "(Ljava/lang/Integer;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_setTemperature_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetTemperature_Ljava_lang_Integer_Handler ()
		{
			if (cb_setTemperature_Ljava_lang_Integer_ == null)
				cb_setTemperature_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTemperature_Ljava_lang_Integer_);
			return cb_setTemperature_Ljava_lang_Integer_;
		}

		static void n_SetTemperature_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetTemperature (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setTemperature_Ljava_lang_Integer_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='setTemperature' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
		[Register ("setTemperature", "(Ljava/lang/Integer;)V", "GetSetTemperature_Ljava_lang_Integer_Handler")]
		public virtual unsafe void SetTemperature (global::Java.Lang.Integer p0)
		{
			if (id_setTemperature_Ljava_lang_Integer_ == IntPtr.Zero)
				id_setTemperature_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setTemperature", "(Ljava/lang/Integer;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setTemperature_Ljava_lang_Integer_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTemperature", "(Ljava/lang/Integer;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_writeToParcel_Landroid_os_Parcel_I;
#pragma warning disable 0169
		static Delegate GetWriteToParcel_Landroid_os_Parcel_IHandler ()
		{
			if (cb_writeToParcel_Landroid_os_Parcel_I == null)
				cb_writeToParcel_Landroid_os_Parcel_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_WriteToParcel_Landroid_os_Parcel_I);
			return cb_writeToParcel_Landroid_os_Parcel_I;
		}

		static void n_WriteToParcel_Landroid_os_Parcel_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int native_p1)
		{
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Parcel p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Parcel> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.ParcelableWriteFlags p1 = (global::Android.OS.ParcelableWriteFlags) native_p1;
			__this.WriteToParcel (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_writeToParcel_Landroid_os_Parcel_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='TransmitterInternal']/method[@name='writeToParcel' and count(parameter)=2 and parameter[1][@type='android.os.Parcel'] and parameter[2][@type='int']]"
		[Register ("writeToParcel", "(Landroid/os/Parcel;I)V", "GetWriteToParcel_Landroid_os_Parcel_IHandler")]
		public virtual unsafe void WriteToParcel (global::Android.OS.Parcel p0, [global::Android.Runtime.GeneratedEnum] global::Android.OS.ParcelableWriteFlags p1)
		{
			if (id_writeToParcel_Landroid_os_Parcel_I == IntPtr.Zero)
				id_writeToParcel_Landroid_os_Parcel_I = JNIEnv.GetMethodID (class_ref, "writeToParcel", "(Landroid/os/Parcel;I)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue ((int) p1);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_writeToParcel_Landroid_os_Parcel_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeToParcel", "(Landroid/os/Parcel;I)V"), __args);
			} finally {
			}
		}

	}
}
