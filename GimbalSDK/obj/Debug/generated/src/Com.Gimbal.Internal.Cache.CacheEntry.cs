using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Cache {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.cache']/class[@name='CacheEntry']"
	[global::Android.Runtime.Register ("com/gimbal/internal/cache/CacheEntry", DoNotGenerateAcw=true)]
	[global::Java.Interop.JavaTypeParameters (new string [] {"T"})]
	public partial class CacheEntry : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep, global::Java.Lang.IComparable {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/cache/CacheEntry", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (CacheEntry); }
		}

		protected CacheEntry (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.cache']/class[@name='CacheEntry']/constructor[@name='CacheEntry' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe CacheEntry ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (CacheEntry)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getEstablishedInCacheTime;
#pragma warning disable 0169
		static Delegate GetGetEstablishedInCacheTimeHandler ()
		{
			if (cb_getEstablishedInCacheTime == null)
				cb_getEstablishedInCacheTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetEstablishedInCacheTime);
			return cb_getEstablishedInCacheTime;
		}

		static long n_GetEstablishedInCacheTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Cache.CacheEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Cache.CacheEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.EstablishedInCacheTime;
		}
#pragma warning restore 0169

		static Delegate cb_setEstablishedInCacheTime_J;
#pragma warning disable 0169
		static Delegate GetSetEstablishedInCacheTime_JHandler ()
		{
			if (cb_setEstablishedInCacheTime_J == null)
				cb_setEstablishedInCacheTime_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetEstablishedInCacheTime_J);
			return cb_setEstablishedInCacheTime_J;
		}

		static void n_SetEstablishedInCacheTime_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Internal.Cache.CacheEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Cache.CacheEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.EstablishedInCacheTime = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEstablishedInCacheTime;
		static IntPtr id_setEstablishedInCacheTime_J;
		public virtual unsafe long EstablishedInCacheTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.cache']/class[@name='CacheEntry']/method[@name='getEstablishedInCacheTime' and count(parameter)=0]"
			[Register ("getEstablishedInCacheTime", "()J", "GetGetEstablishedInCacheTimeHandler")]
			get {
				if (id_getEstablishedInCacheTime == IntPtr.Zero)
					id_getEstablishedInCacheTime = JNIEnv.GetMethodID (class_ref, "getEstablishedInCacheTime", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getEstablishedInCacheTime);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEstablishedInCacheTime", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.cache']/class[@name='CacheEntry']/method[@name='setEstablishedInCacheTime' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setEstablishedInCacheTime", "(J)V", "GetSetEstablishedInCacheTime_JHandler")]
			set {
				if (id_setEstablishedInCacheTime_J == IntPtr.Zero)
					id_setEstablishedInCacheTime_J = JNIEnv.GetMethodID (class_ref, "setEstablishedInCacheTime", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEstablishedInCacheTime_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEstablishedInCacheTime", "(J)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getExpireTimeInMilliseconds;
#pragma warning disable 0169
		static Delegate GetGetExpireTimeInMillisecondsHandler ()
		{
			if (cb_getExpireTimeInMilliseconds == null)
				cb_getExpireTimeInMilliseconds = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetExpireTimeInMilliseconds);
			return cb_getExpireTimeInMilliseconds;
		}

		static long n_GetExpireTimeInMilliseconds (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Cache.CacheEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Cache.CacheEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ExpireTimeInMilliseconds;
		}
#pragma warning restore 0169

		static Delegate cb_setExpireTimeInMilliseconds_J;
#pragma warning disable 0169
		static Delegate GetSetExpireTimeInMilliseconds_JHandler ()
		{
			if (cb_setExpireTimeInMilliseconds_J == null)
				cb_setExpireTimeInMilliseconds_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetExpireTimeInMilliseconds_J);
			return cb_setExpireTimeInMilliseconds_J;
		}

		static void n_SetExpireTimeInMilliseconds_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Internal.Cache.CacheEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Cache.CacheEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ExpireTimeInMilliseconds = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getExpireTimeInMilliseconds;
		static IntPtr id_setExpireTimeInMilliseconds_J;
		public virtual unsafe long ExpireTimeInMilliseconds {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.cache']/class[@name='CacheEntry']/method[@name='getExpireTimeInMilliseconds' and count(parameter)=0]"
			[Register ("getExpireTimeInMilliseconds", "()J", "GetGetExpireTimeInMillisecondsHandler")]
			get {
				if (id_getExpireTimeInMilliseconds == IntPtr.Zero)
					id_getExpireTimeInMilliseconds = JNIEnv.GetMethodID (class_ref, "getExpireTimeInMilliseconds", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getExpireTimeInMilliseconds);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getExpireTimeInMilliseconds", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.cache']/class[@name='CacheEntry']/method[@name='setExpireTimeInMilliseconds' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setExpireTimeInMilliseconds", "(J)V", "GetSetExpireTimeInMilliseconds_JHandler")]
			set {
				if (id_setExpireTimeInMilliseconds_J == IntPtr.Zero)
					id_setExpireTimeInMilliseconds_J = JNIEnv.GetMethodID (class_ref, "setExpireTimeInMilliseconds", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setExpireTimeInMilliseconds_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setExpireTimeInMilliseconds", "(J)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getKey;
#pragma warning disable 0169
		static Delegate GetGetKeyHandler ()
		{
			if (cb_getKey == null)
				cb_getKey = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetKey);
			return cb_getKey;
		}

		static IntPtr n_GetKey (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Cache.CacheEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Cache.CacheEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Key);
		}
#pragma warning restore 0169

		static Delegate cb_setKey_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetKey_Ljava_lang_String_Handler ()
		{
			if (cb_setKey_Ljava_lang_String_ == null)
				cb_setKey_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetKey_Ljava_lang_String_);
			return cb_setKey_Ljava_lang_String_;
		}

		static void n_SetKey_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Cache.CacheEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Cache.CacheEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Key = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getKey;
		static IntPtr id_setKey_Ljava_lang_String_;
		public virtual unsafe string Key {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.cache']/class[@name='CacheEntry']/method[@name='getKey' and count(parameter)=0]"
			[Register ("getKey", "()Ljava/lang/String;", "GetGetKeyHandler")]
			get {
				if (id_getKey == IntPtr.Zero)
					id_getKey = JNIEnv.GetMethodID (class_ref, "getKey", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getKey), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getKey", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.cache']/class[@name='CacheEntry']/method[@name='setKey' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setKey", "(Ljava/lang/String;)V", "GetSetKey_Ljava_lang_String_Handler")]
			set {
				if (id_setKey_Ljava_lang_String_ == IntPtr.Zero)
					id_setKey_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setKey", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setKey_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setKey", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getValue;
#pragma warning disable 0169
		static Delegate GetGetValueHandler ()
		{
			if (cb_getValue == null)
				cb_getValue = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetValue);
			return cb_getValue;
		}

		static IntPtr n_GetValue (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Cache.CacheEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Cache.CacheEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Value);
		}
#pragma warning restore 0169

		static Delegate cb_setValue_Ljava_lang_Object_;
#pragma warning disable 0169
		static Delegate GetSetValue_Ljava_lang_Object_Handler ()
		{
			if (cb_setValue_Ljava_lang_Object_ == null)
				cb_setValue_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetValue_Ljava_lang_Object_);
			return cb_setValue_Ljava_lang_Object_;
		}

		static void n_SetValue_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Cache.CacheEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Cache.CacheEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Object p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Value = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getValue;
		static IntPtr id_setValue_Ljava_lang_Object_;
		public virtual unsafe global::Java.Lang.Object Value {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.cache']/class[@name='CacheEntry']/method[@name='getValue' and count(parameter)=0]"
			[Register ("getValue", "()Ljava/lang/Object;", "GetGetValueHandler")]
			get {
				if (id_getValue == IntPtr.Zero)
					id_getValue = JNIEnv.GetMethodID (class_ref, "getValue", "()Ljava/lang/Object;");
				try {

					if (GetType () == ThresholdType)
						return (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod  (Handle, id_getValue), JniHandleOwnership.TransferLocalRef);
					else
						return (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getValue", "()Ljava/lang/Object;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.cache']/class[@name='CacheEntry']/method[@name='setValue' and count(parameter)=1 and parameter[1][@type='T']]"
			[Register ("setValue", "(Ljava/lang/Object;)V", "GetSetValue_Ljava_lang_Object_Handler")]
			set {
				if (id_setValue_Ljava_lang_Object_ == IntPtr.Zero)
					id_setValue_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "setValue", "(Ljava/lang/Object;)V");
				IntPtr native_value = JNIEnv.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setValue_Ljava_lang_Object_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setValue", "(Ljava/lang/Object;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_compareTo_Lcom_gimbal_internal_cache_CacheEntry_;
#pragma warning disable 0169
		static Delegate GetCompareTo_Lcom_gimbal_internal_cache_CacheEntry_Handler ()
		{
			if (cb_compareTo_Lcom_gimbal_internal_cache_CacheEntry_ == null)
				cb_compareTo_Lcom_gimbal_internal_cache_CacheEntry_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, int>) n_CompareTo_Lcom_gimbal_internal_cache_CacheEntry_);
			return cb_compareTo_Lcom_gimbal_internal_cache_CacheEntry_;
		}

		static int n_CompareTo_Lcom_gimbal_internal_cache_CacheEntry_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Cache.CacheEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Cache.CacheEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Internal.Cache.CacheEntry p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Cache.CacheEntry> (native_p0, JniHandleOwnership.DoNotTransfer);
			int __ret = __this.CompareTo (p0);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_compareTo_Lcom_gimbal_internal_cache_CacheEntry_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.cache']/class[@name='CacheEntry']/method[@name='compareTo' and count(parameter)=1 and parameter[1][@type='com.gimbal.internal.cache.CacheEntry&lt;T&gt;']]"
		[Register ("compareTo", "(Lcom/gimbal/internal/cache/CacheEntry;)I", "GetCompareTo_Lcom_gimbal_internal_cache_CacheEntry_Handler")]
		public virtual unsafe int CompareTo (global::Java.Lang.Object p0)
		{
			if (id_compareTo_Lcom_gimbal_internal_cache_CacheEntry_ == IntPtr.Zero)
				id_compareTo_Lcom_gimbal_internal_cache_CacheEntry_ = JNIEnv.GetMethodID (class_ref, "compareTo", "(Lcom/gimbal/internal/cache/CacheEntry;)I");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				int __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.CallIntMethod  (Handle, id_compareTo_Lcom_gimbal_internal_cache_CacheEntry_, __args);
				else
					__ret = JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "compareTo", "(Lcom/gimbal/internal/cache/CacheEntry;)I"), __args);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_expired;
#pragma warning disable 0169
		static Delegate GetExpiredHandler ()
		{
			if (cb_expired == null)
				cb_expired = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_Expired);
			return cb_expired;
		}

		static bool n_Expired (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Cache.CacheEntry __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Cache.CacheEntry> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Expired ();
		}
#pragma warning restore 0169

		static IntPtr id_expired;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.cache']/class[@name='CacheEntry']/method[@name='expired' and count(parameter)=0]"
		[Register ("expired", "()Z", "GetExpiredHandler")]
		public virtual unsafe bool Expired ()
		{
			if (id_expired == IntPtr.Zero)
				id_expired = JNIEnv.GetMethodID (class_ref, "expired", "()Z");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallBooleanMethod  (Handle, id_expired);
				else
					return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "expired", "()Z"));
			} finally {
			}
		}

	}
}
