using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Impl {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/impl/InternalBeaconFenceVisit", DoNotGenerateAcw=true)]
	public partial class InternalBeaconFenceVisit : global::Java.Lang.Object, global::Android.OS.IParcelable, global::Com.Gimbal.Proguard.IKeep {


		static IntPtr CREATOR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/field[@name='CREATOR']"
		[Register ("CREATOR")]
		public static global::Android.OS.IParcelableCreator Creator {
			get {
				if (CREATOR_jfieldId == IntPtr.Zero)
					CREATOR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CREATOR", "Landroid/os/Parcelable$Creator;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CREATOR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelableCreator> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/impl/InternalBeaconFenceVisit", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (InternalBeaconFenceVisit); }
		}

		protected InternalBeaconFenceVisit (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/constructor[@name='InternalBeaconFenceVisit' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe InternalBeaconFenceVisit ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (InternalBeaconFenceVisit)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getArrivalDate;
#pragma warning disable 0169
		static Delegate GetGetArrivalDateHandler ()
		{
			if (cb_getArrivalDate == null)
				cb_getArrivalDate = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetArrivalDate);
			return cb_getArrivalDate;
		}

		static IntPtr n_GetArrivalDate (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ArrivalDate);
		}
#pragma warning restore 0169

		static Delegate cb_setArrivalDate_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetArrivalDate_Ljava_lang_Long_Handler ()
		{
			if (cb_setArrivalDate_Ljava_lang_Long_ == null)
				cb_setArrivalDate_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetArrivalDate_Ljava_lang_Long_);
			return cb_setArrivalDate_Ljava_lang_Long_;
		}

		static void n_SetArrivalDate_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ArrivalDate = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getArrivalDate;
		static IntPtr id_setArrivalDate_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long ArrivalDate {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='getArrivalDate' and count(parameter)=0]"
			[Register ("getArrivalDate", "()Ljava/lang/Long;", "GetGetArrivalDateHandler")]
			get {
				if (id_getArrivalDate == IntPtr.Zero)
					id_getArrivalDate = JNIEnv.GetMethodID (class_ref, "getArrivalDate", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getArrivalDate), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getArrivalDate", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='setArrivalDate' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setArrivalDate", "(Ljava/lang/Long;)V", "GetSetArrivalDate_Ljava_lang_Long_Handler")]
			set {
				if (id_setArrivalDate_Ljava_lang_Long_ == IntPtr.Zero)
					id_setArrivalDate_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setArrivalDate", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setArrivalDate_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setArrivalDate", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getArrivalRSSI;
#pragma warning disable 0169
		static Delegate GetGetArrivalRSSIHandler ()
		{
			if (cb_getArrivalRSSI == null)
				cb_getArrivalRSSI = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetArrivalRSSI);
			return cb_getArrivalRSSI;
		}

		static IntPtr n_GetArrivalRSSI (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ArrivalRSSI);
		}
#pragma warning restore 0169

		static Delegate cb_setArrivalRSSI_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetArrivalRSSI_Ljava_lang_Integer_Handler ()
		{
			if (cb_setArrivalRSSI_Ljava_lang_Integer_ == null)
				cb_setArrivalRSSI_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetArrivalRSSI_Ljava_lang_Integer_);
			return cb_setArrivalRSSI_Ljava_lang_Integer_;
		}

		static void n_SetArrivalRSSI_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ArrivalRSSI = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getArrivalRSSI;
		static IntPtr id_setArrivalRSSI_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer ArrivalRSSI {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='getArrivalRSSI' and count(parameter)=0]"
			[Register ("getArrivalRSSI", "()Ljava/lang/Integer;", "GetGetArrivalRSSIHandler")]
			get {
				if (id_getArrivalRSSI == IntPtr.Zero)
					id_getArrivalRSSI = JNIEnv.GetMethodID (class_ref, "getArrivalRSSI", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getArrivalRSSI), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getArrivalRSSI", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='setArrivalRSSI' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setArrivalRSSI", "(Ljava/lang/Integer;)V", "GetSetArrivalRSSI_Ljava_lang_Integer_Handler")]
			set {
				if (id_setArrivalRSSI_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setArrivalRSSI_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setArrivalRSSI", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setArrivalRSSI_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setArrivalRSSI", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getBeaconFenceIdentifier;
#pragma warning disable 0169
		static Delegate GetGetBeaconFenceIdentifierHandler ()
		{
			if (cb_getBeaconFenceIdentifier == null)
				cb_getBeaconFenceIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBeaconFenceIdentifier);
			return cb_getBeaconFenceIdentifier;
		}

		static IntPtr n_GetBeaconFenceIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.BeaconFenceIdentifier);
		}
#pragma warning restore 0169

		static Delegate cb_setBeaconFenceIdentifier_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetBeaconFenceIdentifier_Ljava_lang_String_Handler ()
		{
			if (cb_setBeaconFenceIdentifier_Ljava_lang_String_ == null)
				cb_setBeaconFenceIdentifier_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetBeaconFenceIdentifier_Ljava_lang_String_);
			return cb_setBeaconFenceIdentifier_Ljava_lang_String_;
		}

		static void n_SetBeaconFenceIdentifier_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.BeaconFenceIdentifier = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getBeaconFenceIdentifier;
		static IntPtr id_setBeaconFenceIdentifier_Ljava_lang_String_;
		public virtual unsafe string BeaconFenceIdentifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='getBeaconFenceIdentifier' and count(parameter)=0]"
			[Register ("getBeaconFenceIdentifier", "()Ljava/lang/String;", "GetGetBeaconFenceIdentifierHandler")]
			get {
				if (id_getBeaconFenceIdentifier == IntPtr.Zero)
					id_getBeaconFenceIdentifier = JNIEnv.GetMethodID (class_ref, "getBeaconFenceIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getBeaconFenceIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBeaconFenceIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='setBeaconFenceIdentifier' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setBeaconFenceIdentifier", "(Ljava/lang/String;)V", "GetSetBeaconFenceIdentifier_Ljava_lang_String_Handler")]
			set {
				if (id_setBeaconFenceIdentifier_Ljava_lang_String_ == IntPtr.Zero)
					id_setBeaconFenceIdentifier_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setBeaconFenceIdentifier", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setBeaconFenceIdentifier_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setBeaconFenceIdentifier", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getDateOfLastSighting;
#pragma warning disable 0169
		static Delegate GetGetDateOfLastSightingHandler ()
		{
			if (cb_getDateOfLastSighting == null)
				cb_getDateOfLastSighting = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDateOfLastSighting);
			return cb_getDateOfLastSighting;
		}

		static IntPtr n_GetDateOfLastSighting (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DateOfLastSighting);
		}
#pragma warning restore 0169

		static Delegate cb_setDateOfLastSighting_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetDateOfLastSighting_Ljava_lang_Long_Handler ()
		{
			if (cb_setDateOfLastSighting_Ljava_lang_Long_ == null)
				cb_setDateOfLastSighting_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDateOfLastSighting_Ljava_lang_Long_);
			return cb_setDateOfLastSighting_Ljava_lang_Long_;
		}

		static void n_SetDateOfLastSighting_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DateOfLastSighting = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDateOfLastSighting;
		static IntPtr id_setDateOfLastSighting_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long DateOfLastSighting {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='getDateOfLastSighting' and count(parameter)=0]"
			[Register ("getDateOfLastSighting", "()Ljava/lang/Long;", "GetGetDateOfLastSightingHandler")]
			get {
				if (id_getDateOfLastSighting == IntPtr.Zero)
					id_getDateOfLastSighting = JNIEnv.GetMethodID (class_ref, "getDateOfLastSighting", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getDateOfLastSighting), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDateOfLastSighting", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='setDateOfLastSighting' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setDateOfLastSighting", "(Ljava/lang/Long;)V", "GetSetDateOfLastSighting_Ljava_lang_Long_Handler")]
			set {
				if (id_setDateOfLastSighting_Ljava_lang_Long_ == IntPtr.Zero)
					id_setDateOfLastSighting_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setDateOfLastSighting", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDateOfLastSighting_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDateOfLastSighting", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureDate;
#pragma warning disable 0169
		static Delegate GetGetDepartureDateHandler ()
		{
			if (cb_getDepartureDate == null)
				cb_getDepartureDate = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDepartureDate);
			return cb_getDepartureDate;
		}

		static IntPtr n_GetDepartureDate (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DepartureDate);
		}
#pragma warning restore 0169

		static Delegate cb_setDepartureDate_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetDepartureDate_Ljava_lang_Long_Handler ()
		{
			if (cb_setDepartureDate_Ljava_lang_Long_ == null)
				cb_setDepartureDate_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDepartureDate_Ljava_lang_Long_);
			return cb_setDepartureDate_Ljava_lang_Long_;
		}

		static void n_SetDepartureDate_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DepartureDate = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureDate;
		static IntPtr id_setDepartureDate_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long DepartureDate {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='getDepartureDate' and count(parameter)=0]"
			[Register ("getDepartureDate", "()Ljava/lang/Long;", "GetGetDepartureDateHandler")]
			get {
				if (id_getDepartureDate == IntPtr.Zero)
					id_getDepartureDate = JNIEnv.GetMethodID (class_ref, "getDepartureDate", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getDepartureDate), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureDate", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='setDepartureDate' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setDepartureDate", "(Ljava/lang/Long;)V", "GetSetDepartureDate_Ljava_lang_Long_Handler")]
			set {
				if (id_setDepartureDate_Ljava_lang_Long_ == IntPtr.Zero)
					id_setDepartureDate_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setDepartureDate", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDepartureDate_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDepartureDate", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureIntervalInBackground;
#pragma warning disable 0169
		static Delegate GetGetDepartureIntervalInBackgroundHandler ()
		{
			if (cb_getDepartureIntervalInBackground == null)
				cb_getDepartureIntervalInBackground = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDepartureIntervalInBackground);
			return cb_getDepartureIntervalInBackground;
		}

		static IntPtr n_GetDepartureIntervalInBackground (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DepartureIntervalInBackground);
		}
#pragma warning restore 0169

		static Delegate cb_setDepartureIntervalInBackground_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetDepartureIntervalInBackground_Ljava_lang_Long_Handler ()
		{
			if (cb_setDepartureIntervalInBackground_Ljava_lang_Long_ == null)
				cb_setDepartureIntervalInBackground_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDepartureIntervalInBackground_Ljava_lang_Long_);
			return cb_setDepartureIntervalInBackground_Ljava_lang_Long_;
		}

		static void n_SetDepartureIntervalInBackground_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DepartureIntervalInBackground = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureIntervalInBackground;
		static IntPtr id_setDepartureIntervalInBackground_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long DepartureIntervalInBackground {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='getDepartureIntervalInBackground' and count(parameter)=0]"
			[Register ("getDepartureIntervalInBackground", "()Ljava/lang/Long;", "GetGetDepartureIntervalInBackgroundHandler")]
			get {
				if (id_getDepartureIntervalInBackground == IntPtr.Zero)
					id_getDepartureIntervalInBackground = JNIEnv.GetMethodID (class_ref, "getDepartureIntervalInBackground", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getDepartureIntervalInBackground), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureIntervalInBackground", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='setDepartureIntervalInBackground' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setDepartureIntervalInBackground", "(Ljava/lang/Long;)V", "GetSetDepartureIntervalInBackground_Ljava_lang_Long_Handler")]
			set {
				if (id_setDepartureIntervalInBackground_Ljava_lang_Long_ == IntPtr.Zero)
					id_setDepartureIntervalInBackground_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setDepartureIntervalInBackground", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDepartureIntervalInBackground_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDepartureIntervalInBackground", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureIntervalInForeground;
#pragma warning disable 0169
		static Delegate GetGetDepartureIntervalInForegroundHandler ()
		{
			if (cb_getDepartureIntervalInForeground == null)
				cb_getDepartureIntervalInForeground = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDepartureIntervalInForeground);
			return cb_getDepartureIntervalInForeground;
		}

		static IntPtr n_GetDepartureIntervalInForeground (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DepartureIntervalInForeground);
		}
#pragma warning restore 0169

		static Delegate cb_setDepartureIntervalInForeground_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetDepartureIntervalInForeground_Ljava_lang_Long_Handler ()
		{
			if (cb_setDepartureIntervalInForeground_Ljava_lang_Long_ == null)
				cb_setDepartureIntervalInForeground_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDepartureIntervalInForeground_Ljava_lang_Long_);
			return cb_setDepartureIntervalInForeground_Ljava_lang_Long_;
		}

		static void n_SetDepartureIntervalInForeground_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DepartureIntervalInForeground = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureIntervalInForeground;
		static IntPtr id_setDepartureIntervalInForeground_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long DepartureIntervalInForeground {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='getDepartureIntervalInForeground' and count(parameter)=0]"
			[Register ("getDepartureIntervalInForeground", "()Ljava/lang/Long;", "GetGetDepartureIntervalInForegroundHandler")]
			get {
				if (id_getDepartureIntervalInForeground == IntPtr.Zero)
					id_getDepartureIntervalInForeground = JNIEnv.GetMethodID (class_ref, "getDepartureIntervalInForeground", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getDepartureIntervalInForeground), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureIntervalInForeground", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='setDepartureIntervalInForeground' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setDepartureIntervalInForeground", "(Ljava/lang/Long;)V", "GetSetDepartureIntervalInForeground_Ljava_lang_Long_Handler")]
			set {
				if (id_setDepartureIntervalInForeground_Ljava_lang_Long_ == IntPtr.Zero)
					id_setDepartureIntervalInForeground_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setDepartureIntervalInForeground", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDepartureIntervalInForeground_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDepartureIntervalInForeground", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureRSSI;
#pragma warning disable 0169
		static Delegate GetGetDepartureRSSIHandler ()
		{
			if (cb_getDepartureRSSI == null)
				cb_getDepartureRSSI = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDepartureRSSI);
			return cb_getDepartureRSSI;
		}

		static IntPtr n_GetDepartureRSSI (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DepartureRSSI);
		}
#pragma warning restore 0169

		static Delegate cb_setDepartureRSSI_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetDepartureRSSI_Ljava_lang_Integer_Handler ()
		{
			if (cb_setDepartureRSSI_Ljava_lang_Integer_ == null)
				cb_setDepartureRSSI_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDepartureRSSI_Ljava_lang_Integer_);
			return cb_setDepartureRSSI_Ljava_lang_Integer_;
		}

		static void n_SetDepartureRSSI_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DepartureRSSI = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureRSSI;
		static IntPtr id_setDepartureRSSI_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer DepartureRSSI {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='getDepartureRSSI' and count(parameter)=0]"
			[Register ("getDepartureRSSI", "()Ljava/lang/Integer;", "GetGetDepartureRSSIHandler")]
			get {
				if (id_getDepartureRSSI == IntPtr.Zero)
					id_getDepartureRSSI = JNIEnv.GetMethodID (class_ref, "getDepartureRSSI", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getDepartureRSSI), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureRSSI", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='setDepartureRSSI' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setDepartureRSSI", "(Ljava/lang/Integer;)V", "GetSetDepartureRSSI_Ljava_lang_Integer_Handler")]
			set {
				if (id_setDepartureRSSI_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setDepartureRSSI_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setDepartureRSSI", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDepartureRSSI_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDepartureRSSI", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getLongitude;
#pragma warning disable 0169
		static Delegate GetGetLongitudeHandler ()
		{
			if (cb_getLongitude == null)
				cb_getLongitude = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_GetLongitude);
			return cb_getLongitude;
		}

		static double n_GetLongitude (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Longitude;
		}
#pragma warning restore 0169

		static Delegate cb_setLongitude_D;
#pragma warning disable 0169
		static Delegate GetSetLongitude_DHandler ()
		{
			if (cb_setLongitude_D == null)
				cb_setLongitude_D = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, double>) n_SetLongitude_D);
			return cb_setLongitude_D;
		}

		static void n_SetLongitude_D (IntPtr jnienv, IntPtr native__this, double p0)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Longitude = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLongitude;
		static IntPtr id_setLongitude_D;
		public virtual unsafe double Longitude {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='getLongitude' and count(parameter)=0]"
			[Register ("getLongitude", "()D", "GetGetLongitudeHandler")]
			get {
				if (id_getLongitude == IntPtr.Zero)
					id_getLongitude = JNIEnv.GetMethodID (class_ref, "getLongitude", "()D");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallDoubleMethod  (Handle, id_getLongitude);
					else
						return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLongitude", "()D"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='setLongitude' and count(parameter)=1 and parameter[1][@type='double']]"
			[Register ("setLongitude", "(D)V", "GetSetLongitude_DHandler")]
			set {
				if (id_setLongitude_D == IntPtr.Zero)
					id_setLongitude_D = JNIEnv.GetMethodID (class_ref, "setLongitude", "(D)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLongitude_D, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLongitude", "(D)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPlaceId;
#pragma warning disable 0169
		static Delegate GetGetPlaceIdHandler ()
		{
			if (cb_getPlaceId == null)
				cb_getPlaceId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlaceId);
			return cb_getPlaceId;
		}

		static IntPtr n_GetPlaceId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.PlaceId);
		}
#pragma warning restore 0169

		static Delegate cb_setPlaceId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetPlaceId_Ljava_lang_Long_Handler ()
		{
			if (cb_setPlaceId_Ljava_lang_Long_ == null)
				cb_setPlaceId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPlaceId_Ljava_lang_Long_);
			return cb_setPlaceId_Ljava_lang_Long_;
		}

		static void n_SetPlaceId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.PlaceId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPlaceId;
		static IntPtr id_setPlaceId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long PlaceId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='getPlaceId' and count(parameter)=0]"
			[Register ("getPlaceId", "()Ljava/lang/Long;", "GetGetPlaceIdHandler")]
			get {
				if (id_getPlaceId == IntPtr.Zero)
					id_getPlaceId = JNIEnv.GetMethodID (class_ref, "getPlaceId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getPlaceId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlaceId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='setPlaceId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setPlaceId", "(Ljava/lang/Long;)V", "GetSetPlaceId_Ljava_lang_Long_Handler")]
			set {
				if (id_setPlaceId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setPlaceId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setPlaceId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPlaceId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPlaceId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getTransmitterIdentifier;
#pragma warning disable 0169
		static Delegate GetGetTransmitterIdentifierHandler ()
		{
			if (cb_getTransmitterIdentifier == null)
				cb_getTransmitterIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTransmitterIdentifier);
			return cb_getTransmitterIdentifier;
		}

		static IntPtr n_GetTransmitterIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.TransmitterIdentifier);
		}
#pragma warning restore 0169

		static Delegate cb_setTransmitterIdentifier_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetTransmitterIdentifier_Ljava_lang_String_Handler ()
		{
			if (cb_setTransmitterIdentifier_Ljava_lang_String_ == null)
				cb_setTransmitterIdentifier_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTransmitterIdentifier_Ljava_lang_String_);
			return cb_setTransmitterIdentifier_Ljava_lang_String_;
		}

		static void n_SetTransmitterIdentifier_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.TransmitterIdentifier = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTransmitterIdentifier;
		static IntPtr id_setTransmitterIdentifier_Ljava_lang_String_;
		public virtual unsafe string TransmitterIdentifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='getTransmitterIdentifier' and count(parameter)=0]"
			[Register ("getTransmitterIdentifier", "()Ljava/lang/String;", "GetGetTransmitterIdentifierHandler")]
			get {
				if (id_getTransmitterIdentifier == IntPtr.Zero)
					id_getTransmitterIdentifier = JNIEnv.GetMethodID (class_ref, "getTransmitterIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getTransmitterIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTransmitterIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='setTransmitterIdentifier' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setTransmitterIdentifier", "(Ljava/lang/String;)V", "GetSetTransmitterIdentifier_Ljava_lang_String_Handler")]
			set {
				if (id_setTransmitterIdentifier_Ljava_lang_String_ == IntPtr.Zero)
					id_setTransmitterIdentifier_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setTransmitterIdentifier", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTransmitterIdentifier_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTransmitterIdentifier", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_describeContents;
#pragma warning disable 0169
		static Delegate GetDescribeContentsHandler ()
		{
			if (cb_describeContents == null)
				cb_describeContents = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_DescribeContents);
			return cb_describeContents;
		}

		static int n_DescribeContents (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DescribeContents ();
		}
#pragma warning restore 0169

		static IntPtr id_describeContents;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='describeContents' and count(parameter)=0]"
		[Register ("describeContents", "()I", "GetDescribeContentsHandler")]
		public virtual unsafe int DescribeContents ()
		{
			if (id_describeContents == IntPtr.Zero)
				id_describeContents = JNIEnv.GetMethodID (class_ref, "describeContents", "()I");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallIntMethod  (Handle, id_describeContents);
				else
					return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "describeContents", "()I"));
			} finally {
			}
		}

		static Delegate cb_getlatitude;
#pragma warning disable 0169
		static Delegate GetGetlatitudeHandler ()
		{
			if (cb_getlatitude == null)
				cb_getlatitude = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_Getlatitude);
			return cb_getlatitude;
		}

		static double n_Getlatitude (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Getlatitude ();
		}
#pragma warning restore 0169

		static IntPtr id_getlatitude;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='getlatitude' and count(parameter)=0]"
		[Register ("getlatitude", "()D", "GetGetlatitudeHandler")]
		public virtual unsafe double Getlatitude ()
		{
			if (id_getlatitude == IntPtr.Zero)
				id_getlatitude = JNIEnv.GetMethodID (class_ref, "getlatitude", "()D");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallDoubleMethod  (Handle, id_getlatitude);
				else
					return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getlatitude", "()D"));
			} finally {
			}
		}

		static Delegate cb_setlatitude_D;
#pragma warning disable 0169
		static Delegate GetSetlatitude_DHandler ()
		{
			if (cb_setlatitude_D == null)
				cb_setlatitude_D = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, double>) n_Setlatitude_D);
			return cb_setlatitude_D;
		}

		static void n_Setlatitude_D (IntPtr jnienv, IntPtr native__this, double p0)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Setlatitude (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setlatitude_D;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='setlatitude' and count(parameter)=1 and parameter[1][@type='double']]"
		[Register ("setlatitude", "(D)V", "GetSetlatitude_DHandler")]
		public virtual unsafe void Setlatitude (double p0)
		{
			if (id_setlatitude_D == IntPtr.Zero)
				id_setlatitude_D = JNIEnv.GetMethodID (class_ref, "setlatitude", "(D)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setlatitude_D, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setlatitude", "(D)V"), __args);
			} finally {
			}
		}

		static Delegate cb_writeToParcel_Landroid_os_Parcel_I;
#pragma warning disable 0169
		static Delegate GetWriteToParcel_Landroid_os_Parcel_IHandler ()
		{
			if (cb_writeToParcel_Landroid_os_Parcel_I == null)
				cb_writeToParcel_Landroid_os_Parcel_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_WriteToParcel_Landroid_os_Parcel_I);
			return cb_writeToParcel_Landroid_os_Parcel_I;
		}

		static void n_WriteToParcel_Landroid_os_Parcel_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int native_p1)
		{
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Parcel p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Parcel> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.ParcelableWriteFlags p1 = (global::Android.OS.ParcelableWriteFlags) native_p1;
			__this.WriteToParcel (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_writeToParcel_Landroid_os_Parcel_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='InternalBeaconFenceVisit']/method[@name='writeToParcel' and count(parameter)=2 and parameter[1][@type='android.os.Parcel'] and parameter[2][@type='int']]"
		[Register ("writeToParcel", "(Landroid/os/Parcel;I)V", "GetWriteToParcel_Landroid_os_Parcel_IHandler")]
		public virtual unsafe void WriteToParcel (global::Android.OS.Parcel p0, [global::Android.Runtime.GeneratedEnum] global::Android.OS.ParcelableWriteFlags p1)
		{
			if (id_writeToParcel_Landroid_os_Parcel_I == IntPtr.Zero)
				id_writeToParcel_Landroid_os_Parcel_I = JNIEnv.GetMethodID (class_ref, "writeToParcel", "(Landroid/os/Parcel;I)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue ((int) p1);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_writeToParcel_Landroid_os_Parcel_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeToParcel", "(Landroid/os/Parcel;I)V"), __args);
			} finally {
			}
		}

	}
}
