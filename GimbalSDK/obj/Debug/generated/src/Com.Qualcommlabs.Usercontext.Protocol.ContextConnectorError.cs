using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qualcommlabs.Usercontext.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']"
	[global::Android.Runtime.Register ("com/qualcommlabs/usercontext/protocol/ContextConnectorError", DoNotGenerateAcw=true)]
	public sealed partial class ContextConnectorError : global::Java.Lang.Enum {


		static IntPtr APPLICATION_DID_NOT_INITIATE_DOWNLOAD_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='APPLICATION_DID_NOT_INITIATE_DOWNLOAD']"
		[Register ("APPLICATION_DID_NOT_INITIATE_DOWNLOAD")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError ApplicationDidNotInitiateDownload {
			get {
				if (APPLICATION_DID_NOT_INITIATE_DOWNLOAD_jfieldId == IntPtr.Zero)
					APPLICATION_DID_NOT_INITIATE_DOWNLOAD_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "APPLICATION_DID_NOT_INITIATE_DOWNLOAD", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, APPLICATION_DID_NOT_INITIATE_DOWNLOAD_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr APP_NOT_ENABLED_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='APP_NOT_ENABLED']"
		[Register ("APP_NOT_ENABLED")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError AppNotEnabled {
			get {
				if (APP_NOT_ENABLED_jfieldId == IntPtr.Zero)
					APP_NOT_ENABLED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "APP_NOT_ENABLED", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, APP_NOT_ENABLED_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr AUTHENTICATION_FAILURE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='AUTHENTICATION_FAILURE']"
		[Register ("AUTHENTICATION_FAILURE")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError AuthenticationFailure {
			get {
				if (AUTHENTICATION_FAILURE_jfieldId == IntPtr.Zero)
					AUTHENTICATION_FAILURE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "AUTHENTICATION_FAILURE", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, AUTHENTICATION_FAILURE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr CONTEXT_CONNECTOR_SERVICE_NOT_READY_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='CONTEXT_CONNECTOR_SERVICE_NOT_READY']"
		[Register ("CONTEXT_CONNECTOR_SERVICE_NOT_READY")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError ContextConnectorServiceNotReady {
			get {
				if (CONTEXT_CONNECTOR_SERVICE_NOT_READY_jfieldId == IntPtr.Zero)
					CONTEXT_CONNECTOR_SERVICE_NOT_READY_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CONTEXT_CONNECTOR_SERVICE_NOT_READY", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CONTEXT_CONNECTOR_SERVICE_NOT_READY_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr DISABLED_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='DISABLED']"
		[Register ("DISABLED")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError Disabled {
			get {
				if (DISABLED_jfieldId == IntPtr.Zero)
					DISABLED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "DISABLED", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, DISABLED_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr ENABLED_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='ENABLED']"
		[Register ("ENABLED")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError Enabled {
			get {
				if (ENABLED_jfieldId == IntPtr.Zero)
					ENABLED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "ENABLED", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, ENABLED_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr ENABLE_REQUEST_CURRENTLY_IN_PROGRESS_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='ENABLE_REQUEST_CURRENTLY_IN_PROGRESS']"
		[Register ("ENABLE_REQUEST_CURRENTLY_IN_PROGRESS")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError EnableRequestCurrentlyInProgress {
			get {
				if (ENABLE_REQUEST_CURRENTLY_IN_PROGRESS_jfieldId == IntPtr.Zero)
					ENABLE_REQUEST_CURRENTLY_IN_PROGRESS_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "ENABLE_REQUEST_CURRENTLY_IN_PROGRESS", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, ENABLE_REQUEST_CURRENTLY_IN_PROGRESS_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr ENABLE_REQUEST_REQUEST_CANCELLED_BY_USER_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='ENABLE_REQUEST_REQUEST_CANCELLED_BY_USER']"
		[Register ("ENABLE_REQUEST_REQUEST_CANCELLED_BY_USER")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError EnableRequestRequestCancelledByUser {
			get {
				if (ENABLE_REQUEST_REQUEST_CANCELLED_BY_USER_jfieldId == IntPtr.Zero)
					ENABLE_REQUEST_REQUEST_CANCELLED_BY_USER_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "ENABLE_REQUEST_REQUEST_CANCELLED_BY_USER", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, ENABLE_REQUEST_REQUEST_CANCELLED_BY_USER_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr GIMBAL_NOT_ENABLED_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='GIMBAL_NOT_ENABLED']"
		[Register ("GIMBAL_NOT_ENABLED")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError GimbalNotEnabled {
			get {
				if (GIMBAL_NOT_ENABLED_jfieldId == IntPtr.Zero)
					GIMBAL_NOT_ENABLED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "GIMBAL_NOT_ENABLED", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, GIMBAL_NOT_ENABLED_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr INVALID_ARGUMENTS_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='INVALID_ARGUMENTS']"
		[Register ("INVALID_ARGUMENTS")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError InvalidArguments {
			get {
				if (INVALID_ARGUMENTS_jfieldId == IntPtr.Zero)
					INVALID_ARGUMENTS_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "INVALID_ARGUMENTS", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, INVALID_ARGUMENTS_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr INVALID_CONTENT_ID_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='INVALID_CONTENT_ID']"
		[Register ("INVALID_CONTENT_ID")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError InvalidContentId {
			get {
				if (INVALID_CONTENT_ID_jfieldId == IntPtr.Zero)
					INVALID_CONTENT_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "INVALID_CONTENT_ID", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, INVALID_CONTENT_ID_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr INVALID_PLACE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='INVALID_PLACE']"
		[Register ("INVALID_PLACE")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError InvalidPlace {
			get {
				if (INVALID_PLACE_jfieldId == IntPtr.Zero)
					INVALID_PLACE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "INVALID_PLACE", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, INVALID_PLACE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr INVALID_PLACE_ID_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='INVALID_PLACE_ID']"
		[Register ("INVALID_PLACE_ID")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError InvalidPlaceId {
			get {
				if (INVALID_PLACE_ID_jfieldId == IntPtr.Zero)
					INVALID_PLACE_ID_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "INVALID_PLACE_ID", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, INVALID_PLACE_ID_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr INVALID_REQUEST_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='INVALID_REQUEST']"
		[Register ("INVALID_REQUEST")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError InvalidRequest {
			get {
				if (INVALID_REQUEST_jfieldId == IntPtr.Zero)
					INVALID_REQUEST_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "INVALID_REQUEST", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, INVALID_REQUEST_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr NETWORK_ERROR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='NETWORK_ERROR']"
		[Register ("NETWORK_ERROR")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError NetworkError {
			get {
				if (NETWORK_ERROR_jfieldId == IntPtr.Zero)
					NETWORK_ERROR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "NETWORK_ERROR", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, NETWORK_ERROR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr NOT_REGISTERED_ERROR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='NOT_REGISTERED_ERROR']"
		[Register ("NOT_REGISTERED_ERROR")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError NotRegisteredError {
			get {
				if (NOT_REGISTERED_ERROR_jfieldId == IntPtr.Zero)
					NOT_REGISTERED_ERROR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "NOT_REGISTERED_ERROR", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, NOT_REGISTERED_ERROR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr NO_EVENTS_AVAILABLE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='NO_EVENTS_AVAILABLE']"
		[Register ("NO_EVENTS_AVAILABLE")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError NoEventsAvailable {
			get {
				if (NO_EVENTS_AVAILABLE_jfieldId == IntPtr.Zero)
					NO_EVENTS_AVAILABLE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "NO_EVENTS_AVAILABLE", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, NO_EVENTS_AVAILABLE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr NO_LOCATION_PERMISSION_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='NO_LOCATION_PERMISSION']"
		[Register ("NO_LOCATION_PERMISSION")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError NoLocationPermission {
			get {
				if (NO_LOCATION_PERMISSION_jfieldId == IntPtr.Zero)
					NO_LOCATION_PERMISSION_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "NO_LOCATION_PERMISSION", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, NO_LOCATION_PERMISSION_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr NO_PERMISSION_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='NO_PERMISSION']"
		[Register ("NO_PERMISSION")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError NoPermission {
			get {
				if (NO_PERMISSION_jfieldId == IntPtr.Zero)
					NO_PERMISSION_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "NO_PERMISSION", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, NO_PERMISSION_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr NO_PROFILE_PERMISSION_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='NO_PROFILE_PERMISSION']"
		[Register ("NO_PROFILE_PERMISSION")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError NoProfilePermission {
			get {
				if (NO_PROFILE_PERMISSION_jfieldId == IntPtr.Zero)
					NO_PROFILE_PERMISSION_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "NO_PROFILE_PERMISSION", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, NO_PROFILE_PERMISSION_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROFILE_NOT_GENERATED_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='PROFILE_NOT_GENERATED']"
		[Register ("PROFILE_NOT_GENERATED")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError ProfileNotGenerated {
			get {
				if (PROFILE_NOT_GENERATED_jfieldId == IntPtr.Zero)
					PROFILE_NOT_GENERATED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROFILE_NOT_GENERATED", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROFILE_NOT_GENERATED_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr SERVER_ERROR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='SERVER_ERROR']"
		[Register ("SERVER_ERROR")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError ServerError {
			get {
				if (SERVER_ERROR_jfieldId == IntPtr.Zero)
					SERVER_ERROR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "SERVER_ERROR", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, SERVER_ERROR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr TOO_FEW_POLYGON_POINTS_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='TOO_FEW_POLYGON_POINTS']"
		[Register ("TOO_FEW_POLYGON_POINTS")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError TooFewPolygonPoints {
			get {
				if (TOO_FEW_POLYGON_POINTS_jfieldId == IntPtr.Zero)
					TOO_FEW_POLYGON_POINTS_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "TOO_FEW_POLYGON_POINTS", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, TOO_FEW_POLYGON_POINTS_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr UNEXPECTED_CONTEXT_CONNECTOR_ERROR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='UNEXPECTED_CONTEXT_CONNECTOR_ERROR']"
		[Register ("UNEXPECTED_CONTEXT_CONNECTOR_ERROR")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError UnexpectedContextConnectorError {
			get {
				if (UNEXPECTED_CONTEXT_CONNECTOR_ERROR_jfieldId == IntPtr.Zero)
					UNEXPECTED_CONTEXT_CONNECTOR_ERROR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "UNEXPECTED_CONTEXT_CONNECTOR_ERROR", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, UNEXPECTED_CONTEXT_CONNECTOR_ERROR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr UNKNOWN_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='UNKNOWN']"
		[Register ("UNKNOWN")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError Unknown {
			get {
				if (UNKNOWN_jfieldId == IntPtr.Zero)
					UNKNOWN_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "UNKNOWN", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, UNKNOWN_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr UNKNOWN_SERVICE_REQUEST_TYPE_ERROR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/field[@name='UNKNOWN_SERVICE_REQUEST_TYPE_ERROR']"
		[Register ("UNKNOWN_SERVICE_REQUEST_TYPE_ERROR")]
		public static global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError UnknownServiceRequestTypeError {
			get {
				if (UNKNOWN_SERVICE_REQUEST_TYPE_ERROR_jfieldId == IntPtr.Zero)
					UNKNOWN_SERVICE_REQUEST_TYPE_ERROR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "UNKNOWN_SERVICE_REQUEST_TYPE_ERROR", "Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, UNKNOWN_SERVICE_REQUEST_TYPE_ERROR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qualcommlabs/usercontext/protocol/ContextConnectorError", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ContextConnectorError); }
		}

		internal ContextConnectorError (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_getErrorCode;
		public unsafe int ErrorCode {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/method[@name='getErrorCode' and count(parameter)=0]"
			[Register ("getErrorCode", "()I", "GetGetErrorCodeHandler")]
			get {
				if (id_getErrorCode == IntPtr.Zero)
					id_getErrorCode = JNIEnv.GetMethodID (class_ref, "getErrorCode", "()I");
				try {
					return JNIEnv.CallIntMethod  (Handle, id_getErrorCode);
				} finally {
				}
			}
		}

		static IntPtr id_getErrorMessage;
		public unsafe string ErrorMessage {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/method[@name='getErrorMessage' and count(parameter)=0]"
			[Register ("getErrorMessage", "()Ljava/lang/String;", "GetGetErrorMessageHandler")]
			get {
				if (id_getErrorMessage == IntPtr.Zero)
					id_getErrorMessage = JNIEnv.GetMethodID (class_ref, "getErrorMessage", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getErrorMessage), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_valueOf_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("valueOf", "(Ljava/lang/String;)Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;", "")]
		public static unsafe global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError ValueOf (string p0)
		{
			if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
				id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError __ret = global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_values;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='ContextConnectorError']/method[@name='values' and count(parameter)=0]"
		[Register ("values", "()[Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;", "")]
		public static unsafe global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError[] Values ()
		{
			if (id_values == IntPtr.Zero)
				id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qualcommlabs/usercontext/protocol/ContextConnectorError;");
			try {
				return (global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qualcommlabs.Usercontext.Protocol.ContextConnectorError));
			} finally {
			}
		}

	}
}
