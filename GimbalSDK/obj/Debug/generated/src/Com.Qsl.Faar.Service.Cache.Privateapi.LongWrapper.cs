using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Service.Cache.Privateapi {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.service.cache.privateapi']/class[@name='LongWrapper']"
	[global::Android.Runtime.Register ("com/qsl/faar/service/cache/privateapi/LongWrapper", DoNotGenerateAcw=true)]
	public partial class LongWrapper : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/service/cache/privateapi/LongWrapper", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (LongWrapper); }
		}

		protected LongWrapper (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.service.cache.privateapi']/class[@name='LongWrapper']/constructor[@name='LongWrapper' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe LongWrapper ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (LongWrapper)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getLongValue;
#pragma warning disable 0169
		static Delegate GetGetLongValueHandler ()
		{
			if (cb_getLongValue == null)
				cb_getLongValue = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLongValue);
			return cb_getLongValue;
		}

		static IntPtr n_GetLongValue (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Service.Cache.Privateapi.LongWrapper __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.Cache.Privateapi.LongWrapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.LongValue);
		}
#pragma warning restore 0169

		static Delegate cb_setLongValue_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetLongValue_Ljava_lang_Long_Handler ()
		{
			if (cb_setLongValue_Ljava_lang_Long_ == null)
				cb_setLongValue_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLongValue_Ljava_lang_Long_);
			return cb_setLongValue_Ljava_lang_Long_;
		}

		static void n_SetLongValue_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Service.Cache.Privateapi.LongWrapper __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.Cache.Privateapi.LongWrapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.LongValue = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLongValue;
		static IntPtr id_setLongValue_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long LongValue {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service.cache.privateapi']/class[@name='LongWrapper']/method[@name='getLongValue' and count(parameter)=0]"
			[Register ("getLongValue", "()Ljava/lang/Long;", "GetGetLongValueHandler")]
			get {
				if (id_getLongValue == IntPtr.Zero)
					id_getLongValue = JNIEnv.GetMethodID (class_ref, "getLongValue", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getLongValue), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLongValue", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service.cache.privateapi']/class[@name='LongWrapper']/method[@name='setLongValue' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setLongValue", "(Ljava/lang/Long;)V", "GetSetLongValue_Ljava_lang_Long_Handler")]
			set {
				if (id_setLongValue_Ljava_lang_Long_ == IntPtr.Zero)
					id_setLongValue_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setLongValue", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLongValue_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLongValue", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

	}
}
