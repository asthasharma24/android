using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Content {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptorHistory']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/content/ContentDescriptorHistory", DoNotGenerateAcw=true)]
	public partial class ContentDescriptorHistory : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/content/ContentDescriptorHistory", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ContentDescriptorHistory); }
		}

		protected ContentDescriptorHistory (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptorHistory']/constructor[@name='ContentDescriptorHistory' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ContentDescriptorHistory ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ContentDescriptorHistory)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getContentDescriptor;
#pragma warning disable 0169
		static Delegate GetGetContentDescriptorHandler ()
		{
			if (cb_getContentDescriptor == null)
				cb_getContentDescriptor = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetContentDescriptor);
			return cb_getContentDescriptor;
		}

		static IntPtr n_GetContentDescriptor (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptorHistory __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptorHistory> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ContentDescriptor);
		}
#pragma warning restore 0169

		static Delegate cb_setContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_;
#pragma warning disable 0169
		static Delegate GetSetContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_Handler ()
		{
			if (cb_setContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_ == null)
				cb_setContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_);
			return cb_setContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_;
		}

		static void n_SetContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptorHistory __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptorHistory> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ContentDescriptor = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getContentDescriptor;
		static IntPtr id_setContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_;
		public virtual unsafe global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor ContentDescriptor {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptorHistory']/method[@name='getContentDescriptor' and count(parameter)=0]"
			[Register ("getContentDescriptor", "()Lcom/qsl/faar/protocol/content/ContentDescriptor;", "GetGetContentDescriptorHandler")]
			get {
				if (id_getContentDescriptor == IntPtr.Zero)
					id_getContentDescriptor = JNIEnv.GetMethodID (class_ref, "getContentDescriptor", "()Lcom/qsl/faar/protocol/content/ContentDescriptor;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (JNIEnv.CallObjectMethod  (Handle, id_getContentDescriptor), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getContentDescriptor", "()Lcom/qsl/faar/protocol/content/ContentDescriptor;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptorHistory']/method[@name='setContentDescriptor' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.content.ContentDescriptor']]"
			[Register ("setContentDescriptor", "(Lcom/qsl/faar/protocol/content/ContentDescriptor;)V", "GetSetContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_Handler")]
			set {
				if (id_setContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_ == IntPtr.Zero)
					id_setContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_ = JNIEnv.GetMethodID (class_ref, "setContentDescriptor", "(Lcom/qsl/faar/protocol/content/ContentDescriptor;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setContentDescriptor", "(Lcom/qsl/faar/protocol/content/ContentDescriptor;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDeliveredToUserCount;
#pragma warning disable 0169
		static Delegate GetGetDeliveredToUserCountHandler ()
		{
			if (cb_getDeliveredToUserCount == null)
				cb_getDeliveredToUserCount = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDeliveredToUserCount);
			return cb_getDeliveredToUserCount;
		}

		static IntPtr n_GetDeliveredToUserCount (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptorHistory __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptorHistory> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DeliveredToUserCount);
		}
#pragma warning restore 0169

		static Delegate cb_setDeliveredToUserCount_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetDeliveredToUserCount_Ljava_lang_Integer_Handler ()
		{
			if (cb_setDeliveredToUserCount_Ljava_lang_Integer_ == null)
				cb_setDeliveredToUserCount_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDeliveredToUserCount_Ljava_lang_Integer_);
			return cb_setDeliveredToUserCount_Ljava_lang_Integer_;
		}

		static void n_SetDeliveredToUserCount_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptorHistory __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptorHistory> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DeliveredToUserCount = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDeliveredToUserCount;
		static IntPtr id_setDeliveredToUserCount_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer DeliveredToUserCount {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptorHistory']/method[@name='getDeliveredToUserCount' and count(parameter)=0]"
			[Register ("getDeliveredToUserCount", "()Ljava/lang/Integer;", "GetGetDeliveredToUserCountHandler")]
			get {
				if (id_getDeliveredToUserCount == IntPtr.Zero)
					id_getDeliveredToUserCount = JNIEnv.GetMethodID (class_ref, "getDeliveredToUserCount", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getDeliveredToUserCount), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDeliveredToUserCount", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptorHistory']/method[@name='setDeliveredToUserCount' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setDeliveredToUserCount", "(Ljava/lang/Integer;)V", "GetSetDeliveredToUserCount_Ljava_lang_Integer_Handler")]
			set {
				if (id_setDeliveredToUserCount_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setDeliveredToUserCount_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setDeliveredToUserCount", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDeliveredToUserCount_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDeliveredToUserCount", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getLastDeliveryTime;
#pragma warning disable 0169
		static Delegate GetGetLastDeliveryTimeHandler ()
		{
			if (cb_getLastDeliveryTime == null)
				cb_getLastDeliveryTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLastDeliveryTime);
			return cb_getLastDeliveryTime;
		}

		static IntPtr n_GetLastDeliveryTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptorHistory __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptorHistory> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.LastDeliveryTime);
		}
#pragma warning restore 0169

		static Delegate cb_setLastDeliveryTime_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetLastDeliveryTime_Ljava_lang_Long_Handler ()
		{
			if (cb_setLastDeliveryTime_Ljava_lang_Long_ == null)
				cb_setLastDeliveryTime_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLastDeliveryTime_Ljava_lang_Long_);
			return cb_setLastDeliveryTime_Ljava_lang_Long_;
		}

		static void n_SetLastDeliveryTime_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptorHistory __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptorHistory> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.LastDeliveryTime = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLastDeliveryTime;
		static IntPtr id_setLastDeliveryTime_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long LastDeliveryTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptorHistory']/method[@name='getLastDeliveryTime' and count(parameter)=0]"
			[Register ("getLastDeliveryTime", "()Ljava/lang/Long;", "GetGetLastDeliveryTimeHandler")]
			get {
				if (id_getLastDeliveryTime == IntPtr.Zero)
					id_getLastDeliveryTime = JNIEnv.GetMethodID (class_ref, "getLastDeliveryTime", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getLastDeliveryTime), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLastDeliveryTime", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptorHistory']/method[@name='setLastDeliveryTime' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setLastDeliveryTime", "(Ljava/lang/Long;)V", "GetSetLastDeliveryTime_Ljava_lang_Long_Handler")]
			set {
				if (id_setLastDeliveryTime_Ljava_lang_Long_ == IntPtr.Zero)
					id_setLastDeliveryTime_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setLastDeliveryTime", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLastDeliveryTime_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLastDeliveryTime", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

	}
}
