using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Location.Established {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']"
	[global::Android.Runtime.Register ("com/gimbal/location/established/Aggregation", DoNotGenerateAcw=true)]
	public abstract partial class Aggregation : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']/field[@name='ONE_DAY']"
		[Register ("ONE_DAY")]
		public const long OneDay = (long) 86400000L;
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/location/established/Aggregation", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Aggregation); }
		}

		protected Aggregation (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_J;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']/constructor[@name='Aggregation' and count(parameter)=1 and parameter[1][@type='long']]"
		[Register (".ctor", "(J)V", "")]
		protected unsafe Aggregation (long p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (Aggregation)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(J)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(J)V", __args);
					return;
				}

				if (id_ctor_J == IntPtr.Zero)
					id_ctor_J = JNIEnv.GetMethodID (class_ref, "<init>", "(J)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_J, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_J, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Lcom_gimbal_location_established_Aggregation_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']/constructor[@name='Aggregation' and count(parameter)=1 and parameter[1][@type='com.gimbal.location.established.Aggregation']]"
		[Register (".ctor", "(Lcom/gimbal/location/established/Aggregation;)V", "")]
		public unsafe Aggregation (global::Com.Gimbal.Location.Established.Aggregation p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (Aggregation)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Lcom/gimbal/location/established/Aggregation;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Lcom/gimbal/location/established/Aggregation;)V", __args);
					return;
				}

				if (id_ctor_Lcom_gimbal_location_established_Aggregation_ == IntPtr.Zero)
					id_ctor_Lcom_gimbal_location_established_Aggregation_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/gimbal/location/established/Aggregation;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_gimbal_location_established_Aggregation_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Lcom_gimbal_location_established_Aggregation_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']/constructor[@name='Aggregation' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Aggregation ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Aggregation)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getCreatedAt;
#pragma warning disable 0169
		static Delegate GetGetCreatedAtHandler ()
		{
			if (cb_getCreatedAt == null)
				cb_getCreatedAt = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetCreatedAt);
			return cb_getCreatedAt;
		}

		static long n_GetCreatedAt (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Aggregation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Aggregation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.CreatedAt;
		}
#pragma warning restore 0169

		static IntPtr id_getCreatedAt;
		public virtual unsafe long CreatedAt {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']/method[@name='getCreatedAt' and count(parameter)=0]"
			[Register ("getCreatedAt", "()J", "GetGetCreatedAtHandler")]
			get {
				if (id_getCreatedAt == IntPtr.Zero)
					id_getCreatedAt = JNIEnv.GetMethodID (class_ref, "getCreatedAt", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getCreatedAt);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCreatedAt", "()J"));
				} finally {
				}
			}
		}

		static Delegate cb_getDuration;
#pragma warning disable 0169
		static Delegate GetGetDurationHandler ()
		{
			if (cb_getDuration == null)
				cb_getDuration = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetDuration);
			return cb_getDuration;
		}

		static long n_GetDuration (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Aggregation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Aggregation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Duration;
		}
#pragma warning restore 0169

		static IntPtr id_getDuration;
		public virtual unsafe long Duration {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']/method[@name='getDuration' and count(parameter)=0]"
			[Register ("getDuration", "()J", "GetGetDurationHandler")]
			get {
				if (id_getDuration == IntPtr.Zero)
					id_getDuration = JNIEnv.GetMethodID (class_ref, "getDuration", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getDuration);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDuration", "()J"));
				} finally {
				}
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Aggregation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Aggregation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Id);
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		public virtual unsafe string Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/String;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_calcTimes;
#pragma warning disable 0169
		static Delegate GetCalcTimesHandler ()
		{
			if (cb_calcTimes == null)
				cb_calcTimes = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_CalcTimes);
			return cb_calcTimes;
		}

		static void n_CalcTimes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Aggregation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Aggregation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.CalcTimes ();
		}
#pragma warning restore 0169

		static IntPtr id_calcTimes;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']/method[@name='calcTimes' and count(parameter)=0]"
		[Register ("calcTimes", "()V", "GetCalcTimesHandler")]
		public virtual unsafe void CalcTimes ()
		{
			if (id_calcTimes == IntPtr.Zero)
				id_calcTimes = JNIEnv.GetMethodID (class_ref, "calcTimes", "()V");
			try {

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_calcTimes);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "calcTimes", "()V"));
			} finally {
			}
		}

		static Delegate cb_createId_Ljava_lang_Class_;
#pragma warning disable 0169
		static Delegate GetCreateId_Ljava_lang_Class_Handler ()
		{
			if (cb_createId_Ljava_lang_Class_ == null)
				cb_createId_Ljava_lang_Class_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_CreateId_Ljava_lang_Class_);
			return cb_createId_Ljava_lang_Class_;
		}

		static IntPtr n_CreateId_Ljava_lang_Class_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Location.Established.Aggregation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Aggregation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.CreateId (p0));
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']/method[@name='createId' and count(parameter)=1 and parameter[1][@type='java.lang.Class&lt;?&gt;']]"
		[Register ("createId", "(Ljava/lang/Class;)Ljava/lang/String;", "GetCreateId_Ljava_lang_Class_Handler")]
		protected abstract string CreateId (global::Java.Lang.Class p0);

		static Delegate cb_score;
#pragma warning disable 0169
		static Delegate GetScoreHandler ()
		{
			if (cb_score == null)
				cb_score = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_Score);
			return cb_score;
		}

		static double n_Score (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Aggregation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Aggregation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Score ();
		}
#pragma warning restore 0169

		static IntPtr id_score;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']/method[@name='score' and count(parameter)=0]"
		[Register ("score", "()D", "GetScoreHandler")]
		public virtual unsafe double Score ()
		{
			if (id_score == IntPtr.Zero)
				id_score = JNIEnv.GetMethodID (class_ref, "score", "()D");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallDoubleMethod  (Handle, id_score);
				else
					return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "score", "()D"));
			} finally {
			}
		}

		static Delegate cb_score_J;
#pragma warning disable 0169
		static Delegate GetScore_JHandler ()
		{
			if (cb_score_J == null)
				cb_score_J = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long, double>) n_Score_J);
			return cb_score_J;
		}

		static double n_Score_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Location.Established.Aggregation __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Aggregation> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Score (p0);
		}
#pragma warning restore 0169

		static IntPtr id_score_J;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']/method[@name='score' and count(parameter)=1 and parameter[1][@type='long']]"
		[Register ("score", "(J)D", "GetScore_JHandler")]
		public virtual unsafe double Score (long p0)
		{
			if (id_score_J == IntPtr.Zero)
				id_score_J = JNIEnv.GetMethodID (class_ref, "score", "(J)D");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					return JNIEnv.CallDoubleMethod  (Handle, id_score_J, __args);
				else
					return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "score", "(J)D"), __args);
			} finally {
			}
		}

	}

	[global::Android.Runtime.Register ("com/gimbal/location/established/Aggregation", DoNotGenerateAcw=true)]
	internal partial class AggregationInvoker : Aggregation {

		public AggregationInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (AggregationInvoker); }
		}

		static IntPtr id_createId_Ljava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Aggregation']/method[@name='createId' and count(parameter)=1 and parameter[1][@type='java.lang.Class&lt;?&gt;']]"
		[Register ("createId", "(Ljava/lang/Class;)Ljava/lang/String;", "GetCreateId_Ljava_lang_Class_Handler")]
		protected override unsafe string CreateId (global::Java.Lang.Class p0)
		{
			if (id_createId_Ljava_lang_Class_ == IntPtr.Zero)
				id_createId_Ljava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "createId", "(Ljava/lang/Class;)Ljava/lang/String;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				string __ret = JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_createId_Ljava_lang_Class_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

	}

}
