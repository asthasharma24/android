using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Profile {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='Profile']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/profile/Profile", DoNotGenerateAcw=true)]
	public partial class Profile : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/profile/Profile", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Profile); }
		}

		protected Profile (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='Profile']/constructor[@name='Profile' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Profile ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Profile)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAttributes;
#pragma warning disable 0169
		static Delegate GetGetAttributesHandler ()
		{
			if (cb_getAttributes == null)
				cb_getAttributes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAttributes);
			return cb_getAttributes;
		}

		static IntPtr n_GetAttributes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Profile.Profile __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.Profile> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaDictionary<string, global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute>.ToLocalJniHandle (__this.Attributes);
		}
#pragma warning restore 0169

		static Delegate cb_setAttributes_Ljava_util_Map_;
#pragma warning disable 0169
		static Delegate GetSetAttributes_Ljava_util_Map_Handler ()
		{
			if (cb_setAttributes_Ljava_util_Map_ == null)
				cb_setAttributes_Ljava_util_Map_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAttributes_Ljava_util_Map_);
			return cb_setAttributes_Ljava_util_Map_;
		}

		static void n_SetAttributes_Ljava_util_Map_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Profile.Profile __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.Profile> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaDictionary<string, global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Attributes = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getAttributes;
		static IntPtr id_setAttributes_Ljava_util_Map_;
		public virtual unsafe global::System.Collections.Generic.IDictionary<string, global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute> Attributes {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='Profile']/method[@name='getAttributes' and count(parameter)=0]"
			[Register ("getAttributes", "()Ljava/util/Map;", "GetGetAttributesHandler")]
			get {
				if (id_getAttributes == IntPtr.Zero)
					id_getAttributes = JNIEnv.GetMethodID (class_ref, "getAttributes", "()Ljava/util/Map;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaDictionary<string, global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getAttributes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaDictionary<string, global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAttributes", "()Ljava/util/Map;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='Profile']/method[@name='setAttributes' and count(parameter)=1 and parameter[1][@type='java.util.Map&lt;java.lang.String, com.qsl.faar.protocol.profile.ProfileAttribute&gt;']]"
			[Register ("setAttributes", "(Ljava/util/Map;)V", "GetSetAttributes_Ljava_util_Map_Handler")]
			set {
				if (id_setAttributes_Ljava_util_Map_ == IntPtr.Zero)
					id_setAttributes_Ljava_util_Map_ = JNIEnv.GetMethodID (class_ref, "setAttributes", "(Ljava/util/Map;)V");
				IntPtr native_value = global::Android.Runtime.JavaDictionary<string, global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setAttributes_Ljava_util_Map_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAttributes", "(Ljava/util/Map;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getCreationTime;
#pragma warning disable 0169
		static Delegate GetGetCreationTimeHandler ()
		{
			if (cb_getCreationTime == null)
				cb_getCreationTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetCreationTime);
			return cb_getCreationTime;
		}

		static long n_GetCreationTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Profile.Profile __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.Profile> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.CreationTime;
		}
#pragma warning restore 0169

		static Delegate cb_setCreationTime_J;
#pragma warning disable 0169
		static Delegate GetSetCreationTime_JHandler ()
		{
			if (cb_setCreationTime_J == null)
				cb_setCreationTime_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetCreationTime_J);
			return cb_setCreationTime_J;
		}

		static void n_SetCreationTime_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Qsl.Faar.Protocol.Profile.Profile __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.Profile> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.CreationTime = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getCreationTime;
		static IntPtr id_setCreationTime_J;
		public virtual unsafe long CreationTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='Profile']/method[@name='getCreationTime' and count(parameter)=0]"
			[Register ("getCreationTime", "()J", "GetGetCreationTimeHandler")]
			get {
				if (id_getCreationTime == IntPtr.Zero)
					id_getCreationTime = JNIEnv.GetMethodID (class_ref, "getCreationTime", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getCreationTime);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCreationTime", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='Profile']/method[@name='setCreationTime' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setCreationTime", "(J)V", "GetSetCreationTime_JHandler")]
			set {
				if (id_setCreationTime_J == IntPtr.Zero)
					id_setCreationTime_J = JNIEnv.GetMethodID (class_ref, "setCreationTime", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCreationTime_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCreationTime", "(J)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getCustomAttributes;
#pragma warning disable 0169
		static Delegate GetGetCustomAttributesHandler ()
		{
			if (cb_getCustomAttributes == null)
				cb_getCustomAttributes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCustomAttributes);
			return cb_getCustomAttributes;
		}

		static IntPtr n_GetCustomAttributes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Profile.Profile __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.Profile> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.CustomAttributes);
		}
#pragma warning restore 0169

		static Delegate cb_setCustomAttributes_Lcom_qsl_faar_protocol_profile_CustomAttributes_;
#pragma warning disable 0169
		static Delegate GetSetCustomAttributes_Lcom_qsl_faar_protocol_profile_CustomAttributes_Handler ()
		{
			if (cb_setCustomAttributes_Lcom_qsl_faar_protocol_profile_CustomAttributes_ == null)
				cb_setCustomAttributes_Lcom_qsl_faar_protocol_profile_CustomAttributes_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCustomAttributes_Lcom_qsl_faar_protocol_profile_CustomAttributes_);
			return cb_setCustomAttributes_Lcom_qsl_faar_protocol_profile_CustomAttributes_;
		}

		static void n_SetCustomAttributes_Lcom_qsl_faar_protocol_profile_CustomAttributes_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Profile.Profile __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.Profile> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.Profile.CustomAttributes p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.CustomAttributes> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.CustomAttributes = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getCustomAttributes;
		static IntPtr id_setCustomAttributes_Lcom_qsl_faar_protocol_profile_CustomAttributes_;
		public virtual unsafe global::Com.Qsl.Faar.Protocol.Profile.CustomAttributes CustomAttributes {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='Profile']/method[@name='getCustomAttributes' and count(parameter)=0]"
			[Register ("getCustomAttributes", "()Lcom/qsl/faar/protocol/profile/CustomAttributes;", "GetGetCustomAttributesHandler")]
			get {
				if (id_getCustomAttributes == IntPtr.Zero)
					id_getCustomAttributes = JNIEnv.GetMethodID (class_ref, "getCustomAttributes", "()Lcom/qsl/faar/protocol/profile/CustomAttributes;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.CustomAttributes> (JNIEnv.CallObjectMethod  (Handle, id_getCustomAttributes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.CustomAttributes> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCustomAttributes", "()Lcom/qsl/faar/protocol/profile/CustomAttributes;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='Profile']/method[@name='setCustomAttributes' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.profile.CustomAttributes']]"
			[Register ("setCustomAttributes", "(Lcom/qsl/faar/protocol/profile/CustomAttributes;)V", "GetSetCustomAttributes_Lcom_qsl_faar_protocol_profile_CustomAttributes_Handler")]
			set {
				if (id_setCustomAttributes_Lcom_qsl_faar_protocol_profile_CustomAttributes_ == IntPtr.Zero)
					id_setCustomAttributes_Lcom_qsl_faar_protocol_profile_CustomAttributes_ = JNIEnv.GetMethodID (class_ref, "setCustomAttributes", "(Lcom/qsl/faar/protocol/profile/CustomAttributes;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCustomAttributes_Lcom_qsl_faar_protocol_profile_CustomAttributes_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCustomAttributes", "(Lcom/qsl/faar/protocol/profile/CustomAttributes;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_addAttribute_Lcom_qsl_faar_protocol_profile_ProfileAttribute_;
#pragma warning disable 0169
		static Delegate GetAddAttribute_Lcom_qsl_faar_protocol_profile_ProfileAttribute_Handler ()
		{
			if (cb_addAttribute_Lcom_qsl_faar_protocol_profile_ProfileAttribute_ == null)
				cb_addAttribute_Lcom_qsl_faar_protocol_profile_ProfileAttribute_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddAttribute_Lcom_qsl_faar_protocol_profile_ProfileAttribute_);
			return cb_addAttribute_Lcom_qsl_faar_protocol_profile_ProfileAttribute_;
		}

		static void n_AddAttribute_Lcom_qsl_faar_protocol_profile_ProfileAttribute_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Profile.Profile __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.Profile> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddAttribute (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addAttribute_Lcom_qsl_faar_protocol_profile_ProfileAttribute_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='Profile']/method[@name='addAttribute' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.profile.ProfileAttribute']]"
		[Register ("addAttribute", "(Lcom/qsl/faar/protocol/profile/ProfileAttribute;)V", "GetAddAttribute_Lcom_qsl_faar_protocol_profile_ProfileAttribute_Handler")]
		public virtual unsafe void AddAttribute (global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute p0)
		{
			if (id_addAttribute_Lcom_qsl_faar_protocol_profile_ProfileAttribute_ == IntPtr.Zero)
				id_addAttribute_Lcom_qsl_faar_protocol_profile_ProfileAttribute_ = JNIEnv.GetMethodID (class_ref, "addAttribute", "(Lcom/qsl/faar/protocol/profile/ProfileAttribute;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addAttribute_Lcom_qsl_faar_protocol_profile_ProfileAttribute_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addAttribute", "(Lcom/qsl/faar/protocol/profile/ProfileAttribute;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_removeAttribute_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetRemoveAttribute_Ljava_lang_String_Handler ()
		{
			if (cb_removeAttribute_Ljava_lang_String_ == null)
				cb_removeAttribute_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RemoveAttribute_Ljava_lang_String_);
			return cb_removeAttribute_Ljava_lang_String_;
		}

		static void n_RemoveAttribute_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Profile.Profile __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.Profile> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveAttribute (p0);
		}
#pragma warning restore 0169

		static IntPtr id_removeAttribute_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='Profile']/method[@name='removeAttribute' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("removeAttribute", "(Ljava/lang/String;)V", "GetRemoveAttribute_Ljava_lang_String_Handler")]
		public virtual unsafe void RemoveAttribute (string p0)
		{
			if (id_removeAttribute_Ljava_lang_String_ == IntPtr.Zero)
				id_removeAttribute_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "removeAttribute", "(Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_removeAttribute_Ljava_lang_String_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "removeAttribute", "(Ljava/lang/String;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
