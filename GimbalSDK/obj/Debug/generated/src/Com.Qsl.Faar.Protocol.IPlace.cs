using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='Place']"
	[Register ("com/qsl/faar/protocol/Place", "", "Com.Qsl.Faar.Protocol.IPlaceInvoker")]
	public partial interface IPlace : global::Java.IO.ISerializable {

		global::Com.Qsl.Faar.Protocol.GeoFenceCircle GeoFenceCircle {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='Place']/method[@name='getGeoFenceCircle' and count(parameter)=0]"
			[Register ("getGeoFenceCircle", "()Lcom/qsl/faar/protocol/GeoFenceCircle;", "GetGetGeoFenceCircleHandler:Com.Qsl.Faar.Protocol.IPlaceInvoker, GimbalSDK")] get;
		}

		global::Com.Qsl.Faar.Protocol.GeoFencePolygon GeoFencePolygon {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='Place']/method[@name='getGeoFencePolygon' and count(parameter)=0]"
			[Register ("getGeoFencePolygon", "()Lcom/qsl/faar/protocol/GeoFencePolygon;", "GetGetGeoFencePolygonHandler:Com.Qsl.Faar.Protocol.IPlaceInvoker, GimbalSDK")] get;
		}

		global::Java.Lang.Long Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='Place']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/Long;", "GetGetIdHandler:Com.Qsl.Faar.Protocol.IPlaceInvoker, GimbalSDK")] get;
		}

		string Name {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='Place']/method[@name='getName' and count(parameter)=0]"
			[Register ("getName", "()Ljava/lang/String;", "GetGetNameHandler:Com.Qsl.Faar.Protocol.IPlaceInvoker, GimbalSDK")] get;
		}

		string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/interface[@name='Place']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler:Com.Qsl.Faar.Protocol.IPlaceInvoker, GimbalSDK")] get;
		}

	}

	[global::Android.Runtime.Register ("com/qsl/faar/protocol/Place", DoNotGenerateAcw=true)]
	internal class IPlaceInvoker : global::Java.Lang.Object, IPlace {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/qsl/faar/protocol/Place");
		IntPtr class_ref;

		public static IPlace GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IPlace> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.qsl.faar.protocol.Place"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IPlaceInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IPlaceInvoker); }
		}

		static Delegate cb_getGeoFenceCircle;
#pragma warning disable 0169
		static Delegate GetGetGeoFenceCircleHandler ()
		{
			if (cb_getGeoFenceCircle == null)
				cb_getGeoFenceCircle = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetGeoFenceCircle);
			return cb_getGeoFenceCircle;
		}

		static IntPtr n_GetGeoFenceCircle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.IPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.IPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GeoFenceCircle);
		}
#pragma warning restore 0169

		IntPtr id_getGeoFenceCircle;
		public unsafe global::Com.Qsl.Faar.Protocol.GeoFenceCircle GeoFenceCircle {
			get {
				if (id_getGeoFenceCircle == IntPtr.Zero)
					id_getGeoFenceCircle = JNIEnv.GetMethodID (class_ref, "getGeoFenceCircle", "()Lcom/qsl/faar/protocol/GeoFenceCircle;");
				return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFenceCircle> (JNIEnv.CallObjectMethod (Handle, id_getGeoFenceCircle), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getGeoFencePolygon;
#pragma warning disable 0169
		static Delegate GetGetGeoFencePolygonHandler ()
		{
			if (cb_getGeoFencePolygon == null)
				cb_getGeoFencePolygon = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetGeoFencePolygon);
			return cb_getGeoFencePolygon;
		}

		static IntPtr n_GetGeoFencePolygon (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.IPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.IPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GeoFencePolygon);
		}
#pragma warning restore 0169

		IntPtr id_getGeoFencePolygon;
		public unsafe global::Com.Qsl.Faar.Protocol.GeoFencePolygon GeoFencePolygon {
			get {
				if (id_getGeoFencePolygon == IntPtr.Zero)
					id_getGeoFencePolygon = JNIEnv.GetMethodID (class_ref, "getGeoFencePolygon", "()Lcom/qsl/faar/protocol/GeoFencePolygon;");
				return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (JNIEnv.CallObjectMethod (Handle, id_getGeoFencePolygon), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.IPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.IPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Id);
		}
#pragma warning restore 0169

		IntPtr id_getId;
		public unsafe global::Java.Lang.Long Id {
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/Long;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getName;
#pragma warning disable 0169
		static Delegate GetGetNameHandler ()
		{
			if (cb_getName == null)
				cb_getName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetName);
			return cb_getName;
		}

		static IntPtr n_GetName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.IPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.IPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Name);
		}
#pragma warning restore 0169

		IntPtr id_getName;
		public unsafe string Name {
			get {
				if (id_getName == IntPtr.Zero)
					id_getName = JNIEnv.GetMethodID (class_ref, "getName", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (Handle, id_getName), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.IPlace __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.IPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		IntPtr id_getUuid;
		public unsafe string Uuid {
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
			}
		}

	}

}
