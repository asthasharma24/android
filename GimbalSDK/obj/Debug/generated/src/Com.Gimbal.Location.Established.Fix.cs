using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Location.Established {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Fix']"
	[global::Android.Runtime.Register ("com/gimbal/location/established/Fix", DoNotGenerateAcw=true)]
	public partial class Fix : global::Com.Gimbal.Location.Established.Point {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/location/established/Fix", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Fix); }
		}

		protected Fix (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_gimbal_location_established_Fix_Lcom_gimbal_location_established_Fix_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Fix']/constructor[@name='Fix' and count(parameter)=2 and parameter[1][@type='com.gimbal.location.established.Fix'] and parameter[2][@type='com.gimbal.location.established.Fix']]"
		[Register (".ctor", "(Lcom/gimbal/location/established/Fix;Lcom/gimbal/location/established/Fix;)V", "")]
		public unsafe Fix (global::Com.Gimbal.Location.Established.Fix p0, global::Com.Gimbal.Location.Established.Fix p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				if (GetType () != typeof (Fix)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Lcom/gimbal/location/established/Fix;Lcom/gimbal/location/established/Fix;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Lcom/gimbal/location/established/Fix;Lcom/gimbal/location/established/Fix;)V", __args);
					return;
				}

				if (id_ctor_Lcom_gimbal_location_established_Fix_Lcom_gimbal_location_established_Fix_ == IntPtr.Zero)
					id_ctor_Lcom_gimbal_location_established_Fix_Lcom_gimbal_location_established_Fix_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/gimbal/location/established/Fix;Lcom/gimbal/location/established/Fix;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_gimbal_location_established_Fix_Lcom_gimbal_location_established_Fix_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Lcom_gimbal_location_established_Fix_Lcom_gimbal_location_established_Fix_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_DDDJLjava_lang_String_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Fix']/constructor[@name='Fix' and count(parameter)=5 and parameter[1][@type='double'] and parameter[2][@type='double'] and parameter[3][@type='double'] and parameter[4][@type='long'] and parameter[5][@type='java.lang.String']]"
		[Register (".ctor", "(DDDJLjava/lang/String;)V", "")]
		public unsafe Fix (double p0, double p1, double p2, long p3, string p4)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			IntPtr native_p4 = JNIEnv.NewString (p4);
			try {
				JValue* __args = stackalloc JValue [5];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);
				__args [2] = new JValue (p2);
				__args [3] = new JValue (p3);
				__args [4] = new JValue (native_p4);
				if (GetType () != typeof (Fix)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(DDDJLjava/lang/String;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(DDDJLjava/lang/String;)V", __args);
					return;
				}

				if (id_ctor_DDDJLjava_lang_String_ == IntPtr.Zero)
					id_ctor_DDDJLjava_lang_String_ = JNIEnv.GetMethodID (class_ref, "<init>", "(DDDJLjava/lang/String;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_DDDJLjava_lang_String_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_DDDJLjava_lang_String_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p4);
			}
		}

		static Delegate cb_getAccuracy;
#pragma warning disable 0169
		static Delegate GetGetAccuracyHandler ()
		{
			if (cb_getAccuracy == null)
				cb_getAccuracy = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_GetAccuracy);
			return cb_getAccuracy;
		}

		static double n_GetAccuracy (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Fix __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Accuracy;
		}
#pragma warning restore 0169

		static IntPtr id_getAccuracy;
		public virtual unsafe double Accuracy {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Fix']/method[@name='getAccuracy' and count(parameter)=0]"
			[Register ("getAccuracy", "()D", "GetGetAccuracyHandler")]
			get {
				if (id_getAccuracy == IntPtr.Zero)
					id_getAccuracy = JNIEnv.GetMethodID (class_ref, "getAccuracy", "()D");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallDoubleMethod  (Handle, id_getAccuracy);
					else
						return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAccuracy", "()D"));
				} finally {
				}
			}
		}

		static Delegate cb_getLocalSecondsIntoWeek;
#pragma warning disable 0169
		static Delegate GetGetLocalSecondsIntoWeekHandler ()
		{
			if (cb_getLocalSecondsIntoWeek == null)
				cb_getLocalSecondsIntoWeek = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetLocalSecondsIntoWeek);
			return cb_getLocalSecondsIntoWeek;
		}

		static int n_GetLocalSecondsIntoWeek (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Fix __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.LocalSecondsIntoWeek;
		}
#pragma warning restore 0169

		static IntPtr id_getLocalSecondsIntoWeek;
		public virtual unsafe int LocalSecondsIntoWeek {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Fix']/method[@name='getLocalSecondsIntoWeek' and count(parameter)=0]"
			[Register ("getLocalSecondsIntoWeek", "()I", "GetGetLocalSecondsIntoWeekHandler")]
			get {
				if (id_getLocalSecondsIntoWeek == IntPtr.Zero)
					id_getLocalSecondsIntoWeek = JNIEnv.GetMethodID (class_ref, "getLocalSecondsIntoWeek", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getLocalSecondsIntoWeek);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLocalSecondsIntoWeek", "()I"));
				} finally {
				}
			}
		}

		static Delegate cb_getSpeed;
#pragma warning disable 0169
		static Delegate GetGetSpeedHandler ()
		{
			if (cb_getSpeed == null)
				cb_getSpeed = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_GetSpeed);
			return cb_getSpeed;
		}

		static double n_GetSpeed (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Fix __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Speed;
		}
#pragma warning restore 0169

		static Delegate cb_setSpeed_D;
#pragma warning disable 0169
		static Delegate GetSetSpeed_DHandler ()
		{
			if (cb_setSpeed_D == null)
				cb_setSpeed_D = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, double>) n_SetSpeed_D);
			return cb_setSpeed_D;
		}

		static void n_SetSpeed_D (IntPtr jnienv, IntPtr native__this, double p0)
		{
			global::Com.Gimbal.Location.Established.Fix __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Speed = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSpeed;
		static IntPtr id_setSpeed_D;
		public virtual unsafe double Speed {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Fix']/method[@name='getSpeed' and count(parameter)=0]"
			[Register ("getSpeed", "()D", "GetGetSpeedHandler")]
			get {
				if (id_getSpeed == IntPtr.Zero)
					id_getSpeed = JNIEnv.GetMethodID (class_ref, "getSpeed", "()D");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallDoubleMethod  (Handle, id_getSpeed);
					else
						return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSpeed", "()D"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Fix']/method[@name='setSpeed' and count(parameter)=1 and parameter[1][@type='double']]"
			[Register ("setSpeed", "(D)V", "GetSetSpeed_DHandler")]
			set {
				if (id_setSpeed_D == IntPtr.Zero)
					id_setSpeed_D = JNIEnv.GetMethodID (class_ref, "setSpeed", "(D)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSpeed_D, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSpeed", "(D)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getTime;
#pragma warning disable 0169
		static Delegate GetGetTimeHandler ()
		{
			if (cb_getTime == null)
				cb_getTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetTime);
			return cb_getTime;
		}

		static long n_GetTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Fix __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Time;
		}
#pragma warning restore 0169

		static IntPtr id_getTime;
		public virtual unsafe long Time {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Fix']/method[@name='getTime' and count(parameter)=0]"
			[Register ("getTime", "()J", "GetGetTimeHandler")]
			get {
				if (id_getTime == IntPtr.Zero)
					id_getTime = JNIEnv.GetMethodID (class_ref, "getTime", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getTime);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTime", "()J"));
				} finally {
				}
			}
		}

		static Delegate cb_getTimezone;
#pragma warning disable 0169
		static Delegate GetGetTimezoneHandler ()
		{
			if (cb_getTimezone == null)
				cb_getTimezone = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTimezone);
			return cb_getTimezone;
		}

		static IntPtr n_GetTimezone (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.Fix __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Timezone);
		}
#pragma warning restore 0169

		static IntPtr id_getTimezone;
		public virtual unsafe string Timezone {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Fix']/method[@name='getTimezone' and count(parameter)=0]"
			[Register ("getTimezone", "()Ljava/lang/String;", "GetGetTimezoneHandler")]
			get {
				if (id_getTimezone == IntPtr.Zero)
					id_getTimezone = JNIEnv.GetMethodID (class_ref, "getTimezone", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getTimezone), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTimezone", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_calcLocalSecondsIntoWeek_JLjava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Fix']/method[@name='calcLocalSecondsIntoWeek' and count(parameter)=2 and parameter[1][@type='long'] and parameter[2][@type='java.lang.String']]"
		[Register ("calcLocalSecondsIntoWeek", "(JLjava/lang/String;)I", "")]
		public static unsafe int CalcLocalSecondsIntoWeek (long p0, string p1)
		{
			if (id_calcLocalSecondsIntoWeek_JLjava_lang_String_ == IntPtr.Zero)
				id_calcLocalSecondsIntoWeek_JLjava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "calcLocalSecondsIntoWeek", "(JLjava/lang/String;)I");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				int __ret = JNIEnv.CallStaticIntMethod  (class_ref, id_calcLocalSecondsIntoWeek_JLjava_lang_String_, __args);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static Delegate cb_millisTo_Lcom_gimbal_location_established_Fix_;
#pragma warning disable 0169
		static Delegate GetMillisTo_Lcom_gimbal_location_established_Fix_Handler ()
		{
			if (cb_millisTo_Lcom_gimbal_location_established_Fix_ == null)
				cb_millisTo_Lcom_gimbal_location_established_Fix_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, long>) n_MillisTo_Lcom_gimbal_location_established_Fix_);
			return cb_millisTo_Lcom_gimbal_location_established_Fix_;
		}

		static long n_MillisTo_Lcom_gimbal_location_established_Fix_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Location.Established.Fix __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Location.Established.Fix p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (native_p0, JniHandleOwnership.DoNotTransfer);
			long __ret = __this.MillisTo (p0);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_millisTo_Lcom_gimbal_location_established_Fix_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='Fix']/method[@name='millisTo' and count(parameter)=1 and parameter[1][@type='com.gimbal.location.established.Fix']]"
		[Register ("millisTo", "(Lcom/gimbal/location/established/Fix;)J", "GetMillisTo_Lcom_gimbal_location_established_Fix_Handler")]
		public virtual unsafe long MillisTo (global::Com.Gimbal.Location.Established.Fix p0)
		{
			if (id_millisTo_Lcom_gimbal_location_established_Fix_ == IntPtr.Zero)
				id_millisTo_Lcom_gimbal_location_established_Fix_ = JNIEnv.GetMethodID (class_ref, "millisTo", "(Lcom/gimbal/location/established/Fix;)J");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				long __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.CallLongMethod  (Handle, id_millisTo_Lcom_gimbal_location_established_Fix_, __args);
				else
					__ret = JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "millisTo", "(Lcom/gimbal/location/established/Fix;)J"), __args);
				return __ret;
			} finally {
			}
		}

	}
}
