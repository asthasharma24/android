using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/GeoFencePolygon", DoNotGenerateAcw=true)]
	public partial class GeoFencePolygon : global::Java.Lang.Object, global::Java.IO.ISerializable {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/GeoFencePolygon", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (GeoFencePolygon); }
		}

		protected GeoFencePolygon (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']/constructor[@name='GeoFencePolygon' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe GeoFencePolygon ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (GeoFencePolygon)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.GeoFencePolygon __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_Long_Handler ()
		{
			if (cb_setId_Ljava_lang_Long_ == null)
				cb_setId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_Long_);
			return cb_setId_Ljava_lang_Long_;
		}

		static void n_SetId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.GeoFencePolygon __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/Long;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setId", "(Ljava/lang/Long;)V", "GetSetId_Ljava_lang_Long_Handler")]
			set {
				if (id_setId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getLocations;
#pragma warning disable 0169
		static Delegate GetGetLocationsHandler ()
		{
			if (cb_getLocations == null)
				cb_getLocations = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLocations);
			return cb_getLocations;
		}

		static IntPtr n_GetLocations (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.GeoFencePolygon __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Location>.ToLocalJniHandle (__this.Locations);
		}
#pragma warning restore 0169

		static Delegate cb_setLocations_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetLocations_Ljava_util_List_Handler ()
		{
			if (cb_setLocations_Ljava_util_List_ == null)
				cb_setLocations_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLocations_Ljava_util_List_);
			return cb_setLocations_Ljava_util_List_;
		}

		static void n_SetLocations_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.GeoFencePolygon __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Location>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Locations = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLocations;
		static IntPtr id_setLocations_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.Location> Locations {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']/method[@name='getLocations' and count(parameter)=0]"
			[Register ("getLocations", "()Ljava/util/List;", "GetGetLocationsHandler")]
			get {
				if (id_getLocations == IntPtr.Zero)
					id_getLocations = JNIEnv.GetMethodID (class_ref, "getLocations", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Location>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getLocations), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Location>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLocations", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']/method[@name='setLocations' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.qsl.faar.protocol.Location&gt;']]"
			[Register ("setLocations", "(Ljava/util/List;)V", "GetSetLocations_Ljava_util_List_Handler")]
			set {
				if (id_setLocations_Ljava_util_List_ == IntPtr.Zero)
					id_setLocations_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setLocations", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Location>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLocations_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLocations", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.GeoFencePolygon __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		static Delegate cb_setUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setUuid_Ljava_lang_String_ == null)
				cb_setUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUuid_Ljava_lang_String_);
			return cb_setUuid_Ljava_lang_String_;
		}

		static void n_SetUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.GeoFencePolygon __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Uuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUuid;
		static IntPtr id_setUuid_Ljava_lang_String_;
		public virtual unsafe string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler")]
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']/method[@name='setUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUuid", "(Ljava/lang/String;)V", "GetSetUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_addPoint_Lcom_qsl_faar_protocol_Location_;
#pragma warning disable 0169
		static Delegate GetAddPoint_Lcom_qsl_faar_protocol_Location_Handler ()
		{
			if (cb_addPoint_Lcom_qsl_faar_protocol_Location_ == null)
				cb_addPoint_Lcom_qsl_faar_protocol_Location_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddPoint_Lcom_qsl_faar_protocol_Location_);
			return cb_addPoint_Lcom_qsl_faar_protocol_Location_;
		}

		static void n_AddPoint_Lcom_qsl_faar_protocol_Location_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.GeoFencePolygon __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.Location p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Location> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddPoint (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addPoint_Lcom_qsl_faar_protocol_Location_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']/method[@name='addPoint' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.Location']]"
		[Register ("addPoint", "(Lcom/qsl/faar/protocol/Location;)V", "GetAddPoint_Lcom_qsl_faar_protocol_Location_Handler")]
		public virtual unsafe void AddPoint (global::Com.Qsl.Faar.Protocol.Location p0)
		{
			if (id_addPoint_Lcom_qsl_faar_protocol_Location_ == IntPtr.Zero)
				id_addPoint_Lcom_qsl_faar_protocol_Location_ = JNIEnv.GetMethodID (class_ref, "addPoint", "(Lcom/qsl/faar/protocol/Location;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addPoint_Lcom_qsl_faar_protocol_Location_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addPoint", "(Lcom/qsl/faar/protocol/Location;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_getLocationAtIndex_I;
#pragma warning disable 0169
		static Delegate GetGetLocationAtIndex_IHandler ()
		{
			if (cb_getLocationAtIndex_I == null)
				cb_getLocationAtIndex_I = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int, IntPtr>) n_GetLocationAtIndex_I);
			return cb_getLocationAtIndex_I;
		}

		static IntPtr n_GetLocationAtIndex_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Qsl.Faar.Protocol.GeoFencePolygon __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetLocationAtIndex (p0));
		}
#pragma warning restore 0169

		static IntPtr id_getLocationAtIndex_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']/method[@name='getLocationAtIndex' and count(parameter)=1 and parameter[1][@type='int']]"
		[Register ("getLocationAtIndex", "(I)Lcom/qsl/faar/protocol/Location;", "GetGetLocationAtIndex_IHandler")]
		public virtual unsafe global::Com.Qsl.Faar.Protocol.Location GetLocationAtIndex (int p0)
		{
			if (id_getLocationAtIndex_I == IntPtr.Zero)
				id_getLocationAtIndex_I = JNIEnv.GetMethodID (class_ref, "getLocationAtIndex", "(I)Lcom/qsl/faar/protocol/Location;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Location> (JNIEnv.CallObjectMethod  (Handle, id_getLocationAtIndex_I, __args), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Location> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLocationAtIndex", "(I)Lcom/qsl/faar/protocol/Location;"), __args), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_numberOfPoints;
#pragma warning disable 0169
		static Delegate GetNumberOfPointsHandler ()
		{
			if (cb_numberOfPoints == null)
				cb_numberOfPoints = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_NumberOfPoints);
			return cb_numberOfPoints;
		}

		static int n_NumberOfPoints (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.GeoFencePolygon __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.NumberOfPoints ();
		}
#pragma warning restore 0169

		static IntPtr id_numberOfPoints;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']/method[@name='numberOfPoints' and count(parameter)=0]"
		[Register ("numberOfPoints", "()I", "GetNumberOfPointsHandler")]
		public virtual unsafe int NumberOfPoints ()
		{
			if (id_numberOfPoints == IntPtr.Zero)
				id_numberOfPoints = JNIEnv.GetMethodID (class_ref, "numberOfPoints", "()I");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallIntMethod  (Handle, id_numberOfPoints);
				else
					return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "numberOfPoints", "()I"));
			} finally {
			}
		}

		static Delegate cb_retreiveLatitudes;
#pragma warning disable 0169
		static Delegate GetRetreiveLatitudesHandler ()
		{
			if (cb_retreiveLatitudes == null)
				cb_retreiveLatitudes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_RetreiveLatitudes);
			return cb_retreiveLatitudes;
		}

		static IntPtr n_RetreiveLatitudes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.GeoFencePolygon __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.RetreiveLatitudes ());
		}
#pragma warning restore 0169

		static IntPtr id_retreiveLatitudes;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']/method[@name='retreiveLatitudes' and count(parameter)=0]"
		[Register ("retreiveLatitudes", "()[D", "GetRetreiveLatitudesHandler")]
		public virtual unsafe double[] RetreiveLatitudes ()
		{
			if (id_retreiveLatitudes == IntPtr.Zero)
				id_retreiveLatitudes = JNIEnv.GetMethodID (class_ref, "retreiveLatitudes", "()[D");
			try {

				if (GetType () == ThresholdType)
					return (double[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod  (Handle, id_retreiveLatitudes), JniHandleOwnership.TransferLocalRef, typeof (double));
				else
					return (double[]) JNIEnv.GetArray (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "retreiveLatitudes", "()[D")), JniHandleOwnership.TransferLocalRef, typeof (double));
			} finally {
			}
		}

		static Delegate cb_retreiveLongitudes;
#pragma warning disable 0169
		static Delegate GetRetreiveLongitudesHandler ()
		{
			if (cb_retreiveLongitudes == null)
				cb_retreiveLongitudes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_RetreiveLongitudes);
			return cb_retreiveLongitudes;
		}

		static IntPtr n_RetreiveLongitudes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.GeoFencePolygon __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoFencePolygon> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.RetreiveLongitudes ());
		}
#pragma warning restore 0169

		static IntPtr id_retreiveLongitudes;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoFencePolygon']/method[@name='retreiveLongitudes' and count(parameter)=0]"
		[Register ("retreiveLongitudes", "()[D", "GetRetreiveLongitudesHandler")]
		public virtual unsafe double[] RetreiveLongitudes ()
		{
			if (id_retreiveLongitudes == IntPtr.Zero)
				id_retreiveLongitudes = JNIEnv.GetMethodID (class_ref, "retreiveLongitudes", "()[D");
			try {

				if (GetType () == ThresholdType)
					return (double[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod  (Handle, id_retreiveLongitudes), JniHandleOwnership.TransferLocalRef, typeof (double));
				else
					return (double[]) JNIEnv.GetArray (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "retreiveLongitudes", "()[D")), JniHandleOwnership.TransferLocalRef, typeof (double));
			} finally {
			}
		}

	}
}
