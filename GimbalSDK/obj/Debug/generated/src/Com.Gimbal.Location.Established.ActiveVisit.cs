using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Location.Established {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveVisit']"
	[global::Android.Runtime.Register ("com/gimbal/location/established/ActiveVisit", DoNotGenerateAcw=true)]
	public partial class ActiveVisit : global::Com.Gimbal.Location.Established.Visit {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/location/established/ActiveVisit", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ActiveVisit); }
		}

		protected ActiveVisit (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveVisit']/constructor[@name='ActiveVisit' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ActiveVisit ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ActiveVisit)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getFixes;
#pragma warning disable 0169
		static Delegate GetGetFixesHandler ()
		{
			if (cb_getFixes == null)
				cb_getFixes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetFixes);
			return cb_getFixes;
		}

		static IntPtr n_GetFixes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.ActiveVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ActiveVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.Fix>.ToLocalJniHandle (__this.Fixes);
		}
#pragma warning restore 0169

		static IntPtr id_getFixes;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Location.Established.Fix> Fixes {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveVisit']/method[@name='getFixes' and count(parameter)=0]"
			[Register ("getFixes", "()Ljava/util/List;", "GetGetFixesHandler")]
			get {
				if (id_getFixes == IntPtr.Zero)
					id_getFixes = JNIEnv.GetMethodID (class_ref, "getFixes", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.Fix>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getFixes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Location.Established.Fix>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getFixes", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_add_Lcom_gimbal_location_established_Fix_;
#pragma warning disable 0169
		static Delegate GetAdd_Lcom_gimbal_location_established_Fix_Handler ()
		{
			if (cb_add_Lcom_gimbal_location_established_Fix_ == null)
				cb_add_Lcom_gimbal_location_established_Fix_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_Add_Lcom_gimbal_location_established_Fix_);
			return cb_add_Lcom_gimbal_location_established_Fix_;
		}

		static void n_Add_Lcom_gimbal_location_established_Fix_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Location.Established.ActiveVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ActiveVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Location.Established.Fix p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Fix> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Add (p0);
		}
#pragma warning restore 0169

		static IntPtr id_add_Lcom_gimbal_location_established_Fix_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveVisit']/method[@name='add' and count(parameter)=1 and parameter[1][@type='com.gimbal.location.established.Fix']]"
		[Register ("add", "(Lcom/gimbal/location/established/Fix;)V", "GetAdd_Lcom_gimbal_location_established_Fix_Handler")]
		public virtual unsafe void Add (global::Com.Gimbal.Location.Established.Fix p0)
		{
			if (id_add_Lcom_gimbal_location_established_Fix_ == IntPtr.Zero)
				id_add_Lcom_gimbal_location_established_Fix_ = JNIEnv.GetMethodID (class_ref, "add", "(Lcom/gimbal/location/established/Fix;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_add_Lcom_gimbal_location_established_Fix_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "add", "(Lcom/gimbal/location/established/Fix;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_firstFix;
#pragma warning disable 0169
		static Delegate GetFirstFixHandler ()
		{
			if (cb_firstFix == null)
				cb_firstFix = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_FirstFix);
			return cb_firstFix;
		}

		static IntPtr n_FirstFix (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.ActiveVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ActiveVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.FirstFix ());
		}
#pragma warning restore 0169

		static IntPtr id_firstFix;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveVisit']/method[@name='firstFix' and count(parameter)=0]"
		[Register ("firstFix", "()Lcom/gimbal/location/established/Point;", "GetFirstFixHandler")]
		public virtual unsafe global::Com.Gimbal.Location.Established.Point FirstFix ()
		{
			if (id_firstFix == IntPtr.Zero)
				id_firstFix = JNIEnv.GetMethodID (class_ref, "firstFix", "()Lcom/gimbal/location/established/Point;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Point> (JNIEnv.CallObjectMethod  (Handle, id_firstFix), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.Point> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "firstFix", "()Lcom/gimbal/location/established/Point;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_size;
#pragma warning disable 0169
		static Delegate GetSizeHandler ()
		{
			if (cb_size == null)
				cb_size = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_Size);
			return cb_size;
		}

		static int n_Size (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Location.Established.ActiveVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Location.Established.ActiveVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Size ();
		}
#pragma warning restore 0169

		static IntPtr id_size;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.location.established']/class[@name='ActiveVisit']/method[@name='size' and count(parameter)=0]"
		[Register ("size", "()I", "GetSizeHandler")]
		public virtual unsafe int Size ()
		{
			if (id_size == IntPtr.Zero)
				id_size = JNIEnv.GetMethodID (class_ref, "size", "()I");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallIntMethod  (Handle, id_size);
				else
					return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "size", "()I"));
			} finally {
			}
		}

	}
}
