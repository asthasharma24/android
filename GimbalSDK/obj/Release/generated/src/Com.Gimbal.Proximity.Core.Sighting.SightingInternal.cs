using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Sighting {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/sighting/SightingInternal", DoNotGenerateAcw=true)]
	public partial class SightingInternal : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/sighting/SightingInternal", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (SightingInternal); }
		}

		protected SightingInternal (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/constructor[@name='SightingInternal' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe SightingInternal ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (SightingInternal)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAccuracy;
#pragma warning disable 0169
		static Delegate GetGetAccuracyHandler ()
		{
			if (cb_getAccuracy == null)
				cb_getAccuracy = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAccuracy);
			return cb_getAccuracy;
		}

		static IntPtr n_GetAccuracy (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Accuracy);
		}
#pragma warning restore 0169

		static Delegate cb_setAccuracy_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetAccuracy_Ljava_lang_String_Handler ()
		{
			if (cb_setAccuracy_Ljava_lang_String_ == null)
				cb_setAccuracy_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAccuracy_Ljava_lang_String_);
			return cb_setAccuracy_Ljava_lang_String_;
		}

		static void n_SetAccuracy_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Accuracy = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getAccuracy;
		static IntPtr id_setAccuracy_Ljava_lang_String_;
		public virtual unsafe string Accuracy {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getAccuracy' and count(parameter)=0]"
			[Register ("getAccuracy", "()Ljava/lang/String;", "GetGetAccuracyHandler")]
			get {
				if (id_getAccuracy == IntPtr.Zero)
					id_getAccuracy = JNIEnv.GetMethodID (class_ref, "getAccuracy", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getAccuracy), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAccuracy", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setAccuracy' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setAccuracy", "(Ljava/lang/String;)V", "GetSetAccuracy_Ljava_lang_String_Handler")]
			set {
				if (id_setAccuracy_Ljava_lang_String_ == IntPtr.Zero)
					id_setAccuracy_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setAccuracy", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setAccuracy_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAccuracy", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getBatteryLevel;
#pragma warning disable 0169
		static Delegate GetGetBatteryLevelHandler ()
		{
			if (cb_getBatteryLevel == null)
				cb_getBatteryLevel = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBatteryLevel);
			return cb_getBatteryLevel;
		}

		static IntPtr n_GetBatteryLevel (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.BatteryLevel);
		}
#pragma warning restore 0169

		static Delegate cb_setBatteryLevel_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetBatteryLevel_Ljava_lang_Integer_Handler ()
		{
			if (cb_setBatteryLevel_Ljava_lang_Integer_ == null)
				cb_setBatteryLevel_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetBatteryLevel_Ljava_lang_Integer_);
			return cb_setBatteryLevel_Ljava_lang_Integer_;
		}

		static void n_SetBatteryLevel_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.BatteryLevel = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getBatteryLevel;
		static IntPtr id_setBatteryLevel_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer BatteryLevel {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getBatteryLevel' and count(parameter)=0]"
			[Register ("getBatteryLevel", "()Ljava/lang/Integer;", "GetGetBatteryLevelHandler")]
			get {
				if (id_getBatteryLevel == IntPtr.Zero)
					id_getBatteryLevel = JNIEnv.GetMethodID (class_ref, "getBatteryLevel", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getBatteryLevel), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBatteryLevel", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setBatteryLevel' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setBatteryLevel", "(Ljava/lang/Integer;)V", "GetSetBatteryLevel_Ljava_lang_Integer_Handler")]
			set {
				if (id_setBatteryLevel_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setBatteryLevel_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setBatteryLevel", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setBatteryLevel_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setBatteryLevel", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDate;
#pragma warning disable 0169
		static Delegate GetGetDateHandler ()
		{
			if (cb_getDate == null)
				cb_getDate = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDate);
			return cb_getDate;
		}

		static IntPtr n_GetDate (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Date);
		}
#pragma warning restore 0169

		static Delegate cb_setDate_Ljava_util_Date_;
#pragma warning disable 0169
		static Delegate GetSetDate_Ljava_util_Date_Handler ()
		{
			if (cb_setDate_Ljava_util_Date_ == null)
				cb_setDate_Ljava_util_Date_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDate_Ljava_util_Date_);
			return cb_setDate_Ljava_util_Date_;
		}

		static void n_SetDate_Ljava_util_Date_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Util.Date p0 = global::Java.Lang.Object.GetObject<global::Java.Util.Date> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Date = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDate;
		static IntPtr id_setDate_Ljava_util_Date_;
		public virtual unsafe global::Java.Util.Date Date {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getDate' and count(parameter)=0]"
			[Register ("getDate", "()Ljava/util/Date;", "GetGetDateHandler")]
			get {
				if (id_getDate == IntPtr.Zero)
					id_getDate = JNIEnv.GetMethodID (class_ref, "getDate", "()Ljava/util/Date;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Util.Date> (JNIEnv.CallObjectMethod  (Handle, id_getDate), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Util.Date> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDate", "()Ljava/util/Date;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setDate' and count(parameter)=1 and parameter[1][@type='java.util.Date']]"
			[Register ("setDate", "(Ljava/util/Date;)V", "GetSetDate_Ljava_util_Date_Handler")]
			set {
				if (id_setDate_Ljava_util_Date_ == IntPtr.Zero)
					id_setDate_Ljava_util_Date_ = JNIEnv.GetMethodID (class_ref, "setDate", "(Ljava/util/Date;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDate_Ljava_util_Date_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDate", "(Ljava/util/Date;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getFix_time;
#pragma warning disable 0169
		static Delegate GetGetFix_timeHandler ()
		{
			if (cb_getFix_time == null)
				cb_getFix_time = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetFix_time);
			return cb_getFix_time;
		}

		static IntPtr n_GetFix_time (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Fix_time);
		}
#pragma warning restore 0169

		static Delegate cb_setFix_time_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetFix_time_Ljava_lang_String_Handler ()
		{
			if (cb_setFix_time_Ljava_lang_String_ == null)
				cb_setFix_time_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetFix_time_Ljava_lang_String_);
			return cb_setFix_time_Ljava_lang_String_;
		}

		static void n_SetFix_time_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Fix_time = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getFix_time;
		static IntPtr id_setFix_time_Ljava_lang_String_;
		public virtual unsafe string Fix_time {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getFix_time' and count(parameter)=0]"
			[Register ("getFix_time", "()Ljava/lang/String;", "GetGetFix_timeHandler")]
			get {
				if (id_getFix_time == IntPtr.Zero)
					id_getFix_time = JNIEnv.GetMethodID (class_ref, "getFix_time", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getFix_time), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getFix_time", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setFix_time' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setFix_time", "(Ljava/lang/String;)V", "GetSetFix_time_Ljava_lang_String_Handler")]
			set {
				if (id_setFix_time_Ljava_lang_String_ == IntPtr.Zero)
					id_setFix_time_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setFix_time", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setFix_time_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setFix_time", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getGen4MaskedData;
#pragma warning disable 0169
		static Delegate GetGetGen4MaskedDataHandler ()
		{
			if (cb_getGen4MaskedData == null)
				cb_getGen4MaskedData = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetGen4MaskedData);
			return cb_getGen4MaskedData;
		}

		static IntPtr n_GetGen4MaskedData (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Gen4MaskedData);
		}
#pragma warning restore 0169

		static Delegate cb_setGen4MaskedData_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetGen4MaskedData_Ljava_lang_String_Handler ()
		{
			if (cb_setGen4MaskedData_Ljava_lang_String_ == null)
				cb_setGen4MaskedData_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetGen4MaskedData_Ljava_lang_String_);
			return cb_setGen4MaskedData_Ljava_lang_String_;
		}

		static void n_SetGen4MaskedData_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Gen4MaskedData = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getGen4MaskedData;
		static IntPtr id_setGen4MaskedData_Ljava_lang_String_;
		public virtual unsafe string Gen4MaskedData {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getGen4MaskedData' and count(parameter)=0]"
			[Register ("getGen4MaskedData", "()Ljava/lang/String;", "GetGetGen4MaskedDataHandler")]
			get {
				if (id_getGen4MaskedData == IntPtr.Zero)
					id_getGen4MaskedData = JNIEnv.GetMethodID (class_ref, "getGen4MaskedData", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getGen4MaskedData), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getGen4MaskedData", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setGen4MaskedData' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setGen4MaskedData", "(Ljava/lang/String;)V", "GetSetGen4MaskedData_Ljava_lang_String_Handler")]
			set {
				if (id_setGen4MaskedData_Ljava_lang_String_ == IntPtr.Zero)
					id_setGen4MaskedData_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setGen4MaskedData", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setGen4MaskedData_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setGen4MaskedData", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getGen4PacketVersion;
#pragma warning disable 0169
		static Delegate GetGetGen4PacketVersionHandler ()
		{
			if (cb_getGen4PacketVersion == null)
				cb_getGen4PacketVersion = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetGen4PacketVersion);
			return cb_getGen4PacketVersion;
		}

		static IntPtr n_GetGen4PacketVersion (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Gen4PacketVersion);
		}
#pragma warning restore 0169

		static Delegate cb_setGen4PacketVersion_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetGen4PacketVersion_Ljava_lang_String_Handler ()
		{
			if (cb_setGen4PacketVersion_Ljava_lang_String_ == null)
				cb_setGen4PacketVersion_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetGen4PacketVersion_Ljava_lang_String_);
			return cb_setGen4PacketVersion_Ljava_lang_String_;
		}

		static void n_SetGen4PacketVersion_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Gen4PacketVersion = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getGen4PacketVersion;
		static IntPtr id_setGen4PacketVersion_Ljava_lang_String_;
		public virtual unsafe string Gen4PacketVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getGen4PacketVersion' and count(parameter)=0]"
			[Register ("getGen4PacketVersion", "()Ljava/lang/String;", "GetGetGen4PacketVersionHandler")]
			get {
				if (id_getGen4PacketVersion == IntPtr.Zero)
					id_getGen4PacketVersion = JNIEnv.GetMethodID (class_ref, "getGen4PacketVersion", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getGen4PacketVersion), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getGen4PacketVersion", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setGen4PacketVersion' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setGen4PacketVersion", "(Ljava/lang/String;)V", "GetSetGen4PacketVersion_Ljava_lang_String_Handler")]
			set {
				if (id_setGen4PacketVersion_Ljava_lang_String_ == IntPtr.Zero)
					id_setGen4PacketVersion_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setGen4PacketVersion", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setGen4PacketVersion_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setGen4PacketVersion", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getLatitude;
#pragma warning disable 0169
		static Delegate GetGetLatitudeHandler ()
		{
			if (cb_getLatitude == null)
				cb_getLatitude = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLatitude);
			return cb_getLatitude;
		}

		static IntPtr n_GetLatitude (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Latitude);
		}
#pragma warning restore 0169

		static Delegate cb_setLatitude_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetLatitude_Ljava_lang_String_Handler ()
		{
			if (cb_setLatitude_Ljava_lang_String_ == null)
				cb_setLatitude_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLatitude_Ljava_lang_String_);
			return cb_setLatitude_Ljava_lang_String_;
		}

		static void n_SetLatitude_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Latitude = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLatitude;
		static IntPtr id_setLatitude_Ljava_lang_String_;
		public virtual unsafe string Latitude {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getLatitude' and count(parameter)=0]"
			[Register ("getLatitude", "()Ljava/lang/String;", "GetGetLatitudeHandler")]
			get {
				if (id_getLatitude == IntPtr.Zero)
					id_getLatitude = JNIEnv.GetMethodID (class_ref, "getLatitude", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getLatitude), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLatitude", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setLatitude' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setLatitude", "(Ljava/lang/String;)V", "GetSetLatitude_Ljava_lang_String_Handler")]
			set {
				if (id_setLatitude_Ljava_lang_String_ == IntPtr.Zero)
					id_setLatitude_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setLatitude", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLatitude_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLatitude", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getLongitude;
#pragma warning disable 0169
		static Delegate GetGetLongitudeHandler ()
		{
			if (cb_getLongitude == null)
				cb_getLongitude = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLongitude);
			return cb_getLongitude;
		}

		static IntPtr n_GetLongitude (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Longitude);
		}
#pragma warning restore 0169

		static Delegate cb_setLongitude_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetLongitude_Ljava_lang_String_Handler ()
		{
			if (cb_setLongitude_Ljava_lang_String_ == null)
				cb_setLongitude_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLongitude_Ljava_lang_String_);
			return cb_setLongitude_Ljava_lang_String_;
		}

		static void n_SetLongitude_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Longitude = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLongitude;
		static IntPtr id_setLongitude_Ljava_lang_String_;
		public virtual unsafe string Longitude {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getLongitude' and count(parameter)=0]"
			[Register ("getLongitude", "()Ljava/lang/String;", "GetGetLongitudeHandler")]
			get {
				if (id_getLongitude == IntPtr.Zero)
					id_getLongitude = JNIEnv.GetMethodID (class_ref, "getLongitude", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getLongitude), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLongitude", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setLongitude' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setLongitude", "(Ljava/lang/String;)V", "GetSetLongitude_Ljava_lang_String_Handler")]
			set {
				if (id_setLongitude_Ljava_lang_String_ == IntPtr.Zero)
					id_setLongitude_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setLongitude", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLongitude_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLongitude", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getPacketFormat;
#pragma warning disable 0169
		static Delegate GetGetPacketFormatHandler ()
		{
			if (cb_getPacketFormat == null)
				cb_getPacketFormat = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPacketFormat);
			return cb_getPacketFormat;
		}

		static IntPtr n_GetPacketFormat (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.PacketFormat);
		}
#pragma warning restore 0169

		static Delegate cb_setPacketFormat_Ljava_lang_Byte_;
#pragma warning disable 0169
		static Delegate GetSetPacketFormat_Ljava_lang_Byte_Handler ()
		{
			if (cb_setPacketFormat_Ljava_lang_Byte_ == null)
				cb_setPacketFormat_Ljava_lang_Byte_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPacketFormat_Ljava_lang_Byte_);
			return cb_setPacketFormat_Ljava_lang_Byte_;
		}

		static void n_SetPacketFormat_Ljava_lang_Byte_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Byte p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Byte> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.PacketFormat = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPacketFormat;
		static IntPtr id_setPacketFormat_Ljava_lang_Byte_;
		public virtual unsafe global::Java.Lang.Byte PacketFormat {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getPacketFormat' and count(parameter)=0]"
			[Register ("getPacketFormat", "()Ljava/lang/Byte;", "GetGetPacketFormatHandler")]
			get {
				if (id_getPacketFormat == IntPtr.Zero)
					id_getPacketFormat = JNIEnv.GetMethodID (class_ref, "getPacketFormat", "()Ljava/lang/Byte;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Byte> (JNIEnv.CallObjectMethod  (Handle, id_getPacketFormat), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Byte> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPacketFormat", "()Ljava/lang/Byte;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setPacketFormat' and count(parameter)=1 and parameter[1][@type='java.lang.Byte']]"
			[Register ("setPacketFormat", "(Ljava/lang/Byte;)V", "GetSetPacketFormat_Ljava_lang_Byte_Handler")]
			set {
				if (id_setPacketFormat_Ljava_lang_Byte_ == IntPtr.Zero)
					id_setPacketFormat_Ljava_lang_Byte_ = JNIEnv.GetMethodID (class_ref, "setPacketFormat", "(Ljava/lang/Byte;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPacketFormat_Ljava_lang_Byte_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPacketFormat", "(Ljava/lang/Byte;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPayload;
#pragma warning disable 0169
		static Delegate GetGetPayloadHandler ()
		{
			if (cb_getPayload == null)
				cb_getPayload = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPayload);
			return cb_getPayload;
		}

		static IntPtr n_GetPayload (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Payload);
		}
#pragma warning restore 0169

		static Delegate cb_setPayload_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetPayload_Ljava_lang_String_Handler ()
		{
			if (cb_setPayload_Ljava_lang_String_ == null)
				cb_setPayload_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPayload_Ljava_lang_String_);
			return cb_setPayload_Ljava_lang_String_;
		}

		static void n_SetPayload_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Payload = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPayload;
		static IntPtr id_setPayload_Ljava_lang_String_;
		public virtual unsafe string Payload {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getPayload' and count(parameter)=0]"
			[Register ("getPayload", "()Ljava/lang/String;", "GetGetPayloadHandler")]
			get {
				if (id_getPayload == IntPtr.Zero)
					id_getPayload = JNIEnv.GetMethodID (class_ref, "getPayload", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getPayload), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPayload", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setPayload' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setPayload", "(Ljava/lang/String;)V", "GetSetPayload_Ljava_lang_String_Handler")]
			set {
				if (id_setPayload_Ljava_lang_String_ == IntPtr.Zero)
					id_setPayload_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setPayload", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPayload_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPayload", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getRssi;
#pragma warning disable 0169
		static Delegate GetGetRssiHandler ()
		{
			if (cb_getRssi == null)
				cb_getRssi = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetRssi);
			return cb_getRssi;
		}

		static int n_GetRssi (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Rssi;
		}
#pragma warning restore 0169

		static Delegate cb_setRssi_I;
#pragma warning disable 0169
		static Delegate GetSetRssi_IHandler ()
		{
			if (cb_setRssi_I == null)
				cb_setRssi_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetRssi_I);
			return cb_setRssi_I;
		}

		static void n_SetRssi_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Rssi = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getRssi;
		static IntPtr id_setRssi_I;
		public virtual unsafe int Rssi {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getRssi' and count(parameter)=0]"
			[Register ("getRssi", "()I", "GetGetRssiHandler")]
			get {
				if (id_getRssi == IntPtr.Zero)
					id_getRssi = JNIEnv.GetMethodID (class_ref, "getRssi", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getRssi);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRssi", "()I"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setRssi' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setRssi", "(I)V", "GetSetRssi_IHandler")]
			set {
				if (id_setRssi_I == IntPtr.Zero)
					id_setRssi_I = JNIEnv.GetMethodID (class_ref, "setRssi", "(I)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setRssi_I, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setRssi", "(I)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getSequenceNumber;
#pragma warning disable 0169
		static Delegate GetGetSequenceNumberHandler ()
		{
			if (cb_getSequenceNumber == null)
				cb_getSequenceNumber = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSequenceNumber);
			return cb_getSequenceNumber;
		}

		static IntPtr n_GetSequenceNumber (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.SequenceNumber);
		}
#pragma warning restore 0169

		static Delegate cb_setSequenceNumber_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetSequenceNumber_Ljava_lang_Long_Handler ()
		{
			if (cb_setSequenceNumber_Ljava_lang_Long_ == null)
				cb_setSequenceNumber_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSequenceNumber_Ljava_lang_Long_);
			return cb_setSequenceNumber_Ljava_lang_Long_;
		}

		static void n_SetSequenceNumber_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SequenceNumber = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSequenceNumber;
		static IntPtr id_setSequenceNumber_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long SequenceNumber {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getSequenceNumber' and count(parameter)=0]"
			[Register ("getSequenceNumber", "()Ljava/lang/Long;", "GetGetSequenceNumberHandler")]
			get {
				if (id_getSequenceNumber == IntPtr.Zero)
					id_getSequenceNumber = JNIEnv.GetMethodID (class_ref, "getSequenceNumber", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getSequenceNumber), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSequenceNumber", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setSequenceNumber' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setSequenceNumber", "(Ljava/lang/Long;)V", "GetSetSequenceNumber_Ljava_lang_Long_Handler")]
			set {
				if (id_setSequenceNumber_Ljava_lang_Long_ == IntPtr.Zero)
					id_setSequenceNumber_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setSequenceNumber", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSequenceNumber_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSequenceNumber", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getServiceId;
#pragma warning disable 0169
		static Delegate GetGetServiceIdHandler ()
		{
			if (cb_getServiceId == null)
				cb_getServiceId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetServiceId);
			return cb_getServiceId;
		}

		static IntPtr n_GetServiceId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ServiceId);
		}
#pragma warning restore 0169

		static Delegate cb_setServiceId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetServiceId_Ljava_lang_String_Handler ()
		{
			if (cb_setServiceId_Ljava_lang_String_ == null)
				cb_setServiceId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetServiceId_Ljava_lang_String_);
			return cb_setServiceId_Ljava_lang_String_;
		}

		static void n_SetServiceId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ServiceId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getServiceId;
		static IntPtr id_setServiceId_Ljava_lang_String_;
		public virtual unsafe string ServiceId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getServiceId' and count(parameter)=0]"
			[Register ("getServiceId", "()Ljava/lang/String;", "GetGetServiceIdHandler")]
			get {
				if (id_getServiceId == IntPtr.Zero)
					id_getServiceId = JNIEnv.GetMethodID (class_ref, "getServiceId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getServiceId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getServiceId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setServiceId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setServiceId", "(Ljava/lang/String;)V", "GetSetServiceId_Ljava_lang_String_Handler")]
			set {
				if (id_setServiceId_Ljava_lang_String_ == IntPtr.Zero)
					id_setServiceId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setServiceId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setServiceId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setServiceId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getTemperature;
#pragma warning disable 0169
		static Delegate GetGetTemperatureHandler ()
		{
			if (cb_getTemperature == null)
				cb_getTemperature = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTemperature);
			return cb_getTemperature;
		}

		static IntPtr n_GetTemperature (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Temperature);
		}
#pragma warning restore 0169

		static Delegate cb_setTemperature_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetTemperature_Ljava_lang_Integer_Handler ()
		{
			if (cb_setTemperature_Ljava_lang_Integer_ == null)
				cb_setTemperature_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTemperature_Ljava_lang_Integer_);
			return cb_setTemperature_Ljava_lang_Integer_;
		}

		static void n_SetTemperature_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Temperature = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTemperature;
		static IntPtr id_setTemperature_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer Temperature {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getTemperature' and count(parameter)=0]"
			[Register ("getTemperature", "()Ljava/lang/Integer;", "GetGetTemperatureHandler")]
			get {
				if (id_getTemperature == IntPtr.Zero)
					id_getTemperature = JNIEnv.GetMethodID (class_ref, "getTemperature", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getTemperature), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTemperature", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setTemperature' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setTemperature", "(Ljava/lang/Integer;)V", "GetSetTemperature_Ljava_lang_Integer_Handler")]
			set {
				if (id_setTemperature_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setTemperature_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setTemperature", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTemperature_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTemperature", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getTimezone;
#pragma warning disable 0169
		static Delegate GetGetTimezoneHandler ()
		{
			if (cb_getTimezone == null)
				cb_getTimezone = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTimezone);
			return cb_getTimezone;
		}

		static IntPtr n_GetTimezone (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Timezone);
		}
#pragma warning restore 0169

		static Delegate cb_setTimezone_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetTimezone_Ljava_lang_String_Handler ()
		{
			if (cb_setTimezone_Ljava_lang_String_ == null)
				cb_setTimezone_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTimezone_Ljava_lang_String_);
			return cb_setTimezone_Ljava_lang_String_;
		}

		static void n_SetTimezone_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Timezone = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTimezone;
		static IntPtr id_setTimezone_Ljava_lang_String_;
		public virtual unsafe string Timezone {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getTimezone' and count(parameter)=0]"
			[Register ("getTimezone", "()Ljava/lang/String;", "GetGetTimezoneHandler")]
			get {
				if (id_getTimezone == IntPtr.Zero)
					id_getTimezone = JNIEnv.GetMethodID (class_ref, "getTimezone", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getTimezone), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTimezone", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setTimezone' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setTimezone", "(Ljava/lang/String;)V", "GetSetTimezone_Ljava_lang_String_Handler")]
			set {
				if (id_setTimezone_Ljava_lang_String_ == IntPtr.Zero)
					id_setTimezone_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setTimezone", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTimezone_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTimezone", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getVersion;
#pragma warning disable 0169
		static Delegate GetGetVersionHandler ()
		{
			if (cb_getVersion == null)
				cb_getVersion = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetVersion);
			return cb_getVersion;
		}

		static IntPtr n_GetVersion (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Version);
		}
#pragma warning restore 0169

		static Delegate cb_setVersion_Ljava_lang_Byte_;
#pragma warning disable 0169
		static Delegate GetSetVersion_Ljava_lang_Byte_Handler ()
		{
			if (cb_setVersion_Ljava_lang_Byte_ == null)
				cb_setVersion_Ljava_lang_Byte_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetVersion_Ljava_lang_Byte_);
			return cb_setVersion_Ljava_lang_Byte_;
		}

		static void n_SetVersion_Ljava_lang_Byte_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Byte p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Byte> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Version = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getVersion;
		static IntPtr id_setVersion_Ljava_lang_Byte_;
		public virtual unsafe global::Java.Lang.Byte Version {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='getVersion' and count(parameter)=0]"
			[Register ("getVersion", "()Ljava/lang/Byte;", "GetGetVersionHandler")]
			get {
				if (id_getVersion == IntPtr.Zero)
					id_getVersion = JNIEnv.GetMethodID (class_ref, "getVersion", "()Ljava/lang/Byte;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Byte> (JNIEnv.CallObjectMethod  (Handle, id_getVersion), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Byte> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getVersion", "()Ljava/lang/Byte;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingInternal']/method[@name='setVersion' and count(parameter)=1 and parameter[1][@type='java.lang.Byte']]"
			[Register ("setVersion", "(Ljava/lang/Byte;)V", "GetSetVersion_Ljava_lang_Byte_Handler")]
			set {
				if (id_setVersion_Ljava_lang_Byte_ == IntPtr.Zero)
					id_setVersion_Ljava_lang_Byte_ = JNIEnv.GetMethodID (class_ref, "setVersion", "(Ljava/lang/Byte;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setVersion_Ljava_lang_Byte_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setVersion", "(Ljava/lang/Byte;)V"), __args);
				} finally {
				}
			}
		}

	}
}
