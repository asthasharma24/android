using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Content {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentFilter']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/content/ContentFilter", DoNotGenerateAcw=true)]
	public partial class ContentFilter : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/content/ContentFilter", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ContentFilter); }
		}

		protected ContentFilter (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentFilter']/constructor[@name='ContentFilter' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ContentFilter ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ContentFilter)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getSearchAttributes;
#pragma warning disable 0169
		static Delegate GetGetSearchAttributesHandler ()
		{
			if (cb_getSearchAttributes == null)
				cb_getSearchAttributes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSearchAttributes);
			return cb_getSearchAttributes;
		}

		static IntPtr n_GetSearchAttributes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentFilter __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentFilter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaDictionary<string, global::System.Collections.Generic.IList<global::Java.Lang.Object>>.ToLocalJniHandle (__this.SearchAttributes);
		}
#pragma warning restore 0169

		static Delegate cb_setSearchAttributes_Ljava_util_Map_;
#pragma warning disable 0169
		static Delegate GetSetSearchAttributes_Ljava_util_Map_Handler ()
		{
			if (cb_setSearchAttributes_Ljava_util_Map_ == null)
				cb_setSearchAttributes_Ljava_util_Map_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSearchAttributes_Ljava_util_Map_);
			return cb_setSearchAttributes_Ljava_util_Map_;
		}

		static void n_SetSearchAttributes_Ljava_util_Map_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentFilter __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentFilter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaDictionary<string, global::System.Collections.Generic.IList<global::Java.Lang.Object>>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SearchAttributes = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSearchAttributes;
		static IntPtr id_setSearchAttributes_Ljava_util_Map_;
		public virtual unsafe global::System.Collections.Generic.IDictionary<string, global::System.Collections.Generic.IList<global::Java.Lang.Object>> SearchAttributes {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentFilter']/method[@name='getSearchAttributes' and count(parameter)=0]"
			[Register ("getSearchAttributes", "()Ljava/util/Map;", "GetGetSearchAttributesHandler")]
			get {
				if (id_getSearchAttributes == IntPtr.Zero)
					id_getSearchAttributes = JNIEnv.GetMethodID (class_ref, "getSearchAttributes", "()Ljava/util/Map;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaDictionary<string, global::System.Collections.Generic.IList<global::Java.Lang.Object>>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getSearchAttributes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaDictionary<string, global::System.Collections.Generic.IList<global::Java.Lang.Object>>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSearchAttributes", "()Ljava/util/Map;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentFilter']/method[@name='setSearchAttributes' and count(parameter)=1 and parameter[1][@type='java.util.Map&lt;java.lang.String, java.util.List&lt;java.lang.Object&gt;&gt;']]"
			[Register ("setSearchAttributes", "(Ljava/util/Map;)V", "GetSetSearchAttributes_Ljava_util_Map_Handler")]
			set {
				if (id_setSearchAttributes_Ljava_util_Map_ == IntPtr.Zero)
					id_setSearchAttributes_Ljava_util_Map_ = JNIEnv.GetMethodID (class_ref, "setSearchAttributes", "(Ljava/util/Map;)V");
				IntPtr native_value = global::Android.Runtime.JavaDictionary<string, global::System.Collections.Generic.IList<global::Java.Lang.Object>>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSearchAttributes_Ljava_util_Map_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSearchAttributes", "(Ljava/util/Map;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_add_Ljava_lang_String_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetAdd_Ljava_lang_String_Ljava_lang_String_Handler ()
		{
			if (cb_add_Ljava_lang_String_Ljava_lang_String_ == null)
				cb_add_Ljava_lang_String_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_Add_Ljava_lang_String_Ljava_lang_String_);
			return cb_add_Ljava_lang_String_Ljava_lang_String_;
		}

		static void n_Add_Ljava_lang_String_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentFilter __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentFilter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.Add (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_add_Ljava_lang_String_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentFilter']/method[@name='add' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.String']]"
		[Register ("add", "(Ljava/lang/String;Ljava/lang/String;)V", "GetAdd_Ljava_lang_String_Ljava_lang_String_Handler")]
		public virtual unsafe void Add (string p0, string p1)
		{
			if (id_add_Ljava_lang_String_Ljava_lang_String_ == IntPtr.Zero)
				id_add_Ljava_lang_String_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "add", "(Ljava/lang/String;Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (native_p1);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_add_Ljava_lang_String_Ljava_lang_String_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "add", "(Ljava/lang/String;Ljava/lang/String;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static Delegate cb_getValues_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetGetValues_Ljava_lang_String_Handler ()
		{
			if (cb_getValues_Ljava_lang_String_ == null)
				cb_getValues_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_GetValues_Ljava_lang_String_);
			return cb_getValues_Ljava_lang_String_;
		}

		static IntPtr n_GetValues_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentFilter __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentFilter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = global::Android.Runtime.JavaList<global::Java.Lang.Object>.ToLocalJniHandle (__this.GetValues (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_getValues_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentFilter']/method[@name='getValues' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("getValues", "(Ljava/lang/String;)Ljava/util/List;", "GetGetValues_Ljava_lang_String_Handler")]
		public virtual unsafe global::System.Collections.Generic.IList<global::Java.Lang.Object> GetValues (string p0)
		{
			if (id_getValues_Ljava_lang_String_ == IntPtr.Zero)
				id_getValues_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "getValues", "(Ljava/lang/String;)Ljava/util/List;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				global::System.Collections.Generic.IList<global::Java.Lang.Object> __ret;
				if (GetType () == ThresholdType)
					__ret = global::Android.Runtime.JavaList<global::Java.Lang.Object>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getValues_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Android.Runtime.JavaList<global::Java.Lang.Object>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getValues", "(Ljava/lang/String;)Ljava/util/List;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
