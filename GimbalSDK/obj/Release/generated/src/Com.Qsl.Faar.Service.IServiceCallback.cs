using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Service {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.qsl.faar.service']/interface[@name='ServiceCallback']"
	[Register ("com/qsl/faar/service/ServiceCallback", "", "Com.Qsl.Faar.Service.IServiceCallbackInvoker")]
	[global::Java.Interop.JavaTypeParameters (new string [] {"T"})]
	public partial interface IServiceCallback : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service']/interface[@name='ServiceCallback']/method[@name='failure' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='java.lang.String']]"
		[Register ("failure", "(ILjava/lang/String;)V", "GetFailure_ILjava_lang_String_Handler:Com.Qsl.Faar.Service.IServiceCallbackInvoker, GimbalSDK")]
		void Failure (int p0, string p1);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service']/interface[@name='ServiceCallback']/method[@name='success' and count(parameter)=1 and parameter[1][@type='T']]"
		[Register ("success", "(Ljava/lang/Object;)V", "GetSuccess_Ljava_lang_Object_Handler:Com.Qsl.Faar.Service.IServiceCallbackInvoker, GimbalSDK")]
		void Success (global::Java.Lang.Object p0);

	}

	[global::Android.Runtime.Register ("com/qsl/faar/service/ServiceCallback", DoNotGenerateAcw=true)]
	internal class IServiceCallbackInvoker : global::Java.Lang.Object, IServiceCallback {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/qsl/faar/service/ServiceCallback");
		IntPtr class_ref;

		public static IServiceCallback GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IServiceCallback> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.qsl.faar.service.ServiceCallback"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IServiceCallbackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IServiceCallbackInvoker); }
		}

		static Delegate cb_failure_ILjava_lang_String_;
#pragma warning disable 0169
		static Delegate GetFailure_ILjava_lang_String_Handler ()
		{
			if (cb_failure_ILjava_lang_String_ == null)
				cb_failure_ILjava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, IntPtr>) n_Failure_ILjava_lang_String_);
			return cb_failure_ILjava_lang_String_;
		}

		static void n_Failure_ILjava_lang_String_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Com.Qsl.Faar.Service.IServiceCallback __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.IServiceCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.Failure (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_failure_ILjava_lang_String_;
		public unsafe void Failure (int p0, string p1)
		{
			if (id_failure_ILjava_lang_String_ == IntPtr.Zero)
				id_failure_ILjava_lang_String_ = JNIEnv.GetMethodID (class_ref, "failure", "(ILjava/lang/String;)V");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (native_p1);
			JNIEnv.CallVoidMethod (Handle, id_failure_ILjava_lang_String_, __args);
			JNIEnv.DeleteLocalRef (native_p1);
		}

		static Delegate cb_success_Ljava_lang_Object_;
#pragma warning disable 0169
		static Delegate GetSuccess_Ljava_lang_Object_Handler ()
		{
			if (cb_success_Ljava_lang_Object_ == null)
				cb_success_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_Success_Ljava_lang_Object_);
			return cb_success_Ljava_lang_Object_;
		}

		static void n_Success_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Service.IServiceCallback __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.IServiceCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Object p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Success (p0);
		}
#pragma warning restore 0169

		IntPtr id_success_Ljava_lang_Object_;
		public unsafe void Success (global::Java.Lang.Object p0)
		{
			if (id_success_Ljava_lang_Object_ == IntPtr.Zero)
				id_success_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "success", "(Ljava/lang/Object;)V");
			IntPtr native_p0 = JNIEnv.ToLocalJniHandle (p0);
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (native_p0);
			JNIEnv.CallVoidMethod (Handle, id_success_Ljava_lang_Object_, __args);
			JNIEnv.DeleteLocalRef (native_p0);
		}

	}

}
