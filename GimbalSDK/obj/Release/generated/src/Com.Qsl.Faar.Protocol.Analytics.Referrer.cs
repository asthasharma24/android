using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Analytics {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='Referrer']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/Referrer", DoNotGenerateAcw=true)]
	public partial class Referrer : global::Java.Lang.Object {

		// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='Referrer.Source']"
		[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/Referrer$Source", DoNotGenerateAcw=true)]
		public sealed partial class Source : global::Java.Lang.Enum {


			static IntPtr NOTIFICATION_MANAGER_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='Referrer.Source']/field[@name='NOTIFICATION_MANAGER']"
			[Register ("NOTIFICATION_MANAGER")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.Referrer.Source NotificationManager {
				get {
					if (NOTIFICATION_MANAGER_jfieldId == IntPtr.Zero)
						NOTIFICATION_MANAGER_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "NOTIFICATION_MANAGER", "Lcom/qsl/faar/protocol/analytics/Referrer$Source;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, NOTIFICATION_MANAGER_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.Referrer.Source> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr USER_CONTEXT_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='Referrer.Source']/field[@name='USER_CONTEXT']"
			[Register ("USER_CONTEXT")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.Referrer.Source UserContext {
				get {
					if (USER_CONTEXT_jfieldId == IntPtr.Zero)
						USER_CONTEXT_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "USER_CONTEXT", "Lcom/qsl/faar/protocol/analytics/Referrer$Source;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, USER_CONTEXT_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.Referrer.Source> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/Referrer$Source", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (Source); }
			}

			internal Source (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='Referrer.Source']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/Referrer$Source;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.Referrer.Source ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/Referrer$Source;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Qsl.Faar.Protocol.Analytics.Referrer.Source __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.Referrer.Source> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='Referrer.Source']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/qsl/faar/protocol/analytics/Referrer$Source;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.Referrer.Source[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/analytics/Referrer$Source;");
				try {
					return (global::Com.Qsl.Faar.Protocol.Analytics.Referrer.Source[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.Analytics.Referrer.Source));
				} finally {
				}
			}

		}

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/Referrer", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Referrer); }
		}

		protected Referrer (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='Referrer']/constructor[@name='Referrer' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Referrer ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Referrer)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

	}
}
