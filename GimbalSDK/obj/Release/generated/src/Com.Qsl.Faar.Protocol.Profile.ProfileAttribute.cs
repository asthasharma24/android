using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Profile {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='ProfileAttribute']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/profile/ProfileAttribute", DoNotGenerateAcw=true)]
	public partial class ProfileAttribute : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/profile/ProfileAttribute", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ProfileAttribute); }
		}

		protected ProfileAttribute (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='ProfileAttribute']/constructor[@name='ProfileAttribute' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ProfileAttribute ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ProfileAttribute)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAttributeCategories;
#pragma warning disable 0169
		static Delegate GetGetAttributeCategoriesHandler ()
		{
			if (cb_getAttributeCategories == null)
				cb_getAttributeCategories = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAttributeCategories);
			return cb_getAttributeCategories;
		}

		static IntPtr n_GetAttributeCategories (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory>.ToLocalJniHandle (__this.AttributeCategories);
		}
#pragma warning restore 0169

		static Delegate cb_setAttributeCategories_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetAttributeCategories_Ljava_util_List_Handler ()
		{
			if (cb_setAttributeCategories_Ljava_util_List_ == null)
				cb_setAttributeCategories_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAttributeCategories_Ljava_util_List_);
			return cb_setAttributeCategories_Ljava_util_List_;
		}

		static void n_SetAttributeCategories_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AttributeCategories = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getAttributeCategories;
		static IntPtr id_setAttributeCategories_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory> AttributeCategories {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='ProfileAttribute']/method[@name='getAttributeCategories' and count(parameter)=0]"
			[Register ("getAttributeCategories", "()Ljava/util/List;", "GetGetAttributeCategoriesHandler")]
			get {
				if (id_getAttributeCategories == IntPtr.Zero)
					id_getAttributeCategories = JNIEnv.GetMethodID (class_ref, "getAttributeCategories", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getAttributeCategories), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAttributeCategories", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='ProfileAttribute']/method[@name='setAttributeCategories' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.qsl.faar.protocol.profile.AttributeCategory&gt;']]"
			[Register ("setAttributeCategories", "(Ljava/util/List;)V", "GetSetAttributeCategories_Ljava_util_List_Handler")]
			set {
				if (id_setAttributeCategories_Ljava_util_List_ == IntPtr.Zero)
					id_setAttributeCategories_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setAttributeCategories", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setAttributeCategories_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAttributeCategories", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getKey;
#pragma warning disable 0169
		static Delegate GetGetKeyHandler ()
		{
			if (cb_getKey == null)
				cb_getKey = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetKey);
			return cb_getKey;
		}

		static IntPtr n_GetKey (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Key);
		}
#pragma warning restore 0169

		static Delegate cb_setKey_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetKey_Ljava_lang_String_Handler ()
		{
			if (cb_setKey_Ljava_lang_String_ == null)
				cb_setKey_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetKey_Ljava_lang_String_);
			return cb_setKey_Ljava_lang_String_;
		}

		static void n_SetKey_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Key = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getKey;
		static IntPtr id_setKey_Ljava_lang_String_;
		public virtual unsafe string Key {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='ProfileAttribute']/method[@name='getKey' and count(parameter)=0]"
			[Register ("getKey", "()Ljava/lang/String;", "GetGetKeyHandler")]
			get {
				if (id_getKey == IntPtr.Zero)
					id_getKey = JNIEnv.GetMethodID (class_ref, "getKey", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getKey), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getKey", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='ProfileAttribute']/method[@name='setKey' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setKey", "(Ljava/lang/String;)V", "GetSetKey_Ljava_lang_String_Handler")]
			set {
				if (id_setKey_Ljava_lang_String_ == IntPtr.Zero)
					id_setKey_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setKey", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setKey_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setKey", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_addCategory_Lcom_qsl_faar_protocol_profile_AttributeCategory_;
#pragma warning disable 0169
		static Delegate GetAddCategory_Lcom_qsl_faar_protocol_profile_AttributeCategory_Handler ()
		{
			if (cb_addCategory_Lcom_qsl_faar_protocol_profile_AttributeCategory_ == null)
				cb_addCategory_Lcom_qsl_faar_protocol_profile_AttributeCategory_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddCategory_Lcom_qsl_faar_protocol_profile_AttributeCategory_);
			return cb_addCategory_Lcom_qsl_faar_protocol_profile_AttributeCategory_;
		}

		static void n_AddCategory_Lcom_qsl_faar_protocol_profile_AttributeCategory_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.ProfileAttribute> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddCategory (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addCategory_Lcom_qsl_faar_protocol_profile_AttributeCategory_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='ProfileAttribute']/method[@name='addCategory' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.profile.AttributeCategory']]"
		[Register ("addCategory", "(Lcom/qsl/faar/protocol/profile/AttributeCategory;)V", "GetAddCategory_Lcom_qsl_faar_protocol_profile_AttributeCategory_Handler")]
		public virtual unsafe void AddCategory (global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory p0)
		{
			if (id_addCategory_Lcom_qsl_faar_protocol_profile_AttributeCategory_ == IntPtr.Zero)
				id_addCategory_Lcom_qsl_faar_protocol_profile_AttributeCategory_ = JNIEnv.GetMethodID (class_ref, "addCategory", "(Lcom/qsl/faar/protocol/profile/AttributeCategory;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addCategory_Lcom_qsl_faar_protocol_profile_AttributeCategory_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addCategory", "(Lcom/qsl/faar/protocol/profile/AttributeCategory;)V"), __args);
			} finally {
			}
		}

	}
}
