using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Service.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RetrieveTransmittersResponse']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/service/protocol/RetrieveTransmittersResponse", DoNotGenerateAcw=true)]
	public partial class RetrieveTransmittersResponse : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/service/protocol/RetrieveTransmittersResponse", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (RetrieveTransmittersResponse); }
		}

		protected RetrieveTransmittersResponse (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RetrieveTransmittersResponse']/constructor[@name='RetrieveTransmittersResponse' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe RetrieveTransmittersResponse ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (RetrieveTransmittersResponse)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getTransmitters;
#pragma warning disable 0169
		static Delegate GetGetTransmittersHandler ()
		{
			if (cb_getTransmitters == null)
				cb_getTransmitters = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTransmitters);
			return cb_getTransmitters;
		}

		static IntPtr n_GetTransmitters (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.RetrieveTransmittersResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.RetrieveTransmittersResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Impl.TransmitterInternal>.ToLocalJniHandle (__this.Transmitters);
		}
#pragma warning restore 0169

		static Delegate cb_setTransmitters_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetTransmitters_Ljava_util_List_Handler ()
		{
			if (cb_setTransmitters_Ljava_util_List_ == null)
				cb_setTransmitters_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTransmitters_Ljava_util_List_);
			return cb_setTransmitters_Ljava_util_List_;
		}

		static void n_SetTransmitters_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.RetrieveTransmittersResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.RetrieveTransmittersResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Impl.TransmitterInternal>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Transmitters = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTransmitters;
		static IntPtr id_setTransmitters_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> Transmitters {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RetrieveTransmittersResponse']/method[@name='getTransmitters' and count(parameter)=0]"
			[Register ("getTransmitters", "()Ljava/util/List;", "GetGetTransmittersHandler")]
			get {
				if (id_getTransmitters == IntPtr.Zero)
					id_getTransmitters = JNIEnv.GetMethodID (class_ref, "getTransmitters", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Impl.TransmitterInternal>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getTransmitters), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Impl.TransmitterInternal>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTransmitters", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RetrieveTransmittersResponse']/method[@name='setTransmitters' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.proximity.impl.TransmitterInternal&gt;']]"
			[Register ("setTransmitters", "(Ljava/util/List;)V", "GetSetTransmitters_Ljava_util_List_Handler")]
			set {
				if (id_setTransmitters_Ljava_util_List_ == IntPtr.Zero)
					id_setTransmitters_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setTransmitters", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Impl.TransmitterInternal>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTransmitters_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTransmitters", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
