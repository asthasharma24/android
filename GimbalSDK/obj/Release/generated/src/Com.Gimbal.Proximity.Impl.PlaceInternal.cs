using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Impl {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/impl/PlaceInternal", DoNotGenerateAcw=true)]
	public partial class PlaceInternal : global::Java.Lang.Object, global::Android.OS.IParcelable, global::Com.Gimbal.Proguard.IKeep {


		static IntPtr CREATOR_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/field[@name='CREATOR']"
		[Register ("CREATOR")]
		public static global::Android.OS.IParcelableCreator Creator {
			get {
				if (CREATOR_jfieldId == IntPtr.Zero)
					CREATOR_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CREATOR", "Landroid/os/Parcelable$Creator;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CREATOR_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Android.OS.IParcelableCreator> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/impl/PlaceInternal", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PlaceInternal); }
		}

		protected PlaceInternal (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/constructor[@name='PlaceInternal' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe PlaceInternal ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (PlaceInternal)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getArrivalRssi;
#pragma warning disable 0169
		static Delegate GetGetArrivalRssiHandler ()
		{
			if (cb_getArrivalRssi == null)
				cb_getArrivalRssi = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetArrivalRssi);
			return cb_getArrivalRssi;
		}

		static IntPtr n_GetArrivalRssi (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ArrivalRssi);
		}
#pragma warning restore 0169

		static Delegate cb_setArrivalRssi_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetArrivalRssi_Ljava_lang_Integer_Handler ()
		{
			if (cb_setArrivalRssi_Ljava_lang_Integer_ == null)
				cb_setArrivalRssi_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetArrivalRssi_Ljava_lang_Integer_);
			return cb_setArrivalRssi_Ljava_lang_Integer_;
		}

		static void n_SetArrivalRssi_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ArrivalRssi = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getArrivalRssi;
		static IntPtr id_setArrivalRssi_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer ArrivalRssi {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='getArrivalRssi' and count(parameter)=0]"
			[Register ("getArrivalRssi", "()Ljava/lang/Integer;", "GetGetArrivalRssiHandler")]
			get {
				if (id_getArrivalRssi == IntPtr.Zero)
					id_getArrivalRssi = JNIEnv.GetMethodID (class_ref, "getArrivalRssi", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getArrivalRssi), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getArrivalRssi", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='setArrivalRssi' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setArrivalRssi", "(Ljava/lang/Integer;)V", "GetSetArrivalRssi_Ljava_lang_Integer_Handler")]
			set {
				if (id_setArrivalRssi_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setArrivalRssi_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setArrivalRssi", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setArrivalRssi_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setArrivalRssi", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getBackgroundDepartureInterval;
#pragma warning disable 0169
		static Delegate GetGetBackgroundDepartureIntervalHandler ()
		{
			if (cb_getBackgroundDepartureInterval == null)
				cb_getBackgroundDepartureInterval = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBackgroundDepartureInterval);
			return cb_getBackgroundDepartureInterval;
		}

		static IntPtr n_GetBackgroundDepartureInterval (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.BackgroundDepartureInterval);
		}
#pragma warning restore 0169

		static Delegate cb_setBackgroundDepartureInterval_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetBackgroundDepartureInterval_Ljava_lang_Integer_Handler ()
		{
			if (cb_setBackgroundDepartureInterval_Ljava_lang_Integer_ == null)
				cb_setBackgroundDepartureInterval_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetBackgroundDepartureInterval_Ljava_lang_Integer_);
			return cb_setBackgroundDepartureInterval_Ljava_lang_Integer_;
		}

		static void n_SetBackgroundDepartureInterval_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.BackgroundDepartureInterval = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getBackgroundDepartureInterval;
		static IntPtr id_setBackgroundDepartureInterval_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer BackgroundDepartureInterval {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='getBackgroundDepartureInterval' and count(parameter)=0]"
			[Register ("getBackgroundDepartureInterval", "()Ljava/lang/Integer;", "GetGetBackgroundDepartureIntervalHandler")]
			get {
				if (id_getBackgroundDepartureInterval == IntPtr.Zero)
					id_getBackgroundDepartureInterval = JNIEnv.GetMethodID (class_ref, "getBackgroundDepartureInterval", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getBackgroundDepartureInterval), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBackgroundDepartureInterval", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='setBackgroundDepartureInterval' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setBackgroundDepartureInterval", "(Ljava/lang/Integer;)V", "GetSetBackgroundDepartureInterval_Ljava_lang_Integer_Handler")]
			set {
				if (id_setBackgroundDepartureInterval_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setBackgroundDepartureInterval_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setBackgroundDepartureInterval", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setBackgroundDepartureInterval_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setBackgroundDepartureInterval", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureInterval;
#pragma warning disable 0169
		static Delegate GetGetDepartureIntervalHandler ()
		{
			if (cb_getDepartureInterval == null)
				cb_getDepartureInterval = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDepartureInterval);
			return cb_getDepartureInterval;
		}

		static IntPtr n_GetDepartureInterval (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DepartureInterval);
		}
#pragma warning restore 0169

		static Delegate cb_setDepartureInterval_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetDepartureInterval_Ljava_lang_Integer_Handler ()
		{
			if (cb_setDepartureInterval_Ljava_lang_Integer_ == null)
				cb_setDepartureInterval_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDepartureInterval_Ljava_lang_Integer_);
			return cb_setDepartureInterval_Ljava_lang_Integer_;
		}

		static void n_SetDepartureInterval_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DepartureInterval = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureInterval;
		static IntPtr id_setDepartureInterval_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer DepartureInterval {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='getDepartureInterval' and count(parameter)=0]"
			[Register ("getDepartureInterval", "()Ljava/lang/Integer;", "GetGetDepartureIntervalHandler")]
			get {
				if (id_getDepartureInterval == IntPtr.Zero)
					id_getDepartureInterval = JNIEnv.GetMethodID (class_ref, "getDepartureInterval", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getDepartureInterval), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureInterval", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='setDepartureInterval' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setDepartureInterval", "(Ljava/lang/Integer;)V", "GetSetDepartureInterval_Ljava_lang_Integer_Handler")]
			set {
				if (id_setDepartureInterval_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setDepartureInterval_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setDepartureInterval", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDepartureInterval_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDepartureInterval", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureRssi;
#pragma warning disable 0169
		static Delegate GetGetDepartureRssiHandler ()
		{
			if (cb_getDepartureRssi == null)
				cb_getDepartureRssi = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDepartureRssi);
			return cb_getDepartureRssi;
		}

		static IntPtr n_GetDepartureRssi (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DepartureRssi);
		}
#pragma warning restore 0169

		static Delegate cb_setDepartureRssi_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetDepartureRssi_Ljava_lang_Integer_Handler ()
		{
			if (cb_setDepartureRssi_Ljava_lang_Integer_ == null)
				cb_setDepartureRssi_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDepartureRssi_Ljava_lang_Integer_);
			return cb_setDepartureRssi_Ljava_lang_Integer_;
		}

		static void n_SetDepartureRssi_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DepartureRssi = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureRssi;
		static IntPtr id_setDepartureRssi_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer DepartureRssi {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='getDepartureRssi' and count(parameter)=0]"
			[Register ("getDepartureRssi", "()Ljava/lang/Integer;", "GetGetDepartureRssiHandler")]
			get {
				if (id_getDepartureRssi == IntPtr.Zero)
					id_getDepartureRssi = JNIEnv.GetMethodID (class_ref, "getDepartureRssi", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getDepartureRssi), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureRssi", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='setDepartureRssi' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setDepartureRssi", "(Ljava/lang/Integer;)V", "GetSetDepartureRssi_Ljava_lang_Integer_Handler")]
			set {
				if (id_setDepartureRssi_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setDepartureRssi_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setDepartureRssi", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDepartureRssi_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDepartureRssi", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_Long_Handler ()
		{
			if (cb_setId_Ljava_lang_Long_ == null)
				cb_setId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_Long_);
			return cb_setId_Ljava_lang_Long_;
		}

		static void n_SetId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/Long;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setId", "(Ljava/lang/Long;)V", "GetSetId_Ljava_lang_Long_Handler")]
			set {
				if (id_setId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getName;
#pragma warning disable 0169
		static Delegate GetGetNameHandler ()
		{
			if (cb_getName == null)
				cb_getName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetName);
			return cb_getName;
		}

		static IntPtr n_GetName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Name);
		}
#pragma warning restore 0169

		static Delegate cb_setName_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetName_Ljava_lang_String_Handler ()
		{
			if (cb_setName_Ljava_lang_String_ == null)
				cb_setName_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetName_Ljava_lang_String_);
			return cb_setName_Ljava_lang_String_;
		}

		static void n_SetName_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Name = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getName;
		static IntPtr id_setName_Ljava_lang_String_;
		public virtual unsafe string Name {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='getName' and count(parameter)=0]"
			[Register ("getName", "()Ljava/lang/String;", "GetGetNameHandler")]
			get {
				if (id_getName == IntPtr.Zero)
					id_getName = JNIEnv.GetMethodID (class_ref, "getName", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getName), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getName", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='setName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setName", "(Ljava/lang/String;)V", "GetSetName_Ljava_lang_String_Handler")]
			set {
				if (id_setName_Ljava_lang_String_ == IntPtr.Zero)
					id_setName_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setName", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setName_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setName", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		static Delegate cb_setUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setUuid_Ljava_lang_String_ == null)
				cb_setUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUuid_Ljava_lang_String_);
			return cb_setUuid_Ljava_lang_String_;
		}

		static void n_SetUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Uuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUuid;
		static IntPtr id_setUuid_Ljava_lang_String_;
		public virtual unsafe string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler")]
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='setUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUuid", "(Ljava/lang/String;)V", "GetSetUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_describeContents;
#pragma warning disable 0169
		static Delegate GetDescribeContentsHandler ()
		{
			if (cb_describeContents == null)
				cb_describeContents = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_DescribeContents);
			return cb_describeContents;
		}

		static int n_DescribeContents (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.DescribeContents ();
		}
#pragma warning restore 0169

		static IntPtr id_describeContents;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='describeContents' and count(parameter)=0]"
		[Register ("describeContents", "()I", "GetDescribeContentsHandler")]
		public virtual unsafe int DescribeContents ()
		{
			if (id_describeContents == IntPtr.Zero)
				id_describeContents = JNIEnv.GetMethodID (class_ref, "describeContents", "()I");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallIntMethod  (Handle, id_describeContents);
				else
					return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "describeContents", "()I"));
			} finally {
			}
		}

		static Delegate cb_writeToParcel_Landroid_os_Parcel_I;
#pragma warning disable 0169
		static Delegate GetWriteToParcel_Landroid_os_Parcel_IHandler ()
		{
			if (cb_writeToParcel_Landroid_os_Parcel_I == null)
				cb_writeToParcel_Landroid_os_Parcel_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_WriteToParcel_Landroid_os_Parcel_I);
			return cb_writeToParcel_Landroid_os_Parcel_I;
		}

		static void n_WriteToParcel_Landroid_os_Parcel_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int native_p1)
		{
			global::Com.Gimbal.Proximity.Impl.PlaceInternal __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.PlaceInternal> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.Parcel p0 = global::Java.Lang.Object.GetObject<global::Android.OS.Parcel> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Android.OS.ParcelableWriteFlags p1 = (global::Android.OS.ParcelableWriteFlags) native_p1;
			__this.WriteToParcel (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_writeToParcel_Landroid_os_Parcel_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.impl']/class[@name='PlaceInternal']/method[@name='writeToParcel' and count(parameter)=2 and parameter[1][@type='android.os.Parcel'] and parameter[2][@type='int']]"
		[Register ("writeToParcel", "(Landroid/os/Parcel;I)V", "GetWriteToParcel_Landroid_os_Parcel_IHandler")]
		public virtual unsafe void WriteToParcel (global::Android.OS.Parcel p0, [global::Android.Runtime.GeneratedEnum] global::Android.OS.ParcelableWriteFlags p1)
		{
			if (id_writeToParcel_Landroid_os_Parcel_I == IntPtr.Zero)
				id_writeToParcel_Landroid_os_Parcel_I = JNIEnv.GetMethodID (class_ref, "writeToParcel", "(Landroid/os/Parcel;I)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue ((int) p1);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_writeToParcel_Landroid_os_Parcel_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeToParcel", "(Landroid/os/Parcel;I)V"), __args);
			} finally {
			}
		}

	}
}
