using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Service.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RegisterUserResponse']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/service/protocol/RegisterUserResponse", DoNotGenerateAcw=true)]
	public partial class RegisterUserResponse : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/service/protocol/RegisterUserResponse", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (RegisterUserResponse); }
		}

		protected RegisterUserResponse (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RegisterUserResponse']/constructor[@name='RegisterUserResponse' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe RegisterUserResponse ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (RegisterUserResponse)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getIdentifier;
#pragma warning disable 0169
		static Delegate GetGetIdentifierHandler ()
		{
			if (cb_getIdentifier == null)
				cb_getIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIdentifier);
			return cb_getIdentifier;
		}

		static IntPtr n_GetIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.RegisterUserResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.RegisterUserResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Identifier);
		}
#pragma warning restore 0169

		static Delegate cb_setIdentifier_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetIdentifier_Ljava_lang_String_Handler ()
		{
			if (cb_setIdentifier_Ljava_lang_String_ == null)
				cb_setIdentifier_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetIdentifier_Ljava_lang_String_);
			return cb_setIdentifier_Ljava_lang_String_;
		}

		static void n_SetIdentifier_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.RegisterUserResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.RegisterUserResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Identifier = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getIdentifier;
		static IntPtr id_setIdentifier_Ljava_lang_String_;
		public virtual unsafe string Identifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RegisterUserResponse']/method[@name='getIdentifier' and count(parameter)=0]"
			[Register ("getIdentifier", "()Ljava/lang/String;", "GetGetIdentifierHandler")]
			get {
				if (id_getIdentifier == IntPtr.Zero)
					id_getIdentifier = JNIEnv.GetMethodID (class_ref, "getIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RegisterUserResponse']/method[@name='setIdentifier' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setIdentifier", "(Ljava/lang/String;)V", "GetSetIdentifier_Ljava_lang_String_Handler")]
			set {
				if (id_setIdentifier_Ljava_lang_String_ == IntPtr.Zero)
					id_setIdentifier_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setIdentifier", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setIdentifier_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setIdentifier", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
