using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Analytics {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='ARClientEvent']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/ARClientEvent", DoNotGenerateAcw=true)]
	public partial class ARClientEvent : global::Com.Qsl.Faar.Protocol.Analytics.ClientEvent {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='ARClientEvent']/field[@name='AR_USER_ID']"
		[Register ("AR_USER_ID")]
		public const string ArUserId = (string) "AR_USERID";
		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/ARClientEvent", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ARClientEvent); }
		}

		protected ARClientEvent (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='ARClientEvent']/constructor[@name='ARClientEvent' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ARClientEvent ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ARClientEvent)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

	}
}
