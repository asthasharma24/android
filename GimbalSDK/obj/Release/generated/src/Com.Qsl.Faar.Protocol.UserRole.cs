using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserRole']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/UserRole", DoNotGenerateAcw=true)]
	public sealed partial class UserRole : global::Java.Lang.Enum {


		static IntPtr ANONYMOUS_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserRole']/field[@name='ANONYMOUS']"
		[Register ("ANONYMOUS")]
		public static global::Com.Qsl.Faar.Protocol.UserRole Anonymous {
			get {
				if (ANONYMOUS_jfieldId == IntPtr.Zero)
					ANONYMOUS_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "ANONYMOUS", "Lcom/qsl/faar/protocol/UserRole;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, ANONYMOUS_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserRole> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr REGISTERED_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserRole']/field[@name='REGISTERED']"
		[Register ("REGISTERED")]
		public static global::Com.Qsl.Faar.Protocol.UserRole Registered {
			get {
				if (REGISTERED_jfieldId == IntPtr.Zero)
					REGISTERED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "REGISTERED", "Lcom/qsl/faar/protocol/UserRole;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, REGISTERED_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserRole> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/UserRole", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (UserRole); }
		}

		internal UserRole (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_valueOf_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserRole']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/UserRole;", "")]
		public static unsafe global::Com.Qsl.Faar.Protocol.UserRole ValueOf (string p0)
		{
			if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
				id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/UserRole;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				global::Com.Qsl.Faar.Protocol.UserRole __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserRole> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_values;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserRole']/method[@name='values' and count(parameter)=0]"
		[Register ("values", "()[Lcom/qsl/faar/protocol/UserRole;", "")]
		public static unsafe global::Com.Qsl.Faar.Protocol.UserRole[] Values ()
		{
			if (id_values == IntPtr.Zero)
				id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/UserRole;");
			try {
				return (global::Com.Qsl.Faar.Protocol.UserRole[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.UserRole));
			} finally {
			}
		}

	}
}
