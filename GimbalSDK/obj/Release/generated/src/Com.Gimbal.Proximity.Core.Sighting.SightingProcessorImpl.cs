using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Sighting {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingProcessorImpl']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/sighting/SightingProcessorImpl", DoNotGenerateAcw=true)]
	public partial class SightingProcessorImpl : global::Android.Content.BroadcastReceiver {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/sighting/SightingProcessorImpl", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (SightingProcessorImpl); }
		}

		protected SightingProcessorImpl (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_a;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingProcessorImpl']/method[@name='a' and count(parameter)=0]"
		[Register ("a", "()J", "")]
		public unsafe long A ()
		{
			if (id_a == IntPtr.Zero)
				id_a = JNIEnv.GetMethodID (class_ref, "a", "()J");
			try {
				return JNIEnv.CallLongMethod  (Handle, id_a);
			} finally {
			}
		}

		static IntPtr id_a_Lcom_gimbal_proximity_core_sighting_Sighting_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingProcessorImpl']/method[@name='a' and count(parameter)=1 and parameter[1][@type='com.gimbal.proximity.core.sighting.Sighting']]"
		[Register ("a", "(Lcom/gimbal/proximity/core/sighting/Sighting;)V", "")]
		public unsafe void A (global::Com.Gimbal.Proximity.Core.Sighting.Sighting p0)
		{
			if (id_a_Lcom_gimbal_proximity_core_sighting_Sighting_ == IntPtr.Zero)
				id_a_Lcom_gimbal_proximity_core_sighting_Sighting_ = JNIEnv.GetMethodID (class_ref, "a", "(Lcom/gimbal/proximity/core/sighting/Sighting;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod  (Handle, id_a_Lcom_gimbal_proximity_core_sighting_Sighting_, __args);
			} finally {
			}
		}

		static IntPtr id_b;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingProcessorImpl']/method[@name='b' and count(parameter)=0]"
		[Register ("b", "()V", "")]
		public unsafe void B ()
		{
			if (id_b == IntPtr.Zero)
				id_b = JNIEnv.GetMethodID (class_ref, "b", "()V");
			try {
				JNIEnv.CallVoidMethod  (Handle, id_b);
			} finally {
			}
		}

		static Delegate cb_onReceive_Landroid_content_Context_Landroid_content_Intent_;
#pragma warning disable 0169
		static Delegate GetOnReceive_Landroid_content_Context_Landroid_content_Intent_Handler ()
		{
			if (cb_onReceive_Landroid_content_Context_Landroid_content_Intent_ == null)
				cb_onReceive_Landroid_content_Context_Landroid_content_Intent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_OnReceive_Landroid_content_Context_Landroid_content_Intent_);
			return cb_onReceive_Landroid_content_Context_Landroid_content_Intent_;
		}

		static void n_OnReceive_Landroid_content_Context_Landroid_content_Intent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Proximity.Core.Sighting.SightingProcessorImpl __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.SightingProcessorImpl> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Context p0 = global::Java.Lang.Object.GetObject<global::Android.Content.Context> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Android.Content.Intent p1 = global::Java.Lang.Object.GetObject<global::Android.Content.Intent> (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.OnReceive (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_onReceive_Landroid_content_Context_Landroid_content_Intent_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.sighting']/class[@name='SightingProcessorImpl']/method[@name='onReceive' and count(parameter)=2 and parameter[1][@type='android.content.Context'] and parameter[2][@type='android.content.Intent']]"
		[Register ("onReceive", "(Landroid/content/Context;Landroid/content/Intent;)V", "GetOnReceive_Landroid_content_Context_Landroid_content_Intent_Handler")]
		public override unsafe void OnReceive (global::Android.Content.Context p0, global::Android.Content.Intent p1)
		{
			if (id_onReceive_Landroid_content_Context_Landroid_content_Intent_ == IntPtr.Zero)
				id_onReceive_Landroid_content_Context_Landroid_content_Intent_ = JNIEnv.GetMethodID (class_ref, "onReceive", "(Landroid/content/Context;Landroid/content/Intent;)V");
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (p1);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_onReceive_Landroid_content_Context_Landroid_content_Intent_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onReceive", "(Landroid/content/Context;Landroid/content/Intent;)V"), __args);
			} finally {
			}
		}

	}
}
