using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='MovieTimes']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/MovieTimes", DoNotGenerateAcw=true)]
	public partial class MovieTimes : global::Java.Lang.Object, global::Java.IO.ISerializable {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/MovieTimes", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (MovieTimes); }
		}

		protected MovieTimes (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='MovieTimes']/constructor[@name='MovieTimes' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe MovieTimes ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (MovieTimes)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getMovieTimesJson;
#pragma warning disable 0169
		static Delegate GetGetMovieTimesJsonHandler ()
		{
			if (cb_getMovieTimesJson == null)
				cb_getMovieTimesJson = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetMovieTimesJson);
			return cb_getMovieTimesJson;
		}

		static IntPtr n_GetMovieTimesJson (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.MovieTimes __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.MovieTimes> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.MovieTimesJson);
		}
#pragma warning restore 0169

		static Delegate cb_setMovieTimesJson_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetMovieTimesJson_Ljava_lang_String_Handler ()
		{
			if (cb_setMovieTimesJson_Ljava_lang_String_ == null)
				cb_setMovieTimesJson_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetMovieTimesJson_Ljava_lang_String_);
			return cb_setMovieTimesJson_Ljava_lang_String_;
		}

		static void n_SetMovieTimesJson_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.MovieTimes __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.MovieTimes> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.MovieTimesJson = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getMovieTimesJson;
		static IntPtr id_setMovieTimesJson_Ljava_lang_String_;
		public virtual unsafe string MovieTimesJson {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='MovieTimes']/method[@name='getMovieTimesJson' and count(parameter)=0]"
			[Register ("getMovieTimesJson", "()Ljava/lang/String;", "GetGetMovieTimesJsonHandler")]
			get {
				if (id_getMovieTimesJson == IntPtr.Zero)
					id_getMovieTimesJson = JNIEnv.GetMethodID (class_ref, "getMovieTimesJson", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getMovieTimesJson), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMovieTimesJson", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='MovieTimes']/method[@name='setMovieTimesJson' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setMovieTimesJson", "(Ljava/lang/String;)V", "GetSetMovieTimesJson_Ljava_lang_String_Handler")]
			set {
				if (id_setMovieTimesJson_Ljava_lang_String_ == IntPtr.Zero)
					id_setMovieTimesJson_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setMovieTimesJson", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setMovieTimesJson_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setMovieTimesJson", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
