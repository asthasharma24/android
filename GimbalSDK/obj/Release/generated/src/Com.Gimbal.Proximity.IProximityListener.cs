using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='ProximityListener']"
	[Register ("com/gimbal/proximity/ProximityListener", "", "Com.Gimbal.Proximity.IProximityListenerInvoker")]
	public partial interface IProximityListener : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='ProximityListener']/method[@name='serviceStarted' and count(parameter)=0]"
		[Register ("serviceStarted", "()V", "GetServiceStartedHandler:Com.Gimbal.Proximity.IProximityListenerInvoker, GimbalSDK")]
		void ServiceStarted ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='ProximityListener']/method[@name='startServiceFailed' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='java.lang.String']]"
		[Register ("startServiceFailed", "(ILjava/lang/String;)V", "GetStartServiceFailed_ILjava_lang_String_Handler:Com.Gimbal.Proximity.IProximityListenerInvoker, GimbalSDK")]
		void StartServiceFailed (int p0, string p1);

	}

	[global::Android.Runtime.Register ("com/gimbal/proximity/ProximityListener", DoNotGenerateAcw=true)]
	internal class IProximityListenerInvoker : global::Java.Lang.Object, IProximityListener {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/gimbal/proximity/ProximityListener");
		IntPtr class_ref;

		public static IProximityListener GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IProximityListener> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.gimbal.proximity.ProximityListener"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IProximityListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IProximityListenerInvoker); }
		}

		static Delegate cb_serviceStarted;
#pragma warning disable 0169
		static Delegate GetServiceStartedHandler ()
		{
			if (cb_serviceStarted == null)
				cb_serviceStarted = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_ServiceStarted);
			return cb_serviceStarted;
		}

		static void n_ServiceStarted (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.IProximityListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IProximityListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ServiceStarted ();
		}
#pragma warning restore 0169

		IntPtr id_serviceStarted;
		public unsafe void ServiceStarted ()
		{
			if (id_serviceStarted == IntPtr.Zero)
				id_serviceStarted = JNIEnv.GetMethodID (class_ref, "serviceStarted", "()V");
			JNIEnv.CallVoidMethod (Handle, id_serviceStarted);
		}

		static Delegate cb_startServiceFailed_ILjava_lang_String_;
#pragma warning disable 0169
		static Delegate GetStartServiceFailed_ILjava_lang_String_Handler ()
		{
			if (cb_startServiceFailed_ILjava_lang_String_ == null)
				cb_startServiceFailed_ILjava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, IntPtr>) n_StartServiceFailed_ILjava_lang_String_);
			return cb_startServiceFailed_ILjava_lang_String_;
		}

		static void n_StartServiceFailed_ILjava_lang_String_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Proximity.IProximityListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IProximityListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.StartServiceFailed (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_startServiceFailed_ILjava_lang_String_;
		public unsafe void StartServiceFailed (int p0, string p1)
		{
			if (id_startServiceFailed_ILjava_lang_String_ == IntPtr.Zero)
				id_startServiceFailed_ILjava_lang_String_ = JNIEnv.GetMethodID (class_ref, "startServiceFailed", "(ILjava/lang/String;)V");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (native_p1);
			JNIEnv.CallVoidMethod (Handle, id_startServiceFailed_ILjava_lang_String_, __args);
			JNIEnv.DeleteLocalRef (native_p1);
		}

	}

	public partial class StartServiceFailedEventArgs : global::System.EventArgs {

		public StartServiceFailedEventArgs (int p0, string p1)
		{
			this.p0 = p0;
			this.p1 = p1;
		}

		int p0;
		public int P0 {
			get { return p0; }
		}

		string p1;
		public string P1 {
			get { return p1; }
		}
	}

	[global::Android.Runtime.Register ("mono/com/gimbal/proximity/ProximityListenerImplementor")]
	internal sealed partial class IProximityListenerImplementor : global::Java.Lang.Object, IProximityListener {

		object sender;

		public IProximityListenerImplementor (object sender)
			: base (
				global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/com/gimbal/proximity/ProximityListenerImplementor", "()V"),
				JniHandleOwnership.TransferLocalRef)
		{
			global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
			this.sender = sender;
		}

#pragma warning disable 0649
		public EventHandler ServiceStartedHandler;
#pragma warning restore 0649

		public void ServiceStarted ()
		{
			var __h = ServiceStartedHandler;
			if (__h != null)
				__h (sender, new EventArgs ());
		}
#pragma warning disable 0649
		public EventHandler<StartServiceFailedEventArgs> StartServiceFailedHandler;
#pragma warning restore 0649

		public void StartServiceFailed (int p0, string p1)
		{
			var __h = StartServiceFailedHandler;
			if (__h != null)
				__h (sender, new StartServiceFailedEventArgs (p0, p1));
		}

		internal static bool __IsEmpty (IProximityListenerImplementor value)
		{
			return value.ServiceStartedHandler == null && value.StartServiceFailedHandler == null;
		}
	}

}
