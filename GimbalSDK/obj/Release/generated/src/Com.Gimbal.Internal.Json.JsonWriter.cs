using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Json {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonWriter']"
	[global::Android.Runtime.Register ("com/gimbal/internal/json/JsonWriter", DoNotGenerateAcw=true)]
	public partial class JsonWriter : global::Com.Gimbal.Internal.Json.JsonMapperBase {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/json/JsonWriter", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (JsonWriter); }
		}

		protected JsonWriter (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Z;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonWriter']/constructor[@name='JsonWriter' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register (".ctor", "(Z)V", "")]
		public unsafe JsonWriter (bool p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (JsonWriter)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Z)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Z)V", __args);
					return;
				}

				if (id_ctor_Z == IntPtr.Zero)
					id_ctor_Z = JNIEnv.GetMethodID (class_ref, "<init>", "(Z)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Z, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Z, __args);
			} finally {
			}
		}

		static Delegate cb_write_Ljava_lang_Object_;
#pragma warning disable 0169
		static Delegate GetWrite_Ljava_lang_Object_Handler ()
		{
			if (cb_write_Ljava_lang_Object_ == null)
				cb_write_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_Write_Ljava_lang_Object_);
			return cb_write_Ljava_lang_Object_;
		}

		static IntPtr n_Write_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Json.JsonWriter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonWriter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Object p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.Write (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_write_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonWriter']/method[@name='write' and count(parameter)=1 and parameter[1][@type='java.lang.Object']]"
		[Register ("write", "(Ljava/lang/Object;)Ljava/lang/Object;", "GetWrite_Ljava_lang_Object_Handler")]
		public virtual unsafe global::Java.Lang.Object Write (global::Java.Lang.Object p0)
		{
			if (id_write_Ljava_lang_Object_ == IntPtr.Zero)
				id_write_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "write", "(Ljava/lang/Object;)Ljava/lang/Object;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				global::Java.Lang.Object __ret;
				if (GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod  (Handle, id_write_Ljava_lang_Object_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "write", "(Ljava/lang/Object;)Ljava/lang/Object;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_writeObject_Ljava_lang_Object_;
#pragma warning disable 0169
		static Delegate GetWriteObject_Ljava_lang_Object_Handler ()
		{
			if (cb_writeObject_Ljava_lang_Object_ == null)
				cb_writeObject_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_WriteObject_Ljava_lang_Object_);
			return cb_writeObject_Ljava_lang_Object_;
		}

		static IntPtr n_WriteObject_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Json.JsonWriter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonWriter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Object p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.WriteObject (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_writeObject_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonWriter']/method[@name='writeObject' and count(parameter)=1 and parameter[1][@type='java.lang.Object']]"
		[Register ("writeObject", "(Ljava/lang/Object;)Ljava/lang/Object;", "GetWriteObject_Ljava_lang_Object_Handler")]
		public virtual unsafe global::Java.Lang.Object WriteObject (global::Java.Lang.Object p0)
		{
			if (id_writeObject_Ljava_lang_Object_ == IntPtr.Zero)
				id_writeObject_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "writeObject", "(Ljava/lang/Object;)Ljava/lang/Object;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				global::Java.Lang.Object __ret;
				if (GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod  (Handle, id_writeObject_Ljava_lang_Object_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeObject", "(Ljava/lang/Object;)Ljava/lang/Object;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_writeValueAsArray_Ljava_lang_Object_;
#pragma warning disable 0169
		static Delegate GetWriteValueAsArray_Ljava_lang_Object_Handler ()
		{
			if (cb_writeValueAsArray_Ljava_lang_Object_ == null)
				cb_writeValueAsArray_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_WriteValueAsArray_Ljava_lang_Object_);
			return cb_writeValueAsArray_Ljava_lang_Object_;
		}

		static IntPtr n_WriteValueAsArray_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Json.JsonWriter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonWriter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Object p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.WriteValueAsArray (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_writeValueAsArray_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonWriter']/method[@name='writeValueAsArray' and count(parameter)=1 and parameter[1][@type='java.lang.Object']]"
		[Register ("writeValueAsArray", "(Ljava/lang/Object;)Lorg/json/JSONArray;", "GetWriteValueAsArray_Ljava_lang_Object_Handler")]
		public virtual unsafe global::Org.Json.JSONArray WriteValueAsArray (global::Java.Lang.Object p0)
		{
			if (id_writeValueAsArray_Ljava_lang_Object_ == IntPtr.Zero)
				id_writeValueAsArray_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "writeValueAsArray", "(Ljava/lang/Object;)Lorg/json/JSONArray;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				global::Org.Json.JSONArray __ret;
				if (GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Org.Json.JSONArray> (JNIEnv.CallObjectMethod  (Handle, id_writeValueAsArray_Ljava_lang_Object_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Org.Json.JSONArray> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeValueAsArray", "(Ljava/lang/Object;)Lorg/json/JSONArray;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_writeValueAsObject_Ljava_lang_Object_;
#pragma warning disable 0169
		static Delegate GetWriteValueAsObject_Ljava_lang_Object_Handler ()
		{
			if (cb_writeValueAsObject_Ljava_lang_Object_ == null)
				cb_writeValueAsObject_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_WriteValueAsObject_Ljava_lang_Object_);
			return cb_writeValueAsObject_Ljava_lang_Object_;
		}

		static IntPtr n_WriteValueAsObject_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Json.JsonWriter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonWriter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Object p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.WriteValueAsObject (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_writeValueAsObject_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonWriter']/method[@name='writeValueAsObject' and count(parameter)=1 and parameter[1][@type='java.lang.Object']]"
		[Register ("writeValueAsObject", "(Ljava/lang/Object;)Lorg/json/JSONObject;", "GetWriteValueAsObject_Ljava_lang_Object_Handler")]
		public virtual unsafe global::Org.Json.JSONObject WriteValueAsObject (global::Java.Lang.Object p0)
		{
			if (id_writeValueAsObject_Ljava_lang_Object_ == IntPtr.Zero)
				id_writeValueAsObject_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "writeValueAsObject", "(Ljava/lang/Object;)Lorg/json/JSONObject;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				global::Org.Json.JSONObject __ret;
				if (GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Org.Json.JSONObject> (JNIEnv.CallObjectMethod  (Handle, id_writeValueAsObject_Ljava_lang_Object_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Org.Json.JSONObject> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeValueAsObject", "(Ljava/lang/Object;)Lorg/json/JSONObject;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static Delegate cb_writeValueAsString_Ljava_lang_Object_;
#pragma warning disable 0169
		static Delegate GetWriteValueAsString_Ljava_lang_Object_Handler ()
		{
			if (cb_writeValueAsString_Ljava_lang_Object_ == null)
				cb_writeValueAsString_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_WriteValueAsString_Ljava_lang_Object_);
			return cb_writeValueAsString_Ljava_lang_Object_;
		}

		static IntPtr n_WriteValueAsString_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Json.JsonWriter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonWriter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Object p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.WriteValueAsString (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_writeValueAsString_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonWriter']/method[@name='writeValueAsString' and count(parameter)=1 and parameter[1][@type='java.lang.Object']]"
		[Register ("writeValueAsString", "(Ljava/lang/Object;)Ljava/lang/String;", "GetWriteValueAsString_Ljava_lang_Object_Handler")]
		public virtual unsafe string WriteValueAsString (global::Java.Lang.Object p0)
		{
			if (id_writeValueAsString_Ljava_lang_Object_ == IntPtr.Zero)
				id_writeValueAsString_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "writeValueAsString", "(Ljava/lang/Object;)Ljava/lang/String;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				string __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_writeValueAsString_Ljava_lang_Object_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "writeValueAsString", "(Ljava/lang/Object;)Ljava/lang/String;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

	}
}
