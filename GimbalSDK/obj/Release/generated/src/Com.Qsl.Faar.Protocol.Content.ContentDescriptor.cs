using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Content {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/content/ContentDescriptor", DoNotGenerateAcw=true)]
	public partial class ContentDescriptor : global::Java.Lang.Object, global::Java.IO.ISerializable {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/content/ContentDescriptor", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ContentDescriptor); }
		}

		protected ContentDescriptor (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/constructor[@name='ContentDescriptor' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ContentDescriptor ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ContentDescriptor)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getCampaignId;
#pragma warning disable 0169
		static Delegate GetGetCampaignIdHandler ()
		{
			if (cb_getCampaignId == null)
				cb_getCampaignId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCampaignId);
			return cb_getCampaignId;
		}

		static IntPtr n_GetCampaignId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.CampaignId);
		}
#pragma warning restore 0169

		static Delegate cb_setCampaignId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetCampaignId_Ljava_lang_String_Handler ()
		{
			if (cb_setCampaignId_Ljava_lang_String_ == null)
				cb_setCampaignId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCampaignId_Ljava_lang_String_);
			return cb_setCampaignId_Ljava_lang_String_;
		}

		static void n_SetCampaignId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.CampaignId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getCampaignId;
		static IntPtr id_setCampaignId_Ljava_lang_String_;
		public virtual unsafe string CampaignId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='getCampaignId' and count(parameter)=0]"
			[Register ("getCampaignId", "()Ljava/lang/String;", "GetGetCampaignIdHandler")]
			get {
				if (id_getCampaignId == IntPtr.Zero)
					id_getCampaignId = JNIEnv.GetMethodID (class_ref, "getCampaignId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getCampaignId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCampaignId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setCampaignId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setCampaignId", "(Ljava/lang/String;)V", "GetSetCampaignId_Ljava_lang_String_Handler")]
			set {
				if (id_setCampaignId_Ljava_lang_String_ == IntPtr.Zero)
					id_setCampaignId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setCampaignId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCampaignId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCampaignId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_isCombineTitleDescription;
#pragma warning disable 0169
		static Delegate GetIsCombineTitleDescriptionHandler ()
		{
			if (cb_isCombineTitleDescription == null)
				cb_isCombineTitleDescription = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsCombineTitleDescription);
			return cb_isCombineTitleDescription;
		}

		static bool n_IsCombineTitleDescription (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.CombineTitleDescription;
		}
#pragma warning restore 0169

		static Delegate cb_setCombineTitleDescription_Z;
#pragma warning disable 0169
		static Delegate GetSetCombineTitleDescription_ZHandler ()
		{
			if (cb_setCombineTitleDescription_Z == null)
				cb_setCombineTitleDescription_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetCombineTitleDescription_Z);
			return cb_setCombineTitleDescription_Z;
		}

		static void n_SetCombineTitleDescription_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.CombineTitleDescription = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isCombineTitleDescription;
		static IntPtr id_setCombineTitleDescription_Z;
		public virtual unsafe bool CombineTitleDescription {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='isCombineTitleDescription' and count(parameter)=0]"
			[Register ("isCombineTitleDescription", "()Z", "GetIsCombineTitleDescriptionHandler")]
			get {
				if (id_isCombineTitleDescription == IntPtr.Zero)
					id_isCombineTitleDescription = JNIEnv.GetMethodID (class_ref, "isCombineTitleDescription", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isCombineTitleDescription);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isCombineTitleDescription", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setCombineTitleDescription' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setCombineTitleDescription", "(Z)V", "GetSetCombineTitleDescription_ZHandler")]
			set {
				if (id_setCombineTitleDescription_Z == IntPtr.Zero)
					id_setCombineTitleDescription_Z = JNIEnv.GetMethodID (class_ref, "setCombineTitleDescription", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCombineTitleDescription_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCombineTitleDescription", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getContentAttributes;
#pragma warning disable 0169
		static Delegate GetGetContentAttributesHandler ()
		{
			if (cb_getContentAttributes == null)
				cb_getContentAttributes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetContentAttributes);
			return cb_getContentAttributes;
		}

		static IntPtr n_GetContentAttributes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ContentAttributes);
		}
#pragma warning restore 0169

		static Delegate cb_setContentAttributes_Lcom_qsl_faar_protocol_content_ContentAttributes_;
#pragma warning disable 0169
		static Delegate GetSetContentAttributes_Lcom_qsl_faar_protocol_content_ContentAttributes_Handler ()
		{
			if (cb_setContentAttributes_Lcom_qsl_faar_protocol_content_ContentAttributes_ == null)
				cb_setContentAttributes_Lcom_qsl_faar_protocol_content_ContentAttributes_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetContentAttributes_Lcom_qsl_faar_protocol_content_ContentAttributes_);
			return cb_setContentAttributes_Lcom_qsl_faar_protocol_content_ContentAttributes_;
		}

		static void n_SetContentAttributes_Lcom_qsl_faar_protocol_content_ContentAttributes_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.Content.ContentAttributes p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentAttributes> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ContentAttributes = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getContentAttributes;
		static IntPtr id_setContentAttributes_Lcom_qsl_faar_protocol_content_ContentAttributes_;
		public virtual unsafe global::Com.Qsl.Faar.Protocol.Content.ContentAttributes ContentAttributes {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='getContentAttributes' and count(parameter)=0]"
			[Register ("getContentAttributes", "()Lcom/qsl/faar/protocol/content/ContentAttributes;", "GetGetContentAttributesHandler")]
			get {
				if (id_getContentAttributes == IntPtr.Zero)
					id_getContentAttributes = JNIEnv.GetMethodID (class_ref, "getContentAttributes", "()Lcom/qsl/faar/protocol/content/ContentAttributes;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentAttributes> (JNIEnv.CallObjectMethod  (Handle, id_getContentAttributes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentAttributes> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getContentAttributes", "()Lcom/qsl/faar/protocol/content/ContentAttributes;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setContentAttributes' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.content.ContentAttributes']]"
			[Register ("setContentAttributes", "(Lcom/qsl/faar/protocol/content/ContentAttributes;)V", "GetSetContentAttributes_Lcom_qsl_faar_protocol_content_ContentAttributes_Handler")]
			set {
				if (id_setContentAttributes_Lcom_qsl_faar_protocol_content_ContentAttributes_ == IntPtr.Zero)
					id_setContentAttributes_Lcom_qsl_faar_protocol_content_ContentAttributes_ = JNIEnv.GetMethodID (class_ref, "setContentAttributes", "(Lcom/qsl/faar/protocol/content/ContentAttributes;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setContentAttributes_Lcom_qsl_faar_protocol_content_ContentAttributes_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setContentAttributes", "(Lcom/qsl/faar/protocol/content/ContentAttributes;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getContentUrl;
#pragma warning disable 0169
		static Delegate GetGetContentUrlHandler ()
		{
			if (cb_getContentUrl == null)
				cb_getContentUrl = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetContentUrl);
			return cb_getContentUrl;
		}

		static IntPtr n_GetContentUrl (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ContentUrl);
		}
#pragma warning restore 0169

		static Delegate cb_setContentUrl_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetContentUrl_Ljava_lang_String_Handler ()
		{
			if (cb_setContentUrl_Ljava_lang_String_ == null)
				cb_setContentUrl_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetContentUrl_Ljava_lang_String_);
			return cb_setContentUrl_Ljava_lang_String_;
		}

		static void n_SetContentUrl_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ContentUrl = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getContentUrl;
		static IntPtr id_setContentUrl_Ljava_lang_String_;
		public virtual unsafe string ContentUrl {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='getContentUrl' and count(parameter)=0]"
			[Register ("getContentUrl", "()Ljava/lang/String;", "GetGetContentUrlHandler")]
			get {
				if (id_getContentUrl == IntPtr.Zero)
					id_getContentUrl = JNIEnv.GetMethodID (class_ref, "getContentUrl", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getContentUrl), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getContentUrl", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setContentUrl' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setContentUrl", "(Ljava/lang/String;)V", "GetSetContentUrl_Ljava_lang_String_Handler")]
			set {
				if (id_setContentUrl_Ljava_lang_String_ == IntPtr.Zero)
					id_setContentUrl_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setContentUrl", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setContentUrl_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setContentUrl", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getDelayInSeconds;
#pragma warning disable 0169
		static Delegate GetGetDelayInSecondsHandler ()
		{
			if (cb_getDelayInSeconds == null)
				cb_getDelayInSeconds = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDelayInSeconds);
			return cb_getDelayInSeconds;
		}

		static IntPtr n_GetDelayInSeconds (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DelayInSeconds);
		}
#pragma warning restore 0169

		static Delegate cb_setDelayInSeconds_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetDelayInSeconds_Ljava_lang_Long_Handler ()
		{
			if (cb_setDelayInSeconds_Ljava_lang_Long_ == null)
				cb_setDelayInSeconds_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDelayInSeconds_Ljava_lang_Long_);
			return cb_setDelayInSeconds_Ljava_lang_Long_;
		}

		static void n_SetDelayInSeconds_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DelayInSeconds = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDelayInSeconds;
		static IntPtr id_setDelayInSeconds_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long DelayInSeconds {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='getDelayInSeconds' and count(parameter)=0]"
			[Register ("getDelayInSeconds", "()Ljava/lang/Long;", "GetGetDelayInSecondsHandler")]
			get {
				if (id_getDelayInSeconds == IntPtr.Zero)
					id_getDelayInSeconds = JNIEnv.GetMethodID (class_ref, "getDelayInSeconds", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getDelayInSeconds), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDelayInSeconds", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setDelayInSeconds' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setDelayInSeconds", "(Ljava/lang/Long;)V", "GetSetDelayInSeconds_Ljava_lang_Long_Handler")]
			set {
				if (id_setDelayInSeconds_Ljava_lang_Long_ == IntPtr.Zero)
					id_setDelayInSeconds_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setDelayInSeconds", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDelayInSeconds_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDelayInSeconds", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDescription;
#pragma warning disable 0169
		static Delegate GetGetDescriptionHandler ()
		{
			if (cb_getDescription == null)
				cb_getDescription = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDescription);
			return cb_getDescription;
		}

		static IntPtr n_GetDescription (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Description);
		}
#pragma warning restore 0169

		static Delegate cb_setDescription_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetDescription_Ljava_lang_String_Handler ()
		{
			if (cb_setDescription_Ljava_lang_String_ == null)
				cb_setDescription_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDescription_Ljava_lang_String_);
			return cb_setDescription_Ljava_lang_String_;
		}

		static void n_SetDescription_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Description = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDescription;
		static IntPtr id_setDescription_Ljava_lang_String_;
		public virtual unsafe string Description {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='getDescription' and count(parameter)=0]"
			[Register ("getDescription", "()Ljava/lang/String;", "GetGetDescriptionHandler")]
			get {
				if (id_getDescription == IntPtr.Zero)
					id_getDescription = JNIEnv.GetMethodID (class_ref, "getDescription", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getDescription), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDescription", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setDescription' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setDescription", "(Ljava/lang/String;)V", "GetSetDescription_Ljava_lang_String_Handler")]
			set {
				if (id_setDescription_Ljava_lang_String_ == IntPtr.Zero)
					id_setDescription_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setDescription", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDescription_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDescription", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getExpires;
#pragma warning disable 0169
		static Delegate GetGetExpiresHandler ()
		{
			if (cb_getExpires == null)
				cb_getExpires = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetExpires);
			return cb_getExpires;
		}

		static IntPtr n_GetExpires (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Expires);
		}
#pragma warning restore 0169

		static Delegate cb_setExpires_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetExpires_Ljava_lang_Long_Handler ()
		{
			if (cb_setExpires_Ljava_lang_Long_ == null)
				cb_setExpires_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetExpires_Ljava_lang_Long_);
			return cb_setExpires_Ljava_lang_Long_;
		}

		static void n_SetExpires_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Expires = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getExpires;
		static IntPtr id_setExpires_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long Expires {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='getExpires' and count(parameter)=0]"
			[Register ("getExpires", "()Ljava/lang/Long;", "GetGetExpiresHandler")]
			get {
				if (id_getExpires == IntPtr.Zero)
					id_getExpires = JNIEnv.GetMethodID (class_ref, "getExpires", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getExpires), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getExpires", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setExpires' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setExpires", "(Ljava/lang/Long;)V", "GetSetExpires_Ljava_lang_Long_Handler")]
			set {
				if (id_setExpires_Ljava_lang_Long_ == IntPtr.Zero)
					id_setExpires_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setExpires", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setExpires_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setExpires", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getFrequencyLimitInHours;
#pragma warning disable 0169
		static Delegate GetGetFrequencyLimitInHoursHandler ()
		{
			if (cb_getFrequencyLimitInHours == null)
				cb_getFrequencyLimitInHours = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetFrequencyLimitInHours);
			return cb_getFrequencyLimitInHours;
		}

		static IntPtr n_GetFrequencyLimitInHours (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.FrequencyLimitInHours);
		}
#pragma warning restore 0169

		static Delegate cb_setFrequencyLimitInHours_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetFrequencyLimitInHours_Ljava_lang_Long_Handler ()
		{
			if (cb_setFrequencyLimitInHours_Ljava_lang_Long_ == null)
				cb_setFrequencyLimitInHours_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetFrequencyLimitInHours_Ljava_lang_Long_);
			return cb_setFrequencyLimitInHours_Ljava_lang_Long_;
		}

		static void n_SetFrequencyLimitInHours_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.FrequencyLimitInHours = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getFrequencyLimitInHours;
		static IntPtr id_setFrequencyLimitInHours_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long FrequencyLimitInHours {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='getFrequencyLimitInHours' and count(parameter)=0]"
			[Register ("getFrequencyLimitInHours", "()Ljava/lang/Long;", "GetGetFrequencyLimitInHoursHandler")]
			get {
				if (id_getFrequencyLimitInHours == IntPtr.Zero)
					id_getFrequencyLimitInHours = JNIEnv.GetMethodID (class_ref, "getFrequencyLimitInHours", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getFrequencyLimitInHours), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getFrequencyLimitInHours", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setFrequencyLimitInHours' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setFrequencyLimitInHours", "(Ljava/lang/Long;)V", "GetSetFrequencyLimitInHours_Ljava_lang_Long_Handler")]
			set {
				if (id_setFrequencyLimitInHours_Ljava_lang_Long_ == IntPtr.Zero)
					id_setFrequencyLimitInHours_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setFrequencyLimitInHours", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setFrequencyLimitInHours_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setFrequencyLimitInHours", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getIconUrl;
#pragma warning disable 0169
		static Delegate GetGetIconUrlHandler ()
		{
			if (cb_getIconUrl == null)
				cb_getIconUrl = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIconUrl);
			return cb_getIconUrl;
		}

		static IntPtr n_GetIconUrl (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.IconUrl);
		}
#pragma warning restore 0169

		static Delegate cb_setIconUrl_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetIconUrl_Ljava_lang_String_Handler ()
		{
			if (cb_setIconUrl_Ljava_lang_String_ == null)
				cb_setIconUrl_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetIconUrl_Ljava_lang_String_);
			return cb_setIconUrl_Ljava_lang_String_;
		}

		static void n_SetIconUrl_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.IconUrl = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getIconUrl;
		static IntPtr id_setIconUrl_Ljava_lang_String_;
		public virtual unsafe string IconUrl {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='getIconUrl' and count(parameter)=0]"
			[Register ("getIconUrl", "()Ljava/lang/String;", "GetGetIconUrlHandler")]
			get {
				if (id_getIconUrl == IntPtr.Zero)
					id_getIconUrl = JNIEnv.GetMethodID (class_ref, "getIconUrl", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getIconUrl), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getIconUrl", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setIconUrl' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setIconUrl", "(Ljava/lang/String;)V", "GetSetIconUrl_Ljava_lang_String_Handler")]
			set {
				if (id_setIconUrl_Ljava_lang_String_ == IntPtr.Zero)
					id_setIconUrl_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setIconUrl", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setIconUrl_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setIconUrl", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getLastTriggerTime;
#pragma warning disable 0169
		static Delegate GetGetLastTriggerTimeHandler ()
		{
			if (cb_getLastTriggerTime == null)
				cb_getLastTriggerTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLastTriggerTime);
			return cb_getLastTriggerTime;
		}

		static IntPtr n_GetLastTriggerTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.LastTriggerTime);
		}
#pragma warning restore 0169

		static Delegate cb_setLastTriggerTime_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetLastTriggerTime_Ljava_lang_Long_Handler ()
		{
			if (cb_setLastTriggerTime_Ljava_lang_Long_ == null)
				cb_setLastTriggerTime_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLastTriggerTime_Ljava_lang_Long_);
			return cb_setLastTriggerTime_Ljava_lang_Long_;
		}

		static void n_SetLastTriggerTime_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.LastTriggerTime = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLastTriggerTime;
		static IntPtr id_setLastTriggerTime_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long LastTriggerTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='getLastTriggerTime' and count(parameter)=0]"
			[Register ("getLastTriggerTime", "()Ljava/lang/Long;", "GetGetLastTriggerTimeHandler")]
			get {
				if (id_getLastTriggerTime == IntPtr.Zero)
					id_getLastTriggerTime = JNIEnv.GetMethodID (class_ref, "getLastTriggerTime", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getLastTriggerTime), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLastTriggerTime", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setLastTriggerTime' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setLastTriggerTime", "(Ljava/lang/Long;)V", "GetSetLastTriggerTime_Ljava_lang_Long_Handler")]
			set {
				if (id_setLastTriggerTime_Ljava_lang_Long_ == IntPtr.Zero)
					id_setLastTriggerTime_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setLastTriggerTime", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLastTriggerTime_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLastTriggerTime", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_isNotifyUser;
#pragma warning disable 0169
		static Delegate GetIsNotifyUserHandler ()
		{
			if (cb_isNotifyUser == null)
				cb_isNotifyUser = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsNotifyUser);
			return cb_isNotifyUser;
		}

		static bool n_IsNotifyUser (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.NotifyUser;
		}
#pragma warning restore 0169

		static Delegate cb_setNotifyUser_Z;
#pragma warning disable 0169
		static Delegate GetSetNotifyUser_ZHandler ()
		{
			if (cb_setNotifyUser_Z == null)
				cb_setNotifyUser_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetNotifyUser_Z);
			return cb_setNotifyUser_Z;
		}

		static void n_SetNotifyUser_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.NotifyUser = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isNotifyUser;
		static IntPtr id_setNotifyUser_Z;
		public virtual unsafe bool NotifyUser {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='isNotifyUser' and count(parameter)=0]"
			[Register ("isNotifyUser", "()Z", "GetIsNotifyUserHandler")]
			get {
				if (id_isNotifyUser == IntPtr.Zero)
					id_isNotifyUser = JNIEnv.GetMethodID (class_ref, "isNotifyUser", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isNotifyUser);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isNotifyUser", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setNotifyUser' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setNotifyUser", "(Z)V", "GetSetNotifyUser_ZHandler")]
			set {
				if (id_setNotifyUser_Z == IntPtr.Zero)
					id_setNotifyUser_Z = JNIEnv.GetMethodID (class_ref, "setNotifyUser", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setNotifyUser_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setNotifyUser", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getTitle;
#pragma warning disable 0169
		static Delegate GetGetTitleHandler ()
		{
			if (cb_getTitle == null)
				cb_getTitle = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTitle);
			return cb_getTitle;
		}

		static IntPtr n_GetTitle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Title);
		}
#pragma warning restore 0169

		static Delegate cb_setTitle_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetTitle_Ljava_lang_String_Handler ()
		{
			if (cb_setTitle_Ljava_lang_String_ == null)
				cb_setTitle_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTitle_Ljava_lang_String_);
			return cb_setTitle_Ljava_lang_String_;
		}

		static void n_SetTitle_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Title = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTitle;
		static IntPtr id_setTitle_Ljava_lang_String_;
		public virtual unsafe string Title {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='getTitle' and count(parameter)=0]"
			[Register ("getTitle", "()Ljava/lang/String;", "GetGetTitleHandler")]
			get {
				if (id_getTitle == IntPtr.Zero)
					id_getTitle = JNIEnv.GetMethodID (class_ref, "getTitle", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getTitle), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTitle", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setTitle' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setTitle", "(Ljava/lang/String;)V", "GetSetTitle_Ljava_lang_String_Handler")]
			set {
				if (id_setTitle_Ljava_lang_String_ == IntPtr.Zero)
					id_setTitle_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setTitle", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTitle_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTitle", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		static Delegate cb_setUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setUuid_Ljava_lang_String_ == null)
				cb_setUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUuid_Ljava_lang_String_);
			return cb_setUuid_Ljava_lang_String_;
		}

		static void n_SetUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Uuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUuid;
		static IntPtr id_setUuid_Ljava_lang_String_;
		public virtual unsafe string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler")]
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentDescriptor']/method[@name='setUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUuid", "(Ljava/lang/String;)V", "GetSetUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
