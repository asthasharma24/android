using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Json {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapperBase']"
	[global::Android.Runtime.Register ("com/gimbal/internal/json/JsonMapperBase", DoNotGenerateAcw=true)]
	public abstract partial class JsonMapperBase : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/json/JsonMapperBase", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (JsonMapperBase); }
		}

		protected JsonMapperBase (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Z;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapperBase']/constructor[@name='JsonMapperBase' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register (".ctor", "(Z)V", "")]
		protected unsafe JsonMapperBase (bool p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (JsonMapperBase)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Z)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Z)V", __args);
					return;
				}

				if (id_ctor_Z == IntPtr.Zero)
					id_ctor_Z = JNIEnv.GetMethodID (class_ref, "<init>", "(Z)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Z, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Z, __args);
			} finally {
			}
		}

		static IntPtr id_a_Ljava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapperBase']/method[@name='a' and count(parameter)=1 and parameter[1][@type='java.lang.Class&lt;X&gt;']]"
		[Register ("a", "(Ljava/lang/Class;)V", "")]
		[global::Java.Interop.JavaTypeParameters (new string [] {"X"})]
		protected unsafe void A (global::Java.Lang.Class p0)
		{
			if (id_a_Ljava_lang_Class_ == IntPtr.Zero)
				id_a_Ljava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "a", "(Ljava/lang/Class;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod  (Handle, id_a_Ljava_lang_Class_, __args);
			} finally {
			}
		}

		static IntPtr id_a_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapperBase']/method[@name='a' and count(parameter)=1 and parameter[1][@type='java.lang.Object']]"
		[Register ("a", "(Ljava/lang/Object;)V", "")]
		protected unsafe void A (global::Java.Lang.Object p0)
		{
			if (id_a_Ljava_lang_Object_ == IntPtr.Zero)
				id_a_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "a", "(Ljava/lang/Object;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				JNIEnv.CallVoidMethod  (Handle, id_a_Ljava_lang_Object_, __args);
			} finally {
			}
		}

		static Delegate cb_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_;
#pragma warning disable 0169
		static Delegate GetAddClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_Handler ()
		{
			if (cb_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_ == null)
				cb_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_AddClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_);
			return cb_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_;
		}

		static void n_AddClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.JsonMapperBase __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonMapperBase> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Internal.Json.ClassHandler p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.ClassHandler> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class[] p1 = (global::Java.Lang.Class[]) JNIEnv.GetArray (native_p1, JniHandleOwnership.DoNotTransfer, typeof (global::Java.Lang.Class));
			__this.AddClassHandler (p0, p1);
			if (p1 != null)
				JNIEnv.CopyArray (p1, native_p1);
		}
#pragma warning restore 0169

		static IntPtr id_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapperBase']/method[@name='addClassHandler' and count(parameter)=2 and parameter[1][@type='com.gimbal.internal.json.ClassHandler'] and parameter[2][@type='java.lang.Class&lt;?&gt;...']]"
		[Register ("addClassHandler", "(Lcom/gimbal/internal/json/ClassHandler;[Ljava/lang/Class;)V", "GetAddClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_Handler")]
		public virtual unsafe void AddClassHandler (global::Com.Gimbal.Internal.Json.ClassHandler p0, params global:: Java.Lang.Class[] p1)
		{
			if (id_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_ == IntPtr.Zero)
				id_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "addClassHandler", "(Lcom/gimbal/internal/json/ClassHandler;[Ljava/lang/Class;)V");
			IntPtr native_p1 = JNIEnv.NewArray (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addClassHandler_Lcom_gimbal_internal_json_ClassHandler_arrayLjava_lang_Class_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addClassHandler", "(Lcom/gimbal/internal/json/ClassHandler;[Ljava/lang/Class;)V"), __args);
			} finally {
				if (p1 != null) {
					JNIEnv.CopyArray (native_p1, p1);
					JNIEnv.DeleteLocalRef (native_p1);
				}
			}
		}

		static Delegate cb_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_;
#pragma warning disable 0169
		static Delegate GetAddPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_Handler ()
		{
			if (cb_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_ == null)
				cb_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_AddPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_);
			return cb_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_;
		}

		static void n_AddPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.JsonMapperBase __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonMapperBase> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Internal.Json.PropertyNameMapper p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.PropertyNameMapper> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class[] p1 = (global::Java.Lang.Class[]) JNIEnv.GetArray (native_p1, JniHandleOwnership.DoNotTransfer, typeof (global::Java.Lang.Class));
			__this.AddPropertyNameMapper (p0, p1);
			if (p1 != null)
				JNIEnv.CopyArray (p1, native_p1);
		}
#pragma warning restore 0169

		static IntPtr id_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapperBase']/method[@name='addPropertyNameMapper' and count(parameter)=2 and parameter[1][@type='com.gimbal.internal.json.PropertyNameMapper'] and parameter[2][@type='java.lang.Class&lt;?&gt;...']]"
		[Register ("addPropertyNameMapper", "(Lcom/gimbal/internal/json/PropertyNameMapper;[Ljava/lang/Class;)V", "GetAddPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_Handler")]
		public virtual unsafe void AddPropertyNameMapper (global::Com.Gimbal.Internal.Json.PropertyNameMapper p0, params global:: Java.Lang.Class[] p1)
		{
			if (id_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_ == IntPtr.Zero)
				id_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "addPropertyNameMapper", "(Lcom/gimbal/internal/json/PropertyNameMapper;[Ljava/lang/Class;)V");
			IntPtr native_p1 = JNIEnv.NewArray (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addPropertyNameMapper_Lcom_gimbal_internal_json_PropertyNameMapper_arrayLjava_lang_Class_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addPropertyNameMapper", "(Lcom/gimbal/internal/json/PropertyNameMapper;[Ljava/lang/Class;)V"), __args);
			} finally {
				if (p1 != null) {
					JNIEnv.CopyArray (native_p1, p1);
					JNIEnv.DeleteLocalRef (native_p1);
				}
			}
		}

		static IntPtr id_b_Ljava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapperBase']/method[@name='b' and count(parameter)=1 and parameter[1][@type='java.lang.Class&lt;?&gt;']]"
		[Register ("b", "(Ljava/lang/Class;)Z", "")]
		protected unsafe bool B (global::Java.Lang.Class p0)
		{
			if (id_b_Ljava_lang_Class_ == IntPtr.Zero)
				id_b_Ljava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "b", "(Ljava/lang/Class;)Z");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				bool __ret = JNIEnv.CallBooleanMethod  (Handle, id_b_Ljava_lang_Class_, __args);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_b_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapperBase']/method[@name='b' and count(parameter)=1 and parameter[1][@type='java.lang.Object']]"
		[Register ("b", "(Ljava/lang/Object;)Ljava/lang/String;", "")]
		protected static unsafe string B (global::Java.Lang.Object p0)
		{
			if (id_b_Ljava_lang_Object_ == IntPtr.Zero)
				id_b_Ljava_lang_Object_ = JNIEnv.GetStaticMethodID (class_ref, "b", "(Ljava/lang/Object;)Ljava/lang/String;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				string __ret = JNIEnv.GetString (JNIEnv.CallStaticObjectMethod  (class_ref, id_b_Ljava_lang_Object_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

		static IntPtr id_c_Ljava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonMapperBase']/method[@name='c' and count(parameter)=1 and parameter[1][@type='java.lang.Class&lt;?&gt;']]"
		[Register ("c", "(Ljava/lang/Class;)Ljava/util/List;", "")]
		protected static unsafe global::System.Collections.Generic.IList<global::Java.Lang.Reflect.Method> C (global::Java.Lang.Class p0)
		{
			if (id_c_Ljava_lang_Class_ == IntPtr.Zero)
				id_c_Ljava_lang_Class_ = JNIEnv.GetStaticMethodID (class_ref, "c", "(Ljava/lang/Class;)Ljava/util/List;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				global::System.Collections.Generic.IList<global::Java.Lang.Reflect.Method> __ret = global::Android.Runtime.JavaList<global::Java.Lang.Reflect.Method>.FromJniHandle (JNIEnv.CallStaticObjectMethod  (class_ref, id_c_Ljava_lang_Class_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

	}

	[global::Android.Runtime.Register ("com/gimbal/internal/json/JsonMapperBase", DoNotGenerateAcw=true)]
	internal partial class JsonMapperBaseInvoker : JsonMapperBase {

		public JsonMapperBaseInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (JsonMapperBaseInvoker); }
		}

	}

}
