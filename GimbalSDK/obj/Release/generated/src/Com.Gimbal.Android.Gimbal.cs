using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='Gimbal']"
	[global::Android.Runtime.Register ("com/gimbal/android/Gimbal", DoNotGenerateAcw=true)]
	public partial class Gimbal : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/Gimbal", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Gimbal); }
		}

		protected Gimbal (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android']/class[@name='Gimbal']/constructor[@name='Gimbal' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Gimbal ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Gimbal)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static IntPtr id_getApplicationInstanceIdentifier;
		public static unsafe string ApplicationInstanceIdentifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Gimbal']/method[@name='getApplicationInstanceIdentifier' and count(parameter)=0]"
			[Register ("getApplicationInstanceIdentifier", "()Ljava/lang/String;", "GetGetApplicationInstanceIdentifierHandler")]
			get {
				if (id_getApplicationInstanceIdentifier == IntPtr.Zero)
					id_getApplicationInstanceIdentifier = JNIEnv.GetStaticMethodID (class_ref, "getApplicationInstanceIdentifier", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallStaticObjectMethod  (class_ref, id_getApplicationInstanceIdentifier), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_registerForPush_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Gimbal']/method[@name='registerForPush' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("registerForPush", "(Ljava/lang/String;)V", "")]
		public static unsafe void RegisterForPush (string p0)
		{
			if (id_registerForPush_Ljava_lang_String_ == IntPtr.Zero)
				id_registerForPush_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "registerForPush", "(Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_registerForPush_Ljava_lang_String_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_resetApplicationInstanceIdentifier;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Gimbal']/method[@name='resetApplicationInstanceIdentifier' and count(parameter)=0]"
		[Register ("resetApplicationInstanceIdentifier", "()V", "")]
		public static unsafe void ResetApplicationInstanceIdentifier ()
		{
			if (id_resetApplicationInstanceIdentifier == IntPtr.Zero)
				id_resetApplicationInstanceIdentifier = JNIEnv.GetStaticMethodID (class_ref, "resetApplicationInstanceIdentifier", "()V");
			try {
				JNIEnv.CallStaticVoidMethod  (class_ref, id_resetApplicationInstanceIdentifier);
			} finally {
			}
		}

		static IntPtr id_setApiKey_Landroid_app_Application_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Gimbal']/method[@name='setApiKey' and count(parameter)=2 and parameter[1][@type='android.app.Application'] and parameter[2][@type='java.lang.String']]"
		[Register ("setApiKey", "(Landroid/app/Application;Ljava/lang/String;)V", "")]
		public static unsafe void SetApiKey (global::Android.App.Application p0, string p1)
		{
			if (id_setApiKey_Landroid_app_Application_Ljava_lang_String_ == IntPtr.Zero)
				id_setApiKey_Landroid_app_Application_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "setApiKey", "(Landroid/app/Application;Ljava/lang/String;)V");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_setApiKey_Landroid_app_Application_Ljava_lang_String_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static IntPtr id_setApiKeyWithOptions_Landroid_app_Application_Ljava_lang_String_Ljava_util_Map_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='Gimbal']/method[@name='setApiKeyWithOptions' and count(parameter)=3 and parameter[1][@type='android.app.Application'] and parameter[2][@type='java.lang.String'] and parameter[3][@type='java.util.Map&lt;java.lang.String, java.lang.String&gt;']]"
		[Register ("setApiKeyWithOptions", "(Landroid/app/Application;Ljava/lang/String;Ljava/util/Map;)V", "")]
		public static unsafe void SetApiKeyWithOptions (global::Android.App.Application p0, string p1, global::System.Collections.Generic.IDictionary<string, string> p2)
		{
			if (id_setApiKeyWithOptions_Landroid_app_Application_Ljava_lang_String_Ljava_util_Map_ == IntPtr.Zero)
				id_setApiKeyWithOptions_Landroid_app_Application_Ljava_lang_String_Ljava_util_Map_ = JNIEnv.GetStaticMethodID (class_ref, "setApiKeyWithOptions", "(Landroid/app/Application;Ljava/lang/String;Ljava/util/Map;)V");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			IntPtr native_p2 = global::Android.Runtime.JavaDictionary<string, string>.ToLocalJniHandle (p2);
			try {
				JValue* __args = stackalloc JValue [3];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				__args [2] = new JValue (native_p2);
				JNIEnv.CallStaticVoidMethod  (class_ref, id_setApiKeyWithOptions_Landroid_app_Application_Ljava_lang_String_Ljava_util_Map_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
				JNIEnv.DeleteLocalRef (native_p2);
			}
		}

	}
}
