using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='Transmitter']"
	[Register ("com/gimbal/proximity/Transmitter", "", "Com.Gimbal.Proximity.ITransmitterInvoker")]
	public partial interface ITransmitter : IJavaObject {

		int Battery {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='Transmitter']/method[@name='getBattery' and count(parameter)=0]"
			[Register ("getBattery", "()I", "GetGetBatteryHandler:Com.Gimbal.Proximity.ITransmitterInvoker, GimbalSDK")] get;
		}

		string IconUrl {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='Transmitter']/method[@name='getIconUrl' and count(parameter)=0]"
			[Register ("getIconUrl", "()Ljava/lang/String;", "GetGetIconUrlHandler:Com.Gimbal.Proximity.ITransmitterInvoker, GimbalSDK")] get;
		}

		string Identifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='Transmitter']/method[@name='getIdentifier' and count(parameter)=0]"
			[Register ("getIdentifier", "()Ljava/lang/String;", "GetGetIdentifierHandler:Com.Gimbal.Proximity.ITransmitterInvoker, GimbalSDK")] get;
		}

		string Name {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='Transmitter']/method[@name='getName' and count(parameter)=0]"
			[Register ("getName", "()Ljava/lang/String;", "GetGetNameHandler:Com.Gimbal.Proximity.ITransmitterInvoker, GimbalSDK")] get;
		}

		string OwnerId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='Transmitter']/method[@name='getOwnerId' and count(parameter)=0]"
			[Register ("getOwnerId", "()Ljava/lang/String;", "GetGetOwnerIdHandler:Com.Gimbal.Proximity.ITransmitterInvoker, GimbalSDK")] get;
		}

		int Temperature {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='Transmitter']/method[@name='getTemperature' and count(parameter)=0]"
			[Register ("getTemperature", "()I", "GetGetTemperatureHandler:Com.Gimbal.Proximity.ITransmitterInvoker, GimbalSDK")] get;
		}

	}

	[global::Android.Runtime.Register ("com/gimbal/proximity/Transmitter", DoNotGenerateAcw=true)]
	internal class ITransmitterInvoker : global::Java.Lang.Object, ITransmitter {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/gimbal/proximity/Transmitter");
		IntPtr class_ref;

		public static ITransmitter GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<ITransmitter> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.gimbal.proximity.Transmitter"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public ITransmitterInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ITransmitterInvoker); }
		}

		static Delegate cb_getBattery;
#pragma warning disable 0169
		static Delegate GetGetBatteryHandler ()
		{
			if (cb_getBattery == null)
				cb_getBattery = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetBattery);
			return cb_getBattery;
		}

		static int n_GetBattery (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.ITransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ITransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Battery;
		}
#pragma warning restore 0169

		IntPtr id_getBattery;
		public unsafe int Battery {
			get {
				if (id_getBattery == IntPtr.Zero)
					id_getBattery = JNIEnv.GetMethodID (class_ref, "getBattery", "()I");
				return JNIEnv.CallIntMethod (Handle, id_getBattery);
			}
		}

		static Delegate cb_getIconUrl;
#pragma warning disable 0169
		static Delegate GetGetIconUrlHandler ()
		{
			if (cb_getIconUrl == null)
				cb_getIconUrl = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIconUrl);
			return cb_getIconUrl;
		}

		static IntPtr n_GetIconUrl (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.ITransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ITransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.IconUrl);
		}
#pragma warning restore 0169

		IntPtr id_getIconUrl;
		public unsafe string IconUrl {
			get {
				if (id_getIconUrl == IntPtr.Zero)
					id_getIconUrl = JNIEnv.GetMethodID (class_ref, "getIconUrl", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (Handle, id_getIconUrl), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getIdentifier;
#pragma warning disable 0169
		static Delegate GetGetIdentifierHandler ()
		{
			if (cb_getIdentifier == null)
				cb_getIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetIdentifier);
			return cb_getIdentifier;
		}

		static IntPtr n_GetIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.ITransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ITransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Identifier);
		}
#pragma warning restore 0169

		IntPtr id_getIdentifier;
		public unsafe string Identifier {
			get {
				if (id_getIdentifier == IntPtr.Zero)
					id_getIdentifier = JNIEnv.GetMethodID (class_ref, "getIdentifier", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (Handle, id_getIdentifier), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getName;
#pragma warning disable 0169
		static Delegate GetGetNameHandler ()
		{
			if (cb_getName == null)
				cb_getName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetName);
			return cb_getName;
		}

		static IntPtr n_GetName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.ITransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ITransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Name);
		}
#pragma warning restore 0169

		IntPtr id_getName;
		public unsafe string Name {
			get {
				if (id_getName == IntPtr.Zero)
					id_getName = JNIEnv.GetMethodID (class_ref, "getName", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (Handle, id_getName), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getOwnerId;
#pragma warning disable 0169
		static Delegate GetGetOwnerIdHandler ()
		{
			if (cb_getOwnerId == null)
				cb_getOwnerId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOwnerId);
			return cb_getOwnerId;
		}

		static IntPtr n_GetOwnerId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.ITransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ITransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.OwnerId);
		}
#pragma warning restore 0169

		IntPtr id_getOwnerId;
		public unsafe string OwnerId {
			get {
				if (id_getOwnerId == IntPtr.Zero)
					id_getOwnerId = JNIEnv.GetMethodID (class_ref, "getOwnerId", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (Handle, id_getOwnerId), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getTemperature;
#pragma warning disable 0169
		static Delegate GetGetTemperatureHandler ()
		{
			if (cb_getTemperature == null)
				cb_getTemperature = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetTemperature);
			return cb_getTemperature;
		}

		static int n_GetTemperature (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.ITransmitter __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ITransmitter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Temperature;
		}
#pragma warning restore 0169

		IntPtr id_getTemperature;
		public unsafe int Temperature {
			get {
				if (id_getTemperature == IntPtr.Zero)
					id_getTemperature = JNIEnv.GetMethodID (class_ref, "getTemperature", "()I");
				return JNIEnv.CallIntMethod (Handle, id_getTemperature);
			}
		}

	}

}
