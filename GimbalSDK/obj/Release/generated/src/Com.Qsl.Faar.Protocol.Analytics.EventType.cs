using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Analytics {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/EventType", DoNotGenerateAcw=true)]
	public partial class EventType : global::Java.Lang.Object {

		// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.AR']"
		[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/EventType$AR", DoNotGenerateAcw=true)]
		public sealed partial class AR : global::Java.Lang.Enum {


			static IntPtr ASSET_CLICKED_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.AR']/field[@name='ASSET_CLICKED']"
			[Register ("ASSET_CLICKED")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.AR AssetClicked {
				get {
					if (ASSET_CLICKED_jfieldId == IntPtr.Zero)
						ASSET_CLICKED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "ASSET_CLICKED", "Lcom/qsl/faar/protocol/analytics/EventType$AR;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, ASSET_CLICKED_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.AR> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr TARGET_ACQUIRED_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.AR']/field[@name='TARGET_ACQUIRED']"
			[Register ("TARGET_ACQUIRED")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.AR TargetAcquired {
				get {
					if (TARGET_ACQUIRED_jfieldId == IntPtr.Zero)
						TARGET_ACQUIRED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "TARGET_ACQUIRED", "Lcom/qsl/faar/protocol/analytics/EventType$AR;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, TARGET_ACQUIRED_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.AR> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/EventType$AR", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (AR); }
			}

			internal AR (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.AR']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/EventType$AR;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.EventType.AR ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/EventType$AR;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Qsl.Faar.Protocol.Analytics.EventType.AR __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.AR> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.AR']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/qsl/faar/protocol/analytics/EventType$AR;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.EventType.AR[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/analytics/EventType$AR;");
				try {
					return (global::Com.Qsl.Faar.Protocol.Analytics.EventType.AR[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.Analytics.EventType.AR));
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.CONTENT']"
		[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/EventType$CONTENT", DoNotGenerateAcw=true)]
		public sealed partial class CONTENT : global::Java.Lang.Enum {


			static IntPtr CONTENT_CLICKED_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.CONTENT']/field[@name='CONTENT_CLICKED']"
			[Register ("CONTENT_CLICKED")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT ContentClicked {
				get {
					if (CONTENT_CLICKED_jfieldId == IntPtr.Zero)
						CONTENT_CLICKED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CONTENT_CLICKED", "Lcom/qsl/faar/protocol/analytics/EventType$CONTENT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CONTENT_CLICKED_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr CONTENT_DELIVERED_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.CONTENT']/field[@name='CONTENT_DELIVERED']"
			[Register ("CONTENT_DELIVERED")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT ContentDelivered {
				get {
					if (CONTENT_DELIVERED_jfieldId == IntPtr.Zero)
						CONTENT_DELIVERED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CONTENT_DELIVERED", "Lcom/qsl/faar/protocol/analytics/EventType$CONTENT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CONTENT_DELIVERED_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr CONTENT_DISPLAYED_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.CONTENT']/field[@name='CONTENT_DISPLAYED']"
			[Register ("CONTENT_DISPLAYED")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT ContentDisplayed {
				get {
					if (CONTENT_DISPLAYED_jfieldId == IntPtr.Zero)
						CONTENT_DISPLAYED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CONTENT_DISPLAYED", "Lcom/qsl/faar/protocol/analytics/EventType$CONTENT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CONTENT_DISPLAYED_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr CONTENT_NOTIFIED_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.CONTENT']/field[@name='CONTENT_NOTIFIED']"
			[Register ("CONTENT_NOTIFIED")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT ContentNotified {
				get {
					if (CONTENT_NOTIFIED_jfieldId == IntPtr.Zero)
						CONTENT_NOTIFIED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "CONTENT_NOTIFIED", "Lcom/qsl/faar/protocol/analytics/EventType$CONTENT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, CONTENT_NOTIFIED_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr EXPERIENCE_DELIVERED_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.CONTENT']/field[@name='EXPERIENCE_DELIVERED']"
			[Register ("EXPERIENCE_DELIVERED")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT ExperienceDelivered {
				get {
					if (EXPERIENCE_DELIVERED_jfieldId == IntPtr.Zero)
						EXPERIENCE_DELIVERED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "EXPERIENCE_DELIVERED", "Lcom/qsl/faar/protocol/analytics/EventType$CONTENT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, EXPERIENCE_DELIVERED_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr EXPERIENCE_DISPLAYED_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.CONTENT']/field[@name='EXPERIENCE_DISPLAYED']"
			[Register ("EXPERIENCE_DISPLAYED")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT ExperienceDisplayed {
				get {
					if (EXPERIENCE_DISPLAYED_jfieldId == IntPtr.Zero)
						EXPERIENCE_DISPLAYED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "EXPERIENCE_DISPLAYED", "Lcom/qsl/faar/protocol/analytics/EventType$CONTENT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, EXPERIENCE_DISPLAYED_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr EXPERIENCE_NOTIFICATION_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.CONTENT']/field[@name='EXPERIENCE_NOTIFICATION']"
			[Register ("EXPERIENCE_NOTIFICATION")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT ExperienceNotification {
				get {
					if (EXPERIENCE_NOTIFICATION_jfieldId == IntPtr.Zero)
						EXPERIENCE_NOTIFICATION_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "EXPERIENCE_NOTIFICATION", "Lcom/qsl/faar/protocol/analytics/EventType$CONTENT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, EXPERIENCE_NOTIFICATION_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/EventType$CONTENT", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (CONTENT); }
			}

			internal CONTENT (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.CONTENT']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/EventType$CONTENT;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/EventType$CONTENT;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.CONTENT']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/qsl/faar/protocol/analytics/EventType$CONTENT;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/analytics/EventType$CONTENT;");
				try {
					return (global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.Analytics.EventType.CONTENT));
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.USER_CONTEXT']"
		[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/EventType$USER_CONTEXT", DoNotGenerateAcw=true)]
		public sealed partial class USER_CONTEXT : global::Java.Lang.Enum {


			static IntPtr IR_DURATION_EVENT_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.USER_CONTEXT']/field[@name='IR_DURATION_EVENT']"
			[Register ("IR_DURATION_EVENT")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT IrDurationEvent {
				get {
					if (IR_DURATION_EVENT_jfieldId == IntPtr.Zero)
						IR_DURATION_EVENT_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "IR_DURATION_EVENT", "Lcom/qsl/faar/protocol/analytics/EventType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, IR_DURATION_EVENT_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr IR_EVENT_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.USER_CONTEXT']/field[@name='IR_EVENT']"
			[Register ("IR_EVENT")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT IrEvent {
				get {
					if (IR_EVENT_jfieldId == IntPtr.Zero)
						IR_EVENT_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "IR_EVENT", "Lcom/qsl/faar/protocol/analytics/EventType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, IR_EVENT_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr IR_LAUNCHED_EVENT_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.USER_CONTEXT']/field[@name='IR_LAUNCHED_EVENT']"
			[Register ("IR_LAUNCHED_EVENT")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT IrLaunchedEvent {
				get {
					if (IR_LAUNCHED_EVENT_jfieldId == IntPtr.Zero)
						IR_LAUNCHED_EVENT_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "IR_LAUNCHED_EVENT", "Lcom/qsl/faar/protocol/analytics/EventType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, IR_LAUNCHED_EVENT_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr IR_TARGET_ACQUIRED_EVENT_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.USER_CONTEXT']/field[@name='IR_TARGET_ACQUIRED_EVENT']"
			[Register ("IR_TARGET_ACQUIRED_EVENT")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT IrTargetAcquiredEvent {
				get {
					if (IR_TARGET_ACQUIRED_EVENT_jfieldId == IntPtr.Zero)
						IR_TARGET_ACQUIRED_EVENT_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "IR_TARGET_ACQUIRED_EVENT", "Lcom/qsl/faar/protocol/analytics/EventType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, IR_TARGET_ACQUIRED_EVENT_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr ORGANIZATION_PLACE_EVENT_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.USER_CONTEXT']/field[@name='ORGANIZATION_PLACE_EVENT']"
			[Register ("ORGANIZATION_PLACE_EVENT")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT OrganizationPlaceEvent {
				get {
					if (ORGANIZATION_PLACE_EVENT_jfieldId == IntPtr.Zero)
						ORGANIZATION_PLACE_EVENT_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "ORGANIZATION_PLACE_EVENT", "Lcom/qsl/faar/protocol/analytics/EventType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, ORGANIZATION_PLACE_EVENT_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr PRIVATE_PLACE_EVENT_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.USER_CONTEXT']/field[@name='PRIVATE_PLACE_EVENT']"
			[Register ("PRIVATE_PLACE_EVENT")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT PrivatePlaceEvent {
				get {
					if (PRIVATE_PLACE_EVENT_jfieldId == IntPtr.Zero)
						PRIVATE_PLACE_EVENT_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PRIVATE_PLACE_EVENT", "Lcom/qsl/faar/protocol/analytics/EventType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PRIVATE_PLACE_EVENT_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr PROFILE_GENERATED_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.USER_CONTEXT']/field[@name='PROFILE_GENERATED']"
			[Register ("PROFILE_GENERATED")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT ProfileGenerated {
				get {
					if (PROFILE_GENERATED_jfieldId == IntPtr.Zero)
						PROFILE_GENERATED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROFILE_GENERATED", "Lcom/qsl/faar/protocol/analytics/EventType$USER_CONTEXT;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROFILE_GENERATED_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/EventType$USER_CONTEXT", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (USER_CONTEXT); }
			}

			internal USER_CONTEXT (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.USER_CONTEXT']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/EventType$USER_CONTEXT;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/EventType$USER_CONTEXT;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.USER_CONTEXT']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/qsl/faar/protocol/analytics/EventType$USER_CONTEXT;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/analytics/EventType$USER_CONTEXT;");
				try {
					return (global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.Analytics.EventType.USER_CONTEXT));
				} finally {
				}
			}

		}

		// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.V2_EVENTS']"
		[global::Android.Runtime.Register ("com/qsl/faar/protocol/analytics/EventType$V2_EVENTS", DoNotGenerateAcw=true)]
		public sealed partial class V2_EVENTS : global::Java.Lang.Enum {


			static IntPtr BEACON_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.V2_EVENTS']/field[@name='BEACON']"
			[Register ("BEACON")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS Beacon {
				get {
					if (BEACON_jfieldId == IntPtr.Zero)
						BEACON_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "BEACON", "Lcom/qsl/faar/protocol/analytics/EventType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, BEACON_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr GEOFENCE_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.V2_EVENTS']/field[@name='GEOFENCE']"
			[Register ("GEOFENCE")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS Geofence {
				get {
					if (GEOFENCE_jfieldId == IntPtr.Zero)
						GEOFENCE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "GEOFENCE", "Lcom/qsl/faar/protocol/analytics/EventType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, GEOFENCE_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr PERMISSIONS_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.V2_EVENTS']/field[@name='PERMISSIONS']"
			[Register ("PERMISSIONS")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS Permissions {
				get {
					if (PERMISSIONS_jfieldId == IntPtr.Zero)
						PERMISSIONS_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PERMISSIONS", "Lcom/qsl/faar/protocol/analytics/EventType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PERMISSIONS_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr PLACE_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.V2_EVENTS']/field[@name='PLACE']"
			[Register ("PLACE")]
			public static global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS Place {
				get {
					if (PLACE_jfieldId == IntPtr.Zero)
						PLACE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PLACE", "Lcom/qsl/faar/protocol/analytics/EventType$V2_EVENTS;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PLACE_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/EventType$V2_EVENTS", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (V2_EVENTS); }
			}

			internal V2_EVENTS (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.V2_EVENTS']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/EventType$V2_EVENTS;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qsl/faar/protocol/analytics/EventType$V2_EVENTS;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS __ret = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType.V2_EVENTS']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/qsl/faar/protocol/analytics/EventType$V2_EVENTS;", "")]
			public static unsafe global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qsl/faar/protocol/analytics/EventType$V2_EVENTS;");
				try {
					return (global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qsl.Faar.Protocol.Analytics.EventType.V2_EVENTS));
				} finally {
				}
			}

		}

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/analytics/EventType", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (EventType); }
		}

		protected EventType (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.analytics']/class[@name='EventType']/constructor[@name='EventType' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe EventType ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (EventType)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

	}
}
