using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Json {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonReader']"
	[global::Android.Runtime.Register ("com/gimbal/internal/json/JsonReader", DoNotGenerateAcw=true)]
	public partial class JsonReader : global::Com.Gimbal.Internal.Json.JsonMapperBase {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/json/JsonReader", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (JsonReader); }
		}

		protected JsonReader (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Z;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonReader']/constructor[@name='JsonReader' and count(parameter)=1 and parameter[1][@type='boolean']]"
		[Register (".ctor", "(Z)V", "")]
		public unsafe JsonReader (bool p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (JsonReader)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Z)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Z)V", __args);
					return;
				}

				if (id_ctor_Z == IntPtr.Zero)
					id_ctor_Z = JNIEnv.GetMethodID (class_ref, "<init>", "(Z)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Z, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Z, __args);
			} finally {
			}
		}

		static Delegate cb_readValue_Ljava_lang_Class_Ljava_io_InputStream_;
#pragma warning disable 0169
		static Delegate GetReadValue_Ljava_lang_Class_Ljava_io_InputStream_Handler ()
		{
			if (cb_readValue_Ljava_lang_Class_Ljava_io_InputStream_ == null)
				cb_readValue_Ljava_lang_Class_Ljava_io_InputStream_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_ReadValue_Ljava_lang_Class_Ljava_io_InputStream_);
			return cb_readValue_Ljava_lang_Class_Ljava_io_InputStream_;
		}

		static IntPtr n_ReadValue_Ljava_lang_Class_Ljava_io_InputStream_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.JsonReader __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonReader> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			System.IO.Stream p1 = global::Android.Runtime.InputStreamInvoker.FromJniHandle (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.ReadValue (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_readValue_Ljava_lang_Class_Ljava_io_InputStream_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonReader']/method[@name='readValue' and count(parameter)=2 and parameter[1][@type='java.lang.Class&lt;X&gt;'] and parameter[2][@type='java.io.InputStream']]"
		[Register ("readValue", "(Ljava/lang/Class;Ljava/io/InputStream;)Ljava/lang/Object;", "GetReadValue_Ljava_lang_Class_Ljava_io_InputStream_Handler")]
		[global::Java.Interop.JavaTypeParameters (new string [] {"X"})]
		public virtual unsafe global::Java.Lang.Object ReadValue (global::Java.Lang.Class p0, global::System.IO.Stream p1)
		{
			if (id_readValue_Ljava_lang_Class_Ljava_io_InputStream_ == IntPtr.Zero)
				id_readValue_Ljava_lang_Class_Ljava_io_InputStream_ = JNIEnv.GetMethodID (class_ref, "readValue", "(Ljava/lang/Class;Ljava/io/InputStream;)Ljava/lang/Object;");
			IntPtr native_p1 = global::Android.Runtime.InputStreamAdapter.ToLocalJniHandle (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				global::Java.Lang.Object __ret;
				if (GetType () == ThresholdType)
					__ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod  (Handle, id_readValue_Ljava_lang_Class_Ljava_io_InputStream_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "readValue", "(Ljava/lang/Class;Ljava/io/InputStream;)Ljava/lang/Object;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static Delegate cb_readValue_Ljava_lang_Class_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetReadValue_Ljava_lang_Class_Ljava_lang_String_Handler ()
		{
			if (cb_readValue_Ljava_lang_Class_Ljava_lang_String_ == null)
				cb_readValue_Ljava_lang_Class_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_ReadValue_Ljava_lang_Class_Ljava_lang_String_);
			return cb_readValue_Ljava_lang_Class_Ljava_lang_String_;
		}

		static IntPtr n_ReadValue_Ljava_lang_Class_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.JsonReader __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.JsonReader> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.ReadValue (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_readValue_Ljava_lang_Class_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='JsonReader']/method[@name='readValue' and count(parameter)=2 and parameter[1][@type='java.lang.Class&lt;X&gt;'] and parameter[2][@type='java.lang.String']]"
		[Register ("readValue", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;", "GetReadValue_Ljava_lang_Class_Ljava_lang_String_Handler")]
		[global::Java.Interop.JavaTypeParameters (new string [] {"X"})]
		public virtual unsafe global::Java.Lang.Object ReadValue (global::Java.Lang.Class p0, string p1)
		{
			if (id_readValue_Ljava_lang_Class_Ljava_lang_String_ == IntPtr.Zero)
				id_readValue_Ljava_lang_Class_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "readValue", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				global::Java.Lang.Object __ret;
				if (GetType () == ThresholdType)
					__ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallObjectMethod  (Handle, id_readValue_Ljava_lang_Class_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = (Java.Lang.Object) global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "readValue", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

	}
}
