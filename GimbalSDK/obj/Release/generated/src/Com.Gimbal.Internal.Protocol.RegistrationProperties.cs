using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']"
	[global::Android.Runtime.Register ("com/gimbal/internal/protocol/RegistrationProperties", DoNotGenerateAcw=true)]
	public partial class RegistrationProperties : global::Java.Lang.Object, global::Java.IO.ISerializable {

		// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties.RegistrationState']"
		[global::Android.Runtime.Register ("com/gimbal/internal/protocol/RegistrationProperties$RegistrationState", DoNotGenerateAcw=true)]
		public sealed partial class RegistrationState : global::Java.Lang.Enum {


			static IntPtr AwaitingRegistration_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties.RegistrationState']/field[@name='AwaitingRegistration']"
			[Register ("AwaitingRegistration")]
			public static global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState AwaitingRegistration {
				get {
					if (AwaitingRegistration_jfieldId == IntPtr.Zero)
						AwaitingRegistration_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "AwaitingRegistration", "Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, AwaitingRegistration_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr AwaitingReset_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties.RegistrationState']/field[@name='AwaitingReset']"
			[Register ("AwaitingReset")]
			public static global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState AwaitingReset {
				get {
					if (AwaitingReset_jfieldId == IntPtr.Zero)
						AwaitingReset_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "AwaitingReset", "Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, AwaitingReset_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr AwaitingUpdate_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties.RegistrationState']/field[@name='AwaitingUpdate']"
			[Register ("AwaitingUpdate")]
			public static global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState AwaitingUpdate {
				get {
					if (AwaitingUpdate_jfieldId == IntPtr.Zero)
						AwaitingUpdate_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "AwaitingUpdate", "Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, AwaitingUpdate_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr None_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties.RegistrationState']/field[@name='None']"
			[Register ("None")]
			public static global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState None {
				get {
					if (None_jfieldId == IntPtr.Zero)
						None_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "None", "Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, None_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr Registered_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties.RegistrationState']/field[@name='Registered']"
			[Register ("Registered")]
			public static global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState Registered {
				get {
					if (Registered_jfieldId == IntPtr.Zero)
						Registered_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "Registered", "Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, Registered_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/gimbal/internal/protocol/RegistrationProperties$RegistrationState", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (RegistrationState); }
			}

			internal RegistrationState (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties.RegistrationState']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;", "")]
			public static unsafe global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState __ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties.RegistrationState']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;", "")]
			public static unsafe global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;");
				try {
					return (global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState));
				} finally {
				}
			}

		}

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/protocol/RegistrationProperties", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (RegistrationProperties); }
		}

		protected RegistrationProperties (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/constructor[@name='RegistrationProperties' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe RegistrationProperties ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (RegistrationProperties)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getApiKey;
#pragma warning disable 0169
		static Delegate GetGetApiKeyHandler ()
		{
			if (cb_getApiKey == null)
				cb_getApiKey = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApiKey);
			return cb_getApiKey;
		}

		static IntPtr n_GetApiKey (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ApiKey);
		}
#pragma warning restore 0169

		static Delegate cb_setApiKey_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetApiKey_Ljava_lang_String_Handler ()
		{
			if (cb_setApiKey_Ljava_lang_String_ == null)
				cb_setApiKey_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApiKey_Ljava_lang_String_);
			return cb_setApiKey_Ljava_lang_String_;
		}

		static void n_SetApiKey_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApiKey = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApiKey;
		static IntPtr id_setApiKey_Ljava_lang_String_;
		public virtual unsafe string ApiKey {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='getApiKey' and count(parameter)=0]"
			[Register ("getApiKey", "()Ljava/lang/String;", "GetGetApiKeyHandler")]
			get {
				if (id_getApiKey == IntPtr.Zero)
					id_getApiKey = JNIEnv.GetMethodID (class_ref, "getApiKey", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getApiKey), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApiKey", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='setApiKey' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setApiKey", "(Ljava/lang/String;)V", "GetSetApiKey_Ljava_lang_String_Handler")]
			set {
				if (id_setApiKey_Ljava_lang_String_ == IntPtr.Zero)
					id_setApiKey_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setApiKey", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApiKey_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApiKey", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getApplicationId;
#pragma warning disable 0169
		static Delegate GetGetApplicationIdHandler ()
		{
			if (cb_getApplicationId == null)
				cb_getApplicationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApplicationId);
			return cb_getApplicationId;
		}

		static IntPtr n_GetApplicationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ApplicationId);
		}
#pragma warning restore 0169

		static Delegate cb_setApplicationId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetApplicationId_Ljava_lang_Long_Handler ()
		{
			if (cb_setApplicationId_Ljava_lang_Long_ == null)
				cb_setApplicationId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApplicationId_Ljava_lang_Long_);
			return cb_setApplicationId_Ljava_lang_Long_;
		}

		static void n_SetApplicationId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApplicationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApplicationId;
		static IntPtr id_setApplicationId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long ApplicationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='getApplicationId' and count(parameter)=0]"
			[Register ("getApplicationId", "()Ljava/lang/Long;", "GetGetApplicationIdHandler")]
			get {
				if (id_getApplicationId == IntPtr.Zero)
					id_getApplicationId = JNIEnv.GetMethodID (class_ref, "getApplicationId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getApplicationId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApplicationId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='setApplicationId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setApplicationId", "(Ljava/lang/Long;)V", "GetSetApplicationId_Ljava_lang_Long_Handler")]
			set {
				if (id_setApplicationId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setApplicationId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setApplicationId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApplicationId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApplicationId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getApplicationIdentifier;
#pragma warning disable 0169
		static Delegate GetGetApplicationIdentifierHandler ()
		{
			if (cb_getApplicationIdentifier == null)
				cb_getApplicationIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApplicationIdentifier);
			return cb_getApplicationIdentifier;
		}

		static IntPtr n_GetApplicationIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ApplicationIdentifier);
		}
#pragma warning restore 0169

		static Delegate cb_setApplicationIdentifier_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetApplicationIdentifier_Ljava_lang_String_Handler ()
		{
			if (cb_setApplicationIdentifier_Ljava_lang_String_ == null)
				cb_setApplicationIdentifier_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApplicationIdentifier_Ljava_lang_String_);
			return cb_setApplicationIdentifier_Ljava_lang_String_;
		}

		static void n_SetApplicationIdentifier_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApplicationIdentifier = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApplicationIdentifier;
		static IntPtr id_setApplicationIdentifier_Ljava_lang_String_;
		public virtual unsafe string ApplicationIdentifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='getApplicationIdentifier' and count(parameter)=0]"
			[Register ("getApplicationIdentifier", "()Ljava/lang/String;", "GetGetApplicationIdentifierHandler")]
			get {
				if (id_getApplicationIdentifier == IntPtr.Zero)
					id_getApplicationIdentifier = JNIEnv.GetMethodID (class_ref, "getApplicationIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getApplicationIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApplicationIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='setApplicationIdentifier' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setApplicationIdentifier", "(Ljava/lang/String;)V", "GetSetApplicationIdentifier_Ljava_lang_String_Handler")]
			set {
				if (id_setApplicationIdentifier_Ljava_lang_String_ == IntPtr.Zero)
					id_setApplicationIdentifier_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setApplicationIdentifier", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApplicationIdentifier_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApplicationIdentifier", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getApplicationInstanceIdentifier;
#pragma warning disable 0169
		static Delegate GetGetApplicationInstanceIdentifierHandler ()
		{
			if (cb_getApplicationInstanceIdentifier == null)
				cb_getApplicationInstanceIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApplicationInstanceIdentifier);
			return cb_getApplicationInstanceIdentifier;
		}

		static IntPtr n_GetApplicationInstanceIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ApplicationInstanceIdentifier);
		}
#pragma warning restore 0169

		static Delegate cb_setApplicationInstanceIdentifier_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetApplicationInstanceIdentifier_Ljava_lang_String_Handler ()
		{
			if (cb_setApplicationInstanceIdentifier_Ljava_lang_String_ == null)
				cb_setApplicationInstanceIdentifier_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApplicationInstanceIdentifier_Ljava_lang_String_);
			return cb_setApplicationInstanceIdentifier_Ljava_lang_String_;
		}

		static void n_SetApplicationInstanceIdentifier_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApplicationInstanceIdentifier = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApplicationInstanceIdentifier;
		static IntPtr id_setApplicationInstanceIdentifier_Ljava_lang_String_;
		public virtual unsafe string ApplicationInstanceIdentifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='getApplicationInstanceIdentifier' and count(parameter)=0]"
			[Register ("getApplicationInstanceIdentifier", "()Ljava/lang/String;", "GetGetApplicationInstanceIdentifierHandler")]
			get {
				if (id_getApplicationInstanceIdentifier == IntPtr.Zero)
					id_getApplicationInstanceIdentifier = JNIEnv.GetMethodID (class_ref, "getApplicationInstanceIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getApplicationInstanceIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApplicationInstanceIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='setApplicationInstanceIdentifier' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setApplicationInstanceIdentifier", "(Ljava/lang/String;)V", "GetSetApplicationInstanceIdentifier_Ljava_lang_String_Handler")]
			set {
				if (id_setApplicationInstanceIdentifier_Ljava_lang_String_ == IntPtr.Zero)
					id_setApplicationInstanceIdentifier_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setApplicationInstanceIdentifier", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApplicationInstanceIdentifier_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApplicationInstanceIdentifier", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getOrganizationId;
#pragma warning disable 0169
		static Delegate GetGetOrganizationIdHandler ()
		{
			if (cb_getOrganizationId == null)
				cb_getOrganizationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationId);
			return cb_getOrganizationId;
		}

		static IntPtr n_GetOrganizationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OrganizationId);
		}
#pragma warning restore 0169

		static Delegate cb_setOrganizationId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationId_Ljava_lang_Long_Handler ()
		{
			if (cb_setOrganizationId_Ljava_lang_Long_ == null)
				cb_setOrganizationId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationId_Ljava_lang_Long_);
			return cb_setOrganizationId_Ljava_lang_Long_;
		}

		static void n_SetOrganizationId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OrganizationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationId;
		static IntPtr id_setOrganizationId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long OrganizationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='getOrganizationId' and count(parameter)=0]"
			[Register ("getOrganizationId", "()Ljava/lang/Long;", "GetGetOrganizationIdHandler")]
			get {
				if (id_getOrganizationId == IntPtr.Zero)
					id_getOrganizationId = JNIEnv.GetMethodID (class_ref, "getOrganizationId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='setOrganizationId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setOrganizationId", "(Ljava/lang/Long;)V", "GetSetOrganizationId_Ljava_lang_Long_Handler")]
			set {
				if (id_setOrganizationId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setOrganizationId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setOrganizationId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOrganizationId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getProximityApplicationUUID;
#pragma warning disable 0169
		static Delegate GetGetProximityApplicationUUIDHandler ()
		{
			if (cb_getProximityApplicationUUID == null)
				cb_getProximityApplicationUUID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetProximityApplicationUUID);
			return cb_getProximityApplicationUUID;
		}

		static IntPtr n_GetProximityApplicationUUID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ProximityApplicationUUID);
		}
#pragma warning restore 0169

		static Delegate cb_setProximityApplicationUUID_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetProximityApplicationUUID_Ljava_lang_String_Handler ()
		{
			if (cb_setProximityApplicationUUID_Ljava_lang_String_ == null)
				cb_setProximityApplicationUUID_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetProximityApplicationUUID_Ljava_lang_String_);
			return cb_setProximityApplicationUUID_Ljava_lang_String_;
		}

		static void n_SetProximityApplicationUUID_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ProximityApplicationUUID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getProximityApplicationUUID;
		static IntPtr id_setProximityApplicationUUID_Ljava_lang_String_;
		public virtual unsafe string ProximityApplicationUUID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='getProximityApplicationUUID' and count(parameter)=0]"
			[Register ("getProximityApplicationUUID", "()Ljava/lang/String;", "GetGetProximityApplicationUUIDHandler")]
			get {
				if (id_getProximityApplicationUUID == IntPtr.Zero)
					id_getProximityApplicationUUID = JNIEnv.GetMethodID (class_ref, "getProximityApplicationUUID", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getProximityApplicationUUID), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getProximityApplicationUUID", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='setProximityApplicationUUID' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setProximityApplicationUUID", "(Ljava/lang/String;)V", "GetSetProximityApplicationUUID_Ljava_lang_String_Handler")]
			set {
				if (id_setProximityApplicationUUID_Ljava_lang_String_ == IntPtr.Zero)
					id_setProximityApplicationUUID_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setProximityApplicationUUID", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setProximityApplicationUUID_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setProximityApplicationUUID", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getReceiverUUID;
#pragma warning disable 0169
		static Delegate GetGetReceiverUUIDHandler ()
		{
			if (cb_getReceiverUUID == null)
				cb_getReceiverUUID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetReceiverUUID);
			return cb_getReceiverUUID;
		}

		static IntPtr n_GetReceiverUUID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ReceiverUUID);
		}
#pragma warning restore 0169

		static Delegate cb_setReceiverUUID_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetReceiverUUID_Ljava_lang_String_Handler ()
		{
			if (cb_setReceiverUUID_Ljava_lang_String_ == null)
				cb_setReceiverUUID_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetReceiverUUID_Ljava_lang_String_);
			return cb_setReceiverUUID_Ljava_lang_String_;
		}

		static void n_SetReceiverUUID_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReceiverUUID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getReceiverUUID;
		static IntPtr id_setReceiverUUID_Ljava_lang_String_;
		public virtual unsafe string ReceiverUUID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='getReceiverUUID' and count(parameter)=0]"
			[Register ("getReceiverUUID", "()Ljava/lang/String;", "GetGetReceiverUUIDHandler")]
			get {
				if (id_getReceiverUUID == IntPtr.Zero)
					id_getReceiverUUID = JNIEnv.GetMethodID (class_ref, "getReceiverUUID", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getReceiverUUID), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getReceiverUUID", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='setReceiverUUID' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setReceiverUUID", "(Ljava/lang/String;)V", "GetSetReceiverUUID_Ljava_lang_String_Handler")]
			set {
				if (id_setReceiverUUID_Ljava_lang_String_ == IntPtr.Zero)
					id_setReceiverUUID_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setReceiverUUID", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setReceiverUUID_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setReceiverUUID", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getRegistrationTimestamp;
#pragma warning disable 0169
		static Delegate GetGetRegistrationTimestampHandler ()
		{
			if (cb_getRegistrationTimestamp == null)
				cb_getRegistrationTimestamp = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetRegistrationTimestamp);
			return cb_getRegistrationTimestamp;
		}

		static IntPtr n_GetRegistrationTimestamp (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.RegistrationTimestamp);
		}
#pragma warning restore 0169

		static Delegate cb_setRegistrationTimestamp_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetRegistrationTimestamp_Ljava_lang_Long_Handler ()
		{
			if (cb_setRegistrationTimestamp_Ljava_lang_Long_ == null)
				cb_setRegistrationTimestamp_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetRegistrationTimestamp_Ljava_lang_Long_);
			return cb_setRegistrationTimestamp_Ljava_lang_Long_;
		}

		static void n_SetRegistrationTimestamp_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RegistrationTimestamp = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getRegistrationTimestamp;
		static IntPtr id_setRegistrationTimestamp_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long RegistrationTimestamp {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='getRegistrationTimestamp' and count(parameter)=0]"
			[Register ("getRegistrationTimestamp", "()Ljava/lang/Long;", "GetGetRegistrationTimestampHandler")]
			get {
				if (id_getRegistrationTimestamp == IntPtr.Zero)
					id_getRegistrationTimestamp = JNIEnv.GetMethodID (class_ref, "getRegistrationTimestamp", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getRegistrationTimestamp), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRegistrationTimestamp", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='setRegistrationTimestamp' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setRegistrationTimestamp", "(Ljava/lang/Long;)V", "GetSetRegistrationTimestamp_Ljava_lang_Long_Handler")]
			set {
				if (id_setRegistrationTimestamp_Ljava_lang_Long_ == IntPtr.Zero)
					id_setRegistrationTimestamp_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setRegistrationTimestamp", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setRegistrationTimestamp_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setRegistrationTimestamp", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getUserId;
#pragma warning disable 0169
		static Delegate GetGetUserIdHandler ()
		{
			if (cb_getUserId == null)
				cb_getUserId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUserId);
			return cb_getUserId;
		}

		static IntPtr n_GetUserId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.UserId);
		}
#pragma warning restore 0169

		static Delegate cb_setUserId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetUserId_Ljava_lang_Long_Handler ()
		{
			if (cb_setUserId_Ljava_lang_Long_ == null)
				cb_setUserId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUserId_Ljava_lang_Long_);
			return cb_setUserId_Ljava_lang_Long_;
		}

		static void n_SetUserId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.UserId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUserId;
		static IntPtr id_setUserId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long UserId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='getUserId' and count(parameter)=0]"
			[Register ("getUserId", "()Ljava/lang/Long;", "GetGetUserIdHandler")]
			get {
				if (id_getUserId == IntPtr.Zero)
					id_getUserId = JNIEnv.GetMethodID (class_ref, "getUserId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getUserId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUserId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='setUserId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setUserId", "(Ljava/lang/Long;)V", "GetSetUserId_Ljava_lang_Long_Handler")]
			set {
				if (id_setUserId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setUserId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setUserId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUserId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUserId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getUserName;
#pragma warning disable 0169
		static Delegate GetGetUserNameHandler ()
		{
			if (cb_getUserName == null)
				cb_getUserName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUserName);
			return cb_getUserName;
		}

		static IntPtr n_GetUserName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.UserName);
		}
#pragma warning restore 0169

		static Delegate cb_setUserName_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUserName_Ljava_lang_String_Handler ()
		{
			if (cb_setUserName_Ljava_lang_String_ == null)
				cb_setUserName_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUserName_Ljava_lang_String_);
			return cb_setUserName_Ljava_lang_String_;
		}

		static void n_SetUserName_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.UserName = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUserName;
		static IntPtr id_setUserName_Ljava_lang_String_;
		public virtual unsafe string UserName {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='getUserName' and count(parameter)=0]"
			[Register ("getUserName", "()Ljava/lang/String;", "GetGetUserNameHandler")]
			get {
				if (id_getUserName == IntPtr.Zero)
					id_getUserName = JNIEnv.GetMethodID (class_ref, "getUserName", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUserName), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUserName", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='setUserName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUserName", "(Ljava/lang/String;)V", "GetSetUserName_Ljava_lang_String_Handler")]
			set {
				if (id_setUserName_Ljava_lang_String_ == IntPtr.Zero)
					id_setUserName_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUserName", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUserName_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUserName", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUserPassword;
#pragma warning disable 0169
		static Delegate GetGetUserPasswordHandler ()
		{
			if (cb_getUserPassword == null)
				cb_getUserPassword = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUserPassword);
			return cb_getUserPassword;
		}

		static IntPtr n_GetUserPassword (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.UserPassword);
		}
#pragma warning restore 0169

		static Delegate cb_setUserPassword_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUserPassword_Ljava_lang_String_Handler ()
		{
			if (cb_setUserPassword_Ljava_lang_String_ == null)
				cb_setUserPassword_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUserPassword_Ljava_lang_String_);
			return cb_setUserPassword_Ljava_lang_String_;
		}

		static void n_SetUserPassword_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.UserPassword = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUserPassword;
		static IntPtr id_setUserPassword_Ljava_lang_String_;
		public virtual unsafe string UserPassword {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='getUserPassword' and count(parameter)=0]"
			[Register ("getUserPassword", "()Ljava/lang/String;", "GetGetUserPasswordHandler")]
			get {
				if (id_getUserPassword == IntPtr.Zero)
					id_getUserPassword = JNIEnv.GetMethodID (class_ref, "getUserPassword", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUserPassword), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUserPassword", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='setUserPassword' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUserPassword", "(Ljava/lang/String;)V", "GetSetUserPassword_Ljava_lang_String_Handler")]
			set {
				if (id_setUserPassword_Ljava_lang_String_ == IntPtr.Zero)
					id_setUserPassword_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUserPassword", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUserPassword_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUserPassword", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getRegistrationState;
#pragma warning disable 0169
		static Delegate GetGetRegistrationStateHandler ()
		{
			if (cb_getRegistrationState == null)
				cb_getRegistrationState = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetRegistrationState);
			return cb_getRegistrationState;
		}

		static IntPtr n_GetRegistrationState (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.GetRegistrationState ());
		}
#pragma warning restore 0169

		static IntPtr id_getRegistrationState;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='getRegistrationState' and count(parameter)=0]"
		[Register ("getRegistrationState", "()Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;", "GetGetRegistrationStateHandler")]
		public virtual unsafe global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState GetRegistrationState ()
		{
			if (id_getRegistrationState == IntPtr.Zero)
				id_getRegistrationState = JNIEnv.GetMethodID (class_ref, "getRegistrationState", "()Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;");
			try {

				if (GetType () == ThresholdType)
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState> (JNIEnv.CallObjectMethod  (Handle, id_getRegistrationState), JniHandleOwnership.TransferLocalRef);
				else
					return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRegistrationState", "()Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_registered;
#pragma warning disable 0169
		static Delegate GetRegisteredHandler ()
		{
			if (cb_registered == null)
				cb_registered = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_Registered);
			return cb_registered;
		}

		static bool n_Registered (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Registered ();
		}
#pragma warning restore 0169

		static IntPtr id_registered;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='registered' and count(parameter)=0]"
		[Register ("registered", "()Z", "GetRegisteredHandler")]
		public virtual unsafe bool Registered ()
		{
			if (id_registered == IntPtr.Zero)
				id_registered = JNIEnv.GetMethodID (class_ref, "registered", "()Z");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.CallBooleanMethod  (Handle, id_registered);
				else
					return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "registered", "()Z"));
			} finally {
			}
		}

		static Delegate cb_setRegistrationState_Lcom_gimbal_internal_protocol_RegistrationProperties_RegistrationState_;
#pragma warning disable 0169
		static Delegate GetSetRegistrationState_Lcom_gimbal_internal_protocol_RegistrationProperties_RegistrationState_Handler ()
		{
			if (cb_setRegistrationState_Lcom_gimbal_internal_protocol_RegistrationProperties_RegistrationState_ == null)
				cb_setRegistrationState_Lcom_gimbal_internal_protocol_RegistrationProperties_RegistrationState_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetRegistrationState_Lcom_gimbal_internal_protocol_RegistrationProperties_RegistrationState_);
			return cb_setRegistrationState_Lcom_gimbal_internal_protocol_RegistrationProperties_RegistrationState_;
		}

		static void n_SetRegistrationState_Lcom_gimbal_internal_protocol_RegistrationProperties_RegistrationState_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetRegistrationState (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setRegistrationState_Lcom_gimbal_internal_protocol_RegistrationProperties_RegistrationState_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='setRegistrationState' and count(parameter)=1 and parameter[1][@type='com.gimbal.internal.protocol.RegistrationProperties.RegistrationState']]"
		[Register ("setRegistrationState", "(Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;)V", "GetSetRegistrationState_Lcom_gimbal_internal_protocol_RegistrationProperties_RegistrationState_Handler")]
		public virtual unsafe void SetRegistrationState (global::Com.Gimbal.Internal.Protocol.RegistrationProperties.RegistrationState p0)
		{
			if (id_setRegistrationState_Lcom_gimbal_internal_protocol_RegistrationProperties_RegistrationState_ == IntPtr.Zero)
				id_setRegistrationState_Lcom_gimbal_internal_protocol_RegistrationProperties_RegistrationState_ = JNIEnv.GetMethodID (class_ref, "setRegistrationState", "(Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setRegistrationState_Lcom_gimbal_internal_protocol_RegistrationProperties_RegistrationState_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setRegistrationState", "(Lcom/gimbal/internal/protocol/RegistrationProperties$RegistrationState;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_update_Lcom_gimbal_protocol_Registration_;
#pragma warning disable 0169
		static Delegate GetUpdate_Lcom_gimbal_protocol_Registration_Handler ()
		{
			if (cb_update_Lcom_gimbal_protocol_Registration_ == null)
				cb_update_Lcom_gimbal_protocol_Registration_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_Update_Lcom_gimbal_protocol_Registration_);
			return cb_update_Lcom_gimbal_protocol_Registration_;
		}

		static IntPtr n_Update_Lcom_gimbal_protocol_Registration_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.RegistrationProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Protocol.Registration p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.Update (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_update_Lcom_gimbal_protocol_Registration_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='RegistrationProperties']/method[@name='update' and count(parameter)=1 and parameter[1][@type='com.gimbal.protocol.Registration']]"
		[Register ("update", "(Lcom/gimbal/protocol/Registration;)Lcom/gimbal/internal/protocol/RegistrationProperties;", "GetUpdate_Lcom_gimbal_protocol_Registration_Handler")]
		public virtual unsafe global::Com.Gimbal.Internal.Protocol.RegistrationProperties Update (global::Com.Gimbal.Protocol.Registration p0)
		{
			if (id_update_Lcom_gimbal_protocol_Registration_ == IntPtr.Zero)
				id_update_Lcom_gimbal_protocol_Registration_ = JNIEnv.GetMethodID (class_ref, "update", "(Lcom/gimbal/protocol/Registration;)Lcom/gimbal/internal/protocol/RegistrationProperties;");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				global::Com.Gimbal.Internal.Protocol.RegistrationProperties __ret;
				if (GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (JNIEnv.CallObjectMethod  (Handle, id_update_Lcom_gimbal_protocol_Registration_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.RegistrationProperties> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "update", "(Lcom/gimbal/protocol/Registration;)Lcom/gimbal/internal/protocol/RegistrationProperties;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
			}
		}

	}
}
