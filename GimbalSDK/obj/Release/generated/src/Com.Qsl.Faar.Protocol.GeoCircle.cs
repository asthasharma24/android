using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoCircle']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/GeoCircle", DoNotGenerateAcw=true)]
	public partial class GeoCircle : global::Java.Lang.Object, global::Java.IO.ISerializable {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/GeoCircle", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (GeoCircle); }
		}

		protected GeoCircle (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoCircle']/constructor[@name='GeoCircle' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe GeoCircle ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (GeoCircle)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getLocation;
#pragma warning disable 0169
		static Delegate GetGetLocationHandler ()
		{
			if (cb_getLocation == null)
				cb_getLocation = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLocation);
			return cb_getLocation;
		}

		static IntPtr n_GetLocation (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.GeoCircle __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoCircle> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Location);
		}
#pragma warning restore 0169

		static Delegate cb_setLocation_Lcom_qsl_faar_protocol_Location_;
#pragma warning disable 0169
		static Delegate GetSetLocation_Lcom_qsl_faar_protocol_Location_Handler ()
		{
			if (cb_setLocation_Lcom_qsl_faar_protocol_Location_ == null)
				cb_setLocation_Lcom_qsl_faar_protocol_Location_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLocation_Lcom_qsl_faar_protocol_Location_);
			return cb_setLocation_Lcom_qsl_faar_protocol_Location_;
		}

		static void n_SetLocation_Lcom_qsl_faar_protocol_Location_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.GeoCircle __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoCircle> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.Location p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Location> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Location = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLocation;
		static IntPtr id_setLocation_Lcom_qsl_faar_protocol_Location_;
		public virtual unsafe global::Com.Qsl.Faar.Protocol.Location Location {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoCircle']/method[@name='getLocation' and count(parameter)=0]"
			[Register ("getLocation", "()Lcom/qsl/faar/protocol/Location;", "GetGetLocationHandler")]
			get {
				if (id_getLocation == IntPtr.Zero)
					id_getLocation = JNIEnv.GetMethodID (class_ref, "getLocation", "()Lcom/qsl/faar/protocol/Location;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Location> (JNIEnv.CallObjectMethod  (Handle, id_getLocation), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Location> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLocation", "()Lcom/qsl/faar/protocol/Location;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoCircle']/method[@name='setLocation' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.Location']]"
			[Register ("setLocation", "(Lcom/qsl/faar/protocol/Location;)V", "GetSetLocation_Lcom_qsl_faar_protocol_Location_Handler")]
			set {
				if (id_setLocation_Lcom_qsl_faar_protocol_Location_ == IntPtr.Zero)
					id_setLocation_Lcom_qsl_faar_protocol_Location_ = JNIEnv.GetMethodID (class_ref, "setLocation", "(Lcom/qsl/faar/protocol/Location;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLocation_Lcom_qsl_faar_protocol_Location_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLocation", "(Lcom/qsl/faar/protocol/Location;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getRadius;
#pragma warning disable 0169
		static Delegate GetGetRadiusHandler ()
		{
			if (cb_getRadius == null)
				cb_getRadius = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetRadius);
			return cb_getRadius;
		}

		static IntPtr n_GetRadius (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.GeoCircle __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoCircle> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Radius);
		}
#pragma warning restore 0169

		static Delegate cb_setRadius_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetRadius_Ljava_lang_Integer_Handler ()
		{
			if (cb_setRadius_Ljava_lang_Integer_ == null)
				cb_setRadius_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetRadius_Ljava_lang_Integer_);
			return cb_setRadius_Ljava_lang_Integer_;
		}

		static void n_SetRadius_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.GeoCircle __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoCircle> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Radius = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getRadius;
		static IntPtr id_setRadius_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer Radius {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoCircle']/method[@name='getRadius' and count(parameter)=0]"
			[Register ("getRadius", "()Ljava/lang/Integer;", "GetGetRadiusHandler")]
			get {
				if (id_getRadius == IntPtr.Zero)
					id_getRadius = JNIEnv.GetMethodID (class_ref, "getRadius", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getRadius), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRadius", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='GeoCircle']/method[@name='setRadius' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setRadius", "(Ljava/lang/Integer;)V", "GetSetRadius_Ljava_lang_Integer_Handler")]
			set {
				if (id_setRadius_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setRadius_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setRadius", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setRadius_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setRadius", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

	}
}
