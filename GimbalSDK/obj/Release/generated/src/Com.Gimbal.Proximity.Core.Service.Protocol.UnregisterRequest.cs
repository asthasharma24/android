using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Service.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='UnregisterRequest']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/service/protocol/UnregisterRequest", DoNotGenerateAcw=true)]
	public partial class UnregisterRequest : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/service/protocol/UnregisterRequest", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (UnregisterRequest); }
		}

		protected UnregisterRequest (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='UnregisterRequest']/constructor[@name='UnregisterRequest' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe UnregisterRequest ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (UnregisterRequest)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getReceiverUUID;
#pragma warning disable 0169
		static Delegate GetGetReceiverUUIDHandler ()
		{
			if (cb_getReceiverUUID == null)
				cb_getReceiverUUID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetReceiverUUID);
			return cb_getReceiverUUID;
		}

		static IntPtr n_GetReceiverUUID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.UnregisterRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.UnregisterRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ReceiverUUID);
		}
#pragma warning restore 0169

		static Delegate cb_setReceiverUUID_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetReceiverUUID_Ljava_lang_String_Handler ()
		{
			if (cb_setReceiverUUID_Ljava_lang_String_ == null)
				cb_setReceiverUUID_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetReceiverUUID_Ljava_lang_String_);
			return cb_setReceiverUUID_Ljava_lang_String_;
		}

		static void n_SetReceiverUUID_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.UnregisterRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.UnregisterRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReceiverUUID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getReceiverUUID;
		static IntPtr id_setReceiverUUID_Ljava_lang_String_;
		public virtual unsafe string ReceiverUUID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='UnregisterRequest']/method[@name='getReceiverUUID' and count(parameter)=0]"
			[Register ("getReceiverUUID", "()Ljava/lang/String;", "GetGetReceiverUUIDHandler")]
			get {
				if (id_getReceiverUUID == IntPtr.Zero)
					id_getReceiverUUID = JNIEnv.GetMethodID (class_ref, "getReceiverUUID", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getReceiverUUID), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getReceiverUUID", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='UnregisterRequest']/method[@name='setReceiverUUID' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setReceiverUUID", "(Ljava/lang/String;)V", "GetSetReceiverUUID_Ljava_lang_String_Handler")]
			set {
				if (id_setReceiverUUID_Ljava_lang_String_ == IntPtr.Zero)
					id_setReceiverUUID_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setReceiverUUID", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setReceiverUUID_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setReceiverUUID", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
