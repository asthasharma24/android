using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Places {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']"
	[global::Android.Runtime.Register ("com/gimbal/internal/places/InternalPlace", DoNotGenerateAcw=true)]
	public partial class InternalPlace : global::Java.Lang.Object, global::Com.Gimbal.Proguard.IKeep {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/places/InternalPlace", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (InternalPlace); }
		}

		protected InternalPlace (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/constructor[@name='InternalPlace' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe InternalPlace ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (InternalPlace)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAttributes;
#pragma warning disable 0169
		static Delegate GetGetAttributesHandler ()
		{
			if (cb_getAttributes == null)
				cb_getAttributes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAttributes);
			return cb_getAttributes;
		}

		static IntPtr n_GetAttributes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Internal.Places.InternalAttribute>.ToLocalJniHandle (__this.Attributes);
		}
#pragma warning restore 0169

		static Delegate cb_setAttributes_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetAttributes_Ljava_util_List_Handler ()
		{
			if (cb_setAttributes_Ljava_util_List_ == null)
				cb_setAttributes_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAttributes_Ljava_util_List_);
			return cb_setAttributes_Ljava_util_List_;
		}

		static void n_SetAttributes_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Internal.Places.InternalAttribute>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Attributes = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getAttributes;
		static IntPtr id_setAttributes_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Internal.Places.InternalAttribute> Attributes {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='getAttributes' and count(parameter)=0]"
			[Register ("getAttributes", "()Ljava/util/List;", "GetGetAttributesHandler")]
			get {
				if (id_getAttributes == IntPtr.Zero)
					id_getAttributes = JNIEnv.GetMethodID (class_ref, "getAttributes", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Internal.Places.InternalAttribute>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getAttributes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Internal.Places.InternalAttribute>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAttributes", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='setAttributes' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.internal.places.InternalAttribute&gt;']]"
			[Register ("setAttributes", "(Ljava/util/List;)V", "GetSetAttributes_Ljava_util_List_Handler")]
			set {
				if (id_setAttributes_Ljava_util_List_ == IntPtr.Zero)
					id_setAttributes_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setAttributes", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Gimbal.Internal.Places.InternalAttribute>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setAttributes_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAttributes", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getBeaconIds;
#pragma warning disable 0169
		static Delegate GetGetBeaconIdsHandler ()
		{
			if (cb_getBeaconIds == null)
				cb_getBeaconIds = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBeaconIds);
			return cb_getBeaconIds;
		}

		static IntPtr n_GetBeaconIds (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaSet<string>.ToLocalJniHandle (__this.BeaconIds);
		}
#pragma warning restore 0169

		static Delegate cb_setBeaconIds_Ljava_util_Set_;
#pragma warning disable 0169
		static Delegate GetSetBeaconIds_Ljava_util_Set_Handler ()
		{
			if (cb_setBeaconIds_Ljava_util_Set_ == null)
				cb_setBeaconIds_Ljava_util_Set_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetBeaconIds_Ljava_util_Set_);
			return cb_setBeaconIds_Ljava_util_Set_;
		}

		static void n_SetBeaconIds_Ljava_util_Set_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaSet<string>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.BeaconIds = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getBeaconIds;
		static IntPtr id_setBeaconIds_Ljava_util_Set_;
		public virtual unsafe global::System.Collections.Generic.ICollection<string> BeaconIds {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='getBeaconIds' and count(parameter)=0]"
			[Register ("getBeaconIds", "()Ljava/util/Set;", "GetGetBeaconIdsHandler")]
			get {
				if (id_getBeaconIds == IntPtr.Zero)
					id_getBeaconIds = JNIEnv.GetMethodID (class_ref, "getBeaconIds", "()Ljava/util/Set;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaSet<string>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getBeaconIds), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaSet<string>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBeaconIds", "()Ljava/util/Set;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='setBeaconIds' and count(parameter)=1 and parameter[1][@type='java.util.Set&lt;java.lang.String&gt;']]"
			[Register ("setBeaconIds", "(Ljava/util/Set;)V", "GetSetBeaconIds_Ljava_util_Set_Handler")]
			set {
				if (id_setBeaconIds_Ljava_util_Set_ == IntPtr.Zero)
					id_setBeaconIds_Ljava_util_Set_ = JNIEnv.GetMethodID (class_ref, "setBeaconIds", "(Ljava/util/Set;)V");
				IntPtr native_value = global::Android.Runtime.JavaSet<string>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setBeaconIds_Ljava_util_Set_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setBeaconIds", "(Ljava/util/Set;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getBeaconIdsCurrentlyIn;
#pragma warning disable 0169
		static Delegate GetGetBeaconIdsCurrentlyInHandler ()
		{
			if (cb_getBeaconIdsCurrentlyIn == null)
				cb_getBeaconIdsCurrentlyIn = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBeaconIdsCurrentlyIn);
			return cb_getBeaconIdsCurrentlyIn;
		}

		static IntPtr n_GetBeaconIdsCurrentlyIn (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaSet<string>.ToLocalJniHandle (__this.BeaconIdsCurrentlyIn);
		}
#pragma warning restore 0169

		static Delegate cb_setBeaconIdsCurrentlyIn_Ljava_util_Set_;
#pragma warning disable 0169
		static Delegate GetSetBeaconIdsCurrentlyIn_Ljava_util_Set_Handler ()
		{
			if (cb_setBeaconIdsCurrentlyIn_Ljava_util_Set_ == null)
				cb_setBeaconIdsCurrentlyIn_Ljava_util_Set_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetBeaconIdsCurrentlyIn_Ljava_util_Set_);
			return cb_setBeaconIdsCurrentlyIn_Ljava_util_Set_;
		}

		static void n_SetBeaconIdsCurrentlyIn_Ljava_util_Set_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaSet<string>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.BeaconIdsCurrentlyIn = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getBeaconIdsCurrentlyIn;
		static IntPtr id_setBeaconIdsCurrentlyIn_Ljava_util_Set_;
		public virtual unsafe global::System.Collections.Generic.ICollection<string> BeaconIdsCurrentlyIn {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='getBeaconIdsCurrentlyIn' and count(parameter)=0]"
			[Register ("getBeaconIdsCurrentlyIn", "()Ljava/util/Set;", "GetGetBeaconIdsCurrentlyInHandler")]
			get {
				if (id_getBeaconIdsCurrentlyIn == IntPtr.Zero)
					id_getBeaconIdsCurrentlyIn = JNIEnv.GetMethodID (class_ref, "getBeaconIdsCurrentlyIn", "()Ljava/util/Set;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaSet<string>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getBeaconIdsCurrentlyIn), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaSet<string>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBeaconIdsCurrentlyIn", "()Ljava/util/Set;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='setBeaconIdsCurrentlyIn' and count(parameter)=1 and parameter[1][@type='java.util.Set&lt;java.lang.String&gt;']]"
			[Register ("setBeaconIdsCurrentlyIn", "(Ljava/util/Set;)V", "GetSetBeaconIdsCurrentlyIn_Ljava_util_Set_Handler")]
			set {
				if (id_setBeaconIdsCurrentlyIn_Ljava_util_Set_ == IntPtr.Zero)
					id_setBeaconIdsCurrentlyIn_Ljava_util_Set_ = JNIEnv.GetMethodID (class_ref, "setBeaconIdsCurrentlyIn", "(Ljava/util/Set;)V");
				IntPtr native_value = global::Android.Runtime.JavaSet<string>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setBeaconIdsCurrentlyIn_Ljava_util_Set_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setBeaconIdsCurrentlyIn", "(Ljava/util/Set;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getEntryTimeMillis;
#pragma warning disable 0169
		static Delegate GetGetEntryTimeMillisHandler ()
		{
			if (cb_getEntryTimeMillis == null)
				cb_getEntryTimeMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetEntryTimeMillis);
			return cb_getEntryTimeMillis;
		}

		static long n_GetEntryTimeMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.EntryTimeMillis;
		}
#pragma warning restore 0169

		static Delegate cb_setEntryTimeMillis_J;
#pragma warning disable 0169
		static Delegate GetSetEntryTimeMillis_JHandler ()
		{
			if (cb_setEntryTimeMillis_J == null)
				cb_setEntryTimeMillis_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetEntryTimeMillis_J);
			return cb_setEntryTimeMillis_J;
		}

		static void n_SetEntryTimeMillis_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.EntryTimeMillis = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEntryTimeMillis;
		static IntPtr id_setEntryTimeMillis_J;
		public virtual unsafe long EntryTimeMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='getEntryTimeMillis' and count(parameter)=0]"
			[Register ("getEntryTimeMillis", "()J", "GetGetEntryTimeMillisHandler")]
			get {
				if (id_getEntryTimeMillis == IntPtr.Zero)
					id_getEntryTimeMillis = JNIEnv.GetMethodID (class_ref, "getEntryTimeMillis", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getEntryTimeMillis);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEntryTimeMillis", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='setEntryTimeMillis' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setEntryTimeMillis", "(J)V", "GetSetEntryTimeMillis_JHandler")]
			set {
				if (id_setEntryTimeMillis_J == IntPtr.Zero)
					id_setEntryTimeMillis_J = JNIEnv.GetMethodID (class_ref, "setEntryTimeMillis", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEntryTimeMillis_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEntryTimeMillis", "(J)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getExitTimeMillis;
#pragma warning disable 0169
		static Delegate GetGetExitTimeMillisHandler ()
		{
			if (cb_getExitTimeMillis == null)
				cb_getExitTimeMillis = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetExitTimeMillis);
			return cb_getExitTimeMillis;
		}

		static long n_GetExitTimeMillis (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ExitTimeMillis;
		}
#pragma warning restore 0169

		static Delegate cb_setExitTimeMillis_J;
#pragma warning disable 0169
		static Delegate GetSetExitTimeMillis_JHandler ()
		{
			if (cb_setExitTimeMillis_J == null)
				cb_setExitTimeMillis_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetExitTimeMillis_J);
			return cb_setExitTimeMillis_J;
		}

		static void n_SetExitTimeMillis_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ExitTimeMillis = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getExitTimeMillis;
		static IntPtr id_setExitTimeMillis_J;
		public virtual unsafe long ExitTimeMillis {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='getExitTimeMillis' and count(parameter)=0]"
			[Register ("getExitTimeMillis", "()J", "GetGetExitTimeMillisHandler")]
			get {
				if (id_getExitTimeMillis == IntPtr.Zero)
					id_getExitTimeMillis = JNIEnv.GetMethodID (class_ref, "getExitTimeMillis", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getExitTimeMillis);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getExitTimeMillis", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='setExitTimeMillis' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setExitTimeMillis", "(J)V", "GetSetExitTimeMillis_JHandler")]
			set {
				if (id_setExitTimeMillis_J == IntPtr.Zero)
					id_setExitTimeMillis_J = JNIEnv.GetMethodID (class_ref, "setExitTimeMillis", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setExitTimeMillis_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setExitTimeMillis", "(J)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getGeofenceIds;
#pragma warning disable 0169
		static Delegate GetGetGeofenceIdsHandler ()
		{
			if (cb_getGeofenceIds == null)
				cb_getGeofenceIds = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetGeofenceIds);
			return cb_getGeofenceIds;
		}

		static IntPtr n_GetGeofenceIds (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaSet<global::Java.Lang.Long>.ToLocalJniHandle (__this.GeofenceIds);
		}
#pragma warning restore 0169

		static Delegate cb_setGeofenceIds_Ljava_util_Set_;
#pragma warning disable 0169
		static Delegate GetSetGeofenceIds_Ljava_util_Set_Handler ()
		{
			if (cb_setGeofenceIds_Ljava_util_Set_ == null)
				cb_setGeofenceIds_Ljava_util_Set_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetGeofenceIds_Ljava_util_Set_);
			return cb_setGeofenceIds_Ljava_util_Set_;
		}

		static void n_SetGeofenceIds_Ljava_util_Set_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaSet<global::Java.Lang.Long>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.GeofenceIds = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getGeofenceIds;
		static IntPtr id_setGeofenceIds_Ljava_util_Set_;
		public virtual unsafe global::System.Collections.Generic.ICollection<global::Java.Lang.Long> GeofenceIds {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='getGeofenceIds' and count(parameter)=0]"
			[Register ("getGeofenceIds", "()Ljava/util/Set;", "GetGetGeofenceIdsHandler")]
			get {
				if (id_getGeofenceIds == IntPtr.Zero)
					id_getGeofenceIds = JNIEnv.GetMethodID (class_ref, "getGeofenceIds", "()Ljava/util/Set;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaSet<global::Java.Lang.Long>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getGeofenceIds), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaSet<global::Java.Lang.Long>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getGeofenceIds", "()Ljava/util/Set;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='setGeofenceIds' and count(parameter)=1 and parameter[1][@type='java.util.Set&lt;java.lang.Long&gt;']]"
			[Register ("setGeofenceIds", "(Ljava/util/Set;)V", "GetSetGeofenceIds_Ljava_util_Set_Handler")]
			set {
				if (id_setGeofenceIds_Ljava_util_Set_ == IntPtr.Zero)
					id_setGeofenceIds_Ljava_util_Set_ = JNIEnv.GetMethodID (class_ref, "setGeofenceIds", "(Ljava/util/Set;)V");
				IntPtr native_value = global::Android.Runtime.JavaSet<global::Java.Lang.Long>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setGeofenceIds_Ljava_util_Set_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setGeofenceIds", "(Ljava/util/Set;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getGeofenceIdsCurrentlyIn;
#pragma warning disable 0169
		static Delegate GetGetGeofenceIdsCurrentlyInHandler ()
		{
			if (cb_getGeofenceIdsCurrentlyIn == null)
				cb_getGeofenceIdsCurrentlyIn = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetGeofenceIdsCurrentlyIn);
			return cb_getGeofenceIdsCurrentlyIn;
		}

		static IntPtr n_GetGeofenceIdsCurrentlyIn (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaSet<global::Java.Lang.Long>.ToLocalJniHandle (__this.GeofenceIdsCurrentlyIn);
		}
#pragma warning restore 0169

		static Delegate cb_setGeofenceIdsCurrentlyIn_Ljava_util_Set_;
#pragma warning disable 0169
		static Delegate GetSetGeofenceIdsCurrentlyIn_Ljava_util_Set_Handler ()
		{
			if (cb_setGeofenceIdsCurrentlyIn_Ljava_util_Set_ == null)
				cb_setGeofenceIdsCurrentlyIn_Ljava_util_Set_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetGeofenceIdsCurrentlyIn_Ljava_util_Set_);
			return cb_setGeofenceIdsCurrentlyIn_Ljava_util_Set_;
		}

		static void n_SetGeofenceIdsCurrentlyIn_Ljava_util_Set_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaSet<global::Java.Lang.Long>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.GeofenceIdsCurrentlyIn = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getGeofenceIdsCurrentlyIn;
		static IntPtr id_setGeofenceIdsCurrentlyIn_Ljava_util_Set_;
		public virtual unsafe global::System.Collections.Generic.ICollection<global::Java.Lang.Long> GeofenceIdsCurrentlyIn {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='getGeofenceIdsCurrentlyIn' and count(parameter)=0]"
			[Register ("getGeofenceIdsCurrentlyIn", "()Ljava/util/Set;", "GetGetGeofenceIdsCurrentlyInHandler")]
			get {
				if (id_getGeofenceIdsCurrentlyIn == IntPtr.Zero)
					id_getGeofenceIdsCurrentlyIn = JNIEnv.GetMethodID (class_ref, "getGeofenceIdsCurrentlyIn", "()Ljava/util/Set;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaSet<global::Java.Lang.Long>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getGeofenceIdsCurrentlyIn), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaSet<global::Java.Lang.Long>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getGeofenceIdsCurrentlyIn", "()Ljava/util/Set;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='setGeofenceIdsCurrentlyIn' and count(parameter)=1 and parameter[1][@type='java.util.Set&lt;java.lang.Long&gt;']]"
			[Register ("setGeofenceIdsCurrentlyIn", "(Ljava/util/Set;)V", "GetSetGeofenceIdsCurrentlyIn_Ljava_util_Set_Handler")]
			set {
				if (id_setGeofenceIdsCurrentlyIn_Ljava_util_Set_ == IntPtr.Zero)
					id_setGeofenceIdsCurrentlyIn_Ljava_util_Set_ = JNIEnv.GetMethodID (class_ref, "setGeofenceIdsCurrentlyIn", "(Ljava/util/Set;)V");
				IntPtr native_value = global::Android.Runtime.JavaSet<global::Java.Lang.Long>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setGeofenceIdsCurrentlyIn_Ljava_util_Set_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setGeofenceIdsCurrentlyIn", "(Ljava/util/Set;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_Long_Handler ()
		{
			if (cb_setId_Ljava_lang_Long_ == null)
				cb_setId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_Long_);
			return cb_setId_Ljava_lang_Long_;
		}

		static void n_SetId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/Long;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setId", "(Ljava/lang/Long;)V", "GetSetId_Ljava_lang_Long_Handler")]
			set {
				if (id_setId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_isAtAnyFence;
#pragma warning disable 0169
		static Delegate GetIsAtAnyFenceHandler ()
		{
			if (cb_isAtAnyFence == null)
				cb_isAtAnyFence = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsAtAnyFence);
			return cb_isAtAnyFence;
		}

		static bool n_IsAtAnyFence (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsAtAnyFence;
		}
#pragma warning restore 0169

		static IntPtr id_isAtAnyFence;
		public virtual unsafe bool IsAtAnyFence {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='isAtAnyFence' and count(parameter)=0]"
			[Register ("isAtAnyFence", "()Z", "GetIsAtAnyFenceHandler")]
			get {
				if (id_isAtAnyFence == IntPtr.Zero)
					id_isAtAnyFence = JNIEnv.GetMethodID (class_ref, "isAtAnyFence", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isAtAnyFence);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isAtAnyFence", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_getName;
#pragma warning disable 0169
		static Delegate GetGetNameHandler ()
		{
			if (cb_getName == null)
				cb_getName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetName);
			return cb_getName;
		}

		static IntPtr n_GetName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Name);
		}
#pragma warning restore 0169

		static Delegate cb_setName_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetName_Ljava_lang_String_Handler ()
		{
			if (cb_setName_Ljava_lang_String_ == null)
				cb_setName_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetName_Ljava_lang_String_);
			return cb_setName_Ljava_lang_String_;
		}

		static void n_SetName_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Name = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getName;
		static IntPtr id_setName_Ljava_lang_String_;
		public virtual unsafe string Name {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='getName' and count(parameter)=0]"
			[Register ("getName", "()Ljava/lang/String;", "GetGetNameHandler")]
			get {
				if (id_getName == IntPtr.Zero)
					id_getName = JNIEnv.GetMethodID (class_ref, "getName", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getName), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getName", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='setName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setName", "(Ljava/lang/String;)V", "GetSetName_Ljava_lang_String_Handler")]
			set {
				if (id_setName_Ljava_lang_String_ == IntPtr.Zero)
					id_setName_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setName", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setName_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setName", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		static Delegate cb_setUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setUuid_Ljava_lang_String_ == null)
				cb_setUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUuid_Ljava_lang_String_);
			return cb_setUuid_Ljava_lang_String_;
		}

		static void n_SetUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Uuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUuid;
		static IntPtr id_setUuid_Ljava_lang_String_;
		public virtual unsafe string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler")]
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='setUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUuid", "(Ljava/lang/String;)V", "GetSetUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getVisitID;
#pragma warning disable 0169
		static Delegate GetGetVisitIDHandler ()
		{
			if (cb_getVisitID == null)
				cb_getVisitID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetVisitID);
			return cb_getVisitID;
		}

		static IntPtr n_GetVisitID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.VisitID);
		}
#pragma warning restore 0169

		static Delegate cb_setVisitID_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetVisitID_Ljava_lang_String_Handler ()
		{
			if (cb_setVisitID_Ljava_lang_String_ == null)
				cb_setVisitID_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetVisitID_Ljava_lang_String_);
			return cb_setVisitID_Ljava_lang_String_;
		}

		static void n_SetVisitID_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.VisitID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getVisitID;
		static IntPtr id_setVisitID_Ljava_lang_String_;
		public virtual unsafe string VisitID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='getVisitID' and count(parameter)=0]"
			[Register ("getVisitID", "()Ljava/lang/String;", "GetGetVisitIDHandler")]
			get {
				if (id_getVisitID == IntPtr.Zero)
					id_getVisitID = JNIEnv.GetMethodID (class_ref, "getVisitID", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getVisitID), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getVisitID", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='setVisitID' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setVisitID", "(Ljava/lang/String;)V", "GetSetVisitID_Ljava_lang_String_Handler")]
			set {
				if (id_setVisitID_Ljava_lang_String_ == IntPtr.Zero)
					id_setVisitID_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setVisitID", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setVisitID_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setVisitID", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_addBeaconId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetAddBeaconId_Ljava_lang_String_Handler ()
		{
			if (cb_addBeaconId_Ljava_lang_String_ == null)
				cb_addBeaconId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddBeaconId_Ljava_lang_String_);
			return cb_addBeaconId_Ljava_lang_String_;
		}

		static void n_AddBeaconId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddBeaconId (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addBeaconId_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='addBeaconId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("addBeaconId", "(Ljava/lang/String;)V", "GetAddBeaconId_Ljava_lang_String_Handler")]
		public virtual unsafe void AddBeaconId (string p0)
		{
			if (id_addBeaconId_Ljava_lang_String_ == IntPtr.Zero)
				id_addBeaconId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "addBeaconId", "(Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addBeaconId_Ljava_lang_String_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addBeaconId", "(Ljava/lang/String;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_addBeaconIdCurrentlyIn_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetAddBeaconIdCurrentlyIn_Ljava_lang_String_Handler ()
		{
			if (cb_addBeaconIdCurrentlyIn_Ljava_lang_String_ == null)
				cb_addBeaconIdCurrentlyIn_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddBeaconIdCurrentlyIn_Ljava_lang_String_);
			return cb_addBeaconIdCurrentlyIn_Ljava_lang_String_;
		}

		static void n_AddBeaconIdCurrentlyIn_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddBeaconIdCurrentlyIn (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addBeaconIdCurrentlyIn_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='addBeaconIdCurrentlyIn' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("addBeaconIdCurrentlyIn", "(Ljava/lang/String;)V", "GetAddBeaconIdCurrentlyIn_Ljava_lang_String_Handler")]
		public virtual unsafe void AddBeaconIdCurrentlyIn (string p0)
		{
			if (id_addBeaconIdCurrentlyIn_Ljava_lang_String_ == IntPtr.Zero)
				id_addBeaconIdCurrentlyIn_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "addBeaconIdCurrentlyIn", "(Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addBeaconIdCurrentlyIn_Ljava_lang_String_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addBeaconIdCurrentlyIn", "(Ljava/lang/String;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_addGeofenceId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetAddGeofenceId_Ljava_lang_Long_Handler ()
		{
			if (cb_addGeofenceId_Ljava_lang_Long_ == null)
				cb_addGeofenceId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddGeofenceId_Ljava_lang_Long_);
			return cb_addGeofenceId_Ljava_lang_Long_;
		}

		static void n_AddGeofenceId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddGeofenceId (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addGeofenceId_Ljava_lang_Long_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='addGeofenceId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
		[Register ("addGeofenceId", "(Ljava/lang/Long;)V", "GetAddGeofenceId_Ljava_lang_Long_Handler")]
		public virtual unsafe void AddGeofenceId (global::Java.Lang.Long p0)
		{
			if (id_addGeofenceId_Ljava_lang_Long_ == IntPtr.Zero)
				id_addGeofenceId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "addGeofenceId", "(Ljava/lang/Long;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addGeofenceId_Ljava_lang_Long_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addGeofenceId", "(Ljava/lang/Long;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_addGeofenceIdCurrentlyIn_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetAddGeofenceIdCurrentlyIn_Ljava_lang_Long_Handler ()
		{
			if (cb_addGeofenceIdCurrentlyIn_Ljava_lang_Long_ == null)
				cb_addGeofenceIdCurrentlyIn_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddGeofenceIdCurrentlyIn_Ljava_lang_Long_);
			return cb_addGeofenceIdCurrentlyIn_Ljava_lang_Long_;
		}

		static void n_AddGeofenceIdCurrentlyIn_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddGeofenceIdCurrentlyIn (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addGeofenceIdCurrentlyIn_Ljava_lang_Long_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='addGeofenceIdCurrentlyIn' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
		[Register ("addGeofenceIdCurrentlyIn", "(Ljava/lang/Long;)V", "GetAddGeofenceIdCurrentlyIn_Ljava_lang_Long_Handler")]
		public virtual unsafe void AddGeofenceIdCurrentlyIn (global::Java.Lang.Long p0)
		{
			if (id_addGeofenceIdCurrentlyIn_Ljava_lang_Long_ == IntPtr.Zero)
				id_addGeofenceIdCurrentlyIn_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "addGeofenceIdCurrentlyIn", "(Ljava/lang/Long;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addGeofenceIdCurrentlyIn_Ljava_lang_Long_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addGeofenceIdCurrentlyIn", "(Ljava/lang/Long;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_isCurrentlyAtBeacon_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetIsCurrentlyAtBeacon_Ljava_lang_String_Handler ()
		{
			if (cb_isCurrentlyAtBeacon_Ljava_lang_String_ == null)
				cb_isCurrentlyAtBeacon_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_IsCurrentlyAtBeacon_Ljava_lang_String_);
			return cb_isCurrentlyAtBeacon_Ljava_lang_String_;
		}

		static bool n_IsCurrentlyAtBeacon_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.IsCurrentlyAtBeacon (p0);
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_isCurrentlyAtBeacon_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='isCurrentlyAtBeacon' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("isCurrentlyAtBeacon", "(Ljava/lang/String;)Z", "GetIsCurrentlyAtBeacon_Ljava_lang_String_Handler")]
		public virtual unsafe bool IsCurrentlyAtBeacon (string p0)
		{
			if (id_isCurrentlyAtBeacon_Ljava_lang_String_ == IntPtr.Zero)
				id_isCurrentlyAtBeacon_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "isCurrentlyAtBeacon", "(Ljava/lang/String;)Z");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				bool __ret;
				if (GetType () == ThresholdType)
					__ret = JNIEnv.CallBooleanMethod  (Handle, id_isCurrentlyAtBeacon_Ljava_lang_String_, __args);
				else
					__ret = JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isCurrentlyAtBeacon", "(Ljava/lang/String;)Z"), __args);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_isCurrentlyAtGeofence_J;
#pragma warning disable 0169
		static Delegate GetIsCurrentlyAtGeofence_JHandler ()
		{
			if (cb_isCurrentlyAtGeofence_J == null)
				cb_isCurrentlyAtGeofence_J = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long, bool>) n_IsCurrentlyAtGeofence_J);
			return cb_isCurrentlyAtGeofence_J;
		}

		static bool n_IsCurrentlyAtGeofence_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsCurrentlyAtGeofence (p0);
		}
#pragma warning restore 0169

		static IntPtr id_isCurrentlyAtGeofence_J;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='isCurrentlyAtGeofence' and count(parameter)=1 and parameter[1][@type='long']]"
		[Register ("isCurrentlyAtGeofence", "(J)Z", "GetIsCurrentlyAtGeofence_JHandler")]
		public virtual unsafe bool IsCurrentlyAtGeofence (long p0)
		{
			if (id_isCurrentlyAtGeofence_J == IntPtr.Zero)
				id_isCurrentlyAtGeofence_J = JNIEnv.GetMethodID (class_ref, "isCurrentlyAtGeofence", "(J)Z");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					return JNIEnv.CallBooleanMethod  (Handle, id_isCurrentlyAtGeofence_J, __args);
				else
					return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isCurrentlyAtGeofence", "(J)Z"), __args);
			} finally {
			}
		}

		static Delegate cb_removeBeaconIdCurrentlyIn_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetRemoveBeaconIdCurrentlyIn_Ljava_lang_String_Handler ()
		{
			if (cb_removeBeaconIdCurrentlyIn_Ljava_lang_String_ == null)
				cb_removeBeaconIdCurrentlyIn_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RemoveBeaconIdCurrentlyIn_Ljava_lang_String_);
			return cb_removeBeaconIdCurrentlyIn_Ljava_lang_String_;
		}

		static void n_RemoveBeaconIdCurrentlyIn_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveBeaconIdCurrentlyIn (p0);
		}
#pragma warning restore 0169

		static IntPtr id_removeBeaconIdCurrentlyIn_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='removeBeaconIdCurrentlyIn' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("removeBeaconIdCurrentlyIn", "(Ljava/lang/String;)V", "GetRemoveBeaconIdCurrentlyIn_Ljava_lang_String_Handler")]
		public virtual unsafe void RemoveBeaconIdCurrentlyIn (string p0)
		{
			if (id_removeBeaconIdCurrentlyIn_Ljava_lang_String_ == IntPtr.Zero)
				id_removeBeaconIdCurrentlyIn_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "removeBeaconIdCurrentlyIn", "(Ljava/lang/String;)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_removeBeaconIdCurrentlyIn_Ljava_lang_String_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "removeBeaconIdCurrentlyIn", "(Ljava/lang/String;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_removeGeofenceIdCurrentlyIn_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetRemoveGeofenceIdCurrentlyIn_Ljava_lang_Long_Handler ()
		{
			if (cb_removeGeofenceIdCurrentlyIn_Ljava_lang_Long_ == null)
				cb_removeGeofenceIdCurrentlyIn_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_RemoveGeofenceIdCurrentlyIn_Ljava_lang_Long_);
			return cb_removeGeofenceIdCurrentlyIn_Ljava_lang_Long_;
		}

		static void n_RemoveGeofenceIdCurrentlyIn_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RemoveGeofenceIdCurrentlyIn (p0);
		}
#pragma warning restore 0169

		static IntPtr id_removeGeofenceIdCurrentlyIn_Ljava_lang_Long_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='removeGeofenceIdCurrentlyIn' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
		[Register ("removeGeofenceIdCurrentlyIn", "(Ljava/lang/Long;)V", "GetRemoveGeofenceIdCurrentlyIn_Ljava_lang_Long_Handler")]
		public virtual unsafe void RemoveGeofenceIdCurrentlyIn (global::Java.Lang.Long p0)
		{
			if (id_removeGeofenceIdCurrentlyIn_Ljava_lang_Long_ == IntPtr.Zero)
				id_removeGeofenceIdCurrentlyIn_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "removeGeofenceIdCurrentlyIn", "(Ljava/lang/Long;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_removeGeofenceIdCurrentlyIn_Ljava_lang_Long_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "removeGeofenceIdCurrentlyIn", "(Ljava/lang/Long;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_status;
#pragma warning disable 0169
		static Delegate GetStatusHandler ()
		{
			if (cb_status == null)
				cb_status = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_Status);
			return cb_status;
		}

		static IntPtr n_Status (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Places.InternalPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Places.InternalPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Status ());
		}
#pragma warning restore 0169

		static IntPtr id_status;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.places']/class[@name='InternalPlace']/method[@name='status' and count(parameter)=0]"
		[Register ("status", "()Ljava/lang/String;", "GetStatusHandler")]
		public virtual unsafe string Status ()
		{
			if (id_status == IntPtr.Zero)
				id_status = JNIEnv.GetMethodID (class_ref, "status", "()Ljava/lang/String;");
			try {

				if (GetType () == ThresholdType)
					return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_status), JniHandleOwnership.TransferLocalRef);
				else
					return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "status", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

	}
}
