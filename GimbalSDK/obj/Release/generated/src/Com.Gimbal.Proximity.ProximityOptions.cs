using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityOptions']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/ProximityOptions", DoNotGenerateAcw=true)]
	public partial class ProximityOptions : global::Java.Lang.Object {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityOptions']/field[@name='BluetoothScanBackgroundOnly']"
		[Register ("BluetoothScanBackgroundOnly")]
		public const int BluetoothScanBackgroundOnly = (int) 1;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityOptions']/field[@name='BluetoothScanForegroundAndBackground']"
		[Register ("BluetoothScanForegroundAndBackground")]
		public const int BluetoothScanForegroundAndBackground = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityOptions']/field[@name='BluetoothScanModeKey']"
		[Register ("BluetoothScanModeKey")]
		public const string BluetoothScanModeKey = (string) "bluetooth-scan-mode-key";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityOptions']/field[@name='VisitOptionSignalStrengthWindowKey']"
		[Register ("VisitOptionSignalStrengthWindowKey")]
		public const string VisitOptionSignalStrengthWindowKey = (string) "sighting-options-signal-strength-key";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityOptions']/field[@name='VisitOptionSignalStrengthWindowLarge']"
		[Register ("VisitOptionSignalStrengthWindowLarge")]
		public const int VisitOptionSignalStrengthWindowLarge = (int) 10;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityOptions']/field[@name='VisitOptionSignalStrengthWindowMedium']"
		[Register ("VisitOptionSignalStrengthWindowMedium")]
		public const int VisitOptionSignalStrengthWindowMedium = (int) 5;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityOptions']/field[@name='VisitOptionSignalStrengthWindowNone']"
		[Register ("VisitOptionSignalStrengthWindowNone")]
		public const int VisitOptionSignalStrengthWindowNone = (int) 0;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityOptions']/field[@name='VisitOptionSignalStrengthWindowSmall']"
		[Register ("VisitOptionSignalStrengthWindowSmall")]
		public const int VisitOptionSignalStrengthWindowSmall = (int) 2;
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/ProximityOptions", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ProximityOptions); }
		}

		protected ProximityOptions (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityOptions']/constructor[@name='ProximityOptions' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ProximityOptions ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ProximityOptions)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getOptions;
#pragma warning disable 0169
		static Delegate GetGetOptionsHandler ()
		{
			if (cb_getOptions == null)
				cb_getOptions = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOptions);
			return cb_getOptions;
		}

		static IntPtr n_GetOptions (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.ProximityOptions __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityOptions> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaDictionary<string, global::Java.Lang.Integer>.ToLocalJniHandle (__this.Options);
		}
#pragma warning restore 0169

		static IntPtr id_getOptions;
		public virtual unsafe global::System.Collections.Generic.IDictionary<string, global::Java.Lang.Integer> Options {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityOptions']/method[@name='getOptions' and count(parameter)=0]"
			[Register ("getOptions", "()Ljava/util/Map;", "GetGetOptionsHandler")]
			get {
				if (id_getOptions == IntPtr.Zero)
					id_getOptions = JNIEnv.GetMethodID (class_ref, "getOptions", "()Ljava/util/Map;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaDictionary<string, global::Java.Lang.Integer>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getOptions), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaDictionary<string, global::Java.Lang.Integer>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOptions", "()Ljava/util/Map;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_setOption_Ljava_lang_String_I;
#pragma warning disable 0169
		static Delegate GetSetOption_Ljava_lang_String_IHandler ()
		{
			if (cb_setOption_Ljava_lang_String_I == null)
				cb_setOption_Ljava_lang_String_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, int>) n_SetOption_Ljava_lang_String_I);
			return cb_setOption_Ljava_lang_String_I;
		}

		static void n_SetOption_Ljava_lang_String_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, int p1)
		{
			global::Com.Gimbal.Proximity.ProximityOptions __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityOptions> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetOption (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_setOption_Ljava_lang_String_I;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityOptions']/method[@name='setOption' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='int']]"
		[Register ("setOption", "(Ljava/lang/String;I)V", "GetSetOption_Ljava_lang_String_IHandler")]
		public virtual unsafe void SetOption (string p0, int p1)
		{
			if (id_setOption_Ljava_lang_String_I == IntPtr.Zero)
				id_setOption_Ljava_lang_String_I = JNIEnv.GetMethodID (class_ref, "setOption", "(Ljava/lang/String;I)V");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (p1);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setOption_Ljava_lang_String_I, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOption", "(Ljava/lang/String;I)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
