using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Service.Protocol.Parser {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.parser']/interface[@name='ServerErrorParser']"
	[Register ("com/gimbal/proximity/core/service/protocol/parser/ServerErrorParser", "", "Com.Gimbal.Proximity.Core.Service.Protocol.Parser.IServerErrorParserInvoker")]
	public partial interface IServerErrorParser : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.parser']/interface[@name='ServerErrorParser']/method[@name='parse' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("parse", "(Ljava/lang/String;)Lcom/gimbal/proximity/core/service/protocol/ServerError;", "GetParse_Ljava_lang_String_Handler:Com.Gimbal.Proximity.Core.Service.Protocol.Parser.IServerErrorParserInvoker, GimbalSDK")]
		global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError Parse (string p0);

	}

	[global::Android.Runtime.Register ("com/gimbal/proximity/core/service/protocol/parser/ServerErrorParser", DoNotGenerateAcw=true)]
	internal class IServerErrorParserInvoker : global::Java.Lang.Object, IServerErrorParser {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/gimbal/proximity/core/service/protocol/parser/ServerErrorParser");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IServerErrorParserInvoker); }
		}

		IntPtr class_ref;

		public static IServerErrorParser GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IServerErrorParser> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.gimbal.proximity.core.service.protocol.parser.ServerErrorParser"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IServerErrorParserInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_parse_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetParse_Ljava_lang_String_Handler ()
		{
			if (cb_parse_Ljava_lang_String_ == null)
				cb_parse_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_Parse_Ljava_lang_String_);
			return cb_parse_Ljava_lang_String_;
		}

		static IntPtr n_Parse_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.Parser.IServerErrorParser __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.Parser.IServerErrorParser> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.Parse (p0));
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_parse_Ljava_lang_String_;
		public unsafe global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError Parse (string p0)
		{
			if (id_parse_Ljava_lang_String_ == IntPtr.Zero)
				id_parse_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "parse", "(Ljava/lang/String;)Lcom/gimbal/proximity/core/service/protocol/ServerError;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (native_p0);
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError __ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError> (JNIEnv.CallObjectMethod (Handle, id_parse_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
			JNIEnv.DeleteLocalRef (native_p0);
			return __ret;
		}

	}

}
