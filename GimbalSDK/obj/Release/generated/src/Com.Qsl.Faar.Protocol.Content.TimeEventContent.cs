using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Content {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='TimeEventContent']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/content/TimeEventContent", DoNotGenerateAcw=true)]
	public partial class TimeEventContent : global::Java.Lang.Object, global::Com.Qsl.Faar.Protocol.Content.IEventContent {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/content/TimeEventContent", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (TimeEventContent); }
		}

		protected TimeEventContent (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_qsl_faar_protocol_TimeEvent_Ljava_util_List_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='TimeEventContent']/constructor[@name='TimeEventContent' and count(parameter)=2 and parameter[1][@type='com.qsl.faar.protocol.TimeEvent'] and parameter[2][@type='java.util.List&lt;com.qsl.faar.protocol.content.ContentDescriptor&gt;']]"
		[Register (".ctor", "(Lcom/qsl/faar/protocol/TimeEvent;Ljava/util/List;)V", "")]
		public unsafe TimeEventContent (global::Com.Qsl.Faar.Protocol.TimeEvent p0, global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> p1)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			IntPtr native_p1 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.ToLocalJniHandle (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				if (GetType () != typeof (TimeEventContent)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Lcom/qsl/faar/protocol/TimeEvent;Ljava/util/List;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Lcom/qsl/faar/protocol/TimeEvent;Ljava/util/List;)V", __args);
					return;
				}

				if (id_ctor_Lcom_qsl_faar_protocol_TimeEvent_Ljava_util_List_ == IntPtr.Zero)
					id_ctor_Lcom_qsl_faar_protocol_TimeEvent_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/qsl/faar/protocol/TimeEvent;Ljava/util/List;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_qsl_faar_protocol_TimeEvent_Ljava_util_List_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Lcom_qsl_faar_protocol_TimeEvent_Ljava_util_List_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='TimeEventContent']/constructor[@name='TimeEventContent' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe TimeEventContent ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (TimeEventContent)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getContentDescriptors;
#pragma warning disable 0169
		static Delegate GetGetContentDescriptorsHandler ()
		{
			if (cb_getContentDescriptors == null)
				cb_getContentDescriptors = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetContentDescriptors);
			return cb_getContentDescriptors;
		}

		static IntPtr n_GetContentDescriptors (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.TimeEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.ToLocalJniHandle (__this.ContentDescriptors);
		}
#pragma warning restore 0169

		static Delegate cb_setContentDescriptors_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetContentDescriptors_Ljava_util_List_Handler ()
		{
			if (cb_setContentDescriptors_Ljava_util_List_ == null)
				cb_setContentDescriptors_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetContentDescriptors_Ljava_util_List_);
			return cb_setContentDescriptors_Ljava_util_List_;
		}

		static void n_SetContentDescriptors_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.TimeEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ContentDescriptors = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getContentDescriptors;
		static IntPtr id_setContentDescriptors_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> ContentDescriptors {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='TimeEventContent']/method[@name='getContentDescriptors' and count(parameter)=0]"
			[Register ("getContentDescriptors", "()Ljava/util/List;", "GetGetContentDescriptorsHandler")]
			get {
				if (id_getContentDescriptors == IntPtr.Zero)
					id_getContentDescriptors = JNIEnv.GetMethodID (class_ref, "getContentDescriptors", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getContentDescriptors), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getContentDescriptors", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='TimeEventContent']/method[@name='setContentDescriptors' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.qsl.faar.protocol.content.ContentDescriptor&gt;']]"
			[Register ("setContentDescriptors", "(Ljava/util/List;)V", "GetSetContentDescriptors_Ljava_util_List_Handler")]
			set {
				if (id_setContentDescriptors_Ljava_util_List_ == IntPtr.Zero)
					id_setContentDescriptors_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setContentDescriptors", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setContentDescriptors_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setContentDescriptors", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getEventTime;
#pragma warning disable 0169
		static Delegate GetGetEventTimeHandler ()
		{
			if (cb_getEventTime == null)
				cb_getEventTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEventTime);
			return cb_getEventTime;
		}

		static IntPtr n_GetEventTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.TimeEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.EventTime);
		}
#pragma warning restore 0169

		static IntPtr id_getEventTime;
		public virtual unsafe global::Java.Lang.Long EventTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='TimeEventContent']/method[@name='getEventTime' and count(parameter)=0]"
			[Register ("getEventTime", "()Ljava/lang/Long;", "GetGetEventTimeHandler")]
			get {
				if (id_getEventTime == IntPtr.Zero)
					id_getEventTime = JNIEnv.GetMethodID (class_ref, "getEventTime", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getEventTime), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEventTime", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getOrganizationId;
#pragma warning disable 0169
		static Delegate GetGetOrganizationIdHandler ()
		{
			if (cb_getOrganizationId == null)
				cb_getOrganizationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationId);
			return cb_getOrganizationId;
		}

		static IntPtr n_GetOrganizationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.TimeEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OrganizationId);
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationId;
		public virtual unsafe global::Java.Lang.Long OrganizationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='TimeEventContent']/method[@name='getOrganizationId' and count(parameter)=0]"
			[Register ("getOrganizationId", "()Ljava/lang/Long;", "GetGetOrganizationIdHandler")]
			get {
				if (id_getOrganizationId == IntPtr.Zero)
					id_getOrganizationId = JNIEnv.GetMethodID (class_ref, "getOrganizationId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getPlaceId;
#pragma warning disable 0169
		static Delegate GetGetPlaceIdHandler ()
		{
			if (cb_getPlaceId == null)
				cb_getPlaceId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlaceId);
			return cb_getPlaceId;
		}

		static IntPtr n_GetPlaceId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.TimeEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.PlaceId);
		}
#pragma warning restore 0169

		static IntPtr id_getPlaceId;
		public virtual unsafe global::Java.Lang.Long PlaceId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='TimeEventContent']/method[@name='getPlaceId' and count(parameter)=0]"
			[Register ("getPlaceId", "()Ljava/lang/Long;", "GetGetPlaceIdHandler")]
			get {
				if (id_getPlaceId == IntPtr.Zero)
					id_getPlaceId = JNIEnv.GetMethodID (class_ref, "getPlaceId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getPlaceId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlaceId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getTimeEvent;
#pragma warning disable 0169
		static Delegate GetGetTimeEventHandler ()
		{
			if (cb_getTimeEvent == null)
				cb_getTimeEvent = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTimeEvent);
			return cb_getTimeEvent;
		}

		static IntPtr n_GetTimeEvent (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.TimeEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.TimeEvent);
		}
#pragma warning restore 0169

		static Delegate cb_setTimeEvent_Lcom_qsl_faar_protocol_TimeEvent_;
#pragma warning disable 0169
		static Delegate GetSetTimeEvent_Lcom_qsl_faar_protocol_TimeEvent_Handler ()
		{
			if (cb_setTimeEvent_Lcom_qsl_faar_protocol_TimeEvent_ == null)
				cb_setTimeEvent_Lcom_qsl_faar_protocol_TimeEvent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTimeEvent_Lcom_qsl_faar_protocol_TimeEvent_);
			return cb_setTimeEvent_Lcom_qsl_faar_protocol_TimeEvent_;
		}

		static void n_SetTimeEvent_Lcom_qsl_faar_protocol_TimeEvent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.TimeEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.TimeEvent p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.TimeEvent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.TimeEvent = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTimeEvent;
		static IntPtr id_setTimeEvent_Lcom_qsl_faar_protocol_TimeEvent_;
		public virtual unsafe global::Com.Qsl.Faar.Protocol.TimeEvent TimeEvent {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='TimeEventContent']/method[@name='getTimeEvent' and count(parameter)=0]"
			[Register ("getTimeEvent", "()Lcom/qsl/faar/protocol/TimeEvent;", "GetGetTimeEventHandler")]
			get {
				if (id_getTimeEvent == IntPtr.Zero)
					id_getTimeEvent = JNIEnv.GetMethodID (class_ref, "getTimeEvent", "()Lcom/qsl/faar/protocol/TimeEvent;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.TimeEvent> (JNIEnv.CallObjectMethod  (Handle, id_getTimeEvent), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.TimeEvent> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTimeEvent", "()Lcom/qsl/faar/protocol/TimeEvent;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='TimeEventContent']/method[@name='setTimeEvent' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.TimeEvent']]"
			[Register ("setTimeEvent", "(Lcom/qsl/faar/protocol/TimeEvent;)V", "GetSetTimeEvent_Lcom_qsl_faar_protocol_TimeEvent_Handler")]
			set {
				if (id_setTimeEvent_Lcom_qsl_faar_protocol_TimeEvent_ == IntPtr.Zero)
					id_setTimeEvent_Lcom_qsl_faar_protocol_TimeEvent_ = JNIEnv.GetMethodID (class_ref, "setTimeEvent", "(Lcom/qsl/faar/protocol/TimeEvent;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTimeEvent_Lcom_qsl_faar_protocol_TimeEvent_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTimeEvent", "(Lcom/qsl/faar/protocol/TimeEvent;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getType;
#pragma warning disable 0169
		static Delegate GetGetTypeHandler ()
		{
			if (cb_getType == null)
				cb_getType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetType);
			return cb_getType;
		}

		static IntPtr n_GetType (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.TimeEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.TimeEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Type);
		}
#pragma warning restore 0169

		static IntPtr id_getType;
		public virtual unsafe string Type {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='TimeEventContent']/method[@name='getType' and count(parameter)=0]"
			[Register ("getType", "()Ljava/lang/String;", "GetGetTypeHandler")]
			get {
				if (id_getType == IntPtr.Zero)
					id_getType = JNIEnv.GetMethodID (class_ref, "getType", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getType), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getType", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

	}
}
