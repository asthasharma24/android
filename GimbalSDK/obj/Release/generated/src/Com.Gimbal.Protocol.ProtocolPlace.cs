using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolPlace']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/ProtocolPlace", DoNotGenerateAcw=true)]
	public partial class ProtocolPlace : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/ProtocolPlace", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ProtocolPlace); }
		}

		protected ProtocolPlace (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolPlace']/constructor[@name='ProtocolPlace' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ProtocolPlace ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ProtocolPlace)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getAttributes;
#pragma warning disable 0169
		static Delegate GetGetAttributesHandler ()
		{
			if (cb_getAttributes == null)
				cb_getAttributes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetAttributes);
			return cb_getAttributes;
		}

		static IntPtr n_GetAttributes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.ProtocolPlaceAttribute>.ToLocalJniHandle (__this.Attributes);
		}
#pragma warning restore 0169

		static Delegate cb_setAttributes_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetAttributes_Ljava_util_List_Handler ()
		{
			if (cb_setAttributes_Ljava_util_List_ == null)
				cb_setAttributes_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetAttributes_Ljava_util_List_);
			return cb_setAttributes_Ljava_util_List_;
		}

		static void n_SetAttributes_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.ProtocolPlaceAttribute>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Attributes = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getAttributes;
		static IntPtr id_setAttributes_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Protocol.ProtocolPlaceAttribute> Attributes {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolPlace']/method[@name='getAttributes' and count(parameter)=0]"
			[Register ("getAttributes", "()Ljava/util/List;", "GetGetAttributesHandler")]
			get {
				if (id_getAttributes == IntPtr.Zero)
					id_getAttributes = JNIEnv.GetMethodID (class_ref, "getAttributes", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.ProtocolPlaceAttribute>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getAttributes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.ProtocolPlaceAttribute>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getAttributes", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolPlace']/method[@name='setAttributes' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.protocol.ProtocolPlaceAttribute&gt;']]"
			[Register ("setAttributes", "(Ljava/util/List;)V", "GetSetAttributes_Ljava_util_List_Handler")]
			set {
				if (id_setAttributes_Ljava_util_List_ == IntPtr.Zero)
					id_setAttributes_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setAttributes", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.ProtocolPlaceAttribute>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setAttributes_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setAttributes", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getBeaconSettings;
#pragma warning disable 0169
		static Delegate GetGetBeaconSettingsHandler ()
		{
			if (cb_getBeaconSettings == null)
				cb_getBeaconSettings = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBeaconSettings);
			return cb_getBeaconSettings;
		}

		static IntPtr n_GetBeaconSettings (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.BeaconSettings);
		}
#pragma warning restore 0169

		static Delegate cb_setBeaconSettings_Lcom_gimbal_protocol_ProtocolBeaconSettings_;
#pragma warning disable 0169
		static Delegate GetSetBeaconSettings_Lcom_gimbal_protocol_ProtocolBeaconSettings_Handler ()
		{
			if (cb_setBeaconSettings_Lcom_gimbal_protocol_ProtocolBeaconSettings_ == null)
				cb_setBeaconSettings_Lcom_gimbal_protocol_ProtocolBeaconSettings_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetBeaconSettings_Lcom_gimbal_protocol_ProtocolBeaconSettings_);
			return cb_setBeaconSettings_Lcom_gimbal_protocol_ProtocolBeaconSettings_;
		}

		static void n_SetBeaconSettings_Lcom_gimbal_protocol_ProtocolBeaconSettings_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Protocol.ProtocolBeaconSettings p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolBeaconSettings> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.BeaconSettings = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getBeaconSettings;
		static IntPtr id_setBeaconSettings_Lcom_gimbal_protocol_ProtocolBeaconSettings_;
		public virtual unsafe global::Com.Gimbal.Protocol.ProtocolBeaconSettings BeaconSettings {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolPlace']/method[@name='getBeaconSettings' and count(parameter)=0]"
			[Register ("getBeaconSettings", "()Lcom/gimbal/protocol/ProtocolBeaconSettings;", "GetGetBeaconSettingsHandler")]
			get {
				if (id_getBeaconSettings == IntPtr.Zero)
					id_getBeaconSettings = JNIEnv.GetMethodID (class_ref, "getBeaconSettings", "()Lcom/gimbal/protocol/ProtocolBeaconSettings;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolBeaconSettings> (JNIEnv.CallObjectMethod  (Handle, id_getBeaconSettings), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolBeaconSettings> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBeaconSettings", "()Lcom/gimbal/protocol/ProtocolBeaconSettings;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolPlace']/method[@name='setBeaconSettings' and count(parameter)=1 and parameter[1][@type='com.gimbal.protocol.ProtocolBeaconSettings']]"
			[Register ("setBeaconSettings", "(Lcom/gimbal/protocol/ProtocolBeaconSettings;)V", "GetSetBeaconSettings_Lcom_gimbal_protocol_ProtocolBeaconSettings_Handler")]
			set {
				if (id_setBeaconSettings_Lcom_gimbal_protocol_ProtocolBeaconSettings_ == IntPtr.Zero)
					id_setBeaconSettings_Lcom_gimbal_protocol_ProtocolBeaconSettings_ = JNIEnv.GetMethodID (class_ref, "setBeaconSettings", "(Lcom/gimbal/protocol/ProtocolBeaconSettings;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setBeaconSettings_Lcom_gimbal_protocol_ProtocolBeaconSettings_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setBeaconSettings", "(Lcom/gimbal/protocol/ProtocolBeaconSettings;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_Long_Handler ()
		{
			if (cb_setId_Ljava_lang_Long_ == null)
				cb_setId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_Long_);
			return cb_setId_Ljava_lang_Long_;
		}

		static void n_SetId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolPlace']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/Long;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolPlace']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setId", "(Ljava/lang/Long;)V", "GetSetId_Ljava_lang_Long_Handler")]
			set {
				if (id_setId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getName;
#pragma warning disable 0169
		static Delegate GetGetNameHandler ()
		{
			if (cb_getName == null)
				cb_getName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetName);
			return cb_getName;
		}

		static IntPtr n_GetName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Name);
		}
#pragma warning restore 0169

		static Delegate cb_setName_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetName_Ljava_lang_String_Handler ()
		{
			if (cb_setName_Ljava_lang_String_ == null)
				cb_setName_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetName_Ljava_lang_String_);
			return cb_setName_Ljava_lang_String_;
		}

		static void n_SetName_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Name = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getName;
		static IntPtr id_setName_Ljava_lang_String_;
		public virtual unsafe string Name {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolPlace']/method[@name='getName' and count(parameter)=0]"
			[Register ("getName", "()Ljava/lang/String;", "GetGetNameHandler")]
			get {
				if (id_getName == IntPtr.Zero)
					id_getName = JNIEnv.GetMethodID (class_ref, "getName", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getName), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getName", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolPlace']/method[@name='setName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setName", "(Ljava/lang/String;)V", "GetSetName_Ljava_lang_String_Handler")]
			set {
				if (id_setName_Ljava_lang_String_ == IntPtr.Zero)
					id_setName_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setName", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setName_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setName", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		static Delegate cb_setUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setUuid_Ljava_lang_String_ == null)
				cb_setUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUuid_Ljava_lang_String_);
			return cb_setUuid_Ljava_lang_String_;
		}

		static void n_SetUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolPlace __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolPlace> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Uuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUuid;
		static IntPtr id_setUuid_Ljava_lang_String_;
		public virtual unsafe string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolPlace']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler")]
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolPlace']/method[@name='setUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUuid", "(Ljava/lang/String;)V", "GetSetUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
