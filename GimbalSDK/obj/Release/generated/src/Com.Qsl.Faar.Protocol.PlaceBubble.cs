using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceBubble']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/PlaceBubble", DoNotGenerateAcw=true)]
	public partial class PlaceBubble : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/PlaceBubble", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PlaceBubble); }
		}

		protected PlaceBubble (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceBubble']/constructor[@name='PlaceBubble' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe PlaceBubble ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (PlaceBubble)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getBoundary;
#pragma warning disable 0169
		static Delegate GetGetBoundaryHandler ()
		{
			if (cb_getBoundary == null)
				cb_getBoundary = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBoundary);
			return cb_getBoundary;
		}

		static IntPtr n_GetBoundary (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PlaceBubble __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PlaceBubble> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Boundary);
		}
#pragma warning restore 0169

		static Delegate cb_setBoundary_Lcom_qsl_faar_protocol_GeoCircle_;
#pragma warning disable 0169
		static Delegate GetSetBoundary_Lcom_qsl_faar_protocol_GeoCircle_Handler ()
		{
			if (cb_setBoundary_Lcom_qsl_faar_protocol_GeoCircle_ == null)
				cb_setBoundary_Lcom_qsl_faar_protocol_GeoCircle_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetBoundary_Lcom_qsl_faar_protocol_GeoCircle_);
			return cb_setBoundary_Lcom_qsl_faar_protocol_GeoCircle_;
		}

		static void n_SetBoundary_Lcom_qsl_faar_protocol_GeoCircle_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PlaceBubble __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PlaceBubble> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.GeoCircle p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoCircle> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Boundary = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getBoundary;
		static IntPtr id_setBoundary_Lcom_qsl_faar_protocol_GeoCircle_;
		public virtual unsafe global::Com.Qsl.Faar.Protocol.GeoCircle Boundary {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceBubble']/method[@name='getBoundary' and count(parameter)=0]"
			[Register ("getBoundary", "()Lcom/qsl/faar/protocol/GeoCircle;", "GetGetBoundaryHandler")]
			get {
				if (id_getBoundary == IntPtr.Zero)
					id_getBoundary = JNIEnv.GetMethodID (class_ref, "getBoundary", "()Lcom/qsl/faar/protocol/GeoCircle;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoCircle> (JNIEnv.CallObjectMethod  (Handle, id_getBoundary), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.GeoCircle> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBoundary", "()Lcom/qsl/faar/protocol/GeoCircle;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceBubble']/method[@name='setBoundary' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.GeoCircle']]"
			[Register ("setBoundary", "(Lcom/qsl/faar/protocol/GeoCircle;)V", "GetSetBoundary_Lcom_qsl_faar_protocol_GeoCircle_Handler")]
			set {
				if (id_setBoundary_Lcom_qsl_faar_protocol_GeoCircle_ == IntPtr.Zero)
					id_setBoundary_Lcom_qsl_faar_protocol_GeoCircle_ = JNIEnv.GetMethodID (class_ref, "setBoundary", "(Lcom/qsl/faar/protocol/GeoCircle;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setBoundary_Lcom_qsl_faar_protocol_GeoCircle_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setBoundary", "(Lcom/qsl/faar/protocol/GeoCircle;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getOrganizationPlaces;
#pragma warning disable 0169
		static Delegate GetGetOrganizationPlacesHandler ()
		{
			if (cb_getOrganizationPlaces == null)
				cb_getOrganizationPlaces = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationPlaces);
			return cb_getOrganizationPlaces;
		}

		static IntPtr n_GetOrganizationPlaces (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PlaceBubble __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PlaceBubble> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.OrganizationPlace>.ToLocalJniHandle (__this.OrganizationPlaces);
		}
#pragma warning restore 0169

		static Delegate cb_setOrganizationPlaces_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationPlaces_Ljava_util_List_Handler ()
		{
			if (cb_setOrganizationPlaces_Ljava_util_List_ == null)
				cb_setOrganizationPlaces_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationPlaces_Ljava_util_List_);
			return cb_setOrganizationPlaces_Ljava_util_List_;
		}

		static void n_SetOrganizationPlaces_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PlaceBubble __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PlaceBubble> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.OrganizationPlace>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OrganizationPlaces = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationPlaces;
		static IntPtr id_setOrganizationPlaces_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.OrganizationPlace> OrganizationPlaces {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceBubble']/method[@name='getOrganizationPlaces' and count(parameter)=0]"
			[Register ("getOrganizationPlaces", "()Ljava/util/List;", "GetGetOrganizationPlacesHandler")]
			get {
				if (id_getOrganizationPlaces == IntPtr.Zero)
					id_getOrganizationPlaces = JNIEnv.GetMethodID (class_ref, "getOrganizationPlaces", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.OrganizationPlace>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationPlaces), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.OrganizationPlace>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationPlaces", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PlaceBubble']/method[@name='setOrganizationPlaces' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.qsl.faar.protocol.OrganizationPlace&gt;']]"
			[Register ("setOrganizationPlaces", "(Ljava/util/List;)V", "GetSetOrganizationPlaces_Ljava_util_List_Handler")]
			set {
				if (id_setOrganizationPlaces_Ljava_util_List_ == IntPtr.Zero)
					id_setOrganizationPlaces_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setOrganizationPlaces", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.OrganizationPlace>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOrganizationPlaces_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationPlaces", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
