using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/Registration", DoNotGenerateAcw=true)]
	public partial class Registration : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/Registration", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Registration); }
		}

		protected Registration (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/constructor[@name='Registration' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Registration ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Registration)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getApiKey;
#pragma warning disable 0169
		static Delegate GetGetApiKeyHandler ()
		{
			if (cb_getApiKey == null)
				cb_getApiKey = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApiKey);
			return cb_getApiKey;
		}

		static IntPtr n_GetApiKey (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ApiKey);
		}
#pragma warning restore 0169

		static Delegate cb_setApiKey_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetApiKey_Ljava_lang_String_Handler ()
		{
			if (cb_setApiKey_Ljava_lang_String_ == null)
				cb_setApiKey_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApiKey_Ljava_lang_String_);
			return cb_setApiKey_Ljava_lang_String_;
		}

		static void n_SetApiKey_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApiKey = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApiKey;
		static IntPtr id_setApiKey_Ljava_lang_String_;
		public virtual unsafe string ApiKey {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getApiKey' and count(parameter)=0]"
			[Register ("getApiKey", "()Ljava/lang/String;", "GetGetApiKeyHandler")]
			get {
				if (id_getApiKey == IntPtr.Zero)
					id_getApiKey = JNIEnv.GetMethodID (class_ref, "getApiKey", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getApiKey), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApiKey", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setApiKey' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setApiKey", "(Ljava/lang/String;)V", "GetSetApiKey_Ljava_lang_String_Handler")]
			set {
				if (id_setApiKey_Ljava_lang_String_ == IntPtr.Zero)
					id_setApiKey_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setApiKey", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApiKey_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApiKey", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getApplicationConfiguration;
#pragma warning disable 0169
		static Delegate GetGetApplicationConfigurationHandler ()
		{
			if (cb_getApplicationConfiguration == null)
				cb_getApplicationConfiguration = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApplicationConfiguration);
			return cb_getApplicationConfiguration;
		}

		static IntPtr n_GetApplicationConfiguration (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ApplicationConfiguration);
		}
#pragma warning restore 0169

		static Delegate cb_setApplicationConfiguration_Lcom_gimbal_protocol_ApplicationConfiguration_;
#pragma warning disable 0169
		static Delegate GetSetApplicationConfiguration_Lcom_gimbal_protocol_ApplicationConfiguration_Handler ()
		{
			if (cb_setApplicationConfiguration_Lcom_gimbal_protocol_ApplicationConfiguration_ == null)
				cb_setApplicationConfiguration_Lcom_gimbal_protocol_ApplicationConfiguration_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApplicationConfiguration_Lcom_gimbal_protocol_ApplicationConfiguration_);
			return cb_setApplicationConfiguration_Lcom_gimbal_protocol_ApplicationConfiguration_;
		}

		static void n_SetApplicationConfiguration_Lcom_gimbal_protocol_ApplicationConfiguration_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Protocol.ApplicationConfiguration p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApplicationConfiguration = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApplicationConfiguration;
		static IntPtr id_setApplicationConfiguration_Lcom_gimbal_protocol_ApplicationConfiguration_;
		public virtual unsafe global::Com.Gimbal.Protocol.ApplicationConfiguration ApplicationConfiguration {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getApplicationConfiguration' and count(parameter)=0]"
			[Register ("getApplicationConfiguration", "()Lcom/gimbal/protocol/ApplicationConfiguration;", "GetGetApplicationConfigurationHandler")]
			get {
				if (id_getApplicationConfiguration == IntPtr.Zero)
					id_getApplicationConfiguration = JNIEnv.GetMethodID (class_ref, "getApplicationConfiguration", "()Lcom/gimbal/protocol/ApplicationConfiguration;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (JNIEnv.CallObjectMethod  (Handle, id_getApplicationConfiguration), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ApplicationConfiguration> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApplicationConfiguration", "()Lcom/gimbal/protocol/ApplicationConfiguration;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setApplicationConfiguration' and count(parameter)=1 and parameter[1][@type='com.gimbal.protocol.ApplicationConfiguration']]"
			[Register ("setApplicationConfiguration", "(Lcom/gimbal/protocol/ApplicationConfiguration;)V", "GetSetApplicationConfiguration_Lcom_gimbal_protocol_ApplicationConfiguration_Handler")]
			set {
				if (id_setApplicationConfiguration_Lcom_gimbal_protocol_ApplicationConfiguration_ == IntPtr.Zero)
					id_setApplicationConfiguration_Lcom_gimbal_protocol_ApplicationConfiguration_ = JNIEnv.GetMethodID (class_ref, "setApplicationConfiguration", "(Lcom/gimbal/protocol/ApplicationConfiguration;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApplicationConfiguration_Lcom_gimbal_protocol_ApplicationConfiguration_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApplicationConfiguration", "(Lcom/gimbal/protocol/ApplicationConfiguration;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getApplicationID;
#pragma warning disable 0169
		static Delegate GetGetApplicationIDHandler ()
		{
			if (cb_getApplicationID == null)
				cb_getApplicationID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApplicationID);
			return cb_getApplicationID;
		}

		static IntPtr n_GetApplicationID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ApplicationID);
		}
#pragma warning restore 0169

		static Delegate cb_setApplicationID_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetApplicationID_Ljava_lang_Long_Handler ()
		{
			if (cb_setApplicationID_Ljava_lang_Long_ == null)
				cb_setApplicationID_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApplicationID_Ljava_lang_Long_);
			return cb_setApplicationID_Ljava_lang_Long_;
		}

		static void n_SetApplicationID_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApplicationID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApplicationID;
		static IntPtr id_setApplicationID_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long ApplicationID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getApplicationID' and count(parameter)=0]"
			[Register ("getApplicationID", "()Ljava/lang/Long;", "GetGetApplicationIDHandler")]
			get {
				if (id_getApplicationID == IntPtr.Zero)
					id_getApplicationID = JNIEnv.GetMethodID (class_ref, "getApplicationID", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getApplicationID), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApplicationID", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setApplicationID' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setApplicationID", "(Ljava/lang/Long;)V", "GetSetApplicationID_Ljava_lang_Long_Handler")]
			set {
				if (id_setApplicationID_Ljava_lang_Long_ == IntPtr.Zero)
					id_setApplicationID_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setApplicationID", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApplicationID_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApplicationID", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getApplicationIdentifier;
#pragma warning disable 0169
		static Delegate GetGetApplicationIdentifierHandler ()
		{
			if (cb_getApplicationIdentifier == null)
				cb_getApplicationIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApplicationIdentifier);
			return cb_getApplicationIdentifier;
		}

		static IntPtr n_GetApplicationIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ApplicationIdentifier);
		}
#pragma warning restore 0169

		static Delegate cb_setApplicationIdentifier_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetApplicationIdentifier_Ljava_lang_String_Handler ()
		{
			if (cb_setApplicationIdentifier_Ljava_lang_String_ == null)
				cb_setApplicationIdentifier_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApplicationIdentifier_Ljava_lang_String_);
			return cb_setApplicationIdentifier_Ljava_lang_String_;
		}

		static void n_SetApplicationIdentifier_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApplicationIdentifier = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApplicationIdentifier;
		static IntPtr id_setApplicationIdentifier_Ljava_lang_String_;
		public virtual unsafe string ApplicationIdentifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getApplicationIdentifier' and count(parameter)=0]"
			[Register ("getApplicationIdentifier", "()Ljava/lang/String;", "GetGetApplicationIdentifierHandler")]
			get {
				if (id_getApplicationIdentifier == IntPtr.Zero)
					id_getApplicationIdentifier = JNIEnv.GetMethodID (class_ref, "getApplicationIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getApplicationIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApplicationIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setApplicationIdentifier' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setApplicationIdentifier", "(Ljava/lang/String;)V", "GetSetApplicationIdentifier_Ljava_lang_String_Handler")]
			set {
				if (id_setApplicationIdentifier_Ljava_lang_String_ == IntPtr.Zero)
					id_setApplicationIdentifier_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setApplicationIdentifier", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApplicationIdentifier_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApplicationIdentifier", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getApplicationInstanceIdentifier;
#pragma warning disable 0169
		static Delegate GetGetApplicationInstanceIdentifierHandler ()
		{
			if (cb_getApplicationInstanceIdentifier == null)
				cb_getApplicationInstanceIdentifier = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApplicationInstanceIdentifier);
			return cb_getApplicationInstanceIdentifier;
		}

		static IntPtr n_GetApplicationInstanceIdentifier (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ApplicationInstanceIdentifier);
		}
#pragma warning restore 0169

		static Delegate cb_setApplicationInstanceIdentifier_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetApplicationInstanceIdentifier_Ljava_lang_String_Handler ()
		{
			if (cb_setApplicationInstanceIdentifier_Ljava_lang_String_ == null)
				cb_setApplicationInstanceIdentifier_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApplicationInstanceIdentifier_Ljava_lang_String_);
			return cb_setApplicationInstanceIdentifier_Ljava_lang_String_;
		}

		static void n_SetApplicationInstanceIdentifier_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApplicationInstanceIdentifier = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApplicationInstanceIdentifier;
		static IntPtr id_setApplicationInstanceIdentifier_Ljava_lang_String_;
		public virtual unsafe string ApplicationInstanceIdentifier {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getApplicationInstanceIdentifier' and count(parameter)=0]"
			[Register ("getApplicationInstanceIdentifier", "()Ljava/lang/String;", "GetGetApplicationInstanceIdentifierHandler")]
			get {
				if (id_getApplicationInstanceIdentifier == IntPtr.Zero)
					id_getApplicationInstanceIdentifier = JNIEnv.GetMethodID (class_ref, "getApplicationInstanceIdentifier", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getApplicationInstanceIdentifier), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApplicationInstanceIdentifier", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setApplicationInstanceIdentifier' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setApplicationInstanceIdentifier", "(Ljava/lang/String;)V", "GetSetApplicationInstanceIdentifier_Ljava_lang_String_Handler")]
			set {
				if (id_setApplicationInstanceIdentifier_Ljava_lang_String_ == IntPtr.Zero)
					id_setApplicationInstanceIdentifier_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setApplicationInstanceIdentifier", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApplicationInstanceIdentifier_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApplicationInstanceIdentifier", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getOrganizationID;
#pragma warning disable 0169
		static Delegate GetGetOrganizationIDHandler ()
		{
			if (cb_getOrganizationID == null)
				cb_getOrganizationID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationID);
			return cb_getOrganizationID;
		}

		static IntPtr n_GetOrganizationID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OrganizationID);
		}
#pragma warning restore 0169

		static Delegate cb_setOrganizationID_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationID_Ljava_lang_Long_Handler ()
		{
			if (cb_setOrganizationID_Ljava_lang_Long_ == null)
				cb_setOrganizationID_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationID_Ljava_lang_Long_);
			return cb_setOrganizationID_Ljava_lang_Long_;
		}

		static void n_SetOrganizationID_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OrganizationID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationID;
		static IntPtr id_setOrganizationID_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long OrganizationID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getOrganizationID' and count(parameter)=0]"
			[Register ("getOrganizationID", "()Ljava/lang/Long;", "GetGetOrganizationIDHandler")]
			get {
				if (id_getOrganizationID == IntPtr.Zero)
					id_getOrganizationID = JNIEnv.GetMethodID (class_ref, "getOrganizationID", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationID), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationID", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setOrganizationID' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setOrganizationID", "(Ljava/lang/Long;)V", "GetSetOrganizationID_Ljava_lang_Long_Handler")]
			set {
				if (id_setOrganizationID_Ljava_lang_Long_ == IntPtr.Zero)
					id_setOrganizationID_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setOrganizationID", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOrganizationID_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationID", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPassword;
#pragma warning disable 0169
		static Delegate GetGetPasswordHandler ()
		{
			if (cb_getPassword == null)
				cb_getPassword = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPassword);
			return cb_getPassword;
		}

		static IntPtr n_GetPassword (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Password);
		}
#pragma warning restore 0169

		static Delegate cb_setPassword_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetPassword_Ljava_lang_String_Handler ()
		{
			if (cb_setPassword_Ljava_lang_String_ == null)
				cb_setPassword_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPassword_Ljava_lang_String_);
			return cb_setPassword_Ljava_lang_String_;
		}

		static void n_SetPassword_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Password = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPassword;
		static IntPtr id_setPassword_Ljava_lang_String_;
		public virtual unsafe string Password {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getPassword' and count(parameter)=0]"
			[Register ("getPassword", "()Ljava/lang/String;", "GetGetPasswordHandler")]
			get {
				if (id_getPassword == IntPtr.Zero)
					id_getPassword = JNIEnv.GetMethodID (class_ref, "getPassword", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getPassword), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPassword", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setPassword' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setPassword", "(Ljava/lang/String;)V", "GetSetPassword_Ljava_lang_String_Handler")]
			set {
				if (id_setPassword_Ljava_lang_String_ == IntPtr.Zero)
					id_setPassword_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setPassword", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPassword_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPassword", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getPlatform;
#pragma warning disable 0169
		static Delegate GetGetPlatformHandler ()
		{
			if (cb_getPlatform == null)
				cb_getPlatform = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlatform);
			return cb_getPlatform;
		}

		static IntPtr n_GetPlatform (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Platform);
		}
#pragma warning restore 0169

		static Delegate cb_setPlatform_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetPlatform_Ljava_lang_String_Handler ()
		{
			if (cb_setPlatform_Ljava_lang_String_ == null)
				cb_setPlatform_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPlatform_Ljava_lang_String_);
			return cb_setPlatform_Ljava_lang_String_;
		}

		static void n_SetPlatform_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Platform = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPlatform;
		static IntPtr id_setPlatform_Ljava_lang_String_;
		public virtual unsafe string Platform {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getPlatform' and count(parameter)=0]"
			[Register ("getPlatform", "()Ljava/lang/String;", "GetGetPlatformHandler")]
			get {
				if (id_getPlatform == IntPtr.Zero)
					id_getPlatform = JNIEnv.GetMethodID (class_ref, "getPlatform", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getPlatform), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlatform", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setPlatform' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setPlatform", "(Ljava/lang/String;)V", "GetSetPlatform_Ljava_lang_String_Handler")]
			set {
				if (id_setPlatform_Ljava_lang_String_ == IntPtr.Zero)
					id_setPlatform_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setPlatform", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPlatform_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPlatform", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getProximityApplicationUUID;
#pragma warning disable 0169
		static Delegate GetGetProximityApplicationUUIDHandler ()
		{
			if (cb_getProximityApplicationUUID == null)
				cb_getProximityApplicationUUID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetProximityApplicationUUID);
			return cb_getProximityApplicationUUID;
		}

		static IntPtr n_GetProximityApplicationUUID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ProximityApplicationUUID);
		}
#pragma warning restore 0169

		static Delegate cb_setProximityApplicationUUID_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetProximityApplicationUUID_Ljava_lang_String_Handler ()
		{
			if (cb_setProximityApplicationUUID_Ljava_lang_String_ == null)
				cb_setProximityApplicationUUID_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetProximityApplicationUUID_Ljava_lang_String_);
			return cb_setProximityApplicationUUID_Ljava_lang_String_;
		}

		static void n_SetProximityApplicationUUID_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ProximityApplicationUUID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getProximityApplicationUUID;
		static IntPtr id_setProximityApplicationUUID_Ljava_lang_String_;
		public virtual unsafe string ProximityApplicationUUID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getProximityApplicationUUID' and count(parameter)=0]"
			[Register ("getProximityApplicationUUID", "()Ljava/lang/String;", "GetGetProximityApplicationUUIDHandler")]
			get {
				if (id_getProximityApplicationUUID == IntPtr.Zero)
					id_getProximityApplicationUUID = JNIEnv.GetMethodID (class_ref, "getProximityApplicationUUID", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getProximityApplicationUUID), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getProximityApplicationUUID", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setProximityApplicationUUID' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setProximityApplicationUUID", "(Ljava/lang/String;)V", "GetSetProximityApplicationUUID_Ljava_lang_String_Handler")]
			set {
				if (id_setProximityApplicationUUID_Ljava_lang_String_ == IntPtr.Zero)
					id_setProximityApplicationUUID_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setProximityApplicationUUID", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setProximityApplicationUUID_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setProximityApplicationUUID", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getReceiverUUID;
#pragma warning disable 0169
		static Delegate GetGetReceiverUUIDHandler ()
		{
			if (cb_getReceiverUUID == null)
				cb_getReceiverUUID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetReceiverUUID);
			return cb_getReceiverUUID;
		}

		static IntPtr n_GetReceiverUUID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ReceiverUUID);
		}
#pragma warning restore 0169

		static Delegate cb_setReceiverUUID_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetReceiverUUID_Ljava_lang_String_Handler ()
		{
			if (cb_setReceiverUUID_Ljava_lang_String_ == null)
				cb_setReceiverUUID_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetReceiverUUID_Ljava_lang_String_);
			return cb_setReceiverUUID_Ljava_lang_String_;
		}

		static void n_SetReceiverUUID_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReceiverUUID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getReceiverUUID;
		static IntPtr id_setReceiverUUID_Ljava_lang_String_;
		public virtual unsafe string ReceiverUUID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getReceiverUUID' and count(parameter)=0]"
			[Register ("getReceiverUUID", "()Ljava/lang/String;", "GetGetReceiverUUIDHandler")]
			get {
				if (id_getReceiverUUID == IntPtr.Zero)
					id_getReceiverUUID = JNIEnv.GetMethodID (class_ref, "getReceiverUUID", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getReceiverUUID), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getReceiverUUID", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setReceiverUUID' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setReceiverUUID", "(Ljava/lang/String;)V", "GetSetReceiverUUID_Ljava_lang_String_Handler")]
			set {
				if (id_setReceiverUUID_Ljava_lang_String_ == IntPtr.Zero)
					id_setReceiverUUID_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setReceiverUUID", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setReceiverUUID_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setReceiverUUID", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getRegistrationTimestamp;
#pragma warning disable 0169
		static Delegate GetGetRegistrationTimestampHandler ()
		{
			if (cb_getRegistrationTimestamp == null)
				cb_getRegistrationTimestamp = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetRegistrationTimestamp);
			return cb_getRegistrationTimestamp;
		}

		static IntPtr n_GetRegistrationTimestamp (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.RegistrationTimestamp);
		}
#pragma warning restore 0169

		static Delegate cb_setRegistrationTimestamp_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetRegistrationTimestamp_Ljava_lang_Long_Handler ()
		{
			if (cb_setRegistrationTimestamp_Ljava_lang_Long_ == null)
				cb_setRegistrationTimestamp_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetRegistrationTimestamp_Ljava_lang_Long_);
			return cb_setRegistrationTimestamp_Ljava_lang_Long_;
		}

		static void n_SetRegistrationTimestamp_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RegistrationTimestamp = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getRegistrationTimestamp;
		static IntPtr id_setRegistrationTimestamp_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long RegistrationTimestamp {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getRegistrationTimestamp' and count(parameter)=0]"
			[Register ("getRegistrationTimestamp", "()Ljava/lang/Long;", "GetGetRegistrationTimestampHandler")]
			get {
				if (id_getRegistrationTimestamp == IntPtr.Zero)
					id_getRegistrationTimestamp = JNIEnv.GetMethodID (class_ref, "getRegistrationTimestamp", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getRegistrationTimestamp), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRegistrationTimestamp", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setRegistrationTimestamp' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setRegistrationTimestamp", "(Ljava/lang/Long;)V", "GetSetRegistrationTimestamp_Ljava_lang_Long_Handler")]
			set {
				if (id_setRegistrationTimestamp_Ljava_lang_Long_ == IntPtr.Zero)
					id_setRegistrationTimestamp_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setRegistrationTimestamp", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setRegistrationTimestamp_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setRegistrationTimestamp", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getUserID;
#pragma warning disable 0169
		static Delegate GetGetUserIDHandler ()
		{
			if (cb_getUserID == null)
				cb_getUserID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUserID);
			return cb_getUserID;
		}

		static IntPtr n_GetUserID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.UserID);
		}
#pragma warning restore 0169

		static Delegate cb_setUserID_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetUserID_Ljava_lang_Long_Handler ()
		{
			if (cb_setUserID_Ljava_lang_Long_ == null)
				cb_setUserID_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUserID_Ljava_lang_Long_);
			return cb_setUserID_Ljava_lang_Long_;
		}

		static void n_SetUserID_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.UserID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUserID;
		static IntPtr id_setUserID_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long UserID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getUserID' and count(parameter)=0]"
			[Register ("getUserID", "()Ljava/lang/Long;", "GetGetUserIDHandler")]
			get {
				if (id_getUserID == IntPtr.Zero)
					id_getUserID = JNIEnv.GetMethodID (class_ref, "getUserID", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getUserID), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUserID", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setUserID' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setUserID", "(Ljava/lang/Long;)V", "GetSetUserID_Ljava_lang_Long_Handler")]
			set {
				if (id_setUserID_Ljava_lang_Long_ == IntPtr.Zero)
					id_setUserID_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setUserID", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUserID_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUserID", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getUsername;
#pragma warning disable 0169
		static Delegate GetGetUsernameHandler ()
		{
			if (cb_getUsername == null)
				cb_getUsername = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUsername);
			return cb_getUsername;
		}

		static IntPtr n_GetUsername (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Username);
		}
#pragma warning restore 0169

		static Delegate cb_setUsername_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUsername_Ljava_lang_String_Handler ()
		{
			if (cb_setUsername_Ljava_lang_String_ == null)
				cb_setUsername_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUsername_Ljava_lang_String_);
			return cb_setUsername_Ljava_lang_String_;
		}

		static void n_SetUsername_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Registration __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Registration> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Username = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUsername;
		static IntPtr id_setUsername_Ljava_lang_String_;
		public virtual unsafe string Username {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='getUsername' and count(parameter)=0]"
			[Register ("getUsername", "()Ljava/lang/String;", "GetGetUsernameHandler")]
			get {
				if (id_getUsername == IntPtr.Zero)
					id_getUsername = JNIEnv.GetMethodID (class_ref, "getUsername", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUsername), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUsername", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Registration']/method[@name='setUsername' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUsername", "(Ljava/lang/String;)V", "GetSetUsername_Ljava_lang_String_Handler")]
			set {
				if (id_setUsername_Ljava_lang_String_ == IntPtr.Zero)
					id_setUsername_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUsername", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUsername_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUsername", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
