using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='VisitListener']"
	[Register ("com/gimbal/proximity/VisitListener", "", "Com.Gimbal.Proximity.IVisitListenerInvoker")]
	public partial interface IVisitListener : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='VisitListener']/method[@name='didArrive' and count(parameter)=1 and parameter[1][@type='com.gimbal.proximity.impl.InternalBeaconFenceVisit']]"
		[Register ("didArrive", "(Lcom/gimbal/proximity/impl/InternalBeaconFenceVisit;)V", "GetDidArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_Handler:Com.Gimbal.Proximity.IVisitListenerInvoker, GimbalSDK")]
		void DidArrive (global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='VisitListener']/method[@name='didDepart' and count(parameter)=1 and parameter[1][@type='com.gimbal.proximity.impl.InternalBeaconFenceVisit']]"
		[Register ("didDepart", "(Lcom/gimbal/proximity/impl/InternalBeaconFenceVisit;)V", "GetDidDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_Handler:Com.Gimbal.Proximity.IVisitListenerInvoker, GimbalSDK")]
		void DidDepart (global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='VisitListener']/method[@name='receivedSighting' and count(parameter)=2 and parameter[1][@type='com.gimbal.proximity.core.sighting.Sighting'] and parameter[2][@type='com.gimbal.proximity.impl.InternalBeaconFenceVisit']]"
		[Register ("receivedSighting", "(Lcom/gimbal/proximity/core/sighting/Sighting;Lcom/gimbal/proximity/impl/InternalBeaconFenceVisit;)V", "GetReceivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_Handler:Com.Gimbal.Proximity.IVisitListenerInvoker, GimbalSDK")]
		void ReceivedSighting (global::Com.Gimbal.Proximity.Core.Sighting.Sighting p0, global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p1);

	}

	[global::Android.Runtime.Register ("com/gimbal/proximity/VisitListener", DoNotGenerateAcw=true)]
	internal class IVisitListenerInvoker : global::Java.Lang.Object, IVisitListener {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/gimbal/proximity/VisitListener");
		IntPtr class_ref;

		public static IVisitListener GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IVisitListener> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.gimbal.proximity.VisitListener"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IVisitListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IVisitListenerInvoker); }
		}

		static Delegate cb_didArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_;
#pragma warning disable 0169
		static Delegate GetDidArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_Handler ()
		{
			if (cb_didArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ == null)
				cb_didArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_DidArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_);
			return cb_didArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_;
		}

		static void n_DidArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.IVisitListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IVisitListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DidArrive (p0);
		}
#pragma warning restore 0169

		IntPtr id_didArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_;
		public unsafe void DidArrive (global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p0)
		{
			if (id_didArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ == IntPtr.Zero)
				id_didArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ = JNIEnv.GetMethodID (class_ref, "didArrive", "(Lcom/gimbal/proximity/impl/InternalBeaconFenceVisit;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (Handle, id_didArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_, __args);
		}

		static Delegate cb_didDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_;
#pragma warning disable 0169
		static Delegate GetDidDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_Handler ()
		{
			if (cb_didDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ == null)
				cb_didDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_DidDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_);
			return cb_didDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_;
		}

		static void n_DidDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.IVisitListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IVisitListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DidDepart (p0);
		}
#pragma warning restore 0169

		IntPtr id_didDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_;
		public unsafe void DidDepart (global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p0)
		{
			if (id_didDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ == IntPtr.Zero)
				id_didDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ = JNIEnv.GetMethodID (class_ref, "didDepart", "(Lcom/gimbal/proximity/impl/InternalBeaconFenceVisit;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (Handle, id_didDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_, __args);
		}

		static Delegate cb_receivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_;
#pragma warning disable 0169
		static Delegate GetReceivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_Handler ()
		{
			if (cb_receivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ == null)
				cb_receivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr>) n_ReceivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_);
			return cb_receivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_;
		}

		static void n_ReceivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Proximity.IVisitListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IVisitListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Proximity.Core.Sighting.Sighting p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Sighting.Sighting> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p1 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit> (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.ReceivedSighting (p0, p1);
		}
#pragma warning restore 0169

		IntPtr id_receivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_;
		public unsafe void ReceivedSighting (global::Com.Gimbal.Proximity.Core.Sighting.Sighting p0, global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p1)
		{
			if (id_receivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ == IntPtr.Zero)
				id_receivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_ = JNIEnv.GetMethodID (class_ref, "receivedSighting", "(Lcom/gimbal/proximity/core/sighting/Sighting;Lcom/gimbal/proximity/impl/InternalBeaconFenceVisit;)V");
			JValue* __args = stackalloc JValue [2];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			JNIEnv.CallVoidMethod (Handle, id_receivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_, __args);
		}

	}

	public partial class DidArriveEventArgs : global::System.EventArgs {

		public DidArriveEventArgs (global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p0)
		{
			this.p0 = p0;
		}

		global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p0;
		public global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit P0 {
			get { return p0; }
		}
	}

	public partial class DidDepartEventArgs : global::System.EventArgs {

		public DidDepartEventArgs (global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p0)
		{
			this.p0 = p0;
		}

		global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p0;
		public global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit P0 {
			get { return p0; }
		}
	}

	public partial class ReceivedSightingEventArgs : global::System.EventArgs {

		public ReceivedSightingEventArgs (global::Com.Gimbal.Proximity.Core.Sighting.Sighting p0, global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p1)
		{
			this.p0 = p0;
			this.p1 = p1;
		}

		global::Com.Gimbal.Proximity.Core.Sighting.Sighting p0;
		public global::Com.Gimbal.Proximity.Core.Sighting.Sighting P0 {
			get { return p0; }
		}

		global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p1;
		public global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit P1 {
			get { return p1; }
		}
	}

	[global::Android.Runtime.Register ("mono/com/gimbal/proximity/VisitListenerImplementor")]
	internal sealed partial class IVisitListenerImplementor : global::Java.Lang.Object, IVisitListener {

		object sender;

		public IVisitListenerImplementor (object sender)
			: base (
				global::Android.Runtime.JNIEnv.StartCreateInstance ("mono/com/gimbal/proximity/VisitListenerImplementor", "()V"),
				JniHandleOwnership.TransferLocalRef)
		{
			global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
			this.sender = sender;
		}

#pragma warning disable 0649
		public EventHandler<DidArriveEventArgs> DidArriveHandler;
#pragma warning restore 0649

		public void DidArrive (global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p0)
		{
			var __h = DidArriveHandler;
			if (__h != null)
				__h (sender, new DidArriveEventArgs (p0));
		}
#pragma warning disable 0649
		public EventHandler<DidDepartEventArgs> DidDepartHandler;
#pragma warning restore 0649

		public void DidDepart (global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p0)
		{
			var __h = DidDepartHandler;
			if (__h != null)
				__h (sender, new DidDepartEventArgs (p0));
		}
#pragma warning disable 0649
		public EventHandler<ReceivedSightingEventArgs> ReceivedSightingHandler;
#pragma warning restore 0649

		public void ReceivedSighting (global::Com.Gimbal.Proximity.Core.Sighting.Sighting p0, global::Com.Gimbal.Proximity.Impl.InternalBeaconFenceVisit p1)
		{
			var __h = ReceivedSightingHandler;
			if (__h != null)
				__h (sender, new ReceivedSightingEventArgs (p0, p1));
		}

		internal static bool __IsEmpty (IVisitListenerImplementor value)
		{
			return value.DidArriveHandler == null && value.DidDepartHandler == null && value.ReceivedSightingHandler == null;
		}
	}

}
