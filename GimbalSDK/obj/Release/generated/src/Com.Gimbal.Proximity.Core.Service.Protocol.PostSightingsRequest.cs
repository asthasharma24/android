using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Service.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='PostSightingsRequest']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/service/protocol/PostSightingsRequest", DoNotGenerateAcw=true)]
	public partial class PostSightingsRequest : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/service/protocol/PostSightingsRequest", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PostSightingsRequest); }
		}

		protected PostSightingsRequest (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='PostSightingsRequest']/constructor[@name='PostSightingsRequest' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe PostSightingsRequest ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (PostSightingsRequest)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getReceiverUuid;
#pragma warning disable 0169
		static Delegate GetGetReceiverUuidHandler ()
		{
			if (cb_getReceiverUuid == null)
				cb_getReceiverUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetReceiverUuid);
			return cb_getReceiverUuid;
		}

		static IntPtr n_GetReceiverUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.PostSightingsRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.PostSightingsRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ReceiverUuid);
		}
#pragma warning restore 0169

		static Delegate cb_setReceiverUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetReceiverUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setReceiverUuid_Ljava_lang_String_ == null)
				cb_setReceiverUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetReceiverUuid_Ljava_lang_String_);
			return cb_setReceiverUuid_Ljava_lang_String_;
		}

		static void n_SetReceiverUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.PostSightingsRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.PostSightingsRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReceiverUuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getReceiverUuid;
		static IntPtr id_setReceiverUuid_Ljava_lang_String_;
		public virtual unsafe string ReceiverUuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='PostSightingsRequest']/method[@name='getReceiverUuid' and count(parameter)=0]"
			[Register ("getReceiverUuid", "()Ljava/lang/String;", "GetGetReceiverUuidHandler")]
			get {
				if (id_getReceiverUuid == IntPtr.Zero)
					id_getReceiverUuid = JNIEnv.GetMethodID (class_ref, "getReceiverUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getReceiverUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getReceiverUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='PostSightingsRequest']/method[@name='setReceiverUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setReceiverUuid", "(Ljava/lang/String;)V", "GetSetReceiverUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setReceiverUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setReceiverUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setReceiverUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setReceiverUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setReceiverUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getSightings;
#pragma warning disable 0169
		static Delegate GetGetSightingsHandler ()
		{
			if (cb_getSightings == null)
				cb_getSightings = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSightings);
			return cb_getSightings;
		}

		static IntPtr n_GetSightings (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.PostSightingsRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.PostSightingsRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Core.Sighting.Sighting>.ToLocalJniHandle (__this.Sightings);
		}
#pragma warning restore 0169

		static Delegate cb_setSightings_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetSightings_Ljava_util_List_Handler ()
		{
			if (cb_setSightings_Ljava_util_List_ == null)
				cb_setSightings_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSightings_Ljava_util_List_);
			return cb_setSightings_Ljava_util_List_;
		}

		static void n_SetSightings_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.PostSightingsRequest __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.PostSightingsRequest> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Core.Sighting.Sighting>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Sightings = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSightings;
		static IntPtr id_setSightings_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Proximity.Core.Sighting.Sighting> Sightings {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='PostSightingsRequest']/method[@name='getSightings' and count(parameter)=0]"
			[Register ("getSightings", "()Ljava/util/List;", "GetGetSightingsHandler")]
			get {
				if (id_getSightings == IntPtr.Zero)
					id_getSightings = JNIEnv.GetMethodID (class_ref, "getSightings", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Core.Sighting.Sighting>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getSightings), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Core.Sighting.Sighting>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSightings", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='PostSightingsRequest']/method[@name='setSightings' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.proximity.core.sighting.Sighting&gt;']]"
			[Register ("setSightings", "(Ljava/util/List;)V", "GetSetSightings_Ljava_util_List_Handler")]
			set {
				if (id_setSightings_Ljava_util_List_ == IntPtr.Zero)
					id_setSightings_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setSightings", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Core.Sighting.Sighting>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSightings_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSightings", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
