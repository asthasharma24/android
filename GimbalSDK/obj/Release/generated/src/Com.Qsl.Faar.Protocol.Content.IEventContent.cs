using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Content {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.qsl.faar.protocol.content']/interface[@name='EventContent']"
	[Register ("com/qsl/faar/protocol/content/EventContent", "", "Com.Qsl.Faar.Protocol.Content.IEventContentInvoker")]
	public partial interface IEventContent : global::Java.IO.ISerializable {

		global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> ContentDescriptors {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/interface[@name='EventContent']/method[@name='getContentDescriptors' and count(parameter)=0]"
			[Register ("getContentDescriptors", "()Ljava/util/List;", "GetGetContentDescriptorsHandler:Com.Qsl.Faar.Protocol.Content.IEventContentInvoker, GimbalSDK")] get;
		}

		global::Java.Lang.Long EventTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/interface[@name='EventContent']/method[@name='getEventTime' and count(parameter)=0]"
			[Register ("getEventTime", "()Ljava/lang/Long;", "GetGetEventTimeHandler:Com.Qsl.Faar.Protocol.Content.IEventContentInvoker, GimbalSDK")] get;
		}

		global::Java.Lang.Long OrganizationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/interface[@name='EventContent']/method[@name='getOrganizationId' and count(parameter)=0]"
			[Register ("getOrganizationId", "()Ljava/lang/Long;", "GetGetOrganizationIdHandler:Com.Qsl.Faar.Protocol.Content.IEventContentInvoker, GimbalSDK")] get;
		}

		global::Java.Lang.Long PlaceId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/interface[@name='EventContent']/method[@name='getPlaceId' and count(parameter)=0]"
			[Register ("getPlaceId", "()Ljava/lang/Long;", "GetGetPlaceIdHandler:Com.Qsl.Faar.Protocol.Content.IEventContentInvoker, GimbalSDK")] get;
		}

		string Type {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/interface[@name='EventContent']/method[@name='getType' and count(parameter)=0]"
			[Register ("getType", "()Ljava/lang/String;", "GetGetTypeHandler:Com.Qsl.Faar.Protocol.Content.IEventContentInvoker, GimbalSDK")] get;
		}

	}

	[global::Android.Runtime.Register ("com/qsl/faar/protocol/content/EventContent", DoNotGenerateAcw=true)]
	internal class IEventContentInvoker : global::Java.Lang.Object, IEventContent {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/qsl/faar/protocol/content/EventContent");

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IEventContentInvoker); }
		}

		IntPtr class_ref;

		public static IEventContent GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IEventContent> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.qsl.faar.protocol.content.EventContent"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IEventContentInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		static Delegate cb_getContentDescriptors;
#pragma warning disable 0169
		static Delegate GetGetContentDescriptorsHandler ()
		{
			if (cb_getContentDescriptors == null)
				cb_getContentDescriptors = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetContentDescriptors);
			return cb_getContentDescriptors;
		}

		static IntPtr n_GetContentDescriptors (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.IEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.IEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.ToLocalJniHandle (__this.ContentDescriptors);
		}
#pragma warning restore 0169

		IntPtr id_getContentDescriptors;
		public unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> ContentDescriptors {
			get {
				if (id_getContentDescriptors == IntPtr.Zero)
					id_getContentDescriptors = JNIEnv.GetMethodID (class_ref, "getContentDescriptors", "()Ljava/util/List;");
				return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.FromJniHandle (JNIEnv.CallObjectMethod (Handle, id_getContentDescriptors), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getEventTime;
#pragma warning disable 0169
		static Delegate GetGetEventTimeHandler ()
		{
			if (cb_getEventTime == null)
				cb_getEventTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEventTime);
			return cb_getEventTime;
		}

		static IntPtr n_GetEventTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.IEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.IEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.EventTime);
		}
#pragma warning restore 0169

		IntPtr id_getEventTime;
		public unsafe global::Java.Lang.Long EventTime {
			get {
				if (id_getEventTime == IntPtr.Zero)
					id_getEventTime = JNIEnv.GetMethodID (class_ref, "getEventTime", "()Ljava/lang/Long;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod (Handle, id_getEventTime), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getOrganizationId;
#pragma warning disable 0169
		static Delegate GetGetOrganizationIdHandler ()
		{
			if (cb_getOrganizationId == null)
				cb_getOrganizationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationId);
			return cb_getOrganizationId;
		}

		static IntPtr n_GetOrganizationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.IEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.IEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OrganizationId);
		}
#pragma warning restore 0169

		IntPtr id_getOrganizationId;
		public unsafe global::Java.Lang.Long OrganizationId {
			get {
				if (id_getOrganizationId == IntPtr.Zero)
					id_getOrganizationId = JNIEnv.GetMethodID (class_ref, "getOrganizationId", "()Ljava/lang/Long;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod (Handle, id_getOrganizationId), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getPlaceId;
#pragma warning disable 0169
		static Delegate GetGetPlaceIdHandler ()
		{
			if (cb_getPlaceId == null)
				cb_getPlaceId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlaceId);
			return cb_getPlaceId;
		}

		static IntPtr n_GetPlaceId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.IEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.IEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.PlaceId);
		}
#pragma warning restore 0169

		IntPtr id_getPlaceId;
		public unsafe global::Java.Lang.Long PlaceId {
			get {
				if (id_getPlaceId == IntPtr.Zero)
					id_getPlaceId = JNIEnv.GetMethodID (class_ref, "getPlaceId", "()Ljava/lang/Long;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod (Handle, id_getPlaceId), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getType;
#pragma warning disable 0169
		static Delegate GetGetTypeHandler ()
		{
			if (cb_getType == null)
				cb_getType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetType);
			return cb_getType;
		}

		static IntPtr n_GetType (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.IEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.IEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Type);
		}
#pragma warning restore 0169

		IntPtr id_getType;
		public unsafe string Type {
			get {
				if (id_getType == IntPtr.Zero)
					id_getType = JNIEnv.GetMethodID (class_ref, "getType", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (Handle, id_getType), JniHandleOwnership.TransferLocalRef);
			}
		}

	}

}
