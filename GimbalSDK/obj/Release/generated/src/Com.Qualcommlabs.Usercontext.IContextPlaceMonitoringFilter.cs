using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qualcommlabs.Usercontext {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.qualcommlabs.usercontext']/interface[@name='ContextPlaceMonitoringFilter']"
	[Register ("com/qualcommlabs/usercontext/ContextPlaceMonitoringFilter", "", "Com.Qualcommlabs.Usercontext.IContextPlaceMonitoringFilterInvoker")]
	public partial interface IContextPlaceMonitoringFilter : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.qualcommlabs.usercontext']/interface[@name='ContextPlaceMonitoringFilter']/method[@name='shouldMonitor' and count(parameter)=1 and parameter[1][@type='com.gimbal.protocol.Place']]"
		[Register ("shouldMonitor", "(Lcom/gimbal/protocol/Place;)Z", "GetShouldMonitor_Lcom_gimbal_protocol_Place_Handler:Com.Qualcommlabs.Usercontext.IContextPlaceMonitoringFilterInvoker, GimbalSDK")]
		bool ShouldMonitor (global::Com.Gimbal.Protocol.Place p0);

	}

	[global::Android.Runtime.Register ("com/qualcommlabs/usercontext/ContextPlaceMonitoringFilter", DoNotGenerateAcw=true)]
	internal class IContextPlaceMonitoringFilterInvoker : global::Java.Lang.Object, IContextPlaceMonitoringFilter {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/qualcommlabs/usercontext/ContextPlaceMonitoringFilter");
		IntPtr class_ref;

		public static IContextPlaceMonitoringFilter GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IContextPlaceMonitoringFilter> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.qualcommlabs.usercontext.ContextPlaceMonitoringFilter"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IContextPlaceMonitoringFilterInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IContextPlaceMonitoringFilterInvoker); }
		}

		static Delegate cb_shouldMonitor_Lcom_gimbal_protocol_Place_;
#pragma warning disable 0169
		static Delegate GetShouldMonitor_Lcom_gimbal_protocol_Place_Handler ()
		{
			if (cb_shouldMonitor_Lcom_gimbal_protocol_Place_ == null)
				cb_shouldMonitor_Lcom_gimbal_protocol_Place_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, bool>) n_ShouldMonitor_Lcom_gimbal_protocol_Place_);
			return cb_shouldMonitor_Lcom_gimbal_protocol_Place_;
		}

		static bool n_ShouldMonitor_Lcom_gimbal_protocol_Place_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qualcommlabs.Usercontext.IContextPlaceMonitoringFilter __this = global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.IContextPlaceMonitoringFilter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Protocol.Place p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Place> (native_p0, JniHandleOwnership.DoNotTransfer);
			bool __ret = __this.ShouldMonitor (p0);
			return __ret;
		}
#pragma warning restore 0169

		IntPtr id_shouldMonitor_Lcom_gimbal_protocol_Place_;
		public unsafe bool ShouldMonitor (global::Com.Gimbal.Protocol.Place p0)
		{
			if (id_shouldMonitor_Lcom_gimbal_protocol_Place_ == IntPtr.Zero)
				id_shouldMonitor_Lcom_gimbal_protocol_Place_ = JNIEnv.GetMethodID (class_ref, "shouldMonitor", "(Lcom/gimbal/protocol/Place;)Z");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			bool __ret = JNIEnv.CallBooleanMethod (Handle, id_shouldMonitor_Lcom_gimbal_protocol_Place_, __args);
			return __ret;
		}

	}

}
