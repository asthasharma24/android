using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='Visit']"
	[Register ("com/gimbal/proximity/Visit", "", "Com.Gimbal.Proximity.IVisitInvoker")]
	public partial interface IVisit : IJavaObject {

		global::Java.Lang.Long DwellTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='Visit']/method[@name='getDwellTime' and count(parameter)=0]"
			[Register ("getDwellTime", "()Ljava/lang/Long;", "GetGetDwellTimeHandler:Com.Gimbal.Proximity.IVisitInvoker, GimbalSDK")] get;
		}

		global::Java.Util.Date LastUpdateTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='Visit']/method[@name='getLastUpdateTime' and count(parameter)=0]"
			[Register ("getLastUpdateTime", "()Ljava/util/Date;", "GetGetLastUpdateTimeHandler:Com.Gimbal.Proximity.IVisitInvoker, GimbalSDK")] get;
		}

		global::Java.Util.Date StartTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='Visit']/method[@name='getStartTime' and count(parameter)=0]"
			[Register ("getStartTime", "()Ljava/util/Date;", "GetGetStartTimeHandler:Com.Gimbal.Proximity.IVisitInvoker, GimbalSDK")] get;
		}

		global::Com.Gimbal.Proximity.ITransmitter Transmitter {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='Visit']/method[@name='getTransmitter' and count(parameter)=0]"
			[Register ("getTransmitter", "()Lcom/gimbal/proximity/Transmitter;", "GetGetTransmitterHandler:Com.Gimbal.Proximity.IVisitInvoker, GimbalSDK")] get;
		}

	}

	[global::Android.Runtime.Register ("com/gimbal/proximity/Visit", DoNotGenerateAcw=true)]
	internal class IVisitInvoker : global::Java.Lang.Object, IVisit {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/gimbal/proximity/Visit");
		IntPtr class_ref;

		public static IVisit GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IVisit> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.gimbal.proximity.Visit"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IVisitInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IVisitInvoker); }
		}

		static Delegate cb_getDwellTime;
#pragma warning disable 0169
		static Delegate GetGetDwellTimeHandler ()
		{
			if (cb_getDwellTime == null)
				cb_getDwellTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDwellTime);
			return cb_getDwellTime;
		}

		static IntPtr n_GetDwellTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.IVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DwellTime);
		}
#pragma warning restore 0169

		IntPtr id_getDwellTime;
		public unsafe global::Java.Lang.Long DwellTime {
			get {
				if (id_getDwellTime == IntPtr.Zero)
					id_getDwellTime = JNIEnv.GetMethodID (class_ref, "getDwellTime", "()Ljava/lang/Long;");
				return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod (Handle, id_getDwellTime), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getLastUpdateTime;
#pragma warning disable 0169
		static Delegate GetGetLastUpdateTimeHandler ()
		{
			if (cb_getLastUpdateTime == null)
				cb_getLastUpdateTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLastUpdateTime);
			return cb_getLastUpdateTime;
		}

		static IntPtr n_GetLastUpdateTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.IVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.LastUpdateTime);
		}
#pragma warning restore 0169

		IntPtr id_getLastUpdateTime;
		public unsafe global::Java.Util.Date LastUpdateTime {
			get {
				if (id_getLastUpdateTime == IntPtr.Zero)
					id_getLastUpdateTime = JNIEnv.GetMethodID (class_ref, "getLastUpdateTime", "()Ljava/util/Date;");
				return global::Java.Lang.Object.GetObject<global::Java.Util.Date> (JNIEnv.CallObjectMethod (Handle, id_getLastUpdateTime), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getStartTime;
#pragma warning disable 0169
		static Delegate GetGetStartTimeHandler ()
		{
			if (cb_getStartTime == null)
				cb_getStartTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetStartTime);
			return cb_getStartTime;
		}

		static IntPtr n_GetStartTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.IVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.StartTime);
		}
#pragma warning restore 0169

		IntPtr id_getStartTime;
		public unsafe global::Java.Util.Date StartTime {
			get {
				if (id_getStartTime == IntPtr.Zero)
					id_getStartTime = JNIEnv.GetMethodID (class_ref, "getStartTime", "()Ljava/util/Date;");
				return global::Java.Lang.Object.GetObject<global::Java.Util.Date> (JNIEnv.CallObjectMethod (Handle, id_getStartTime), JniHandleOwnership.TransferLocalRef);
			}
		}

		static Delegate cb_getTransmitter;
#pragma warning disable 0169
		static Delegate GetGetTransmitterHandler ()
		{
			if (cb_getTransmitter == null)
				cb_getTransmitter = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTransmitter);
			return cb_getTransmitter;
		}

		static IntPtr n_GetTransmitter (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.IVisit __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IVisit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Transmitter);
		}
#pragma warning restore 0169

		IntPtr id_getTransmitter;
		public unsafe global::Com.Gimbal.Proximity.ITransmitter Transmitter {
			get {
				if (id_getTransmitter == IntPtr.Zero)
					id_getTransmitter = JNIEnv.GetMethodID (class_ref, "getTransmitter", "()Lcom/gimbal/proximity/Transmitter;");
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ITransmitter> (JNIEnv.CallObjectMethod (Handle, id_getTransmitter), JniHandleOwnership.TransferLocalRef);
			}
		}

	}

}
