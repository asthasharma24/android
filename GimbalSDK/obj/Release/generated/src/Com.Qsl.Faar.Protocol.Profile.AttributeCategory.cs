using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Profile {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='AttributeCategory']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/profile/AttributeCategory", DoNotGenerateAcw=true)]
	public partial class AttributeCategory : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/profile/AttributeCategory", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (AttributeCategory); }
		}

		protected AttributeCategory (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='AttributeCategory']/constructor[@name='AttributeCategory' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe AttributeCategory ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (AttributeCategory)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getKey;
#pragma warning disable 0169
		static Delegate GetGetKeyHandler ()
		{
			if (cb_getKey == null)
				cb_getKey = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetKey);
			return cb_getKey;
		}

		static IntPtr n_GetKey (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Key);
		}
#pragma warning restore 0169

		static Delegate cb_setKey_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetKey_Ljava_lang_String_Handler ()
		{
			if (cb_setKey_Ljava_lang_String_ == null)
				cb_setKey_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetKey_Ljava_lang_String_);
			return cb_setKey_Ljava_lang_String_;
		}

		static void n_SetKey_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Key = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getKey;
		static IntPtr id_setKey_Ljava_lang_String_;
		public virtual unsafe string Key {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='AttributeCategory']/method[@name='getKey' and count(parameter)=0]"
			[Register ("getKey", "()Ljava/lang/String;", "GetGetKeyHandler")]
			get {
				if (id_getKey == IntPtr.Zero)
					id_getKey = JNIEnv.GetMethodID (class_ref, "getKey", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getKey), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getKey", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='AttributeCategory']/method[@name='setKey' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setKey", "(Ljava/lang/String;)V", "GetSetKey_Ljava_lang_String_Handler")]
			set {
				if (id_setKey_Ljava_lang_String_ == IntPtr.Zero)
					id_setKey_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setKey", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setKey_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setKey", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getLikelihood;
#pragma warning disable 0169
		static Delegate GetGetLikelihoodHandler ()
		{
			if (cb_getLikelihood == null)
				cb_getLikelihood = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, double>) n_GetLikelihood);
			return cb_getLikelihood;
		}

		static double n_GetLikelihood (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Likelihood;
		}
#pragma warning restore 0169

		static Delegate cb_setLikelihood_D;
#pragma warning disable 0169
		static Delegate GetSetLikelihood_DHandler ()
		{
			if (cb_setLikelihood_D == null)
				cb_setLikelihood_D = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, double>) n_SetLikelihood_D);
			return cb_setLikelihood_D;
		}

		static void n_SetLikelihood_D (IntPtr jnienv, IntPtr native__this, double p0)
		{
			global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Profile.AttributeCategory> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Likelihood = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLikelihood;
		static IntPtr id_setLikelihood_D;
		public virtual unsafe double Likelihood {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='AttributeCategory']/method[@name='getLikelihood' and count(parameter)=0]"
			[Register ("getLikelihood", "()D", "GetGetLikelihoodHandler")]
			get {
				if (id_getLikelihood == IntPtr.Zero)
					id_getLikelihood = JNIEnv.GetMethodID (class_ref, "getLikelihood", "()D");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallDoubleMethod  (Handle, id_getLikelihood);
					else
						return JNIEnv.CallNonvirtualDoubleMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLikelihood", "()D"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.profile']/class[@name='AttributeCategory']/method[@name='setLikelihood' and count(parameter)=1 and parameter[1][@type='double']]"
			[Register ("setLikelihood", "(D)V", "GetSetLikelihood_DHandler")]
			set {
				if (id_setLikelihood_D == IntPtr.Zero)
					id_setLikelihood_D = JNIEnv.GetMethodID (class_ref, "setLikelihood", "(D)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLikelihood_D, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLikelihood", "(D)V"), __args);
				} finally {
				}
			}
		}

	}
}
