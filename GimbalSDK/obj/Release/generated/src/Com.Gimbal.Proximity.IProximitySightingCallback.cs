using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='ProximitySightingCallback']"
	[Register ("com/gimbal/proximity/ProximitySightingCallback", "", "Com.Gimbal.Proximity.IProximitySightingCallbackInvoker")]
	public partial interface IProximitySightingCallback : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='ProximitySightingCallback']/method[@name='didReceiveSighting' and count(parameter)=3 and parameter[1][@type='com.gimbal.proximity.impl.TransmitterInternal'] and parameter[2][@type='java.util.Date'] and parameter[3][@type='int']]"
		[Register ("didReceiveSighting", "(Lcom/gimbal/proximity/impl/TransmitterInternal;Ljava/util/Date;I)V", "GetDidReceiveSighting_Lcom_gimbal_proximity_impl_TransmitterInternal_Ljava_util_Date_IHandler:Com.Gimbal.Proximity.IProximitySightingCallbackInvoker, GimbalSDK")]
		void DidReceiveSighting (global::Com.Gimbal.Proximity.Impl.TransmitterInternal p0, global::Java.Util.Date p1, int p2);

	}

	[global::Android.Runtime.Register ("com/gimbal/proximity/ProximitySightingCallback", DoNotGenerateAcw=true)]
	internal class IProximitySightingCallbackInvoker : global::Java.Lang.Object, IProximitySightingCallback {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/gimbal/proximity/ProximitySightingCallback");
		IntPtr class_ref;

		public static IProximitySightingCallback GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IProximitySightingCallback> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.gimbal.proximity.ProximitySightingCallback"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IProximitySightingCallbackInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IProximitySightingCallbackInvoker); }
		}

		static Delegate cb_didReceiveSighting_Lcom_gimbal_proximity_impl_TransmitterInternal_Ljava_util_Date_I;
#pragma warning disable 0169
		static Delegate GetDidReceiveSighting_Lcom_gimbal_proximity_impl_TransmitterInternal_Ljava_util_Date_IHandler ()
		{
			if (cb_didReceiveSighting_Lcom_gimbal_proximity_impl_TransmitterInternal_Ljava_util_Date_I == null)
				cb_didReceiveSighting_Lcom_gimbal_proximity_impl_TransmitterInternal_Ljava_util_Date_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr, IntPtr, int>) n_DidReceiveSighting_Lcom_gimbal_proximity_impl_TransmitterInternal_Ljava_util_Date_I);
			return cb_didReceiveSighting_Lcom_gimbal_proximity_impl_TransmitterInternal_Ljava_util_Date_I;
		}

		static void n_DidReceiveSighting_Lcom_gimbal_proximity_impl_TransmitterInternal_Ljava_util_Date_I (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1, int p2)
		{
			global::Com.Gimbal.Proximity.IProximitySightingCallback __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IProximitySightingCallback> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Proximity.Impl.TransmitterInternal p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Impl.TransmitterInternal> (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Java.Util.Date p1 = global::Java.Lang.Object.GetObject<global::Java.Util.Date> (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.DidReceiveSighting (p0, p1, p2);
		}
#pragma warning restore 0169

		IntPtr id_didReceiveSighting_Lcom_gimbal_proximity_impl_TransmitterInternal_Ljava_util_Date_I;
		public unsafe void DidReceiveSighting (global::Com.Gimbal.Proximity.Impl.TransmitterInternal p0, global::Java.Util.Date p1, int p2)
		{
			if (id_didReceiveSighting_Lcom_gimbal_proximity_impl_TransmitterInternal_Ljava_util_Date_I == IntPtr.Zero)
				id_didReceiveSighting_Lcom_gimbal_proximity_impl_TransmitterInternal_Ljava_util_Date_I = JNIEnv.GetMethodID (class_ref, "didReceiveSighting", "(Lcom/gimbal/proximity/impl/TransmitterInternal;Ljava/util/Date;I)V");
			JValue* __args = stackalloc JValue [3];
			__args [0] = new JValue (p0);
			__args [1] = new JValue (p1);
			__args [2] = new JValue (p2);
			JNIEnv.CallVoidMethod (Handle, id_didReceiveSighting_Lcom_gimbal_proximity_impl_TransmitterInternal_Ljava_util_Date_I, __args);
		}

	}

}
