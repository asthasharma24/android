using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Service.Util {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.service.util']/class[@name='UserContextProperties']"
	[global::Android.Runtime.Register ("com/qsl/faar/service/util/UserContextProperties", DoNotGenerateAcw=true)]
	public partial class UserContextProperties : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/service/util/UserContextProperties", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (UserContextProperties); }
		}

		protected UserContextProperties (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Landroid_content_Context_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.service.util']/class[@name='UserContextProperties']/constructor[@name='UserContextProperties' and count(parameter)=1 and parameter[1][@type='android.content.Context']]"
		[Register (".ctor", "(Landroid/content/Context;)V", "")]
		public unsafe UserContextProperties (global::Android.Content.Context p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (UserContextProperties)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Landroid/content/Context;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Landroid/content/Context;)V", __args);
					return;
				}

				if (id_ctor_Landroid_content_Context_ == IntPtr.Zero)
					id_ctor_Landroid_content_Context_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Landroid/content/Context;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Landroid_content_Context_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Landroid_content_Context_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor_Ljava_util_Properties_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.service.util']/class[@name='UserContextProperties']/constructor[@name='UserContextProperties' and count(parameter)=1 and parameter[1][@type='java.util.Properties']]"
		[Register (".ctor", "(Ljava/util/Properties;)V", "")]
		public unsafe UserContextProperties (global::Java.Util.Properties p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (UserContextProperties)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Ljava/util/Properties;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Ljava/util/Properties;)V", __args);
					return;
				}

				if (id_ctor_Ljava_util_Properties_ == IntPtr.Zero)
					id_ctor_Ljava_util_Properties_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Ljava/util/Properties;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Ljava_util_Properties_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Ljava_util_Properties_, __args);
			} finally {
			}
		}

		static Delegate cb_getExtraExitDelay;
#pragma warning disable 0169
		static Delegate GetGetExtraExitDelayHandler ()
		{
			if (cb_getExtraExitDelay == null)
				cb_getExtraExitDelay = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetExtraExitDelay);
			return cb_getExtraExitDelay;
		}

		static long n_GetExtraExitDelay (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Service.Util.UserContextProperties __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.Util.UserContextProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ExtraExitDelay;
		}
#pragma warning restore 0169

		static IntPtr id_getExtraExitDelay;
		public virtual unsafe long ExtraExitDelay {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service.util']/class[@name='UserContextProperties']/method[@name='getExtraExitDelay' and count(parameter)=0]"
			[Register ("getExtraExitDelay", "()J", "GetGetExtraExitDelayHandler")]
			get {
				if (id_getExtraExitDelay == IntPtr.Zero)
					id_getExtraExitDelay = JNIEnv.GetMethodID (class_ref, "getExtraExitDelay", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getExtraExitDelay);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getExtraExitDelay", "()J"));
				} finally {
				}
			}
		}

		static Delegate cb_hasExtraExitDelay;
#pragma warning disable 0169
		static Delegate GetHasExtraExitDelayHandler ()
		{
			if (cb_hasExtraExitDelay == null)
				cb_hasExtraExitDelay = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_HasExtraExitDelay);
			return cb_hasExtraExitDelay;
		}

		static bool n_HasExtraExitDelay (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Service.Util.UserContextProperties __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.Util.UserContextProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.HasExtraExitDelay;
		}
#pragma warning restore 0169

		static IntPtr id_hasExtraExitDelay;
		public virtual unsafe bool HasExtraExitDelay {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service.util']/class[@name='UserContextProperties']/method[@name='hasExtraExitDelay' and count(parameter)=0]"
			[Register ("hasExtraExitDelay", "()Z", "GetHasExtraExitDelayHandler")]
			get {
				if (id_hasExtraExitDelay == IntPtr.Zero)
					id_hasExtraExitDelay = JNIEnv.GetMethodID (class_ref, "hasExtraExitDelay", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_hasExtraExitDelay);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "hasExtraExitDelay", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isBackgroundGeofencingDisabled;
#pragma warning disable 0169
		static Delegate GetIsBackgroundGeofencingDisabledHandler ()
		{
			if (cb_isBackgroundGeofencingDisabled == null)
				cb_isBackgroundGeofencingDisabled = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsBackgroundGeofencingDisabled);
			return cb_isBackgroundGeofencingDisabled;
		}

		static bool n_IsBackgroundGeofencingDisabled (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Service.Util.UserContextProperties __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.Util.UserContextProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsBackgroundGeofencingDisabled;
		}
#pragma warning restore 0169

		static IntPtr id_isBackgroundGeofencingDisabled;
		public virtual unsafe bool IsBackgroundGeofencingDisabled {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service.util']/class[@name='UserContextProperties']/method[@name='isBackgroundGeofencingDisabled' and count(parameter)=0]"
			[Register ("isBackgroundGeofencingDisabled", "()Z", "GetIsBackgroundGeofencingDisabledHandler")]
			get {
				if (id_isBackgroundGeofencingDisabled == IntPtr.Zero)
					id_isBackgroundGeofencingDisabled = JNIEnv.GetMethodID (class_ref, "isBackgroundGeofencingDisabled", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isBackgroundGeofencingDisabled);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isBackgroundGeofencingDisabled", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isCommunicationsDisabled;
#pragma warning disable 0169
		static Delegate GetIsCommunicationsDisabledHandler ()
		{
			if (cb_isCommunicationsDisabled == null)
				cb_isCommunicationsDisabled = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsCommunicationsDisabled);
			return cb_isCommunicationsDisabled;
		}

		static bool n_IsCommunicationsDisabled (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Service.Util.UserContextProperties __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.Util.UserContextProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsCommunicationsDisabled;
		}
#pragma warning restore 0169

		static IntPtr id_isCommunicationsDisabled;
		public virtual unsafe bool IsCommunicationsDisabled {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service.util']/class[@name='UserContextProperties']/method[@name='isCommunicationsDisabled' and count(parameter)=0]"
			[Register ("isCommunicationsDisabled", "()Z", "GetIsCommunicationsDisabledHandler")]
			get {
				if (id_isCommunicationsDisabled == IntPtr.Zero)
					id_isCommunicationsDisabled = JNIEnv.GetMethodID (class_ref, "isCommunicationsDisabled", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isCommunicationsDisabled);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isCommunicationsDisabled", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isCustomOptIn;
#pragma warning disable 0169
		static Delegate GetIsCustomOptInHandler ()
		{
			if (cb_isCustomOptIn == null)
				cb_isCustomOptIn = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsCustomOptIn);
			return cb_isCustomOptIn;
		}

		static bool n_IsCustomOptIn (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Service.Util.UserContextProperties __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.Util.UserContextProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsCustomOptIn;
		}
#pragma warning restore 0169

		static IntPtr id_isCustomOptIn;
		public virtual unsafe bool IsCustomOptIn {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service.util']/class[@name='UserContextProperties']/method[@name='isCustomOptIn' and count(parameter)=0]"
			[Register ("isCustomOptIn", "()Z", "GetIsCustomOptInHandler")]
			get {
				if (id_isCustomOptIn == IntPtr.Zero)
					id_isCustomOptIn = JNIEnv.GetMethodID (class_ref, "isCustomOptIn", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isCustomOptIn);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isCustomOptIn", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isLiberalPlaceEventsAllowedWhenWifiOff;
#pragma warning disable 0169
		static Delegate GetIsLiberalPlaceEventsAllowedWhenWifiOffHandler ()
		{
			if (cb_isLiberalPlaceEventsAllowedWhenWifiOff == null)
				cb_isLiberalPlaceEventsAllowedWhenWifiOff = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsLiberalPlaceEventsAllowedWhenWifiOff);
			return cb_isLiberalPlaceEventsAllowedWhenWifiOff;
		}

		static bool n_IsLiberalPlaceEventsAllowedWhenWifiOff (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Service.Util.UserContextProperties __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.Util.UserContextProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsLiberalPlaceEventsAllowedWhenWifiOff;
		}
#pragma warning restore 0169

		static IntPtr id_isLiberalPlaceEventsAllowedWhenWifiOff;
		public virtual unsafe bool IsLiberalPlaceEventsAllowedWhenWifiOff {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service.util']/class[@name='UserContextProperties']/method[@name='isLiberalPlaceEventsAllowedWhenWifiOff' and count(parameter)=0]"
			[Register ("isLiberalPlaceEventsAllowedWhenWifiOff", "()Z", "GetIsLiberalPlaceEventsAllowedWhenWifiOffHandler")]
			get {
				if (id_isLiberalPlaceEventsAllowedWhenWifiOff == IntPtr.Zero)
					id_isLiberalPlaceEventsAllowedWhenWifiOff = JNIEnv.GetMethodID (class_ref, "isLiberalPlaceEventsAllowedWhenWifiOff", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isLiberalPlaceEventsAllowedWhenWifiOff);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isLiberalPlaceEventsAllowedWhenWifiOff", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_isPlaceStateAvailableOnServer;
#pragma warning disable 0169
		static Delegate GetIsPlaceStateAvailableOnServerHandler ()
		{
			if (cb_isPlaceStateAvailableOnServer == null)
				cb_isPlaceStateAvailableOnServer = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsPlaceStateAvailableOnServer);
			return cb_isPlaceStateAvailableOnServer;
		}

		static bool n_IsPlaceStateAvailableOnServer (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Service.Util.UserContextProperties __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.Util.UserContextProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.IsPlaceStateAvailableOnServer;
		}
#pragma warning restore 0169

		static IntPtr id_isPlaceStateAvailableOnServer;
		public virtual unsafe bool IsPlaceStateAvailableOnServer {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service.util']/class[@name='UserContextProperties']/method[@name='isPlaceStateAvailableOnServer' and count(parameter)=0]"
			[Register ("isPlaceStateAvailableOnServer", "()Z", "GetIsPlaceStateAvailableOnServerHandler")]
			get {
				if (id_isPlaceStateAvailableOnServer == IntPtr.Zero)
					id_isPlaceStateAvailableOnServer = JNIEnv.GetMethodID (class_ref, "isPlaceStateAvailableOnServer", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isPlaceStateAvailableOnServer);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isPlaceStateAvailableOnServer", "()Z"));
				} finally {
				}
			}
		}

		static Delegate cb_getUserCredentialEmailDomain;
#pragma warning disable 0169
		static Delegate GetGetUserCredentialEmailDomainHandler ()
		{
			if (cb_getUserCredentialEmailDomain == null)
				cb_getUserCredentialEmailDomain = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUserCredentialEmailDomain);
			return cb_getUserCredentialEmailDomain;
		}

		static IntPtr n_GetUserCredentialEmailDomain (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Service.Util.UserContextProperties __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.Util.UserContextProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.UserCredentialEmailDomain);
		}
#pragma warning restore 0169

		static IntPtr id_getUserCredentialEmailDomain;
		public virtual unsafe string UserCredentialEmailDomain {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service.util']/class[@name='UserContextProperties']/method[@name='getUserCredentialEmailDomain' and count(parameter)=0]"
			[Register ("getUserCredentialEmailDomain", "()Ljava/lang/String;", "GetGetUserCredentialEmailDomainHandler")]
			get {
				if (id_getUserCredentialEmailDomain == IntPtr.Zero)
					id_getUserCredentialEmailDomain = JNIEnv.GetMethodID (class_ref, "getUserCredentialEmailDomain", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUserCredentialEmailDomain), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUserCredentialEmailDomain", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_clear;
#pragma warning disable 0169
		static Delegate GetClearHandler ()
		{
			if (cb_clear == null)
				cb_clear = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Clear);
			return cb_clear;
		}

		static void n_Clear (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Service.Util.UserContextProperties __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.Util.UserContextProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Clear ();
		}
#pragma warning restore 0169

		static IntPtr id_clear;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service.util']/class[@name='UserContextProperties']/method[@name='clear' and count(parameter)=0]"
		[Register ("clear", "()V", "GetClearHandler")]
		public virtual unsafe void Clear ()
		{
			if (id_clear == IntPtr.Zero)
				id_clear = JNIEnv.GetMethodID (class_ref, "clear", "()V");
			try {

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_clear);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "clear", "()V"));
			} finally {
			}
		}

	}
}
