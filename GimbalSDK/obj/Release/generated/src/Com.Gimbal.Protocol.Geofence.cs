using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/Geofence", DoNotGenerateAcw=true)]
	public partial class Geofence : global::Java.Lang.Object {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/field[@name='SHAPE_CIRCLE']"
		[Register ("SHAPE_CIRCLE")]
		public const string ShapeCircle = (string) "CIRCLE";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/field[@name='SHAPE_CONFIDENTIAL']"
		[Register ("SHAPE_CONFIDENTIAL")]
		public const string ShapeConfidential = (string) "CONFIDENTIAL";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/field[@name='SHAPE_POLYGON']"
		[Register ("SHAPE_POLYGON")]
		public const string ShapePolygon = (string) "POLYGON";
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/Geofence", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Geofence); }
		}

		protected Geofence (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/constructor[@name='Geofence' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Geofence ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Geofence)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getCenter;
#pragma warning disable 0169
		static Delegate GetGetCenterHandler ()
		{
			if (cb_getCenter == null)
				cb_getCenter = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCenter);
			return cb_getCenter;
		}

		static IntPtr n_GetCenter (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Center);
		}
#pragma warning restore 0169

		static Delegate cb_setCenter_Lcom_gimbal_protocol_Point_;
#pragma warning disable 0169
		static Delegate GetSetCenter_Lcom_gimbal_protocol_Point_Handler ()
		{
			if (cb_setCenter_Lcom_gimbal_protocol_Point_ == null)
				cb_setCenter_Lcom_gimbal_protocol_Point_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCenter_Lcom_gimbal_protocol_Point_);
			return cb_setCenter_Lcom_gimbal_protocol_Point_;
		}

		static void n_SetCenter_Lcom_gimbal_protocol_Point_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Protocol.Point p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Point> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Center = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getCenter;
		static IntPtr id_setCenter_Lcom_gimbal_protocol_Point_;
		public virtual unsafe global::Com.Gimbal.Protocol.Point Center {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='getCenter' and count(parameter)=0]"
			[Register ("getCenter", "()Lcom/gimbal/protocol/Point;", "GetGetCenterHandler")]
			get {
				if (id_getCenter == IntPtr.Zero)
					id_getCenter = JNIEnv.GetMethodID (class_ref, "getCenter", "()Lcom/gimbal/protocol/Point;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Point> (JNIEnv.CallObjectMethod  (Handle, id_getCenter), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Point> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCenter", "()Lcom/gimbal/protocol/Point;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='setCenter' and count(parameter)=1 and parameter[1][@type='com.gimbal.protocol.Point']]"
			[Register ("setCenter", "(Lcom/gimbal/protocol/Point;)V", "GetSetCenter_Lcom_gimbal_protocol_Point_Handler")]
			set {
				if (id_setCenter_Lcom_gimbal_protocol_Point_ == IntPtr.Zero)
					id_setCenter_Lcom_gimbal_protocol_Point_ = JNIEnv.GetMethodID (class_ref, "setCenter", "(Lcom/gimbal/protocol/Point;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCenter_Lcom_gimbal_protocol_Point_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCenter", "(Lcom/gimbal/protocol/Point;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPoints;
#pragma warning disable 0169
		static Delegate GetGetPointsHandler ()
		{
			if (cb_getPoints == null)
				cb_getPoints = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPoints);
			return cb_getPoints;
		}

		static IntPtr n_GetPoints (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Point>.ToLocalJniHandle (__this.Points);
		}
#pragma warning restore 0169

		static Delegate cb_setPoints_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetPoints_Ljava_util_List_Handler ()
		{
			if (cb_setPoints_Ljava_util_List_ == null)
				cb_setPoints_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPoints_Ljava_util_List_);
			return cb_setPoints_Ljava_util_List_;
		}

		static void n_SetPoints_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Point>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Points = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPoints;
		static IntPtr id_setPoints_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Protocol.Point> Points {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='getPoints' and count(parameter)=0]"
			[Register ("getPoints", "()Ljava/util/List;", "GetGetPointsHandler")]
			get {
				if (id_getPoints == IntPtr.Zero)
					id_getPoints = JNIEnv.GetMethodID (class_ref, "getPoints", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Point>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getPoints), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Point>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPoints", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='setPoints' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.protocol.Point&gt;']]"
			[Register ("setPoints", "(Ljava/util/List;)V", "GetSetPoints_Ljava_util_List_Handler")]
			set {
				if (id_setPoints_Ljava_util_List_ == IntPtr.Zero)
					id_setPoints_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setPoints", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Gimbal.Protocol.Point>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPoints_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPoints", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getRadius;
#pragma warning disable 0169
		static Delegate GetGetRadiusHandler ()
		{
			if (cb_getRadius == null)
				cb_getRadius = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetRadius);
			return cb_getRadius;
		}

		static IntPtr n_GetRadius (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Radius);
		}
#pragma warning restore 0169

		static Delegate cb_setRadius_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetRadius_Ljava_lang_Integer_Handler ()
		{
			if (cb_setRadius_Ljava_lang_Integer_ == null)
				cb_setRadius_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetRadius_Ljava_lang_Integer_);
			return cb_setRadius_Ljava_lang_Integer_;
		}

		static void n_SetRadius_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Radius = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getRadius;
		static IntPtr id_setRadius_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer Radius {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='getRadius' and count(parameter)=0]"
			[Register ("getRadius", "()Ljava/lang/Integer;", "GetGetRadiusHandler")]
			get {
				if (id_getRadius == IntPtr.Zero)
					id_getRadius = JNIEnv.GetMethodID (class_ref, "getRadius", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getRadius), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRadius", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='setRadius' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setRadius", "(Ljava/lang/Integer;)V", "GetSetRadius_Ljava_lang_Integer_Handler")]
			set {
				if (id_setRadius_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setRadius_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setRadius", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setRadius_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setRadius", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getShape;
#pragma warning disable 0169
		static Delegate GetGetShapeHandler ()
		{
			if (cb_getShape == null)
				cb_getShape = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetShape);
			return cb_getShape;
		}

		static IntPtr n_GetShape (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Shape);
		}
#pragma warning restore 0169

		static Delegate cb_setShape_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetShape_Ljava_lang_String_Handler ()
		{
			if (cb_setShape_Ljava_lang_String_ == null)
				cb_setShape_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetShape_Ljava_lang_String_);
			return cb_setShape_Ljava_lang_String_;
		}

		static void n_SetShape_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Shape = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getShape;
		static IntPtr id_setShape_Ljava_lang_String_;
		public virtual unsafe string Shape {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='getShape' and count(parameter)=0]"
			[Register ("getShape", "()Ljava/lang/String;", "GetGetShapeHandler")]
			get {
				if (id_getShape == IntPtr.Zero)
					id_getShape = JNIEnv.GetMethodID (class_ref, "getShape", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getShape), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getShape", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='setShape' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setShape", "(Ljava/lang/String;)V", "GetSetShape_Ljava_lang_String_Handler")]
			set {
				if (id_setShape_Ljava_lang_String_ == IntPtr.Zero)
					id_setShape_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setShape", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setShape_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setShape", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getSource;
#pragma warning disable 0169
		static Delegate GetGetSourceHandler ()
		{
			if (cb_getSource == null)
				cb_getSource = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSource);
			return cb_getSource;
		}

		static IntPtr n_GetSource (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Source);
		}
#pragma warning restore 0169

		static Delegate cb_setSource_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetSource_Ljava_lang_String_Handler ()
		{
			if (cb_setSource_Ljava_lang_String_ == null)
				cb_setSource_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSource_Ljava_lang_String_);
			return cb_setSource_Ljava_lang_String_;
		}

		static void n_SetSource_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Source = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSource;
		static IntPtr id_setSource_Ljava_lang_String_;
		public virtual unsafe string Source {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='getSource' and count(parameter)=0]"
			[Register ("getSource", "()Ljava/lang/String;", "GetGetSourceHandler")]
			get {
				if (id_getSource == IntPtr.Zero)
					id_getSource = JNIEnv.GetMethodID (class_ref, "getSource", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getSource), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSource", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='setSource' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setSource", "(Ljava/lang/String;)V", "GetSetSource_Ljava_lang_String_Handler")]
			set {
				if (id_setSource_Ljava_lang_String_ == IntPtr.Zero)
					id_setSource_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setSource", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSource_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSource", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getSourceId;
#pragma warning disable 0169
		static Delegate GetGetSourceIdHandler ()
		{
			if (cb_getSourceId == null)
				cb_getSourceId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSourceId);
			return cb_getSourceId;
		}

		static IntPtr n_GetSourceId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.SourceId);
		}
#pragma warning restore 0169

		static Delegate cb_setSourceId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetSourceId_Ljava_lang_String_Handler ()
		{
			if (cb_setSourceId_Ljava_lang_String_ == null)
				cb_setSourceId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSourceId_Ljava_lang_String_);
			return cb_setSourceId_Ljava_lang_String_;
		}

		static void n_SetSourceId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SourceId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSourceId;
		static IntPtr id_setSourceId_Ljava_lang_String_;
		public virtual unsafe string SourceId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='getSourceId' and count(parameter)=0]"
			[Register ("getSourceId", "()Ljava/lang/String;", "GetGetSourceIdHandler")]
			get {
				if (id_getSourceId == IntPtr.Zero)
					id_getSourceId = JNIEnv.GetMethodID (class_ref, "getSourceId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getSourceId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSourceId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='setSourceId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setSourceId", "(Ljava/lang/String;)V", "GetSetSourceId_Ljava_lang_String_Handler")]
			set {
				if (id_setSourceId_Ljava_lang_String_ == IntPtr.Zero)
					id_setSourceId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setSourceId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSourceId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSourceId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getSourceVersion;
#pragma warning disable 0169
		static Delegate GetGetSourceVersionHandler ()
		{
			if (cb_getSourceVersion == null)
				cb_getSourceVersion = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSourceVersion);
			return cb_getSourceVersion;
		}

		static IntPtr n_GetSourceVersion (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.SourceVersion);
		}
#pragma warning restore 0169

		static Delegate cb_setSourceVersion_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetSourceVersion_Ljava_lang_String_Handler ()
		{
			if (cb_setSourceVersion_Ljava_lang_String_ == null)
				cb_setSourceVersion_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSourceVersion_Ljava_lang_String_);
			return cb_setSourceVersion_Ljava_lang_String_;
		}

		static void n_SetSourceVersion_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.Geofence __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.Geofence> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SourceVersion = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSourceVersion;
		static IntPtr id_setSourceVersion_Ljava_lang_String_;
		public virtual unsafe string SourceVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='getSourceVersion' and count(parameter)=0]"
			[Register ("getSourceVersion", "()Ljava/lang/String;", "GetGetSourceVersionHandler")]
			get {
				if (id_getSourceVersion == IntPtr.Zero)
					id_getSourceVersion = JNIEnv.GetMethodID (class_ref, "getSourceVersion", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getSourceVersion), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSourceVersion", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='Geofence']/method[@name='setSourceVersion' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setSourceVersion", "(Ljava/lang/String;)V", "GetSetSourceVersion_Ljava_lang_String_Handler")]
			set {
				if (id_setSourceVersion_Ljava_lang_String_ == IntPtr.Zero)
					id_setSourceVersion_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setSourceVersion", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSourceVersion_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSourceVersion", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
