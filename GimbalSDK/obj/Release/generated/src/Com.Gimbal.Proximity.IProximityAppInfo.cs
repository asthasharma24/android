using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='ProximityAppInfo']"
	[Register ("com/gimbal/proximity/ProximityAppInfo", "", "Com.Gimbal.Proximity.IProximityAppInfoInvoker")]
	public partial interface IProximityAppInfo : IJavaObject {

		string ReceiverId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='ProximityAppInfo']/method[@name='getReceiverId' and count(parameter)=0]"
			[Register ("getReceiverId", "()Ljava/lang/String;", "GetGetReceiverIdHandler:Com.Gimbal.Proximity.IProximityAppInfoInvoker, GimbalSDK")] get;
		}

	}

	[global::Android.Runtime.Register ("com/gimbal/proximity/ProximityAppInfo", DoNotGenerateAcw=true)]
	internal class IProximityAppInfoInvoker : global::Java.Lang.Object, IProximityAppInfo {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/gimbal/proximity/ProximityAppInfo");
		IntPtr class_ref;

		public static IProximityAppInfo GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IProximityAppInfo> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.gimbal.proximity.ProximityAppInfo"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IProximityAppInfoInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IProximityAppInfoInvoker); }
		}

		static Delegate cb_getReceiverId;
#pragma warning disable 0169
		static Delegate GetGetReceiverIdHandler ()
		{
			if (cb_getReceiverId == null)
				cb_getReceiverId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetReceiverId);
			return cb_getReceiverId;
		}

		static IntPtr n_GetReceiverId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.IProximityAppInfo __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IProximityAppInfo> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ReceiverId);
		}
#pragma warning restore 0169

		IntPtr id_getReceiverId;
		public unsafe string ReceiverId {
			get {
				if (id_getReceiverId == IntPtr.Zero)
					id_getReceiverId = JNIEnv.GetMethodID (class_ref, "getReceiverId", "()Ljava/lang/String;");
				return JNIEnv.GetString (JNIEnv.CallObjectMethod (Handle, id_getReceiverId), JniHandleOwnership.TransferLocalRef);
			}
		}

	}

}
