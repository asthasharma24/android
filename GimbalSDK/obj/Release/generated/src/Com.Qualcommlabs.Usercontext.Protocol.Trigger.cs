using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qualcommlabs.Usercontext.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='Trigger']"
	[global::Android.Runtime.Register ("com/qualcommlabs/usercontext/protocol/Trigger", DoNotGenerateAcw=true)]
	public abstract partial class Trigger : global::Java.Lang.Object {

		// Metadata.xml XPath class reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='Trigger.TYPE']"
		[global::Android.Runtime.Register ("com/qualcommlabs/usercontext/protocol/Trigger$TYPE", DoNotGenerateAcw=true)]
		public sealed partial class TYPE : global::Java.Lang.Enum {


			static IntPtr PLACE_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='Trigger.TYPE']/field[@name='PLACE']"
			[Register ("PLACE")]
			public static global::Com.Qualcommlabs.Usercontext.Protocol.Trigger.TYPE Place {
				get {
					if (PLACE_jfieldId == IntPtr.Zero)
						PLACE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PLACE", "Lcom/qualcommlabs/usercontext/protocol/Trigger$TYPE;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PLACE_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.Trigger.TYPE> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr QUERIED_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='Trigger.TYPE']/field[@name='QUERIED']"
			[Register ("QUERIED")]
			public static global::Com.Qualcommlabs.Usercontext.Protocol.Trigger.TYPE Queried {
				get {
					if (QUERIED_jfieldId == IntPtr.Zero)
						QUERIED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "QUERIED", "Lcom/qualcommlabs/usercontext/protocol/Trigger$TYPE;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, QUERIED_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.Trigger.TYPE> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}

			static IntPtr TIME_jfieldId;

			// Metadata.xml XPath field reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='Trigger.TYPE']/field[@name='TIME']"
			[Register ("TIME")]
			public static global::Com.Qualcommlabs.Usercontext.Protocol.Trigger.TYPE Time {
				get {
					if (TIME_jfieldId == IntPtr.Zero)
						TIME_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "TIME", "Lcom/qualcommlabs/usercontext/protocol/Trigger$TYPE;");
					IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, TIME_jfieldId);
					return global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.Trigger.TYPE> (__ret, JniHandleOwnership.TransferLocalRef);
				}
			}
			internal static IntPtr java_class_handle;
			internal static IntPtr class_ref {
				get {
					return JNIEnv.FindClass ("com/qualcommlabs/usercontext/protocol/Trigger$TYPE", ref java_class_handle);
				}
			}

			protected override IntPtr ThresholdClass {
				get { return class_ref; }
			}

			protected override global::System.Type ThresholdType {
				get { return typeof (TYPE); }
			}

			internal TYPE (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

			static IntPtr id_valueOf_Ljava_lang_String_;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='Trigger.TYPE']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("valueOf", "(Ljava/lang/String;)Lcom/qualcommlabs/usercontext/protocol/Trigger$TYPE;", "")]
			public static unsafe global::Com.Qualcommlabs.Usercontext.Protocol.Trigger.TYPE ValueOf (string p0)
			{
				if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
					id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/qualcommlabs/usercontext/protocol/Trigger$TYPE;");
				IntPtr native_p0 = JNIEnv.NewString (p0);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_p0);
					global::Com.Qualcommlabs.Usercontext.Protocol.Trigger.TYPE __ret = global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.Trigger.TYPE> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
					return __ret;
				} finally {
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}

			static IntPtr id_values;
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='Trigger.TYPE']/method[@name='values' and count(parameter)=0]"
			[Register ("values", "()[Lcom/qualcommlabs/usercontext/protocol/Trigger$TYPE;", "")]
			public static unsafe global::Com.Qualcommlabs.Usercontext.Protocol.Trigger.TYPE[] Values ()
			{
				if (id_values == IntPtr.Zero)
					id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/qualcommlabs/usercontext/protocol/Trigger$TYPE;");
				try {
					return (global::Com.Qualcommlabs.Usercontext.Protocol.Trigger.TYPE[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Qualcommlabs.Usercontext.Protocol.Trigger.TYPE));
				} finally {
				}
			}

		}

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qualcommlabs/usercontext/protocol/Trigger", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Trigger); }
		}

		protected Trigger (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='Trigger']/constructor[@name='Trigger' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Trigger ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Trigger)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qualcommlabs.Usercontext.Protocol.Trigger __this = global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.Trigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_String_Handler ()
		{
			if (cb_setId_Ljava_lang_String_ == null)
				cb_setId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_String_);
			return cb_setId_Ljava_lang_String_;
		}

		static void n_SetId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qualcommlabs.Usercontext.Protocol.Trigger __this = global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.Trigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_String_;
		public virtual unsafe string Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='Trigger']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/String;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='Trigger']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setId", "(Ljava/lang/String;)V", "GetSetId_Ljava_lang_String_Handler")]
			set {
				if (id_setId_Ljava_lang_String_ == IntPtr.Zero)
					id_setId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getType;
#pragma warning disable 0169
		static Delegate GetGetTypeHandler ()
		{
			if (cb_getType == null)
				cb_getType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetType);
			return cb_getType;
		}

		static IntPtr n_GetType (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qualcommlabs.Usercontext.Protocol.Trigger __this = global::Java.Lang.Object.GetObject<global::Com.Qualcommlabs.Usercontext.Protocol.Trigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Type);
		}
#pragma warning restore 0169

		public abstract string Type {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='Trigger']/method[@name='getType' and count(parameter)=0]"
			[Register ("getType", "()Ljava/lang/String;", "GetGetTypeHandler")] get;
		}

	}

	[global::Android.Runtime.Register ("com/qualcommlabs/usercontext/protocol/Trigger", DoNotGenerateAcw=true)]
	internal partial class TriggerInvoker : Trigger {

		public TriggerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (TriggerInvoker); }
		}

		static IntPtr id_getType;
		public override unsafe string Type {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qualcommlabs.usercontext.protocol']/class[@name='Trigger']/method[@name='getType' and count(parameter)=0]"
			[Register ("getType", "()Ljava/lang/String;", "GetGetTypeHandler")]
			get {
				if (id_getType == IntPtr.Zero)
					id_getType = JNIEnv.GetMethodID (class_ref, "getType", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getType), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

	}

}
