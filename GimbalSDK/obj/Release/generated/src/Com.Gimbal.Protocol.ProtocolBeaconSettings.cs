using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolBeaconSettings']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/ProtocolBeaconSettings", DoNotGenerateAcw=true)]
	public partial class ProtocolBeaconSettings : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/ProtocolBeaconSettings", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ProtocolBeaconSettings); }
		}

		protected ProtocolBeaconSettings (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolBeaconSettings']/constructor[@name='ProtocolBeaconSettings' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ProtocolBeaconSettings ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ProtocolBeaconSettings)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getArrivalRssi;
#pragma warning disable 0169
		static Delegate GetGetArrivalRssiHandler ()
		{
			if (cb_getArrivalRssi == null)
				cb_getArrivalRssi = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetArrivalRssi);
			return cb_getArrivalRssi;
		}

		static IntPtr n_GetArrivalRssi (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolBeaconSettings __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolBeaconSettings> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ArrivalRssi);
		}
#pragma warning restore 0169

		static Delegate cb_setArrivalRssi_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetArrivalRssi_Ljava_lang_Integer_Handler ()
		{
			if (cb_setArrivalRssi_Ljava_lang_Integer_ == null)
				cb_setArrivalRssi_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetArrivalRssi_Ljava_lang_Integer_);
			return cb_setArrivalRssi_Ljava_lang_Integer_;
		}

		static void n_SetArrivalRssi_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolBeaconSettings __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolBeaconSettings> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ArrivalRssi = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getArrivalRssi;
		static IntPtr id_setArrivalRssi_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer ArrivalRssi {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolBeaconSettings']/method[@name='getArrivalRssi' and count(parameter)=0]"
			[Register ("getArrivalRssi", "()Ljava/lang/Integer;", "GetGetArrivalRssiHandler")]
			get {
				if (id_getArrivalRssi == IntPtr.Zero)
					id_getArrivalRssi = JNIEnv.GetMethodID (class_ref, "getArrivalRssi", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getArrivalRssi), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getArrivalRssi", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolBeaconSettings']/method[@name='setArrivalRssi' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setArrivalRssi", "(Ljava/lang/Integer;)V", "GetSetArrivalRssi_Ljava_lang_Integer_Handler")]
			set {
				if (id_setArrivalRssi_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setArrivalRssi_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setArrivalRssi", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setArrivalRssi_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setArrivalRssi", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getBackgroundDepartureInterval;
#pragma warning disable 0169
		static Delegate GetGetBackgroundDepartureIntervalHandler ()
		{
			if (cb_getBackgroundDepartureInterval == null)
				cb_getBackgroundDepartureInterval = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBackgroundDepartureInterval);
			return cb_getBackgroundDepartureInterval;
		}

		static IntPtr n_GetBackgroundDepartureInterval (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolBeaconSettings __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolBeaconSettings> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.BackgroundDepartureInterval);
		}
#pragma warning restore 0169

		static Delegate cb_setBackgroundDepartureInterval_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetBackgroundDepartureInterval_Ljava_lang_Integer_Handler ()
		{
			if (cb_setBackgroundDepartureInterval_Ljava_lang_Integer_ == null)
				cb_setBackgroundDepartureInterval_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetBackgroundDepartureInterval_Ljava_lang_Integer_);
			return cb_setBackgroundDepartureInterval_Ljava_lang_Integer_;
		}

		static void n_SetBackgroundDepartureInterval_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolBeaconSettings __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolBeaconSettings> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.BackgroundDepartureInterval = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getBackgroundDepartureInterval;
		static IntPtr id_setBackgroundDepartureInterval_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer BackgroundDepartureInterval {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolBeaconSettings']/method[@name='getBackgroundDepartureInterval' and count(parameter)=0]"
			[Register ("getBackgroundDepartureInterval", "()Ljava/lang/Integer;", "GetGetBackgroundDepartureIntervalHandler")]
			get {
				if (id_getBackgroundDepartureInterval == IntPtr.Zero)
					id_getBackgroundDepartureInterval = JNIEnv.GetMethodID (class_ref, "getBackgroundDepartureInterval", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getBackgroundDepartureInterval), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBackgroundDepartureInterval", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolBeaconSettings']/method[@name='setBackgroundDepartureInterval' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setBackgroundDepartureInterval", "(Ljava/lang/Integer;)V", "GetSetBackgroundDepartureInterval_Ljava_lang_Integer_Handler")]
			set {
				if (id_setBackgroundDepartureInterval_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setBackgroundDepartureInterval_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setBackgroundDepartureInterval", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setBackgroundDepartureInterval_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setBackgroundDepartureInterval", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureInterval;
#pragma warning disable 0169
		static Delegate GetGetDepartureIntervalHandler ()
		{
			if (cb_getDepartureInterval == null)
				cb_getDepartureInterval = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDepartureInterval);
			return cb_getDepartureInterval;
		}

		static IntPtr n_GetDepartureInterval (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolBeaconSettings __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolBeaconSettings> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DepartureInterval);
		}
#pragma warning restore 0169

		static Delegate cb_setDepartureInterval_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetDepartureInterval_Ljava_lang_Integer_Handler ()
		{
			if (cb_setDepartureInterval_Ljava_lang_Integer_ == null)
				cb_setDepartureInterval_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDepartureInterval_Ljava_lang_Integer_);
			return cb_setDepartureInterval_Ljava_lang_Integer_;
		}

		static void n_SetDepartureInterval_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolBeaconSettings __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolBeaconSettings> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DepartureInterval = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureInterval;
		static IntPtr id_setDepartureInterval_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer DepartureInterval {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolBeaconSettings']/method[@name='getDepartureInterval' and count(parameter)=0]"
			[Register ("getDepartureInterval", "()Ljava/lang/Integer;", "GetGetDepartureIntervalHandler")]
			get {
				if (id_getDepartureInterval == IntPtr.Zero)
					id_getDepartureInterval = JNIEnv.GetMethodID (class_ref, "getDepartureInterval", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getDepartureInterval), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureInterval", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolBeaconSettings']/method[@name='setDepartureInterval' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setDepartureInterval", "(Ljava/lang/Integer;)V", "GetSetDepartureInterval_Ljava_lang_Integer_Handler")]
			set {
				if (id_setDepartureInterval_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setDepartureInterval_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setDepartureInterval", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDepartureInterval_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDepartureInterval", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDepartureRssi;
#pragma warning disable 0169
		static Delegate GetGetDepartureRssiHandler ()
		{
			if (cb_getDepartureRssi == null)
				cb_getDepartureRssi = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDepartureRssi);
			return cb_getDepartureRssi;
		}

		static IntPtr n_GetDepartureRssi (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.ProtocolBeaconSettings __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolBeaconSettings> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DepartureRssi);
		}
#pragma warning restore 0169

		static Delegate cb_setDepartureRssi_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetDepartureRssi_Ljava_lang_Integer_Handler ()
		{
			if (cb_setDepartureRssi_Ljava_lang_Integer_ == null)
				cb_setDepartureRssi_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDepartureRssi_Ljava_lang_Integer_);
			return cb_setDepartureRssi_Ljava_lang_Integer_;
		}

		static void n_SetDepartureRssi_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.ProtocolBeaconSettings __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.ProtocolBeaconSettings> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DepartureRssi = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDepartureRssi;
		static IntPtr id_setDepartureRssi_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer DepartureRssi {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolBeaconSettings']/method[@name='getDepartureRssi' and count(parameter)=0]"
			[Register ("getDepartureRssi", "()Ljava/lang/Integer;", "GetGetDepartureRssiHandler")]
			get {
				if (id_getDepartureRssi == IntPtr.Zero)
					id_getDepartureRssi = JNIEnv.GetMethodID (class_ref, "getDepartureRssi", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getDepartureRssi), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDepartureRssi", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='ProtocolBeaconSettings']/method[@name='setDepartureRssi' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setDepartureRssi", "(Ljava/lang/Integer;)V", "GetSetDepartureRssi_Ljava_lang_Integer_Handler")]
			set {
				if (id_setDepartureRssi_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setDepartureRssi_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setDepartureRssi", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDepartureRssi_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDepartureRssi", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

	}
}
