using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Service.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='GetBeaconUUIDResponse']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/service/protocol/GetBeaconUUIDResponse", DoNotGenerateAcw=true)]
	public partial class GetBeaconUUIDResponse : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/service/protocol/GetBeaconUUIDResponse", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (GetBeaconUUIDResponse); }
		}

		protected GetBeaconUUIDResponse (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='GetBeaconUUIDResponse']/constructor[@name='GetBeaconUUIDResponse' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe GetBeaconUUIDResponse ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (GetBeaconUUIDResponse)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getBeaconUuids;
#pragma warning disable 0169
		static Delegate GetGetBeaconUuidsHandler ()
		{
			if (cb_getBeaconUuids == null)
				cb_getBeaconUuids = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetBeaconUuids);
			return cb_getBeaconUuids;
		}

		static IntPtr n_GetBeaconUuids (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.GetBeaconUUIDResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.GetBeaconUUIDResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID>.ToLocalJniHandle (__this.BeaconUuids);
		}
#pragma warning restore 0169

		static Delegate cb_setBeaconUuids_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetBeaconUuids_Ljava_util_List_Handler ()
		{
			if (cb_setBeaconUuids_Ljava_util_List_ == null)
				cb_setBeaconUuids_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetBeaconUuids_Ljava_util_List_);
			return cb_setBeaconUuids_Ljava_util_List_;
		}

		static void n_SetBeaconUuids_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.GetBeaconUUIDResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.GetBeaconUUIDResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.BeaconUuids = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getBeaconUuids;
		static IntPtr id_setBeaconUuids_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID> BeaconUuids {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='GetBeaconUUIDResponse']/method[@name='getBeaconUuids' and count(parameter)=0]"
			[Register ("getBeaconUuids", "()Ljava/util/List;", "GetGetBeaconUuidsHandler")]
			get {
				if (id_getBeaconUuids == IntPtr.Zero)
					id_getBeaconUuids = JNIEnv.GetMethodID (class_ref, "getBeaconUuids", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getBeaconUuids), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getBeaconUuids", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='GetBeaconUUIDResponse']/method[@name='setBeaconUuids' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.gimbal.proximity.core.service.protocol.internal.BeaconUUID&gt;']]"
			[Register ("setBeaconUuids", "(Ljava/util/List;)V", "GetSetBeaconUuids_Ljava_util_List_Handler")]
			set {
				if (id_setBeaconUuids_Ljava_util_List_ == IntPtr.Zero)
					id_setBeaconUuids_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setBeaconUuids", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Gimbal.Proximity.Core.Service.Protocol.Internal.BeaconUUID>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setBeaconUuids_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setBeaconUuids", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
