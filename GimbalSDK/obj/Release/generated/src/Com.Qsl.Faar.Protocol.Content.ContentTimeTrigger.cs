using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Content {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/content/ContentTimeTrigger", DoNotGenerateAcw=true)]
	public partial class ContentTimeTrigger : global::Java.Lang.Object, global::Java.IO.ISerializable {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/content/ContentTimeTrigger", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ContentTimeTrigger); }
		}

		protected ContentTimeTrigger (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/constructor[@name='ContentTimeTrigger' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ContentTimeTrigger ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ContentTimeTrigger)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getCampaignId;
#pragma warning disable 0169
		static Delegate GetGetCampaignIdHandler ()
		{
			if (cb_getCampaignId == null)
				cb_getCampaignId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCampaignId);
			return cb_getCampaignId;
		}

		static IntPtr n_GetCampaignId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.CampaignId);
		}
#pragma warning restore 0169

		static Delegate cb_setCampaignId_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetCampaignId_Ljava_lang_Integer_Handler ()
		{
			if (cb_setCampaignId_Ljava_lang_Integer_ == null)
				cb_setCampaignId_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCampaignId_Ljava_lang_Integer_);
			return cb_setCampaignId_Ljava_lang_Integer_;
		}

		static void n_SetCampaignId_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.CampaignId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getCampaignId;
		static IntPtr id_setCampaignId_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer CampaignId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='getCampaignId' and count(parameter)=0]"
			[Register ("getCampaignId", "()Ljava/lang/Integer;", "GetGetCampaignIdHandler")]
			get {
				if (id_getCampaignId == IntPtr.Zero)
					id_getCampaignId = JNIEnv.GetMethodID (class_ref, "getCampaignId", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getCampaignId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCampaignId", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='setCampaignId' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setCampaignId", "(Ljava/lang/Integer;)V", "GetSetCampaignId_Ljava_lang_Integer_Handler")]
			set {
				if (id_setCampaignId_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setCampaignId_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setCampaignId", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCampaignId_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCampaignId", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDayOfMonth;
#pragma warning disable 0169
		static Delegate GetGetDayOfMonthHandler ()
		{
			if (cb_getDayOfMonth == null)
				cb_getDayOfMonth = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDayOfMonth);
			return cb_getDayOfMonth;
		}

		static IntPtr n_GetDayOfMonth (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DayOfMonth);
		}
#pragma warning restore 0169

		static Delegate cb_setDayOfMonth_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetDayOfMonth_Ljava_lang_Integer_Handler ()
		{
			if (cb_setDayOfMonth_Ljava_lang_Integer_ == null)
				cb_setDayOfMonth_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDayOfMonth_Ljava_lang_Integer_);
			return cb_setDayOfMonth_Ljava_lang_Integer_;
		}

		static void n_SetDayOfMonth_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DayOfMonth = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDayOfMonth;
		static IntPtr id_setDayOfMonth_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer DayOfMonth {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='getDayOfMonth' and count(parameter)=0]"
			[Register ("getDayOfMonth", "()Ljava/lang/Integer;", "GetGetDayOfMonthHandler")]
			get {
				if (id_getDayOfMonth == IntPtr.Zero)
					id_getDayOfMonth = JNIEnv.GetMethodID (class_ref, "getDayOfMonth", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getDayOfMonth), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDayOfMonth", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='setDayOfMonth' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setDayOfMonth", "(Ljava/lang/Integer;)V", "GetSetDayOfMonth_Ljava_lang_Integer_Handler")]
			set {
				if (id_setDayOfMonth_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setDayOfMonth_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setDayOfMonth", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDayOfMonth_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDayOfMonth", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getDayOfWeek;
#pragma warning disable 0169
		static Delegate GetGetDayOfWeekHandler ()
		{
			if (cb_getDayOfWeek == null)
				cb_getDayOfWeek = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDayOfWeek);
			return cb_getDayOfWeek;
		}

		static IntPtr n_GetDayOfWeek (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.DayOfWeek);
		}
#pragma warning restore 0169

		static Delegate cb_setDayOfWeek_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetDayOfWeek_Ljava_lang_Integer_Handler ()
		{
			if (cb_setDayOfWeek_Ljava_lang_Integer_ == null)
				cb_setDayOfWeek_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDayOfWeek_Ljava_lang_Integer_);
			return cb_setDayOfWeek_Ljava_lang_Integer_;
		}

		static void n_SetDayOfWeek_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.DayOfWeek = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDayOfWeek;
		static IntPtr id_setDayOfWeek_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer DayOfWeek {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='getDayOfWeek' and count(parameter)=0]"
			[Register ("getDayOfWeek", "()Ljava/lang/Integer;", "GetGetDayOfWeekHandler")]
			get {
				if (id_getDayOfWeek == IntPtr.Zero)
					id_getDayOfWeek = JNIEnv.GetMethodID (class_ref, "getDayOfWeek", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getDayOfWeek), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDayOfWeek", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='setDayOfWeek' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setDayOfWeek", "(Ljava/lang/Integer;)V", "GetSetDayOfWeek_Ljava_lang_Integer_Handler")]
			set {
				if (id_setDayOfWeek_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setDayOfWeek_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setDayOfWeek", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDayOfWeek_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDayOfWeek", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getHour;
#pragma warning disable 0169
		static Delegate GetGetHourHandler ()
		{
			if (cb_getHour == null)
				cb_getHour = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetHour);
			return cb_getHour;
		}

		static IntPtr n_GetHour (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Hour);
		}
#pragma warning restore 0169

		static Delegate cb_setHour_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetHour_Ljava_lang_Integer_Handler ()
		{
			if (cb_setHour_Ljava_lang_Integer_ == null)
				cb_setHour_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetHour_Ljava_lang_Integer_);
			return cb_setHour_Ljava_lang_Integer_;
		}

		static void n_SetHour_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Hour = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getHour;
		static IntPtr id_setHour_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer Hour {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='getHour' and count(parameter)=0]"
			[Register ("getHour", "()Ljava/lang/Integer;", "GetGetHourHandler")]
			get {
				if (id_getHour == IntPtr.Zero)
					id_getHour = JNIEnv.GetMethodID (class_ref, "getHour", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getHour), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getHour", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='setHour' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setHour", "(Ljava/lang/Integer;)V", "GetSetHour_Ljava_lang_Integer_Handler")]
			set {
				if (id_setHour_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setHour_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setHour", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setHour_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setHour", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getMinute;
#pragma warning disable 0169
		static Delegate GetGetMinuteHandler ()
		{
			if (cb_getMinute == null)
				cb_getMinute = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetMinute);
			return cb_getMinute;
		}

		static IntPtr n_GetMinute (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Minute);
		}
#pragma warning restore 0169

		static Delegate cb_setMinute_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetMinute_Ljava_lang_Integer_Handler ()
		{
			if (cb_setMinute_Ljava_lang_Integer_ == null)
				cb_setMinute_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetMinute_Ljava_lang_Integer_);
			return cb_setMinute_Ljava_lang_Integer_;
		}

		static void n_SetMinute_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Minute = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getMinute;
		static IntPtr id_setMinute_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer Minute {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='getMinute' and count(parameter)=0]"
			[Register ("getMinute", "()Ljava/lang/Integer;", "GetGetMinuteHandler")]
			get {
				if (id_getMinute == IntPtr.Zero)
					id_getMinute = JNIEnv.GetMethodID (class_ref, "getMinute", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getMinute), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMinute", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='setMinute' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setMinute", "(Ljava/lang/Integer;)V", "GetSetMinute_Ljava_lang_Integer_Handler")]
			set {
				if (id_setMinute_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setMinute_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setMinute", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setMinute_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setMinute", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getMonth;
#pragma warning disable 0169
		static Delegate GetGetMonthHandler ()
		{
			if (cb_getMonth == null)
				cb_getMonth = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetMonth);
			return cb_getMonth;
		}

		static IntPtr n_GetMonth (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Month);
		}
#pragma warning restore 0169

		static Delegate cb_setMonth_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetMonth_Ljava_lang_Integer_Handler ()
		{
			if (cb_setMonth_Ljava_lang_Integer_ == null)
				cb_setMonth_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetMonth_Ljava_lang_Integer_);
			return cb_setMonth_Ljava_lang_Integer_;
		}

		static void n_SetMonth_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Month = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getMonth;
		static IntPtr id_setMonth_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer Month {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='getMonth' and count(parameter)=0]"
			[Register ("getMonth", "()Ljava/lang/Integer;", "GetGetMonthHandler")]
			get {
				if (id_getMonth == IntPtr.Zero)
					id_getMonth = JNIEnv.GetMethodID (class_ref, "getMonth", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getMonth), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getMonth", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='setMonth' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setMonth", "(Ljava/lang/Integer;)V", "GetSetMonth_Ljava_lang_Integer_Handler")]
			set {
				if (id_setMonth_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setMonth_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setMonth", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setMonth_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setMonth", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getOrganizationId;
#pragma warning disable 0169
		static Delegate GetGetOrganizationIdHandler ()
		{
			if (cb_getOrganizationId == null)
				cb_getOrganizationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationId);
			return cb_getOrganizationId;
		}

		static IntPtr n_GetOrganizationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OrganizationId);
		}
#pragma warning restore 0169

		static Delegate cb_setOrganizationId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationId_Ljava_lang_Long_Handler ()
		{
			if (cb_setOrganizationId_Ljava_lang_Long_ == null)
				cb_setOrganizationId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationId_Ljava_lang_Long_);
			return cb_setOrganizationId_Ljava_lang_Long_;
		}

		static void n_SetOrganizationId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OrganizationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationId;
		static IntPtr id_setOrganizationId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long OrganizationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='getOrganizationId' and count(parameter)=0]"
			[Register ("getOrganizationId", "()Ljava/lang/Long;", "GetGetOrganizationIdHandler")]
			get {
				if (id_getOrganizationId == IntPtr.Zero)
					id_getOrganizationId = JNIEnv.GetMethodID (class_ref, "getOrganizationId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='setOrganizationId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setOrganizationId", "(Ljava/lang/Long;)V", "GetSetOrganizationId_Ljava_lang_Long_Handler")]
			set {
				if (id_setOrganizationId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setOrganizationId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setOrganizationId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOrganizationId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getTweenContentRandomInterval;
#pragma warning disable 0169
		static Delegate GetGetTweenContentRandomIntervalHandler ()
		{
			if (cb_getTweenContentRandomInterval == null)
				cb_getTweenContentRandomInterval = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTweenContentRandomInterval);
			return cb_getTweenContentRandomInterval;
		}

		static IntPtr n_GetTweenContentRandomInterval (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.TweenContentRandomInterval);
		}
#pragma warning restore 0169

		static Delegate cb_setTweenContentRandomInterval_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetTweenContentRandomInterval_Ljava_lang_Integer_Handler ()
		{
			if (cb_setTweenContentRandomInterval_Ljava_lang_Integer_ == null)
				cb_setTweenContentRandomInterval_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTweenContentRandomInterval_Ljava_lang_Integer_);
			return cb_setTweenContentRandomInterval_Ljava_lang_Integer_;
		}

		static void n_SetTweenContentRandomInterval_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.TweenContentRandomInterval = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTweenContentRandomInterval;
		static IntPtr id_setTweenContentRandomInterval_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer TweenContentRandomInterval {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='getTweenContentRandomInterval' and count(parameter)=0]"
			[Register ("getTweenContentRandomInterval", "()Ljava/lang/Integer;", "GetGetTweenContentRandomIntervalHandler")]
			get {
				if (id_getTweenContentRandomInterval == IntPtr.Zero)
					id_getTweenContentRandomInterval = JNIEnv.GetMethodID (class_ref, "getTweenContentRandomInterval", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getTweenContentRandomInterval), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTweenContentRandomInterval", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='setTweenContentRandomInterval' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setTweenContentRandomInterval", "(Ljava/lang/Integer;)V", "GetSetTweenContentRandomInterval_Ljava_lang_Integer_Handler")]
			set {
				if (id_setTweenContentRandomInterval_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setTweenContentRandomInterval_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setTweenContentRandomInterval", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTweenContentRandomInterval_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTweenContentRandomInterval", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getYear;
#pragma warning disable 0169
		static Delegate GetGetYearHandler ()
		{
			if (cb_getYear == null)
				cb_getYear = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetYear);
			return cb_getYear;
		}

		static IntPtr n_GetYear (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Year);
		}
#pragma warning restore 0169

		static Delegate cb_setYear_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetYear_Ljava_lang_Integer_Handler ()
		{
			if (cb_setYear_Ljava_lang_Integer_ == null)
				cb_setYear_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetYear_Ljava_lang_Integer_);
			return cb_setYear_Ljava_lang_Integer_;
		}

		static void n_SetYear_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTrigger> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Year = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getYear;
		static IntPtr id_setYear_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer Year {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='getYear' and count(parameter)=0]"
			[Register ("getYear", "()Ljava/lang/Integer;", "GetGetYearHandler")]
			get {
				if (id_getYear == IntPtr.Zero)
					id_getYear = JNIEnv.GetMethodID (class_ref, "getYear", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getYear), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getYear", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTrigger']/method[@name='setYear' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setYear", "(Ljava/lang/Integer;)V", "GetSetYear_Ljava_lang_Integer_Handler")]
			set {
				if (id_setYear_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setYear_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setYear", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setYear_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setYear", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

	}
}
