using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Service {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.service']/class[@name='ServiceCallbackHandler']"
	[global::Android.Runtime.Register ("com/qsl/faar/service/ServiceCallbackHandler", DoNotGenerateAcw=true)]
	[global::Java.Interop.JavaTypeParameters (new string [] {"T"})]
	public abstract partial class ServiceCallbackHandler : global::Android.OS.Handler, global::Com.Qsl.Faar.Service.IServiceCallback {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/service/ServiceCallbackHandler", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ServiceCallbackHandler); }
		}

		protected ServiceCallbackHandler (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.service']/class[@name='ServiceCallbackHandler']/constructor[@name='ServiceCallbackHandler' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ServiceCallbackHandler ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ServiceCallbackHandler)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_failure_ILjava_lang_String_;
#pragma warning disable 0169
		static Delegate GetFailure_ILjava_lang_String_Handler ()
		{
			if (cb_failure_ILjava_lang_String_ == null)
				cb_failure_ILjava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, IntPtr>) n_Failure_ILjava_lang_String_);
			return cb_failure_ILjava_lang_String_;
		}

		static void n_Failure_ILjava_lang_String_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Com.Qsl.Faar.Service.ServiceCallbackHandler __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.ServiceCallbackHandler> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.Failure (p0, p1);
		}
#pragma warning restore 0169

		static IntPtr id_failure_ILjava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service']/class[@name='ServiceCallbackHandler']/method[@name='failure' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='java.lang.String']]"
		[Register ("failure", "(ILjava/lang/String;)V", "GetFailure_ILjava_lang_String_Handler")]
		public virtual unsafe void Failure (int p0, string p1)
		{
			if (id_failure_ILjava_lang_String_ == IntPtr.Zero)
				id_failure_ILjava_lang_String_ = JNIEnv.GetMethodID (class_ref, "failure", "(ILjava/lang/String;)V");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_failure_ILjava_lang_String_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "failure", "(ILjava/lang/String;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static Delegate cb_failureResponse_ILjava_lang_String_;
#pragma warning disable 0169
		static Delegate GetFailureResponse_ILjava_lang_String_Handler ()
		{
			if (cb_failureResponse_ILjava_lang_String_ == null)
				cb_failureResponse_ILjava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int, IntPtr>) n_FailureResponse_ILjava_lang_String_);
			return cb_failureResponse_ILjava_lang_String_;
		}

		static void n_FailureResponse_ILjava_lang_String_ (IntPtr jnienv, IntPtr native__this, int p0, IntPtr native_p1)
		{
			global::Com.Qsl.Faar.Service.ServiceCallbackHandler __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.ServiceCallbackHandler> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			__this.FailureResponse (p0, p1);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service']/class[@name='ServiceCallbackHandler']/method[@name='failureResponse' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='java.lang.String']]"
		[Register ("failureResponse", "(ILjava/lang/String;)V", "GetFailureResponse_ILjava_lang_String_Handler")]
		public abstract void FailureResponse (int p0, string p1);

		static Delegate cb_success_Ljava_lang_Object_;
#pragma warning disable 0169
		static Delegate GetSuccess_Ljava_lang_Object_Handler ()
		{
			if (cb_success_Ljava_lang_Object_ == null)
				cb_success_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_Success_Ljava_lang_Object_);
			return cb_success_Ljava_lang_Object_;
		}

		static void n_Success_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Service.ServiceCallbackHandler __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.ServiceCallbackHandler> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Object p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Success (p0);
		}
#pragma warning restore 0169

		static IntPtr id_success_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service']/class[@name='ServiceCallbackHandler']/method[@name='success' and count(parameter)=1 and parameter[1][@type='T']]"
		[Register ("success", "(Ljava/lang/Object;)V", "GetSuccess_Ljava_lang_Object_Handler")]
		public virtual unsafe void Success (global::Java.Lang.Object p0)
		{
			if (id_success_Ljava_lang_Object_ == IntPtr.Zero)
				id_success_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "success", "(Ljava/lang/Object;)V");
			IntPtr native_p0 = JNIEnv.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_success_Ljava_lang_Object_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "success", "(Ljava/lang/Object;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static Delegate cb_successResponse_Ljava_lang_Object_;
#pragma warning disable 0169
		static Delegate GetSuccessResponse_Ljava_lang_Object_Handler ()
		{
			if (cb_successResponse_Ljava_lang_Object_ == null)
				cb_successResponse_Ljava_lang_Object_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SuccessResponse_Ljava_lang_Object_);
			return cb_successResponse_Ljava_lang_Object_;
		}

		static void n_SuccessResponse_Ljava_lang_Object_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Service.ServiceCallbackHandler __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Service.ServiceCallbackHandler> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Object p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Object> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SuccessResponse (p0);
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service']/class[@name='ServiceCallbackHandler']/method[@name='successResponse' and count(parameter)=1 and parameter[1][@type='T']]"
		[Register ("successResponse", "(Ljava/lang/Object;)V", "GetSuccessResponse_Ljava_lang_Object_Handler")]
		public abstract void SuccessResponse (global::Java.Lang.Object p0);

	}

	[global::Android.Runtime.Register ("com/qsl/faar/service/ServiceCallbackHandler", DoNotGenerateAcw=true)]
	internal partial class ServiceCallbackHandlerInvoker : ServiceCallbackHandler, global::Com.Qsl.Faar.Service.IServiceCallback {

		public ServiceCallbackHandlerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (ServiceCallbackHandlerInvoker); }
		}

		static IntPtr id_failureResponse_ILjava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service']/class[@name='ServiceCallbackHandler']/method[@name='failureResponse' and count(parameter)=2 and parameter[1][@type='int'] and parameter[2][@type='java.lang.String']]"
		[Register ("failureResponse", "(ILjava/lang/String;)V", "GetFailureResponse_ILjava_lang_String_Handler")]
		public override unsafe void FailureResponse (int p0, string p1)
		{
			if (id_failureResponse_ILjava_lang_String_ == IntPtr.Zero)
				id_failureResponse_ILjava_lang_String_ = JNIEnv.GetMethodID (class_ref, "failureResponse", "(ILjava/lang/String;)V");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				JNIEnv.CallVoidMethod  (Handle, id_failureResponse_ILjava_lang_String_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static IntPtr id_successResponse_Ljava_lang_Object_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.service']/class[@name='ServiceCallbackHandler']/method[@name='successResponse' and count(parameter)=1 and parameter[1][@type='T']]"
		[Register ("successResponse", "(Ljava/lang/Object;)V", "GetSuccessResponse_Ljava_lang_Object_Handler")]
		public override unsafe void SuccessResponse (global::Java.Lang.Object p0)
		{
			if (id_successResponse_Ljava_lang_Object_ == IntPtr.Zero)
				id_successResponse_Ljava_lang_Object_ = JNIEnv.GetMethodID (class_ref, "successResponse", "(Ljava/lang/Object;)V");
			IntPtr native_p0 = JNIEnv.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				JNIEnv.CallVoidMethod  (Handle, id_successResponse_Ljava_lang_Object_, __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}

}
