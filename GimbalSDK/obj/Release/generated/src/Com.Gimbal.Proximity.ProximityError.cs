using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/ProximityError", DoNotGenerateAcw=true)]
	public sealed partial class ProximityError : global::Java.Lang.Enum {


		static IntPtr PROXIMITY_API_LEVEL_NOT_SUPPORTED_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_API_LEVEL_NOT_SUPPORTED']"
		[Register ("PROXIMITY_API_LEVEL_NOT_SUPPORTED")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityApiLevelNotSupported {
			get {
				if (PROXIMITY_API_LEVEL_NOT_SUPPORTED_jfieldId == IntPtr.Zero)
					PROXIMITY_API_LEVEL_NOT_SUPPORTED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_API_LEVEL_NOT_SUPPORTED", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_API_LEVEL_NOT_SUPPORTED_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_APP_NOT_INITIALIZED_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_APP_NOT_INITIALIZED']"
		[Register ("PROXIMITY_APP_NOT_INITIALIZED")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityAppNotInitialized {
			get {
				if (PROXIMITY_APP_NOT_INITIALIZED_jfieldId == IntPtr.Zero)
					PROXIMITY_APP_NOT_INITIALIZED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_APP_NOT_INITIALIZED", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_APP_NOT_INITIALIZED_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_APP_NOT_REGISTERED_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_APP_NOT_REGISTERED']"
		[Register ("PROXIMITY_APP_NOT_REGISTERED")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityAppNotRegistered {
			get {
				if (PROXIMITY_APP_NOT_REGISTERED_jfieldId == IntPtr.Zero)
					PROXIMITY_APP_NOT_REGISTERED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_APP_NOT_REGISTERED", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_APP_NOT_REGISTERED_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_BLE_SUPPORT_NOT_AVAILABLE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_BLE_SUPPORT_NOT_AVAILABLE']"
		[Register ("PROXIMITY_BLE_SUPPORT_NOT_AVAILABLE")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityBleSupportNotAvailable {
			get {
				if (PROXIMITY_BLE_SUPPORT_NOT_AVAILABLE_jfieldId == IntPtr.Zero)
					PROXIMITY_BLE_SUPPORT_NOT_AVAILABLE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_BLE_SUPPORT_NOT_AVAILABLE", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_BLE_SUPPORT_NOT_AVAILABLE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_BLUETOOTH_IS_OFF_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_BLUETOOTH_IS_OFF']"
		[Register ("PROXIMITY_BLUETOOTH_IS_OFF")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityBluetoothIsOff {
			get {
				if (PROXIMITY_BLUETOOTH_IS_OFF_jfieldId == IntPtr.Zero)
					PROXIMITY_BLUETOOTH_IS_OFF_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_BLUETOOTH_IS_OFF", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_BLUETOOTH_IS_OFF_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_INITIALIZATION_FAILURE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_INITIALIZATION_FAILURE']"
		[Register ("PROXIMITY_INITIALIZATION_FAILURE")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityInitializationFailure {
			get {
				if (PROXIMITY_INITIALIZATION_FAILURE_jfieldId == IntPtr.Zero)
					PROXIMITY_INITIALIZATION_FAILURE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_INITIALIZATION_FAILURE", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_INITIALIZATION_FAILURE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_NETWORK_AUTH_FAILURE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_NETWORK_AUTH_FAILURE']"
		[Register ("PROXIMITY_NETWORK_AUTH_FAILURE")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityNetworkAuthFailure {
			get {
				if (PROXIMITY_NETWORK_AUTH_FAILURE_jfieldId == IntPtr.Zero)
					PROXIMITY_NETWORK_AUTH_FAILURE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_NETWORK_AUTH_FAILURE", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_NETWORK_AUTH_FAILURE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_RELEASE_VERSION_NOT_SUPPORTED_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_RELEASE_VERSION_NOT_SUPPORTED']"
		[Register ("PROXIMITY_RELEASE_VERSION_NOT_SUPPORTED")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityReleaseVersionNotSupported {
			get {
				if (PROXIMITY_RELEASE_VERSION_NOT_SUPPORTED_jfieldId == IntPtr.Zero)
					PROXIMITY_RELEASE_VERSION_NOT_SUPPORTED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_RELEASE_VERSION_NOT_SUPPORTED", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_RELEASE_VERSION_NOT_SUPPORTED_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_REQUIRES_AUTHORIZATION_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_REQUIRES_AUTHORIZATION']"
		[Register ("PROXIMITY_REQUIRES_AUTHORIZATION")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityRequiresAuthorization {
			get {
				if (PROXIMITY_REQUIRES_AUTHORIZATION_jfieldId == IntPtr.Zero)
					PROXIMITY_REQUIRES_AUTHORIZATION_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_REQUIRES_AUTHORIZATION", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_REQUIRES_AUTHORIZATION_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_UNKNOWN_AUTH_FAILURE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_UNKNOWN_AUTH_FAILURE']"
		[Register ("PROXIMITY_UNKNOWN_AUTH_FAILURE")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityUnknownAuthFailure {
			get {
				if (PROXIMITY_UNKNOWN_AUTH_FAILURE_jfieldId == IntPtr.Zero)
					PROXIMITY_UNKNOWN_AUTH_FAILURE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_UNKNOWN_AUTH_FAILURE", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_UNKNOWN_AUTH_FAILURE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_UNKNOWN_TYPE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_UNKNOWN_TYPE']"
		[Register ("PROXIMITY_UNKNOWN_TYPE")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityUnknownType {
			get {
				if (PROXIMITY_UNKNOWN_TYPE_jfieldId == IntPtr.Zero)
					PROXIMITY_UNKNOWN_TYPE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_UNKNOWN_TYPE", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_UNKNOWN_TYPE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_USER_CANCELLED_AUTH_FAILURE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_USER_CANCELLED_AUTH_FAILURE']"
		[Register ("PROXIMITY_USER_CANCELLED_AUTH_FAILURE")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityUserCancelledAuthFailure {
			get {
				if (PROXIMITY_USER_CANCELLED_AUTH_FAILURE_jfieldId == IntPtr.Zero)
					PROXIMITY_USER_CANCELLED_AUTH_FAILURE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_USER_CANCELLED_AUTH_FAILURE", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_USER_CANCELLED_AUTH_FAILURE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_USER_DENIED_AUTH_FAILURE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/field[@name='PROXIMITY_USER_DENIED_AUTH_FAILURE']"
		[Register ("PROXIMITY_USER_DENIED_AUTH_FAILURE")]
		public static global::Com.Gimbal.Proximity.ProximityError ProximityUserDeniedAuthFailure {
			get {
				if (PROXIMITY_USER_DENIED_AUTH_FAILURE_jfieldId == IntPtr.Zero)
					PROXIMITY_USER_DENIED_AUTH_FAILURE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_USER_DENIED_AUTH_FAILURE", "Lcom/gimbal/proximity/ProximityError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_USER_DENIED_AUTH_FAILURE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/ProximityError", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ProximityError); }
		}

		internal ProximityError (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_getCode;
		public unsafe int Code {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/method[@name='getCode' and count(parameter)=0]"
			[Register ("getCode", "()I", "GetGetCodeHandler")]
			get {
				if (id_getCode == IntPtr.Zero)
					id_getCode = JNIEnv.GetMethodID (class_ref, "getCode", "()I");
				try {
					return JNIEnv.CallIntMethod  (Handle, id_getCode);
				} finally {
				}
			}
		}

		static IntPtr id_getMessage;
		public unsafe string Message {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/method[@name='getMessage' and count(parameter)=0]"
			[Register ("getMessage", "()Ljava/lang/String;", "GetGetMessageHandler")]
			get {
				if (id_getMessage == IntPtr.Zero)
					id_getMessage = JNIEnv.GetMethodID (class_ref, "getMessage", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getMessage), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_toString;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/method[@name='toString' and count(parameter)=0]"
		[Register ("toString", "()Ljava/lang/String;", "")]
		public override sealed unsafe string ToString ()
		{
			if (id_toString == IntPtr.Zero)
				id_toString = JNIEnv.GetMethodID (class_ref, "toString", "()Ljava/lang/String;");
			try {
				return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_toString), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static IntPtr id_valueOf_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("valueOf", "(Ljava/lang/String;)Lcom/gimbal/proximity/ProximityError;", "")]
		public static unsafe global::Com.Gimbal.Proximity.ProximityError ValueOf (string p0)
		{
			if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
				id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/gimbal/proximity/ProximityError;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				global::Com.Gimbal.Proximity.ProximityError __ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityError> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_values;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityError']/method[@name='values' and count(parameter)=0]"
		[Register ("values", "()[Lcom/gimbal/proximity/ProximityError;", "")]
		public static unsafe global::Com.Gimbal.Proximity.ProximityError[] Values ()
		{
			if (id_values == IntPtr.Zero)
				id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/gimbal/proximity/ProximityError;");
			try {
				return (global::Com.Gimbal.Proximity.ProximityError[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Gimbal.Proximity.ProximityError));
			} finally {
			}
		}

	}
}
