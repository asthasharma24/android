using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='GeofenceSource']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/GeofenceSource", DoNotGenerateAcw=true)]
	public sealed partial class GeofenceSource : global::Java.Lang.Enum {


		static IntPtr MAPONICS_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='GeofenceSource']/field[@name='MAPONICS']"
		[Register ("MAPONICS")]
		public static global::Com.Gimbal.Protocol.GeofenceSource Maponics {
			get {
				if (MAPONICS_jfieldId == IntPtr.Zero)
					MAPONICS_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "MAPONICS", "Lcom/gimbal/protocol/GeofenceSource;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, MAPONICS_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.GeofenceSource> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr UNKNOWN_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='GeofenceSource']/field[@name='UNKNOWN']"
		[Register ("UNKNOWN")]
		public static global::Com.Gimbal.Protocol.GeofenceSource Unknown {
			get {
				if (UNKNOWN_jfieldId == IntPtr.Zero)
					UNKNOWN_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "UNKNOWN", "Lcom/gimbal/protocol/GeofenceSource;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, UNKNOWN_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.GeofenceSource> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr label_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='GeofenceSource']/field[@name='label']"
		[Register ("label")]
		public string Label {
			get {
				if (label_jfieldId == IntPtr.Zero)
					label_jfieldId = JNIEnv.GetFieldID (class_ref, "label", "Ljava/lang/String;");
				IntPtr __ret = JNIEnv.GetObjectField (Handle, label_jfieldId);
				return JNIEnv.GetString (__ret, JniHandleOwnership.TransferLocalRef);
			}
			set {
				if (label_jfieldId == IntPtr.Zero)
					label_jfieldId = JNIEnv.GetFieldID (class_ref, "label", "Ljava/lang/String;");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JNIEnv.SetField (Handle, label_jfieldId, native_value);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static IntPtr value_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='GeofenceSource']/field[@name='value']"
		[Register ("value")]
		public int Value {
			get {
				if (value_jfieldId == IntPtr.Zero)
					value_jfieldId = JNIEnv.GetFieldID (class_ref, "value", "I");
				return JNIEnv.GetIntField (Handle, value_jfieldId);
			}
			set {
				if (value_jfieldId == IntPtr.Zero)
					value_jfieldId = JNIEnv.GetFieldID (class_ref, "value", "I");
				try {
					JNIEnv.SetField (Handle, value_jfieldId, value);
				} finally {
				}
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/GeofenceSource", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (GeofenceSource); }
		}

		internal GeofenceSource (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_mapSourceName_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='GeofenceSource']/method[@name='mapSourceName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("mapSourceName", "(Ljava/lang/String;)Ljava/lang/String;", "")]
		public static unsafe string MapSourceName (string p0)
		{
			if (id_mapSourceName_Ljava_lang_String_ == IntPtr.Zero)
				id_mapSourceName_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "mapSourceName", "(Ljava/lang/String;)Ljava/lang/String;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				string __ret = JNIEnv.GetString (JNIEnv.CallStaticObjectMethod  (class_ref, id_mapSourceName_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_valueOf_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='GeofenceSource']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("valueOf", "(Ljava/lang/String;)Lcom/gimbal/protocol/GeofenceSource;", "")]
		public static unsafe global::Com.Gimbal.Protocol.GeofenceSource ValueOf (string p0)
		{
			if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
				id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/gimbal/protocol/GeofenceSource;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				global::Com.Gimbal.Protocol.GeofenceSource __ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.GeofenceSource> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_values;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='GeofenceSource']/method[@name='values' and count(parameter)=0]"
		[Register ("values", "()[Lcom/gimbal/protocol/GeofenceSource;", "")]
		public static unsafe global::Com.Gimbal.Protocol.GeofenceSource[] Values ()
		{
			if (id_values == IntPtr.Zero)
				id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/gimbal/protocol/GeofenceSource;");
			try {
				return (global::Com.Gimbal.Protocol.GeofenceSource[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Gimbal.Protocol.GeofenceSource));
			} finally {
			}
		}

	}
}
