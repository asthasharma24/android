using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityServerError']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/ProximityServerError", DoNotGenerateAcw=true)]
	public sealed partial class ProximityServerError : global::Java.Lang.Enum {


		static IntPtr PROXIMITY_BAD_PARAMS_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityServerError']/field[@name='PROXIMITY_BAD_PARAMS']"
		[Register ("PROXIMITY_BAD_PARAMS")]
		public static global::Com.Gimbal.Proximity.ProximityServerError ProximityBadParams {
			get {
				if (PROXIMITY_BAD_PARAMS_jfieldId == IntPtr.Zero)
					PROXIMITY_BAD_PARAMS_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_BAD_PARAMS", "Lcom/gimbal/proximity/ProximityServerError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_BAD_PARAMS_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityServerError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_INVALID_GRANT_AUTH_FAILURE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityServerError']/field[@name='PROXIMITY_INVALID_GRANT_AUTH_FAILURE']"
		[Register ("PROXIMITY_INVALID_GRANT_AUTH_FAILURE")]
		public static global::Com.Gimbal.Proximity.ProximityServerError ProximityInvalidGrantAuthFailure {
			get {
				if (PROXIMITY_INVALID_GRANT_AUTH_FAILURE_jfieldId == IntPtr.Zero)
					PROXIMITY_INVALID_GRANT_AUTH_FAILURE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_INVALID_GRANT_AUTH_FAILURE", "Lcom/gimbal/proximity/ProximityServerError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_INVALID_GRANT_AUTH_FAILURE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityServerError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_TOKEN_DOESNOT_EXIST_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityServerError']/field[@name='PROXIMITY_TOKEN_DOESNOT_EXIST']"
		[Register ("PROXIMITY_TOKEN_DOESNOT_EXIST")]
		public static global::Com.Gimbal.Proximity.ProximityServerError ProximityTokenDoesnotExist {
			get {
				if (PROXIMITY_TOKEN_DOESNOT_EXIST_jfieldId == IntPtr.Zero)
					PROXIMITY_TOKEN_DOESNOT_EXIST_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_TOKEN_DOESNOT_EXIST", "Lcom/gimbal/proximity/ProximityServerError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_TOKEN_DOESNOT_EXIST_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityServerError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_UNAUTHORIZED_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityServerError']/field[@name='PROXIMITY_UNAUTHORIZED']"
		[Register ("PROXIMITY_UNAUTHORIZED")]
		public static global::Com.Gimbal.Proximity.ProximityServerError ProximityUnauthorized {
			get {
				if (PROXIMITY_UNAUTHORIZED_jfieldId == IntPtr.Zero)
					PROXIMITY_UNAUTHORIZED_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_UNAUTHORIZED", "Lcom/gimbal/proximity/ProximityServerError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_UNAUTHORIZED_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityServerError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_UNKNOWN_AUTH_FAILURE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityServerError']/field[@name='PROXIMITY_UNKNOWN_AUTH_FAILURE']"
		[Register ("PROXIMITY_UNKNOWN_AUTH_FAILURE")]
		public static global::Com.Gimbal.Proximity.ProximityServerError ProximityUnknownAuthFailure {
			get {
				if (PROXIMITY_UNKNOWN_AUTH_FAILURE_jfieldId == IntPtr.Zero)
					PROXIMITY_UNKNOWN_AUTH_FAILURE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_UNKNOWN_AUTH_FAILURE", "Lcom/gimbal/proximity/ProximityServerError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_UNKNOWN_AUTH_FAILURE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityServerError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_UNKNOWN_TYPE_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityServerError']/field[@name='PROXIMITY_UNKNOWN_TYPE']"
		[Register ("PROXIMITY_UNKNOWN_TYPE")]
		public static global::Com.Gimbal.Proximity.ProximityServerError ProximityUnknownType {
			get {
				if (PROXIMITY_UNKNOWN_TYPE_jfieldId == IntPtr.Zero)
					PROXIMITY_UNKNOWN_TYPE_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_UNKNOWN_TYPE", "Lcom/gimbal/proximity/ProximityServerError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_UNKNOWN_TYPE_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityServerError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}

		static IntPtr PROXIMITY_UNPROCESSABLE_ENTITY_jfieldId;

		// Metadata.xml XPath field reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityServerError']/field[@name='PROXIMITY_UNPROCESSABLE_ENTITY']"
		[Register ("PROXIMITY_UNPROCESSABLE_ENTITY")]
		public static global::Com.Gimbal.Proximity.ProximityServerError ProximityUnprocessableEntity {
			get {
				if (PROXIMITY_UNPROCESSABLE_ENTITY_jfieldId == IntPtr.Zero)
					PROXIMITY_UNPROCESSABLE_ENTITY_jfieldId = JNIEnv.GetStaticFieldID (class_ref, "PROXIMITY_UNPROCESSABLE_ENTITY", "Lcom/gimbal/proximity/ProximityServerError;");
				IntPtr __ret = JNIEnv.GetStaticObjectField (class_ref, PROXIMITY_UNPROCESSABLE_ENTITY_jfieldId);
				return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityServerError> (__ret, JniHandleOwnership.TransferLocalRef);
			}
		}
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/ProximityServerError", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ProximityServerError); }
		}

		internal ProximityServerError (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_getCode;
		public unsafe int Code {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityServerError']/method[@name='getCode' and count(parameter)=0]"
			[Register ("getCode", "()I", "GetGetCodeHandler")]
			get {
				if (id_getCode == IntPtr.Zero)
					id_getCode = JNIEnv.GetMethodID (class_ref, "getCode", "()I");
				try {
					return JNIEnv.CallIntMethod  (Handle, id_getCode);
				} finally {
				}
			}
		}

		static IntPtr id_getMessage;
		public unsafe string Message {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityServerError']/method[@name='getMessage' and count(parameter)=0]"
			[Register ("getMessage", "()Ljava/lang/String;", "GetGetMessageHandler")]
			get {
				if (id_getMessage == IntPtr.Zero)
					id_getMessage = JNIEnv.GetMethodID (class_ref, "getMessage", "()Ljava/lang/String;");
				try {
					return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getMessage), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static IntPtr id_valueOf_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityServerError']/method[@name='valueOf' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("valueOf", "(Ljava/lang/String;)Lcom/gimbal/proximity/ProximityServerError;", "")]
		public static unsafe global::Com.Gimbal.Proximity.ProximityServerError ValueOf (string p0)
		{
			if (id_valueOf_Ljava_lang_String_ == IntPtr.Zero)
				id_valueOf_Ljava_lang_String_ = JNIEnv.GetStaticMethodID (class_ref, "valueOf", "(Ljava/lang/String;)Lcom/gimbal/proximity/ProximityServerError;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				global::Com.Gimbal.Proximity.ProximityServerError __ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.ProximityServerError> (JNIEnv.CallStaticObjectMethod  (class_ref, id_valueOf_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

		static IntPtr id_values;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/class[@name='ProximityServerError']/method[@name='values' and count(parameter)=0]"
		[Register ("values", "()[Lcom/gimbal/proximity/ProximityServerError;", "")]
		public static unsafe global::Com.Gimbal.Proximity.ProximityServerError[] Values ()
		{
			if (id_values == IntPtr.Zero)
				id_values = JNIEnv.GetStaticMethodID (class_ref, "values", "()[Lcom/gimbal/proximity/ProximityServerError;");
			try {
				return (global::Com.Gimbal.Proximity.ProximityServerError[]) JNIEnv.GetArray (JNIEnv.CallStaticObjectMethod  (class_ref, id_values), JniHandleOwnership.TransferLocalRef, typeof (global::Com.Gimbal.Proximity.ProximityServerError));
			} finally {
			}
		}

	}
}
