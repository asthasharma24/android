using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/Organization", DoNotGenerateAcw=true)]
	public partial class Organization : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/Organization", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (Organization); }
		}

		protected Organization (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']/constructor[@name='Organization' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe Organization ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (Organization)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_isCustomOptIn;
#pragma warning disable 0169
		static Delegate GetIsCustomOptInHandler ()
		{
			if (cb_isCustomOptIn == null)
				cb_isCustomOptIn = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsCustomOptIn);
			return cb_isCustomOptIn;
		}

		static bool n_IsCustomOptIn (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Organization __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Organization> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.CustomOptIn;
		}
#pragma warning restore 0169

		static Delegate cb_setCustomOptIn_Z;
#pragma warning disable 0169
		static Delegate GetSetCustomOptIn_ZHandler ()
		{
			if (cb_setCustomOptIn_Z == null)
				cb_setCustomOptIn_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetCustomOptIn_Z);
			return cb_setCustomOptIn_Z;
		}

		static void n_SetCustomOptIn_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Qsl.Faar.Protocol.Organization __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Organization> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.CustomOptIn = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isCustomOptIn;
		static IntPtr id_setCustomOptIn_Z;
		public virtual unsafe bool CustomOptIn {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']/method[@name='isCustomOptIn' and count(parameter)=0]"
			[Register ("isCustomOptIn", "()Z", "GetIsCustomOptInHandler")]
			get {
				if (id_isCustomOptIn == IntPtr.Zero)
					id_isCustomOptIn = JNIEnv.GetMethodID (class_ref, "isCustomOptIn", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isCustomOptIn);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isCustomOptIn", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']/method[@name='setCustomOptIn' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setCustomOptIn", "(Z)V", "GetSetCustomOptIn_ZHandler")]
			set {
				if (id_setCustomOptIn_Z == IntPtr.Zero)
					id_setCustomOptIn_Z = JNIEnv.GetMethodID (class_ref, "setCustomOptIn", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCustomOptIn_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCustomOptIn", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Organization __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Organization> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_Long_Handler ()
		{
			if (cb_setId_Ljava_lang_Long_ == null)
				cb_setId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_Long_);
			return cb_setId_Ljava_lang_Long_;
		}

		static void n_SetId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Organization __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Organization> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/Long;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setId", "(Ljava/lang/Long;)V", "GetSetId_Ljava_lang_Long_Handler")]
			set {
				if (id_setId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getName;
#pragma warning disable 0169
		static Delegate GetGetNameHandler ()
		{
			if (cb_getName == null)
				cb_getName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetName);
			return cb_getName;
		}

		static IntPtr n_GetName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Organization __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Organization> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Name);
		}
#pragma warning restore 0169

		static Delegate cb_setName_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetName_Ljava_lang_String_Handler ()
		{
			if (cb_setName_Ljava_lang_String_ == null)
				cb_setName_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetName_Ljava_lang_String_);
			return cb_setName_Ljava_lang_String_;
		}

		static void n_SetName_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Organization __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Organization> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Name = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getName;
		static IntPtr id_setName_Ljava_lang_String_;
		public virtual unsafe string Name {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']/method[@name='getName' and count(parameter)=0]"
			[Register ("getName", "()Ljava/lang/String;", "GetGetNameHandler")]
			get {
				if (id_getName == IntPtr.Zero)
					id_getName = JNIEnv.GetMethodID (class_ref, "getName", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getName), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getName", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']/method[@name='setName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setName", "(Ljava/lang/String;)V", "GetSetName_Ljava_lang_String_Handler")]
			set {
				if (id_setName_Ljava_lang_String_ == IntPtr.Zero)
					id_setName_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setName", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setName_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setName", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getOrganizationApplications;
#pragma warning disable 0169
		static Delegate GetGetOrganizationApplicationsHandler ()
		{
			if (cb_getOrganizationApplications == null)
				cb_getOrganizationApplications = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationApplications);
			return cb_getOrganizationApplications;
		}

		static IntPtr n_GetOrganizationApplications (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Organization __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Organization> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Application>.ToLocalJniHandle (__this.OrganizationApplications);
		}
#pragma warning restore 0169

		static Delegate cb_setOrganizationApplications_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationApplications_Ljava_util_List_Handler ()
		{
			if (cb_setOrganizationApplications_Ljava_util_List_ == null)
				cb_setOrganizationApplications_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationApplications_Ljava_util_List_);
			return cb_setOrganizationApplications_Ljava_util_List_;
		}

		static void n_SetOrganizationApplications_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Organization __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Organization> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Application>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OrganizationApplications = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationApplications;
		static IntPtr id_setOrganizationApplications_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.Application> OrganizationApplications {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']/method[@name='getOrganizationApplications' and count(parameter)=0]"
			[Register ("getOrganizationApplications", "()Ljava/util/List;", "GetGetOrganizationApplicationsHandler")]
			get {
				if (id_getOrganizationApplications == IntPtr.Zero)
					id_getOrganizationApplications = JNIEnv.GetMethodID (class_ref, "getOrganizationApplications", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Application>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationApplications), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Application>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationApplications", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']/method[@name='setOrganizationApplications' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.qsl.faar.protocol.Application&gt;']]"
			[Register ("setOrganizationApplications", "(Ljava/util/List;)V", "GetSetOrganizationApplications_Ljava_util_List_Handler")]
			set {
				if (id_setOrganizationApplications_Ljava_util_List_ == IntPtr.Zero)
					id_setOrganizationApplications_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setOrganizationApplications", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Application>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOrganizationApplications_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationApplications", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_isValid;
#pragma warning disable 0169
		static Delegate GetIsValidHandler ()
		{
			if (cb_isValid == null)
				cb_isValid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsValid);
			return cb_isValid;
		}

		static bool n_IsValid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Organization __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Organization> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Valid;
		}
#pragma warning restore 0169

		static Delegate cb_setValid_Z;
#pragma warning disable 0169
		static Delegate GetSetValid_ZHandler ()
		{
			if (cb_setValid_Z == null)
				cb_setValid_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetValid_Z);
			return cb_setValid_Z;
		}

		static void n_SetValid_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Qsl.Faar.Protocol.Organization __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Organization> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Valid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isValid;
		static IntPtr id_setValid_Z;
		public virtual unsafe bool Valid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']/method[@name='isValid' and count(parameter)=0]"
			[Register ("isValid", "()Z", "GetIsValidHandler")]
			get {
				if (id_isValid == IntPtr.Zero)
					id_isValid = JNIEnv.GetMethodID (class_ref, "isValid", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isValid);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isValid", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']/method[@name='setValid' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setValid", "(Z)V", "GetSetValid_ZHandler")]
			set {
				if (id_setValid_Z == IntPtr.Zero)
					id_setValid_Z = JNIEnv.GetMethodID (class_ref, "setValid", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setValid_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setValid", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_addOrganizationApplication_Lcom_qsl_faar_protocol_Application_;
#pragma warning disable 0169
		static Delegate GetAddOrganizationApplication_Lcom_qsl_faar_protocol_Application_Handler ()
		{
			if (cb_addOrganizationApplication_Lcom_qsl_faar_protocol_Application_ == null)
				cb_addOrganizationApplication_Lcom_qsl_faar_protocol_Application_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddOrganizationApplication_Lcom_qsl_faar_protocol_Application_);
			return cb_addOrganizationApplication_Lcom_qsl_faar_protocol_Application_;
		}

		static void n_AddOrganizationApplication_Lcom_qsl_faar_protocol_Application_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Organization __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Organization> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.Application p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Application> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddOrganizationApplication (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addOrganizationApplication_Lcom_qsl_faar_protocol_Application_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='Organization']/method[@name='addOrganizationApplication' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.Application']]"
		[Register ("addOrganizationApplication", "(Lcom/qsl/faar/protocol/Application;)V", "GetAddOrganizationApplication_Lcom_qsl_faar_protocol_Application_Handler")]
		public virtual unsafe void AddOrganizationApplication (global::Com.Qsl.Faar.Protocol.Application p0)
		{
			if (id_addOrganizationApplication_Lcom_qsl_faar_protocol_Application_ == IntPtr.Zero)
				id_addOrganizationApplication_Lcom_qsl_faar_protocol_Application_ = JNIEnv.GetMethodID (class_ref, "addOrganizationApplication", "(Lcom/qsl/faar/protocol/Application;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addOrganizationApplication_Lcom_qsl_faar_protocol_Application_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addOrganizationApplication", "(Lcom/qsl/faar/protocol/Application;)V"), __args);
			} finally {
			}
		}

	}
}
