using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Android {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconEventListener']"
	[global::Android.Runtime.Register ("com/gimbal/android/BeaconEventListener", DoNotGenerateAcw=true)]
	public abstract partial class BeaconEventListener : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/android/BeaconEventListener", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (BeaconEventListener); }
		}

		protected BeaconEventListener (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconEventListener']/constructor[@name='BeaconEventListener' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe BeaconEventListener ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (BeaconEventListener)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_;
#pragma warning disable 0169
		static Delegate GetOnBeaconSighting_Lcom_gimbal_android_BeaconSighting_Handler ()
		{
			if (cb_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_ == null)
				cb_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_OnBeaconSighting_Lcom_gimbal_android_BeaconSighting_);
			return cb_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_;
		}

		static void n_OnBeaconSighting_Lcom_gimbal_android_BeaconSighting_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Android.BeaconEventListener __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.BeaconEventListener> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Android.BeaconSighting p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Android.BeaconSighting> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OnBeaconSighting (p0);
		}
#pragma warning restore 0169

		static IntPtr id_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.android']/class[@name='BeaconEventListener']/method[@name='onBeaconSighting' and count(parameter)=1 and parameter[1][@type='com.gimbal.android.BeaconSighting']]"
		[Register ("onBeaconSighting", "(Lcom/gimbal/android/BeaconSighting;)V", "GetOnBeaconSighting_Lcom_gimbal_android_BeaconSighting_Handler")]
		public virtual unsafe void OnBeaconSighting (global::Com.Gimbal.Android.BeaconSighting p0)
		{
			if (id_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_ == IntPtr.Zero)
				id_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_ = JNIEnv.GetMethodID (class_ref, "onBeaconSighting", "(Lcom/gimbal/android/BeaconSighting;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_onBeaconSighting_Lcom_gimbal_android_BeaconSighting_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "onBeaconSighting", "(Lcom/gimbal/android/BeaconSighting;)V"), __args);
			} finally {
			}
		}

	}

	[global::Android.Runtime.Register ("com/gimbal/android/BeaconEventListener", DoNotGenerateAcw=true)]
	internal partial class BeaconEventListenerInvoker : BeaconEventListener {

		public BeaconEventListenerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (BeaconEventListenerInvoker); }
		}

	}

}
