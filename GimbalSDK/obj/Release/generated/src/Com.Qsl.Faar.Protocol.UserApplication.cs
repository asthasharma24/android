using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/UserApplication", DoNotGenerateAcw=true)]
	public partial class UserApplication : global::Java.Lang.Object, global::Java.IO.ISerializable {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/UserApplication", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (UserApplication); }
		}

		protected UserApplication (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_Lcom_qsl_faar_protocol_UserApplication_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/constructor[@name='UserApplication' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.UserApplication']]"
		[Register (".ctor", "(Lcom/qsl/faar/protocol/UserApplication;)V", "")]
		public unsafe UserApplication (global::Com.Qsl.Faar.Protocol.UserApplication p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);
				if (GetType () != typeof (UserApplication)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "(Lcom/qsl/faar/protocol/UserApplication;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "(Lcom/qsl/faar/protocol/UserApplication;)V", __args);
					return;
				}

				if (id_ctor_Lcom_qsl_faar_protocol_UserApplication_ == IntPtr.Zero)
					id_ctor_Lcom_qsl_faar_protocol_UserApplication_ = JNIEnv.GetMethodID (class_ref, "<init>", "(Lcom/qsl/faar/protocol/UserApplication;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_Lcom_qsl_faar_protocol_UserApplication_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_Lcom_qsl_faar_protocol_UserApplication_, __args);
			} finally {
			}
		}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/constructor[@name='UserApplication' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe UserApplication ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (UserApplication)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getApplicationId;
#pragma warning disable 0169
		static Delegate GetGetApplicationIdHandler ()
		{
			if (cb_getApplicationId == null)
				cb_getApplicationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApplicationId);
			return cb_getApplicationId;
		}

		static IntPtr n_GetApplicationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.ApplicationId);
		}
#pragma warning restore 0169

		static Delegate cb_setApplicationId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetApplicationId_Ljava_lang_Long_Handler ()
		{
			if (cb_setApplicationId_Ljava_lang_Long_ == null)
				cb_setApplicationId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApplicationId_Ljava_lang_Long_);
			return cb_setApplicationId_Ljava_lang_Long_;
		}

		static void n_SetApplicationId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApplicationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApplicationId;
		static IntPtr id_setApplicationId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long ApplicationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='getApplicationId' and count(parameter)=0]"
			[Register ("getApplicationId", "()Ljava/lang/Long;", "GetGetApplicationIdHandler")]
			get {
				if (id_getApplicationId == IntPtr.Zero)
					id_getApplicationId = JNIEnv.GetMethodID (class_ref, "getApplicationId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getApplicationId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApplicationId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='setApplicationId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setApplicationId", "(Ljava/lang/Long;)V", "GetSetApplicationId_Ljava_lang_Long_Handler")]
			set {
				if (id_setApplicationId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setApplicationId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setApplicationId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApplicationId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApplicationId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_isLocationPermission;
#pragma warning disable 0169
		static Delegate GetIsLocationPermissionHandler ()
		{
			if (cb_isLocationPermission == null)
				cb_isLocationPermission = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsLocationPermission);
			return cb_isLocationPermission;
		}

		static bool n_IsLocationPermission (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.LocationPermission;
		}
#pragma warning restore 0169

		static Delegate cb_setLocationPermission_Z;
#pragma warning disable 0169
		static Delegate GetSetLocationPermission_ZHandler ()
		{
			if (cb_setLocationPermission_Z == null)
				cb_setLocationPermission_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetLocationPermission_Z);
			return cb_setLocationPermission_Z;
		}

		static void n_SetLocationPermission_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.LocationPermission = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isLocationPermission;
		static IntPtr id_setLocationPermission_Z;
		[Obsolete (@"deprecated")]
		public virtual unsafe bool LocationPermission {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='isLocationPermission' and count(parameter)=0]"
			[Register ("isLocationPermission", "()Z", "GetIsLocationPermissionHandler")]
			get {
				if (id_isLocationPermission == IntPtr.Zero)
					id_isLocationPermission = JNIEnv.GetMethodID (class_ref, "isLocationPermission", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isLocationPermission);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isLocationPermission", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='setLocationPermission' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setLocationPermission", "(Z)V", "GetSetLocationPermission_ZHandler")]
			set {
				if (id_setLocationPermission_Z == IntPtr.Zero)
					id_setLocationPermission_Z = JNIEnv.GetMethodID (class_ref, "setLocationPermission", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLocationPermission_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLocationPermission", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getOrganizationId;
#pragma warning disable 0169
		static Delegate GetGetOrganizationIdHandler ()
		{
			if (cb_getOrganizationId == null)
				cb_getOrganizationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationId);
			return cb_getOrganizationId;
		}

		static IntPtr n_GetOrganizationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OrganizationId);
		}
#pragma warning restore 0169

		static Delegate cb_setOrganizationId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationId_Ljava_lang_Long_Handler ()
		{
			if (cb_setOrganizationId_Ljava_lang_Long_ == null)
				cb_setOrganizationId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationId_Ljava_lang_Long_);
			return cb_setOrganizationId_Ljava_lang_Long_;
		}

		static void n_SetOrganizationId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OrganizationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationId;
		static IntPtr id_setOrganizationId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long OrganizationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='getOrganizationId' and count(parameter)=0]"
			[Register ("getOrganizationId", "()Ljava/lang/Long;", "GetGetOrganizationIdHandler")]
			get {
				if (id_getOrganizationId == IntPtr.Zero)
					id_getOrganizationId = JNIEnv.GetMethodID (class_ref, "getOrganizationId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='setOrganizationId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setOrganizationId", "(Ljava/lang/Long;)V", "GetSetOrganizationId_Ljava_lang_Long_Handler")]
			set {
				if (id_setOrganizationId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setOrganizationId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setOrganizationId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOrganizationId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_isProfilePermission;
#pragma warning disable 0169
		static Delegate GetIsProfilePermissionHandler ()
		{
			if (cb_isProfilePermission == null)
				cb_isProfilePermission = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsProfilePermission);
			return cb_isProfilePermission;
		}

		static bool n_IsProfilePermission (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ProfilePermission;
		}
#pragma warning restore 0169

		static Delegate cb_setProfilePermission_Z;
#pragma warning disable 0169
		static Delegate GetSetProfilePermission_ZHandler ()
		{
			if (cb_setProfilePermission_Z == null)
				cb_setProfilePermission_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetProfilePermission_Z);
			return cb_setProfilePermission_Z;
		}

		static void n_SetProfilePermission_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ProfilePermission = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isProfilePermission;
		static IntPtr id_setProfilePermission_Z;
		[Obsolete (@"deprecated")]
		public virtual unsafe bool ProfilePermission {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='isProfilePermission' and count(parameter)=0]"
			[Register ("isProfilePermission", "()Z", "GetIsProfilePermissionHandler")]
			get {
				if (id_isProfilePermission == IntPtr.Zero)
					id_isProfilePermission = JNIEnv.GetMethodID (class_ref, "isProfilePermission", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isProfilePermission);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isProfilePermission", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='setProfilePermission' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setProfilePermission", "(Z)V", "GetSetProfilePermission_ZHandler")]
			set {
				if (id_setProfilePermission_Z == IntPtr.Zero)
					id_setProfilePermission_Z = JNIEnv.GetMethodID (class_ref, "setProfilePermission", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setProfilePermission_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setProfilePermission", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getReceiverUid;
#pragma warning disable 0169
		static Delegate GetGetReceiverUidHandler ()
		{
			if (cb_getReceiverUid == null)
				cb_getReceiverUid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetReceiverUid);
			return cb_getReceiverUid;
		}

		static IntPtr n_GetReceiverUid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ReceiverUid);
		}
#pragma warning restore 0169

		static Delegate cb_setReceiverUid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetReceiverUid_Ljava_lang_String_Handler ()
		{
			if (cb_setReceiverUid_Ljava_lang_String_ == null)
				cb_setReceiverUid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetReceiverUid_Ljava_lang_String_);
			return cb_setReceiverUid_Ljava_lang_String_;
		}

		static void n_SetReceiverUid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ReceiverUid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getReceiverUid;
		static IntPtr id_setReceiverUid_Ljava_lang_String_;
		public virtual unsafe string ReceiverUid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='getReceiverUid' and count(parameter)=0]"
			[Register ("getReceiverUid", "()Ljava/lang/String;", "GetGetReceiverUidHandler")]
			get {
				if (id_getReceiverUid == IntPtr.Zero)
					id_getReceiverUid = JNIEnv.GetMethodID (class_ref, "getReceiverUid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getReceiverUid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getReceiverUid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='setReceiverUid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setReceiverUid", "(Ljava/lang/String;)V", "GetSetReceiverUid_Ljava_lang_String_Handler")]
			set {
				if (id_setReceiverUid_Ljava_lang_String_ == IntPtr.Zero)
					id_setReceiverUid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setReceiverUid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setReceiverUid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setReceiverUid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_isSubscriptionPermission;
#pragma warning disable 0169
		static Delegate GetIsSubscriptionPermissionHandler ()
		{
			if (cb_isSubscriptionPermission == null)
				cb_isSubscriptionPermission = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsSubscriptionPermission);
			return cb_isSubscriptionPermission;
		}

		static bool n_IsSubscriptionPermission (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.SubscriptionPermission;
		}
#pragma warning restore 0169

		static Delegate cb_setSubscriptionPermission_Z;
#pragma warning disable 0169
		static Delegate GetSetSubscriptionPermission_ZHandler ()
		{
			if (cb_setSubscriptionPermission_Z == null)
				cb_setSubscriptionPermission_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetSubscriptionPermission_Z);
			return cb_setSubscriptionPermission_Z;
		}

		static void n_SetSubscriptionPermission_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.SubscriptionPermission = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isSubscriptionPermission;
		static IntPtr id_setSubscriptionPermission_Z;
		public virtual unsafe bool SubscriptionPermission {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='isSubscriptionPermission' and count(parameter)=0]"
			[Register ("isSubscriptionPermission", "()Z", "GetIsSubscriptionPermissionHandler")]
			get {
				if (id_isSubscriptionPermission == IntPtr.Zero)
					id_isSubscriptionPermission = JNIEnv.GetMethodID (class_ref, "isSubscriptionPermission", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isSubscriptionPermission);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isSubscriptionPermission", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='setSubscriptionPermission' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setSubscriptionPermission", "(Z)V", "GetSetSubscriptionPermission_ZHandler")]
			set {
				if (id_setSubscriptionPermission_Z == IntPtr.Zero)
					id_setSubscriptionPermission_Z = JNIEnv.GetMethodID (class_ref, "setSubscriptionPermission", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSubscriptionPermission_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSubscriptionPermission", "(Z)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getUserId;
#pragma warning disable 0169
		static Delegate GetGetUserIdHandler ()
		{
			if (cb_getUserId == null)
				cb_getUserId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUserId);
			return cb_getUserId;
		}

		static IntPtr n_GetUserId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.UserId);
		}
#pragma warning restore 0169

		static Delegate cb_setUserId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetUserId_Ljava_lang_Long_Handler ()
		{
			if (cb_setUserId_Ljava_lang_Long_ == null)
				cb_setUserId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUserId_Ljava_lang_Long_);
			return cb_setUserId_Ljava_lang_Long_;
		}

		static void n_SetUserId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.UserId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUserId;
		static IntPtr id_setUserId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long UserId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='getUserId' and count(parameter)=0]"
			[Register ("getUserId", "()Ljava/lang/Long;", "GetGetUserIdHandler")]
			get {
				if (id_getUserId == IntPtr.Zero)
					id_getUserId = JNIEnv.GetMethodID (class_ref, "getUserId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getUserId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUserId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='setUserId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setUserId", "(Ljava/lang/Long;)V", "GetSetUserId_Ljava_lang_Long_Handler")]
			set {
				if (id_setUserId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setUserId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setUserId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUserId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUserId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_isValid;
#pragma warning disable 0169
		static Delegate GetIsValidHandler ()
		{
			if (cb_isValid == null)
				cb_isValid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, bool>) n_IsValid);
			return cb_isValid;
		}

		static bool n_IsValid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.Valid;
		}
#pragma warning restore 0169

		static Delegate cb_setValid_Z;
#pragma warning disable 0169
		static Delegate GetSetValid_ZHandler ()
		{
			if (cb_setValid_Z == null)
				cb_setValid_Z = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, bool>) n_SetValid_Z);
			return cb_setValid_Z;
		}

		static void n_SetValid_Z (IntPtr jnienv, IntPtr native__this, bool p0)
		{
			global::Com.Qsl.Faar.Protocol.UserApplication __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.UserApplication> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Valid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_isValid;
		static IntPtr id_setValid_Z;
		public virtual unsafe bool Valid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='isValid' and count(parameter)=0]"
			[Register ("isValid", "()Z", "GetIsValidHandler")]
			get {
				if (id_isValid == IntPtr.Zero)
					id_isValid = JNIEnv.GetMethodID (class_ref, "isValid", "()Z");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallBooleanMethod  (Handle, id_isValid);
					else
						return JNIEnv.CallNonvirtualBooleanMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "isValid", "()Z"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='UserApplication']/method[@name='setValid' and count(parameter)=1 and parameter[1][@type='boolean']]"
			[Register ("setValid", "(Z)V", "GetSetValid_ZHandler")]
			set {
				if (id_setValid_Z == IntPtr.Zero)
					id_setValid_Z = JNIEnv.GetMethodID (class_ref, "setValid", "(Z)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setValid_Z, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setValid", "(Z)V"), __args);
				} finally {
				}
			}
		}

	}
}
