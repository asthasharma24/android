using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Service.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ServerErrorResponse']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/service/protocol/ServerErrorResponse", DoNotGenerateAcw=true)]
	public partial class ServerErrorResponse : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/service/protocol/ServerErrorResponse", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ServerErrorResponse); }
		}

		protected ServerErrorResponse (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ServerErrorResponse']/constructor[@name='ServerErrorResponse' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ServerErrorResponse ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ServerErrorResponse)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getError;
#pragma warning disable 0169
		static Delegate GetGetErrorHandler ()
		{
			if (cb_getError == null)
				cb_getError = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetError);
			return cb_getError;
		}

		static IntPtr n_GetError (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerErrorResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerErrorResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Error);
		}
#pragma warning restore 0169

		static Delegate cb_setError_Lcom_gimbal_proximity_core_service_protocol_ServerError_;
#pragma warning disable 0169
		static Delegate GetSetError_Lcom_gimbal_proximity_core_service_protocol_ServerError_Handler ()
		{
			if (cb_setError_Lcom_gimbal_proximity_core_service_protocol_ServerError_ == null)
				cb_setError_Lcom_gimbal_proximity_core_service_protocol_ServerError_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetError_Lcom_gimbal_proximity_core_service_protocol_ServerError_);
			return cb_setError_Lcom_gimbal_proximity_core_service_protocol_ServerError_;
		}

		static void n_SetError_Lcom_gimbal_proximity_core_service_protocol_ServerError_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerErrorResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerErrorResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError p0 = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Error = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getError;
		static IntPtr id_setError_Lcom_gimbal_proximity_core_service_protocol_ServerError_;
		public virtual unsafe global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError Error {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ServerErrorResponse']/method[@name='getError' and count(parameter)=0]"
			[Register ("getError", "()Lcom/gimbal/proximity/core/service/protocol/ServerError;", "GetGetErrorHandler")]
			get {
				if (id_getError == IntPtr.Zero)
					id_getError = JNIEnv.GetMethodID (class_ref, "getError", "()Lcom/gimbal/proximity/core/service/protocol/ServerError;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError> (JNIEnv.CallObjectMethod  (Handle, id_getError), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getError", "()Lcom/gimbal/proximity/core/service/protocol/ServerError;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='ServerErrorResponse']/method[@name='setError' and count(parameter)=1 and parameter[1][@type='com.gimbal.proximity.core.service.protocol.ServerError']]"
			[Register ("setError", "(Lcom/gimbal/proximity/core/service/protocol/ServerError;)V", "GetSetError_Lcom_gimbal_proximity_core_service_protocol_ServerError_Handler")]
			set {
				if (id_setError_Lcom_gimbal_proximity_core_service_protocol_ServerError_ == IntPtr.Zero)
					id_setError_Lcom_gimbal_proximity_core_service_protocol_ServerError_ = JNIEnv.GetMethodID (class_ref, "setError", "(Lcom/gimbal/proximity/core/service/protocol/ServerError;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setError_Lcom_gimbal_proximity_core_service_protocol_ServerError_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setError", "(Lcom/gimbal/proximity/core/service/protocol/ServerError;)V"), __args);
				} finally {
				}
			}
		}

	}
}
