using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Json {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='ClassHandler']"
	[global::Android.Runtime.Register ("com/gimbal/internal/json/ClassHandler", DoNotGenerateAcw=true)]
	public partial class ClassHandler : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/json/ClassHandler", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ClassHandler); }
		}

		protected ClassHandler (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_arrayLjava_lang_Class_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='ClassHandler']/constructor[@name='ClassHandler' and count(parameter)=1 and parameter[1][@type='java.lang.Class&lt;?&gt;...']]"
		[Register (".ctor", "([Ljava/lang/Class;)V", "")]
		public unsafe ClassHandler (params global:: Java.Lang.Class[] p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			IntPtr native_p0 = JNIEnv.NewArray (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				if (GetType () != typeof (ClassHandler)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "([Ljava/lang/Class;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "([Ljava/lang/Class;)V", __args);
					return;
				}

				if (id_ctor_arrayLjava_lang_Class_ == IntPtr.Zero)
					id_ctor_arrayLjava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "<init>", "([Ljava/lang/Class;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_arrayLjava_lang_Class_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_arrayLjava_lang_Class_, __args);
			} finally {
				if (p0 != null) {
					JNIEnv.CopyArray (native_p0, p0);
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}
		}

		static Delegate cb_getTargetClass;
#pragma warning disable 0169
		static Delegate GetGetTargetClassHandler ()
		{
			if (cb_getTargetClass == null)
				cb_getTargetClass = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTargetClass);
			return cb_getTargetClass;
		}

		static IntPtr n_GetTargetClass (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Json.ClassHandler __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.ClassHandler> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetTargetClass ());
		}
#pragma warning restore 0169

		static IntPtr id_getTargetClass;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='ClassHandler']/method[@name='getTargetClass' and count(parameter)=0]"
		[Register ("getTargetClass", "()[Ljava/lang/Class;", "GetGetTargetClassHandler")]
		public virtual unsafe global::Java.Lang.Class[] GetTargetClass ()
		{
			if (id_getTargetClass == IntPtr.Zero)
				id_getTargetClass = JNIEnv.GetMethodID (class_ref, "getTargetClass", "()[Ljava/lang/Class;");
			try {

				if (GetType () == ThresholdType)
					return (global::Java.Lang.Class[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod  (Handle, id_getTargetClass), JniHandleOwnership.TransferLocalRef, typeof (global::Java.Lang.Class));
				else
					return (global::Java.Lang.Class[]) JNIEnv.GetArray (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTargetClass", "()[Ljava/lang/Class;")), JniHandleOwnership.TransferLocalRef, typeof (global::Java.Lang.Class));
			} finally {
			}
		}

	}
}
