using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Content {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='PushEventContent']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/content/PushEventContent", DoNotGenerateAcw=true)]
	public partial class PushEventContent : global::Java.Lang.Object, global::Com.Qsl.Faar.Protocol.Content.IEventContent {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/content/PushEventContent", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PushEventContent); }
		}

		protected PushEventContent (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='PushEventContent']/constructor[@name='PushEventContent' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe PushEventContent ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (PushEventContent)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getContentDescriptors;
#pragma warning disable 0169
		static Delegate GetGetContentDescriptorsHandler ()
		{
			if (cb_getContentDescriptors == null)
				cb_getContentDescriptors = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetContentDescriptors);
			return cb_getContentDescriptors;
		}

		static IntPtr n_GetContentDescriptors (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.PushEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.PushEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.ToLocalJniHandle (__this.ContentDescriptors);
		}
#pragma warning restore 0169

		static Delegate cb_setContentDescriptors_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetContentDescriptors_Ljava_util_List_Handler ()
		{
			if (cb_setContentDescriptors_Ljava_util_List_ == null)
				cb_setContentDescriptors_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetContentDescriptors_Ljava_util_List_);
			return cb_setContentDescriptors_Ljava_util_List_;
		}

		static void n_SetContentDescriptors_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.PushEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.PushEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ContentDescriptors = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getContentDescriptors;
		static IntPtr id_setContentDescriptors_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> ContentDescriptors {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='PushEventContent']/method[@name='getContentDescriptors' and count(parameter)=0]"
			[Register ("getContentDescriptors", "()Ljava/util/List;", "GetGetContentDescriptorsHandler")]
			get {
				if (id_getContentDescriptors == IntPtr.Zero)
					id_getContentDescriptors = JNIEnv.GetMethodID (class_ref, "getContentDescriptors", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getContentDescriptors), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getContentDescriptors", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='PushEventContent']/method[@name='setContentDescriptors' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.qsl.faar.protocol.content.ContentDescriptor&gt;']]"
			[Register ("setContentDescriptors", "(Ljava/util/List;)V", "GetSetContentDescriptors_Ljava_util_List_Handler")]
			set {
				if (id_setContentDescriptors_Ljava_util_List_ == IntPtr.Zero)
					id_setContentDescriptors_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setContentDescriptors", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setContentDescriptors_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setContentDescriptors", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getEventTime;
#pragma warning disable 0169
		static Delegate GetGetEventTimeHandler ()
		{
			if (cb_getEventTime == null)
				cb_getEventTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEventTime);
			return cb_getEventTime;
		}

		static IntPtr n_GetEventTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.PushEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.PushEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.EventTime);
		}
#pragma warning restore 0169

		static IntPtr id_getEventTime;
		public virtual unsafe global::Java.Lang.Long EventTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='PushEventContent']/method[@name='getEventTime' and count(parameter)=0]"
			[Register ("getEventTime", "()Ljava/lang/Long;", "GetGetEventTimeHandler")]
			get {
				if (id_getEventTime == IntPtr.Zero)
					id_getEventTime = JNIEnv.GetMethodID (class_ref, "getEventTime", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getEventTime), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEventTime", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getOrganizationId;
#pragma warning disable 0169
		static Delegate GetGetOrganizationIdHandler ()
		{
			if (cb_getOrganizationId == null)
				cb_getOrganizationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationId);
			return cb_getOrganizationId;
		}

		static IntPtr n_GetOrganizationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.PushEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.PushEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.OrganizationId);
		}
#pragma warning restore 0169

		static Delegate cb_setOrganizationId_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationId_Ljava_lang_Long_Handler ()
		{
			if (cb_setOrganizationId_Ljava_lang_Long_ == null)
				cb_setOrganizationId_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationId_Ljava_lang_Long_);
			return cb_setOrganizationId_Ljava_lang_Long_;
		}

		static void n_SetOrganizationId_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.PushEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.PushEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OrganizationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationId;
		static IntPtr id_setOrganizationId_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long OrganizationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='PushEventContent']/method[@name='getOrganizationId' and count(parameter)=0]"
			[Register ("getOrganizationId", "()Ljava/lang/Long;", "GetGetOrganizationIdHandler")]
			get {
				if (id_getOrganizationId == IntPtr.Zero)
					id_getOrganizationId = JNIEnv.GetMethodID (class_ref, "getOrganizationId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='PushEventContent']/method[@name='setOrganizationId' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setOrganizationId", "(Ljava/lang/Long;)V", "GetSetOrganizationId_Ljava_lang_Long_Handler")]
			set {
				if (id_setOrganizationId_Ljava_lang_Long_ == IntPtr.Zero)
					id_setOrganizationId_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setOrganizationId", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOrganizationId_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationId", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPlaceId;
#pragma warning disable 0169
		static Delegate GetGetPlaceIdHandler ()
		{
			if (cb_getPlaceId == null)
				cb_getPlaceId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPlaceId);
			return cb_getPlaceId;
		}

		static IntPtr n_GetPlaceId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.PushEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.PushEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.PlaceId);
		}
#pragma warning restore 0169

		static IntPtr id_getPlaceId;
		public virtual unsafe global::Java.Lang.Long PlaceId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='PushEventContent']/method[@name='getPlaceId' and count(parameter)=0]"
			[Register ("getPlaceId", "()Ljava/lang/Long;", "GetGetPlaceIdHandler")]
			get {
				if (id_getPlaceId == IntPtr.Zero)
					id_getPlaceId = JNIEnv.GetMethodID (class_ref, "getPlaceId", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getPlaceId), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPlaceId", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getPushEvent;
#pragma warning disable 0169
		static Delegate GetGetPushEventHandler ()
		{
			if (cb_getPushEvent == null)
				cb_getPushEvent = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPushEvent);
			return cb_getPushEvent;
		}

		static IntPtr n_GetPushEvent (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.PushEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.PushEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.PushEvent);
		}
#pragma warning restore 0169

		static Delegate cb_setPushEvent_Lcom_qsl_faar_protocol_PushEvent_;
#pragma warning disable 0169
		static Delegate GetSetPushEvent_Lcom_qsl_faar_protocol_PushEvent_Handler ()
		{
			if (cb_setPushEvent_Lcom_qsl_faar_protocol_PushEvent_ == null)
				cb_setPushEvent_Lcom_qsl_faar_protocol_PushEvent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPushEvent_Lcom_qsl_faar_protocol_PushEvent_);
			return cb_setPushEvent_Lcom_qsl_faar_protocol_PushEvent_;
		}

		static void n_SetPushEvent_Lcom_qsl_faar_protocol_PushEvent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.PushEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.PushEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.PushEvent p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.PushEvent = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPushEvent;
		static IntPtr id_setPushEvent_Lcom_qsl_faar_protocol_PushEvent_;
		public virtual unsafe global::Com.Qsl.Faar.Protocol.PushEvent PushEvent {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='PushEventContent']/method[@name='getPushEvent' and count(parameter)=0]"
			[Register ("getPushEvent", "()Lcom/qsl/faar/protocol/PushEvent;", "GetGetPushEventHandler")]
			get {
				if (id_getPushEvent == IntPtr.Zero)
					id_getPushEvent = JNIEnv.GetMethodID (class_ref, "getPushEvent", "()Lcom/qsl/faar/protocol/PushEvent;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (JNIEnv.CallObjectMethod  (Handle, id_getPushEvent), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPushEvent", "()Lcom/qsl/faar/protocol/PushEvent;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='PushEventContent']/method[@name='setPushEvent' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.PushEvent']]"
			[Register ("setPushEvent", "(Lcom/qsl/faar/protocol/PushEvent;)V", "GetSetPushEvent_Lcom_qsl_faar_protocol_PushEvent_Handler")]
			set {
				if (id_setPushEvent_Lcom_qsl_faar_protocol_PushEvent_ == IntPtr.Zero)
					id_setPushEvent_Lcom_qsl_faar_protocol_PushEvent_ = JNIEnv.GetMethodID (class_ref, "setPushEvent", "(Lcom/qsl/faar/protocol/PushEvent;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPushEvent_Lcom_qsl_faar_protocol_PushEvent_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPushEvent", "(Lcom/qsl/faar/protocol/PushEvent;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getType;
#pragma warning disable 0169
		static Delegate GetGetTypeHandler ()
		{
			if (cb_getType == null)
				cb_getType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetType);
			return cb_getType;
		}

		static IntPtr n_GetType (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.PushEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.PushEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Type);
		}
#pragma warning restore 0169

		static IntPtr id_getType;
		public virtual unsafe string Type {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='PushEventContent']/method[@name='getType' and count(parameter)=0]"
			[Register ("getType", "()Ljava/lang/String;", "GetGetTypeHandler")]
			get {
				if (id_getType == IntPtr.Zero)
					id_getType = JNIEnv.GetMethodID (class_ref, "getType", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getType), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getType", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_addContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_;
#pragma warning disable 0169
		static Delegate GetAddContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_Handler ()
		{
			if (cb_addContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_ == null)
				cb_addContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_);
			return cb_addContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_;
		}

		static void n_AddContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.PushEventContent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.PushEventContent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddContentDescriptor (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='PushEventContent']/method[@name='addContentDescriptor' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.content.ContentDescriptor']]"
		[Register ("addContentDescriptor", "(Lcom/qsl/faar/protocol/content/ContentDescriptor;)V", "GetAddContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_Handler")]
		public virtual unsafe void AddContentDescriptor (global::Com.Qsl.Faar.Protocol.Content.ContentDescriptor p0)
		{
			if (id_addContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_ == IntPtr.Zero)
				id_addContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_ = JNIEnv.GetMethodID (class_ref, "addContentDescriptor", "(Lcom/qsl/faar/protocol/content/ContentDescriptor;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addContentDescriptor_Lcom_qsl_faar_protocol_content_ContentDescriptor_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addContentDescriptor", "(Lcom/qsl/faar/protocol/content/ContentDescriptor;)V"), __args);
			} finally {
			}
		}

	}
}
