using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity {

	// Metadata.xml XPath interface reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='VisitManager']"
	[Register ("com/gimbal/proximity/VisitManager", "", "Com.Gimbal.Proximity.IVisitManagerInvoker")]
	public partial interface IVisitManager : IJavaObject {

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='VisitManager']/method[@name='setVisitListener' and count(parameter)=1 and parameter[1][@type='com.gimbal.proximity.VisitListener']]"
		[Register ("setVisitListener", "(Lcom/gimbal/proximity/VisitListener;)V", "GetSetVisitListener_Lcom_gimbal_proximity_VisitListener_Handler:Com.Gimbal.Proximity.IVisitManagerInvoker, GimbalSDK")]
		void SetVisitListener (global::Com.Gimbal.Proximity.IVisitListener p0);

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='VisitManager']/method[@name='start' and count(parameter)=0]"
		[Register ("start", "()V", "GetStartHandler:Com.Gimbal.Proximity.IVisitManagerInvoker, GimbalSDK")]
		void Start ();

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity']/interface[@name='VisitManager']/method[@name='stop' and count(parameter)=0]"
		[Register ("stop", "()V", "GetStopHandler:Com.Gimbal.Proximity.IVisitManagerInvoker, GimbalSDK")]
		void Stop ();

	}

	[global::Android.Runtime.Register ("com/gimbal/proximity/VisitManager", DoNotGenerateAcw=true)]
	internal class IVisitManagerInvoker : global::Java.Lang.Object, IVisitManager {

		static IntPtr java_class_ref = JNIEnv.FindClass ("com/gimbal/proximity/VisitManager");
		IntPtr class_ref;

		public static IVisitManager GetObject (IntPtr handle, JniHandleOwnership transfer)
		{
			return global::Java.Lang.Object.GetObject<IVisitManager> (handle, transfer);
		}

		static IntPtr Validate (IntPtr handle)
		{
			if (!JNIEnv.IsInstanceOf (handle, java_class_ref))
				throw new InvalidCastException (string.Format ("Unable to convert instance of type '{0}' to type '{1}'.",
							JNIEnv.GetClassNameFromInstance (handle), "com.gimbal.proximity.VisitManager"));
			return handle;
		}

		protected override void Dispose (bool disposing)
		{
			if (this.class_ref != IntPtr.Zero)
				JNIEnv.DeleteGlobalRef (this.class_ref);
			this.class_ref = IntPtr.Zero;
			base.Dispose (disposing);
		}

		public IVisitManagerInvoker (IntPtr handle, JniHandleOwnership transfer) : base (Validate (handle), transfer)
		{
			IntPtr local_ref = JNIEnv.GetObjectClass (Handle);
			this.class_ref = JNIEnv.NewGlobalRef (local_ref);
			JNIEnv.DeleteLocalRef (local_ref);
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (IVisitManagerInvoker); }
		}

		static Delegate cb_setVisitListener_Lcom_gimbal_proximity_VisitListener_;
#pragma warning disable 0169
		static Delegate GetSetVisitListener_Lcom_gimbal_proximity_VisitListener_Handler ()
		{
			if (cb_setVisitListener_Lcom_gimbal_proximity_VisitListener_ == null)
				cb_setVisitListener_Lcom_gimbal_proximity_VisitListener_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetVisitListener_Lcom_gimbal_proximity_VisitListener_);
			return cb_setVisitListener_Lcom_gimbal_proximity_VisitListener_;
		}

		static void n_SetVisitListener_Lcom_gimbal_proximity_VisitListener_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.IVisitManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IVisitManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Gimbal.Proximity.IVisitListener p0 = (global::Com.Gimbal.Proximity.IVisitListener)global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IVisitListener> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetVisitListener (p0);
		}
#pragma warning restore 0169

		IntPtr id_setVisitListener_Lcom_gimbal_proximity_VisitListener_;
		public unsafe void SetVisitListener (global::Com.Gimbal.Proximity.IVisitListener p0)
		{
			if (id_setVisitListener_Lcom_gimbal_proximity_VisitListener_ == IntPtr.Zero)
				id_setVisitListener_Lcom_gimbal_proximity_VisitListener_ = JNIEnv.GetMethodID (class_ref, "setVisitListener", "(Lcom/gimbal/proximity/VisitListener;)V");
			JValue* __args = stackalloc JValue [1];
			__args [0] = new JValue (p0);
			JNIEnv.CallVoidMethod (Handle, id_setVisitListener_Lcom_gimbal_proximity_VisitListener_, __args);
		}

		static Delegate cb_start;
#pragma warning disable 0169
		static Delegate GetStartHandler ()
		{
			if (cb_start == null)
				cb_start = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Start);
			return cb_start;
		}

		static void n_Start (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.IVisitManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IVisitManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Start ();
		}
#pragma warning restore 0169

		IntPtr id_start;
		public unsafe void Start ()
		{
			if (id_start == IntPtr.Zero)
				id_start = JNIEnv.GetMethodID (class_ref, "start", "()V");
			JNIEnv.CallVoidMethod (Handle, id_start);
		}

		static Delegate cb_stop;
#pragma warning disable 0169
		static Delegate GetStopHandler ()
		{
			if (cb_stop == null)
				cb_stop = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr>) n_Stop);
			return cb_stop;
		}

		static void n_Stop (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.IVisitManager __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.IVisitManager> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.Stop ();
		}
#pragma warning restore 0169

		IntPtr id_stop;
		public unsafe void Stop ()
		{
			if (id_stop == IntPtr.Zero)
				id_stop = JNIEnv.GetMethodID (class_ref, "stop", "()V");
			JNIEnv.CallVoidMethod (Handle, id_stop);
		}

	}

}
