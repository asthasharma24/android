using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='PushProperties']"
	[global::Android.Runtime.Register ("com/gimbal/internal/protocol/PushProperties", DoNotGenerateAcw=true)]
	public partial class PushProperties : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/protocol/PushProperties", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PushProperties); }
		}

		protected PushProperties (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='PushProperties']/constructor[@name='PushProperties' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe PushProperties ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (PushProperties)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getApplicationVersion;
#pragma warning disable 0169
		static Delegate GetGetApplicationVersionHandler ()
		{
			if (cb_getApplicationVersion == null)
				cb_getApplicationVersion = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, int>) n_GetApplicationVersion);
			return cb_getApplicationVersion;
		}

		static int n_GetApplicationVersion (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.PushProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.PushProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.ApplicationVersion;
		}
#pragma warning restore 0169

		static Delegate cb_setApplicationVersion_I;
#pragma warning disable 0169
		static Delegate GetSetApplicationVersion_IHandler ()
		{
			if (cb_setApplicationVersion_I == null)
				cb_setApplicationVersion_I = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, int>) n_SetApplicationVersion_I);
			return cb_setApplicationVersion_I;
		}

		static void n_SetApplicationVersion_I (IntPtr jnienv, IntPtr native__this, int p0)
		{
			global::Com.Gimbal.Internal.Protocol.PushProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.PushProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.ApplicationVersion = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApplicationVersion;
		static IntPtr id_setApplicationVersion_I;
		public virtual unsafe int ApplicationVersion {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='PushProperties']/method[@name='getApplicationVersion' and count(parameter)=0]"
			[Register ("getApplicationVersion", "()I", "GetGetApplicationVersionHandler")]
			get {
				if (id_getApplicationVersion == IntPtr.Zero)
					id_getApplicationVersion = JNIEnv.GetMethodID (class_ref, "getApplicationVersion", "()I");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallIntMethod  (Handle, id_getApplicationVersion);
					else
						return JNIEnv.CallNonvirtualIntMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApplicationVersion", "()I"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='PushProperties']/method[@name='setApplicationVersion' and count(parameter)=1 and parameter[1][@type='int']]"
			[Register ("setApplicationVersion", "(I)V", "GetSetApplicationVersion_IHandler")]
			set {
				if (id_setApplicationVersion_I == IntPtr.Zero)
					id_setApplicationVersion_I = JNIEnv.GetMethodID (class_ref, "setApplicationVersion", "(I)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApplicationVersion_I, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApplicationVersion", "(I)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPushRegistrationId;
#pragma warning disable 0169
		static Delegate GetGetPushRegistrationIdHandler ()
		{
			if (cb_getPushRegistrationId == null)
				cb_getPushRegistrationId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPushRegistrationId);
			return cb_getPushRegistrationId;
		}

		static IntPtr n_GetPushRegistrationId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.PushProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.PushProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.PushRegistrationId);
		}
#pragma warning restore 0169

		static Delegate cb_setPushRegistrationId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetPushRegistrationId_Ljava_lang_String_Handler ()
		{
			if (cb_setPushRegistrationId_Ljava_lang_String_ == null)
				cb_setPushRegistrationId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPushRegistrationId_Ljava_lang_String_);
			return cb_setPushRegistrationId_Ljava_lang_String_;
		}

		static void n_SetPushRegistrationId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.PushProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.PushProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.PushRegistrationId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPushRegistrationId;
		static IntPtr id_setPushRegistrationId_Ljava_lang_String_;
		public virtual unsafe string PushRegistrationId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='PushProperties']/method[@name='getPushRegistrationId' and count(parameter)=0]"
			[Register ("getPushRegistrationId", "()Ljava/lang/String;", "GetGetPushRegistrationIdHandler")]
			get {
				if (id_getPushRegistrationId == IntPtr.Zero)
					id_getPushRegistrationId = JNIEnv.GetMethodID (class_ref, "getPushRegistrationId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getPushRegistrationId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPushRegistrationId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='PushProperties']/method[@name='setPushRegistrationId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setPushRegistrationId", "(Ljava/lang/String;)V", "GetSetPushRegistrationId_Ljava_lang_String_Handler")]
			set {
				if (id_setPushRegistrationId_Ljava_lang_String_ == IntPtr.Zero)
					id_setPushRegistrationId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setPushRegistrationId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPushRegistrationId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPushRegistrationId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getSenderId;
#pragma warning disable 0169
		static Delegate GetGetSenderIdHandler ()
		{
			if (cb_getSenderId == null)
				cb_getSenderId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetSenderId);
			return cb_getSenderId;
		}

		static IntPtr n_GetSenderId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Protocol.PushProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.PushProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.SenderId);
		}
#pragma warning restore 0169

		static Delegate cb_setSenderId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetSenderId_Ljava_lang_String_Handler ()
		{
			if (cb_setSenderId_Ljava_lang_String_ == null)
				cb_setSenderId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetSenderId_Ljava_lang_String_);
			return cb_setSenderId_Ljava_lang_String_;
		}

		static void n_SetSenderId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Internal.Protocol.PushProperties __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Protocol.PushProperties> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SenderId = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getSenderId;
		static IntPtr id_setSenderId_Ljava_lang_String_;
		public virtual unsafe string SenderId {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='PushProperties']/method[@name='getSenderId' and count(parameter)=0]"
			[Register ("getSenderId", "()Ljava/lang/String;", "GetGetSenderIdHandler")]
			get {
				if (id_getSenderId == IntPtr.Zero)
					id_getSenderId = JNIEnv.GetMethodID (class_ref, "getSenderId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getSenderId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getSenderId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.protocol']/class[@name='PushProperties']/method[@name='setSenderId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setSenderId", "(Ljava/lang/String;)V", "GetSetSenderId_Ljava_lang_String_Handler")]
			set {
				if (id_setSenderId_Ljava_lang_String_ == IntPtr.Zero)
					id_setSenderId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setSenderId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setSenderId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setSenderId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
