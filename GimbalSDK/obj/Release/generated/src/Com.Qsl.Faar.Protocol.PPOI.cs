using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/PPOI", DoNotGenerateAcw=true)]
	public partial class PPOI : global::Java.Lang.Object, global::Java.IO.ISerializable {


		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/field[@name='HOME']"
		[Register ("HOME")]
		public const string Home = (string) "home";

		// Metadata.xml XPath field reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/field[@name='WORK']"
		[Register ("WORK")]
		public const string Work = (string) "work";
		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/PPOI", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PPOI); }
		}

		protected PPOI (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/constructor[@name='PPOI' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe PPOI ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (PPOI)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getId;
#pragma warning disable 0169
		static Delegate GetGetIdHandler ()
		{
			if (cb_getId == null)
				cb_getId = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetId);
			return cb_getId;
		}

		static IntPtr n_GetId (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PPOI __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PPOI> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Id);
		}
#pragma warning restore 0169

		static Delegate cb_setId_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetId_Ljava_lang_String_Handler ()
		{
			if (cb_setId_Ljava_lang_String_ == null)
				cb_setId_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetId_Ljava_lang_String_);
			return cb_setId_Ljava_lang_String_;
		}

		static void n_SetId_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PPOI __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PPOI> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Id = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getId;
		static IntPtr id_setId_Ljava_lang_String_;
		public virtual unsafe string Id {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/method[@name='getId' and count(parameter)=0]"
			[Register ("getId", "()Ljava/lang/String;", "GetGetIdHandler")]
			get {
				if (id_getId == IntPtr.Zero)
					id_getId = JNIEnv.GetMethodID (class_ref, "getId", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getId), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getId", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/method[@name='setId' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setId", "(Ljava/lang/String;)V", "GetSetId_Ljava_lang_String_Handler")]
			set {
				if (id_setId_Ljava_lang_String_ == IntPtr.Zero)
					id_setId_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setId", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setId_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setId", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getLatitude;
#pragma warning disable 0169
		static Delegate GetGetLatitudeHandler ()
		{
			if (cb_getLatitude == null)
				cb_getLatitude = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLatitude);
			return cb_getLatitude;
		}

		static IntPtr n_GetLatitude (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PPOI __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PPOI> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Latitude);
		}
#pragma warning restore 0169

		static Delegate cb_setLatitude_Ljava_lang_Double_;
#pragma warning disable 0169
		static Delegate GetSetLatitude_Ljava_lang_Double_Handler ()
		{
			if (cb_setLatitude_Ljava_lang_Double_ == null)
				cb_setLatitude_Ljava_lang_Double_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLatitude_Ljava_lang_Double_);
			return cb_setLatitude_Ljava_lang_Double_;
		}

		static void n_SetLatitude_Ljava_lang_Double_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PPOI __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PPOI> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Double p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Latitude = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLatitude;
		static IntPtr id_setLatitude_Ljava_lang_Double_;
		public virtual unsafe global::Java.Lang.Double Latitude {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/method[@name='getLatitude' and count(parameter)=0]"
			[Register ("getLatitude", "()Ljava/lang/Double;", "GetGetLatitudeHandler")]
			get {
				if (id_getLatitude == IntPtr.Zero)
					id_getLatitude = JNIEnv.GetMethodID (class_ref, "getLatitude", "()Ljava/lang/Double;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (JNIEnv.CallObjectMethod  (Handle, id_getLatitude), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLatitude", "()Ljava/lang/Double;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/method[@name='setLatitude' and count(parameter)=1 and parameter[1][@type='java.lang.Double']]"
			[Register ("setLatitude", "(Ljava/lang/Double;)V", "GetSetLatitude_Ljava_lang_Double_Handler")]
			set {
				if (id_setLatitude_Ljava_lang_Double_ == IntPtr.Zero)
					id_setLatitude_Ljava_lang_Double_ = JNIEnv.GetMethodID (class_ref, "setLatitude", "(Ljava/lang/Double;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLatitude_Ljava_lang_Double_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLatitude", "(Ljava/lang/Double;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getLongitude;
#pragma warning disable 0169
		static Delegate GetGetLongitudeHandler ()
		{
			if (cb_getLongitude == null)
				cb_getLongitude = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLongitude);
			return cb_getLongitude;
		}

		static IntPtr n_GetLongitude (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PPOI __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PPOI> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Longitude);
		}
#pragma warning restore 0169

		static Delegate cb_setLongitude_Ljava_lang_Double_;
#pragma warning disable 0169
		static Delegate GetSetLongitude_Ljava_lang_Double_Handler ()
		{
			if (cb_setLongitude_Ljava_lang_Double_ == null)
				cb_setLongitude_Ljava_lang_Double_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLongitude_Ljava_lang_Double_);
			return cb_setLongitude_Ljava_lang_Double_;
		}

		static void n_SetLongitude_Ljava_lang_Double_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PPOI __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PPOI> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Double p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Longitude = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLongitude;
		static IntPtr id_setLongitude_Ljava_lang_Double_;
		public virtual unsafe global::Java.Lang.Double Longitude {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/method[@name='getLongitude' and count(parameter)=0]"
			[Register ("getLongitude", "()Ljava/lang/Double;", "GetGetLongitudeHandler")]
			get {
				if (id_getLongitude == IntPtr.Zero)
					id_getLongitude = JNIEnv.GetMethodID (class_ref, "getLongitude", "()Ljava/lang/Double;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (JNIEnv.CallObjectMethod  (Handle, id_getLongitude), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Double> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLongitude", "()Ljava/lang/Double;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/method[@name='setLongitude' and count(parameter)=1 and parameter[1][@type='java.lang.Double']]"
			[Register ("setLongitude", "(Ljava/lang/Double;)V", "GetSetLongitude_Ljava_lang_Double_Handler")]
			set {
				if (id_setLongitude_Ljava_lang_Double_ == IntPtr.Zero)
					id_setLongitude_Ljava_lang_Double_ = JNIEnv.GetMethodID (class_ref, "setLongitude", "(Ljava/lang/Double;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLongitude_Ljava_lang_Double_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLongitude", "(Ljava/lang/Double;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getNeighborhoodIds;
#pragma warning disable 0169
		static Delegate GetGetNeighborhoodIdsHandler ()
		{
			if (cb_getNeighborhoodIds == null)
				cb_getNeighborhoodIds = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetNeighborhoodIds);
			return cb_getNeighborhoodIds;
		}

		static IntPtr n_GetNeighborhoodIds (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PPOI __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PPOI> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<string>.ToLocalJniHandle (__this.NeighborhoodIds);
		}
#pragma warning restore 0169

		static IntPtr id_getNeighborhoodIds;
		public virtual unsafe global::System.Collections.Generic.IList<string> NeighborhoodIds {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/method[@name='getNeighborhoodIds' and count(parameter)=0]"
			[Register ("getNeighborhoodIds", "()Ljava/util/List;", "GetGetNeighborhoodIdsHandler")]
			get {
				if (id_getNeighborhoodIds == IntPtr.Zero)
					id_getNeighborhoodIds = JNIEnv.GetMethodID (class_ref, "getNeighborhoodIds", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<string>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getNeighborhoodIds), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<string>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getNeighborhoodIds", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
		}

		static Delegate cb_getRelevanceIndex;
#pragma warning disable 0169
		static Delegate GetGetRelevanceIndexHandler ()
		{
			if (cb_getRelevanceIndex == null)
				cb_getRelevanceIndex = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetRelevanceIndex);
			return cb_getRelevanceIndex;
		}

		static IntPtr n_GetRelevanceIndex (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PPOI __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PPOI> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.RelevanceIndex);
		}
#pragma warning restore 0169

		static Delegate cb_setRelevanceIndex_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetRelevanceIndex_Ljava_lang_Long_Handler ()
		{
			if (cb_setRelevanceIndex_Ljava_lang_Long_ == null)
				cb_setRelevanceIndex_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetRelevanceIndex_Ljava_lang_Long_);
			return cb_setRelevanceIndex_Ljava_lang_Long_;
		}

		static void n_SetRelevanceIndex_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PPOI __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PPOI> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.RelevanceIndex = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getRelevanceIndex;
		static IntPtr id_setRelevanceIndex_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long RelevanceIndex {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/method[@name='getRelevanceIndex' and count(parameter)=0]"
			[Register ("getRelevanceIndex", "()Ljava/lang/Long;", "GetGetRelevanceIndexHandler")]
			get {
				if (id_getRelevanceIndex == IntPtr.Zero)
					id_getRelevanceIndex = JNIEnv.GetMethodID (class_ref, "getRelevanceIndex", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getRelevanceIndex), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getRelevanceIndex", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/method[@name='setRelevanceIndex' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setRelevanceIndex", "(Ljava/lang/Long;)V", "GetSetRelevanceIndex_Ljava_lang_Long_Handler")]
			set {
				if (id_setRelevanceIndex_Ljava_lang_Long_ == IntPtr.Zero)
					id_setRelevanceIndex_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setRelevanceIndex", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setRelevanceIndex_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setRelevanceIndex", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getTypes;
#pragma warning disable 0169
		static Delegate GetGetTypesHandler ()
		{
			if (cb_getTypes == null)
				cb_getTypes = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTypes);
			return cb_getTypes;
		}

		static IntPtr n_GetTypes (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PPOI __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PPOI> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<string>.ToLocalJniHandle (__this.Types);
		}
#pragma warning restore 0169

		static Delegate cb_setTypes_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetTypes_Ljava_util_List_Handler ()
		{
			if (cb_setTypes_Ljava_util_List_ == null)
				cb_setTypes_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTypes_Ljava_util_List_);
			return cb_setTypes_Ljava_util_List_;
		}

		static void n_SetTypes_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PPOI __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PPOI> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<string>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Types = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTypes;
		static IntPtr id_setTypes_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<string> Types {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/method[@name='getTypes' and count(parameter)=0]"
			[Register ("getTypes", "()Ljava/util/List;", "GetGetTypesHandler")]
			get {
				if (id_getTypes == IntPtr.Zero)
					id_getTypes = JNIEnv.GetMethodID (class_ref, "getTypes", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<string>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getTypes), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<string>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTypes", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/method[@name='setTypes' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;java.lang.String&gt;']]"
			[Register ("setTypes", "(Ljava/util/List;)V", "GetSetTypes_Ljava_util_List_Handler")]
			set {
				if (id_setTypes_Ljava_util_List_ == IntPtr.Zero)
					id_setTypes_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setTypes", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<string>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTypes_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTypes", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_setNeighborhoodSourceIds_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetNeighborhoodSourceIds_Ljava_util_List_Handler ()
		{
			if (cb_setNeighborhoodSourceIds_Ljava_util_List_ == null)
				cb_setNeighborhoodSourceIds_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetNeighborhoodSourceIds_Ljava_util_List_);
			return cb_setNeighborhoodSourceIds_Ljava_util_List_;
		}

		static void n_SetNeighborhoodSourceIds_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PPOI __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PPOI> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<string>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetNeighborhoodSourceIds (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setNeighborhoodSourceIds_Ljava_util_List_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PPOI']/method[@name='setNeighborhoodSourceIds' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;java.lang.String&gt;']]"
		[Register ("setNeighborhoodSourceIds", "(Ljava/util/List;)V", "GetSetNeighborhoodSourceIds_Ljava_util_List_Handler")]
		public virtual unsafe void SetNeighborhoodSourceIds (global::System.Collections.Generic.IList<string> p0)
		{
			if (id_setNeighborhoodSourceIds_Ljava_util_List_ == IntPtr.Zero)
				id_setNeighborhoodSourceIds_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setNeighborhoodSourceIds", "(Ljava/util/List;)V");
			IntPtr native_p0 = global::Android.Runtime.JavaList<string>.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setNeighborhoodSourceIds_Ljava_util_List_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setNeighborhoodSourceIds", "(Ljava/util/List;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
