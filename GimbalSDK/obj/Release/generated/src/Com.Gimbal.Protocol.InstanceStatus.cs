using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='InstanceStatus']"
	[global::Android.Runtime.Register ("com/gimbal/protocol/InstanceStatus", DoNotGenerateAcw=true)]
	public partial class InstanceStatus : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/protocol/InstanceStatus", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (InstanceStatus); }
		}

		protected InstanceStatus (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='InstanceStatus']/constructor[@name='InstanceStatus' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe InstanceStatus ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (InstanceStatus)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getEnabledAt;
#pragma warning disable 0169
		static Delegate GetGetEnabledAtHandler ()
		{
			if (cb_getEnabledAt == null)
				cb_getEnabledAt = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetEnabledAt);
			return cb_getEnabledAt;
		}

		static IntPtr n_GetEnabledAt (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.InstanceStatus __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.InstanceStatus> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.EnabledAt);
		}
#pragma warning restore 0169

		static Delegate cb_setEnabledAt_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetEnabledAt_Ljava_lang_Long_Handler ()
		{
			if (cb_setEnabledAt_Ljava_lang_Long_ == null)
				cb_setEnabledAt_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetEnabledAt_Ljava_lang_Long_);
			return cb_setEnabledAt_Ljava_lang_Long_;
		}

		static void n_SetEnabledAt_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.InstanceStatus __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.InstanceStatus> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.EnabledAt = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEnabledAt;
		static IntPtr id_setEnabledAt_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long EnabledAt {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='InstanceStatus']/method[@name='getEnabledAt' and count(parameter)=0]"
			[Register ("getEnabledAt", "()Ljava/lang/Long;", "GetGetEnabledAtHandler")]
			get {
				if (id_getEnabledAt == IntPtr.Zero)
					id_getEnabledAt = JNIEnv.GetMethodID (class_ref, "getEnabledAt", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getEnabledAt), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEnabledAt", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='InstanceStatus']/method[@name='setEnabledAt' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setEnabledAt", "(Ljava/lang/Long;)V", "GetSetEnabledAt_Ljava_lang_Long_Handler")]
			set {
				if (id_setEnabledAt_Ljava_lang_Long_ == IntPtr.Zero)
					id_setEnabledAt_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setEnabledAt", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEnabledAt_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEnabledAt", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getPermittedAt;
#pragma warning disable 0169
		static Delegate GetGetPermittedAtHandler ()
		{
			if (cb_getPermittedAt == null)
				cb_getPermittedAt = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetPermittedAt);
			return cb_getPermittedAt;
		}

		static IntPtr n_GetPermittedAt (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Protocol.InstanceStatus __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.InstanceStatus> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.PermittedAt);
		}
#pragma warning restore 0169

		static Delegate cb_setPermittedAt_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetPermittedAt_Ljava_lang_Long_Handler ()
		{
			if (cb_setPermittedAt_Ljava_lang_Long_ == null)
				cb_setPermittedAt_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetPermittedAt_Ljava_lang_Long_);
			return cb_setPermittedAt_Ljava_lang_Long_;
		}

		static void n_SetPermittedAt_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Protocol.InstanceStatus __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Protocol.InstanceStatus> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.PermittedAt = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getPermittedAt;
		static IntPtr id_setPermittedAt_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long PermittedAt {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='InstanceStatus']/method[@name='getPermittedAt' and count(parameter)=0]"
			[Register ("getPermittedAt", "()Ljava/lang/Long;", "GetGetPermittedAtHandler")]
			get {
				if (id_getPermittedAt == IntPtr.Zero)
					id_getPermittedAt = JNIEnv.GetMethodID (class_ref, "getPermittedAt", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getPermittedAt), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getPermittedAt", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.protocol']/class[@name='InstanceStatus']/method[@name='setPermittedAt' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setPermittedAt", "(Ljava/lang/Long;)V", "GetSetPermittedAt_Ljava_lang_Long_Handler")]
			set {
				if (id_setPermittedAt_Ljava_lang_Long_ == IntPtr.Zero)
					id_setPermittedAt_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setPermittedAt", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setPermittedAt_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setPermittedAt", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

	}
}
