using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlaceEvents']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/OrganizationPlaceEvents", DoNotGenerateAcw=true)]
	public partial class OrganizationPlaceEvents : global::Java.Lang.Object, global::Java.IO.ISerializable {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/OrganizationPlaceEvents", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (OrganizationPlaceEvents); }
		}

		protected OrganizationPlaceEvents (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlaceEvents']/constructor[@name='OrganizationPlaceEvents' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe OrganizationPlaceEvents ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (OrganizationPlaceEvents)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_addOrganizationPlaceEvent_Lcom_qsl_faar_protocol_OrganizationPlaceEvent_;
#pragma warning disable 0169
		static Delegate GetAddOrganizationPlaceEvent_Lcom_qsl_faar_protocol_OrganizationPlaceEvent_Handler ()
		{
			if (cb_addOrganizationPlaceEvent_Lcom_qsl_faar_protocol_OrganizationPlaceEvent_ == null)
				cb_addOrganizationPlaceEvent_Lcom_qsl_faar_protocol_OrganizationPlaceEvent_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_AddOrganizationPlaceEvent_Lcom_qsl_faar_protocol_OrganizationPlaceEvent_);
			return cb_addOrganizationPlaceEvent_Lcom_qsl_faar_protocol_OrganizationPlaceEvent_;
		}

		static void n_AddOrganizationPlaceEvent_Lcom_qsl_faar_protocol_OrganizationPlaceEvent_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvents __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvents> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvent p0 = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvent> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.AddOrganizationPlaceEvent (p0);
		}
#pragma warning restore 0169

		static IntPtr id_addOrganizationPlaceEvent_Lcom_qsl_faar_protocol_OrganizationPlaceEvent_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlaceEvents']/method[@name='addOrganizationPlaceEvent' and count(parameter)=1 and parameter[1][@type='com.qsl.faar.protocol.OrganizationPlaceEvent']]"
		[Register ("addOrganizationPlaceEvent", "(Lcom/qsl/faar/protocol/OrganizationPlaceEvent;)V", "GetAddOrganizationPlaceEvent_Lcom_qsl_faar_protocol_OrganizationPlaceEvent_Handler")]
		public virtual unsafe void AddOrganizationPlaceEvent (global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvent p0)
		{
			if (id_addOrganizationPlaceEvent_Lcom_qsl_faar_protocol_OrganizationPlaceEvent_ == IntPtr.Zero)
				id_addOrganizationPlaceEvent_Lcom_qsl_faar_protocol_OrganizationPlaceEvent_ = JNIEnv.GetMethodID (class_ref, "addOrganizationPlaceEvent", "(Lcom/qsl/faar/protocol/OrganizationPlaceEvent;)V");
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_addOrganizationPlaceEvent_Lcom_qsl_faar_protocol_OrganizationPlaceEvent_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addOrganizationPlaceEvent", "(Lcom/qsl/faar/protocol/OrganizationPlaceEvent;)V"), __args);
			} finally {
			}
		}

		static Delegate cb_getOrganizationPlaceEvents;
#pragma warning disable 0169
		static Delegate GetGetOrganizationPlaceEventsHandler ()
		{
			if (cb_getOrganizationPlaceEvents == null)
				cb_getOrganizationPlaceEvents = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationPlaceEvents);
			return cb_getOrganizationPlaceEvents;
		}

		static IntPtr n_GetOrganizationPlaceEvents (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvents __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvents> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvent>.ToLocalJniHandle (__this.GetOrganizationPlaceEvents ());
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationPlaceEvents;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlaceEvents']/method[@name='getOrganizationPlaceEvents' and count(parameter)=0]"
		[Register ("getOrganizationPlaceEvents", "()Ljava/util/List;", "GetGetOrganizationPlaceEventsHandler")]
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvent> GetOrganizationPlaceEvents ()
		{
			if (id_getOrganizationPlaceEvents == IntPtr.Zero)
				id_getOrganizationPlaceEvents = JNIEnv.GetMethodID (class_ref, "getOrganizationPlaceEvents", "()Ljava/util/List;");
			try {

				if (GetType () == ThresholdType)
					return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvent>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationPlaceEvents), JniHandleOwnership.TransferLocalRef);
				else
					return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvent>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationPlaceEvents", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
			} finally {
			}
		}

		static Delegate cb_setOrganizationPlaceEvents_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationPlaceEvents_Ljava_util_List_Handler ()
		{
			if (cb_setOrganizationPlaceEvents_Ljava_util_List_ == null)
				cb_setOrganizationPlaceEvents_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationPlaceEvents_Ljava_util_List_);
			return cb_setOrganizationPlaceEvents_Ljava_util_List_;
		}

		static void n_SetOrganizationPlaceEvents_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvents __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvents> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvent>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.SetOrganizationPlaceEvents (p0);
		}
#pragma warning restore 0169

		static IntPtr id_setOrganizationPlaceEvents_Ljava_util_List_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='OrganizationPlaceEvents']/method[@name='setOrganizationPlaceEvents' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.qsl.faar.protocol.OrganizationPlaceEvent&gt;']]"
		[Register ("setOrganizationPlaceEvents", "(Ljava/util/List;)V", "GetSetOrganizationPlaceEvents_Ljava_util_List_Handler")]
		public virtual unsafe void SetOrganizationPlaceEvents (global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvent> p0)
		{
			if (id_setOrganizationPlaceEvents_Ljava_util_List_ == IntPtr.Zero)
				id_setOrganizationPlaceEvents_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setOrganizationPlaceEvents", "(Ljava/util/List;)V");
			IntPtr native_p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.OrganizationPlaceEvent>.ToLocalJniHandle (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				if (GetType () == ThresholdType)
					JNIEnv.CallVoidMethod  (Handle, id_setOrganizationPlaceEvents_Ljava_util_List_, __args);
				else
					JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationPlaceEvents", "(Ljava/util/List;)V"), __args);
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
