using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Service.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RegisterResponse']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/service/protocol/RegisterResponse", DoNotGenerateAcw=true)]
	public partial class RegisterResponse : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/service/protocol/RegisterResponse", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (RegisterResponse); }
		}

		protected RegisterResponse (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RegisterResponse']/constructor[@name='RegisterResponse' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe RegisterResponse ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (RegisterResponse)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getCallbackUrl;
#pragma warning disable 0169
		static Delegate GetGetCallbackUrlHandler ()
		{
			if (cb_getCallbackUrl == null)
				cb_getCallbackUrl = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetCallbackUrl);
			return cb_getCallbackUrl;
		}

		static IntPtr n_GetCallbackUrl (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.RegisterResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.RegisterResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.CallbackUrl);
		}
#pragma warning restore 0169

		static Delegate cb_setCallbackUrl_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetCallbackUrl_Ljava_lang_String_Handler ()
		{
			if (cb_setCallbackUrl_Ljava_lang_String_ == null)
				cb_setCallbackUrl_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetCallbackUrl_Ljava_lang_String_);
			return cb_setCallbackUrl_Ljava_lang_String_;
		}

		static void n_SetCallbackUrl_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.RegisterResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.RegisterResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.CallbackUrl = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getCallbackUrl;
		static IntPtr id_setCallbackUrl_Ljava_lang_String_;
		public virtual unsafe string CallbackUrl {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RegisterResponse']/method[@name='getCallbackUrl' and count(parameter)=0]"
			[Register ("getCallbackUrl", "()Ljava/lang/String;", "GetGetCallbackUrlHandler")]
			get {
				if (id_getCallbackUrl == IntPtr.Zero)
					id_getCallbackUrl = JNIEnv.GetMethodID (class_ref, "getCallbackUrl", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getCallbackUrl), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getCallbackUrl", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RegisterResponse']/method[@name='setCallbackUrl' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setCallbackUrl", "(Ljava/lang/String;)V", "GetSetCallbackUrl_Ljava_lang_String_Handler")]
			set {
				if (id_setCallbackUrl_Ljava_lang_String_ == IntPtr.Zero)
					id_setCallbackUrl_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setCallbackUrl", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setCallbackUrl_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setCallbackUrl", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getUuid;
#pragma warning disable 0169
		static Delegate GetGetUuidHandler ()
		{
			if (cb_getUuid == null)
				cb_getUuid = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetUuid);
			return cb_getUuid;
		}

		static IntPtr n_GetUuid (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.RegisterResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.RegisterResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Uuid);
		}
#pragma warning restore 0169

		static Delegate cb_setUuid_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetUuid_Ljava_lang_String_Handler ()
		{
			if (cb_setUuid_Ljava_lang_String_ == null)
				cb_setUuid_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetUuid_Ljava_lang_String_);
			return cb_setUuid_Ljava_lang_String_;
		}

		static void n_SetUuid_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.RegisterResponse __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.RegisterResponse> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Uuid = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getUuid;
		static IntPtr id_setUuid_Ljava_lang_String_;
		public virtual unsafe string Uuid {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RegisterResponse']/method[@name='getUuid' and count(parameter)=0]"
			[Register ("getUuid", "()Ljava/lang/String;", "GetGetUuidHandler")]
			get {
				if (id_getUuid == IntPtr.Zero)
					id_getUuid = JNIEnv.GetMethodID (class_ref, "getUuid", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getUuid), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getUuid", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol']/class[@name='RegisterResponse']/method[@name='setUuid' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setUuid", "(Ljava/lang/String;)V", "GetSetUuid_Ljava_lang_String_Handler")]
			set {
				if (id_setUuid_Ljava_lang_String_ == IntPtr.Zero)
					id_setUuid_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setUuid", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setUuid_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setUuid", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
