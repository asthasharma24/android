using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Proximity.Core.Service.Protocol.Parser {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.parser']/class[@name='OAuthServerErrorParser']"
	[global::Android.Runtime.Register ("com/gimbal/proximity/core/service/protocol/parser/OAuthServerErrorParser", DoNotGenerateAcw=true)]
	public partial class OAuthServerErrorParser : global::Java.Lang.Object, global::Com.Gimbal.Proximity.Core.Service.Protocol.Parser.IServerErrorParser {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/proximity/core/service/protocol/parser/OAuthServerErrorParser", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (OAuthServerErrorParser); }
		}

		protected OAuthServerErrorParser (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.parser']/class[@name='OAuthServerErrorParser']/constructor[@name='OAuthServerErrorParser' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe OAuthServerErrorParser ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (OAuthServerErrorParser)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_parse_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetParse_Ljava_lang_String_Handler ()
		{
			if (cb_parse_Ljava_lang_String_ == null)
				cb_parse_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr>) n_Parse_Ljava_lang_String_);
			return cb_parse_Ljava_lang_String_;
		}

		static IntPtr n_Parse_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Gimbal.Proximity.Core.Service.Protocol.Parser.OAuthServerErrorParser __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.Parser.OAuthServerErrorParser> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.Parse (p0));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_parse_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.proximity.core.service.protocol.parser']/class[@name='OAuthServerErrorParser']/method[@name='parse' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
		[Register ("parse", "(Ljava/lang/String;)Lcom/gimbal/proximity/core/service/protocol/ServerError;", "GetParse_Ljava_lang_String_Handler")]
		public virtual unsafe global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError Parse (string p0)
		{
			if (id_parse_Ljava_lang_String_ == IntPtr.Zero)
				id_parse_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "parse", "(Ljava/lang/String;)Lcom/gimbal/proximity/core/service/protocol/ServerError;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);

				global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError __ret;
				if (GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError> (JNIEnv.CallObjectMethod  (Handle, id_parse_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Proximity.Core.Service.Protocol.ServerError> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "parse", "(Ljava/lang/String;)Lcom/gimbal/proximity/core/service/protocol/ServerError;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
