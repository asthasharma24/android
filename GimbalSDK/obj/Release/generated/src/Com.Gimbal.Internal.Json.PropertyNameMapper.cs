using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Json {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='PropertyNameMapper']"
	[global::Android.Runtime.Register ("com/gimbal/internal/json/PropertyNameMapper", DoNotGenerateAcw=true)]
	public abstract partial class PropertyNameMapper : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/json/PropertyNameMapper", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PropertyNameMapper); }
		}

		protected PropertyNameMapper (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor_arrayLjava_lang_Class_;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='PropertyNameMapper']/constructor[@name='PropertyNameMapper' and count(parameter)=1 and parameter[1][@type='java.lang.Class&lt;?&gt;...']]"
		[Register (".ctor", "([Ljava/lang/Class;)V", "")]
		public unsafe PropertyNameMapper (params global:: Java.Lang.Class[] p0)
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			IntPtr native_p0 = JNIEnv.NewArray (p0);
			try {
				JValue* __args = stackalloc JValue [1];
				__args [0] = new JValue (native_p0);
				if (GetType () != typeof (PropertyNameMapper)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "([Ljava/lang/Class;)V", __args),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "([Ljava/lang/Class;)V", __args);
					return;
				}

				if (id_ctor_arrayLjava_lang_Class_ == IntPtr.Zero)
					id_ctor_arrayLjava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "<init>", "([Ljava/lang/Class;)V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor_arrayLjava_lang_Class_, __args),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor_arrayLjava_lang_Class_, __args);
			} finally {
				if (p0 != null) {
					JNIEnv.CopyArray (native_p0, p0);
					JNIEnv.DeleteLocalRef (native_p0);
				}
			}
		}

		static Delegate cb_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetConvertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_Handler ()
		{
			if (cb_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ == null)
				cb_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_ConvertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_);
			return cb_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_;
		}

		static IntPtr n_ConvertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.PropertyNameMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.PropertyNameMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.ConvertFromJsonPropertyName (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='PropertyNameMapper']/method[@name='convertFromJsonPropertyName' and count(parameter)=2 and parameter[1][@type='java.lang.Class&lt;?&gt;'] and parameter[2][@type='java.lang.String']]"
		[Register ("convertFromJsonPropertyName", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;", "GetConvertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_Handler")]
		public abstract string ConvertFromJsonPropertyName (global::Java.Lang.Class p0, string p1);

		static Delegate cb_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetConvertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_Handler ()
		{
			if (cb_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ == null)
				cb_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_ConvertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_);
			return cb_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_;
		}

		static IntPtr n_ConvertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.PropertyNameMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.PropertyNameMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p0, JniHandleOwnership.DoNotTransfer);
			string p1 = JNIEnv.GetString (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.NewString (__this.ConvertToJsonPropertyName (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='PropertyNameMapper']/method[@name='convertToJsonPropertyName' and count(parameter)=2 and parameter[1][@type='java.lang.Class&lt;?&gt;'] and parameter[2][@type='java.lang.String']]"
		[Register ("convertToJsonPropertyName", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;", "GetConvertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_Handler")]
		public abstract string ConvertToJsonPropertyName (global::Java.Lang.Class p0, string p1);

		static Delegate cb_getTargetClass;
#pragma warning disable 0169
		static Delegate GetGetTargetClassHandler ()
		{
			if (cb_getTargetClass == null)
				cb_getTargetClass = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTargetClass);
			return cb_getTargetClass;
		}

		static IntPtr n_GetTargetClass (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Gimbal.Internal.Json.PropertyNameMapper __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.PropertyNameMapper> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewArray (__this.GetTargetClass ());
		}
#pragma warning restore 0169

		static IntPtr id_getTargetClass;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='PropertyNameMapper']/method[@name='getTargetClass' and count(parameter)=0]"
		[Register ("getTargetClass", "()[Ljava/lang/Class;", "GetGetTargetClassHandler")]
		public virtual unsafe global::Java.Lang.Class[] GetTargetClass ()
		{
			if (id_getTargetClass == IntPtr.Zero)
				id_getTargetClass = JNIEnv.GetMethodID (class_ref, "getTargetClass", "()[Ljava/lang/Class;");
			try {

				if (GetType () == ThresholdType)
					return (global::Java.Lang.Class[]) JNIEnv.GetArray (JNIEnv.CallObjectMethod  (Handle, id_getTargetClass), JniHandleOwnership.TransferLocalRef, typeof (global::Java.Lang.Class));
				else
					return (global::Java.Lang.Class[]) JNIEnv.GetArray (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTargetClass", "()[Ljava/lang/Class;")), JniHandleOwnership.TransferLocalRef, typeof (global::Java.Lang.Class));
			} finally {
			}
		}

	}

	[global::Android.Runtime.Register ("com/gimbal/internal/json/PropertyNameMapper", DoNotGenerateAcw=true)]
	internal partial class PropertyNameMapperInvoker : PropertyNameMapper {

		public PropertyNameMapperInvoker (IntPtr handle, JniHandleOwnership transfer) : base (handle, transfer) {}

		protected override global::System.Type ThresholdType {
			get { return typeof (PropertyNameMapperInvoker); }
		}

		static IntPtr id_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='PropertyNameMapper']/method[@name='convertFromJsonPropertyName' and count(parameter)=2 and parameter[1][@type='java.lang.Class&lt;?&gt;'] and parameter[2][@type='java.lang.String']]"
		[Register ("convertFromJsonPropertyName", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;", "GetConvertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_Handler")]
		public override unsafe string ConvertFromJsonPropertyName (global::Java.Lang.Class p0, string p1)
		{
			if (id_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ == IntPtr.Zero)
				id_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "convertFromJsonPropertyName", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				string __ret = JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_convertFromJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

		static IntPtr id_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='PropertyNameMapper']/method[@name='convertToJsonPropertyName' and count(parameter)=2 and parameter[1][@type='java.lang.Class&lt;?&gt;'] and parameter[2][@type='java.lang.String']]"
		[Register ("convertToJsonPropertyName", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;", "GetConvertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_Handler")]
		public override unsafe string ConvertToJsonPropertyName (global::Java.Lang.Class p0, string p1)
		{
			if (id_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ == IntPtr.Zero)
				id_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "convertToJsonPropertyName", "(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/String;");
			IntPtr native_p1 = JNIEnv.NewString (p1);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (p0);
				__args [1] = new JValue (native_p1);
				string __ret = JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_convertToJsonPropertyName_Ljava_lang_Class_Ljava_lang_String_, __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p1);
			}
		}

	}

}
