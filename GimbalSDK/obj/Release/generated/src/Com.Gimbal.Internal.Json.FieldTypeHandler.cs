using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Gimbal.Internal.Json {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='FieldTypeHandler']"
	[global::Android.Runtime.Register ("com/gimbal/internal/json/FieldTypeHandler", DoNotGenerateAcw=true)]
	public partial class FieldTypeHandler : global::Com.Gimbal.Internal.Json.ClassHandler {

		internal static new IntPtr java_class_handle;
		internal static new IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/gimbal/internal/json/FieldTypeHandler", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (FieldTypeHandler); }
		}

		protected FieldTypeHandler (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='FieldTypeHandler']/constructor[@name='FieldTypeHandler' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe FieldTypeHandler ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (FieldTypeHandler)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_addFieldType_Ljava_lang_String_Ljava_lang_Class_;
#pragma warning disable 0169
		static Delegate GetAddFieldType_Ljava_lang_String_Ljava_lang_Class_Handler ()
		{
			if (cb_addFieldType_Ljava_lang_String_Ljava_lang_Class_ == null)
				cb_addFieldType_Ljava_lang_String_Ljava_lang_Class_ = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr, IntPtr, IntPtr>) n_AddFieldType_Ljava_lang_String_Ljava_lang_Class_);
			return cb_addFieldType_Ljava_lang_String_Ljava_lang_Class_;
		}

		static IntPtr n_AddFieldType_Ljava_lang_String_Ljava_lang_Class_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0, IntPtr native_p1)
		{
			global::Com.Gimbal.Internal.Json.FieldTypeHandler __this = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.FieldTypeHandler> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Class p1 = global::Java.Lang.Object.GetObject<global::Java.Lang.Class> (native_p1, JniHandleOwnership.DoNotTransfer);
			IntPtr __ret = JNIEnv.ToLocalJniHandle (__this.AddFieldType (p0, p1));
			return __ret;
		}
#pragma warning restore 0169

		static IntPtr id_addFieldType_Ljava_lang_String_Ljava_lang_Class_;
		// Metadata.xml XPath method reference: path="/api/package[@name='com.gimbal.internal.json']/class[@name='FieldTypeHandler']/method[@name='addFieldType' and count(parameter)=2 and parameter[1][@type='java.lang.String'] and parameter[2][@type='java.lang.Class&lt;?&gt;']]"
		[Register ("addFieldType", "(Ljava/lang/String;Ljava/lang/Class;)Lcom/gimbal/internal/json/FieldTypeHandler;", "GetAddFieldType_Ljava_lang_String_Ljava_lang_Class_Handler")]
		public virtual unsafe global::Com.Gimbal.Internal.Json.FieldTypeHandler AddFieldType (string p0, global::Java.Lang.Class p1)
		{
			if (id_addFieldType_Ljava_lang_String_Ljava_lang_Class_ == IntPtr.Zero)
				id_addFieldType_Ljava_lang_String_Ljava_lang_Class_ = JNIEnv.GetMethodID (class_ref, "addFieldType", "(Ljava/lang/String;Ljava/lang/Class;)Lcom/gimbal/internal/json/FieldTypeHandler;");
			IntPtr native_p0 = JNIEnv.NewString (p0);
			try {
				JValue* __args = stackalloc JValue [2];
				__args [0] = new JValue (native_p0);
				__args [1] = new JValue (p1);

				global::Com.Gimbal.Internal.Json.FieldTypeHandler __ret;
				if (GetType () == ThresholdType)
					__ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.FieldTypeHandler> (JNIEnv.CallObjectMethod  (Handle, id_addFieldType_Ljava_lang_String_Ljava_lang_Class_, __args), JniHandleOwnership.TransferLocalRef);
				else
					__ret = global::Java.Lang.Object.GetObject<global::Com.Gimbal.Internal.Json.FieldTypeHandler> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "addFieldType", "(Ljava/lang/String;Ljava/lang/Class;)Lcom/gimbal/internal/json/FieldTypeHandler;"), __args), JniHandleOwnership.TransferLocalRef);
				return __ret;
			} finally {
				JNIEnv.DeleteLocalRef (native_p0);
			}
		}

	}
}
