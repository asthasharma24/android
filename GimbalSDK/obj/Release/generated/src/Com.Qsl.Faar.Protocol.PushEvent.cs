using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/PushEvent", DoNotGenerateAcw=true)]
	public partial class PushEvent : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/PushEvent", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PushEvent); }
		}

		protected PushEvent (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/constructor[@name='PushEvent' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe PushEvent ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (PushEvent)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getActionTag;
#pragma warning disable 0169
		static Delegate GetGetActionTagHandler ()
		{
			if (cb_getActionTag == null)
				cb_getActionTag = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetActionTag);
			return cb_getActionTag;
		}

		static IntPtr n_GetActionTag (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PushEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ActionTag);
		}
#pragma warning restore 0169

		static Delegate cb_setActionTag_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetActionTag_Ljava_lang_String_Handler ()
		{
			if (cb_setActionTag_Ljava_lang_String_ == null)
				cb_setActionTag_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetActionTag_Ljava_lang_String_);
			return cb_setActionTag_Ljava_lang_String_;
		}

		static void n_SetActionTag_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PushEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ActionTag = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getActionTag;
		static IntPtr id_setActionTag_Ljava_lang_String_;
		public virtual unsafe string ActionTag {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/method[@name='getActionTag' and count(parameter)=0]"
			[Register ("getActionTag", "()Ljava/lang/String;", "GetGetActionTagHandler")]
			get {
				if (id_getActionTag == IntPtr.Zero)
					id_getActionTag = JNIEnv.GetMethodID (class_ref, "getActionTag", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getActionTag), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getActionTag", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/method[@name='setActionTag' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setActionTag", "(Ljava/lang/String;)V", "GetSetActionTag_Ljava_lang_String_Handler")]
			set {
				if (id_setActionTag_Ljava_lang_String_ == IntPtr.Zero)
					id_setActionTag_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setActionTag", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setActionTag_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setActionTag", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getContentUUID;
#pragma warning disable 0169
		static Delegate GetGetContentUUIDHandler ()
		{
			if (cb_getContentUUID == null)
				cb_getContentUUID = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetContentUUID);
			return cb_getContentUUID;
		}

		static IntPtr n_GetContentUUID (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PushEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ContentUUID);
		}
#pragma warning restore 0169

		static Delegate cb_setContentUUID_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetContentUUID_Ljava_lang_String_Handler ()
		{
			if (cb_setContentUUID_Ljava_lang_String_ == null)
				cb_setContentUUID_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetContentUUID_Ljava_lang_String_);
			return cb_setContentUUID_Ljava_lang_String_;
		}

		static void n_SetContentUUID_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PushEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ContentUUID = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getContentUUID;
		static IntPtr id_setContentUUID_Ljava_lang_String_;
		public virtual unsafe string ContentUUID {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/method[@name='getContentUUID' and count(parameter)=0]"
			[Register ("getContentUUID", "()Ljava/lang/String;", "GetGetContentUUIDHandler")]
			get {
				if (id_getContentUUID == IntPtr.Zero)
					id_getContentUUID = JNIEnv.GetMethodID (class_ref, "getContentUUID", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getContentUUID), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getContentUUID", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/method[@name='setContentUUID' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setContentUUID", "(Ljava/lang/String;)V", "GetSetContentUUID_Ljava_lang_String_Handler")]
			set {
				if (id_setContentUUID_Ljava_lang_String_ == IntPtr.Zero)
					id_setContentUUID_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setContentUUID", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setContentUUID_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setContentUUID", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getDescription;
#pragma warning disable 0169
		static Delegate GetGetDescriptionHandler ()
		{
			if (cb_getDescription == null)
				cb_getDescription = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetDescription);
			return cb_getDescription;
		}

		static IntPtr n_GetDescription (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PushEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Description);
		}
#pragma warning restore 0169

		static Delegate cb_setDescription_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetDescription_Ljava_lang_String_Handler ()
		{
			if (cb_setDescription_Ljava_lang_String_ == null)
				cb_setDescription_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetDescription_Ljava_lang_String_);
			return cb_setDescription_Ljava_lang_String_;
		}

		static void n_SetDescription_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PushEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Description = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getDescription;
		static IntPtr id_setDescription_Ljava_lang_String_;
		public virtual unsafe string Description {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/method[@name='getDescription' and count(parameter)=0]"
			[Register ("getDescription", "()Ljava/lang/String;", "GetGetDescriptionHandler")]
			get {
				if (id_getDescription == IntPtr.Zero)
					id_getDescription = JNIEnv.GetMethodID (class_ref, "getDescription", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getDescription), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getDescription", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/method[@name='setDescription' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setDescription", "(Ljava/lang/String;)V", "GetSetDescription_Ljava_lang_String_Handler")]
			set {
				if (id_setDescription_Ljava_lang_String_ == IntPtr.Zero)
					id_setDescription_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setDescription", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setDescription_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setDescription", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getEventTime;
#pragma warning disable 0169
		static Delegate GetGetEventTimeHandler ()
		{
			if (cb_getEventTime == null)
				cb_getEventTime = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, long>) n_GetEventTime);
			return cb_getEventTime;
		}

		static long n_GetEventTime (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PushEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return __this.EventTime;
		}
#pragma warning restore 0169

		static Delegate cb_setEventTime_J;
#pragma warning disable 0169
		static Delegate GetSetEventTime_JHandler ()
		{
			if (cb_setEventTime_J == null)
				cb_setEventTime_J = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, long>) n_SetEventTime_J);
			return cb_setEventTime_J;
		}

		static void n_SetEventTime_J (IntPtr jnienv, IntPtr native__this, long p0)
		{
			global::Com.Qsl.Faar.Protocol.PushEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			__this.EventTime = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getEventTime;
		static IntPtr id_setEventTime_J;
		public virtual unsafe long EventTime {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/method[@name='getEventTime' and count(parameter)=0]"
			[Register ("getEventTime", "()J", "GetGetEventTimeHandler")]
			get {
				if (id_getEventTime == IntPtr.Zero)
					id_getEventTime = JNIEnv.GetMethodID (class_ref, "getEventTime", "()J");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.CallLongMethod  (Handle, id_getEventTime);
					else
						return JNIEnv.CallNonvirtualLongMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getEventTime", "()J"));
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/method[@name='setEventTime' and count(parameter)=1 and parameter[1][@type='long']]"
			[Register ("setEventTime", "(J)V", "GetSetEventTime_JHandler")]
			set {
				if (id_setEventTime_J == IntPtr.Zero)
					id_setEventTime_J = JNIEnv.GetMethodID (class_ref, "setEventTime", "(J)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setEventTime_J, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setEventTime", "(J)V"), __args);
				} finally {
				}
			}
		}

		static Delegate cb_getTitle;
#pragma warning disable 0169
		static Delegate GetGetTitleHandler ()
		{
			if (cb_getTitle == null)
				cb_getTitle = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTitle);
			return cb_getTitle;
		}

		static IntPtr n_GetTitle (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PushEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Title);
		}
#pragma warning restore 0169

		static Delegate cb_setTitle_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetTitle_Ljava_lang_String_Handler ()
		{
			if (cb_setTitle_Ljava_lang_String_ == null)
				cb_setTitle_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTitle_Ljava_lang_String_);
			return cb_setTitle_Ljava_lang_String_;
		}

		static void n_SetTitle_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PushEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Title = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTitle;
		static IntPtr id_setTitle_Ljava_lang_String_;
		public virtual unsafe string Title {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/method[@name='getTitle' and count(parameter)=0]"
			[Register ("getTitle", "()Ljava/lang/String;", "GetGetTitleHandler")]
			get {
				if (id_getTitle == IntPtr.Zero)
					id_getTitle = JNIEnv.GetMethodID (class_ref, "getTitle", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getTitle), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTitle", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/method[@name='setTitle' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setTitle", "(Ljava/lang/String;)V", "GetSetTitle_Ljava_lang_String_Handler")]
			set {
				if (id_setTitle_Ljava_lang_String_ == IntPtr.Zero)
					id_setTitle_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setTitle", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTitle_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTitle", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getType;
#pragma warning disable 0169
		static Delegate GetGetTypeHandler ()
		{
			if (cb_getType == null)
				cb_getType = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetType);
			return cb_getType;
		}

		static IntPtr n_GetType (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PushEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.Type);
		}
#pragma warning restore 0169

		static Delegate cb_setType_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetType_Ljava_lang_String_Handler ()
		{
			if (cb_setType_Ljava_lang_String_ == null)
				cb_setType_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetType_Ljava_lang_String_);
			return cb_setType_Ljava_lang_String_;
		}

		static void n_SetType_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PushEvent __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushEvent> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Type = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getType;
		static IntPtr id_setType_Ljava_lang_String_;
		public virtual unsafe string Type {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/method[@name='getType' and count(parameter)=0]"
			[Register ("getType", "()Ljava/lang/String;", "GetGetTypeHandler")]
			get {
				if (id_getType == IntPtr.Zero)
					id_getType = JNIEnv.GetMethodID (class_ref, "getType", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getType), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getType", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushEvent']/method[@name='setType' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setType", "(Ljava/lang/String;)V", "GetSetType_Ljava_lang_String_Handler")]
			set {
				if (id_setType_Ljava_lang_String_ == IntPtr.Zero)
					id_setType_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setType", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setType_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setType", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

	}
}
