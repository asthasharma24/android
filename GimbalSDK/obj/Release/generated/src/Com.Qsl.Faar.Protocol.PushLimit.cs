using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushLimit']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/PushLimit", DoNotGenerateAcw=true)]
	public partial class PushLimit : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/PushLimit", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (PushLimit); }
		}

		protected PushLimit (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushLimit']/constructor[@name='PushLimit' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe PushLimit ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (PushLimit)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getApplicationName;
#pragma warning disable 0169
		static Delegate GetGetApplicationNameHandler ()
		{
			if (cb_getApplicationName == null)
				cb_getApplicationName = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetApplicationName);
			return cb_getApplicationName;
		}

		static IntPtr n_GetApplicationName (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PushLimit __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushLimit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.NewString (__this.ApplicationName);
		}
#pragma warning restore 0169

		static Delegate cb_setApplicationName_Ljava_lang_String_;
#pragma warning disable 0169
		static Delegate GetSetApplicationName_Ljava_lang_String_Handler ()
		{
			if (cb_setApplicationName_Ljava_lang_String_ == null)
				cb_setApplicationName_Ljava_lang_String_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetApplicationName_Ljava_lang_String_);
			return cb_setApplicationName_Ljava_lang_String_;
		}

		static void n_SetApplicationName_Ljava_lang_String_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PushLimit __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushLimit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			string p0 = JNIEnv.GetString (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.ApplicationName = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getApplicationName;
		static IntPtr id_setApplicationName_Ljava_lang_String_;
		public virtual unsafe string ApplicationName {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushLimit']/method[@name='getApplicationName' and count(parameter)=0]"
			[Register ("getApplicationName", "()Ljava/lang/String;", "GetGetApplicationNameHandler")]
			get {
				if (id_getApplicationName == IntPtr.Zero)
					id_getApplicationName = JNIEnv.GetMethodID (class_ref, "getApplicationName", "()Ljava/lang/String;");
				try {

					if (GetType () == ThresholdType)
						return JNIEnv.GetString (JNIEnv.CallObjectMethod  (Handle, id_getApplicationName), JniHandleOwnership.TransferLocalRef);
					else
						return JNIEnv.GetString (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getApplicationName", "()Ljava/lang/String;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushLimit']/method[@name='setApplicationName' and count(parameter)=1 and parameter[1][@type='java.lang.String']]"
			[Register ("setApplicationName", "(Ljava/lang/String;)V", "GetSetApplicationName_Ljava_lang_String_Handler")]
			set {
				if (id_setApplicationName_Ljava_lang_String_ == IntPtr.Zero)
					id_setApplicationName_Ljava_lang_String_ = JNIEnv.GetMethodID (class_ref, "setApplicationName", "(Ljava/lang/String;)V");
				IntPtr native_value = JNIEnv.NewString (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setApplicationName_Ljava_lang_String_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setApplicationName", "(Ljava/lang/String;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getLimit;
#pragma warning disable 0169
		static Delegate GetGetLimitHandler ()
		{
			if (cb_getLimit == null)
				cb_getLimit = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetLimit);
			return cb_getLimit;
		}

		static IntPtr n_GetLimit (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.PushLimit __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushLimit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Limit);
		}
#pragma warning restore 0169

		static Delegate cb_setLimit_Ljava_lang_Integer_;
#pragma warning disable 0169
		static Delegate GetSetLimit_Ljava_lang_Integer_Handler ()
		{
			if (cb_setLimit_Ljava_lang_Integer_ == null)
				cb_setLimit_Ljava_lang_Integer_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetLimit_Ljava_lang_Integer_);
			return cb_setLimit_Ljava_lang_Integer_;
		}

		static void n_SetLimit_Ljava_lang_Integer_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.PushLimit __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.PushLimit> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Integer p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Limit = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getLimit;
		static IntPtr id_setLimit_Ljava_lang_Integer_;
		public virtual unsafe global::Java.Lang.Integer Limit {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushLimit']/method[@name='getLimit' and count(parameter)=0]"
			[Register ("getLimit", "()Ljava/lang/Integer;", "GetGetLimitHandler")]
			get {
				if (id_getLimit == IntPtr.Zero)
					id_getLimit = JNIEnv.GetMethodID (class_ref, "getLimit", "()Ljava/lang/Integer;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallObjectMethod  (Handle, id_getLimit), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Integer> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getLimit", "()Ljava/lang/Integer;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol']/class[@name='PushLimit']/method[@name='setLimit' and count(parameter)=1 and parameter[1][@type='java.lang.Integer']]"
			[Register ("setLimit", "(Ljava/lang/Integer;)V", "GetSetLimit_Ljava_lang_Integer_Handler")]
			set {
				if (id_setLimit_Ljava_lang_Integer_ == IntPtr.Zero)
					id_setLimit_Ljava_lang_Integer_ = JNIEnv.GetMethodID (class_ref, "setLimit", "(Ljava/lang/Integer;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setLimit_Ljava_lang_Integer_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setLimit", "(Ljava/lang/Integer;)V"), __args);
				} finally {
				}
			}
		}

	}
}
