using System;
using System.Collections.Generic;
using Android.Runtime;

namespace Com.Qsl.Faar.Protocol.Content {

	// Metadata.xml XPath class reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTriggerFilter']"
	[global::Android.Runtime.Register ("com/qsl/faar/protocol/content/ContentTimeTriggerFilter", DoNotGenerateAcw=true)]
	public partial class ContentTimeTriggerFilter : global::Java.Lang.Object {

		internal static IntPtr java_class_handle;
		internal static IntPtr class_ref {
			get {
				return JNIEnv.FindClass ("com/qsl/faar/protocol/content/ContentTimeTriggerFilter", ref java_class_handle);
			}
		}

		protected override IntPtr ThresholdClass {
			get { return class_ref; }
		}

		protected override global::System.Type ThresholdType {
			get { return typeof (ContentTimeTriggerFilter); }
		}

		protected ContentTimeTriggerFilter (IntPtr javaReference, JniHandleOwnership transfer) : base (javaReference, transfer) {}

		static IntPtr id_ctor;
		// Metadata.xml XPath constructor reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTriggerFilter']/constructor[@name='ContentTimeTriggerFilter' and count(parameter)=0]"
		[Register (".ctor", "()V", "")]
		public unsafe ContentTimeTriggerFilter ()
			: base (IntPtr.Zero, JniHandleOwnership.DoNotTransfer)
		{
			if (Handle != IntPtr.Zero)
				return;

			try {
				if (GetType () != typeof (ContentTimeTriggerFilter)) {
					SetHandle (
							global::Android.Runtime.JNIEnv.StartCreateInstance (GetType (), "()V"),
							JniHandleOwnership.TransferLocalRef);
					global::Android.Runtime.JNIEnv.FinishCreateInstance (Handle, "()V");
					return;
				}

				if (id_ctor == IntPtr.Zero)
					id_ctor = JNIEnv.GetMethodID (class_ref, "<init>", "()V");
				SetHandle (
						global::Android.Runtime.JNIEnv.StartCreateInstance (class_ref, id_ctor),
						JniHandleOwnership.TransferLocalRef);
				JNIEnv.FinishCreateInstance (Handle, class_ref, id_ctor);
			} finally {
			}
		}

		static Delegate cb_getOrganizationsWithProfilePermission;
#pragma warning disable 0169
		static Delegate GetGetOrganizationsWithProfilePermissionHandler ()
		{
			if (cb_getOrganizationsWithProfilePermission == null)
				cb_getOrganizationsWithProfilePermission = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetOrganizationsWithProfilePermission);
			return cb_getOrganizationsWithProfilePermission;
		}

		static IntPtr n_GetOrganizationsWithProfilePermission (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTriggerFilter __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTriggerFilter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission>.ToLocalJniHandle (__this.OrganizationsWithProfilePermission);
		}
#pragma warning restore 0169

		static Delegate cb_setOrganizationsWithProfilePermission_Ljava_util_List_;
#pragma warning disable 0169
		static Delegate GetSetOrganizationsWithProfilePermission_Ljava_util_List_Handler ()
		{
			if (cb_setOrganizationsWithProfilePermission_Ljava_util_List_ == null)
				cb_setOrganizationsWithProfilePermission_Ljava_util_List_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetOrganizationsWithProfilePermission_Ljava_util_List_);
			return cb_setOrganizationsWithProfilePermission_Ljava_util_List_;
		}

		static void n_SetOrganizationsWithProfilePermission_Ljava_util_List_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTriggerFilter __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTriggerFilter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			var p0 = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission>.FromJniHandle (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.OrganizationsWithProfilePermission = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getOrganizationsWithProfilePermission;
		static IntPtr id_setOrganizationsWithProfilePermission_Ljava_util_List_;
		public virtual unsafe global::System.Collections.Generic.IList<global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission> OrganizationsWithProfilePermission {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTriggerFilter']/method[@name='getOrganizationsWithProfilePermission' and count(parameter)=0]"
			[Register ("getOrganizationsWithProfilePermission", "()Ljava/util/List;", "GetGetOrganizationsWithProfilePermissionHandler")]
			get {
				if (id_getOrganizationsWithProfilePermission == IntPtr.Zero)
					id_getOrganizationsWithProfilePermission = JNIEnv.GetMethodID (class_ref, "getOrganizationsWithProfilePermission", "()Ljava/util/List;");
				try {

					if (GetType () == ThresholdType)
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission>.FromJniHandle (JNIEnv.CallObjectMethod  (Handle, id_getOrganizationsWithProfilePermission), JniHandleOwnership.TransferLocalRef);
					else
						return global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission>.FromJniHandle (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getOrganizationsWithProfilePermission", "()Ljava/util/List;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTriggerFilter']/method[@name='setOrganizationsWithProfilePermission' and count(parameter)=1 and parameter[1][@type='java.util.List&lt;com.qsl.faar.protocol.content.OrganizationWithProfilePermission&gt;']]"
			[Register ("setOrganizationsWithProfilePermission", "(Ljava/util/List;)V", "GetSetOrganizationsWithProfilePermission_Ljava_util_List_Handler")]
			set {
				if (id_setOrganizationsWithProfilePermission_Ljava_util_List_ == IntPtr.Zero)
					id_setOrganizationsWithProfilePermission_Ljava_util_List_ = JNIEnv.GetMethodID (class_ref, "setOrganizationsWithProfilePermission", "(Ljava/util/List;)V");
				IntPtr native_value = global::Android.Runtime.JavaList<global::Com.Qsl.Faar.Protocol.Content.OrganizationWithProfilePermission>.ToLocalJniHandle (value);
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (native_value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setOrganizationsWithProfilePermission_Ljava_util_List_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setOrganizationsWithProfilePermission", "(Ljava/util/List;)V"), __args);
				} finally {
					JNIEnv.DeleteLocalRef (native_value);
				}
			}
		}

		static Delegate cb_getTimestamp;
#pragma warning disable 0169
		static Delegate GetGetTimestampHandler ()
		{
			if (cb_getTimestamp == null)
				cb_getTimestamp = JNINativeWrapper.CreateDelegate ((Func<IntPtr, IntPtr, IntPtr>) n_GetTimestamp);
			return cb_getTimestamp;
		}

		static IntPtr n_GetTimestamp (IntPtr jnienv, IntPtr native__this)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTriggerFilter __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTriggerFilter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			return JNIEnv.ToLocalJniHandle (__this.Timestamp);
		}
#pragma warning restore 0169

		static Delegate cb_setTimestamp_Ljava_lang_Long_;
#pragma warning disable 0169
		static Delegate GetSetTimestamp_Ljava_lang_Long_Handler ()
		{
			if (cb_setTimestamp_Ljava_lang_Long_ == null)
				cb_setTimestamp_Ljava_lang_Long_ = JNINativeWrapper.CreateDelegate ((Action<IntPtr, IntPtr, IntPtr>) n_SetTimestamp_Ljava_lang_Long_);
			return cb_setTimestamp_Ljava_lang_Long_;
		}

		static void n_SetTimestamp_Ljava_lang_Long_ (IntPtr jnienv, IntPtr native__this, IntPtr native_p0)
		{
			global::Com.Qsl.Faar.Protocol.Content.ContentTimeTriggerFilter __this = global::Java.Lang.Object.GetObject<global::Com.Qsl.Faar.Protocol.Content.ContentTimeTriggerFilter> (jnienv, native__this, JniHandleOwnership.DoNotTransfer);
			global::Java.Lang.Long p0 = global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (native_p0, JniHandleOwnership.DoNotTransfer);
			__this.Timestamp = p0;
		}
#pragma warning restore 0169

		static IntPtr id_getTimestamp;
		static IntPtr id_setTimestamp_Ljava_lang_Long_;
		public virtual unsafe global::Java.Lang.Long Timestamp {
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTriggerFilter']/method[@name='getTimestamp' and count(parameter)=0]"
			[Register ("getTimestamp", "()Ljava/lang/Long;", "GetGetTimestampHandler")]
			get {
				if (id_getTimestamp == IntPtr.Zero)
					id_getTimestamp = JNIEnv.GetMethodID (class_ref, "getTimestamp", "()Ljava/lang/Long;");
				try {

					if (GetType () == ThresholdType)
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallObjectMethod  (Handle, id_getTimestamp), JniHandleOwnership.TransferLocalRef);
					else
						return global::Java.Lang.Object.GetObject<global::Java.Lang.Long> (JNIEnv.CallNonvirtualObjectMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "getTimestamp", "()Ljava/lang/Long;")), JniHandleOwnership.TransferLocalRef);
				} finally {
				}
			}
			// Metadata.xml XPath method reference: path="/api/package[@name='com.qsl.faar.protocol.content']/class[@name='ContentTimeTriggerFilter']/method[@name='setTimestamp' and count(parameter)=1 and parameter[1][@type='java.lang.Long']]"
			[Register ("setTimestamp", "(Ljava/lang/Long;)V", "GetSetTimestamp_Ljava_lang_Long_Handler")]
			set {
				if (id_setTimestamp_Ljava_lang_Long_ == IntPtr.Zero)
					id_setTimestamp_Ljava_lang_Long_ = JNIEnv.GetMethodID (class_ref, "setTimestamp", "(Ljava/lang/Long;)V");
				try {
					JValue* __args = stackalloc JValue [1];
					__args [0] = new JValue (value);

					if (GetType () == ThresholdType)
						JNIEnv.CallVoidMethod  (Handle, id_setTimestamp_Ljava_lang_Long_, __args);
					else
						JNIEnv.CallNonvirtualVoidMethod  (Handle, ThresholdClass, JNIEnv.GetMethodID (ThresholdClass, "setTimestamp", "(Ljava/lang/Long;)V"), __args);
				} finally {
				}
			}
		}

	}
}
