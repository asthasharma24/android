﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SchoolRewards_App
{
	[BroadcastReceiver]
	[IntentFilter(new[] {Intent.ActionBootCompleted})]
	public class MyGimbalServiceReceiver : BroadcastReceiver
	{
		public MyGimbalServiceReceiver(){}

		public override void OnReceive (Context context, Intent intent)
		{
//			Toast.MakeText (context, "Received intent!", ToastLength.Short).Show ();
			context.StartService (new Intent (context, typeof(MyGimbalService)));
		}
	}
}

