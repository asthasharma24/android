using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V4.App;
using Android.Gms.Maps.Model;
using Android.Gms.Maps;
using Android.Webkit;
using System.Collections.Generic;

using System.Net;
using System.Text.RegularExpressions;
using Android.Content.PM;

namespace SchoolRewards_App
{
    [Activity(Label = "Maps", Theme = "@android:style/Theme.NoTitleBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class Maps : Activity
    {
        private MapFragment mapFrag;
        private MapFragment _myMapFragment;
        MarkerOptions markerOpt1;
        List<string> list = new List<string>();
        string[] rid = new string[4];
        string[] restaurants = new string[20];
        string[] finalSplit;
        string[] mySplit;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Maps);

            //URl Body retreive
           

            WebClient client = new WebClient();
			String htmlCode = client.DownloadString("https://app.schoolicious.org/SR_RestMapData");
            int start = htmlCode.IndexOf("<MyData>");
            int to = htmlCode.IndexOf("</MyData>", start + "<MyData>".Length);
            string s = htmlCode.Substring(start + "<MyData>".Length, to - start - "<MyData>".Length);
            mySplit = s.Split(new string[] { "^" }, StringSplitOptions.RemoveEmptyEntries);
            
//            ImageButton qr = FindViewById<ImageButton>(Resource.Id.imageButton_scanner);
            ImageButton home = FindViewById<ImageButton>(Resource.Id.imageButton_home);
            ImageButton maps = FindViewById<ImageButton>(Resource.Id.imageButton_maps);
            ImageButton rlist = FindViewById<ImageButton>(Resource.Id.imageButton_rlist);
          

            LatLng location = new LatLng(32.9700, -117.0386);
            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            builder.Target(location);
            builder.Zoom(12);
            builder.Bearing(155);
            CameraPosition cameraPosition = builder.Build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);

            MapFragment mapFrag = (MapFragment)FragmentManager.FindFragmentById(Resource.Id.map);
            GoogleMap map = mapFrag.Map;
            if (map != null)
            {
                for (int i = 0; i < mySplit.Length; i++)
                {
                    finalSplit = mySplit[i].Split(',');
                    for (int j = 0; j < finalSplit.Length; j++)
                    {
                        map.MoveCamera(cameraUpdate);
                        MarkerOptions markerOpt1 = new MarkerOptions();
                        markerOpt1.InvokeIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.pinball_Left_Red));
                        markerOpt1.SetPosition(new LatLng(Convert.ToDouble(finalSplit[2]), Convert.ToDouble(finalSplit[3])));
                        markerOpt1.SetTitle(finalSplit[1]);
                        Marker mar = map.AddMarker(markerOpt1);
                    }
                    
                }
                map.InfoWindowClick += onInfoWindowClick;
               // map.MarkerClick += MapOnMarkerClick;
            }

//            qr.Click += async delegate
//            {
//                //Tell our scanner to use the default overlay
//                scanner.UseCustomOverlay = false;
//
//                //We can customize the top and bottom text of the default overlay
//                scanner.TopText = "Hold the camera up to the barcode\nAbout 6 inches away";
//                scanner.BottomText = "Wait for the barcode to automatically scan!";
//
//                //Start scanning
//                var result = await scanner.Scan();
//
//                HandleScanResult(result);
//            };
            home.Click += delegate
            {
                StartActivity(typeof(Home));
            };
            maps.Click += delegate
            {
                StartActivity(typeof(Maps));
            };
            rlist.Click += delegate
            {
                StartActivity(typeof(RList));
            };

        }


        public class HelloWebViewClient : WebViewClient
        {
            public override bool ShouldOverrideUrlLoading(WebView view, string url)
            {

                view.LoadUrl(url);
                return true;
            }
        }

       


        void onInfoWindowClick(object sender, GoogleMap.InfoWindowClickEventArgs e) {
            Marker marker = e.Marker;

                for (int i = 0; i < mySplit.Length; i++)
                {
                    string[] fSplit = mySplit[i].Split(',');
                    for (int j = 0; j < fSplit.Length; j++)
                    {
                        if (marker.Title == fSplit[1])
                        {
                            //WebView wb = FindViewById<WebView>(Resource.Id.webView1);
						DOL.URL = "https://app.schoolicious.org/SR_Menu?RID=" + fSplit[4] + "&RestaurantCode=" + fSplit[4];
                            StartActivity(typeof(MenuList));
                        }
                    }
                    
                }
               
        
        }
    }
}