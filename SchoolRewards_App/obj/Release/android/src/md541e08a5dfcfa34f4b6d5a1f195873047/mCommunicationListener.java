package md541e08a5dfcfa34f4b6d5a1f195873047;


public class mCommunicationListener
	extends com.gimbal.android.CommunicationListener
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_presentNotificationForCommunications:(Ljava/util/Collection;Lcom/gimbal/android/Visit;)Ljava/util/Collection;:GetPresentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Visit_Handler\n" +
			"n_presentNotificationForCommunications:(Ljava/util/Collection;Lcom/gimbal/android/Push;)Ljava/util/Collection;:GetPresentNotificationForCommunications_Ljava_util_Collection_Lcom_gimbal_android_Push_Handler\n" +
			"n_prepareCommunicationForDisplay:(Lcom/gimbal/android/Communication;Lcom/gimbal/android/Visit;I)Landroid/app/Notification$Builder;:GetPrepareCommunicationForDisplay_Lcom_gimbal_android_Communication_Lcom_gimbal_android_Visit_IHandler\n" +
			"n_onNotificationClicked:(Ljava/util/List;)V:GetOnNotificationClicked_Ljava_util_List_Handler\n" +
			"";
		mono.android.Runtime.register ("SchoolRewards_App.mCommunicationListener, SchoolRewards_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", mCommunicationListener.class, __md_methods);
	}


	public mCommunicationListener () throws java.lang.Throwable
	{
		super ();
		if (getClass () == mCommunicationListener.class)
			mono.android.TypeManager.Activate ("SchoolRewards_App.mCommunicationListener, SchoolRewards_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public java.util.Collection presentNotificationForCommunications (java.util.Collection p0, com.gimbal.android.Visit p1)
	{
		return n_presentNotificationForCommunications (p0, p1);
	}

	private native java.util.Collection n_presentNotificationForCommunications (java.util.Collection p0, com.gimbal.android.Visit p1);


	public java.util.Collection presentNotificationForCommunications (java.util.Collection p0, com.gimbal.android.Push p1)
	{
		return n_presentNotificationForCommunications (p0, p1);
	}

	private native java.util.Collection n_presentNotificationForCommunications (java.util.Collection p0, com.gimbal.android.Push p1);


	public android.app.Notification.Builder prepareCommunicationForDisplay (com.gimbal.android.Communication p0, com.gimbal.android.Visit p1, int p2)
	{
		return n_prepareCommunicationForDisplay (p0, p1, p2);
	}

	private native android.app.Notification.Builder n_prepareCommunicationForDisplay (com.gimbal.android.Communication p0, com.gimbal.android.Visit p1, int p2);


	public void onNotificationClicked (java.util.List p0)
	{
		n_onNotificationClicked (p0);
	}

	private native void n_onNotificationClicked (java.util.List p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
