package md541e08a5dfcfa34f4b6d5a1f195873047;


public class MainActivity_mPlaceEventListener
	extends com.gimbal.android.PlaceEventListener
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onVisitStart:(Lcom/gimbal/android/Visit;)V:GetOnVisitStart_Lcom_gimbal_android_Visit_Handler\n" +
			"n_onVisitEnd:(Lcom/gimbal/android/Visit;)V:GetOnVisitEnd_Lcom_gimbal_android_Visit_Handler\n" +
			"";
		mono.android.Runtime.register ("SchoolRewards_App.MainActivity+mPlaceEventListener, SchoolRewards_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", MainActivity_mPlaceEventListener.class, __md_methods);
	}


	public MainActivity_mPlaceEventListener () throws java.lang.Throwable
	{
		super ();
		if (getClass () == MainActivity_mPlaceEventListener.class)
			mono.android.TypeManager.Activate ("SchoolRewards_App.MainActivity+mPlaceEventListener, SchoolRewards_App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onVisitStart (com.gimbal.android.Visit p0)
	{
		n_onVisitStart (p0);
	}

	private native void n_onVisitStart (com.gimbal.android.Visit p0);


	public void onVisitEnd (com.gimbal.android.Visit p0)
	{
		n_onVisitEnd (p0);
	}

	private native void n_onVisitEnd (com.gimbal.android.Visit p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
