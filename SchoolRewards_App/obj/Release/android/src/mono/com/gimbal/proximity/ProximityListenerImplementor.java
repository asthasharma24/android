package mono.com.gimbal.proximity;


public class ProximityListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.gimbal.proximity.ProximityListener
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_serviceStarted:()V:GetServiceStartedHandler:Com.Gimbal.Proximity.IProximityListenerInvoker, GimbalSDK\n" +
			"n_startServiceFailed:(ILjava/lang/String;)V:GetStartServiceFailed_ILjava_lang_String_Handler:Com.Gimbal.Proximity.IProximityListenerInvoker, GimbalSDK\n" +
			"";
		mono.android.Runtime.register ("Com.Gimbal.Proximity.IProximityListenerImplementor, GimbalSDK, Version=2.8.1.0, Culture=neutral, PublicKeyToken=null", ProximityListenerImplementor.class, __md_methods);
	}


	public ProximityListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == ProximityListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Gimbal.Proximity.IProximityListenerImplementor, GimbalSDK, Version=2.8.1.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void serviceStarted ()
	{
		n_serviceStarted ();
	}

	private native void n_serviceStarted ();


	public void startServiceFailed (int p0, java.lang.String p1)
	{
		n_startServiceFailed (p0, p1);
	}

	private native void n_startServiceFailed (int p0, java.lang.String p1);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
