package mono.com.gimbal.proximity;


public class VisitListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.gimbal.proximity.VisitListener
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_didArrive:(Lcom/gimbal/proximity/impl/InternalBeaconFenceVisit;)V:GetDidArrive_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_Handler:Com.Gimbal.Proximity.IVisitListenerInvoker, GimbalSDK\n" +
			"n_didDepart:(Lcom/gimbal/proximity/impl/InternalBeaconFenceVisit;)V:GetDidDepart_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_Handler:Com.Gimbal.Proximity.IVisitListenerInvoker, GimbalSDK\n" +
			"n_receivedSighting:(Lcom/gimbal/proximity/core/sighting/Sighting;Lcom/gimbal/proximity/impl/InternalBeaconFenceVisit;)V:GetReceivedSighting_Lcom_gimbal_proximity_core_sighting_Sighting_Lcom_gimbal_proximity_impl_InternalBeaconFenceVisit_Handler:Com.Gimbal.Proximity.IVisitListenerInvoker, GimbalSDK\n" +
			"";
		mono.android.Runtime.register ("Com.Gimbal.Proximity.IVisitListenerImplementor, GimbalSDK, Version=2.8.1.0, Culture=neutral, PublicKeyToken=null", VisitListenerImplementor.class, __md_methods);
	}


	public VisitListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == VisitListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Gimbal.Proximity.IVisitListenerImplementor, GimbalSDK, Version=2.8.1.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void didArrive (com.gimbal.proximity.impl.InternalBeaconFenceVisit p0)
	{
		n_didArrive (p0);
	}

	private native void n_didArrive (com.gimbal.proximity.impl.InternalBeaconFenceVisit p0);


	public void didDepart (com.gimbal.proximity.impl.InternalBeaconFenceVisit p0)
	{
		n_didDepart (p0);
	}

	private native void n_didDepart (com.gimbal.proximity.impl.InternalBeaconFenceVisit p0);


	public void receivedSighting (com.gimbal.proximity.core.sighting.Sighting p0, com.gimbal.proximity.impl.InternalBeaconFenceVisit p1)
	{
		n_receivedSighting (p0, p1);
	}

	private native void n_receivedSighting (com.gimbal.proximity.core.sighting.Sighting p0, com.gimbal.proximity.impl.InternalBeaconFenceVisit p1);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
