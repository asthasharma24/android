package mono.com.gimbal.logging.util;


public class LogCapture_LogListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.gimbal.logging.util.LogCapture.LogListener
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_log:(Lcom/gimbal/logging/util/LogCapture$LogEntry;Ljava/util/List;)V:GetLog_Lcom_gimbal_logging_util_LogCapture_LogEntry_Ljava_util_List_Handler:Com.Gimbal.Logging.Util.LogCapture/ILogListenerInvoker, GimbalSDK\n" +
			"";
		mono.android.Runtime.register ("Com.Gimbal.Logging.Util.LogCapture+ILogListenerImplementor, GimbalSDK, Version=2.8.1.0, Culture=neutral, PublicKeyToken=null", LogCapture_LogListenerImplementor.class, __md_methods);
	}


	public LogCapture_LogListenerImplementor () throws java.lang.Throwable
	{
		super ();
		if (getClass () == LogCapture_LogListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Gimbal.Logging.Util.LogCapture+ILogListenerImplementor, GimbalSDK, Version=2.8.1.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void log (com.gimbal.logging.util.LogCapture.LogEntry p0, java.util.List p1)
	{
		n_log (p0, p1);
	}

	private native void n_log (com.gimbal.logging.util.LogCapture.LogEntry p0, java.util.List p1);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
