using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;

using Android.Content.PM;

namespace SchoolRewards_App
{
    [Activity(Label = "ThankYou", Theme = "@android:style/Theme.NoTitleBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class ThankYou : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.ThankYou);
            WebView wb = FindViewById<WebView>(Resource.Id.webView_thankyou);
            wb.Settings.JavaScriptEnabled = true;
            if(DOL.QR_Results.Contains("ifNoCoupon"))
            {
             wb.LoadUrl(DOL.QR_Results + "&DeviceID=" + DOL.Android_ID);
            }
            else
            {
				wb.LoadUrl("https://app.schoolicious.org/SR_Home?DeviceID=" + DOL.Android_ID);
            }
            wb.SetWebViewClient(new HelloWebViewClient());
         

//            ImageButton qr = FindViewById<ImageButton>(Resource.Id.imageButton_scanner);
            ImageButton home = FindViewById<ImageButton>(Resource.Id.imageButton_home);
            ImageButton map = FindViewById<ImageButton>(Resource.Id.imageButton_maps);
            ImageButton rlist = FindViewById<ImageButton>(Resource.Id.imageButton_rlist);
//            qr.Click += async delegate
//            {
//                //Tell our scanner to use the default overlay
//                scanner.UseCustomOverlay = false;
//
//                //We can customize the top and bottom text of the default overlay
//                scanner.TopText = "Hold the camera up to the barcode\nAbout 6 inches away";
//                scanner.BottomText = "Wait for the barcode to automatically scan!";
//
//                //Start scanning
//                var result = await scanner.Scan();
//
//                HandleScanResult(result);
//            };
            home.Click += delegate
            {
                StartActivity(typeof(Home));
            };
            map.Click += delegate
            {
                StartActivity(typeof(Maps));
            };
            rlist.Click += delegate
            {
                StartActivity(typeof(RList));
            };
        }
			

        public class HelloWebViewClient : WebViewClient
        {
            public override bool ShouldOverrideUrlLoading(WebView view, string url)
            {

                view.LoadUrl(url);
                return true;
            }
        }
    }
}