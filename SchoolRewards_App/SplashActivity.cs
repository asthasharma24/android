using System.Threading;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Content.PM;
using Android.Net;

namespace SchoolRewards_App
{
    [Activity(Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true, ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Thread.Sleep(3000); // Simulate a long loading process on app startup.
            /*
            string droidid = Settings.Secure.GetString(ContentResolver, Settings.Secure.AndroidId);
            //SR_Library.SR_Lib.Android_ID = droidid;
            TextView txt = (TextView)FindViewById<TextView>(Resource.Id.textView1);
            var str = droidid.ToString();
             * */
             
            ConnectivityManager connectivityManager = (ConnectivityManager)GetSystemService(ConnectivityService);
            NetworkInfo activeConnection = connectivityManager.ActiveNetworkInfo;
            bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

            if (isOnline)
            {
                StartActivity(typeof(MainActivity));
            }
            else
            {
                Toast.MakeText(this, "No Internet Connection available.", ToastLength.Long).Show();
            }
            
            Finish();
        }
    }


}