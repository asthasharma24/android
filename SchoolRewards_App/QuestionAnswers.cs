﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Webkit;
using Android.Provider;
using Android.Net;
using Android.Content.PM;

namespace SchoolRewards_App
{
	[Activity(Label = "QuestionAnswers", Theme = "@android:style/Theme.NoTitleBar", ScreenOrientation = ScreenOrientation.Portrait)]	
	public class QuestionAnswers : Activity
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView(Resource.Layout.QuestionAnswers);
			// Create your application here

			string urlToOpen = Intent.GetStringExtra ("url");


			WebView wb = FindViewById<WebView>(Resource.Id.webView_home);
			wb.Settings.JavaScriptEnabled = true;
//			http://staging.schoolicious.org/participatingrestaurant?rid=100042288
			wb.LoadUrl(urlToOpen);
			wb.SetWebViewClient(new HelloWebViewClient());
			//MobileBarcodeScanner scanner = new MobileBarcodeScanner(this);

//			ImageButton qr = FindViewById<ImageButton>(Resource.Id.imageButton_scanner);
			ImageButton home = FindViewById<ImageButton>(Resource.Id.imageButton_home);
			ImageButton map = FindViewById<ImageButton>(Resource.Id.imageButton_maps);
			ImageButton rlist = FindViewById<ImageButton>(Resource.Id.imageButton_rlist);
//			qr.Click += async delegate
//			{
//				//Tell our scanner to use the default overlay
//				scanner.UseCustomOverlay = false;
//
//				//We can customize the top and bottom text of the default overlay
//				scanner.TopText = "Hold the camera up to the barcode\nAbout 6 inches away";
//				scanner.BottomText = "Wait for the barcode to automatically scan!";
//
//				//Start scanning
//				var result = await scanner.Scan();
//
//				HandleScanResult(result);
//			};
			home.Click += delegate
			{
				StartActivity(typeof(Home));
			};
			map.Click += delegate
			{
				StartActivity(typeof(Maps));
			};
			rlist.Click += delegate
			{
				StartActivity(typeof(RList));
			};

		}



		public class HelloWebViewClient : WebViewClient
		{
			public override bool ShouldOverrideUrlLoading(WebView view, string url)
			{

				view.LoadUrl(url);
				return true;
			}
		}
	}

}

