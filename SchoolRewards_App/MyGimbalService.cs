using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
//using Android.Support.V4.AppCompat;
using Android.Media;
using Com.Gimbal.Android;
using Android.Widget;

namespace SchoolRewards_App
{
    [Service]
    public class MyGimbalService : Service
    {
        private mPlaceEventListener mPlaceEventListener;
        private mCommunicationListener mCommunicationListener;

        public override void OnCreate()
        {

//			Toast.MakeText (Android.App.Application.Context, "Service Oncreate", ToastLength.Long).Show ();

            base.OnCreate();

            
			Gimbal.SetApiKey (Application, "495f81aa-7c55-448e-aaa1-3c6d8799cc7d");
			//Gimbal.SetApiKey (Application, "c1c40441-b3a1-4c3c-9703-871935641e41");
			//Gimbal.RegisterForPush("451021036090");
            Gimbal.RegisterForPush("950843506647");

            Log.Debug("GimbalService", "Created the Gimbal service, setting API key");

            mPlaceEventListener = new mPlaceEventListener();
            PlaceManager.Instance.AddListener(mPlaceEventListener);
			mCommunicationListener = new mCommunicationListener();
            CommunicationManager.Instance.AddListener(mCommunicationListener);

			PlaceManager.Instance.StartMonitoring ();
			CommunicationManager.Instance.StartReceivingCommunications();



        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
			Log.Error ("Service","Started");


            base.OnStartCommand(intent, flags, startId);
            return StartCommandResult.Sticky;
        }


        public override void OnDestroy()
        {

//            PlaceManager.Instance.RemoveListener(mPlaceEventListener);
//            CommunicationManager.Instance.RemoveListener(mCommunicationListener);
            
			base.OnDestroy();

			StartService (new Intent (this, typeof(MyGimbalService)));
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;

        }
    }

	// communication event Listener
	public class mCommunicationListener : CommunicationListener
	{

		public mCommunicationListener()
		{
			//_context = (MainActivity)econtext;
		}

		public override ICollection<Communication> PresentNotificationForCommunications(ICollection<Communication> communications, Visit visit)
		{
			//base.PresentNotificationForCommunications (communications);
			// Handle new communications from Place entry event

			Console.WriteLine ("PresentNotificationForCommunications Visit ----------");

			//Toast.MakeText (Android.App.Application.Context, "PresentNotificationForCommunications", ToastLength.Long).Show ();

			foreach (Communication communication in communications)
			{

                if (MainActivity.appIsInForegroundMode)
                {

                    if (communication.Attributes.GetValue("Event").Equals("Exit"))
                    {

                       // MainActivity.wb.LoadUrl("https://app.schoolicious.org/participatingrestaurant?rid=" + communication.Attributes.GetValue("rid") + "&DeviceID=" + DOL.Android_ID);
                        MainActivity.wb.LoadUrl("http://staging.app.schoolicious.org/participatingrestaurant?SRID=" + communication.Attributes.GetValue("rid") + "&DeviceID=" + DOL.Android_ID);

                    }
                    else
                    {
                        MainActivity.wb.LoadUrl("http://staging.app.schoolicious.org/SR_Menu?SRID=" + communication.Attributes.GetValue("rid") + "&DeviceID=" + DOL.Android_ID);

                    }

                }
              

             

                    Console.WriteLine ("Title : "+communication.Title+"\n" +
					"Description : "+communication.Description + "\n" + "appIsInForegroundMode >>>> " + MainActivity.appIsInForegroundMode + "");
			}

			return communications;
		}

		public override ICollection<Communication> PresentNotificationForCommunications(ICollection<Communication> communications, Push push)
		{

			Console.WriteLine ("PresentNotificationForCommunications Push ----------");
           // Toast.MakeText(Android.App.Application.Context, "PresentNotificationForCommunications Push", ToastLength.Long).Show();
            foreach (Communication communication in communications)
			{
				Console.WriteLine ("Titlee for push: "+communication.Title+"\n" +
					"Descriptionn for push: "+communication.Description + "\n" + "appIsInForegroundMode push>>>> " + MainActivity.appIsInForegroundMode + "");

            }

			return communications;
		}

		public override Notification.Builder PrepareCommunicationForDisplay(Communication comm, Visit vst, int notificationId) {

            // Build the notification:
            Console.WriteLine("Notification Builder -----visit----- appIsInForegroundMode >>>> " + MainActivity.appIsInForegroundMode + "");
           // Toast.MakeText(Android.App.Application.Context,
            //    "Notification Builder -----visit----- appIsInForegroundMode >>>> " 
            //    + MainActivity.appIsInForegroundMode + "",
            //    ToastLength.Long).Show();
            Notification.Builder builder = new Notification.Builder (Android.App.Application.Context);
			builder.SetSound(RingtoneManager.GetDefaultUri(RingtoneType.Notification));
			builder.SetAutoCancel (true) ;
			builder.SetContentTitle (comm.Title);
			builder.SetContentText (comm.Description);
			builder.SetSmallIcon (Resource.Drawable.icon);


			return builder;
		}

       

        public override void OnNotificationClicked (IList<Communication> p0)
		{
//			Toast.MakeText (Android.App.Application.Context, "Notification Clicked", ToastLength.Long).Show ();
			base.OnNotificationClicked (p0);


			if (MainActivity.appIsInForegroundMode) {
				//Toast.MakeText (Android.App.Application.Context, "Notification Clicked App is in foreground", ToastLength.Long).Show ();
				foreach (Communication comm in p0) {

					string msg_event = comm.Attributes.GetValue ("Event");
					string event_rid = comm.Attributes.GetValue ("rid");

					Console.WriteLine ("Notification clicked - APP FORGROUND- -- " + comm.Attributes.GetValue("Event"));

					if (msg_event.Equals ("Exit"))
                    {
                        MainActivity.wb.LoadUrl("http://staging.app.schoolicious.org/participatingrestaurant?SRID=" + event_rid + "&DeviceID=" + DOL.Android_ID);
					} else
                    {
                        MainActivity.wb.LoadUrl("http://staging.app.schoolicious.org/SR_Menu?SRID=" + event_rid + "&DeviceID=" + DOL.Android_ID);
                    }

				}

			} 
			else {

			
				foreach (Communication comm in p0) {

					string msg_event = comm.Attributes.GetValue ("Event");
					string event_rid = comm.Attributes.GetValue ("rid");
					string urlToOpen = "http://staging.app.schoolicious.org/sr_Contact";

					Console.WriteLine ("Notification clicked - - bg-- " + comm.Attributes.GetValue("Event"));
					Intent intent;

					if (msg_event.Equals ("Exit")) {
						urlToOpen = "http://staging.app.schoolicious.org/participatingrestaurant?SRID=" + event_rid + "&DeviceID=" + DOL.Android_ID;
					}
					else{
						urlToOpen = "http://staging.app.schoolicious.org/SR_Menu?SRID=" + event_rid + "&DeviceID=" + DOL.Android_ID;
					}

					intent = new Intent(Android.App.Application.Context, typeof(MainActivity));
					intent.PutExtra ("url",urlToOpen);
					intent.AddFlags(ActivityFlags.FromBackground);
					intent.AddCategory(Intent.CategoryLauncher);
					intent.AddFlags (ActivityFlags.ClearTask);
					intent.AddFlags (ActivityFlags.NewTask);
					Android.App.Application.Context.StartActivity(intent);

				}

			}


		}


	}


	// Place Event Listener
	public class mPlaceEventListener : PlaceEventListener
	{
		public mPlaceEventListener()
		{
		}
		public override void OnVisitStart(Visit p0)
		{
			base.OnVisitStart(p0);
			Console.WriteLine ("On Visit Start : " + p0.Place.Name);
		}

		public override void OnVisitEnd(Visit p0)
		{
			base.OnVisitEnd(p0);
			Console.WriteLine ("On Visit END : " + p0.Place.Name);
		}
	}
}