﻿using System;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Webkit;
using Android.Provider;
using Android.Content.PM;
using Android.Bluetooth;
using Android.Graphics;

namespace SchoolRewards_App
{
    [Activity(Label = "SchoolRewards_App", MainLauncher = false, Icon = "@drawable/icon", Theme = "@android:style/Theme.NoTitleBar", ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
		public static Boolean appIsInForegroundMode=false;
        static ImageButton about;
        static ImageButton terms;
        static ImageButton contact;
		static ImageButton restaurant;
		static ImageButton home;
		static ImageButton profile;
		static ImageButton header;
		static ImageButton backButton;
		static RelativeLayout relativeTabbarLayout;
		static RelativeLayout relativeRedTabbar;
        //WebView wb;

		public static WebView wb;
        static string urlListen;

		MyGimbalServiceReceiver receiver;

		public static Context temp_context;
        public static string backUrl = "";



        protected override void OnCreate(Bundle bundle)
        {
			temp_context = this;

		//	try{
			Intent enableIntent = new Intent(BluetoothAdapter.ActionRequestEnable); 
			StartActivityForResult (enableIntent,0);
			//}catch(Exception e){
			//}

           base.OnCreate(bundle);


			receiver = new MyGimbalServiceReceiver(); 
			RegisterReceiver(receiver, new IntentFilter(Intent.ActionBootCompleted)); 
			//RegisterReceiver(receiver, new IntentFilter(LocationManager.ProvidersChangedAction));

           SetContentView(Resource.Layout.MainWebView);

     

    		about = FindViewById<ImageButton>(Resource.Id.imageButton_about);
			terms = FindViewById<ImageButton>(Resource.Id.imageButton_terms);
			contact = FindViewById<ImageButton>(Resource.Id.imageButton_contact);
			restaurant = FindViewById<ImageButton>(Resource.Id.imageButton_restaurant);
			home = FindViewById<ImageButton>(Resource.Id.imageButton_home);
			profile = FindViewById<ImageButton>(Resource.Id.imageButton_profile);
			header = FindViewById<ImageButton> (Resource.Id.imageButton_header);
			backButton = FindViewById<ImageButton> (Resource.Id.backButton_header);

			relativeTabbarLayout = FindViewById<RelativeLayout> (Resource.Id.relativeTabbarLayout);
            relativeRedTabbar = FindViewById<RelativeLayout> (Resource.Id.relativeRedTabbar);
             
           wb = FindViewById<WebView>(Resource.Id.webView_Main);
           string droidid = Settings.Secure.GetString(ContentResolver, Settings.Secure.AndroidId);
           // droidid = "58A4529D-4988-4485-A4A2-68A1AB0EA482"; //temp testing
           DOL.Android_ID = droidid;

		//string url = Intent.GetStringExtra ("url") ?? "https://app.schoolicious.org/SR_TermsCondition?DeviceID=" + droidid;
			string url = Intent.GetStringExtra ("url") ?? "http://staging.app.schoolicious.org/SR_TermsCondition?DeviceID=" + droidid;

           // string url = Intent.GetStringExtra("url") ?? "https://app.schoolicious.org/SR_TermsCondition?DeviceID=" + droidid;

            wb.Settings.JavaScriptEnabled = true;
           wb.LoadUrl(url);
           wb.SetWebViewClient(new HelloWebViewClient());

            relativeRedTabbar.Visibility = ViewStates.Invisible;

			if (url.Contains ("SR_TermsCondition") || url.Contains ("SR_Register") || url.Contains ("PrivacyPolicy")) {
				relativeTabbarLayout.Visibility = ViewStates.Invisible;
                relativeRedTabbar.Visibility = ViewStates.Invisible;
                backButton.Visibility = ViewStates.Invisible;
            }
            else if(url.Contains ("Home") || url.Contains("http://staging.app.schoolicious.org/?DeviceID")){
				relativeTabbarLayout.Visibility = ViewStates.Visible;
                relativeRedTabbar.Visibility = ViewStates.Invisible;
                backButton.Visibility = ViewStates.Invisible;
			}
			else {
				relativeTabbarLayout.Visibility = ViewStates.Invisible;
                relativeRedTabbar.Visibility = ViewStates.Visible;
			}
            
              
            /////// Click listener delegates //////
			about.Click += delegate
           {

				url = "http://staging.app.schoolicious.org/sr_about";
				wb.LoadUrl(url);
	
           };
			terms.Click += delegate
           {
				//url =  "https://app.schoolicious.org/SR_TermsCondition?DeviceID=" + droidid;
				url = "http://staging.app.schoolicious.org/SR_TermsCondition";
				Console.WriteLine ("terms clicked - "+url);
				wb.LoadUrl(url);
           };
			contact.Click += delegate
           {
				url = "http://staging.app.schoolicious.org/sr_Contact";
				wb.LoadUrl(url);
           };
			restaurant.Click += delegate
			{
				url = "http://staging.app.schoolicious.org/SR_RList.aspx?DeviceID=" + droidid;
				wb.LoadUrl(url);
			};
			profile.Click += delegate
			{
				url = "http://staging.app.schoolicious.org/SR_Profile.aspx?DeviceID=" + droidid;
				wb.LoadUrl(url);
			};
			header.Click += delegate {
				url = "http://staging.app.schoolicious.org/SR_TermsCondition?DeviceID=" + droidid;
				//url = "http://staging.app.schoolicious.org/SR_Home?DeviceID=" + droidid;
				wb.LoadUrl(url);
			};
			home.Click += delegate {
				url = "http://staging.app.schoolicious.org/SR_TermsCondition?DeviceID=" + droidid;
				//url = "http://staging.app.schoolicious.org/SR_Home?DeviceID=" + droidid;
				wb.LoadUrl(url);
			};
			backButton.Click += delegate {
				if(wb.CanGoBack()){
                    if (backUrl.Contains("SR_Profile"))
                    {
                        url = "http://staging.app.schoolicious.org/SR_TermsCondition?DeviceID=" + droidid;
                        wb.LoadUrl(url);
                        backUrl = "";
                    }
                    else {
                        wb.GoBack();
                    }
					
				}
			};

			StartService (new Intent (this, typeof(MyGimbalService)));


        }


        protected override void OnResume()
        {
			
			base.OnResume();
			appIsInForegroundMode = true;
            Console.WriteLine(">>>>>>>>>>>>>>>>>OnResume--> AppIsInForground - True");


        }


		protected override void OnPause() {
			base.OnPause();
			appIsInForegroundMode = false;
            Console.WriteLine(">>>>>>>>>>>>>>>>>Onpause--> AppIsInForground -"+appIsInForegroundMode+"");


        }

		protected override void OnStop()
		{
			base.OnStop();
			//appIsInForegroundMode = false;
            Console.WriteLine(">>>>>>>>>>>>>>>>>OnStop--> AppIsInForground -"+appIsInForegroundMode+"");

        }
		protected override void OnDestroy()
		{
			base.OnDestroy();
			//appIsInForegroundMode = false;
            Console.WriteLine(">>>>>>>>>>>>>>>>>OnDestroy--> AppIsInForground - False");


        }
        
        public class HelloWebViewClient : WebViewClient
        {
            

            public override bool ShouldOverrideUrlLoading(WebView view, string url)
            {
				Console.WriteLine ("url to load >>>>>>>>> "+url);

                if (url.StartsWith("https:") || url.StartsWith("http:"))
                {
                    view.LoadUrl(url);
                }
                else
                {
                    // Otherwise allow the OS to handle things like tel, mailto, etc.
                    Intent intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(url));
                    intent.AddFlags(ActivityFlags.NewTask);
                    Android.App.Application.Context.StartActivity(intent);
                }

                    return true;
            }

            public override void OnPageStarted(WebView view, string url, Bitmap favicon)
            {
                Console.WriteLine("url loaded - " + url);

                if (url.Contains("SR_TermsCondition?DeviceID="))
                {
                    relativeTabbarLayout.Visibility = ViewStates.Invisible;
                    relativeRedTabbar.Visibility = ViewStates.Invisible;
                    backButton.Visibility = ViewStates.Invisible;

                }
                else if (url.Contains("SR_Register") || url.Contains("PrivacyPolicy"))
                {
                    relativeTabbarLayout.Visibility = ViewStates.Invisible;
                    relativeRedTabbar.Visibility = ViewStates.Invisible;
                    backButton.Visibility = ViewStates.Visible;
                }
                else if (url.Contains("SR_Home"))
                {
                    relativeTabbarLayout.Visibility = ViewStates.Visible;
                    relativeRedTabbar.Visibility = ViewStates.Invisible;
                    backButton.Visibility = ViewStates.Invisible;
                }
                else if (url.Contains("SR_Profile"))
                {
                    relativeTabbarLayout.Visibility = ViewStates.Invisible;
                    relativeRedTabbar.Visibility = ViewStates.Visible;
                    backButton.Visibility = ViewStates.Visible;
                    backUrl = "SR_Profile";
                }
                else if (url.Contains("SR_RList") || url.Contains("SR_Profile")
                    || url.Contains("SR_Menu") || url.Contains("participatingrestaurant"))
                {
                    relativeTabbarLayout.Visibility = ViewStates.Invisible;
                    relativeRedTabbar.Visibility = ViewStates.Visible;
                    backButton.Visibility = ViewStates.Visible;
                }
                //else if (url.Contains("app.schoolicious.org/?DeviceID"))
                else if (url.Contains("staging.app.schoolicious.org/?DeviceID"))
                {
                    relativeTabbarLayout.Visibility = ViewStates.Visible;
                    relativeRedTabbar.Visibility = ViewStates.Invisible;
                    backButton.Visibility = ViewStates.Invisible;

                }
                else
                {
                    relativeTabbarLayout.Visibility = ViewStates.Invisible;
                    relativeRedTabbar.Visibility = ViewStates.Visible;
                    backButton.Visibility = ViewStates.Visible;
                }
            }

            public override void OnPageFinished(WebView view, string url)
            {

				
            }
            

        }
        
       
    }
		

  }
 

