using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SchoolRewards_App
{
    public class DOL
    {

        public static string Android_ID { get; set; }

        public static string hostURL { get; set; }

        public static string Client_Code { get; set; }

        public static string QR_Results { get; set; }

        public static string URL { get; set; }

        public static bool urlListen { get { return urlListen; } set { urlListen = false; } }

        public static bool checkButton { get { return checkButton; } set { checkButton = false; } }
    }
}